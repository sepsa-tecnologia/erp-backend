/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.MontoTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MontoTarifaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/monto-tarifa")
@Log4j2
public class MontoTarifaService extends AbstractService<MontoTarifa, MontoTarifaParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    @Context
    private SecurityContext sc;
    
    /**
     * Crea un monto tarifa
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearMontoTarifa(MontoTarifaParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearMontoTarifa");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            MontoTarifa montoTarifa = facades
                    .getMontoTarifaFacade()
                    .create(param, cp.getUserInfo());
            
            if(montoTarifa == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", montoTarifa.getIdTarifa());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Edita un monto tarifa
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ADMIN"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarMontoTarifa(MontoTarifaParam param) {

        try {
            
            param.log("Método: editarMontoTarifa");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            MontoTarifa monto = facades
                    .getMontoTarifaFacade()
                    .edit(param, cp.getUserInfo());
            
            if(monto != null) {
                JsonObject result = new JsonObject();
                result.addProperty("idTarifa", monto.getIdTarifa());
                return new OkResponse(ResponseCode.SERVICE_OK, result).build();
            } else {
                return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @Override
    public Facade<MontoTarifa, MontoTarifaParam, UserInfoImpl> getFacade() {
        return facades.getMontoTarifaFacade();
    }
}
