/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ServicioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/servicio")
@Log4j2
public class ServicioService extends AbstractService<Servicio, ServicioParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @Context
    private SecurityContext sc;
    
    /**
     * Crea un servicio
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearServicio(ServicioParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearServicio");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Servicio servicio = facades
                    .getServicioFacade()
                    .create(param, cp.getUserInfo());
            
            if(servicio == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", servicio.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en crearServicio:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<Servicio, ServicioParam, UserInfoImpl> getFacade() {
        return facades.getServicioFacade();
    }
}
