/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.PeriodoLiquidacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/periodo-liquidacion")
public class PeriodoLiquidacionService extends AbstractService<PeriodoLiquidacion, PeriodoLiquidacionParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<PeriodoLiquidacion, PeriodoLiquidacionParam, UserInfoImpl> getFacade() {
        return facades.getPeriodoLiquidacionFacade();
    }

}
