/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.comercial.ConfigBonificacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ConfigBonificacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/config-bonificacion")
public class ConfigBonificacionService extends AbstractService<ConfigBonificacion, ConfigBonificacionParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<ConfigBonificacion, ConfigBonificacionParam, UserInfoImpl> getFacade() {
        return facades.getConfigBonificacionFacade();
    }

}
