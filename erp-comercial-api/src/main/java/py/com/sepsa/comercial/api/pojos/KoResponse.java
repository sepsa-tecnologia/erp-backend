package py.com.sepsa.comercial.api.pojos;

import com.google.gson.JsonElement;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Respuesta KO al servicio
 * @author Daniel F. Escauriza Arza
 */
public class KoResponse extends AbstractResponse {
    
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final Response.Status STATUS = Response.Status
            .INTERNAL_SERVER_ERROR;
    private static final String MEDIA_TYPE = MediaType.APPLICATION_JSON;
    private static final boolean SUCCESS = false;
    //</editor-fold>
    
    /**
     * Constructor de KoResponse
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public KoResponse(ResponseCode responseCode, JsonElement payload) {
        super(STATUS, MEDIA_TYPE, SUCCESS, responseCode, payload);
    }
    
}
