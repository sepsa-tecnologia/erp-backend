/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application
 * @author Sergio D. Riveros Vazquez
 */
@ApplicationPath("/api")
public class App extends Application {
    
    public App() {}
    
}
