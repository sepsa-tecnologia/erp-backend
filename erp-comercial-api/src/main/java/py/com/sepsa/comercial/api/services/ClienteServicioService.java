/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.ClienteServicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteServicioParam;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ClienteServicioPojo;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ReporteClienteServicioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/cliente-servicio")
@Log4j2
public class ClienteServicioService extends AbstractService<ClienteServicio, ClienteServicioParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que obtiene la lista de servicios asociados al cliente
     *
     * @param idCliente Identificador de cliente
     * @param idProducto Identificador de producto
     * @param idServicio Identificador de servicio
     * @return Response
     */
    @RolesAllowed({"SEPSA_CORE", "SEPSA_ADMIN", "SEPSA_CLIENTE"})
    @Path("/asociado")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServicioAsociado(
            @QueryParam("idCliente") Integer idCliente,
            @QueryParam("idProducto") Integer idProducto,
            @QueryParam("idServicio") Integer idServicio) {
        AbstractResponse resp = null;

        if (idCliente == null) {
            return new BrResponse(ResponseCode.PARAM_ERROR, null)
                    .build();
        }

        log.info("Método: getServicioAsociado");
        log.info("idCliente: " + idCliente);

        try {
            List<ClienteServicioPojo> lista = facades
                    .getClienteServicioFacade()
                    .asociado(idCliente, idProducto, idServicio);
            
            Integer totalSize = lista.size();
            
            JsonArray json = new JsonArray();
            
            Gson gson = GsonUtils.generateDetault();
            
            for (ClienteServicioPojo item : lista) {
                json.add(gson.toJsonTree(item, ClienteServicioPojo.class));
            }

            JsonObject result = new JsonObject();
            result.add("data", json);
            result.addProperty("totalSize", totalSize);
            result.addProperty("firstResult", 0);
            result.addProperty("pageSize", totalSize);

            resp = new OkResponse(ResponseCode.SERVICE_OK, result);

        } catch (Exception ex) {
            log.fatal("Se produjo un error en getServicioAsociado:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Metodo que obtiene la lista de cliente servicio
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ADMIN"})
    @Path("/reporte")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReporteClienteServicio(@BeanParam ClienteServicioParam param) {
        AbstractResponse resp = null;

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null)
                    .build();
        }

        log.info("Método: getReporteClienteServicio");

        try {
            List<ReporteClienteServicioPojo> lista = facades
                    .getClienteServicioFacade()
                    .findReporte(param);
            
            Long totalSize = facades
                    .getClienteServicioFacade()
                    .findReporteSize(param);
            
            JsonArray json = new JsonArray();
            
            Gson gson = GsonUtils.generateDetault();
            
            for (ReporteClienteServicioPojo item : lista) {
                json.add(gson.toJsonTree(item, ReporteClienteServicioPojo.class));
            }

            JsonObject result = new JsonObject();
            result.add("data", json);
            result.addProperty("totalSize", totalSize);
            result.addProperty("firstResult", param.getFirstResult());
            result.addProperty("pageSize", param.getPageSize());

            resp = new OkResponse(ResponseCode.SERVICE_OK, result);

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<ClienteServicio, ClienteServicioParam, UserInfoImpl> getFacade() {
        return facades.getClienteServicioFacade();
    }
}
