package py.com.sepsa.comercial.api.pojos;

/**
 * Definición de códigos de respuesta
 *
 * @author Daniel F. Escauriza Arza
 */
public enum ResponseCode {

    SERVICE_OK(200, "Servicio ejecutado con éxito"),
    SERVICE_KO(500, "Se produjo un error en el servicio"),
    INVALID_PIN(401, "El PIN indicado no existe o no está habilitado"),
    KEY_EMPTY_NULL(402, "La clave es nula o vacia"),
    VERSION_SOFTWARE_EMPTY_NULL(403, "El software y/o version de software indicado no es válido"),
    MALFORMED_INSTALL_KEY(404, "Error en el JSON clave de instalacion"),
    EMPTY_KEY(405, "Clave de encriptacion de instalación nula o vacia"),
    INSTALLATION_USER_ERROR(406, "Instalación y/o usuario inhabilitado para la transmision"),
    CREDENTIAL_ERROR(407, "Error en las credenciales o el usuario está inactivo"),
    DATE_FORMAT_ERROR(408, "Error en el formato de la fecha o valor nulo"),
    SOFT_VERSION_ERROR(409, "Error en el software y/o version indicada"),
    EVENT_DATA_ERROR(410, "Error en los datos asociados a evento"),
    INSTALL_PROPERTIES_ERROR(411, "No se indicaron algunas propiedades de la instalacion"),
    DATA_ERROR(412, "No se encuentra ningún dato con los parámetros indicados"),
    KEY_EVENT_ERROR(413, "No existe ningun evento con la clave indicada"),
    ACCESS_DENIED(414, "Acceso denegado"),
    ACCESS_FORBIDDEN(415, "Acceso bloqueado"),
    USERNAME_PASSWORD_ERROR(417, "Nombre de usuario y/o contraseña indicado no es válido"),
    EXPIRED_TOKEN(416, "Token expirado"),
    PARAM_ERROR(417, "Error en parametros requeridos"),
    CONFIG_UPDATE_ERROR(417, "Configuración no actualizable"),
    RESOURCE_NOT_FOUND(418, "No se encontró el recurso"),
    RESOURCE_TYPE_NOT_FOUND(419, "No se encontró el tipo de recurso"),
    ATTRIBUTE_NOT_FOUND(420, "No se encontró el atributo"),
    DESCRIPTION_ERROR(421, "Ya existe esa descripcion"),
    VERSION_SOFTWARE_NOT_FOUND(422, "La version de software no existe"),
    DELETE_ERROR(423, "No se pudo eliminar el recurso"),
    DATA_EXISTS(424, "Elemento ya fue creado");
    //TODO: Completar con los códigos que se requiera
    //INVALID_RUC(501, "RUC invalido");

    /**
     * Código de respuesta
     */
    private int code;

    /**
     * Descripción de la respuesta
     */
    private String description;

    /**
     * Obtiene el código de respuesta
     *
     * @return Código de respuesta
     */
    public int getCode() {
        return code;
    }

    /**
     * Setea el código de respuesta
     *
     * @param code Código de respuesta
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Obtiene la descripción de la respuesta
     *
     * @return Descripción de la respuesta
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción de la respuesta
     *
     * @param description Descripción de la respuesta
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Constructor de ResponseCode
     *
     * @param code Código de respuesta
     * @param description Descripción de la respuesta
     */
    private ResponseCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

}
