/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicioTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioTarifaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/contrato-servicio-tarifa")
@Log4j2
public class ContratoServicioTarifaService extends AbstractService<ContratoServicioTarifa, ContratoServicioTarifaParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    @Context
    private SecurityContext sc;
    
    /**
     * Obtiene la lista de contrato servicio tarifa
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response guardarContratoServicioTarifa(ContratoServicioTarifaParam param) {
        
        AbstractResponse resp;
        
        param.log("Método: guardarContratoServicioTarifa");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            ContratoServicioTarifa contratoServicioTarifa = facades
                    .getContratoServicioTarifaFacade()
                    .editarOCrear(param, cp.getUserInfo());
            
            if(contratoServicioTarifa == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                
                JsonObject result = new JsonObject();
                result.addProperty("idContrato", param.getIdContrato());
                result.addProperty("idServicio", param.getIdServicio());
                result.addProperty("idTarifa", param.getIdTarifa());

                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<ContratoServicioTarifa, ContratoServicioTarifaParam, UserInfoImpl> getFacade() {
        return facades.getContratoServicioTarifaFacade();
    }
}
