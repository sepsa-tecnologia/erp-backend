/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.Serializable;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.facades.Facade;

import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan Bernal
 * @param <T> Parámetro de Tipo
 * @param <U> Parámetro de Tipo
 */
@Log4j2
public abstract class AbstractService<T, U extends CommonParam> implements Serializable{
    
    public abstract Facade<T, U, UserInfoImpl> getFacade();
    
    @Context
    protected SecurityContext sc;
    
    /**
     * Obtiene la lista de tipo telefono
     *
     * @param param
     * @return
     */
    @GET
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@BeanParam U param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            param.log("Método: find");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            List<T> lista = getFacade().find(param);
            Long totalSize = getFacade().findSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (T item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());
            
            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Se produjo un error en el servicio:", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @GET
    @Path("/id/{id}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") U param) {

        param.setId(Units.execute(() -> Integer.valueOf(param.getBruto())));
        
        if (!param.isValidToGetById()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            param.log("Método: findById");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            T t = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = t == null ? null : gson.toJsonTree(t);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Se produjo un error en el servicio:", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @GET
    @Path("/codigo/{codigo}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByCode(@PathParam("codigo") U param) {

        param.setCodigo(param.getBruto());
        
        if (!param.isValidToGetByCode()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            param.log("Método: findByCode");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            T t = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = t == null ? null : gson.toJsonTree(t);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Se produjo un error en el servicio:", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
}
