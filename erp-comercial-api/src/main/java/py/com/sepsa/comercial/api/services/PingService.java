/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import java.io.Serializable;
import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;


/**
 * Controlador para PING
 * @author Jonathan D. Bernal Fernández
 */
@Path("/ping")
@Log4j2
public class PingService implements Serializable {
    
    /**
     * Método que maneja las peticiones de ping
     * @return Response
     */
    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response ping() {
        
        log.info("Método: ping");
        
        AbstractResponse resp = new OkResponse(ResponseCode.SERVICE_OK, null);

        return resp.build();
    }
}
