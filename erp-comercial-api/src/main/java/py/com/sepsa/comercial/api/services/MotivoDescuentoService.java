/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.MotivoDescuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MotivoDescuentoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/motivo-descuento")
@Log4j2
public class MotivoDescuentoService extends AbstractService<MotivoDescuento, MotivoDescuentoParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @Context
    private SecurityContext sc;
    
    /**
     * Crea una instancia de motivo descuento
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearMotivoDescuento(MotivoDescuentoParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearMotivoDescuento");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            MotivoDescuento elem = facades
                    .getMotivoDescuentoFacade()
                    .create(param, cp.getUserInfo());
            
            if(elem == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", elem.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en crearMotivoDescuento:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Crea una instancia de motivo descuento
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarMotivoDescuento(MotivoDescuentoParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: editarMotivoDescuento");
            
            if(!param.isValidToEdit()) {
                return new BrResponse(ResponseCode.PARAM_ERROR, null).build();
            }
            
            MotivoDescuento elem = facades
                    .getMotivoDescuentoFacade()
                    .find(param.getId());
            
            if(elem == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                elem.setDescripcion(param.getDescripcion().trim());
                elem.setActivo(param.getActivo());
                elem.setPorcentual(param.getPorcentual());
                elem.setRangoDesde(param.getRangoDesde());
                elem.setRangoHasta(param.getRangoHasta());
                elem.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
                elem.setFechaDesde(param.getFechaDesde());
                elem.setFechaHasta(param.getFechaHasta());
                
                facades.getMotivoDescuentoFacade().edit(elem);
                
                JsonObject result = new JsonObject();
                result.addProperty("id", elem.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en editarMotivoDescuento:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<MotivoDescuento, MotivoDescuentoParam, UserInfoImpl> getFacade() {
        return facades.getMotivoDescuentoFacade();
    }
}
