/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ProductoComParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/producto")
@Log4j2
public class ProductoService extends AbstractService<ProductoCom, ProductoComParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Crea un producto
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProducto(ProductoComParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearProducto");
            
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal)sc.getUserPrincipal();
            
            ProductoCom producto = facades
                    .getProductoComFacade()
                    .create(param, customPrincipal.getUserInfo());
            
            if(producto == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", producto.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en crearProducto:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<ProductoCom, ProductoComParam, UserInfoImpl> getFacade() {
        return facades.getProductoComFacade();
    }
}
