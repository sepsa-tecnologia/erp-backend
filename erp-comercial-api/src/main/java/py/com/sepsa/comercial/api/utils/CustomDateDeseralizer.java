/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.utils;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class CustomDateDeseralizer extends StdDeserializer<Date> {

    public CustomDateDeseralizer() { 
        this(null); 
    } 
 
    public CustomDateDeseralizer(Class<?> vc) { 
        super(vc); 
    }
 
    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) 
            throws IOException, JsonProcessingException {
        
        SimpleDateFormat sdf;
        
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
            return sdf.parse(jp.getText());
        } catch (Exception e) {
        }
        
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(jp.getText());
        } catch (Exception e) {
        }
        
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(jp.getText());
        } catch (Exception e) {
        }
        
        throw new IOException("Error en el formato de fecha");
    }
}