package py.com.sepsa.comercial.api.pojos;

import com.google.gson.JsonElement;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Respuesta BAD REQUEST al servicio
 * @author Jonathan D. Bernal Fernandez
 */
public class BrResponse extends AbstractResponse {
    
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final Response.Status STATUS = Response.Status.BAD_REQUEST;
    private static final String MEDIA_TYPE = MediaType.APPLICATION_JSON;
    private static final boolean SUCCESS = false;
    //</editor-fold>
    
    /**
     * Constructor de BrResponse
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public BrResponse(ResponseCode responseCode, JsonElement payload) {
        super(STATUS, MEDIA_TYPE, SUCCESS, responseCode, payload);
    }
    
    /**
     * Constructor de BrResponse
     * @param responseCode Código de respuesta
     * @param param parámetros
     * @param payload Datos asociados a la respuesta
     */
    public BrResponse(ResponseCode responseCode, CommonParam param, JsonElement payload) {
        super(STATUS, MEDIA_TYPE, SUCCESS, responseCode, payload, param);
    }
    
}
