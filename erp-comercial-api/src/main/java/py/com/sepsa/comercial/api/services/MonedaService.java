/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MonedaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/moneda")
@Log4j2
public class MonedaService extends AbstractService<Moneda, MonedaParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @Context
    private SecurityContext sc;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearMoneda(MonedaParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearMoneda");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Moneda moneda = facades
                    .getMonedaFacade()
                    .create(param, cp.getUserInfo());
            
            if(moneda == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                Gson gson = GsonUtils.generateDetault();
                JsonElement result = gson.toJsonTree(moneda, Moneda.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarMoneda(MonedaParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: editarMoneda");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Moneda moneda = facades
                    .getMonedaFacade()
                    .edit(param, cp.getUserInfo());
            
            if(moneda == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                Gson gson = GsonUtils.generateDetault();
                JsonElement result = gson.toJsonTree(moneda, Moneda.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<Moneda, MonedaParam, UserInfoImpl> getFacade() {
        return facades.getMonedaFacade();
    }
}
