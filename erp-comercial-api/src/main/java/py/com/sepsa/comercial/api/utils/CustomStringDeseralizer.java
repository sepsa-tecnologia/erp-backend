/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.utils;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

/**
 *
 * @author Antonella Lucero
 */
public class CustomStringDeseralizer extends StdDeserializer<String> {

    public CustomStringDeseralizer() {
        this(null);
    }

    public CustomStringDeseralizer(Class<?> vc) {
        super(vc);
    }

    @Override
    public String deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        String str = jp.getText();
        if (str != null) {
            return str.trim();
        } else {
            return str;
        }

    }
}
