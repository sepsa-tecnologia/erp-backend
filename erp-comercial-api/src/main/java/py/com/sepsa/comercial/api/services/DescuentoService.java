/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.Calendar;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.Descuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.DescuentoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.CustomPrincipal.UserInfo;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/descuento")
@Log4j2
public class DescuentoService extends AbstractService<Descuento, DescuentoParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    /**
     * Contexto de seguridad
     */
    @Context
    private SecurityContext sc;

    /**
     * Crea una instancia de descuento
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearDescuento(DescuentoParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearDescuento");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Descuento elem = facades
                    .getDescuentoFacade()
                    .create(param, cp.getUserInfo());
            
            if(elem == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                Gson gson = GsonUtils.generateDetault();
                JsonElement result = gson.toJsonTree(elem, Descuento.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en crearDescuento:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Aprueba un contrato
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/aprobar")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response aprobarDescuento(DescuentoParam param) {
        
        AbstractResponse resp = null;

        try {
            
            param.log("Método: aprobarDescuento");
            
            /*Boolean valid = facades.getDescuentoFacade().validToActivate(param);
            
            if(!valid) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                CustomPrincipal principal =
                        (CustomPrincipal) sc.getUserPrincipal();
                
                if(verificarEtiqueta(descuento, principal.getUserInfo())) {
                    facades.getDescuentoFacade().activar(param);
                }
                
                JsonObject result = new JsonObject();
                result.addProperty("id", descuento.getId());
                result.addProperty("idEstado", descuento.getIdEstado());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }*/

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Inactiva la lista de descuentos vencidos
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/inactivar-vencidos")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response inactivarVencidos() {
        
        AbstractResponse resp;

        try {
            
            log.info("Método: inactivarVencidos");
            
            //Inactivamos los descuentos vencidos con fecha hasta el día anterior
            Calendar hasta = Calendar.getInstance();
            hasta.add(Calendar.DAY_OF_MONTH, -1);
            
            DescuentoParam param = new DescuentoParam();
            param.setCodigoEstado("ACTIVO");
            param.setFechaHastaHasta(hasta.getTime());
            param.setFirstResult(0);
            param.setPageSize(1000);
            
            List<Descuento> lista = facades
                    .getDescuentoFacade()
                    .find(param);
            
            JsonArray json = new JsonArray();
            Gson gson = GsonUtils.generateDetault();

            for (Descuento elem : lista) {
                DescuentoParam param1 = new DescuentoParam();
                param1.setId(elem.getId());
                facades.getDescuentoFacade().inactivar(param1);
                json.add(gson.toJsonTree(elem, Descuento.class));
            }
            
            JsonObject result = new JsonObject();
            result.add("data", json);
            result.addProperty("totalSize", lista.size());
            result.addProperty("firstResult", param.getFirstResult());
            result.addProperty("pageSize", param.getPageSize());
            
            resp = new OkResponse(ResponseCode.SERVICE_OK, result);

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Verifica el estado de las etiquetas de aprobación antes de cambiar de estado 
     * @param descuento Contrato
     * @return Estado
     */
    private Boolean verificarEtiqueta(Descuento descuento, UserInfo userInfo) {
        
        Boolean orden = Boolean.TRUE;
        
        /*List<ConfigAprobacion> list = facades
                .getConfigAprobacionFacade()
                .configuracionPendienteDescuento(descuento.getId(),
                        descuento.getIdMotivoDescuento());
        
        //No existen etiquetas pendientes, se puede aprobar
        if(list == null || list.isEmpty()) {
            return Boolean.TRUE;
        }
        
        PersonaEtiquetaParam param = new PersonaEtiquetaParam();
        param.setIdPersona(userInfo.getIdCliente());
        List<PersonaEtiqueta> list2 = facades
                .getPersonaEtiquetaFacade()
                .find(param);
        
        //La persona no puede aprobar nada, mantener estado
        if(list2 == null || list2.isEmpty()) {
            return Boolean.FALSE;
        }
        
        
        for (ConfigAprobacion configAprobacion : list) {
            
            Boolean contiene = Boolean.FALSE;
            
            for (PersonaEtiqueta personaEtiqueta : list2) {
                
                if(personaEtiqueta.getEtiqueta().getId()
                        .equals(configAprobacion.getIdEtiqueta())) {
                    
                    facades.getAprobacionDescuentoFacade().create(userInfo.getId(),
                            configAprobacion.getIdEtiqueta(),
                            Calendar.getInstance().getTime(), descuento, 'S', null);
                    contiene = Boolean.TRUE;
                }
            }
            
            if(!contiene) {
                orden = Boolean.FALSE;
                break;
            }
        }*/
        
        return orden;
    }
    
    /**
     * Inactiva un descuento
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/inactivar")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inactivarDescuento(DescuentoParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: inactivarDescuento");
            
            Descuento descuento = facades
                    .getDescuentoFacade()
                    .inactivar(param);
            
            if(descuento == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", descuento.getId());
                result.addProperty("idEstado", descuento.getIdEstado());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en inactivarDescuento:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<Descuento, DescuentoParam, UserInfoImpl> getFacade() {
        return facades.getDescuentoFacade();
    }
}
