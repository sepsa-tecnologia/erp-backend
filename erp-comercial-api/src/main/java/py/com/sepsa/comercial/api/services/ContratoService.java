/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.comercial.api.services;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.comercial.api.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.comercial.api.pojos.AbstractResponse;
import py.com.sepsa.comercial.api.pojos.BrResponse;
import py.com.sepsa.comercial.api.pojos.KoResponse;
import py.com.sepsa.comercial.api.pojos.OkResponse;
import py.com.sepsa.comercial.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.CustomPrincipal.UserInfo;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/contrato")
@Log4j2
public class ContratoService extends AbstractService<Contrato, ContratoParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    /**
     * Contexto de seguridad
     */
    @Context
    private SecurityContext sc;

    /**
     * Crea un contrato
     *
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearContrato(ContratoParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: crearContrato");

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal)sc.getUserPrincipal();
            
            Contrato contrato = facades
                    .getContratoFacade()
                    .create(param, customPrincipal.getUserInfo());

            if (contrato == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", contrato.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en crearContrato:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Aprueba un contrato
     *
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/aprobar")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response aprobarContrato(ContratoParam param) {

        AbstractResponse resp = null;

        try {

            param.log("Método: aprobarContrato");

            /*Contrato contrato = param.getId() == null
                    ? null
                    : facades.getContratoFacade().find(param.getId());

            if (contrato == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                CustomPrincipal principal
                        = (CustomPrincipal) sc.getUserPrincipal();

                if (verificarEtiqueta(contrato, principal.getUserInfo())) {
                    contrato.setEstado('A');
                    facades.getContratoFacade().edit(contrato);

                    facades.getClienteProductoFacade()
                            .actualizarClienteProductoServicio(
                                    contrato.getIdCliente(),
                                    contrato.getIdProducto());
                }

                JsonObject result = new JsonObject();
                result.addProperty("id", contrato.getId());
                result.addProperty("idEstado", contrato.getIdEstado());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }*/

        } catch (Exception ex) {
            log.fatal("Se produjo un error en aprobarContrato:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Verifica el estado de las etiquetas de aprobación antes de cambiar de
     * estado
     *
     * @param contrato Contrato
     * @return Estado
     */
    private Boolean verificarEtiqueta(Contrato contrato, UserInfo userInfo) {

        Boolean orden = Boolean.TRUE;
        
        /*List<ConfigAprobacionDetalle> list = facades
                .getConfigAprobacionFacade()
                .configuracionPendiente(contrato.getIdAprobacion(),
                        contrato.getConceptoFactura().getIdTipoEtiqueta());

        //No existen etiquetas pendientes, se puede aprobar
        if (list == null || list.isEmpty()) {
            return Boolean.TRUE;
        }

        PersonaEtiquetaParam param = new PersonaEtiquetaParam();
        param.setIdPersona(userInfo.getIdCliente());
        param.setActivo('S');
        List<PersonaEtiqueta> list2 = facades
                .getPersonaEtiquetaFacade()
                .find(param);

        //La persona no puede aprobar nada, mantener estado
        if (list2 == null || list2.isEmpty()) {
            return Boolean.FALSE;
        }

        for (ConfigAprobacionDetalle item : list) {

            Boolean contiene = Boolean.FALSE;

            for (PersonaEtiqueta personaEtiqueta : list2) {

                if (personaEtiqueta.getEtiqueta().getId()
                        .equals(item.getIdEtiqueta())) {

                    facades.getAprobacionContratoFacade().create(userInfo.getId(),
                            item.getIdEtiqueta(),
                            Calendar.getInstance().getTime(), contrato, 'S', null);
                    contiene = Boolean.TRUE;
                }
            }

            if (!contiene) {
                orden = Boolean.FALSE;
                break;
            }
        }*/

        return orden;
    }

    /**
     * Renueva la lista de contratos vencidos con renovación automática
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/renovar")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response renovar(ContratoParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: renovar");

            Contrato contrato = facades
                    .getContratoFacade()
                    .inactivar(param);

            if (contrato == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", contrato.getId());
                result.addProperty("idEstado", contrato.getIdEstado());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Inactiva un contrato
     *
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/inactivar")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response inactivarContrato(ContratoParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: inactivarContrato");

            Contrato contrato = facades
                    .getContratoFacade()
                    .inactivar(param);

            if (contrato == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                JsonObject result = new JsonObject();
                result.addProperty("id", contrato.getId());
                result.addProperty("idEstado", contrato.getIdEstado());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Se produjo un error en inactivarContrato:", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<Contrato, ContratoParam, UserInfoImpl> getFacade() {
        return facades.getContratoFacade();
    }
}
