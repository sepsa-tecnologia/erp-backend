package py.com.sepsa.core.api.pojos;

import com.google.gson.JsonElement;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Respuesta BAD REQUEST al servicio
 * @author Jonathan D. Bernal Fernandez
 */
public class FbResponse extends AbstractResponse {
    
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final Response.Status STATUS = Response.Status.FORBIDDEN;
    private static final String MEDIA_TYPE = MediaType.APPLICATION_JSON;
    private static final boolean SUCCESS = false;
    //</editor-fold>
    
    /**
     * Constructor de BrResponse
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public FbResponse(ResponseCode responseCode, JsonElement payload) {
        super(STATUS, MEDIA_TYPE, SUCCESS, responseCode, payload);
    }
    
}
