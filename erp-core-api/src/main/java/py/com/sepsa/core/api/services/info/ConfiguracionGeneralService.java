/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import javax.inject.Inject;
import javax.ws.rs.Path;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.info.ConfiguracionGeneral;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionGeneralParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Gustavo Benitez
 */
@Path("/configuracion-general")
@Log4j2
public class ConfiguracionGeneralService extends AbstractService<ConfiguracionGeneral, ConfiguracionGeneralParam>{

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @Override
    public Facade<ConfiguracionGeneral, ConfiguracionGeneralParam, UserInfoImpl> getFacade() {
        return facades.getConfiguracionGeneralFacade();
    }
}
