/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services;

import com.google.gson.JsonObject;
import java.io.Serializable;
import java.util.Calendar;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;

/**
 * Controlador para la sincrionización
 * @author Jonathan D. Bernal Fernández
 */
@Path("/time")
@Log4j2
public class TimeService implements Serializable {
    
    /**
     * Obtiene el horario del servidor en milisegundos
     * @return Response
     */
    @PermitAll
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTime() {
        
        AbstractResponse resp;
        JsonObject respValue = new JsonObject();
        
        log.info("Método: getTime");
        
        try {

            Calendar now = Calendar.getInstance();
            respValue.addProperty("time", now.getTimeInMillis());
            resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);

        } catch(Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp == null ? null : resp.build();
    }
}
