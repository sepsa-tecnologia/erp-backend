/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.info.filters.RucParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.RucPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Jonathan Bernal
 */
@Path("ruc")
@Log4j2
public class RucService {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @GET
    @Path("/consulta/{ruc}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultaRuc(@PathParam("ruc") RucParam param) {

        try {

            param.setRuc(Units.execute(() -> param.getBruto().trim()));
            param.log("Método: consultaRuc");

            RucPojo ruc = facades.getRucFacade().find(param);

            if (ruc == null) {
                return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
            } else {
                Gson gson = GsonUtils.generateDetault();
                JsonElement result = gson.toJsonTree(ruc);
                return new OkResponse(ResponseCode.SERVICE_OK, result).build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

}
