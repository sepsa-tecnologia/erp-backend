/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;
import py.com.sepsa.erp.ejb.entities.info.filters.RepositorioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para Repositorio
 *
 * @author Jonathan Bernal
 */
@Path("/repositorio")
@Log4j2
public class RepositorioService extends AbstractService<Repositorio, RepositorioParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearRepositorio(RepositorioParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearRepositorio");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Repositorio repositorio = facades
                    .getRepositorioFacade()
                    .create(param, cp.getUserInfo());
            
            if(repositorio == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                Gson gson = GsonUtils.generateDetault();
                JsonElement result = gson.toJsonTree(repositorio);
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        
        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarRepositorio(RepositorioParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: editarRepositorio");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Repositorio repositorio = facades
                    .getRepositorioFacade()
                    .edit(param, cp.getUserInfo());
            
            if(repositorio == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            } else {
                Gson gson = GsonUtils.generateDetault();
                JsonElement result = gson.toJsonTree(repositorio);
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        
        return resp.build();
    }

    @Override
    public Facade<Repositorio, RepositorioParam, UserInfoImpl> getFacade() {
        return facades.getRepositorioFacade();
    }
}
