/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.math.BigInteger;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.filters.LocalParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para local
 *
 * @author Jonathan Bernal
 */
@Path("/local")
@Log4j2
public class LocalService extends AbstractService<Local, LocalParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo para setear local
     *
     * @param param
     * @return
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearLocal(LocalParam param) {

        param.log("Método: crearLocal");
        
        AbstractResponse resp = null;
        
        try {
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            Local local = facades
                    .getLocalFacade()
                    .create(param, customPrincipal.getUserInfo());

            if (local != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(local, Local.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @GET
    @Path("/gln/{gln}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByGln(@PathParam("gln") LocalParam param) {

        param.setGln(Units.execute(() -> new BigInteger(param.getBruto())));
        
        if (!param.isValidToGetByGln()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findByGln");
            
            Local local = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = local == null ? null : gson.toJsonTree(local);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @GET
    @Path("/idExterno/{idExterno}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByIdExterno(@PathParam("idExterno") LocalParam param) {

        param.setIdExterno(param.getBruto());
        
        if (!param.isValidToGetByIdExterno()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findByIdExterno");
            
            Local local = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = local == null ? null : gson.toJsonTree(local);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    /**
     * Metodo para editar local
     *
     * @param param
     * @return
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarLocal(LocalParam param) {

        param.log("Método: editarLocal");
        
        AbstractResponse resp = null;
        
        try {
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            Local local = facades
                    .getLocalFacade()
                    .edit(param, customPrincipal.getUserInfo());

            if (local != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(local, Local.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<Local, LocalParam, UserInfoImpl> getFacade() {
        return facades.getLocalFacade();
    }
}
