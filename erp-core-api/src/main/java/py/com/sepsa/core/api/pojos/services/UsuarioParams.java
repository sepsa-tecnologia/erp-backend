/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.pojos.services;

import javax.ws.rs.QueryParam;

/**
 * Pojo para Usuario
 *
 * @author Jonathan Bernal
 */
public class UsuarioParams {

    @QueryParam("id")
    private Integer id;
    @QueryParam("idPersona")
    private Integer idPersona;
    @QueryParam("usuario")
    private String usuario;
    @QueryParam("contrasena")
    private String contrasena;
    @QueryParam("nuevaContrasena")
    private String nuevaContrasena;
    @QueryParam("estado")
    private Character estado;
    @QueryParam("email")
    private String email;
    @QueryParam("telefono")
    private String telefono;
    @QueryParam("validacion")
    private Character validacion;
    @QueryParam("idPersonaFisica")
    private Integer idPersonaFisica;
    @QueryParam("pageSize")
    private Integer pageSize;
    @QueryParam("firstResult")
    private Integer firstResult;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the idPersona
     */
    public Integer getIdPersona() {
        return idPersona;
    }

    /**
     * @param idPersona the idPersona to set
     */
    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setNuevaContrasena(String nuevaContrasena) {
        this.nuevaContrasena = nuevaContrasena;
    }

    public String getNuevaContrasena() {
        return nuevaContrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getContrasena() {
        return contrasena;
    }
    /**
     * @return the estado
     */
    public Character getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Character estado) {
        this.estado = estado;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the validacion
     */
    public Character getValidacion() {
        return validacion;
    }

    /**
     * @param validacion the validacion to set
     */
    public void setValidacion(Character validacion) {
        this.validacion = validacion;
    }

    /**
     * @return the idPersonaFisica
     */
    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    /**
     * @param idPersonaFisica the idPersonaFisica to set
     */
    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    /**
     * Metodo para validar campos obligarorios de User
     *
     * @return
     */
    public boolean validateParams() {
        boolean noError = true;

        if (this.idPersona == null) {
            noError = false;
        }

        if (this.usuario == null || this.usuario.isEmpty()) {
            noError = false;
        }
        if (this.contrasena == null || this.contrasena.isEmpty()) {
            noError = false;
        }

        if (this.estado == null) {
            noError = false;
        }

        if (this.validacion == null) {
            noError = false;
        }

        return noError;
    }

    public boolean editPassword() {
        boolean noError = true;
        if (this.usuario == null || this.usuario.isEmpty()) {
            noError = false;
        }

        if (this.contrasena == null || this.contrasena.isEmpty()) {
            noError = false;
        }

        if (this.nuevaContrasena == null || this.nuevaContrasena.isEmpty()) {
            noError = false;
        }

        return noError;
    }

    public boolean editPass() {
        boolean noError = true;
        if (this.usuario == null || this.usuario.isEmpty()) {
            noError = false;
        }

        if (this.contrasena == null || this.contrasena.isEmpty()) {
            noError = false;
        }

        return noError;
    }

    /**
     * @return the maxResults
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the firstResult
     */
    public Integer getFirstResult() {
        return firstResult;
    }

    /**
     * @param firstResult the firstResult to set
     */
    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public boolean isValid() {
        this.setPageSize(getPageSize() == null || getPageSize() == 0 ? 10 : getPageSize());

        this.setFirstResult(getFirstResult() == null ? 0 : getFirstResult());

        return true;
    }
}
