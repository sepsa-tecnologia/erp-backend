/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.info.ProductoRelacionado;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoRelacionadoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Williams Vera
 */
@Path("/producto-relacionado")
@Log4j2
public class ProductoRelacionadoService extends AbstractService<ProductoRelacionado, ProductoRelacionadoParam> {
    
     /**
     * Facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProductoRelacionado(ProductoRelacionadoParam param) {

        AbstractResponse resp;

        param.log("Método: crearProductoRelacionado");

        try {

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            ProductoRelacionado productoRelacionado = getFacade().create(param, customPrincipal.getUserInfo());

            if(productoRelacionado != null) {
                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(productoRelacionado, ProductoRelacionado.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
     @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProductoRelacionado(ProductoRelacionadoParam param) {

        AbstractResponse resp;

        param.log("Método: editarProductoRelacionado");

        try {

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            ProductoRelacionado productoRelacionado = getFacade().edit(param, customPrincipal.getUserInfo());

            if(productoRelacionado != null) {
                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(productoRelacionado, ProductoRelacionado.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }


    @Path("/crear-editar")
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCrearProductoRelacionado(ProductoRelacionadoParam param) {

        AbstractResponse resp;

        param.log("Método: editarCrearProductoRelacionado");

        try {

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            ProductoRelacionado productoRelacionado = getFacade().editarOCrear(param, customPrincipal.getUserInfo());

            if(productoRelacionado != null) {
                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(productoRelacionado, ProductoRelacionado.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @Path("/relacionado")
    @GET
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findRelacionado(@BeanParam ProductoRelacionadoParam param) {

        if (!param.isValidToListRelacionado()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            param.log("Método: findRelacionado");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            List<ProductoRelacionado> lista = facades.getProductoRelacionadoFacade().findRelacionados(param);
            Long totalSize = facades.getProductoRelacionadoFacade().findRelacionadosSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (ProductoRelacionado item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());
            
            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @Override
    public Facade<ProductoRelacionado,ProductoRelacionadoParam, UserInfoImpl> getFacade() {
        return facades.getProductoRelacionadoFacade();
    }
}
