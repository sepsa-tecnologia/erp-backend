/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.proceso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.pojos.Result;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.entities.proceso.pojos.LotePojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan
 */
@Path("/lote")
@Log4j2
public class LoteService extends AbstractService<Lote, LoteParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearLote(LoteParam param) {

        AbstractResponse resp;

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch (cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            param.log("Método: crearLote");

            Lote lote = facades.getLoteFacade().create(param, cp.getUserInfo());

            if (lote != null) {
                resp = new OkResponse(ResponseCode.SERVICE_OK, null);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProceso(LoteParam param) {

        AbstractResponse resp;

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch (cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            param.log("Método: editarLote");

            Lote lote = facades.getLoteFacade().edit(param, cp.getUserInfo());

            if (lote != null) {
                resp = new OkResponse(ResponseCode.SERVICE_OK, null);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/resultado/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response obtenerResultadoLote(@PathParam("id") Integer id, @BeanParam LoteParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch (cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }

            param.setId(id);
            param.log("Método: obtenerResultadoLote");

            LotePojo lote = facades.getLoteFacade().obtenerResultado(param);

            if (lote != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(lote.getResultado() + ""));

                String filename = FilenameUtils.removeExtension(lote.getNombreArchivo());

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("text/csv");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"resultado_%s.csv\"", filename));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/archivos-relacionados/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response obtenerArchivosRelacionados(@PathParam("id") Integer id, @BeanParam LoteParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch (cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            param.setId(id);
            param.log("Método: obtenerArchivosRelacionados");

            List<LoteArchivo> archivos = facades.getLoteFacade().obtenerLoteArchivo(param);

            if (archivos == null || archivos.isEmpty()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            } else if (archivos.size() > 1) {

                try ( ByteArrayOutputStream baos = new ByteArrayOutputStream();  ZipOutputStream zos = new ZipOutputStream(baos)) {

                    for (LoteArchivo lote : archivos) {
                        byte[] fileContent = Base64.getDecoder().decode(lote.getContenido() + "");
                        ZipEntry entry = new ZipEntry(FilenameUtils.removeExtension(lote.getNombreArchivo()) + ".csv");
                        zos.putNextEntry(entry);
                        zos.write(fileContent);
                        zos.closeEntry();
                    }

                    zos.finish();

                    ByteArrayInputStream inputStream = new ByteArrayInputStream(baos.toByteArray());
                    return Response.ok(inputStream)
                            .type("application/zip")
                            .header("Content-Disposition", "attachment; filename=\"resultados.zip\"")
                            .build();

                } catch (IOException e) {
                    log.error("Error al generar el archivo ZIP", e);
                    return Response.serverError().build();
                }

            } else {

                String resultado = "";
                String filenam = "";
                for (LoteArchivo archivo : archivos) {
                    resultado = archivo.getContenido();
                    filenam = archivo.getNombreArchivo();
                }
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(resultado));

                String filename = FilenameUtils.removeExtension(filenam);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("text/csv");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"resultado_%s.csv\"", filename));
                return responseBuilder.build();

            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<Lote, LoteParam, UserInfoImpl> getFacade() {
        return facades.getLoteFacade();
    }
}
