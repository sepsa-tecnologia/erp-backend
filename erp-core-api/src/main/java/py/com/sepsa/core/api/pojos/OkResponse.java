package py.com.sepsa.core.api.pojos;

import com.google.gson.JsonElement;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Respuesta OK al servicio
 * @author Daniel F. Escauriza Arza
 */
public class OkResponse extends AbstractResponse {
    
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final Response.Status STATUS = Response.Status.OK;
    private static final String MEDIA_TYPE = MediaType.APPLICATION_JSON;
    private static final boolean SUCCESS = true;
    //</editor-fold>
    
    /**
     * Constructor de OkResponse
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public OkResponse(ResponseCode responseCode, JsonElement payload) {
        super(STATUS, MEDIA_TYPE, SUCCESS, responseCode, payload);
    }
    
}
