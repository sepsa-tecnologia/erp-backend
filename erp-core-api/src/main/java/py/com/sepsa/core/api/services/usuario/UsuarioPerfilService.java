/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.usuario;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.pojos.Result;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.usuario.Perfil;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioPerfil;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioPerfilParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPerfilPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/usuario-perfil")
@Log4j2
public class UsuarioPerfilService extends AbstractService<UsuarioPerfil, UsuarioPerfilParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que edita o crea la relacion entre usuario y perfil
     *
     * @param param
     * @return
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCrearUsuarioPerfil(UsuarioPerfilParam param) {

        AbstractResponse resp;

        param.log("Método: editarCrearUsuarioPerfil");

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            UsuarioPerfil userPerfil = getFacade().editarOCrear(param, cp.getUserInfo());

            if(userPerfil != null) {
                actualizarPersonaEtiqueta(param);

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(userPerfil, UsuarioPerfil.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("{id}/permisos")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response getPermisosById(@PathParam("id") Integer id){
        
        UsuarioPerfilParam param = new UsuarioPerfilParam();
        param.setId(id);
        param.log("Método: getPermisosById");

        try {
            JsonElement permisos = facades.getUsuarioPerfilFacade().findPermisosById(param);

            if(permisos != null){
                JsonElement result = permisos;
                return Result.createInstance().ok()
                    .setPayload(result)
                    .build();
            }else{
                return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .setParametros(param)
                    .build();       
            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    
    @Path("/relacionado")
    @GET
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findRelacionado(@BeanParam UsuarioPerfilParam param) {

        if (!param.isValidToListRelacionado()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            param.log("Método: findRelacionado");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            List<UsuarioPerfil> lista = facades.getUsuarioPerfilFacade().findRelacionados(param);
            Long totalSize = facades.getUsuarioPerfilFacade().findRelacionadosSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (UsuarioPerfil item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());
            
            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    private void actualizarPersonaEtiqueta(UsuarioPerfilParam param1) {
        
        /*Usuario usuario = facades.getUsuarioFacade().find(param1.getIdUsuario());
        
        Perfil perfil = facades.getPerfilFacade().find(param1.getIdPerfil());
        
        if(usuario == null || usuario.getIdPersonaFisica() == null
                || perfil == null) {
            return;
        }
        
        String codigo = String.format("%s_%s",
                perfil.getSoftware().getCodigo(),
                perfil.getDescripcion());
        
        EtiquetaParam param = new EtiquetaParam();
        param.setCodigo(codigo);
        param.isValidToList();
        List<EtiquetaPojo> list = facades
                .getEtiquetaFacade()
                .find(param);
        
        if(list == null || list.isEmpty()) {
            return;
        }
        
        EtiquetaPojo etiqueta = list.get(0);
        
        PersonaEtiquetaPK pk = new PersonaEtiquetaPK(etiqueta.getId(),
                usuario.getIdPersonaFisica());
        
        PersonaEtiqueta pe = facades.getPersonaEtiquetaFacade().find(pk);
        
        if(pe == null) {
            pe = new PersonaEtiqueta(pk, param1.getEstado());
            facades.getPersonaEtiquetaFacade().create(pe);
        } else {
            pe.setEstado(param1.getEstado());
            facades.getPersonaEtiquetaFacade().edit(pe);
        }*/
    }

    @Override
    public Facade<UsuarioPerfil, UsuarioPerfilParam, UserInfoImpl> getFacade() {
        return facades.getUsuarioPerfilFacade();
    }
}
