/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.proceso;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesamientoArchivoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan
 */
@Path("/procesamiento-archivo")
@Log4j2
public class ProcesamientoArchivoService extends AbstractService<ProcesamientoArchivo, ProcesamientoArchivoParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @Context
    private SecurityContext sc;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProcesamientoArchivo(ProcesamientoArchivoParam param) {

        param.log("Método: crearProcesamientoArchivo");
        
        AbstractResponse resp;
        
        try {
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            ProcesamientoArchivo procesamientoArchivo = facades
                    .getProcesamientoArchivoFacade()
                    .create(param, customPrincipal.getUserInfo());

            if (procesamientoArchivo != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(procesamientoArchivo, ProcesamientoArchivo.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProcesamientoArchivo(ProcesamientoArchivoParam param) {

        param.log("Método: editarProcesamientoArchivo");
        
        AbstractResponse resp;
        
        try {
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            ProcesamientoArchivo procesamientoArchivo = facades
                    .getProcesamientoArchivoFacade()
                    .edit(param, customPrincipal.getUserInfo());

            if (procesamientoArchivo != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(procesamientoArchivo, ProcesamientoArchivo.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/reenviar/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response reenviarProcesamientoArchivo(@PathParam("id") Integer id) {
        
        AbstractResponse resp;
        
        try {
            
            ProcesamientoArchivoParam param = new ProcesamientoArchivoParam();
            param.setId(id);
            param.log("Método: reenviarProcesamientoArchivo");

            ProcesamientoArchivo procesamientoArchivo = facades.getProcesamientoArchivoFacade().reenviar(param);

            if (procesamientoArchivo != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(procesamientoArchivo, ProcesamientoArchivo.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @Override
    public Facade<ProcesamientoArchivo, ProcesamientoArchivoParam, UserInfoImpl> getFacade() {
        return facades.getProcesamientoArchivoFacade();
    }
}
