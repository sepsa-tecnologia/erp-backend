
package py.com.sepsa.core.api.pojos;

import com.google.gson.JsonElement;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Respuesta genérica del servicio
 * @author Daniel F. Escauriza Arza
 */
public class AbstractResponse {
    
    /**
     * Estatus de la respuesta
     */
    private final Response.Status status;
    
    /**
     * Tipo de respuesta
     */
    private final String mediaType;
    
    /**
     * Respuesta encapsulada 
     */
    private final ResponseWrapper responseWrapper;
    
    /**
     * Obtiene una respuesta al servicio
     * @return Respuesta al servicio
     */
    public Response build() {
        ResponseBuilder builder = Response
                .status(status)
                //.type(mediaType)
                .entity(responseWrapper.toJson());
        
        return builder.build();
    }

    /**
     * Setea si existe una actualización de config
     * @param configUpdate Bandera que indica si existe una actualización de config
     */
    public void setConfigUpdate(boolean configUpdate) {
        responseWrapper.setConfigUpdate(configUpdate);
    }
    
    /**
     * Seta si existe un comando a ejecutar
     * @param existCommand Bandera que indica si existe un comando a ejecutar
     */
    public void setExistCommand(boolean existCommand) {
        responseWrapper.setExistCommand(existCommand);
    }
    
    /**
     * Constructor de AbstractResponse
     * @param status Estatus de la respuesta
     * @param mediaType Tipo de respuesta
     * @param success Bandera que indica si la respuesta se realizo con éxito
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public AbstractResponse(Response.Status status, String mediaType, 
            boolean success, ResponseCode responseCode, JsonElement payload) {
        
        this.status = status;
        this.mediaType = mediaType;
        this.responseWrapper = new ResponseWrapper(success, responseCode, 
                payload);
    }
    
    /**
     * Constructor de AbstractResponse
     * @param status Estatus de la respuesta
     * @param mediaType Tipo de respuesta
     * @param success Bandera que indica si la respuesta se realizo con éxito
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     * @param parametros Parámetros
     */
    public AbstractResponse(Response.Status status, String mediaType, 
            boolean success, ResponseCode responseCode, JsonElement payload,
            CommonParam parametros) {
        
        this.status = status;
        this.mediaType = mediaType;
        this.responseWrapper = new ResponseWrapper(success, responseCode, 
                payload, parametros);
    }
}
