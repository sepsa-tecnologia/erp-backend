/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.proceso;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.proceso.TipoArchivo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Path("/tipo-archivo")

public class TipoArchivoService extends AbstractService<TipoArchivo, CommonParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @Override
    public Facade<TipoArchivo, CommonParam, UserInfoImpl> getFacade() {
        return facades.getTipoArchivoFacade();
    }
}
