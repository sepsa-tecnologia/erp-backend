/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.erp.ejb.entities.info.PersonaEmailNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailNotificacionParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.PersonaEmailNotificacionPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/persona-email-notificacion")
@Log4j2
public class PersonaEmailNotificacionService extends AbstractService<PersonaEmailNotificacion, PersonaEmailNotificacionParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que obtiene la lista de contacto email notificacion asociado
     *
     * @param idContacto Identificador de contacto
     * @param idEmail Identificador de email
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/asociado")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPersonaEmailNotificacionAsociado(
            @QueryParam("idContacto") Integer idContacto,
            @QueryParam("idEmail") Integer idEmail) {
        AbstractResponse resp = null;

        if (idContacto == null || idEmail == null) {
            return new BrResponse(ResponseCode.PARAM_ERROR, null)
                    .build();
        }

        log.info("Método: getPersonaEmailNotificacionAsociado");
        log.info("idContacto: " + idContacto);
        log.info("idEmail: " + idEmail);

        try {
            List<PersonaEmailNotificacionPojo> lista = facades
                    .getPersonaEmailNotificacionFacade()
                    .asociado(idContacto, idEmail);
            
            Integer totalSize = lista.size();
            
            JsonArray json = new JsonArray();
            
            Gson gson = GsonUtils.generateDetault();
            
            for (PersonaEmailNotificacionPojo item : lista) {
                json.add(gson.toJsonTree(item,
                        PersonaEmailNotificacionPojo.class));
            }

            JsonObject result = new JsonObject();
            result.add("data", json);
            result.addProperty("totalSize", totalSize);
            result.addProperty("firstResult", 0);
            result.addProperty("pageSize", totalSize);

            resp = new OkResponse(ResponseCode.SERVICE_OK, result);

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<PersonaEmailNotificacion, PersonaEmailNotificacionParam, UserInfoImpl> getFacade() {
        return facades.getPersonaEmailNotificacionFacade();
    }
}
