/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.MenuPerfil;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuPerfilParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/menu-perfil")
@Log4j2
public class MenuPerfilService extends AbstractService<MenuPerfil, MenuPerfilParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCrearMenuPerfil(MenuPerfilParam param) {

        AbstractResponse resp;

        param.log("Método: editarCrearMenuPerfil");

        try {

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            MenuPerfil userPerfil = getFacade().editarOCrear(param, customPrincipal.getUserInfo());

            if(userPerfil != null) {
                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(userPerfil, MenuPerfil.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @Path("/relacionado")
    @GET
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findRelacionado(@BeanParam MenuPerfilParam param) {

        if (!param.isValidToListRelacionado()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            param.log("Método: findRelacionado");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            List<MenuPerfil> lista = facades.getMenuPerfilFacade().findRelacionados(param);
            Long totalSize = facades.getMenuPerfilFacade().findRelacionadosSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (MenuPerfil item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());
            
            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @Override
    public Facade<MenuPerfil, MenuPerfilParam, UserInfoImpl> getFacade() {
        return facades.getMenuPerfilFacade();
    }
}
