/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionGeneralParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.FirmaDigitalPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Gustavo Benitez
 */
@Path("/firma-digital")
@Log4j2
public class FirmaDigitalService {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @Context
    private SecurityContext sc;

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findFirmaDigital(@BeanParam ConfiguracionGeneralParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdClienteSepsa(cp.getUserInfo().getIdClienteSepsa());
            
            param.log("Método: findFirmaDigital");
            param.isValidToList();
            List<FirmaDigitalPojo> lista = facades.getConfiguracionGeneralFacade().getFirmaDigital(param);

            if (lista != null) {
                Gson gson = GsonUtils.generateDetault();
                FirmaDigitalPojo item = lista.isEmpty() ? null : lista.get(0);
                JsonObject respValue = item == null ? null : gson.toJsonTree(item).getAsJsonObject();
                return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();
            } else {
                return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
}
