/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.ConfigAprobacion;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfigAprobacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal
 */
@Path("/config-aprobacion")
@Log4j2
public class ConfigAprobacionService extends AbstractService<ConfigAprobacion, ConfigAprobacionParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Crea un config aprobacion
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearConfigAprobacion(ConfigAprobacionParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: crearConfigAprobacion");
            
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            ConfigAprobacion configAprobacion = facades
                    .getConfigAprobacionFacade()
                    .create(param, customPrincipal.getUserInfo());
            
            if(configAprobacion == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {
                
                Map<String, String> atributos = new HashMap<>();
                atributos.put("id", configAprobacion.getId() + " ");
                atributos.put("id_tipo_etiqueta", configAprobacion.getIdTipoEtiqueta() + " ");
                atributos.put("descripcion", configAprobacion.getDescripcion() + " ");

                facades.getRegistroFacade().create("info", "config_aprobacion", atributos, customPrincipal.getUserInfo());
                
                JsonObject result = new JsonObject();
                result.addProperty("id", configAprobacion.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Edita una configuración de aprobacion
     * @param param Parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editConfigAprobacion(ConfigAprobacionParam param) {
        
        AbstractResponse resp;

        try {
            
            param.log("Método: editConfigAprobacion");
            
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            ConfigAprobacion configAprobacion = facades
                    .getConfigAprobacionFacade()
                    .edit(param, customPrincipal.getUserInfo());
            
            if(configAprobacion == null) {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            } else {

                Map<String, String> pk = new HashMap<>();
                pk.put("id", configAprobacion.getId() + " ");
                Map<String, String> atributos = new HashMap<>();
                atributos.put("id_tipo_etiqueta", configAprobacion.getIdTipoEtiqueta() + " ");
                atributos.put("descripcion", configAprobacion.getDescripcion()+ " ");

                facades.getRegistroFacade().edit("info", "config_aprobacion", atributos, customPrincipal.getUserInfo(), pk);

                JsonObject result = new JsonObject();
                result.addProperty("id", configAprobacion.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, result);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<ConfigAprobacion, ConfigAprobacionParam, UserInfoImpl> getFacade() {
        return facades.getConfigAprobacionFacade();
    }
}
