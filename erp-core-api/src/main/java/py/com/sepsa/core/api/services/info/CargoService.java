/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.Cargo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/cargo")
public class CargoService extends AbstractService<Cargo, CommonParam> {

    /**
     * Facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<Cargo, CommonParam, UserInfoImpl> getFacade() {
        return facades.getCargoFacade();
    }

}
