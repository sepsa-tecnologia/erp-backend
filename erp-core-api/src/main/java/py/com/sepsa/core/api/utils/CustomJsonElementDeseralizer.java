/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.utils;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.google.gson.JsonElement;
import java.io.IOException;

/**
 *
 * @author Jonathan
 */
public class CustomJsonElementDeseralizer extends StdDeserializer<JsonElement> {

    private final com.google.gson.JsonParser parser = new com.google.gson.JsonParser();

    public CustomJsonElementDeseralizer() {
        this(null);
    }

    public CustomJsonElementDeseralizer(Class<?> vc) {
        super(vc);
    }

    @Override
    public JsonElement deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        try {
            JsonNode node = jp.getCodec().readTree(jp);
            return com.google.gson.JsonParser.parseString(node.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new IOException("Error en el formato de json");
    }
}
