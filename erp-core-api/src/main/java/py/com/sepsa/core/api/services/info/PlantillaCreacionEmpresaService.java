/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.core.api.services.info;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.info.PlantillaCreacionEmpresa;
import py.com.sepsa.erp.ejb.entities.info.filters.PlantillaCreacionEmpresaParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Williams Vera
 */
@Path("plantilla-creacion-empresa")
public class PlantillaCreacionEmpresaService extends AbstractService<PlantillaCreacionEmpresa, PlantillaCreacionEmpresaParam>  {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @Override
    public Facade<PlantillaCreacionEmpresa, PlantillaCreacionEmpresaParam, UserInfoImpl> getFacade() {
        return facades.getPlantillaCreacionEmpresaFacade();
    }
    
}
