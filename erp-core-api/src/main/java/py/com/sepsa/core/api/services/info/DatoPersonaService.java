/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.DatoPersona;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.TipoDatoPersona;
import py.com.sepsa.erp.ejb.entities.info.filters.DatoPersonaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/dato-persona")
@Log4j2
public class DatoPersonaService extends AbstractService<DatoPersona, DatoPersonaParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo para editar dato persona
     *
     * @param param
     * @return
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editDatoPersona(DatoPersonaParam param) {

        AbstractResponse resp = null;
        JsonObject respValue = new JsonObject();
        boolean noError = true;
        if (!param.isValidToEdit()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, null)
                    .build();
        }

        Integer idPersona = param.getIdPersona();
        Integer idTipoDatoPersona = param.getIdTipoDatoPersona();
        String valor = param.getValor();
        Integer firstResult = param.getFirstResult();
        Integer pageSize = param.getPageSize();

        log.info("Método: editDatoPersona");
        log.info("idPersona: " + idPersona);
        log.info("idTipoDatoPersona: " + idTipoDatoPersona);
        log.info("valor: " + valor);
        log.info("firstResult: " + firstResult);
        log.info("maxResult: " + pageSize);

        List<DatoPersona> lista = facades.getDatoPersonaFacade().find(param);

        log.info("lista de dato persona : " + lista);
        if (noError) {
            Persona persona = facades.getPersonaFacade().find(idPersona);
            if (persona == null) {
                noError = false;
            }
        }

        if (noError) {
            TipoDatoPersona tipoDatoPersona = facades.getTipoDatoPersonaFacade().find(idTipoDatoPersona);
            if (tipoDatoPersona == null) {
                noError = false;
            }
        }

        if (noError) {
            try {
                DatoPersona datoPersona = lista != null && !lista.isEmpty()
                        ? lista.get(0)
                        : new DatoPersona(idPersona, idTipoDatoPersona);

                if (datoPersona != null) {
                    datoPersona.setValor(valor);

                    if (lista == null || lista.isEmpty()) {
                        facades.getDatoPersonaFacade().create(datoPersona);
                        respValue.addProperty("idPersona", datoPersona.getDatoPersonaPK().getIdPersona());
                        respValue.addProperty("idTipoDatoPersona", datoPersona.getDatoPersonaPK().getIdTipoDatoPersona());
                        resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
                    } else {
                        facades.getDatoPersonaFacade().edit(datoPersona);
                        respValue.addProperty("idPersona", datoPersona.getDatoPersonaPK().getIdPersona());
                        respValue.addProperty("idTipoDatoPersona", datoPersona.getDatoPersonaPK().getIdTipoDatoPersona());
                        resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
                    }

                } else {
                    resp = new BrResponse(ResponseCode.DESCRIPTION_ERROR, null);
                }

            } catch (Exception e) {
                log.fatal("Error", e);
                resp = new KoResponse(ResponseCode.SERVICE_KO, null);
            }
        } else {
            resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
        }

        return resp.build();
    }

    @Override
    public Facade<DatoPersona, DatoPersonaParam, UserInfoImpl> getFacade() {
        return facades.getDatoPersonaFacade();
    }
}
