/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.TipoRepositorio;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Controlador para Tipo Repositorio
 *
 * @author Jonathan Bernal
 */
@Path("/tipo-repositorio")
public class TipoRepositorioService extends AbstractService<TipoRepositorio, CommonParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<TipoRepositorio, CommonParam, UserInfoImpl> getFacade() {
        return facades.getTipoRepositorioFacade();
    }
}
