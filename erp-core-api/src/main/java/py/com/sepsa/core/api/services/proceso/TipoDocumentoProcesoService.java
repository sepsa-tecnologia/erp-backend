/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.proceso;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.proceso.TipoDocumentoProceso;
import py.com.sepsa.erp.ejb.entities.proceso.filters.TipoDocumentoProcesoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 * Controlador para la instancia TipoDocumentoProceso
 * @author Jonathan D. Bernal Fernández
 */
@Path("/tipo-documento-proceso")
public class TipoDocumentoProcesoService extends AbstractService<TipoDocumentoProceso, TipoDocumentoProcesoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<TipoDocumentoProceso, TipoDocumentoProcesoParam, UserInfoImpl> getFacade() {
        return facades.getTipoDocumentoProcesoFacade();
    }

}
