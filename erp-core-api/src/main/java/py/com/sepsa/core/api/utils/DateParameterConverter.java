/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.ParamConverter;

/**
 *
 * @author Jonathan
 */
public class DateParameterConverter implements ParamConverter<Date> {

    public static final String format = "yyyy-MM-dd HH:mm:ssZ";

    @Override
    public Date fromString(String string) {
        
        SimpleDateFormat sdf;
        
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
            return sdf.parse(string);
        } catch (Exception e) {
        }
        
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(string);
        } catch (Exception e) {
        }
        
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(string);
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
    }

    @Override
    public String toString(Date t) {
        return new SimpleDateFormat(format).format(t);
    }

}