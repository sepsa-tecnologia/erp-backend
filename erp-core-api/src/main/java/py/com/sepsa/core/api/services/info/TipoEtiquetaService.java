/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.TipoEtiqueta;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoEtiquetaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/tipo-etiqueta")
@Log4j2
public class TipoEtiquetaService extends AbstractService<TipoEtiqueta, TipoEtiquetaParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que guarda un tipo etiqueta
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setTipoEtiqueta(TipoEtiquetaParam param) {
        
        AbstractResponse resp = null;
        JsonObject respValue = new JsonObject();
        
        try {

            param.log("Método: setTipoEtiqueta");
            
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();
            
            TipoEtiqueta tipoEtiqueta = facades
                    .getTipoEtiquetaFacade()
                    .create(param, customPrincipal.getUserInfo());
            
            if (tipoEtiqueta != null) {

                Map<String, String> atributos = new HashMap();
                atributos.put("id", tipoEtiqueta.getId() + " ");
                atributos.put("codigo", tipoEtiqueta.getCodigo() + " ");
                atributos.put("descripcion", tipoEtiqueta.getDescripcion()+ " ");

                facades.getRegistroFacade().create("info", "tipo_etiqueta", atributos, customPrincipal.getUserInfo());

                respValue.addProperty("id", tipoEtiqueta.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }

    @Override
    public Facade<TipoEtiqueta, TipoEtiquetaParam, UserInfoImpl> getFacade() {
        return facades.getTipoEtiquetaFacade();
    }
}
