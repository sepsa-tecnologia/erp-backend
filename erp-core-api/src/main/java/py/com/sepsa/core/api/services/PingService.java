/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services;

import java.io.Serializable;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.erp.ejb.facades.Facades;

/**
 * Controlador para PING
 * @author Jonathan D. Bernal Fernández
 */
@Path("/ping")
@Log4j2
public class PingService implements Serializable {
    
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Método que maneja las peticiones de ping
     * @return Response
     */
    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public Response ping() {
        
        try {
            log.info("Método: ping.");
            facades.getTipoEstadoFacade().count();
            return new OkResponse(ResponseCode.SERVICE_OK, null).build();
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
}
