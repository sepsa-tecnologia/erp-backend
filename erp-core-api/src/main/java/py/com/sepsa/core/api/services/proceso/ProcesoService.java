/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.proceso;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.proceso.Proceso;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan
 */
@Path("/proceso")
@Log4j2
public class ProcesoService extends AbstractService<Proceso, ProcesoParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response find(@BeanParam ProcesoParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            param.log("Método: findProceso");

            List<Proceso> lista = getFacade().find(param);
            Long totalSize = getFacade().findSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Proceso item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());
            
            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProceso(ProcesoParam param) {
        
        AbstractResponse resp;
        
        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            param.log("Método: crearProceso");

            Proceso proceso = facades.getProcesoFacade().create(param, cp.getUserInfo());

            if (proceso != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(proceso, Proceso.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProceso(ProcesoParam param) {
        
        AbstractResponse resp;
        
        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            param.log("Método: editarProceso");

            Proceso proceso = facades.getProcesoFacade().edit(param, cp.getUserInfo());

            if (proceso != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(proceso, Proceso.class);
                
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @Override
    public Facade<Proceso, ProcesoParam, UserInfoImpl> getFacade() {
        return facades.getProcesoFacade();
    }
}
