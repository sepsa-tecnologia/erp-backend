/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.usuario;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import py.com.sepsa.core.api.pojos.services.UsuarioParams;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioEmpresaParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioLocalParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioPerfilParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.EncargadoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.JWEUtils;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/usuario")
@Log4j2
public class UsuarioService extends AbstractService<Usuario, UsuarioParam> {

    /**
     * Fachada de los facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo login que retorna un token de sesion
     *
     * @param param Parámetros de login
     * @return
     */
    @PermitAll
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(UsuarioParam param) {

        AbstractResponse resp = null;
        JsonObject respValue = new JsonObject();

        if (!param.isValidToLogin()) {
            resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            return resp.build();
        }

        String usuario = param.getUsuario();
        String contrasena = param.getContrasena();

        log.info("Método: login");
        log.info("usuario: " + usuario);

        try {
            boolean noError = true;

            UsuarioPojo user = facades.getUsuarioFacade().findUsuarioPojo(usuario);

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (contrasena == null
                    || user == null
                    || !user.getCodigoEstado().equals("ACTIVO")
                    || !encoder.matches(contrasena, user.getContrasena())) {
                resp = new BrResponse(ResponseCode.USERNAME_PASSWORD_ERROR, null);
                noError = false;
            } else if (user.getCodigoEstado().equals("INACTIVO")) {
                resp = new BrResponse(ResponseCode.CREDENTIAL_ERROR, null);
                noError = false;
            } else if (user.getCodigoEstado().equals("BLOQUEADO")) {
                resp = new BrResponse(ResponseCode.PAYMENT_NEEDED, null);
                noError = false;
            } else {
                
                user = facades.getUsuarioFacade().findPojoCompleto(user);
                
                if (isNullOrEmpty(user.getUsuarioEmpresas())) {
                    resp = new BrResponse(ResponseCode.USERNAME_BUSINESS_ERROR, null);
                    noError = false;
                }
            }

            if (noError) {
                respValue.addProperty("token", JWEUtils.buildJWT(user, null));
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, respValue);
        }

        return resp == null ? null : resp.build();
    }

    @GET
    @Path("/info")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUserInfo() {

        CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

        UsuarioParam param = new UsuarioParam();
        param.setId(customPrincipal.getUserInfo().getId());

        if (!param.isValidToGetById()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            param.log("Método: findUserInfo");

            Usuario usuario = getFacade().findFirst(param);

            if (usuario != null && usuario.getUsuarioPerfiles() != null) {
                usuario.getUsuarioPerfiles().removeIf(e -> !e.getEstado().getCodigo().equals("ACTIVO"));
            }

            if (usuario != null && usuario.getUsuarioLocales() != null) {
                usuario.getUsuarioLocales().removeIf(e -> !e.getActivo().equals('S'));
            }

            if (usuario != null && usuario.getUsuarioEmpresas() != null) {
                usuario.getUsuarioEmpresas().removeIf(e -> !e.getActivo().equals('S'));
            }

            Gson gson = GsonUtils.generateDetault();
            JsonElement result = usuario == null ? null : gson.toJsonTree(usuario);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @GET
    @Path("/info/pojo")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUserInfoPojo() {

        CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

        UsuarioParam param = new UsuarioParam();
        param.setId(customPrincipal.getUserInfo().getId());

        if (!param.isValidToGetById()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            param.log("Método: findUserInfoPojo");

            UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(param);

            if (usuario != null) {
                UsuarioPerfilParam upparam = new UsuarioPerfilParam();
                upparam.setCodigoEstado("ACTIVO");
                upparam.setIdUsuario(usuario.getId());
                upparam.setPageSize(100);
                usuario.setUsuarioPerfiles(facades.getUsuarioPerfilFacade().findPojo(upparam));
            }

            if (usuario != null) {
                UsuarioLocalParam ulparam = new UsuarioLocalParam();
                ulparam.setActivo('S');
                ulparam.setIdUsuario(usuario.getId());
                ulparam.setPageSize(100);
                usuario.setUsuarioLocales(facades.getUsuarioLocalFacade().findPojo(ulparam));
            }

            if (usuario != null) {
                UsuarioEmpresaParam ueparam = new UsuarioEmpresaParam();
                ueparam.setActivo('S');
                ueparam.setIdUsuario(usuario.getId());
                ueparam.setPageSize(100);
                usuario.setUsuarioEmpresas(facades.getUsuarioEmpresaFacade().findPojo(ueparam));
            }

            Gson gson = GsonUtils.generateDetault();
            JsonElement result = usuario == null ? null : gson.toJsonTree(usuario);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @POST
    @Path("/token-empresa")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response generateBusinessToken(UsuarioParam param) {

        CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();

        param.setId(cp.getUserInfo().getId());

        if (!param.isValidToGenerateBusinessToken()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        AbstractResponse resp = null;

        try {

            boolean noError = true;

            UsuarioPojo user = facades.getUsuarioFacade().findFirstPojo(param);

            if (user == null) {
                resp = new BrResponse(ResponseCode.DATA_ERROR, null);
                noError = false;
            } else {
                user = facades.getUsuarioFacade().findPojoCompleto(user);

                user.getUsuarioEmpresas().removeIf((item)
                        -> !item.getActivo().equals('S')
                        || !(item.getIdEmpresa().equals(param.getIdEmpresa())
                        || item.getCodigoEmpresa().equals(param.getCodigoEmpresa())));
                
                if (isNullOrEmpty(user.getUsuarioEmpresas())) {
                    resp = new BrResponse(ResponseCode.USERNAME_BUSINESS_ERROR, null);
                    noError = false;
                }
            }

            if (noError) {
                JsonObject respValue = new JsonObject();
                respValue.addProperty("token", JWEUtils.buildJWT(user, cp.getUserInfo().getIdEmpresa()));
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp == null ? null : resp.build();
    }

    /**
     * Metodo dar de alta un usuario
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP_ROLE_ADMIN"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearUsuario(UsuarioParam param) {

        AbstractResponse resp;

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            
            if (param.getAdmin() == null) {
                switch (cp.getUserInfo().getCodigoTipoEmpresa()) {
                    case "CLIENTE":
                        param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                        break;
                }   
            }
            
            param.log("Método: crearUsuario");

            Usuario user = facades.getUsuarioFacade().create(param, cp.getUserInfo());

            if (user != null) {
                JsonObject respValue = new JsonObject();
                respValue.addProperty("id", user.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Edita un usuario
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarUsuario(UsuarioParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: editarUsuario");

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            Usuario user = facades.getUsuarioFacade().edit(param, customPrincipal.getUserInfo());

            if (user != null) {
                JsonObject respValue = new JsonObject();
                respValue.addProperty("id", user.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Edita una contraseña de un usuario con su pass antiguo
     *
     * @param parametros
     * @return
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/editar-password")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPassword(UsuarioParams parametros) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        AbstractResponse resp = null;
        JsonObject respValue = new JsonObject();
        boolean noError = true;
        Usuario usuarioAEditar = null;

        String usuario = parametros.getUsuario();
        String contrasena = parametros.getContrasena();
        String newContrasena = parametros.getNuevaContrasena();

        log.info("Método: editPassword");
        log.info("usuario: " + usuario);
        log.info("contrasena: " + contrasena);
        log.info("newContrasena: " + newContrasena);

        if (!parametros.editPassword()) {
            noError = false;
        }
        if (noError) {
            usuarioAEditar = facades.getUsuarioFacade().findUsuario(usuario);
        }

        if (usuarioAEditar == null) {
            noError = false;
        }

        if (noError && !encoder.matches(contrasena, usuarioAEditar.getContrasena())) {
            noError = false;
        }

        try {
            if (noError) {
                String contraseñaEncriptada = encoder.encode(newContrasena);
                usuarioAEditar.setContrasena(contraseñaEncriptada);
                facades.getUsuarioFacade().edit(usuarioAEditar);

                CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

                Map<String, String> pk = new HashMap();
                pk.put("id", usuarioAEditar.getId() + " ");

                Map<String, String> atributos = new HashMap();
                atributos.put("contrasena", usuarioAEditar.getContrasena() + " ");

                facades.getRegistroFacade().edit("usuario", "usuario", atributos, customPrincipal.getUserInfo(), pk);

                respValue.addProperty("id", usuarioAEditar.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                log.info("Los datos no parasaron la verificacion para cambiar contraseña");
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Edita una contraseña como super usuario
     *
     * @param parametros
     * @return
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/editar-password-super-usuario")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPass(UsuarioParams parametros) {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        AbstractResponse resp = null;
        JsonObject respValue = new JsonObject();
        boolean noError = true;
        Usuario usuarioAEditar = null;

        String usuario = parametros.getUsuario();
        String contrasena = parametros.getContrasena();

        log.info("Método: editPassword");
        log.info("usuario: " + usuario);
        log.info("contrasena: " + contrasena);

        if (!parametros.editPass()) {
            noError = false;
        }
        if (noError) {
            usuarioAEditar = facades.getUsuarioFacade().findUsuario(usuario);
        }

        if (usuarioAEditar == null) {
            noError = false;
        }

        try {
            if (noError) {
                String contraseñaEncriptada = encoder.encode(contrasena);
                usuarioAEditar.setContrasena(contraseñaEncriptada);
                facades.getUsuarioFacade().edit(usuarioAEditar);

                CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

                Map<String, String> pk = new HashMap();
                pk.put("id", usuarioAEditar.getId() + " ");

                Map<String, String> atributos = new HashMap();
                atributos.put("contrasena", usuarioAEditar.getContrasena() + " ");

                facades.getRegistroFacade().edit("usuario", "usuario", atributos, customPrincipal.getUserInfo(), pk);

                respValue.addProperty("id", usuarioAEditar.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                log.info("Los datos no parasaron la verificacion para cambiar contraseña");
                resp = new BrResponse(ResponseCode.PARAM_ERROR, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Método para enviar el correo de recuperación de contraseña
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/enviar-correo-recuperacion")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response enviarCorreoRecuperacion(UsuarioParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: enviarCorreoRecuperacion");

            Boolean success = facades
                    .getUsuarioFacade()
                    .enviarCorreoRecuperacion(param);

            if (success) {

                resp = new OkResponse(ResponseCode.SERVICE_OK, null);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Método para recuperación de contraseña
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/recuperar-contrasena")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response recuperarContrasena(UsuarioParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: recuperarContrasena");

            Boolean success = facades
                    .getUsuarioFacade()
                    .recuperarContrasena(param);

            if (success) {

                resp = new OkResponse(ResponseCode.SERVICE_OK, null);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Método para validar hash de recuperación de contraseña
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/validar-hash")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validarHash(UsuarioParam param) {

        AbstractResponse resp;

        try {

            param.log("Método: validarHash");

            Boolean success = facades
                    .getUsuarioFacade()
                    .validarHash(param);

            if (success) {

                resp = new OkResponse(ResponseCode.SERVICE_OK, null);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception e) {
            log.fatal("Error", e);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    /**
     * Metodo que obtiene la lista de encargados
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/encargados")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEncargados(@BeanParam CommonParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, null).build();
        }

        param.log("Método: findEncargados");

        try {
            List<EncargadoPojo> lista = facades
                    .getUsuarioFacade()
                    .findEncargados(param);

            Long totalSize = facades
                    .getUsuarioFacade()
                    .findEncargadosSize(param);

            JsonArray json = new JsonArray();

            Gson gson = new Gson();

            for (EncargadoPojo item : lista) {
                json.add(gson.toJsonTree(item, EncargadoPojo.class));
            }

            JsonObject result = new JsonObject();
            result.add("data", json);
            result.addProperty("totalSize", totalSize);
            result.addProperty("firstResult", param.getFirstResult());
            result.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @Override
    public Facade<Usuario, UsuarioParam, UserInfoImpl> getFacade() {
        return facades.getUsuarioFacade();
    }
}
