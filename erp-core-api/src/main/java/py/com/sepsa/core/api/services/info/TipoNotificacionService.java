/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.TipoNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoNotificacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Path("/tipo-notificacion")

public class TipoNotificacionService extends AbstractService<TipoNotificacion, TipoNotificacionParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<TipoNotificacion, TipoNotificacionParam, UserInfoImpl> getFacade() {
        return facades.getTipoNotificacionFacade();
    }
}
