/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.comercial;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.pojos.Result;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/cliente")
@Log4j2
public class ClienteService extends AbstractService<Cliente, ClienteParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @GET
    @Path("/nroDocumento/{nroDocumento}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByNroDocumento(@PathParam("nroDocumento") ClienteParam param) {

        param.setNroDocumentoEq(param.getBruto());
        
        if (!param.isValidToGetByNroDocumento()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findByNroDocumento");
            
            Cliente cliente = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = cliente == null ? null : gson.toJsonTree(cliente);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearCliente(ClienteParam param) {

        AbstractResponse resp = null;

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearCliente");
            
            Cliente clienteCreado = facades.getClienteFacade().create(param, cp.getUserInfo());
            
            if(clienteCreado == null) {
                return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
            }

            Gson gson = GsonUtils.generateDetault();
            JsonElement result = gson.toJsonTree(clienteCreado);
            
            resp = new OkResponse(ResponseCode.SERVICE_OK, result);
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCliente(ClienteParam param) {

        AbstractResponse resp;

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: editarCliente");
            
            Cliente clienteEditado = facades.getClienteFacade().edit(param, cp.getUserInfo());
            
            if(clienteEditado == null) {
                return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
            }

            Gson gson = GsonUtils.generateDetault();
            JsonElement result = gson.toJsonTree(clienteEditado);
            
            resp = new OkResponse(ResponseCode.SERVICE_OK, result);
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/crear-masivo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response crearFacturaMasiva(@MultipartForm ClienteParam param) {

        Response resp;

        try {
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearFacturaMasiva");
   
            List<String> lineas = Lists.empty();
            String mensaje = "";
            List<String> result = new ArrayList();
            try {
                result = facades
                        .getClienteFacade()
                        .altaMasivaClientes(param, cp.getUserInfo());
                
            } catch (Exception e) {
                mensaje = "Ocurrió un error al generar las facturas";
            }

            if (result != null) {
                lineas.add(mensaje);
                StringBuilder buffer = new StringBuilder();
                for (String linea : result) {
                    buffer.append(linea).append("\r\n");
                }

                //Para octet stream
                ByteArrayInputStream bais = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));

                Response.ResponseBuilder responseBuilder = Response.ok(bais);
                responseBuilder.type("text/csv");
                responseBuilder.header("Content-Disposition", "attachment; filename=\"respuesta.csv\"");
                return responseBuilder.build();
            } else {
                resp = Result.createInstance().ko().build();
            }
            
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = Result.createInstance().ko().build();

        }
        return resp;
    }

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/update-masivo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response updateClienteMasivo(@MultipartForm ClienteParam param) {

        Response resp;

        try {
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: updateClienteMasivo");
   
            List<String> lineas = Lists.empty();
            String mensaje = "";
            List<String> result = new ArrayList();
            try {
                result = facades
                        .getClienteFacade()
                        .updateClienteMasivo(param, cp.getUserInfo());
                
            } catch (Exception e) {
                mensaje = "Ocurrió un error al generar las facturas";
            }

            if (result != null) {
                lineas.add(mensaje);
                StringBuilder buffer = new StringBuilder();
                for (String linea : result) {
                    buffer.append(linea).append("\r\n");
                }

                //Para octet stream
                ByteArrayInputStream bais = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));

                Response.ResponseBuilder responseBuilder = Response.ok(bais);
                responseBuilder.type("text/csv");
                responseBuilder.header("Content-Disposition", "attachment; filename=\"respuesta.csv\"");
                return responseBuilder.build();
            } else {
                resp = Result.createInstance().ko().build();
            }
            
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = Result.createInstance().ko().build();

        }
        return resp;
    }

    

    @Override
    public Facade<Cliente, ClienteParam, UserInfoImpl> getFacade() {
        return facades.getClienteFacade();
    }
}
