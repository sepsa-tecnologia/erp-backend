/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.TipoReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 * Controlador de referencia geografica
 *
 * @author Jonathan
 */
@Path("/tipo-referencia-geografica")
public class TipoReferenciaGeograficaService extends AbstractService<TipoReferenciaGeografica, TipoReferenciaGeograficaParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<TipoReferenciaGeografica, TipoReferenciaGeograficaParam, UserInfoImpl> getFacade() {
        return facades.getTipoReferenciaGeograficaFacade();
    }

}
