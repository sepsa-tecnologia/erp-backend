/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application
 * @author Jonathan Bernal
 */
@ApplicationPath("/api")
public class App extends Application {
    
    public App() {}
    
}
