/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.Date;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.info.ProductoPrecio;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoPrecioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador de producto precio
 *
 * @author Jonathan Bernal
 */
@Path("/producto-precio")
@Log4j2
public class ProductoPrecioService extends AbstractService<ProductoPrecio, ProductoPrecioParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProductoPrecio(ProductoPrecioParam param) {
        
        AbstractResponse resp = null;
        
        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearProductoPrecio");
            
            ProductoPrecio productoPrecio = facades.getProductoPrecioFacade().create(param, cp.getUserInfo());
            
            if (productoPrecio != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(productoPrecio, ProductoPrecio.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProductoPrecio(ProductoPrecioParam param) {
        
        AbstractResponse resp = null;
        
        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: editarProductoPrecio");
            
            ProductoPrecio productoPrecio = facades.getProductoPrecioFacade().edit(param, cp.getUserInfo());
            
            if (productoPrecio != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(productoPrecio, ProductoPrecio.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }
    
    @GET
    @Path("/consultar/{fecha}/{idProducto}/{idMoneda}/{idCanalVenta}/{idCliente}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarProductoPrecio(@PathParam("fecha") Date fecha,
            @PathParam("idProducto") Integer idProducto,
            @PathParam("idMoneda") Integer idMoneda,
            @PathParam("idCanalVenta") Integer idCanalVenta,
            @PathParam("idCliente") Integer idCliente) {
        
        try {
            
            ProductoPrecioParam param = new ProductoPrecioParam();
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setIdProducto(idProducto);
            param.setIdMoneda(idMoneda);
            param.setIdCanalVenta(idCanalVenta);
            param.setIdCanalVentaNull(Boolean.FALSE);
            param.setIdCliente(idCliente);
            param.setIdClienteNull(Boolean.FALSE);
            param.setFechaInicioHasta(fecha);
            param.setFechaFinDesde(fecha);
            param.setFechaFinNull(Boolean.FALSE);
            param.log("Método: consultarProductoPrecio");

            ProductoPrecio item = getFacade().findFirst(param);

            if(item == null) {
                param.setFechaFinDesde(null);
                param.setFechaFinNull(Boolean.TRUE);
                item = getFacade().findFirst(param);
            }
            
            if(item == null) {
                param.setFechaFinDesde(fecha);
                param.setFechaFinNull(Boolean.FALSE);
                param.setIdCanalVenta(idCanalVenta);
                param.setIdCanalVentaNull(Boolean.FALSE);
                param.setIdCliente(null);
                param.setIdClienteNull(Boolean.TRUE);
                item = getFacade().findFirst(param);
            }
            
            if(item == null) {
                param.setFechaFinDesde(null);
                param.setFechaFinNull(Boolean.TRUE);
                item = getFacade().findFirst(param);
            }

            if(item == null) {
                param.setFechaFinDesde(fecha);
                param.setFechaFinNull(Boolean.FALSE);
                param.setIdCanalVenta(null);
                param.setIdCanalVentaNull(Boolean.TRUE);
                param.setIdCliente(idCliente);
                param.setIdClienteNull(Boolean.FALSE);
                item = getFacade().findFirst(param);
            }
            
            if(item == null) {
                param.setFechaFinDesde(null);
                param.setFechaFinNull(Boolean.TRUE);
                item = getFacade().findFirst(param);
            }

            if(item == null) {
                param.setFechaFinDesde(fecha);
                param.setFechaFinNull(Boolean.FALSE);
                param.setIdCanalVenta(null);
                param.setIdCanalVentaNull(Boolean.TRUE);
                param.setIdCliente(null);
                param.setIdClienteNull(Boolean.TRUE);
                item = getFacade().findFirst(param);
            }
            
            if(item == null) {
                param.setFechaFinDesde(null);
                param.setFechaFinNull(Boolean.TRUE);
                item = getFacade().findFirst(param);
            }
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = item == null ? null : gson.toJsonTree(item);
            
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @Override
    public Facade<ProductoPrecio, ProductoPrecioParam, UserInfoImpl> getFacade() {
        return facades.getProductoPrecioFacade();
    }
}
