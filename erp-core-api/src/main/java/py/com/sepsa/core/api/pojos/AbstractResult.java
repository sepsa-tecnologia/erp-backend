/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.core.api.pojos;

import javax.ws.rs.core.Response;


/**
 * Respuesta genérica del servicio
 * @author Daniel F. Escauriza Arza
 */
public abstract class AbstractResult {
    
    /**
     * Estatus de la respuesta
     */
    protected Response.Status status;
    
    /**
     * Obtiene una respuesta al servicio
     * @return Respuesta al servicio
     */
    public Response build() {
        Response.ResponseBuilder builder = Response
                .status(status)
                .entity(getJsonResponse());

        return builder.build();
    }

    public abstract String getJsonResponse();
    
    public abstract void generatePayload();
    
    public AbstractResult setStatus(Response.Status status) {
        this.status = status;
        return this;
    }

    public Response.Status getStatus() {
        return status;
    }
}