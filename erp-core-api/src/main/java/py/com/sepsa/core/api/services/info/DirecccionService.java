/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.entities.info.Direccion;
import py.com.sepsa.erp.ejb.entities.info.filters.DireccionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Servicio que maneja la entidad Direccion
 *
 * @author Jonathan Bernal
 */
@Path("/direccion")
@Log4j2
public class DirecccionService extends AbstractService<Direccion, DireccionParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo para crear Direccion
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearDireccion(DireccionParam param) {
        AbstractResponse resp;
        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Metodo: crearDireccion");
            
            Direccion direction = facades
                    .getDireccionFacade()
                    .create(param, cp.getUserInfo());
            
            if (direction != null) {
                JsonObject respValue = new JsonObject();
                respValue.addProperty("id", direction.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }
    
    /**
     * Metodo para crear Direccion
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarDireccion(DireccionParam param) {
        AbstractResponse resp = null;
        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Metodo: editarDireccion");
            
            Direccion direction = facades
                    .getDireccionFacade()
                    .edit(param, cp.getUserInfo());
            
            if (direction != null) {
                JsonObject respValue = new JsonObject();
                respValue.addProperty("id", direction.getId());
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }

        return resp.build();
    }

    @Override
    public Facade<Direccion, DireccionParam, UserInfoImpl> getFacade() {
        return facades.getDireccionFacade();
    }
}
