/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.filters.EmpresaGeneralParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EmpresaPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan
 */
@Path("/empresa")
@Log4j2
public class EmpresaService extends AbstractService<Empresa, EmpresaParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public Response find(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findEmpresa");

            List<Empresa> lista = getFacade().find(param);
            Long totalSize = getFacade().findSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Empresa item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @GET
    @Path("/cantidad-empresas")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findCantidadEmpresas(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findCantidadEmpresas");

            List<EmpresaPojo> lista = facades.getEmpresaFacade().findCantidadEmpresas(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (EmpresaPojo item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @GET
    @Path("/cantidad-usuarios")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findCantidadUsuarios(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findCantidadUsuarios");

            List<EmpresaPojo> lista = facades.getEmpresaFacade().findCantidadUsuarios(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (EmpresaPojo item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearEmpresa(EmpresaParam param) {
 
        AbstractResponse resp = null;

        try {

            param.log("Método: crearEmpresa");

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            Empresa empresa = facades.getEmpresaFacade().create(param, customPrincipal.getUserInfo());

            if (empresa != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(empresa, Empresa.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/general")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearEmpresaGeneral(EmpresaGeneralParam param) {

        AbstractResponse resp = null;

        try {

            param.log("Método: crearEmpresa");
           
                    
            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            Empresa empresa = facades.getEmpresaFacade().create(param.getEmpresa(), customPrincipal.getUserInfo());

            if (empresa != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(empresa, Empresa.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarEmpresa(EmpresaParam param) {

        AbstractResponse resp = null;

        try {

            param.log("Método: editarEmpresa");

            CustomPrincipal<UserInfoImpl> customPrincipal = (CustomPrincipal) sc.getUserPrincipal();

            Empresa empresa = facades.getEmpresaFacade().edit(param, customPrincipal.getUserInfo());

            if (empresa != null) {
                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(empresa, Empresa.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }
    
    
    @GET
    @Path("/facturas-pendientes")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEmpresasFacturasPendientes(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findEmpresasFacturasPendientes");

            List<Empresa> lista = facades.getEmpresaFacade().findEmpresasFacturasPendientes(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Empresa item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
        
    @GET
    @Path("/autofacturas-pendientes")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEmpresasAutofacturasPendientes(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findEmpresasAutofacturasPendientes");

            List<Empresa> lista = facades.getEmpresaFacade().findEmpresasAutofacturasPendientes(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Empresa item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
        
    @GET
    @Path("/nota-credito-pendientes")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEmpresasNotaCreditoPendientes(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findEmpresasNotaCreditoPendientes");

            List<Empresa> lista = facades.getEmpresaFacade().findEmpresasNotaCreditoPendientes(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Empresa item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
        
    @GET
    @Path("/nota-debito-pendientes")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEmpresasNotaDebitoPendientes(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findEmpresasNotaDebitoPendientes");

            List<Empresa> lista = facades.getEmpresaFacade().findEmpresasNotaDebitoPendientes(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Empresa item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
        
    @GET
    @Path("/nota-remision-pendientes")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEmpresasNotaRemisionPendientes(@BeanParam EmpresaParam param) {

        if (!param.isValidToList()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp == null ? null : cp.getUserInfo().getIdEmpresa());
            param.log("Método: findEmpresasNotaRemisionPendientes");

            List<Empresa> lista = facades.getEmpresaFacade().findEmpresasNotaRemisionPendientes(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (Empresa item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", lista.size());
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());

            return new OkResponse(ResponseCode.SERVICE_OK, respValue).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }

    @Override
    public Facade<Empresa, EmpresaParam, UserInfoImpl> getFacade() {
        return facades.getEmpresaFacade();
    }
}
