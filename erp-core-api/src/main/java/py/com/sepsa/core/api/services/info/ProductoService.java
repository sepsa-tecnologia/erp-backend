/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.core.api.services.info;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.core.api.pojos.AbstractResponse;
import py.com.sepsa.core.api.pojos.KoResponse;
import py.com.sepsa.core.api.pojos.OkResponse;
import py.com.sepsa.core.api.pojos.ResponseCode;
import py.com.sepsa.core.api.pojos.BrResponse;
import py.com.sepsa.core.api.services.AbstractService;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.filters.MarcaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan
 */
@Path("/producto")
@Log4j2
public class ProductoService extends AbstractService<Producto, ProductoParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @GET
    @Path("/codigoGtin/{codigoGtin}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByCodigoGtin(@PathParam("codigoGtin") ProductoParam param) {

        param.setCodigoGtinEq(param.getBruto());
        
        if (!param.isValidToGetByCodigoGtin()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findByCodigoGtin");
            
            Producto producto = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = producto == null ? null : gson.toJsonTree(producto);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @GET
    @Path("/codigoInterno/{codigoInterno}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByCodigoInterno(@PathParam("codigoInterno") ProductoParam param) {

        param.setCodigoInternoEq(param.getBruto());
        
        if (!param.isValidToGetByCodigoInterno()) {
            return new BrResponse(ResponseCode.PARAM_ERROR, param, null).build();
        }

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findByCodigoInterno");
            
            Producto producto = getFacade().findFirst(param);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = producto == null ? null : gson.toJsonTree(producto);
            return new OkResponse(ResponseCode.SERVICE_OK, result).build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return new KoResponse(ResponseCode.SERVICE_KO, null).build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearProducto(ProductoParam param) {
        
        AbstractResponse resp = null;
        
        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearProducto");
            
            Producto producto = facades.getProductoFacade().create(param, cp.getUserInfo());
            
            if (producto != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(producto, Producto.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/crear-masivo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response crearProductoMasivo(@MultipartForm ProductoParam param) {
        
        AbstractResponse resp;
        
        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            
            InputStreamReader input = new InputStreamReader(new ByteArrayInputStream(param.getUploadedFileBytes()));
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;

            while (true) {                    
                line = reader.readLine();
                if(line == null) {
                    break;
                }
                lineas.add(line);
            }

            for (int i = 1; i < lineas.size(); i++) {
                lineas.set(i, lineas.get(i) + ";");
            }
            
            for (int i = 1; i < lineas.size(); i++) {

                String mensaje = "";
                
                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    String[] stringArr = lineas.get(i).split(";");
                    
                    String codigoGtin = null;
                    String codigoInterno = null;
                    String descripcion = null;
                    String porcentajeImpuesto = null;
                    String codigoMarca = null;
                    String descripcionMarca = null;
                    
                    for (int k = 0; k < stringArr.length; k++) {
                        switch(k) {
                            case 0:
                                codigoGtin = (String)stringArr[k];
                                break;
                            case 1:
                                codigoInterno = (String)stringArr[k];
                                break;
                            case 2:
                                descripcion = (String)stringArr[k];
                                break;
                            case 3:
                                porcentajeImpuesto = (String)stringArr[k];
                                break;
                            case 4:
                                codigoMarca = (String)stringArr[k];
                                break;
                            case 5:
                                descripcionMarca = (String)stringArr[k];
                                break;
                        }
                    }
                        
                    try {

                        MarcaParam mparam = new MarcaParam();
                        mparam.setCodigo(codigoMarca);
                        mparam.setDescripcion(descripcionMarca);
                        ProductoParam pparam = new ProductoParam();
                        pparam.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                        pparam.setActivo('S');
                        pparam.setCodigoGtin(codigoGtin);
                        pparam.setCodigoInterno(codigoInterno);
                        pparam.setDescripcion(descripcion);
                        pparam.setMarca(mparam);
                        pparam.setPorcentajeImpuesto(Integer.valueOf(porcentajeImpuesto));

                        Producto producto = facades.getProductoFacade().create(pparam, cp.getUserInfo());

                        if (producto != null) {
                            mensaje = "Producto registrado";
                        } else {
                            if(pparam.getErrores() != null && !pparam.getErrores().isEmpty()) {
                                mensaje = pparam.getErrores().get(0).getDescripcion();
                            }
                        }
                    } catch (Exception e) {
                        mensaje = "Debe indicar un porcentaje de impuesto válido";
                    }
                }
                
                lineas.set(i, String.format("%s%s;", lineas.get(i), mensaje));
            }
            
            StringBuilder buffer = new StringBuilder();
            for (String linea : lineas) {
                buffer.append(linea).append(System.lineSeparator());
            }
            
            //Para octet stream
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));

            Response.ResponseBuilder responseBuilder = Response.ok(bais);
            responseBuilder.type("text/csv");
            responseBuilder.header("Content-Disposition", "attachment; filename=\"respuesta.csv\"");
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarProducto(ProductoParam param) {
        
        AbstractResponse resp = null;
        
        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: editarProducto");
            
            Producto producto = facades.getProductoFacade().edit(param, cp.getUserInfo());
            
            if (producto != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement respValue = gson.toJsonTree(producto, Producto.class);
                resp = new OkResponse(ResponseCode.SERVICE_OK, respValue);
            } else {
                resp = new BrResponse(ResponseCode.PARAM_ERROR, param, null);
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = new KoResponse(ResponseCode.SERVICE_KO, null);
        }
        return resp.build();
    }

    @Override
    public Facade<Producto, ProductoParam, UserInfoImpl> getFacade() {
        return facades.getProductoFacade();
    }
    
}
