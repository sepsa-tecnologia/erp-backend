/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.ResguardoDocumento;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ResguardoDocumentoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia ResguardoDocumento
 * @author Jonathan D. Bernal Fernández
 */
@Path("/resguardo-documento")
@Log4j2
public class ResguardoDocumentoService extends AbstractService<ResguardoDocumento, ResguardoDocumentoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearResguardoDocumento(ResguardoDocumentoParam param) {
        
        try {
            
            param.log("Método: crearResguardoDocumento");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            ResguardoDocumento resguardoDocumento = facades
                    .getResguardoDocumentoFacade()
                    .create(param, cp.getUserInfo());
            
            if(resguardoDocumento != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(resguardoDocumento, ResguardoDocumento.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar una factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarResguardoDocumento(ResguardoDocumentoParam param) {

        try {
            
            param.log("Método: editarResguardoDocumento");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            ResguardoDocumento resguardoDocumento = facades
                    .getResguardoDocumentoFacade()
                    .edit(param, cp.getUserInfo());
            
            if(resguardoDocumento != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(resguardoDocumento, ResguardoDocumento.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/url-descarga/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarUrlDescarga(@PathParam("id") Integer id) {

        try {
            
            ResguardoDocumentoParam param = new ResguardoDocumentoParam();
            param.setId(id);
            param.log("Método: consultarUrlDescarga");
            
            String result = facades.getResguardoDocumentoFacade().generateDownloadUrl(param);
            
            if (result != null) {
                JsonObject json = new JsonObject();
                json.addProperty("id", id);
                json.addProperty("urlDescarga", result);
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<ResguardoDocumento, ResguardoDocumentoParam, UserInfoImpl> getFacade() {
        return facades.getResguardoDocumentoFacade();
    }
}
