/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import java.io.Serializable;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.PeriodoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.HistoricoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.HistoricoLiquidacionPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;

/**
 * Controlador para los reportes
 * @author Jonathan D. Bernal Fernández
 */
@Path("/reporte")
@Log4j2
public class ReporteService implements Serializable {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que obtiene la lista de historico liquidacion por cadena
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/cadena")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findHistoricoLiquidacionCadena(
            @BeanParam HistoricoLiquidacionParam param) {
        
        if (!param.isValidToListPeriodo()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }
        
        param.log("Método: findHistoricoLiquidacionCadena");

        try {
            
            PeriodoLiquidacionParam param1 = new PeriodoLiquidacionParam();
            param1.setMes(param.getMes());
            param1.setAno(param.getAno());
            param1.isValidToList();
            List<PeriodoLiquidacion> list = facades
                    .getPeriodoLiquidacionFacade()
                    .find(param1);
            
            if(list == null || list.isEmpty()) {
                return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
            }
            
            param.setFechaDesde(list.get(0).getFechaInicio());
            param.setFechaHasta(list.get(0).getFechaFin());
            
            List<HistoricoLiquidacionPojo> lista = facades
                    .getHistoricoLiquidacionFacade()
                    .findCadena(param);

            Long totalSize = facades
                    .getHistoricoLiquidacionFacade()
                    .findCadenaSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
}
