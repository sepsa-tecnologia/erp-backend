/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Retencion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Retencion
 * @author Jonathan D. Bernal Fernández
 */
@Path("/retencion")
@Log4j2
public class RetencionService extends AbstractService<Retencion, RetencionParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una retención
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearRetencion(RetencionParam param) {
        
        param.log("Método: crearRetencion");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Retencion retencion = facades
                    .getRetencionFacade()
                    .create(param, cp.getUserInfo());
            
            if(retencion != null) {
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(retencion, Retencion.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularRetencion(@PathParam("id") Integer id, RetencionParam param) {

        try {
            
            param.setId(id);
            param.log("Método: anularRetencion");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Retencion retencion = facades.getRetencionFacade().anular(param, cp.getUserInfo());
            
            if(retencion != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(retencion, Retencion.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<Retencion, RetencionParam, UserInfoImpl> getFacade() {
        return facades.getRetencionFacade();
    }
}
