/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia OrdenCompra
 * @author Jonathan D. Bernal Fernández
 */
@Path("/orden-compra")
@Log4j2
public class OrdenCompraService extends AbstractService<OrdenCompra, OrdenCompraParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una ordencompra
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearOrdenCompra(OrdenCompraParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearOrdenCompra");
            
            OrdenCompra ordenCompra = facades
                    .getOrdenCompraFacade()
                    .create(param, cp.getUserInfo());
            
            if(ordenCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ordenCompra);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar una ordencompra
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarOrdenCompra(OrdenCompraParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: editarOrdenCompra");
            
            OrdenCompra ordenCompra = facades
                    .getOrdenCompraFacade()
                    .edit(param, cp.getUserInfo());
            
            if(ordenCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ordenCompra, OrdenCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar estado de una ordencompra
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Path("/estado")
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarEstadoOrdenCompra(OrdenCompraParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: editarEstadoOrdenCompra");
            
            OrdenCompra ordenCompra = facades
                    .getOrdenCompraFacade()
                    .changeState(param, cp.getUserInfo());
            
            if(ordenCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ordenCompra, OrdenCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para cancelar una ordencompra
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Path("/anular/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelarOrdenCompra(@PathParam("id") OrdenCompraParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setId(Units.execute(() -> Integer.valueOf(param.getBruto())));
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: cancelarOrdenCompra");
            
            OrdenCompra ordenCompra = facades
                    .getOrdenCompraFacade()
                    .cancel(param, cp.getUserInfo());
            
            if(ordenCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ordenCompra, OrdenCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para habilitar una ordencompra
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Path("/habilitar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response habilitarOrdenCompra(@PathParam("id") OrdenCompraParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setId(Units.execute(() -> Integer.valueOf(param.getBruto())));
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: habilitarOrdenCompra");
            
            OrdenCompra ordenCompra = facades
                    .getOrdenCompraFacade()
                    .enable(param, cp.getUserInfo());
            
            if(ordenCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ordenCompra, OrdenCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @Path("/{id}/procesamiento-archivo/{idProcesamientoArchivo}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarProcesamientoArchivo(
            @PathParam("id") Integer id,
            @PathParam("idProcesamientoArchivo") Integer idProcesamientoArchivo) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            OrdenCompraParam param = new OrdenCompraParam();
            param.setId(id);
            param.setIdProcesamientoArchivo(idProcesamientoArchivo);
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: actualizarProcesamientoArchivo");
            
            OrdenCompra ordenCompra = facades.getOrdenCompraFacade().updateFileProcess(param, cp.getUserInfo());
            
            if(ordenCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ordenCompra, OrdenCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfOrdenCompra(@PathParam("id") Integer id, @BeanParam OrdenCompraParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarPdfOrdenCompra");

            byte[] bytes = facades.getOrdenCompraFacade().getPdfOrdenCompra(param, cp.getUserInfo());
            
            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/pdf");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.pdf\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<OrdenCompra, OrdenCompraParam, UserInfoImpl> getFacade() {
        return facades.getOrdenCompraFacade();
    }
    
}
