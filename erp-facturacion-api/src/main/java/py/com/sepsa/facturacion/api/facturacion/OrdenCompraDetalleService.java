/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraDetallePojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia OrdenCompraDetalle
 * @author Jonathan D. Bernal Fernández
 */
@Path("/orden-compra-detalle")
@Log4j2
public class OrdenCompraDetalleService extends AbstractService<OrdenCompraDetalle, OrdenCompraDetalleParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @GET
    @Path("/idOrdenCompra/{idOrdenCompra}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByIdOrdenCompra(@PathParam("idOrdenCompra") OrdenCompraDetalleParam param) {

        param.setIdOrdenCompra(Units.execute(() -> Integer.valueOf(param.getBruto())));
        
        if (!param.isValidToGetByOrdenCompra()) {
            return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
        }

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findByIdOrdenCompra");
            
            List<OrdenCompraDetallePojo> list = facades
                    .getOrdenCompraDetalleFacade()
                    .getByOrdenCompra(param);
            
            return ResultList.createInstance()
                    .storeData(param, list, list.size())
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<OrdenCompraDetalle, OrdenCompraDetalleParam, UserInfoImpl> getFacade() {
        return facades.getOrdenCompraDetalleFacade();
    }
}
