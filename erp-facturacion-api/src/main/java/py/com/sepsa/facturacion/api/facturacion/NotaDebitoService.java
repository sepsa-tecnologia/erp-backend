/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.NotaDebitoUtils;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.misc.Dates;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Controlador para la instancia Nota de Débito
 * @author Williams Vera
 */
@Path("/nota-debito")
@Log4j2
public class NotaDebitoService extends AbstractService<NotaDebito, NotaDebitoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una nota de débito
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearNd(NotaDebitoParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setIdUsuario(cp.getUserInfo().getId());
            param.log("Método: crearNd");
            
            NotaDebito nd = facades
                    .getNotaDebitoFacade()
                    .create(param, cp.getUserInfo());
            
            if(nd != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(nd);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/crear-masivo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response crearNotaDebitoMasivo(@MultipartForm NotaDebitoParam param) {
        NotaDebitoUtils n = new NotaDebitoUtils();
        Response resp = null;
        StringBuilder buffer = new StringBuilder();
        try {
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearNotaDebitoMasivo");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            List<NotaDebitoParam> notaDebitoList = n.convertToList(facades, param);
            String inicio = String.format("TIMBRADO;NRO_NOTA_DEBITO;FECHA;ID_NOTA_DEBITO;ESTADO_RESULTADO");
            buffer.append(inicio).append("\r\n");
            for (NotaDebitoParam nc : notaDebitoList) {
                if (nc.tieneErrores()) {
                    String error = String.format("%s;%s;-;-;ERROR;", nc.getTimbrado(), nc.getNroNotaDebito());
                    error = error.concat("%s| ");
                    for (MensajePojo mp : nc.getErrores()) {
                        error = String.format(error, mp.getDescripcion());
                        error = error.concat("%s| ");
                    }
                    error = error.substring(0, error.length() - 5);
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                } else {
                    NotaDebito notaDebito = facades
                            .getNotaDebitoFacade()
                            .create(nc, cp.getUserInfo());

                    if (notaDebito == null) {
                        String error = String.format("%s;%s;-;-;ERROR;", nc.getTimbrado(), nc.getNroNotaDebito());
                        error = error.concat("| ");
                        for (MensajePojo mp : nc.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {

                        String ok = String.format("%s;%s;%s;%s;OK", nc.getTimbrado(), nc.getNroNotaDebito(), sdf.format(nc.getFecha()), notaDebito.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }
                }
            }
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));
            Response.ResponseBuilder responseBuilder = Response.ok(bais);
            responseBuilder.type("text/csv");
            responseBuilder.header("Content-Disposition", "attachment; filename=\"respuesta.csv\"");
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = Result.createInstance().ko().build();
        }
        return resp;
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfNotaDebito(@PathParam("id") Integer id, @BeanParam NotaDebitoParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarPdfNotaDebito");
            
            byte[] bytes = facades.getNotaDebitoFacade().getPdfNotaDebito(param);
            
            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/pdf");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.pdf\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta-xml/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarXmlNotaDebito(@PathParam("id") Integer id, @BeanParam NotaDebitoParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarXmlNotaDebito");

            byte[] bytes = facades.getNotaDebitoFacade().getXmlNotaDebito(param);

            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/xml");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.xml\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar una nd
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarNd(NotaDebitoParam param) {

        try {
            
            param.log("Método: editarNd");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            NotaDebito nd = facades
                    .getNotaDebitoFacade()
                    .edit(param, cp.getUserInfo());
            
            if(nd != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(nd);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/comprobante-venta/{fecha}/{anual}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarComprobanteVenta(@BeanParam ReporteComprobanteParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: consultarComprobanteVenta");
            
            if(!param.isValidToList()) {
                return Result.createInstance().br()
                            .setResponseCode(ResponseCode.PARAM_ERROR)
                            .setParametros(param)
                            .build();
            }
            
            Integer size = facades
                    .getNotaDebitoFacade()
                    .generarComprobanteVentaSize(param);
            
            List<RegistroComprobantePojo> resultList = new ArrayList<>();
            
            for (int i = 0, j = 0; i < size; i = i + 5000, j++) {
                
                param.setPageSize(5000);
                param.setFirstResult(i);
                
                List<RegistroComprobantePojo> list = facades
                        .getNotaDebitoFacade()
                        .generarComprobanteVenta(param);

                RegistroComprobantePojo result = facades
                        .getReportUtils()
                        .generarReporteVenta(j, list, param);
                
                resultList.add(result);
            }
                
            RegistroComprobantePojo result = facades.getReportUtils()
                    .generarReporteVenta(resultList);
            
            //Para octet stream
            ByteArrayInputStream input = new ByteArrayInputStream(result.getBytes());

            Response.ResponseBuilder responseBuilder = Response.ok(input);
            responseBuilder.type(result.getTipoContenido());
            responseBuilder.header("Content-Disposition",
                    String.format("attachment; filename=\"%s\"", result.getNombreArchivo()));
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para consultar los datos para crear una nota de débito
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/datos-crear")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerDatos(@BeanParam TalonarioParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }
            
            Date date = Calendar.getInstance().getTime();
            param.setFecha(date);
            param.setIdTipoDocumento(6);
            TalonarioPojo talonario = facades.getTalonarioFacade().getTalonario(param);
            
            if(talonario == null) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.TALONARIO_ERROR)
                        .build();
            }
            
            NotaDebitoPojo item = facades.getNotaDebitoFacade().obtenerDatosCrear(talonario, param.getIdCliente());
            
            Gson gson = GsonUtils.generateDetault();
            
            JsonElement result = gson.toJsonTree(item, NotaDebitoPojo.class);
            
            return Result.createInstance()
                    .ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para crear una nota de débito a partir de una factura
     *
     * @param ndparam parámetros de n
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/crear-desde-factura")
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearDesdeFactura(NotaDebitoParam ndparam) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            ndparam.setIdEmpresa(cp.getUserInfo().getIdEmpresa());

            NotaDebito nc = facades
                    .getNotaDebitoFacade()
                    .createFromInvoice(ndparam, cp.getUserInfo());
            
            if(nc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(nc);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(ndparam)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para consultar la lista de talonarios para factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/talonario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response talonarioNd(@BeanParam TalonarioParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFecha(Calendar.getInstance().getTime());
            param.setIdTipoDocumento(6);
            param.log("Método: talonarioNd");
            
            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

            List<TalonarioPojo> lista = facades.getTalonarioFacade().getTalonarios(param);
            Integer totalSize = lista.size();
            
            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularNd(@PathParam("id") Integer id, NotaDebitoParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: anularNd");
            
            NotaDebito notaDebito = facades.getNotaDebitoFacade().anular(param, cp.getUserInfo());
            
            if(notaDebito != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(notaDebito, NotaDebito.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/reporte-venta/{fechaDesde}/{fechaHasta}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarReporteVenta(
            @PathParam("fechaDesde") Date fechaDesde,
            @PathParam("fechaHasta") Date fechaHasta,
            @BeanParam NotaDebitoParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFechaDesde(fechaDesde);
            param.setFechaHasta(fechaHasta);
            param.log("Método: consultarReporteVenta");
            
            byte[] bytes = facades.getNotaDebitoFacade().getXlsReporteVenta(param, cp.getUserInfo());
            
            if (bytes != null) {
                
                String fecha = Dates.formatToString(Calendar.getInstance().getTime(), Dates.DateFormat.DATE_XML);
                
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);
                
                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/ms-excel");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"reporte-venta-%s.xlsx\"", fecha));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/reporte-venta")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarReporteVentaCliente(
            @QueryParam("idCliente") Integer idCliente,
            @QueryParam("fechaDesde") Date fechaDesde,
            @QueryParam("fechaHasta") Date fechaHasta,
            @BeanParam NotaDebitoParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setIdCliente(idCliente);
            param.setFechaDesde(fechaDesde);
            param.setFechaHasta(fechaHasta);
            param.log("Método: consultarReporteVenta");
            
            byte[] bytes = facades.getNotaDebitoFacade().getXlsReporteVenta(param, cp.getUserInfo());
            
            if (bytes != null) {
                
                String fecha = Dates.formatToString(Calendar.getInstance().getTime(), Dates.DateFormat.DATE_XML);
                
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);
                
                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/ms-excel");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"reporte-venta-%s.xlsx\"", fecha));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<NotaDebito, NotaDebitoParam, UserInfoImpl> getFacade() {
        return facades.getNotaDebitoFacade();
    }
}
