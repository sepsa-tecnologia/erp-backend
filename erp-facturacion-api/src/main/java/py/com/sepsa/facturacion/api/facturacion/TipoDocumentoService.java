/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoDocumento;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Controlador para la instancia TipoDocumento
 * @author Jonathan D. Bernal Fernández
 */
@Path("/tipo-documento")
public class TipoDocumentoService extends AbstractService<TipoDocumento, CommonParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<TipoDocumento, CommonParam, UserInfoImpl> getFacade() {
        return facades.getTipoDocumentoFacade();
    }

}
