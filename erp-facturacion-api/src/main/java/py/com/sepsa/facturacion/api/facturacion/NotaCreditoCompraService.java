/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Nota de Crédito compra
 * @author Jonathan D. Bernal Fernández
 */
@Path("/nota-credito-compra")
@Log4j2
public class NotaCreditoCompraService extends AbstractService<NotaCreditoCompra, NotaCreditoCompraParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearNcc(NotaCreditoCompraParam param) {
        
        param.log("Método: crearNcc");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            NotaCreditoCompra ncc = facades
                    .getNotaCreditoCompraFacade()
                    .create(param, cp.getUserInfo());
            
            if(ncc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(ncc);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularNcc(@PathParam("id") Integer id, NotaCreditoCompraParam param) {

        try {
            
            param.setId(id);
            param.log("Método: anularNcc");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            NotaCreditoCompra notaCredito = facades.getNotaCreditoCompraFacade().anular(param, cp.getUserInfo());
            
            if(notaCredito != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(notaCredito, NotaCreditoCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarNcc(@PathParam("id") Integer id) {

        try {
            NotaCreditoCompraParam param = new NotaCreditoCompraParam();
            param.setId(id);
            param.log("Método: eliminarNcc");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Boolean ok = facades.getNotaCreditoCompraFacade().delete(param, cp.getUserInfo());
            
            if(ok) {
                return Result.createInstance().ok().build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/comprobante-compra/{fecha}/{anual}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarComprobanteCompra(@BeanParam ReporteComprobanteParam param) {

        try {
            
            param.log("Método: consultarComprobanteCompra");
            
            if(!param.isValidToList()) {
                return Result.createInstance().br()
                            .setResponseCode(ResponseCode.PARAM_ERROR)
                            .setParametros(param)
                            .build();
            }
            
            Integer size = facades
                    .getNotaCreditoCompraFacade()
                    .generarComprobanteCompraSize(param);
            
            List<RegistroComprobantePojo> resultList = new ArrayList<>();
            
            for (int i = 0, j = 0; i < size; i = i + 5000, j++) {
                
                param.setPageSize(5000);
                param.setFirstResult(i);
                
                List<RegistroComprobantePojo> list = facades
                        .getNotaCreditoCompraFacade()
                        .generarComprobanteCompra(param);

                RegistroComprobantePojo result = facades
                        .getReportUtils()
                        .generarReporteCompra(j, list, param);
                
                resultList.add(result);
            }
                
            RegistroComprobantePojo result = facades.getReportUtils()
                    .generarReporteCompra(resultList);
                
            //Para octet stream
            ByteArrayInputStream input = new ByteArrayInputStream(result.getBytes());

            Response.ResponseBuilder responseBuilder = Response.ok(input);
            responseBuilder.type(result.getTipoContenido());
            responseBuilder.header("Content-Disposition",
                    String.format("attachment; filename=\"%s\"", result.getNombreArchivo()));
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<NotaCreditoCompra, NotaCreditoCompraParam, UserInfoImpl> getFacade() {
        return facades.getNotaCreditoCompraFacade();
    }
}
