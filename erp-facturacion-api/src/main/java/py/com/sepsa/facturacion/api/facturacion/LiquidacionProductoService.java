/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionProducto;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionProductoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Liquidacion producto
 * @author Jonathan D. Bernal Fernández
 */
@Path("/liquidacion-producto")
@Log4j2 
public class LiquidacionProductoService extends AbstractService<LiquidacionProducto, LiquidacionProductoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una liquidación
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarOCrearLiquidacionProducto(LiquidacionProductoParam param) {
        
        param.log("Método: editarOCrearLiquidacionProducto");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            LiquidacionProducto item = facades
                    .getLiquidacionProductoFacade()
                    .editarOCrear(param, cp.getUserInfo());
            
            if(item != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<LiquidacionProducto, LiquidacionProductoParam, UserInfoImpl> getFacade() {
        return facades.getLiquidacionProductoFacade();
    }
}
