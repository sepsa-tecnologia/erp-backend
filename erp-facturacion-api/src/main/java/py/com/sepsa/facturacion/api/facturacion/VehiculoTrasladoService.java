/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Transportista;
import py.com.sepsa.erp.ejb.entities.facturacion.VehiculoTraslado;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TransportistaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.VehiculoTrasladoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Williams Vera
 */
@Path("/vehiculo-traslado")
@Log4j2
public class VehiculoTrasladoService  extends AbstractService<VehiculoTraslado, VehiculoTrasladoParam>{

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
     /**
     * Metodo para crear un VehiculoTraslado
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearVehiculoTraslado(VehiculoTrasladoParam param) {

        try {
            
            param.log("Método: crearVehiculoTraslado");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            VehiculoTraslado item = facades
                    .getVehiculoTrasladoFacade()
                    .create(param, cp.getUserInfo());
            
            if(item != null) {

                JsonObject element = new JsonObject();
                element.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                        .setPayload(element)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Edita una cuenta de entidad financiera
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarVehiculoTraslado(VehiculoTrasladoParam param) {

        try {
            
            param.log("Método: editarVehiculoTraslado");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            VehiculoTraslado item = facades
                    .getVehiculoTrasladoFacade()
                    .edit(param, cp.getUserInfo());
            
            if(item != null) {

                JsonObject element = new JsonObject();
                element.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                        .setPayload(element)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<VehiculoTraslado, VehiculoTrasladoParam, UserInfoImpl> getFacade() {
        return facades.getVehiculoTrasladoFacade();
    }
    
}
