/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionSnc;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.MotivoEmisionSncParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
@Path("/motivo-emision-snc")
@Log4j2
public class MotivoEmisionSncService extends AbstractService<MotivoEmisionSnc, MotivoEmisionSncParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearMotivoEmisionSnc(MotivoEmisionSncParam param) {
        
        try {
            
            param.log("Método: crearMotivoEmisionSnc");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            MotivoEmisionSnc motivoEmisionSnc = facades
                    .getMotivoEmisionSncFacade()
                    .create(param, cp.getUserInfo());
            
            if(motivoEmisionSnc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(motivoEmisionSnc, MotivoEmisionSnc.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<MotivoEmisionSnc, MotivoEmisionSncParam, UserInfoImpl> getFacade() {
        return facades.getMotivoEmisionSncFacade();
    }
}
