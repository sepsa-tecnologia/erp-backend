/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.Serializable;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan Bernal
 * @param <T> Parámetro de Tipo
 * @param <U> Parámetro de Tipo
 */
@Log4j2
public abstract class AbstractService<T, U extends CommonParam> implements Serializable{
    
    public abstract Facade<T, U, UserInfoImpl> getFacade();
    
    @Context
    protected SecurityContext sc;
    
    /**
     * Obtiene el identificador de empresa
     * @return Identificador de empresa
     */
    protected Integer getIdEmpresa() {
        CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
        return cp.getUserInfo().getIdEmpresa();
    }
    
    /**
     * Obtiene la lista de tipo telefono
     *
     * @param param
     * @return
     */
    @GET
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@BeanParam U param) {

        if (!param.isValidToList()) {
            return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
        }

        try {
            
            param.log("Método: find");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            List<T> lista = param.getListadoPojo()
                    ? getFacade().findPojo(param)
                    : getFacade().find(param);
            Long totalSize = getFacade().findSize(param);
            
            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @GET
    @Path("/id/{id}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@PathParam("id") U pparam, @BeanParam U qparam) {

        qparam.setId(Units.execute(() -> Integer.valueOf(pparam.getBruto())));
        
        if (!qparam.isValidToGetById()) {
            return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
        }

        try {
            
            qparam.log("Método: findById");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    qparam.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            T t = qparam.getListadoPojo()
                    ? getFacade().findFirstPojo(qparam)
                    : getFacade().findFirst(qparam);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = t == null ? null : gson.toJsonTree(t);
            return Result.createInstance().ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @GET
    @Path("/codigo/{codigo}")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByCode(@PathParam("codigo") U pparam, @BeanParam U qparam) {

        qparam.setCodigo(pparam.getBruto());
        
        if (!qparam.isValidToGetByCode()) {
            return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
        }

        try {
            
            qparam.log("Método: findByCode");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    qparam.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            T t = qparam.getListadoPojo()
                    ? getFacade().findFirstPojo(qparam)
                    : getFacade().findFirst(qparam);
            
            Gson gson = GsonUtils.generateDetault();
            JsonElement result = t == null ? null : gson.toJsonTree(t);
            return Result.createInstance().ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
}
