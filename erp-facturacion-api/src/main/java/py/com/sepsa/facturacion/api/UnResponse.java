package py.com.sepsa.facturacion.api;

import com.google.gson.JsonElement;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Respuesta BAD REQUEST al servicio
 * @author Jonathan D. Bernal Fernandez
 */
public class UnResponse extends AbstractResponse {
    
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final Response.Status STATUS = Response.Status.UNAUTHORIZED;
    private static final String MEDIA_TYPE = MediaType.APPLICATION_JSON;
    private static final boolean SUCCESS = false;
    //</editor-fold>
    
    /**
     * Constructor de BrResponse
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public UnResponse(ResponseCode responseCode, JsonElement payload) {
        super(STATUS, MEDIA_TYPE, SUCCESS, responseCode, payload);
    }
    
}
