/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.PeriodoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.HistoricoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.HistoricoLiquidacionPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Historico Liquidacion
 * @author Jonathan D. Bernal Fernández
 */
@Path("/historico-liquidacion")
@Log4j2
public class HistoricoLiquidacionService extends AbstractService<HistoricoLiquidacion, HistoricoLiquidacionParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para consultar el historico de liquidación
     *
     * @param idHistoricoLiquidacion Identificador de historico de liquidación
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findHistoricoLiquidacion(
            @QueryParam(value = "idHistoricoLiquidacion") Integer idHistoricoLiquidacion) {
        
        if (idHistoricoLiquidacion == null) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }

        try {
            
            HistoricoLiquidacionParam param = new HistoricoLiquidacionParam();
            param.setIdHistoricoLiquidacion(idHistoricoLiquidacion);
            param.isValidToList();
            List<HistoricoLiquidacion> lista = facades
                    .getHistoricoLiquidacionFacade()
                    .find(param);

            if(lista == null || lista.isEmpty()) {
                return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
            }
            
            LiquidacionDetalleParam param1 = new LiquidacionDetalleParam();
            param1.setIdHistoricoLiquidacion(idHistoricoLiquidacion);
            param1.setFirstResult(0);
            param1.setPageSize(100);
            List<LiquidacionDetalle> detalles = facades
                    .getLiquidacionDetalleFacade()
                    .find(param1);
            
            HistoricoLiquidacion item = lista.get(0);
            //item.setDetalles(detalles);
            
            Gson gson = GsonUtils.generateDetault();
            
            JsonElement result = gson.toJsonTree(item, HistoricoLiquidacion.class);
            
            return Result.createInstance()
                    .ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo que obtiene la lista de historico liquidacion periodo
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/periodo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findHistoricoLiquidacionPeriodo(
            @BeanParam HistoricoLiquidacionParam param) {
        
        if (!param.isValidToListPeriodo()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }
        
        param.log("Método: findHistoricoLiquidacionPeriodo");

        try {
            
            PeriodoLiquidacionParam param1 = new PeriodoLiquidacionParam();
            param1.setMes(param.getMes());
            param1.setAno(param.getAno());
            param1.isValidToList();
            List<PeriodoLiquidacion> list = facades
                    .getPeriodoLiquidacionFacade()
                    .find(param1);
            
            if(list == null || list.isEmpty()) {
                return ResultList.createInstance()
                        .storeData(param, new ArrayList(), 0)
                        .build();
            }
            
            param.setIdPeriodoLiquidacion(list.get(0).getId());
            
            List<HistoricoLiquidacionPojo> lista = facades
                    .getHistoricoLiquidacionFacade()
                    .findPeriodo(param);

            Long totalSize = facades
                    .getHistoricoLiquidacionFacade()
                    .findPeriodoSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo que obtiene la lista de historico liquidacion por cadena
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/cadena")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findHistoricoLiquidacionCadena(
            @BeanParam HistoricoLiquidacionParam param) {
        
        if (!param.isValidToListPeriodo()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }
        
        param.log("Método: findHistoricoLiquidacionCadena");

        try {
            
            PeriodoLiquidacionParam param1 = new PeriodoLiquidacionParam();
            param1.setMes(param.getMes());
            param1.setAno(param.getAno());
            param1.isValidToList();
            List<PeriodoLiquidacion> list = facades
                    .getPeriodoLiquidacionFacade()
                    .find(param1);
            
            if(list == null || list.isEmpty()) {
                return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
            }
            
            param.setFechaDesde(list.get(0).getFechaInicio());
            param.setFechaHasta(list.get(0).getFechaFin());
            
            List<HistoricoLiquidacionPojo> lista = facades
                    .getHistoricoLiquidacionFacade()
                    .findCadena(param);

            Long totalSize = facades
                    .getHistoricoLiquidacionFacade()
                    .findCadenaSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para crear un historico de liquidación
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearHistoricoLiquidacion(HistoricoLiquidacionParam param) {
        
        param.log("Método: crearHistoricoLiquidacion");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            HistoricoLiquidacion item = facades
                    .getHistoricoLiquidacionFacade()
                    .create(param, cp.getUserInfo());
            
            if(item != null) {
                
                Liquidacion l = facades
                        .getLiquidacionFacade()
                        .find(param.getIdLiquidacion());
                l.setIdHistoricoLiquidacion(item.getId());
                facades.getLiquidacionFacade().edit(l);
                
                JsonObject json = new JsonObject();
                json.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<HistoricoLiquidacion, HistoricoLiquidacionParam, UserInfoImpl> getFacade() {
        return facades.getHistoricoLiquidacionFacade();
    }
}
