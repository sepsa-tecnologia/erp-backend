/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.PeriodoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleCadenaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.LiquidacionDetalleCadenaPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Liquidacion Detalle
 * @author Jonathan D. Bernal Fernández
 */
@Path("/liquidacion-detalle")
@Log4j2
public class LiquidacionDetalleService extends AbstractService<LiquidacionDetalle, LiquidacionDetalleParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que obtiene la lista de liquidacion detalle
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/cadena")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findLiquidacionDetalleCadena(@BeanParam LiquidacionDetalleCadenaParam param) {
        
        if (!param.isValidToListPeriodo()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .setParametros(param)
                    .build();
        }
        
        param.log("Método: findLiquidacionDetalleCadena");

        try {
                
            PeriodoLiquidacionParam param1 = new PeriodoLiquidacionParam();
            param1.setMes(param.getMes());
            param1.setAno(param.getAno());
            param1.isValidToList();
            List<PeriodoLiquidacion> list = facades
                    .getPeriodoLiquidacionFacade()
                    .find(param1);

            if(list != null && !list.isEmpty()) {
                param.setFechaDesde(list.get(0).getFechaInicio());
                param.setFechaHasta(list.get(0).getFechaFin());
            }
            
            List<LiquidacionDetalleCadenaPojo> lista = facades
                    .getLiquidacionDetalleFacade()
                    .findCadena(param);

            Long totalSize = facades
                    .getLiquidacionDetalleFacade()
                    .findCadenaSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para crear un detalle de liquidación
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearOEditarDetalleLiquidacion(LiquidacionDetalleParam param) {
        
        param.log("Método: crearOEditarDetalleLiquidacion");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            LiquidacionDetalle item = facades
                    .getLiquidacionDetalleFacade()
                    .editarOCrear(param, cp.getUserInfo());
            
            if(item != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<LiquidacionDetalle, LiquidacionDetalleParam, UserInfoImpl> getFacade() {
        return facades.getLiquidacionDetalleFacade();
    }
}
