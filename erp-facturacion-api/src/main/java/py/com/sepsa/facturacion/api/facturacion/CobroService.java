/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Cobro;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Cobro
 * @author Jonathan D. Bernal Fernández
 */
@Path("/cobro")
@Log4j2
public class CobroService extends AbstractService<Cobro, CobroParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearCobro(CobroParam param) {
        
        param.log("Método: crearCobro");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Cobro cobro = facades
                    .getCobroFacade()
                    .create(param, cp.getUserInfo());
            
            if(cobro != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(cobro, Cobro.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/datos-crear")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerDatos(@BeanParam TalonarioParam param) {

        try {
            
            param.setIdEmpresa(getIdEmpresa());
            
            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }
            
            Date date = Calendar.getInstance().getTime();
            param.setIdTipoDocumento(2);
            TalonarioPojo talonario = facades.getTalonarioFacade().getTalonario(param);
            
            if(talonario == null) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.TALONARIO_ERROR)
                        .build();
            }
            
            String nroRecibo = facades.getTalonarioFacade().sigNumRecibo(talonario);
            
            Cobro item = new Cobro();
            item.setIdTalonario(talonario.getId());
            item.setFecha(date);
            item.setNroRecibo(nroRecibo);
            
            Gson gson = GsonUtils.generateDetault();
            
            JsonElement result = gson.toJsonTree(item, Cobro.class);
            
            return Result.createInstance()
                    .ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar un cobro
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCobro(CobroParam param) {
        
        param.log("Método: editarCobro");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Cobro cobro = facades
                    .getCobroFacade()
                    .edit(param, cp.getUserInfo());
            
            if(cobro != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", cobro.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/talonario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response talonarioCobro(@BeanParam TalonarioParam param) {

        try {
            
            param.setIdEmpresa(getIdEmpresa());
            param.setFecha(Calendar.getInstance().getTime());
            param.setIdTipoDocumento(2);
            param.log("Método: talonarioCobro");
            
            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }
            
            List<TalonarioPojo> lista = facades.getTalonarioFacade().getTalonarios(param);
            Integer totalSize = lista.size();
            
            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfCobro(@PathParam("id") Integer id, @BeanParam CobroParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarPdfCobro");

            byte[] bytes = facades.getCobroFacade().getPdfCobro(param, cp.getUserInfo());
            
            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/pdf");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.pdf\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/descarga-masiva")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfCobroMasivo(CobroParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(getIdEmpresa());
            param.log("Método: consultarPdfCobroMasivo");

            byte[] bytes = facades.getCobroFacade().getPdfCobroMasivo(param, cp.getUserInfo());
            
            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/zip");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"listado-cobros.zip\""));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularCobro(@PathParam("id") Integer id, CobroParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(getIdEmpresa());
            param.setId(id);
            param.log("Método: anularCobro");
            
            Cobro cobro = facades.getCobroFacade().anular(param, cp.getUserInfo());
            
            if(cobro != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(cobro, Cobro.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/upload/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadCobro(@PathParam("id") Integer id) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            CobroParam param = new CobroParam();
            param.setIdEmpresa(getIdEmpresa());
            param.setId(id);
            param.log("Método: anularCobro");
            
            Boolean upload = facades.getCobroFacade().upload(param, cp.getUserInfo());
            
            if(upload) {
                return Result.createInstance().ok()
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo que obtiene los productos más facturados
     *
     * @param param parámetros
     * @return Response
     */
    @Path("/total-emitidos")
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response totalCobrosEmitidos(@BeanParam CobroParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: totalCobrosEmitidos");

            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            Long result = facades
                    .getCobroFacade()
                    .cantidadCobrosEmitidas(param);
            
             Gson gson = GsonUtils.generateDetault();

            JsonObject data = new JsonObject();
            data.addProperty("cantidad", result);
            
            if(result != null) {
                return Result.createInstance().ok().setPayload(data).build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo que el total de facturas emitidas
     *
     * @param param parámetros
     * @return Response
     */
    @Path("/total-ventas")
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response montoTotalVentas(@BeanParam CobroParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: montoTotalVentas");

            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            Long result = facades
                    .getCobroFacade()
                    .montoTotalVentas(param);

            JsonObject data = new JsonObject();
            data.addProperty("cantidad", result);
            
            if(result != null) {
                return Result.createInstance().ok().setPayload(data).build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<Cobro, CobroParam, UserInfoImpl> getFacade() {
        return facades.getCobroFacade();
    }
}
