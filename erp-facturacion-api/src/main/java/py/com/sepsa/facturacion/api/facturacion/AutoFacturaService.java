/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.erp.ejb.entities.facturacion.AutoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.AutoFacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.misc.Dates;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.misc.NumberToLetterConverter;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Autofactura
 *
 * @author Williams Vera
 */
@Path("/autofactura")
@Log4j2
public class AutoFacturaService extends AbstractService<AutoFactura, AutoFacturaParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo para crear una factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearFactura(AutoFacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearFactura");

            AutoFactura factura = facades
                    .getAutoFacturaFacade()
                    .create(param, cp.getUserInfo());

            if (factura != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(factura);

                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/crear-masivo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response crearFacturaMasiva(@MultipartForm FacturaParam param) {

        Response resp;

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearFacturaMasiva");

            List<FacturaParam> fpl = new ArrayList<>();
            List<FacturaDetalleParam> fdpl = new ArrayList<>();

            InputStreamReader input = new InputStreamReader(new ByteArrayInputStream(param.getUploadedFileBytes()));
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;

            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                lineas.add(line);
            }

            for (int i = 1; i < lineas.size(); i++) {
                lineas.set(i, lineas.get(i) + ";");
            }

            for (int i = 1; i < lineas.size(); i++) {

                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    String[] stringArr = lineas.get(i).split(";");
                    String tipoLinea = stringArr.length <= 0 ? "" : stringArr[0];

                    //Datos de Cabecera
                    String fechaFact = null;
                    String nroFactura = null;
                    String razonSocial = null;
                    String nroDocumento = null;
                    String email = null;
                    String digital = null;
                    String codMoneda = null;
                    String codTipoFactura = null;
                    String tipoOperacion = null;
                    String diasCredito = null;
                    String total = null;

                    //Datos de Detalle
                    String nroFacturaDetalle = null;
                    //String codProducto = null;
                    String concepto = null;
                    String precioUnitarioConIva = null;
                    String porcentajeIva = null;
                    String cantidad = null;

                    FacturaParam fp = new FacturaParam();
                    FacturaDetalleParam fdp = new FacturaDetalleParam();

                    switch (tipoLinea) {
                        case "1":
                            //TODO cabecera

                            for (int k = 0; k < stringArr.length; k++) {
                                switch (k) {
                                    case 1:
                                        nroFactura = (String) stringArr[k];
                                        break;
                                    case 2:
                                        fechaFact = (String) stringArr[k];
                                        break;
                                    case 3:
                                        razonSocial = (String) stringArr[k];
                                        break;
                                    case 4:
                                        nroDocumento = (String) stringArr[k];
                                        break;
                                    case 6:
                                        email = (String) stringArr[k];
                                        break;
                                    case 12:
                                        digital = (String) stringArr[k];
                                        break;
                                    case 13:
                                        codMoneda = (String) stringArr[k];
                                        break;
                                    case 14:
                                        codTipoFactura = (String) stringArr[k];
                                        break;
                                    case 15:
                                        tipoOperacion = (String) stringArr[k];
                                        break;
                                    case 16:
                                        diasCredito = (String) stringArr[k];
                                        break;
                                    case 18:
                                        total = (String) stringArr[k];
                                        break;
                                }

                            }

                            fp.setNroFactura(nroFactura);
                            fp.setFecha(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFact));
                            fp.setRazonSocial(razonSocial);
                            fp.setRuc(nroDocumento);
                            fp.setEmail(email);
                            fp.setDigital(digital.charAt(0));
                            fp.setCodigoMoneda(codMoneda);
                            fp.setCodigoTipoFactura(codTipoFactura);
                            fp.setCodigoTipoOperacionCredito(tipoOperacion);
                            fp.setDiasCredito(Integer.parseInt(diasCredito));
                            try {
                                fp.setMontoTotalFactura(new BigDecimal(total));
                            }
                            catch (NumberFormatException e) {
                                String total1 = precioUnitarioConIva.replace("\"","").trim().replace(" ","");
                                DecimalFormat decimalFormat = new DecimalFormat();
                                decimalFormat.setParseBigDecimal(true);

                                Number parsedTotal = decimalFormat.parse(total1);
                                BigDecimal bdTotal = (BigDecimal) parsedTotal;
                                fp.setMontoTotalFactura(bdTotal);
                            }
                            fpl.add(fp);
                            log.debug("Cabecera");
                            break;
                        case "2":
                            //TODO detalle
                            for (int k = 0; k < stringArr.length; k++) {
                                switch (k) {
                                    case 1:
                                        nroFacturaDetalle = (String) stringArr[k];
                                        break;
                                    case 3:
                                        concepto = (String) stringArr[k];
                                        break;
                                    case 4:
                                        precioUnitarioConIva = (String) stringArr[k];
                                        break;
                                    case 5:
                                        porcentajeIva = (String) stringArr[k];
                                        break;
                                    case 6:
                                        cantidad = (String) stringArr[k];
                                        break;
                                }
                            }
                            
                            fdp.setNroFactura(nroFacturaDetalle);
                            fdp.setDescripcion(concepto);
                            try {
                                fdp.setPrecioUnitarioConIva(new BigDecimal(precioUnitarioConIva.trim().replace(" ","")));
                                fdp.setCantidad(new BigDecimal (cantidad.trim().replace(" ","")));
                            }
                            catch (NumberFormatException e){
                                String precioU = precioUnitarioConIva.replace("\"","").trim().replace(" ","");
                                String cantidad1 = cantidad.replace("\"","").trim().replace(" ","");
                                
                                DecimalFormat decimalFormat = new DecimalFormat();
                                decimalFormat.setParseBigDecimal(true);

                                Number parsedPrecio = decimalFormat.parse(precioU);
                                BigDecimal bdPrecio = (BigDecimal) parsedPrecio;
                            
                                Number parsedCantidad = decimalFormat.parse(cantidad1);
                                BigDecimal bdCantidad = (BigDecimal) parsedCantidad;
                                
                                fdp.setPrecioUnitarioConIva(bdPrecio);
                                fdp.setCantidad(bdCantidad);
                                
                            }
                            fdp.setPorcentajeIva(Integer.parseInt(porcentajeIva));
                            fdpl.add(fdp);
                            log.debug("Detalle");
                            break;
                    }

                }

            }
            
            String mensaje = "";
            List<String> result = new ArrayList();
            try {
                result = facades
                        .getFacturaFacade()
                        .createFacturaMasiva(fpl, fdpl, cp.getUserInfo());
                
            } catch (Exception e) {
                mensaje = "Ocurrió un error al generar las facturas";
            }

            lineas.add(mensaje);

            StringBuilder buffer = new StringBuilder();
            for (String linea : result) {
                buffer.append(linea).append("\r\n");
            }

            //Para octet stream
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));

            Response.ResponseBuilder responseBuilder = Response.ok(bais);
            responseBuilder.type("text/csv");
            responseBuilder.header("Content-Disposition", "attachment; filename=\"respuesta.csv\"");
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = Result.createInstance().ko().build();

        }
        return resp;
    }

    /**
     * Metodo para editar una factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarAutofactura(AutoFacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: editarFactura");

            AutoFacturaPojo factura = facades
                    .getAutoFacturaFacade()
                    .editPojo(param, cp.getUserInfo());

            if (factura != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(factura, AutoFacturaPojo.class);

                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para reenviar facturas digitales rechazadas
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Path("/reenviar-rechazadas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response reenviarFacturasRechazadas(FacturaParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: reenviarFacturasRechazadas");
            
            Boolean ok = facades
                    .getFacturaFacade()
                    .reenviarFacturasRechazadas(param);
            
            if(ok) {
                return Result.createInstance().ok().build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    

    /**
     * Metodo para consultar los datos para facturar
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/datos-crear")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerDatos(@BeanParam TalonarioParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());

            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

            Date date = Calendar.getInstance().getTime();
            param.setFecha(date);
            param.setIdTipoDocumento(5);
            TalonarioPojo talonario = facades
                    .getTalonarioFacade()
                    .getTalonario(param);

            if (talonario == null) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.TALONARIO_ERROR)
                        .build();
            }

            AutoFacturaPojo item = facades
                    .getAutoFacturaFacade()
                    .obtenerDatosCrear(talonario, param.getIdCliente());

            Gson gson = GsonUtils.generateDetault();

            JsonElement result = gson.toJsonTree(item, AutoFacturaPojo.class);

            return Result.createInstance()
                    .ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo para consultar la lista de talonarios para factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/talonario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response talonarioFactura(@BeanParam TalonarioParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFecha(Calendar.getInstance().getTime());
            param.setIdTipoDocumento(5);
            param.log("Método: talonarioFactura");

            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

            List<TalonarioPojo> lista = facades.getTalonarioFacade().getTalonarios(param);
            Integer totalSize = lista.size();

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo para obtener el monto en letras
     *
     * @param monto monto
     * @param codigoMoneda Código de moneda
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/monto-letras")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerMontoLetras(
            @QueryParam(value = "monto") BigDecimal monto,
            @QueryParam(value = "codigoMoneda") String codigoMoneda) {

        try {

            if (monto == null || codigoMoneda == null) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            NumberToLetterConverter.Moneda moneda;

            try {
                moneda = NumberToLetterConverter.Moneda.valueOf(codigoMoneda);
            } catch (Exception e) {
                moneda = NumberToLetterConverter.Moneda.USD;
            }

            String letras = NumberToLetterConverter.convertNumberToLetter(monto, moneda);

            AutoFacturaPojo item = new AutoFacturaPojo();
            item.setTotalLetras(letras);

            Gson gson = GsonUtils.generateDetault();

            JsonElement result = gson.toJsonTree(item, AutoFacturaPojo.class);

            return Result.createInstance()
                    .ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfFactura(@PathParam("id") Integer id, @BeanParam AutoFacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarPdfAutofactura");

            byte[] bytes = facades.getAutoFacturaFacade().getPdfFactura(param, cp.getUserInfo());

            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/pdf");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.pdf\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/reporte-venta/{fechaDesde}/{fechaHasta}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarReporteVenta(
            @PathParam("fechaDesde") Date fechaDesde,
            @PathParam("fechaHasta") Date fechaHasta,
            @BeanParam FacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFechaDesde(fechaDesde);
            param.setFechaHasta(fechaHasta);
            param.log("Método: consultarReporteVenta");

            byte[] bytes = facades.getFacturaFacade().getXlsReporteVenta(param, cp.getUserInfo());

            if (bytes != null) {

                String fecha = Dates.formatToString(Calendar.getInstance().getTime(), Dates.DateFormat.DATE_XML);

                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/ms-excel");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"reporte-venta-%s.xlsx\"", fecha));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/comprobante-venta/{fecha}/{anual}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarComprobanteVenta(@BeanParam ReporteComprobanteParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: consultarComprobanteVenta");

            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

            Integer size = facades
                    .getFacturaFacade()
                    .generarComprobanteVentaSize(param);

            List<RegistroComprobantePojo> resultList = new ArrayList<>();

            for (int i = 0, j = 0; i < size; i = i + 5000, j++) {

                param.setPageSize(5000);
                param.setFirstResult(i);

                List<RegistroComprobantePojo> list = facades
                        .getFacturaFacade()
                        .generarComprobanteVenta(param);

                RegistroComprobantePojo result = facades
                        .getReportUtils()
                        .generarReporteVenta(j, list, param);

                resultList.add(result);
            }

            RegistroComprobantePojo result = facades.getReportUtils()
                    .generarReporteVenta(resultList);

            //Para octet stream
            ByteArrayInputStream input = new ByteArrayInputStream(result.getBytes());

            Response.ResponseBuilder responseBuilder = Response.ok(input);
            responseBuilder.type(result.getTipoContenido());
            responseBuilder.header("Content-Disposition",
                    String.format("attachment; filename=\"%s\"", result.getNombreArchivo()));
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo que obtiene la lista de factura monto
     *
     * @param param parámetros
     * @return Response
     */
    @Path("/monto")
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findFacturaMonto(@BeanParam FacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findFacturaMonto");

            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            List lista = facades.getFacturaFacade().findMonto(param);

            Long totalSize = facades.getFacturaFacade().findMontoSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo que obtiene la lista de facturas agrupados por cliente
     *
     * @param param parámetros
     * @return Response
     */
    @Path("/monto/cliente")
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findFacturaMontoCliente(@BeanParam FacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFirstResult(0);
            param.setPageSize(100000);
            param.log("Método: findFacturaMontoCliente");

            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            Integer firstResult = param.getFirstResult();
            Integer pageSize = param.getPageSize();

            List<FacturaPojo> lista = facades.getFacturaFacade()
                    .findMonto(param);

            param.setFirstResult(firstResult);
            param.setPageSize(pageSize);

            Map<Integer, FacturaPojo> map = new HashMap<>();

            for (FacturaPojo factura : lista) {

                if (factura.getIdCliente() == null) {
                    continue;
                }

                FacturaPojo item = map.getOrDefault(factura.getIdCliente(), new FacturaPojo());
                item.setIdCliente(factura.getIdCliente());
                item.setRazonSocial(factura.getRazonSocial());

                if (item.getCantidad() == null) {
                    item.setCantidad(1);
                } else {
                    item.setCantidad(item.getCantidad() + 1);
                }

                if (item.getSaldo() == null) {
                    item.setSaldo(factura.getSaldo());
                } else {
                    item.setSaldo(item.getSaldo().add(factura.getSaldo()));
                }

                map.put(factura.getIdCliente(), item);
            }

            List<FacturaPojo> list = new ArrayList<>();
            list.addAll(map.values());

            Integer totalSize = list.size();

            List<FacturaPojo> result = new ArrayList<>();

            if (param.getFirstResult() >= 0
                    && param.getPageSize() > 0
                    && param.getFirstResult() < list.size()) {

                int size = param.getPageSize() + param.getFirstResult();
                size = size > list.size()
                        ? list.size()
                        : size;
                result = list.subList(param.getFirstResult(), size);

            }

            return ResultList.createInstance()
                    .storeData(param, result, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo que obtiene la sumatoria de montos de facturas
     *
     * @param param parámetros
     * @return Response
     */
    @Path("/monto/sumatoria")
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findMontoSumatoria(@BeanParam FacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: findMontoSumatoria");

            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            FacturaPojo result = facades
                    .getFacturaFacade()
                    .findMontoSumatoria(param);

            Gson gson = GsonUtils.generateDetault();

            JsonElement data = gson.toJsonTree(result, FacturaPojo.class);

            return Result.createInstance().ok()
                    .setPayload(data)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularFactura(@PathParam("id") Integer id, AutoFacturaParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: anularAutofactura");

            AutoFactura factura = facades.getAutoFacturaFacade().anular(param, cp.getUserInfo());

            if (factura != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(factura, AutoFactura.class);

                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<AutoFactura, AutoFacturaParam, UserInfoImpl> getFacade() {
        return facades.getAutoFacturaFacade();
    }
}
