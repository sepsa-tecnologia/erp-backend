/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteClienteMoraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.ReporteClienteMoraPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.gson.GsonUtils;

/**
 * Controlador para el reporte de clientes en mora
 * @author Jonathan D. Bernal Fernández
 */
@Path("/reporte/cliente-mora")
@Log4j2
public class ReporteClienteMoraService implements Serializable {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo que obtiene la lista de historico liquidacion
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findClientesMora(@BeanParam ReporteClienteMoraParam param) {
        
        param.log("Método: findClientesMora");
        
        if (!param.isValidToList()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .setParametros(param)
                    .build();
        }

        try {
            FacturaParam param1 = new FacturaParam();
            param1.setFechaDesde(param.getFechaDesde());
            param1.setFechaHasta(param.getFechaHasta());
            param1.setAnulado('N');
            if(!param.getTipoReporte().equalsIgnoreCase("MORA")) {
                param1.setCobrado('N');
            }
            param1.setPageSize(100000);
            param1.isValidToList();
            List<FacturaPojo> list = facades
                    .getFacturaFacade()
                    .findMonto(param1);
            
            Map<String, ReporteClienteMoraPojo> map = new HashMap();
            
            BigDecimal totalFacturado = BigDecimal.ZERO;
            BigDecimal totalPendiente = BigDecimal.ZERO;
            BigDecimal porcentajeMora = BigDecimal.ZERO;
            BigDecimal total0 = BigDecimal.ZERO;
            BigDecimal total30 = BigDecimal.ZERO;
            BigDecimal total60 = BigDecimal.ZERO;
            BigDecimal total90 = BigDecimal.ZERO;
            BigDecimal total120 = BigDecimal.ZERO;
            BigDecimal total150 = BigDecimal.ZERO;
            BigDecimal total180 = BigDecimal.ZERO;
            
            for (FacturaPojo factura : list) {
                
                Long dif = ((param.getFechaHasta().getTime() - factura.getFecha().getTime()) / (24 * 60 * 60 * 1000));
                
                if(!(param.getTipoReporte().equalsIgnoreCase("MORA")
                        || (factura.getSaldo().compareTo(BigDecimal.ZERO) == 1 && dif >= 0))) {
                    continue;
                }
                
                String key = null;
                
                if(param.getTipoReporte().equalsIgnoreCase("CLIENTE")
                        || param.getTipoReporte().equalsIgnoreCase("MORA")) {
                    key = factura.getIdCliente() == null
                        ? factura.getRazonSocial()
                        : factura.getIdCliente().toString();
                } else if (param.getTipoReporte().equalsIgnoreCase("COMERCIAL")) {
                    key = factura.getIdComercial()== null
                        ? factura.getComercial()
                        : factura.getIdComercial().toString();
                }
                
                if(key == null || key.trim().isEmpty()) {
                    continue;
                }
                
                ReporteClienteMoraPojo item = map.getOrDefault(key,
                        new ReporteClienteMoraPojo());
                
                if (!param.getTipoReporte().equalsIgnoreCase("COMERCIAL")) {
                    item.setIdCliente(factura.getIdCliente());
                    item.setCliente(factura.getRazonSocial());
                }
                item.setIdComercial(factura.getIdComercial());
                item.setComercial(factura.getComercial());
                
                
                Calendar fechaFactura = Calendar.getInstance();
                fechaFactura.setTime(factura.getFecha());
                
                BigDecimal monto;
                
                BigDecimal temp = factura.getSaldo().compareTo(BigDecimal.ZERO) == -1
                        ? BigDecimal.ZERO
                        : factura.getSaldo();
                BigDecimal pendiente = item.getMontoTotalPendiente().add(temp);
                item.setMontoTotalPendiente(pendiente);
                
                Calendar cal = Calendar.getInstance();
                cal.setTime(param.getFechaHasta());
                if(compare(cal, Calendar.MONTH, 0, fechaFactura)) {
                    monto = item.getMonto0().add(temp);
                    item.setMonto0(monto);
                    total0 = total0.add(temp);
                } else if(compare(cal, Calendar.MONTH, -1, fechaFactura)) {
                    monto = item.getMonto30().add(temp);
                    item.setMonto30(monto);
                    total30 = total30.add(temp);
                } else if(compare(cal, Calendar.MONTH, -1, fechaFactura)) {
                    monto = item.getMonto60().add(temp);
                    item.setMonto60(monto);
                    total60 = total60.add(temp);
                } else if(compare(cal, Calendar.MONTH, -1, fechaFactura)) {
                    monto = item.getMonto90().add(temp);
                    item.setMonto90(monto);
                    total90 = total90.add(temp);
                } else if(compare(cal, Calendar.MONTH, -1, fechaFactura)) {
                    monto = item.getMonto120().add(temp);
                    item.setMonto120(monto);
                    total120 = total120.add(temp);
                } else if(compare(cal, Calendar.MONTH, -1, fechaFactura)) {
                    monto = item.getMonto150().add(temp);
                    item.setMonto150(monto);
                    total150 = total150.add(temp);
                } else {
                    monto = item.getMonto180().add(temp);
                    item.setMonto180(monto);
                    total180 = total180.add(temp);
                }
                
                BigDecimal totalFactura = item.getMontoTotalFacturado()
                        .add(factura.getMontoTotalFactura());
                item.setMontoTotalFacturado(totalFactura);
                
                BigDecimal porcentaje = item.getMontoTotalFacturado().compareTo(BigDecimal.ZERO) == 0
                        ? BigDecimal.ZERO
                        : item.getMontoTotalPendiente()
                                .divide(item.getMontoTotalFacturado(), 6, RoundingMode.HALF_UP)
                                .multiply(new BigDecimal("100"));
                item.setPorcentajeMora(porcentaje);
                
                totalPendiente = totalPendiente.add(temp);
                totalFacturado = totalFacturado.add(factura.getMontoTotalFactura());
                map.put(key, item);
            }
            
            porcentajeMora = !totalFacturado.equals(BigDecimal.ZERO)
                ? (totalPendiente.multiply(new BigDecimal("100")).divide(totalFacturado, 3, RoundingMode.HALF_UP))
                : porcentajeMora;
            
            List<ReporteClienteMoraPojo> result = new ArrayList<>();
            result.addAll(map.values());

            Integer totalSize = result.size();

            List<ReporteClienteMoraPojo> lista = new ArrayList<>();
            
            if(param.getFirstResult() >= 0
                    && param.getPageSize() > 0
                    && param.getFirstResult() < result.size()) {
                
                int size = param.getPageSize() + param.getFirstResult();
                size = size > result.size()
                        ? result.size()
                        : size;
                lista = result.subList(param.getFirstResult(), size);
                
            }
            
            if(param.getResumen()) {
                
                ReporteClienteMoraPojo item = new ReporteClienteMoraPojo();
                item.setMonto0(total0);
                item.setMonto30(total30);
                item.setMonto60(total60);
                item.setMonto90(total90);
                item.setMonto120(total120);
                item.setMonto150(total150);
                item.setMonto180(total180);
                item.setPorcentajeMora(porcentajeMora);
                item.setMontoTotalFacturado(totalFacturado);
                item.setMontoTotalPendiente(totalPendiente);
                
                Gson gson = GsonUtils.generateDetault();
                
                return Result.createInstance().ok()
                        .setPayload(gson.toJsonTree(item))
                        .build();
            } else {
                return ResultList.createInstance()
                        .storeData(param, lista, totalSize)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    private boolean compare(Calendar cal, int fiel, int add,
            Calendar cal2) {
        
        cal.add(fiel, add);
        
        return cal.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }
}
