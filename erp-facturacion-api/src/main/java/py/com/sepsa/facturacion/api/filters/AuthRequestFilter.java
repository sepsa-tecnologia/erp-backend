/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.filters;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import py.com.sepsa.erp.ejb.entities.usuario.filters.TokenParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.TokenPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.facades.usuario.TokenFacade;
import py.com.sepsa.erp.ejb.facades.usuario.UsuarioFacade;
import py.com.sepsa.erp.ejb.utils.jwe.AuthUtil;
import py.com.sepsa.erp.ejb.utils.jwe.JWEUtils;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan Bernal
 */
@Provider
public class AuthRequestFilter implements ContainerRequestFilter {

    @Inject
    private TokenFacade tokenFacade;

    @Inject
    private UsuarioFacade usuarioFacade;
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) requestContext
                .getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        Method method = methodInvoker.getMethod();

        if (!method.isAnnotationPresent(PermitAll.class)
                && !requestContext.getMethod().equalsIgnoreCase("OPTIONS")
                && method.isAnnotationPresent(RolesAllowed.class)) {

            //Access denied for all
            if (method.isAnnotationPresent(DenyAll.class)) {
                requestContext.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.ACCESS_FORBIDDEN).build());
                return;
            }

            AuthUtil auth = AuthUtil.instance(requestContext);
            
            switch(auth.getAuthType()) {
                case BEARER:
                    authWithBearer(requestContext, method, auth.getPayload());
                    break;
                    
                case TOKEN:
                    authWithToken(requestContext, method, auth.getPayload());
                    break;
                    
                case NONE:
                    //If no authorization information present; block access
                    requestContext.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.ACCESS_DENIED).build());
                    break;
            }
        }
    }
    
    private void authWithBearer(ContainerRequestContext request, Method method, String token) {
        
        try {
            final JsonObject json = JWEUtils.validateAsJson(token);
            final String subject = json.get("usuario").getAsString();
            final List<String> roles = new ArrayList();
            final String vencimiento = json.get("vencimiento").getAsString();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
            Date fechaVencimiento = sdf.parse(vencimiento);

            final SecurityContext securityContext = request.getSecurityContext();

            JsonArray perfiles = json.getAsJsonArray("perfiles");
            for (JsonElement perfile : perfiles) {
                String software = perfile.getAsJsonObject().get("software").getAsString();
                List<String> softwaresPermitidos = Arrays
                        .asList("SEPSA_ERP");
                if (softwaresPermitidos.contains(software)) {
                    JsonArray array = perfile.getAsJsonObject().get("roles").getAsJsonArray();
                    //Agregamos los roles del usuario asociado al software y el software mismo como un rol mas efectos practicos necesarios
                    if(array.size() > 0) {
                        roles.add(software);
                    }
                    for (JsonElement jsonElement : array) {
                        roles.add(String.format("%s_%s", software, jsonElement.getAsString()));
                    }
                }
            }

            //Verify expiration date
            if (!fechaVencimiento.after(Calendar.getInstance().getTime())) {
                request.setProperty("auth-failed", true);
                request.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.EXPIRED_TOKEN).build());
                return;
            }
            //Verify user access
            if (method.isAnnotationPresent(RolesAllowed.class)) {
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

                //Is user valid?
                if (!isUserAllowed(rolesSet, roles)) {
                    request.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.ACCESS_DENIED).build());
                    return;
                }
            }

            if (subject != null) {
                request.setSecurityContext(new SecurityContext() {
                    @Override
                    public Principal getUserPrincipal() {
                        return new CustomPrincipal() {
                            @Override
                            public String getName() {
                                return subject;
                            }

                            @Override
                            public CustomPrincipal.UserInfo getUserInfo() {
                                return UserInfoImpl.generate(String.format("Bearer %s", token), json);
                            }
                        };
                    }

                    @Override
                    public boolean isUserInRole(String role) {
                        return roles.contains(role);
                    }

                    @Override
                    public boolean isSecure() {
                        return securityContext.isSecure();
                    }

                    @Override
                    public String getAuthenticationScheme() {
                        return securityContext.getAuthenticationScheme();
                    }
                });
            }

        } catch (Exception ex) {
            //Token Inválido
            request.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.ACCESS_DENIED).build());
        }
        
    }
    
    private void authWithToken(ContainerRequestContext request, Method method, String clave) {
        
        try {
            
            TokenParam param = new TokenParam();
            param.setClave(clave);
            param.setActivo('S');
            param.isValidToList();
            TokenPojo tp = tokenFacade.findFirstPojo(param);
            
            if(tp == null) {
                //Token Inválido
                request.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.ACCESS_DENIED).build());
                return;
            }
            
            UsuarioParam uparam = new UsuarioParam();
            uparam.setUsuario(tp.getUsuario());
            UsuarioPojo user = usuarioFacade.findUsuarioPojo(tp.getUsuario());
            user = usuarioFacade.findPojoCompleto(user);
            
            String token = JWEUtils.buildJWT(user, tp.getIdEmpresa());
            
            authWithBearer(request, method, token);
        } catch (Exception ex) {
            //Token Inválido
            request.abortWith(Result.createInstance().un().setResponseCode(ResponseCode.ACCESS_DENIED).build());
        }
        
    }
    
    private boolean isUserAllowed(final Set<String> rolesSet, final List<String> roles) {
        boolean isAllowed = false;

        for (String userRole : roles) {
            if (rolesSet.contains(userRole)) {
                isAllowed = true;
            }
        }

        return isAllowed;
    }
}
