/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.misc.Dates;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Factura Compra
 * @author Jonathan D. Bernal Fernández
 */
@Path("/factura-compra")
@Log4j2
public class FacturaCompraService extends AbstractService<FacturaCompra, FacturaCompraParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearFacturaCompra(FacturaCompraParam param) {
        
        param.log("Método: crearFacturaCompra");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            FacturaCompra facturaCompra = facades
                    .getFacturaCompraFacade()
                    .create(param, cp.getUserInfo());
            
            if(facturaCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(facturaCompra);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularFacturaCompra(@PathParam("id") Integer id, FacturaCompraParam param) {

        try {
            
            param.setId(id);
            param.log("Método: anularFacturaCompra");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            FacturaCompra factura = facades.getFacturaCompraFacade().anular(param, cp.getUserInfo());
            
            if(factura != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(factura, FacturaCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarFacturaCompra(@PathParam("id") Integer id) {

        try {
            FacturaCompraParam param = new FacturaCompraParam();
            param.setId(id);
            param.log("Método: eliminarFacturaCompra");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Boolean ok = facades.getFacturaCompraFacade().delete(param, cp.getUserInfo());
            
            if(ok) {
                return Result.createInstance().ok().build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/comprobante-compra/{fecha}/{anual}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarComprobanteCompra(@BeanParam ReporteComprobanteParam param) {

        try {
            
            param.log("Método: consultarComprobanteCompra");
            
            if(!param.isValidToList()) {
                return Result.createInstance().br()
                            .setResponseCode(ResponseCode.PARAM_ERROR)
                            .setParametros(param)
                            .build();
            }
            
            Integer size = facades
                    .getFacturaCompraFacade()
                    .generarComprobanteCompraSize(param);
            
            List<RegistroComprobantePojo> resultList = new ArrayList<>();
            
            for (int i = 0, j = 0; i < size; i = i + 5000, j++) {
                
                param.setPageSize(5000);
                param.setFirstResult(i);
                
                List<RegistroComprobantePojo> list = facades
                        .getFacturaCompraFacade()
                        .generarComprobanteCompra(param);

                RegistroComprobantePojo result = facades
                        .getReportUtils()
                        .generarReporteCompra(j, list, param);
                
                resultList.add(result);
            }
                
            RegistroComprobantePojo result = facades.getReportUtils()
                    .generarReporteCompra(resultList);
                
            //Para octet stream
            ByteArrayInputStream input = new ByteArrayInputStream(result.getBytes());

            Response.ResponseBuilder responseBuilder = Response.ok(input);
            responseBuilder.type(result.getTipoContenido());
            responseBuilder.header("Content-Disposition",
                    String.format("attachment; filename=\"%s\"", result.getNombreArchivo()));
            return responseBuilder.build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/{id}/procesamiento-archivo/{idProcesamientoArchivo}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarProcesamientoArchivo(
            @PathParam("id") Integer id,
            @PathParam("idProcesamientoArchivo") Integer idProcesamientoArchivo) {

        try {
            
            FacturaCompraParam param = new FacturaCompraParam();
            param.setId(id);
            param.setIdProcesamientoArchivo(idProcesamientoArchivo);
            param.log("Método: actualizarProcesamientoArchivo");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            FacturaCompra factura = facades.getFacturaCompraFacade().updateFileProcess(param, cp.getUserInfo());
            
            if(factura != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(factura, FacturaCompra.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Path("/monto")
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findFacturaCompraMonto(@BeanParam FacturaCompraParam param) {
        
        param.log("Método: findFacturaCompraMonto");

        try {
            
            if (!param.isValidToList()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }
            
            List lista = facades.getFacturaCompraFacade().findMonto(param);

            Long totalSize = facades.getFacturaCompraFacade().findMontoSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/reporte-compra/{fechaDesde}/{fechaHasta}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarReporteCompra(
            @PathParam("fechaDesde") Date fechaDesde,
            @PathParam("fechaHasta") Date fechaHasta) {

        try {
            
            FacturaCompraParam param = new FacturaCompraParam();
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFechaDesde(fechaDesde);
            param.setFechaHasta(fechaHasta);
            
            param.log("Método: consultarReporteCompra");
            
            byte[] bytes = facades.getFacturaCompraFacade().getPdfReporteCompra(param, cp.getUserInfo());
            
            if (bytes != null) {
                
                String fecha = Dates.formatToString(Calendar.getInstance().getTime(), Dates.DateFormat.DATE_XML);
                
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);
                
                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/ms-excel");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"reporte-compra-%s.xlsx\"", fecha));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarFacturaCompra(FacturaCompraParam param) {

        try {
            
            param.log("Método: editarFacturaCompra");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            FacturaCompra facturaCompra = facades.getFacturaCompraFacade().edit(param, cp.getUserInfo());
            
            if(facturaCompra != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(facturaCompra);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<FacturaCompra, FacturaCompraParam, UserInfoImpl> getFacade() {
        return facades.getFacturaCompraFacade();
    }
}
