/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Cuenta;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CuentaParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Cuenta
 * @author Jonathan D. Bernal Fernández
 */
@Path("/cuenta")
@Log4j2
public class CuentaService extends AbstractService<Cuenta, CuentaParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una cuenta de entidad financiera
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearCuenta(CuentaParam param) {

        try {
            
            param.log("Método: crearCuenta");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Cuenta item = facades
                    .getCuentaFacade()
                    .create(param, cp.getUserInfo());
            
            if(item != null) {

                JsonObject element = new JsonObject();
                element.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                        .setPayload(element)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Edita una cuenta de entidad financiera
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCuenta(CuentaParam param) {

        try {
            
            param.log("Método: editarCuenta");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Cuenta item = facades
                    .getCuentaFacade()
                    .edit(param, cp.getUserInfo());
            
            if(item != null) {

                JsonObject element = new JsonObject();
                element.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                        .setPayload(element)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<Cuenta, CuentaParam, UserInfoImpl> getFacade() {
        return facades.getCuentaFacade();
    }
}
