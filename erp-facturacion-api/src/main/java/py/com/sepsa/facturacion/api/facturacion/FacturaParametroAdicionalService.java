/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaParametroAdicional;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParametroAdicionalParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Williams Vera
 */
@Path("/factura-parametro-adicional")
@Log4j2
public class FacturaParametroAdicionalService  extends AbstractService<FacturaParametroAdicional, FacturaParametroAdicionalParam>{
    
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearFacturaParametroAdicional(FacturaParametroAdicionalParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearFacturaParametroAdicional");
            
            FacturaParametroAdicional facturaParametro = facades.getFacturaParametroAdicionalFacade().create(param, cp.getUserInfo());
            
            if(facturaParametro != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(facturaParametro);
                
                return Result.createInstance().ok().setPayload(json).build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<FacturaParametroAdicional, FacturaParametroAdicionalParam, UserInfoImpl> getFacade() {
        return facades.getFacturaParametroAdicionalFacade();
    }
    
}
