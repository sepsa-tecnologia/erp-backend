/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.utils.facades.Facade;

/**
 * Controlador para la instancia Nota Credito Detalle
 * @author Jonathan D. Bernal Fernández
 */
@Path("/nota-credito-detalle")
public class NotaCreditoDetalleService extends AbstractService<NotaCreditoDetalle, NotaCreditoDetalleParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<NotaCreditoDetalle, NotaCreditoDetalleParam, UserInfoImpl> getFacade() {
        return facades.getNotaCreditoDetalleFacade();
    }

}
