
package py.com.sepsa.facturacion.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Encapsula las respuestas del servicio
 * @author Daniel F. Escauriza Arza
 */
public class ResponseWrapper {
    
    /**
     * Bandera que indica si el servicio se ejecuto con éxito
     */
    private boolean success;
    
    /**
     * Bandera que indica si existe una actualización de config
     */
    private boolean configUpdate = false;
    
    /**
     * Bandera que indica si existe un comando
     */
    private boolean existCommand = false;
    
    /**
     * Código de respuesta
     */
    private ResponseCode responseCode;
    
    /**
     * Datos asociados a la respuesta
     */
    private JsonElement payload;
    
    /**
     * Parámetros
     */
    private CommonParam parametros;

    /**
     * Verifica si el servicio se ejecuto con éxito
     * @return Si el servicio se ejecuto con éxito
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Setea si el servicio se ejecuto con éxito
     * @param success Bandera que indica si el servicio se ejecuto con éxito
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Obtiene el código de respuesta
     * @return Código de respuesta
     */
    public ResponseCode getResponseCode() {
        return responseCode;
    }

    /**
     * Setea el código de respuesta
     * @param responseCode Código de respuesta
     */
    public void setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Verifica si existe una actualización de config
     * @return Bandera que indica si existe una actualización de config
     */
    public boolean isConfigUpdate() {
        return configUpdate;
    }

    /**
     * Setea si existe una actualización de config
     * @param configUpdate Bandera que indica si existe una actualización de config
     */
    public void setConfigUpdate(boolean configUpdate) {
        this.configUpdate = configUpdate;
    }

    /**
     * Setea si existe un comando a ejecutar
     * @param existCommand Bandera que indica si existe un comando
     */
    public void setExistCommand(boolean existCommand) {
        this.existCommand = existCommand;
    }

    /**
     * Verifica si existe un comando a ejecutar
     * @return Bandera que indica si existe un comando
     */
    public boolean isExistCommand() {
        return existCommand;
    }

    /**
     * Obtiene los datos asociados a la respuesta
     * @return Datos asociados a la respuesta
     */
    public JsonElement getPayload() {
        return payload;
    }

    /**
     * Setea los datos asociados a la respuesta
     * @param payload Datos asociados a la respuesta
     */
    public void setPayload(JsonElement payload) {
        this.payload = payload;
    }

    /**
     * Convierte la respuesta encapsulada en formato JSON - String
     * @return Respuesta encapsulada en formato JSON - String
     */
    public String toJson() {
        
        JsonObject status = new JsonObject();
        
        Date now = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        status.addProperty("stamp", sdf.format(now));
        
        if(responseCode != null) {
            status.addProperty("description", responseCode.getDescription());
        }
        
        if(parametros != null && parametros.tieneErrores()) {
            
            JsonArray errores = new JsonArray();
            
            for (MensajePojo error : parametros.getErrores()) {
                
                JsonObject element = new JsonObject();
                element.addProperty("descripcion", error.getDescripcion());
                
                errores.add(element);
            }
            
            status.add("mensajes", errores);
        }
        
        JsonObject json = new JsonObject();
        json.addProperty("success", success);
        json.addProperty("raw", false);
        json.add("status", status);
        if(configUpdate){
            json.addProperty("configUpdate", configUpdate);
        }
        if(existCommand){
            json.addProperty("existCommand", existCommand);
        }
        
        if(payload != null) {
            json.add("payload", payload);
        }
        
        return json.toString();
    }
    
    /**
     * Constructor de ResponseWrapper
     * @param success Bandera que indica si el servicio se ejecuto correctamente
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     */
    public ResponseWrapper(boolean success, ResponseCode responseCode, 
            JsonElement payload) {
        this.success = success;
        this.responseCode = responseCode;
        this.payload = payload;
    }
    
    /**
     * Constructor de ResponseWrapper
     * @param success Bandera que indica si el servicio se ejecuto correctamente
     * @param responseCode Código de respuesta
     * @param payload Datos asociados a la respuesta
     * @param parametros Parámetros
     */
    public ResponseWrapper(boolean success, ResponseCode responseCode, 
            JsonElement payload, CommonParam parametros) {
        this.success = success;
        this.responseCode = responseCode;
        this.payload = payload;
        this.parametros = parametros;
    }
    
}
