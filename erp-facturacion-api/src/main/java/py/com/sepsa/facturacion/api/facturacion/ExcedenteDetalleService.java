/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.JsonObject;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.ExcedenteDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ExcedenteDetalleParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Excedente Detalle
 * @author Jonathan D. Bernal Fernández
 */
@Path("/excedente-detalle")
@Log4j2
public class ExcedenteDetalleService extends AbstractService<ExcedenteDetalle, ExcedenteDetalleParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear un excedente detalle
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearExcedenteDetalle(ExcedenteDetalleParam param) {
        
        param.log("Método: crearExcedenteDetalle");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            ExcedenteDetalle item = facades
                    .getExcedenteDetalleFacade()
                    .create(param, cp.getUserInfo());
            
            if(item != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar un excedente detalle
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarExcedenteDetalle(ExcedenteDetalleParam param) {
        
        param.log("Método: editarExcedenteDetalle");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            ExcedenteDetalle item = facades
                    .getExcedenteDetalleFacade()
                    .edit(param, cp.getUserInfo());
            
            if(item != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<ExcedenteDetalle, ExcedenteDetalleParam, UserInfoImpl> getFacade() {
        return facades.getExcedenteDetalleFacade();
    }
}
