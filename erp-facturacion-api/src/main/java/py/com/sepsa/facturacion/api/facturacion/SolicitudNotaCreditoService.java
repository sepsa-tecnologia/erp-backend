/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.SolicitudNotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.SolicitudNotaCreditoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Solicitud Nota de Crédito
 * @author Jonathan D. Bernal Fernández
 */
@Path("/solicitud-nota-credito")
@Log4j2
public class SolicitudNotaCreditoService extends AbstractService<SolicitudNotaCredito, SolicitudNotaCreditoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearSnc(SolicitudNotaCreditoParam param) {
        
        param.log("Método: crearSnc");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            SolicitudNotaCredito snc = facades
                    .getSolicitudNotaCreditoFacade()
                    .create(param, cp.getUserInfo());
            
            if(snc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(snc);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/{id}/procesamiento-archivo/{idProcesamientoArchivo}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarProcesamientoArchivo(
            @PathParam("id") Integer id,
            @PathParam("idProcesamientoArchivo") Integer idProcesamientoArchivo) {

        try {
            
            SolicitudNotaCreditoParam param = new SolicitudNotaCreditoParam();
            param.setId(id);
            param.setIdProcesamientoArchivo(idProcesamientoArchivo);
            param.log("Método: actualizarProcesamientoArchivo");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            SolicitudNotaCredito snc = facades.getSolicitudNotaCreditoFacade().updateFileProcess(param, cp.getUserInfo());
            
            if(snc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(snc, SolicitudNotaCredito.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarSnc(SolicitudNotaCreditoParam param) {

        try {
            
            param.log("Método: editarSnc");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            SolicitudNotaCredito snc = facades.getSolicitudNotaCreditoFacade().edit(param, cp.getUserInfo());
            
            if(snc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(snc);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<SolicitudNotaCredito, SolicitudNotaCreditoParam, UserInfoImpl> getFacade() {
        return facades.getSolicitudNotaCreditoFacade();
    }
}
