/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionServicio;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionServicioParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Liquidación Servicio
 * @author Jonathan D. Bernal Fernández
 */
@Path("/liquidacion-servicio")
@Log4j2
public class LiquidacionServicioService extends AbstractService<LiquidacionServicio, LiquidacionServicioParam>{
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    /**
     * Metodo que obtiene la lista de liquidación servicio
     *
     * @param idServicio Identificador de servicio
     * @param idCliente Identificador de cliente
     * @param idLiquidacion Identificador de liquidacion
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/descripcion")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findLiquidacionServicioDescripcion(
            @QueryParam("idServicio") Integer idServicio,
            @QueryParam("idCliente") Integer idCliente,
            @QueryParam("idLiquidacion") Integer idLiquidacion) {

        if (idServicio == null) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }
        
        try {
            
            LiquidacionServicioParam param = new LiquidacionServicioParam();
            param.setIdServicio(idServicio);
            param.setIdCliente(idCliente);
            param.isValidToList();
            
            List<LiquidacionServicio> list = facades
                    .getLiquidacionServicioFacade().find(param);
            
            if(list == null || list.isEmpty()) {
                param.setIdCliente(null);
                list = facades.getLiquidacionServicioFacade().find(param);
            }
            
            if(list == null || list.isEmpty()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.RESOURCE_NOT_FOUND)
                        .build();
            }
            
            LiquidacionServicio item = list.get(0);
            
            Liquidacion liquidacion = idLiquidacion == null
                    ? null
                    : facades.getLiquidacionFacade().find(idLiquidacion);
            if(liquidacion != null) {
                
                Map<String, String> parametros = new HashMap<>();
                /*parametros.put("PRODUCTO", liquidacion.getServicio().getIdProducto().getDescripcion());
                parametros.put("SERVICIO", liquidacion.getServicio().getDescripcion());
                parametros.put("ID_CONTRATO", String.valueOf(liquidacion.getIdContrato()));*/

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat sdf1 = new SimpleDateFormat("MMMMM");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");
                
                /*parametros.put("FECHA_DESDE", sdf.format(liquidacion.getFechaDesde()));
                parametros.put("FECHA_HASTA", sdf.format(liquidacion.getFechaHasta()));
                parametros.put("MES", sdf1.format(liquidacion.getFechaHasta()));
                parametros.put("ANHO", sdf2.format(liquidacion.getFechaHasta()));*/
                
                String desc = item.getDescripcion();

                for (Map.Entry<String, String> entry : parametros.entrySet()) {
                    desc = desc.replace(String.format("${%s}", entry.getKey()), entry.getValue());
                }
                
                item.setDescripcion(desc);
            }
            
            
            Gson gson = GsonUtils.generateDetault();
            
            return Result.createInstance().ok()
                    .setPayload(gson.toJsonTree(item))
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo para editar/crear una liquidación servicio
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarOCrearLiquidacionServicio(LiquidacionServicioParam param) {
        
        param.log("Método: crearFactura");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            LiquidacionServicio item = facades
                    .getLiquidacionServicioFacade()
                    .create(param, cp.getUserInfo());
            
            if(item != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", item.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<LiquidacionServicio, LiquidacionServicioParam, UserInfoImpl> getFacade() {
        return facades.getLiquidacionServicioFacade();
    }
}
