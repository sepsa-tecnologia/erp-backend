/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoCambio;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoCambioParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia TipoCambio
 * @author Jonathan D. Bernal Fernández
 */
@Path("/tipo-cambio")
@Log4j2
public class TipoCambioService extends AbstractService<TipoCambio, TipoCambioParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearTipoCambio(TipoCambioParam param) {
        
        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearTipoCambio");
            
            
            TipoCambio tipoCambio = facades
                    .getTipoCambioFacade()
                    .create(param, cp.getUserInfo());
            
            if(tipoCambio != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(tipoCambio, TipoCambio.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/actualizar-tipo-cambio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarTipoCambio(TipoCambioParam param) {
        
        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: actualizarTipoCambio");
            
            
            TipoCambio tipoCambio = facades
                    .getTipoCambioFacade()
                    .actualizarTipoCambio(param, cp.getUserInfo());
            
            if(tipoCambio != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(tipoCambio, TipoCambio.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    
    /**
     * Metodo para editar una factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarTipoCambio(TipoCambioParam param) {

        try {
            
            param.log("Método: editarTipoCambio");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            TipoCambio tipoCambio = facades
                    .getTipoCambioFacade()
                    .edit(param, cp.getUserInfo());
            
            if(tipoCambio != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(tipoCambio, TipoCambio.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<TipoCambio, TipoCambioParam, UserInfoImpl> getFacade() {
        return facades.getTipoCambioFacade();
    }
}
