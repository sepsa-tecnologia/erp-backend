/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.TalonarioLocal;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioLocalParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan Bernal
 */
@Path("/talonario-local")
@Log4j2
public class TalonarioLocalService extends AbstractService<TalonarioLocal, TalonarioLocalParam> {
    
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarCrearTalonarioLocal(TalonarioLocalParam param) {

        param.log("Método: editarCrearTalonarioLocal");

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            TalonarioLocal talonario = getFacade().editarOCrear(param, cp.getUserInfo());

            if(talonario != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(talonario, TalonarioLocal.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }
        } catch (Exception e) {
            log.fatal("Error", e);
            return Result.createInstance().ko().build();
        }
    }
    
    @Path("/relacionado")
    @GET
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findRelacionado(@BeanParam TalonarioLocalParam param) {

        if (!param.isValidToListRelacionado()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .setParametros(param)
                    .build();
        }

        try {
            
            param.log("Método: findRelacionado");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            List<TalonarioLocal> lista = facades.getTalonarioLocalFacade().findRelacionados(param);
            Long totalSize = facades.getTalonarioLocalFacade().findRelacionadosSize(param);

            JsonArray json = new JsonArray();

            Gson gson = GsonUtils.generateDetault();

            for (TalonarioLocal item : lista) {
                json.add(gson.toJsonTree(item));
            }

            JsonObject respValue = new JsonObject();
            respValue.add("data", json);
            respValue.addProperty("totalSize", totalSize);
            respValue.addProperty("firstResult", param.getFirstResult());
            respValue.addProperty("pageSize", param.getPageSize());
            
            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/asociar-masivo")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response asociarMasivo(TalonarioLocalParam param) {

        if (!param.isValidToCreateMasivo()) {
            return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
        }
        
        try {
            
            param.log("Método: asociarMasivo");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            facades.getTalonarioLocalFacade().asociarMasivo(param, cp.getUserInfo());
                
            return Result.createInstance().ok().build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<TalonarioLocal, TalonarioLocalParam, UserInfoImpl> getFacade() {
        return facades.getTalonarioLocalFacade();
    }
}
