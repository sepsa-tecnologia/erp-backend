/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Talonario
 * @author Jonathan D. Bernal Fernández
 */
@Path("/talonario")
@Log4j2
public class TalonarioService extends AbstractService<Talonario, TalonarioParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo que obtiene la lista de encargados
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/encargados")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findEncargados(@BeanParam PersonaParam param) {
        
        if (!param.isValidToList()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }
        
        param.log("Método: findEncargados");

        try {
            List lista = facades
                    .getPersonaFacade()
                    .find(param);

            Long totalSize = facades
                    .getPersonaFacade()
                    .findSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo que obtiene la lista de talonario
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response creatTalonario(TalonarioParam param) {

        try {
            
            param.log("Método: creatTalonario");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Talonario talonario = facades
                    .getTalonarioFacade()
                    .create(param, cp.getUserInfo());
            
            if(talonario != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(talonario, Talonario.class);
                
                facades.getTalonarioFacade().inactivarTalonarios(
                        cp.getUserInfo().getIdEmpresa(),
                        talonario.getIdTipoDocumento(),
                        talonario.getIdEncargado(),
                        talonario.getId());
                
                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Edita un talonario
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editTalonario(TalonarioParam param) {

        try {
            
            param.log("Método: editTalonario");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Talonario talonario = facades
                    .getTalonarioFacade()
                    .edit(param, cp.getUserInfo());
            
            if(talonario != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(talonario, Talonario.class);
                
                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    /**
     * Metodo que obtiene la cantidad de hojas de un talonario
     *
     * @param id Identificador de talonario
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/cantidad-hojas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cantidadHojas(@PathParam("id") Integer id) {

        try {
            TalonarioParam param = new TalonarioParam();
            param.setId(id);
            param.log("Método: cantidadHojas");
            
            Integer cantidadHojas = facades
                    .getTalonarioFacade()
                    .cantidadHojas(param);
            
            if(cantidadHojas != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("cantidadHojas", cantidadHojas);
                
                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<Talonario, TalonarioParam, UserInfoImpl> getFacade() {
        return facades.getTalonarioFacade();
    }
}
