/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.erp.ejb.entities.facturacion.LugarCobro;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Controlador para la instancia LugarCobro
 * @author Jonathan D. Bernal Fernández
 */
@Path("/lugar-cobro")
public class LugarCobroService extends AbstractService<LugarCobro, CommonParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<LugarCobro, CommonParam, UserInfoImpl> getFacade() {
        return facades.getLugarCobroFacade();
    }

}
