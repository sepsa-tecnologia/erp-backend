/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import java.io.Serializable;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Antonella Lucero
 */
@Path("/bloqueo")
@Log4j2
public class BloqueoService implements Serializable {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Context
    private SecurityContext sc;

    @RolesAllowed({"SEPSA_ERP"})
    @Path("/realizar-bloqueo/{idClienteSepsa}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response realizarBloqueo(@PathParam("idClienteSepsa") Integer idClienteSepsa) {

        try {
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();

            List<UsuarioPojo> result = facades
                    .getUsuarioFacade()
                    .bloquearUsuarios(idClienteSepsa, cp.getUserInfo());

            if (result == null || result.isEmpty()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }
            
            CommonParam param = new CommonParam();

            return ResultList.createInstance()
                    .storeData(param, result, result.size())
                    .build();
        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @Path("/realizar-desbloqueo/{idClienteSepsa}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response realizarDesbloqueo(@PathParam("idClienteSepsa") Integer idClienteSepsa) {

        try {
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();

            List<UsuarioPojo> result = facades
                    .getUsuarioFacade()
                    .desbloquearUsuarios(idClienteSepsa, cp.getUserInfo());

            if (result == null || result.isEmpty()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
            }

            CommonParam param = new CommonParam();
            
            return ResultList.createInstance()
                    .storeData(param, result, result.size())
                    .build();
        } catch (Exception ex) {
            log.fatal("Se produjo un error:", ex);
            return Result.createInstance().ko().build();
        }
    }

}
