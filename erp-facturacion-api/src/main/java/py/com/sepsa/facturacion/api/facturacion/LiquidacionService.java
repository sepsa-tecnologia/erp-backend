/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.LiquidacionPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Controlador para la instancia Liquidacion
 * @author Jonathan D. Bernal Fernández
 */
@Path("/liquidacion")
@Log4j2
public class LiquidacionService extends AbstractService<Liquidacion, LiquidacionParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    /**
     * Metodo para crear una liquidación
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearLiquidacion(LiquidacionParam param) {
        
        param.log("Método: crearLiquidacion");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Liquidacion liquidacion = facades
                    .getLiquidacionFacade()
                    .findOrCreate(param, cp.getUserInfo());
            
            if(liquidacion != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(liquidacion, Liquidacion.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo para editar una liquidación
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarLiquidacion(LiquidacionParam param) {
        
        param.log("Método: editarLiquidacion");

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            Liquidacion liquidacion = facades
                    .getLiquidacionFacade()
                    .edit(param, cp.getUserInfo());
            
            if(liquidacion != null) {
                
                JsonObject json = new JsonObject();
                json.addProperty("id", liquidacion.getId());
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    /**
     * Metodo que obtiene la lista de liquidacion por cliente-producto
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @Path("/por-cliente-producto")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findLiquidacionPorClienteProducto(@BeanParam LiquidacionParam param) {
        
        if (!param.isValidToList()) {
            return Result.createInstance().br()
                    .setResponseCode(ResponseCode.PARAM_ERROR)
                    .build();
        }
        
        param.log("Método: findLiquidacionPorClienteProducto");

        try {
            List<LiquidacionPojo> lista = facades
                    .getLiquidacionFacade()
                    .findPorClienteProducto(param);

            Long totalSize = facades
                    .getLiquidacionFacade()
                    .findPorClienteProductoSize(param);

            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @Override
    public Facade<Liquidacion, LiquidacionParam, UserInfoImpl> getFacade() {
        return facades.getLiquidacionFacade();
    }
}
