package py.com.sepsa.facturacion.api.facturacion;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemision;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaRemisionPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.NotaRemisionUtils;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Controlador para la instancia Nota de Remision
 * @author Williams Vera
 */
@Path("/nota-remision")
@Log4j2
public class NotaRemisionService extends AbstractService<NotaRemision,NotaRemisionParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

      /**
     * Metodo para crear una nota de crédito
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearNr(NotaRemisionParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setIdUsuario(cp.getUserInfo().getId());
            param.log("Método: crearNr");
            
            NotaRemision nr = facades
                    .getNotaRemisionFacade()
                    .create(param, cp.getUserInfo());
            
            if(nr != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(nr);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            }  else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }
            
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        } 
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Path("/crear-masivo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response crearNotaRemisionMasivo(@MultipartForm NotaRemisionParam param) {
        NotaRemisionUtils n = new NotaRemisionUtils();
        Response resp = null;
        StringBuilder buffer = new StringBuilder();
        try {
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearNotaRemisionMasivo");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            List<NotaRemisionParam> notaRemisionList = n.convertToList(facades,param);
            String inicio = String.format("TIMBRADO;NRO_NOTA_REMISION;FECHA;ID_NOTA_REMISION;ESTADO_RESULTADO");
            buffer.append(inicio).append("\r\n");
            for (NotaRemisionParam nr : notaRemisionList) {
                if(nr.tieneErrores()){
                    String error = String.format("%s;%s;-;-;ERROR;", nr.getTimbrado(), nr.getNroNotaRemision());
                    error = error.concat("%s| ");
                    for (MensajePojo mp : nr.getErrores()) {
                        error = String.format(error, mp.getDescripcion());
                        error = error.concat("%s| ");
                    }
                    error = error.substring(0, error.length() - 5);
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                }else{
                    NotaRemision notaRemision = facades
                        .getNotaRemisionFacade()
                        .create(nr, cp.getUserInfo());
                
                    if (notaRemision == null) {
                        String error = String.format("%s;%s;-;-;ERROR;", nr.getTimbrado(), nr.getNroNotaRemision());
                        error = error.concat("| ");
                        for (MensajePojo mp : nr.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {
                        
                        String ok = String.format("%s;%s;%s;%s;OK", nr.getTimbrado(), nr.getNroNotaRemision(), sdf.format(nr.getFecha()), notaRemision.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }
                }     
            }
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));
            Response.ResponseBuilder responseBuilder = Response.ok(bais);
            responseBuilder.type("text/csv");
            responseBuilder.header("Content-Disposition", "attachment; filename=\"respuesta.csv\"");
            return responseBuilder.build();
   
        } catch (Exception ex) {
            log.fatal("Error", ex);
            resp = Result.createInstance().ko().build();
        }
        return resp; 
    }
    
      /**
     * Metodo para editar una nc
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarNr(NotaRemisionParam param) {

        try {
            
            param.log("Método: editarNr");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            NotaRemision nc = facades
                    .getNotaRemisionFacade()
                    .edit(param, cp.getUserInfo());
            
            if(nc != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(nc);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
     /**
     * Metodo para consultar la lista de talonarios para factura
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/talonario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response talonarioNr(@BeanParam TalonarioParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setFecha(Calendar.getInstance().getTime());
            param.setIdTipoDocumento(4);
            param.log("Método: talonarioNr");
            
            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

            List<TalonarioPojo> lista = facades.getTalonarioFacade().getTalonarios(param);
            Integer totalSize = lista.size();
            
            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();
        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
     /**
     * Metodo para consultar los datos para crear una nota de crédito
     *
     * @param param parámetros
     * @return Response
     */
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/datos-crear")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerDatos(@BeanParam TalonarioParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            
            if (!param.datosCrearValido()) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }
            
            Date date = Calendar.getInstance().getTime();
            param.setFecha(date);
            param.setIdTipoDocumento(4);
            TalonarioPojo talonario = facades.getTalonarioFacade().getTalonario(param);
            
            if(talonario == null) {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.TALONARIO_ERROR)
                        .build();
            }
            
            NotaRemisionPojo item = facades.getNotaRemisionFacade().obtenerDatosCrear(talonario, param.getIdCliente());
            
            Gson gson = GsonUtils.generateDetault();
            
            JsonElement result = gson.toJsonTree(item, NotaRemisionPojo.class);
            
            return Result.createInstance()
                    .ok()
                    .setPayload(result)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfNotaRemision(@PathParam("id") Integer id, @BeanParam NotaRemisionParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarPdfNotaCredito");
            
            byte[] bytes = facades.getNotaRemisionFacade().getPdfNotaRemision(param);
            
            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/pdf");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.pdf\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta-xml/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarXmlNotaRemision(@PathParam("id") Integer id, @BeanParam NotaRemisionParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarXmlNotaRemision");

            byte[] bytes = facades.getNotaRemisionFacade().getXmlNotaRemision(param, cp.getUserInfo());

            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/xml");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.xml\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
     @RolesAllowed({"SEPSA_ERP"})
    @Path("/anular/{id}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response anularNr(@PathParam("id") Integer id, NotaRemisionParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: anularNr");
            
            NotaRemision notaRemision = facades.getNotaRemisionFacade().anular(param, cp.getUserInfo());
            
            if(notaRemision != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(notaRemision, NotaRemision.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<NotaRemision, NotaRemisionParam, UserInfoImpl> getFacade() {
        return facades.getNotaRemisionFacade();
    }
    
    
    
}
