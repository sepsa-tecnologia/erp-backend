/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.facturacion;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.erp.ejb.entities.set.DetalleProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.DetalleProcesamientoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.utils.facades.Facade;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
@Path("/detalle-procesamiento")
public class DetalleProcesamientoService extends AbstractService<DetalleProcesamiento, DetalleProcesamientoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<DetalleProcesamiento, DetalleProcesamientoParam, UserInfoImpl> getFacade() {
        return facades.getDetalleProcesamientoFacade();
    }

}
