/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.externalservice.http;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import py.com.sepsa.utils.rest.client.API;
import py.com.sepsa.utils.rest.client.ConnectionPojo;

/**
 * Clase para el manejo de los host
 * @author Jonathan D. Bernal Fernández
 */
public class Host {
    /**
     * Prioridad del host
     */
    private Integer prioridad;
    
    /**
     * Dirección del host
     */
    private String direccion;
    
    /**
     * Puerto del host
     */
    private String puerto;

    /**
     * Setea el puerto del host
     * @param puerto Puerto del host
     */
    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    /**
     * Obtiene el puerto del host
     * @return Puerto del host
     */
    public String getPuerto() {
        return puerto;
    }

    /**
     * Setea la dirección del host
     * @param direccion Dirección del host
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Obtiene la dirección del host
     * @return Dirección del host
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Setea la prioridad del host
     * @param prioridad Prioridad
     */
    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    /**
     * Obtiene la prioridad del host
     * @return Prioridad
     */
    public Integer getPrioridad() {
        return prioridad;
    }
    
    /**
     * Constructor.
     */
    public Host() {
    }
    
    /**
     * Crea una lista de instancia dada una cadena
     * @param data Cadenas
     * @return Lista de host
     */
    public static ConnectionPojo createConnectionPojo(String data) {
        List<Host> list = createInstaces(data);
        if(list == null || list.isEmpty()) {
            return null;
        }
        
        StringTokenizer st1 = new StringTokenizer(list.get(0).getDireccion(), "://");
        String protocol = st1.nextToken();
        String host = st1.nextToken();
        String port = st1.nextToken();
        
        ConnectionPojo result = new ConnectionPojo();
        result.setProtocol(protocol.toLowerCase().trim().equals("https") ? API.Scheme.HTTPS : API.Scheme.HTTP);
        result.setHost(host);
        result.setPort(Integer.valueOf(port));
        return result;
    }
    
    /**
     * Crea una lista de instancia dada una cadena
     * @param data Cadenas
     * @return Lista de host
     */
    public static List<Host> createInstaces(String data) {
        
        List<Host> result = new ArrayList<>();
        
        try {
            StringTokenizer st = new StringTokenizer(data, ";");
            
            while (st.hasMoreTokens()) {
                
                StringTokenizer st2 = new StringTokenizer(st.nextToken(), "#");
                
                if(st2.countTokens() >= 2) {

                    Integer prioridad = Integer.valueOf(st2.nextToken());
                    
                    Host host = new Host();
                    host.setPrioridad(prioridad);
                    host.setDireccion(st2.nextToken());
                    
                    result.add(host);
                }
            }
        } catch(Exception ex) {}
        
        return result;
    }
}
