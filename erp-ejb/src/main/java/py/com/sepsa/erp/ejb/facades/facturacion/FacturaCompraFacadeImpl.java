/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.erp.reporte.pojos.factura.ReporteCompraParam;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaCompraFacade", mappedName = "FacturaCompraFacade")
@javax.ejb.Local(FacturaCompraFacade.class)
public class FacturaCompraFacadeImpl extends FacadeImpl<FacturaCompra, FacturaCompraParam> implements FacturaCompraFacade {

    public FacturaCompraFacadeImpl() {
        super(FacturaCompra.class);
    }

    public Boolean validToEdit(FacturaCompraParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        FacturaCompra factura = find(param.getId());

        if (factura == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToCreate(UserInfoImpl userInfo, FacturaCompraParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        TipoFactura tipoFactura = facades
                .getTipoFacturaFacade()
                .find(param.getIdTipoFactura(), param.getCodigoTipoFactura());

        if (tipoFactura == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de factura"));
        } else {
            param.setIdTipoFactura(tipoFactura.getId());
        }

        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda(), param.getCodigoMoneda());

        if (moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
        }

        if (!isNull(param.getIdOrdenCompra())) {

            OrdenCompra ordenCompra = facades.getOrdenCompraFacade().find(param.getIdOrdenCompra());

            if (isNull(ordenCompra)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
            }
        }
        
        if(param.getIdPersona() != null) {
            
            Persona persona = facades.getPersonaFacade().find(param.getIdPersona());

            if (persona == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la persona"));
            }
        }

        if(param.getIdLocalOrigen() != null) {
            
            Local local = facades.getLocalFacade().find(param.getIdLocalOrigen());
            
            if(local == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local origen"));
            }
        }

        if(param.getIdLocalDestino() != null) {
            
            Local local = facades.getLocalFacade().find(param.getIdLocalDestino());
            
            if(local == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local destino"));
            }
        }
        
        if(!isNullOrEmpty(param.getHashProcesamientoArchivo())) {
            
            FacturaCompraParam param1 = new FacturaCompraParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setHashProcesamientoArchivo(param.getHashProcesamientoArchivo());
            Long size = findSize(param1);
            
            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe una factura de compra registrada con el mismo hash de archivo"));
            }
        }
        
        if (!facades.getTalonarioFacade().fechaDocumentoEnRango(null, param.getFechaVencimientoTimbrado(), param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la factura debe estar en el rango de vigencia del timbrado"));
        }
        
        if (!isNull(param.getIdOrdenCompra())) {

            OrdenCompra ordenCompra = facades.getOrdenCompraFacade().find(param.getIdOrdenCompra());

            if (isNull(ordenCompra)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
            }
        }

        if (!isNull(param.getOrdenCompra())) {
            facades.getOrdenCompraFacade().validToCreate(userInfo, param.getOrdenCompra());
        }
        
        for (FacturaCompraDetalleParam detalle : param.getFacturaCompraDetalles()) {
            facades.getFacturaCompraDetalleFacade().validToCreate(detalle, Boolean.FALSE);
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToCancel(FacturaCompraParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "FACTURA_COMPRA");
        
        if(motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        }
        
        FacturaCompra factura = facades.getFacturaCompraFacade().find(param.getId());

        if (factura == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura de compra"));
        } else {

            if (Objects.equals(factura.getAnulado(), 'S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La factura de compra ya se encuentra anulada"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToDelete(FacturaCompraParam param) {

        if (!param.isValidToDelete()) {
            return Boolean.FALSE;
        }
        
        FacturaCompra factura = facades.getFacturaCompraFacade().find(param.getId());

        if (factura == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura de compra"));
        }

        return !param.tieneErrores();
    }
    
    public Boolean validToUpdateFileProcess(FacturaCompraParam param) {
        
        if(!param.isValidToUpdateFileProcess()) {
            return Boolean.FALSE;
        }
        
        FacturaCompra facturaCompra = find(param.getId());
        
        if(isNull(facturaCompra)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura de compra"));
        }
        
        ProcesamientoArchivo procesamientoArchivo = facades.getProcesamientoArchivoFacade().find(param.getIdProcesamientoArchivo());
        
        if(isNull(procesamientoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el procesamiento de archivo"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public FacturaCompra create(FacturaCompraParam param, UserInfoImpl userInfo) {
        
        if (!validToCreate(userInfo, param)) {
            return null;
        }
        
        if (!isNull(param.getOrdenCompra())) {
            OrdenCompra ordenCompra = facades.getOrdenCompraFacade().create(param.getOrdenCompra(), userInfo);
            param.setIdOrdenCompra(ordenCompra == null ? null : ordenCompra.getId());
        }

        FacturaCompra factura = new FacturaCompra();
        factura.setFechaInsercion(Calendar.getInstance().getTime());
        factura.setIdEmpresa(userInfo.getIdEmpresa());
        factura.setAnulado(param.getAnulado());
        factura.setCdc(param.getCdc());
        factura.setDiasCredito(param.getDiasCredito());
        factura.setDigital(param.getDigital());
        factura.setPagado(param.getPagado());
        factura.setFecha(param.getFecha());
        factura.setNroTimbrado(param.getNroTimbrado());
        factura.setFechaVencimientoTimbrado(param.getFechaVencimientoTimbrado());
        factura.setIdPersona(param.getIdPersona());
        factura.setIdMoneda(param.getIdMoneda());
        factura.setIdTipoFactura(param.getIdTipoFactura());
        factura.setMontoImponible10(param.getMontoImponible10().stripTrailingZeros());
        factura.setMontoImponible5(param.getMontoImponible5().stripTrailingZeros());
        factura.setMontoIva10(param.getMontoIva10().stripTrailingZeros());
        factura.setMontoIva5(param.getMontoIva5().stripTrailingZeros());
        factura.setMontoTotal10(param.getMontoTotal10().stripTrailingZeros());
        factura.setMontoTotal5(param.getMontoTotal5().stripTrailingZeros());
        factura.setMontoTotalExento(param.getMontoTotalExento().stripTrailingZeros());
        factura.setMontoImponibleTotal(param.getMontoImponibleTotal().stripTrailingZeros());
        factura.setMontoIvaTotal(param.getMontoIvaTotal().stripTrailingZeros());
        factura.setMontoTotalFactura(param.getMontoTotalFactura().stripTrailingZeros());
        factura.setSaldo(param.getMontoTotalFactura().stripTrailingZeros());
        factura.setNroFactura(param.getNroFactura());
        factura.setRazonSocial(param.getRazonSocial());
        factura.setRuc(param.getRuc());
        factura.setIdOrdenCompra(param.getIdOrdenCompra());
        factura.setCdc(param.getCdc());
        factura.setIdLocalOrigen(param.getIdLocalOrigen());
        factura.setIdLocalDestino(param.getIdLocalDestino());
        factura.setIdOrdenCompra(param.getIdOrdenCompra());

        create(factura);

        List<FacturaCompraDetalle> detalles = new ArrayList();
        
        if(param.getFacturaCompraDetalles() != null) {
            for (FacturaCompraDetalleParam detalle : param.getFacturaCompraDetalles()) {
                detalle.setIdFacturaCompra(factura.getId());
                FacturaCompraDetalle item = facades.getFacturaCompraDetalleFacade().create(detalle, userInfo);
                detalles.add(item);
            }
        }
        
        return factura;
    }
    
    @Override
    public FacturaCompra anular(FacturaCompraParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        FacturaCompra factura = find(param.getId());
        factura.setAnulado('S');
        factura.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
        factura.setObservacionAnulacion(param.getObservacionAnulacion());

        edit(factura);

        return factura;
    }
    
    @Override
    public Boolean delete(FacturaCompraParam param, UserInfoImpl userInfo) {

        if (!validToDelete(param)) {
            return Boolean.FALSE;
        }

        RetencionCompraParam param1 = new RetencionCompraParam();
        param1.setIdFacturaCompra(param.getId());
        facades.getRetencionCompraFacade().delete(param1, userInfo);

        NotaCreditoCompraParam param2 = new NotaCreditoCompraParam();
        param2.setIdFacturaCompra(param.getId());
        facades.getNotaCreditoCompraFacade().delete(param2, userInfo);
        
        FacturaCompraDetalleParam param3 = new FacturaCompraDetalleParam();
        param3.setIdFacturaCompra(param.getId());
        param3.setFirstResult(0);
        param3.setPageSize(100);
        
        List<FacturaCompraDetalle> list = facades
                .getFacturaCompraDetalleFacade().find(param3);
        
        for (FacturaCompraDetalle item : list) {
            facades.getFacturaCompraDetalleFacade().remove(item);
        }
        
        FacturaCompra factura = find(param.getId());

        remove(factura);

        return Boolean.TRUE;
    }
    
    @Override
    public FacturaCompra updateFileProcess(FacturaCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToUpdateFileProcess(param)) {
            return null;
        }
        
        FacturaCompra item = find(param.getId());
        item.setIdProcesamientoArchivo(param.getIdProcesamientoArchivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public void actualizarSaldoFactura(Integer idFacturaCompra, BigDecimal saldo) {
        
        FacturaCompra facturaCompra = find(idFacturaCompra);
        
        BigDecimal temp = facturaCompra.getSaldo();
        temp = temp.subtract(saldo);
        
        Character pagado = temp.compareTo(BigDecimal.ZERO) > 0 ? 'N' : 'S';
        
        facturaCompra.setSaldo(temp);
        facturaCompra.setPagado(pagado);
        
        edit(facturaCompra);
    }
    
    @Override
    public List<FacturaCompra> find(FacturaCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(FacturaCompra.class);
        Root<FacturaCompra> root = cq.from(FacturaCompra.class);
        Join<FacturaCompra, Empresa> empresa = root.join("empresa");
        Join<FacturaCompra, OrdenCompra> ordenCompra = root.join("ordenCompra", JoinType.LEFT);
        Join<FacturaCompra, ProcesamientoArchivo> procesamientoArchivo = root.join("procesamientoArchivo", JoinType.LEFT);

        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }

        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            predList.add(qb.equal(ordenCompra.get("nroOrdenCompra"), param.getNroOrdenCompra().trim()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }

        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }

        if (param.getNroTimbrado() != null && !param.getNroTimbrado().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroTimbrado"), param.getNroTimbrado().trim()));
        }

        if (param.getIdTipoFactura() != null) {
            predList.add(qb.equal(root.get("idTipoFactura"), param.getIdTipoFactura()));
        }

        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        if (param.getPagado() != null) {
            predList.add(qb.equal(root.get("pagado"), param.getPagado()));
        }

        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroFactura")),
                    String.format("%%%s%%", param.getNroFactura().trim().toUpperCase())));
        }

        if (param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }

        if (param.getHashProcesamientoArchivo() != null && !param.getHashProcesamientoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(procesamientoArchivo.get("hash"), param.getHashProcesamientoArchivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(root.get("id")));

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<FacturaCompra> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaCompra> root = cq.from(FacturaCompra.class);
        Join<FacturaCompra, Empresa> empresa = root.join("empresa");
        Join<FacturaCompra, OrdenCompra> ordenCompra = root.join("ordenCompra", JoinType.LEFT);
        Join<FacturaCompra, ProcesamientoArchivo> procesamientoArchivo = root.join("procesamientoArchivo", JoinType.LEFT);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }

        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            predList.add(qb.equal(ordenCompra.get("nroOrdenCompra"), param.getNroOrdenCompra().trim()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }

        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }

        if (param.getNroTimbrado() != null && !param.getNroTimbrado().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroTimbrado"), param.getNroTimbrado().trim()));
        }

        if (param.getIdTipoFactura() != null) {
            predList.add(qb.equal(root.get("idTipoFactura"), param.getIdTipoFactura()));
        }

        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getPagado() != null) {
            predList.add(qb.equal(root.get("pagado"), param.getPagado()));
        }

        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroFactura")),
                    String.format("%%%s%%", param.getNroFactura().trim().toUpperCase())));
        }

        if (param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }

        if (param.getHashProcesamientoArchivo() != null && !param.getHashProcesamientoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(procesamientoArchivo.get("hash"), param.getHashProcesamientoArchivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
    
    @Override
    public List<RegistroComprobantePojo> generarComprobanteCompra(ReporteComprobanteParam param) {
        
        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and fc.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format("%s and fc.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format("%s and fc.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select fc.ruc, fc.razon_social, "
                + "109 as tipo_comprobante, fc.fecha, fc.nro_timbrado, "
                + "fc.nro_factura, fc.monto_total_10, fc.monto_total_5, "
                + "fc.monto_total_exento, fc.monto_total_factura, "
                + "case when fc.id_tipo_factura = 1 then 2 else 1 end as tipo_factura, "
                + "case when m.codigo in ('PYG','Gs.') then 'N' else 'S' end  as codigo_moneda "
                + "from facturacion.factura_compra fc "
                + "left join comercial.moneda m on m.id = fc.id_moneda "
                + "where %s offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<RegistroComprobantePojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            RegistroComprobantePojo pojo = new RegistroComprobantePojo();
            pojo.setIdentificacionVendedor((String) objects[i++]);
            pojo.setRazonSocialVendedor((String) objects[i++]);
            pojo.setTipoComprobante((Integer) objects[i++]);
            pojo.setFechaEmisionComprobante((Date) objects[i++]);
            pojo.setNumeroTimbrado(new Integer((String) objects[i++]));
            pojo.setNumeroComprobante((String) objects[i++]);
            pojo.setMontoIva10(new BigDecimal(objects[i++] + ""));
            pojo.setMontoIva5(new BigDecimal(objects[i++] + ""));
            pojo.setMontoExento(new BigDecimal(objects[i++] + ""));
            pojo.setMontoTotal(new BigDecimal(objects[i++] + ""));
            pojo.setCondicionVenta((Integer) objects[i++]);
            pojo.setMonedaExtrangera(((String) objects[i++]).charAt(0));
            result.add(pojo);
        }

        return result;
    }

    @Override
    public Integer generarComprobanteCompraSize(ReporteComprobanteParam param) {
        
        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and fc.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format("%s and fc.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format("%s and fc.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select count(fc.*) "
                + "from facturacion.factura_compra fc "
                + "where %s ", where);
        
        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Integer result = object == null
                ? 0
                : ((Number)object).intValue();

        return result;
    }

    @Override
    public List<FacturaPojo> findMonto(FacturaCompraParam param) {

        String where = "true ";

        if (param.getId() != null) {
            where = String.format("%s and f.id = %d", where, param.getId());
        }

        if(param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format("%s and f.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        if (param.getTieneRetencion() != null) {
            where = String.format("%s and %s", where, param.getTieneRetencion()
                    ? "not (rd.monto is null)"
                    : "rd.monto is null");
        }

        String where2 = "true ";

        if (param.getTieneSaldo() != null) {
            where2 = String.format("%s and f.saldo %s", where2,
                    param.getTieneSaldo() ? "> 0" : "<= 0");
        }

        String sql = String.format("with f as (with\n"
                + "nc as (select ncd.id_factura_compra, sum(ncd.monto_total) as monto from facturacion.nota_credito_compra_detalle ncd, facturacion.nota_credito_compra nc where ncd.id_nota_credito_compra = nc.id and nc.anulado = 'N' group by ncd.id_factura_compra),\n"
                //+ "cd as (select cd.id_factura_compra, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura_compra),\n"
                + "rd as (select rd.id_factura_compra, sum(rd.monto_retenido) as monto from facturacion.retencion_compra_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura_compra)\n"
                + "select distinct f.id, f.id_persona, f.nro_timbrado, f.fecha_vencimiento_timbrado, f.id_moneda, m.codigo as codigo_moneda, f.id_tipo_factura,\n"
                + "f.nro_factura, f.fecha, f.razon_social, f.ruc, f.anulado,\n"
                + "f.pagado, f.digital, f.dias_credito, f.monto_iva_5, f.monto_imponible_5, f.monto_total_5,\n"
                + "f.monto_iva_10, f.monto_imponible_10, f.monto_total_10, f.monto_total_exento, f.monto_iva_total, f.monto_imponible_total, f.monto_total_factura, f.cdc,\n"
                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
                //+ "coalesce(cd.monto, 0) as monto_cobro,\n"
                + "coalesce(rd.monto, 0) as monto_retencion,\n"
                + "f.saldo\n"
                + "from facturacion.factura_compra f\n"
                + "left join comercial.moneda m on m.id = f.id_moneda\n"
                + "left join facturacion.tipo_factura tf on tf.id = f.id_tipo_factura\n"
                + "left join nc on nc.id_factura_compra = f.id\n"
                + "left join rd on rd.id_factura_compra = f.id\n"
                //+ "left join cd on cd.id_factura_compra = f.id\n"
                + "where %s) select * from f where %s order by f.fecha desc offset %d limit %d",
                where, where2, param.getFirstResult(), param.getPageSize());

        Query q = getEntityManager().createNativeQuery(sql);

        List<FacturaPojo> result = new ArrayList<>();

        List<Object[]> list = q.getResultList();

        for (Object[] objects : list) {
            int i = 0;
            FacturaPojo pojo = new FacturaPojo();
            pojo.setId((Integer) objects[i++]);
            pojo.setIdPersona((Integer) objects[i++]);
            pojo.setTimbrado((String) objects[i++]);
            pojo.setFechaVencimientoTimbrado((Date) objects[i++]);
            pojo.setIdMoneda((Integer) objects[i++]);
            pojo.setCodigoMoneda((String) objects[i++]);
            pojo.setIdTipoFactura((Integer) objects[i++]);
            pojo.setNroFactura((String) objects[i++]);
            pojo.setFecha((Date) objects[i++]);
            pojo.setRazonSocial((String) objects[i++]);
            pojo.setRuc((String) objects[i++]);
            pojo.setAnulado((Character) objects[i++]);
            pojo.setCobrado((Character) objects[i++]);
            pojo.setDigital((Character) objects[i++]);
            pojo.setDiasCredito((Integer) objects[i++]);
            pojo.setMontoIva5((BigDecimal) objects[i++]);
            pojo.setMontoImponible5((BigDecimal) objects[i++]);
            pojo.setMontoTotal5((BigDecimal) objects[i++]);
            pojo.setMontoIva10((BigDecimal) objects[i++]);
            pojo.setMontoImponible10((BigDecimal) objects[i++]);
            pojo.setMontoTotal10((BigDecimal) objects[i++]);
            pojo.setMontoTotalExento((BigDecimal) objects[i++]);
            pojo.setMontoIvaTotal((BigDecimal) objects[i++]);
            pojo.setMontoImponibleTotal((BigDecimal) objects[i++]);
            pojo.setMontoTotalFactura((BigDecimal) objects[i++]);
            pojo.setCdc((String) objects[i++]);
            pojo.setMontoNotaCredito((BigDecimal) objects[i++]);
            //pojo.setMontoCobro((BigDecimal) objects[i++]);
            pojo.setMontoRetencion((BigDecimal) objects[i++]);
            pojo.setSaldo((BigDecimal) objects[i++]);

            result.add(pojo);
        }

        return result;
    }

    @Override
    public Long findMontoSize(FacturaCompraParam param) {

        String where = "true ";

        if (param.getId() != null) {
            where = String.format("%s and f.id = %d", where, param.getId());
        }

        if(param.getIdEmpresa() != null) {
            where = String.format("%s and fc.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format("%s and f.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        if (param.getTieneRetencion() != null) {
            where = String.format("%s and %s", where, param.getTieneRetencion()
                    ? "not (rd.monto is null)"
                    : "rd.monto is null");
        }

        String where2 = "true ";

        if (param.getTieneSaldo() != null) {
            where2 = String.format("%s and f.saldo %s", where2,
                    param.getTieneSaldo() ? "> 0" : "<= 0");
        }

        String sql = String.format("with f as (with\n"
                + "nc as (select ncd.id_factura_compra, sum(ncd.monto_total) as monto from facturacion.nota_credito_compra_detalle ncd, facturacion.nota_credito_compra nc where ncd.id_nota_credito_compra = nc.id and nc.anulado = 'N' group by ncd.id_factura_compra),\n"
                //+ "cd as (select cd.id_factura_compra, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura_compra),\n"
                + "rd as (select rd.id_factura_compra, sum(rd.monto_retenido) as monto from facturacion.retencion_compra_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura_compra)\n"
                + "select f.id, f.id_persona, f.id_moneda, m.codigo as codigo_moneda, f.id_tipo_factura,\n"
                + "f.nro_factura, f.fecha, f.razon_social, f.ruc, f.anulado,\n"
                + "f.pagado, f.digital, f.dias_credito, f.monto_iva_5, f.monto_imponible_5, f.monto_total_5,\n"
                + "f.monto_iva_10, f.monto_imponible_10, f.monto_total_10, f.monto_total_exento, f.monto_iva_total, f.monto_imponible_total, f.monto_total_factura, f.cdc,\n"
                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
                //+ "coalesce(cd.monto, 0) as monto_cobro,\n"
                + "coalesce(rd.monto, 0) as monto_retencion,\n"
                + "f.saldo\n"
                + "from facturacion.factura_compra f\n"
                + "left join comercial.moneda m on m.id = f.id_moneda\n"
                + "left join facturacion.tipo_factura tf on tf.id = f.id_tipo_factura\n"
                + "left join nc on nc.id_factura_compra = f.id\n"
                + "left join rd on rd.id_factura_compra = f.id\n"
                //+ "left join cd on cd.id_factura_compra = f.id\n"
                + "where %s) select count(*) from f where %s", where, where2);

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
    
    @Override
    public byte[] getPdfReporteCompra(FacturaCompraParam param, UserInfoImpl userInfo) {

        if (!param.isValidToGetReporteVenta()) {
            return null;
        }

        byte[] result = null;

        Closer closer = Closer.instance();

        try {
            
            param.setFirstResult(0);
            param.setPageSize(100000);

            List<FacturaCompra> facturas = facades.getFacturaCompraFacade().find(param);

            ReporteCompraParam params = facades.getReportUtils().getReporteCompraParam(closer, param.getIdEmpresa(), param.getFechaDesde(), param.getFechaHasta(), facturas);

            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(userInfo.getIdEmpresa(), "TEMPLATE_REPORTE_COMPRA");
            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);

            if (isNull(is)) {
                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el reporte de compra"));
            } else {
                
                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                configuration.setOnePagePerSheet(false);
                configuration.setDetectCellType(false);
                //borrar espacio entre filas
                configuration.setRemoveEmptySpaceBetweenRows(true);
                //borrar espacio entre columnas
                configuration.setRemoveEmptySpaceBetweenColumns(true);
                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
                configuration.setIgnoreCellBorder(false);
                configuration.setWhitePageBackground(false);

                result = JasperReporteGenerator.exportReportToXlsxBytes(params, is, configuration);
            }

        } finally {
            closer.close();
        }

        return result;
    }
}
