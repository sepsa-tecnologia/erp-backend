/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.pojos;

import java.math.BigInteger;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class UsuarioLocalPojo {

    public UsuarioLocalPojo() {
    }

    public UsuarioLocalPojo(Integer id, Integer idUsuario, String usuario,
            Integer idLocal, BigInteger gln, String idExterno, String local,
            Character activo) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idLocal = idLocal;
        this.gln = gln;
        this.idExterno = idExterno;
        this.local = local;
        this.activo = activo;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    private String usuario;
    
    /**
     * Identificador de local
     */
    private Integer idLocal;
    
    /**
     * GLN
     */
    private BigInteger gln;
    
    /**
     * Identificador externo
     */
    private String idExterno;
    
    /**
     * Local
     */
    private String local;
    
    /**
     * Activo
     */
    private Character activo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public BigInteger getGln() {
        return gln;
    }

    public void setGln(BigInteger gln) {
        this.gln = gln;
    }

    public void setIdExterno(String idExterno) {
        this.idExterno = idExterno;
    }

    public String getIdExterno() {
        return idExterno;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
