/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionProducto;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionProductoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LiquidacionProductoFacade", mappedName = "LiquidacionProductoFacade")
@Local(LiquidacionProductoFacade.class)
public class LiquidacionProductoFacadeImpl extends FacadeImpl<LiquidacionProducto, LiquidacionProductoParam> implements LiquidacionProductoFacade {

    public LiquidacionProductoFacadeImpl() {
        super(LiquidacionProducto.class);
    }

    public Boolean validToCreate(LiquidacionProductoParam param) {
        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        LiquidacionProductoParam param1 = new LiquidacionProductoParam();
        param1.setIdProducto(param.getIdProducto());
        param1.setIdCliente(param.getIdCliente());
        Long size = findSize(param1);

        if (size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }

        ProductoCom producto = facades
                .getProductoComFacade()
                .find(param.getIdProducto());

        if (producto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto"));
        }

        if (param.getIdCliente() != null) {
            Cliente cliente = facades
                    .getClienteFacade()
                    .find(param.getIdCliente());

            if (cliente == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(LiquidacionProductoParam param) {
        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        LiquidacionProductoParam param1 = new LiquidacionProductoParam();
        param1.setIdProducto(param.getIdProducto());
        param1.setIdCliente(param.getIdCliente());
        param1.setFirstResult(0);
        param1.setPageSize(1);
        List<LiquidacionProducto> list = find(param1);

        if (list == null || list.isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un registro"));
        } else {
            param.setId(list.get(0).getId());
        }

        ProductoCom producto = facades
                .getProductoComFacade()
                .find(param.getIdProducto());

        if (producto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto"));
        }

        if (param.getIdCliente() != null) {
            Cliente cliente = facades
                    .getClienteFacade()
                    .find(param.getIdCliente());

            if (cliente == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente"));
            }
        }

        return !param.tieneErrores();
    }

    public LiquidacionProducto create(LiquidacionProductoParam param) {

        if (!validToCreate(param)) {
            return null;
        }

        LiquidacionProducto item = new LiquidacionProducto();
        item.setIdProducto(param.getIdProducto());
        item.setIdCliente(param.getIdCliente());
        item.setPlazo(param.getPlazo());
        item.setExtension(param.getExtension());
        item.setFacturasPendientes(param.getFacturasPendientes());
        item.setActivo(param.getActivo());

        create(item);

        return item;
    }

    public LiquidacionProducto edit(LiquidacionProductoParam param) {

        if (!validToEdit(param)) {
            return null;
        }

        LiquidacionProducto item = find(param.getId());
        item.setIdProducto(param.getIdProducto());
        item.setIdCliente(param.getIdCliente());
        item.setPlazo(param.getPlazo());
        item.setExtension(param.getExtension());
        item.setFacturasPendientes(param.getFacturasPendientes());
        item.setActivo(param.getActivo());

        edit(item);

        return item;
    }

    public LiquidacionProducto editarOCrear(LiquidacionProductoParam param) {

        LiquidacionProducto item = edit(param);

        if (item == null) {
            item = create(param);
        }

        return item;
    }

    @Override
    public List<LiquidacionProducto> find(LiquidacionProductoParam param) {

        String where = "true";

        if (param.getId() != null) {
            where = String.format("%s and lp.id = %d", where, param.getId());
        }

        if (param.getIdProducto() != null) {
            where = String.format("%s and lp.id_producto = %d", where, param.getIdProducto());
        }

        if (param.getIdCliente() != null) {
            where = String.format("%s and lp.id_cliente = %d", where, param.getIdCliente());
        }

        if (param.getActivo() != null) {
            where = String.format("%s and lp.activo = '%s'", where, param.getActivo());
        }

        String sql = String.format("select lp.*"
                + " from facturacion.liquidacion_producto lp"
                + " left join comercial.cliente c on c.id_cliente = lp.id_cliente"
                + " left join comercial.producto p on p.id = lp.id_producto"
                + " where %s order by lp.estado, lp.id_producto, lp.id_cliente"
                + " offset %d limit %d", where,
                param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, LiquidacionProducto.class);

        List<LiquidacionProducto> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(LiquidacionProductoParam param) {

        String where = "true";

        if (param.getId() != null) {
            where = String.format("%s and lp.id = %d", where, param.getId());
        }

        if (param.getIdProducto() != null) {
            where = String.format("%s and lp.id_producto = %d", where, param.getIdProducto());
        }

        if (param.getIdCliente() != null) {
            where = String.format("%s and lp.id_cliente = %d", where, param.getIdCliente());
        }

        if (param.getActivo() != null) {
            where = String.format("%s and lp.activo = '%s'", where, param.getActivo());
        }

        String sql = String.format("select count(lp.id)"
                + " from facturacion.liquidacion_producto lp"
                + " left join comercial.cliente c on c.id_cliente = lp.id_cliente"
                + " left join comercial.producto p on p.id = lp.id_producto"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number) object).longValue();

        return result;
    }
}
