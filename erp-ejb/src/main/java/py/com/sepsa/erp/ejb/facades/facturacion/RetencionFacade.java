/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.Retencion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface RetencionFacade extends Facade<Retencion, RetencionParam, UserInfoImpl> {

    public Retencion anular(RetencionParam param, UserInfoImpl userInfo);
    
}