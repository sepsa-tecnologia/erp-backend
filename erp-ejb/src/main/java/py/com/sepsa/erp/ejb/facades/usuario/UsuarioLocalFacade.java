/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioLocal;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioLocalParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface UsuarioLocalFacade extends Facade<UsuarioLocal, UsuarioLocalParam, UserInfoImpl> {

    public Boolean validToCreate(UsuarioLocalParam param);

    public Boolean validToEdit(UsuarioLocalParam param);

    public List<UsuarioLocal> findRelacionados(UsuarioLocalParam param);

    public Long findRelacionadosSize(UsuarioLocalParam param);

    public void asociarMasivo(UsuarioLocalParam param, UserInfoImpl userInfo);
    
}
