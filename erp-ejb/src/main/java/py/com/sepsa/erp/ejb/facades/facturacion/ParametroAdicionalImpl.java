/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.ParametroAdicional;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ParametroAdicionalParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "ParametroAdicionalFacade", mappedName = "ParametroAdicionalFacade")
@Local(ParametroAdicionalFacade.class)
@Log4j2
public class ParametroAdicionalImpl extends FacadeImpl<ParametroAdicional, ParametroAdicionalParam> implements ParametroAdicionalFacade {

    public ParametroAdicionalImpl() {
        super(ParametroAdicional.class);
    }

    /**
     * Verifica si el objeto es válido para editar
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(ParametroAdicionalParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        ParametroAdicional parametroAdicional = facades.getParametroAdicionalFacade().find(param.getId());

        if (parametroAdicional == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de parametro adicional"));
        }

        return !param.tieneErrores();
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(ParametroAdicionalParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        return !param.tieneErrores();
    }

    @Override
    public ParametroAdicional create(ParametroAdicionalParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        ParametroAdicional parametroAdicional = new ParametroAdicional();
        parametroAdicional.setIdEmpresa(userInfo.getIdEmpresa());
        parametroAdicional.setCodigo(param.getCodigo());
        parametroAdicional.setActivo(param.getActivo());
        parametroAdicional.setDescripcion(param.getDescripcion());
        parametroAdicional.setCodigoTipoParametroAdicional(param.getCodigoTipoParametroAdicional());
        parametroAdicional.setCodigoTipoDatoParametroAdicional(param.getCodigoTipoDatoParametroAdicional());
        parametroAdicional.setMandatorio(param.getMandatorio());
        parametroAdicional.setVisible(param.getVisible());
        parametroAdicional.setValorDefecto(param.getValorDefecto());
        parametroAdicional.setDatosAdicionales(param.getDatosAdicionales());
        parametroAdicional.setFechaInsercion(Calendar.getInstance().getTime());

        create(parametroAdicional);

        return parametroAdicional;
    }

    /**
     * Edita una instancia de documento
     *
     * @param param parámetros
     * @return Instancia
     */
    public ParametroAdicional edit(ParametroAdicionalParam param) {

        if (!validToEdit(param)) {
            return null;
        }

        ParametroAdicional parametroAdicional = facades.getParametroAdicionalFacade().find(param.getId());
        parametroAdicional.setActivo(param.getActivo());
        parametroAdicional.setVisible(param.getVisible());
        parametroAdicional.setDescripcion(param.getDescripcion());
        edit(parametroAdicional);

        return parametroAdicional;
    }

    @Override
    public List<ParametroAdicional> find(ParametroAdicionalParam param) {

        AbstractFind find = new AbstractFind(ParametroAdicional.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, ParametroAdicionalParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<ParametroAdicional> root = cq.from(ParametroAdicional.class);
        Join<ParametroAdicional, Empresa> empresa = root.join("empresa");

        find.addPath("root", root);
        find.addPath("empresa", empresa);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getVisible() != null) {
            predList.add(qb.equal(root.get("visible"), param.getVisible()));
        }

        if (param.getMandatorio() != null) {
            predList.add(qb.equal(root.get("mandatorio"), param.getMandatorio()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getCodigoTipoDatoParametroAdicional() != null && !param.getCodigoTipoDatoParametroAdicional().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoTipoDatoParametroAdicional"), param.getCodigoTipoDatoParametroAdicional().trim()));
        }

        if (param.getCodigoTipoParametroAdicional() != null && !param.getCodigoTipoParametroAdicional().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoTipoParametroAdicional"), param.getCodigoTipoParametroAdicional().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ParametroAdicionalParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ParametroAdicional> root = cq.from(ParametroAdicional.class);
        Join<ParametroAdicional, Empresa> empresa = root.join("empresa");

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getVisible() != null) {
            predList.add(qb.equal(root.get("visible"), param.getVisible()));
        }

        if (param.getMandatorio() != null) {
            predList.add(qb.equal(root.get("mandatorio"), param.getMandatorio()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getCodigoTipoDatoParametroAdicional() != null && !param.getCodigoTipoDatoParametroAdicional().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoTipoDatoParametroAdicional"), param.getCodigoTipoDatoParametroAdicional().trim()));
        }

        if (param.getCodigoTipoParametroAdicional() != null && !param.getCodigoTipoParametroAdicional().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoTipoParametroAdicional"), param.getCodigoTipoParametroAdicional().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
