/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.time.DateUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.NaturalezaClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoOperacionCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCuotaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoCambioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TipoCambioPojo;
import py.com.sepsa.erp.ejb.entities.info.PersonaEmail;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefono;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaTelefonoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Antonella Lucero
 */
public class FacturaUtils {

    // Patrón para validar el email
    private static final Pattern MAIL_PATTERN = Pattern.compile("^([0-9a-zA-Z#$%]([-.\\w]*[0-9a-zA-Z#$%'\\.\\-_])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$");

    public static List<FacturaParam> convertToList(Facades facades, @MultipartForm FacturaParam param) {
        List<FacturaParam> lista = new ArrayList<>();
        try {

            List<FacturaParam> fpl = new ArrayList<>();

            InputStreamReader input = new InputStreamReader(new BOMInputStream(new ByteArrayInputStream(param.getUploadedFileBytes()), false), StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;

            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                Pattern pattern = Pattern.compile(";|,");
                Matcher mather = pattern.matcher(line);

                if (mather.find() == true) {
                    lineas.add(line);
                }
            }

            for (int i = 0; i < lineas.size(); i++) {
                FacturaParam fInicial = new FacturaParam();
                int errores = 0;
                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    String[] stringArr = lineas.get(i).split(";|,");
                    String tipoLinea = stringArr.length <= 0 ? "" : stringArr[0];
                    String timb = stringArr.length <= 1 ? "" : stringArr[1];
                    String nroF = stringArr.length <= 2 ? "" : stringArr[2];
                    if (timb != null && !timb.trim().isEmpty()) {
                        fInicial.setTimbrado(timb);
                    } else {
                        lista.add(fInicial);
                        fInicial.setTimbrado("-");
                        fInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de timbrado de la factura"));
                        errores += 1;
                    }
                    if (nroF != null && !nroF.trim().isEmpty()) {
                        fInicial.setNroFactura(nroF);
                    } else {
                        fInicial.setNroFactura("-");
                        fInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de factura"));
                        errores += 1;
                    }
                    if (!(tipoLinea != null && !tipoLinea.trim().isEmpty())) {
                        if (!lista.contains(fInicial)) {
                            lista.add(fInicial);
                        }
                        fInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar un Tipo de Línea"));
                        errores += 1;
                        continue;
                    } else {
                        if (errores > 0) {
                            continue;
                        }
                        //Datos de Cabecera
                        String timbrado = null;
                        String nroFactura = null;
                        String fechaFact = null;
                        String codMoneda = null;
                        String nroDocumento = null;
                        String razonSocial = null;
                        String codSeguridad = null;
                        String tipoCambio = null;
                        String codTipoFactura = null;
                        String codTipoCredito = null;
                        String diasCredito = null;
                        String cantidadCuotas = null;
                        String email = null;
                        String observacion = null;
                        //Datos de Detalle
                        String codProducto = null;
                        String porcentajeIva = null;
                        String porcentajeGrav = null;
                        String precioUnitarioConIva = null;
                        String descuentoUnitario = null;
                        String cantidad = null;
                        String concepto = null;
                        String datoAdicional = null;

                        FacturaParam fp = new FacturaParam();
                        FacturaDetalleParam fdp = new FacturaDetalleParam();

                        switch (tipoLinea) {
                            case "1":
                                //TODO cabecera

                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroFactura = (String) stringArr[k];
                                            break;
                                        case 3:
                                            fechaFact = (String) stringArr[k];
                                            break;
                                        case 4:
                                            codMoneda = (String) stringArr[k];
                                            break;
                                        case 5:
                                            nroDocumento = (String) stringArr[k];
                                            break;
                                        case 6:
                                            razonSocial = (String) stringArr[k];
                                            break;
                                        case 7:
                                            codSeguridad = (String) stringArr[k];
                                            break;
                                        case 8:
                                            tipoCambio = (String) stringArr[k];
                                            break;
                                        case 9:
                                            codTipoFactura = (String) stringArr[k];
                                            break;
                                        case 10:
                                            codTipoCredito = (String) stringArr[k];
                                            break;
                                        case 11:
                                            diasCredito = (String) stringArr[k];
                                            break;
                                        case 12:
                                            cantidadCuotas = (String) stringArr[k];
                                            break;
                                        case 13:
                                            email = (String) stringArr[k];
                                            break;
                                    }
                                }
                                fp.setIdEmpresa(param.getIdEmpresa());
                                fp.setIdUsuario(param.getIdUsuario());
                                fp.setIdEncargado(param.getIdUsuario());
                                fp.setTimbrado(timbrado);
                                fp.setNroFactura(nroFactura);
                                if (fp.getNroFactura() != null && !fp.getNroFactura().trim().isEmpty()) {
                                    if (fp.getNroFactura().length() != 15 || !(fp.getNroFactura().matches("^[0-9]{3}[\\-][0-9]{3}[\\-][0-9]{7}$"))) { //validar formato
                                        lista.add(fp);
                                        fp.addError(MensajePojo.createInstance().descripcion("El formato del nro de factura es inválido"));
                                        continue;
                                    }
                                }
                                if (fechaFact != null && !fechaFact.trim().isEmpty()) {
                                    fp.setFecha(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFact));
                                }
                                int j = 0;

                                if (email != null && !email.trim().isEmpty()) {
                                    List<FacturaNotificacionParam> listEmail = new ArrayList();
                                    for (StringTokenizer stringTokenizer = new StringTokenizer(email, "/ ,;"); stringTokenizer.hasMoreTokens(); j++) {
                                        String token = stringTokenizer.nextToken();
                                        FacturaNotificacionParam fnp = new FacturaNotificacionParam();
                                        fnp.setEmail(token);
                                        fnp.setCodigoTipoNotificacion("APROBACION_DTE");
                                        listEmail.add(fnp);
                                    }
                                    fp.setFacturaNotificaciones(listEmail);
                                }

                                fp.setRazonSocial(razonSocial);
                                fp.setRuc(nroDocumento.replace(".", ""));
                                if (fp.getRuc() != null && !fp.getRuc().trim().isEmpty()) {
                                    if (fp.getRuc().contains("-")) {
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if (n != null) {
                                            fp.setIdNaturalezaCliente(n.getId());
                                        }
                                    } else {
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("NO_CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if (n != null) {
                                            fp.setIdNaturalezaCliente(n.getId());
                                        }
                                    }
                                    ClienteParam paramc = new ClienteParam();
                                    paramc.setIdEmpresa(fp.getIdEmpresa());
                                    paramc.setNroDocumentoEq(fp.getRuc());
                                    Cliente cliente = facades.getClienteFacade().findFirst(paramc);
                                    if (cliente != null) {
                                        fp.setIdCliente(cliente.getIdCliente());
                                        fp.setIdNaturalezaCliente(cliente.getIdNaturalezaCliente());

                                        //Agregamos datos de dirección del cliente
                                        Integer nroCasa = Units.execute(0, () -> cliente.getPersona().getDireccion().getNumero());
                                        String direccion = Units.execute(() -> cliente.getPersona().getDireccion().getDireccion());
                                        Integer idDepartamento = Units.execute(() -> cliente.getPersona().getDireccion().getIdDepartamento());
                                        Integer idDistrito = Units.execute(() -> cliente.getPersona().getDireccion().getIdDistrito());
                                        Integer idCiudad = Units.execute(() -> cliente.getPersona().getDireccion().getIdCiudad());

                                        //Validamos datos de dirección del cliente
                                        if (!isNull(nroCasa) && !isNullOrEmpty(direccion)
                                                && !isNull(idDepartamento)
                                                && !isNull(idDistrito)
                                                && !isNull(idCiudad)) {
                                            fp.setNroCasa(nroCasa);
                                            fp.setDireccion(direccion);
                                            fp.setIdDepartamento(idDepartamento);
                                            fp.setIdDistrito(idDistrito);
                                            fp.setIdCiudad(idCiudad);
                                        }

                                        //Agregamos datos de telefono del cliente
                                        PersonaTelefonoParam ptparam = new PersonaTelefonoParam();
                                        ptparam.setIdEmpresa(fp.getIdEmpresa());
                                        ptparam.setIdPersona(cliente.getIdCliente());
                                        PersonaTelefono pt = facades.getPersonaTelefonoFacade().findFirst(ptparam);
                                        String telefono = Units.execute(() -> pt.getTelefono().getPrefijo() + pt.getTelefono().getNumero());

                                        //Validamos datos de telefono del cliente
                                        if (!isNullOrEmpty(telefono) && telefono.trim().length() >= 6) {
                                            fp.setTelefono(telefono);
                                        }

                                        //Agregamos datos de email del cliente
                                        PersonaEmailParam peparam = new PersonaEmailParam();
                                        peparam.setIdEmpresa(fp.getIdEmpresa());
                                        peparam.setIdPersona(cliente.getIdCliente());
                                        PersonaEmail pe = facades.getPersonaEmailFacade().findFirst(peparam);
                                        String correo = Units.execute(() -> pe.getEmail().getEmail());

                                        //Validamos datos de email del cliente
                                        if (!isNullOrEmpty(correo) && MAIL_PATTERN.matcher(correo).find()) {
                                            fp.setEmail(correo);
                                        }
                                    }
                                }
                                UsuarioParam uparam = new UsuarioParam();
                                uparam.setId(param.getIdUsuario());
                                UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);
                                fp.setIdUsuario(usuario.getId());
                                fp.setCodSeguridad(codSeguridad);
                                if (codMoneda != null && !codMoneda.trim().isEmpty()) {
                                    Moneda moneda = facades.getMonedaFacade().find(fp.getIdMoneda(), codMoneda);
                                    if (moneda == null) {
                                        lista.add(fp);
                                        fp.addError(MensajePojo.createInstance().descripcion("No existe la moneda"));
                                        continue;
                                    } else {
                                        fp.setIdMoneda(moneda.getId());
                                        fp.setCodigoMoneda(moneda.getCodigo());
                                        if (fp.getCodigoMoneda().equalsIgnoreCase("PYG")) {
                                            fp.setIdTipoCambio(null);
                                        } else {
                                            TipoCambioParam cambio = new TipoCambioParam();
                                            cambio.setCodigoMoneda(fp.getCodigoMoneda());
                                            if (tipoCambio != null && !tipoCambio.trim().isEmpty()) {
                                                try {
                                                    cambio.setCompra(new BigDecimal(tipoCambio.trim().replace(" ", "")));
                                                } catch (NumberFormatException e) {
                                                    String camb = tipoCambio.replace("\"", "").trim().replace(" ", "");

                                                    DecimalFormat decimalFormat = new DecimalFormat();
                                                    decimalFormat.setParseBigDecimal(true);
                                                    Number parsedCambio = decimalFormat.parse(camb);
                                                    BigDecimal bdCambio = (BigDecimal) parsedCambio;

                                                    cambio.setCompra(bdCambio);
                                                    cambio.setVenta(bdCambio);
                                                }
                                                cambio.setActivo('S');
                                                TipoCambioPojo tipoCambioM = facades.getTipoCambioFacade().findFirstPojo(cambio);
                                                if (tipoCambioM != null) {
                                                    fp.setIdTipoCambio(tipoCambioM.getId());
                                                } else {
                                                    lista.add(fp);
                                                    fp.addError(MensajePojo.createInstance().descripcion("Tipo de Cambio Inexistente"));
                                                    continue;
                                                }
                                            }

                                        }
                                    }
                                }
                                if (codTipoFactura != null && !codTipoFactura.trim().isEmpty()) {
                                    fp.setCodigoTipoFactura(codTipoFactura);
                                    if (fp.getCodigoTipoFactura() != null && fp.getCodigoTipoFactura().equalsIgnoreCase("CONTADO")) {
                                        if (fp.getFecha() != null) {
                                            fp.setFechaVencimiento(fp.getFecha());
                                        }
                                        fp.setCodigoTipoOperacionCredito(null);
                                        fp.setDiasCredito(null);
                                        fp.setCantidadCuotas(null);
                                    } else if (fp.getCodigoTipoFactura() != null && fp.getCodigoTipoFactura().equalsIgnoreCase("CREDITO")) {
                                        if (codTipoCredito != null && !codTipoCredito.trim().isEmpty()) {
                                            TipoOperacionCredito toc = facades.getTipoOperacionCreditoFacade()
                                                    .find(fp.getIdTipoOperacionCredito(), codTipoCredito);
                                            if (toc != null) {
                                                fp.setIdTipoOperacionCredito(toc.getId());
                                                fp.setCodigoTipoOperacionCredito(toc.getCodigo());

                                                if (fp.getCodigoTipoOperacionCredito().equalsIgnoreCase("PLAZO")) {
                                                    if (fp.getFecha() != null && diasCredito != null && !diasCredito.trim().isEmpty()) {
                                                        fp.setFechaVencimiento(DateUtils.addDays(fp.getFecha(), Integer.parseInt(diasCredito)));
                                                        fp.setDiasCredito(Integer.parseInt(diasCredito));
                                                    }
                                                } else if (fp.getCodigoTipoOperacionCredito().equalsIgnoreCase("CUOTA")) {
                                                    if (cantidadCuotas != null && !cantidadCuotas.trim().isEmpty()) {
                                                        fp.setCantidadCuotas(Integer.parseInt(cantidadCuotas));
                                                        fp.setFechaVencimiento(fp.getFecha());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                fp.setMontoTotalDescuentoGlobal(BigDecimal.ZERO);
                                TalonarioPojo talonario = null;
                                if (fp.getNroFactura() != null && !fp.getNroFactura().trim().isEmpty() && fp.getTimbrado() != null && !fp.getTimbrado().trim().isEmpty()) {
                                    TalonarioParam param2 = new TalonarioParam();
                                    param2.setIdEmpresa(fp.getIdEmpresa());
                                    param2.setTimbrado(fp.getTimbrado());
                                    param2.setActivo('S');
                                    param2.setNroSucursal(fp.getNroFactura().substring(0, 3));
                                    param2.setNroPuntoVenta(fp.getNroFactura().substring(4, 7));
                                    param2.setIdTipoDocumento(1);
                                    talonario = facades.getTalonarioFacade().findFirstPojo(param2);
                                    if (talonario != null) {
                                        fp.setIdTalonario(talonario.getId());
                                        fp.setDigital(talonario.getDigital());
                                    } else {
                                        lista.add(fp);
                                        fp.addError(MensajePojo.createInstance().descripcion("Nro de timbrado, nro de sucursal y nro de punto de venta inexistentes"));
                                        continue;
                                    }
                                }
                                fp.setArchivoSet('S');
                                fp.setIdTipoTransaccion(3);
                                fpl.add(fp);
                                break;
                            case "2":
                                //TODO detalle
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroFactura = (String) stringArr[k];
                                            break;
                                        case 3:
                                            codProducto = (String) stringArr[k];
                                            break;
                                        case 4:
                                            porcentajeIva = (String) stringArr[k];
                                            break;
                                        case 5:
                                            porcentajeGrav = (String) stringArr[k];
                                            break;
                                        case 6:
                                            precioUnitarioConIva = (String) stringArr[k];
                                            break;
                                        case 7:
                                            descuentoUnitario = (String) stringArr[k];
                                            break;
                                        case 8:
                                            cantidad = (String) stringArr[k];
                                            break;
                                        case 9:
                                            concepto = (String) stringArr[k];
                                            break;
                                        case 10:
                                            datoAdicional = (String) stringArr[k];
                                            break;
                                    }
                                }
                                //guardar el detalle a la factura correspondiente
                                boolean coincide = false;
                                for (FacturaParam f : fpl) {
                                    if (f.getTimbrado() != null && !f.getTimbrado().trim().isEmpty() && timbrado != null && !timbrado.trim().isEmpty() && f.getNroFactura() != null && !f.getNroFactura().trim().isEmpty() && nroFactura != null && !nroFactura.trim().isEmpty()) {
                                        if (f.getTimbrado().equalsIgnoreCase(timbrado) && f.getNroFactura().equalsIgnoreCase(nroFactura)) {
                                            f.setDescripcion(concepto);
                                            f.getFacturaDetalles().add(fdp);

                                            coincide = true;
                                            if (!lista.contains(f)) {
                                                lista.add(f);
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (coincide == false) {
                                    FacturaParam fError = new FacturaParam();
                                    if (nroFactura != null && !nroFactura.trim().isEmpty()) {
                                        fError.setNroFactura(nroFactura);
                                    } else {
                                        fError.setNroFactura("-");
                                    }
                                    if (timbrado != null && !timbrado.trim().isEmpty()) {
                                        fError.setTimbrado(timbrado);
                                    } else {
                                        fError.setTimbrado("-");
                                    }
                                    lista.add(fError);
                                    fError.addError(MensajePojo.createInstance().descripcion("El detalle no coincide con ninguna cabecera"));
                                    continue;
                                }
                                fdp.setIdEmpresa(param.getIdEmpresa());
                                fdp.setNroLinea(2);
                                fdp.setNroFactura(nroFactura);
                                if (fdp.getNroFactura() != null) {
                                    if (fdp.getNroFactura().length() != 15 || !(fdp.getNroFactura().matches("^[0-9]{3}[\\-][0-9]{3}[\\-][0-9]{7}$"))) {
                                        fdp.addError(MensajePojo.createInstance().descripcion("El formato del nro de factura del detalle es inválido"));
                                        continue;
                                    }
                                }
                                ProductoParam prod = new ProductoParam();
                                if (codProducto != null && !codProducto.trim().isEmpty()) {
                                    prod.setCodigoInternoEq(codProducto);
                                    prod.setIdEmpresa(fdp.getIdEmpresa());
                                    prod.setActivo('S');
                                    ProductoPojo producto = facades.getProductoFacade().findFirstPojo(prod);
                                    if (producto == null) {
                                        fdp.addError(MensajePojo.createInstance().descripcion("Producto no encontrado, verificar código interno"));
                                        continue;
                                    } else {
                                        fdp.setIdProducto(producto.getId());
                                    }
                                } else {
                                    fdp.addError(MensajePojo.createInstance().descripcion("Se debe indicar el código interno del producto"));
                                    continue;
                                }
                                fdp.setDescripcion(concepto);
                                try {
                                    byte[] bytesDecodificados = Base64.getDecoder().decode(datoAdicional);
                                    String decoded = new String(bytesDecodificados);
                                    fdp.setDatoAdicional(decoded);
                                } catch (Exception e) {
                                    fdp.setDatoAdicional(datoAdicional);
                                }

                                if (precioUnitarioConIva != null && cantidad != null && descuentoUnitario != null) {
                                    if (!precioUnitarioConIva.trim().isEmpty() && !cantidad.trim().isEmpty() && !descuentoUnitario.trim().isEmpty()) {
                                        try {
                                            fdp.setPrecioUnitarioConIva(new BigDecimal(precioUnitarioConIva.trim().replace(" ", "")));
                                            fdp.setCantidad(new BigDecimal(cantidad.trim().replace(" ", "")));
                                            fdp.setMontoDescuentoParticular(new BigDecimal(descuentoUnitario.trim().replace(" ", "")));
                                        } catch (NumberFormatException e) {
                                            String precioU = precioUnitarioConIva.replace("\"", "").trim().replace(" ", "");
                                            String cantidad1 = cantidad.replace("\"", "").trim().replace(" ", "");
                                            String descuento1 = descuentoUnitario.replace("\"", "").trim().replace(" ", "");

                                            DecimalFormat decimalFormat = new DecimalFormat();
                                            decimalFormat.setParseBigDecimal(true);

                                            Number parsedPrecio = decimalFormat.parse(precioU);
                                            BigDecimal bdPrecio = (BigDecimal) parsedPrecio;

                                            Number parsedCantidad = decimalFormat.parse(cantidad1);
                                            BigDecimal bdCantidad = (BigDecimal) parsedCantidad;

                                            Number parsedDescuento = decimalFormat.parse(descuento1);
                                            BigDecimal bdDescuento = (BigDecimal) parsedDescuento;

                                            fdp.setPrecioUnitarioConIva(bdPrecio);
                                            fdp.setCantidad(bdCantidad);
                                            fdp.setMontoDescuentoParticular(bdDescuento);
                                        }
                                        fdp.setPorcentajeIva(Integer.parseInt(porcentajeIva));
                                        if (fdp.getMontoDescuentoParticular() != null && (((fdp.getMontoDescuentoParticular().compareTo(BigDecimal.ZERO) == 1) || (fdp.getMontoDescuentoParticular().compareTo(BigDecimal.ZERO) == 0)) && (fdp.getMontoDescuentoParticular().compareTo(fdp.getPrecioUnitarioConIva()) == -1))) {
                                            BigDecimal aux = fdp.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                                            BigDecimal descuentoPorc = aux.divide(fdp.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                                            fdp.setDescuentoParticularUnitario(descuentoPorc);

                                        } else {
                                            fdp.setDescuentoParticularUnitario(BigDecimal.ZERO);
                                            fdp.setMontoDescuentoParticular(BigDecimal.ZERO);
                                        }
                                        BigDecimal precioUnitarioConIVA = fdp.getPrecioUnitarioConIva().subtract(fdp.getDescuentoParticularUnitario());
                                        if (fdp.getPorcentajeIva() != null) {
                                            if (fdp.getPorcentajeIva() == 5) {
                                                BigDecimal iva = new BigDecimal("1.05");
                                                BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(iva, 5, RoundingMode.HALF_UP);
                                                fdp.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
                                                fdp.setMontoIva(precioUnitarioConIVA.subtract(fdp.getPrecioUnitarioSinIva()));
                                            } else if (fdp.getPorcentajeIva() == 10) {
                                                BigDecimal iva = new BigDecimal("1.1");
                                                BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(iva, 5, RoundingMode.HALF_UP);
                                                fdp.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
                                                fdp.setMontoIva(precioUnitarioConIVA.subtract(fdp.getPrecioUnitarioSinIva()));
                                            } else if (fdp.getPorcentajeIva() == 0) {
                                                fdp.setPrecioUnitarioSinIva(BigDecimal.ZERO);
                                                fdp.setMontoIva(BigDecimal.ZERO);
                                            }
                                        }
                                        if (porcentajeGrav != null) {
                                            fdp.setPorcentajeGravada(new BigDecimal(porcentajeGrav));
                                        } else {
                                            fdp.setPorcentajeGravada(new BigDecimal(100));
                                        }
                                        BigDecimal _montoTotal = fdp.getPrecioUnitarioConIva().subtract(fdp.getMontoDescuentoParticular()).multiply(fdp.getCantidad());
                                        BigDecimal _montoImponible = _montoTotal.multiply(fdp.getPorcentajeGravada()).multiply(new BigDecimal(100))
                                                .divide(new BigDecimal(10000).add(new BigDecimal(fdp.getPorcentajeIva()).multiply(fdp.getPorcentajeGravada())), 8, RoundingMode.HALF_UP);
                                        BigDecimal _montoIva = _montoImponible.multiply(new BigDecimal(fdp.getPorcentajeIva())).divide(new BigDecimal(100), 8, RoundingMode.HALF_UP);
                                        fdp.setMontoIva(_montoIva);
                                        fdp.setMontoImponible(_montoImponible);
                                        fdp.setMontoTotal(_montoTotal);
                                        fdp.setDescuentoGlobalUnitario(BigDecimal.ZERO);
                                        fdp.setMontoDescuentoGlobal(BigDecimal.ZERO);
                                        fdp.setMontoExentoGravado(BigDecimal.ZERO);
                                    }
                                }
                                break;
                            case "3":
                                //TODO detalle
                                String obs = "";
                                for (int m = 3; m < stringArr.length; m++) {
                                    obs = obs + stringArr[m] + ",";
                                }
                                obs = obs.substring(0, obs.length() - 1); //elimina la última coma
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroFactura = (String) stringArr[k];
                                            break;
                                        case 3:
                                            observacion = obs;
                                            break;
                                    }
                                }
                                //guardar el detalle a la factura correspondiente
                                boolean coincide3 = false;
                                for (FacturaParam f : fpl) {
                                    if (f.getTimbrado() != null && !f.getTimbrado().trim().isEmpty() && timbrado != null && !timbrado.trim().isEmpty() && f.getNroFactura() != null && !f.getNroFactura().trim().isEmpty() && nroFactura != null && !nroFactura.trim().isEmpty()) {
                                        if (f.getTimbrado().equalsIgnoreCase(timbrado) && f.getNroFactura().equalsIgnoreCase(nroFactura)) {
                                            f.setObservacion(observacion);

                                            coincide3 = true;
                                            if (!lista.contains(f)) {
                                                lista.add(f);
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (coincide3 == false) {
                                    FacturaParam fError = new FacturaParam();
                                    if (nroFactura != null && !nroFactura.trim().isEmpty()) {
                                        fError.setNroFactura(nroFactura);
                                    } else {
                                        fError.setNroFactura("-");
                                    }
                                    if (timbrado != null && !timbrado.trim().isEmpty()) {
                                        fError.setTimbrado(timbrado);
                                    } else {
                                        fError.setTimbrado("-");
                                    }
                                    lista.add(fError);
                                    fError.addError(MensajePojo.createInstance().descripcion("El línea 3 no coincide con ninguna cabecera"));
                                    continue;
                                }

                                break;

                            default:
                                if (!lista.contains(fInicial)) {
                                    lista.add(fInicial);
                                }
                                fInicial.addError(MensajePojo.createInstance().descripcion("Tipo de línea únicamente puede ser 1, 2 o 3"));
                                break;
                        }
                    }
                }
            }
            boolean continuar = false;

            for (FacturaParam factura : lista) {
                for (FacturaDetalleParam detalle : factura.getFacturaDetalles()) {
                    continuar = detalle.getCantidad() != null && detalle.getPrecioUnitarioConIva() != null;
                }
                if (continuar) {
                    try {
                        calcularDetalle(factura, factura.getFacturaDetalles());
                    } catch (Exception ex) {
                        factura.addError(MensajePojo.createInstance().descripcion("Error al calcular los montos, verificar la cantidad y precio unitario de la factura"));
                        continue;
                    }

                    if (factura.getCodigoTipoFactura().equalsIgnoreCase("CREDITO") && factura.getCodigoTipoOperacionCredito().equalsIgnoreCase("CUOTA")) {
                        BigDecimal monto = factura.getMontoTotalFactura().divide(BigDecimal.valueOf(factura.getCantidadCuotas()));
                        Date fecha = factura.getFecha();
                        for (int i = 0; i < factura.getCantidadCuotas(); i++) {
                            FacturaCuotaParam cuota = new FacturaCuotaParam();
                            cuota.setMonto(monto);
                            cuota.setFechaVencimiento(fecha);
                            fecha = DateUtils.addDays(fecha, 30);
                            cuota.setIdFactura(factura.getId());
                            cuota.setIdEmpresa(factura.getIdEmpresa());
                            cuota.setIdMoneda(factura.getIdMoneda());
                            factura.getFacturaCuotas().add(cuota);
                        }
                    }
                }
            }

            return lista;
        } catch (Exception ex) {
            return lista;
        }
    }

    private static void calcularDetalle(FacturaParam fact, List<FacturaDetalleParam> listaDetalle) {
        BigDecimal totalDetalle = new BigDecimal("0");
        int i = 0;
        for (FacturaDetalleParam info : listaDetalle) {
            i++;
            info.setNroLinea(i);
            info.setMontoIva(info.getMontoIva());
            info.setMontoImponible(info.getMontoImponible());
            info.setMontoTotal(info.getMontoTotal());
            info.setPrecioUnitarioSinIva(info.getPrecioUnitarioSinIva());
            //info.setMontoDescuentoParticular(info.getMontoDescuentoParticular());
            info.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
            totalDetalle = totalDetalle.add(info.getMontoTotal());
        }
        fact.setMontoTotalFactura(totalDetalle);
        fact.setMontoTotalGuaranies(totalDetalle);
        fact.setMontoTotalDescuentoParticular(BigDecimal.valueOf(0));
        fact.setFacturaDetalles(listaDetalle);
        calcularTotalesGenerales(fact, fact.getFacturaDetalles());
    }

    private static void calcularTotalesGenerales(FacturaParam fact, List<FacturaDetalleParam> listaDetalle) {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");
        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }
        fact.setMontoIva5(montoIva5Acumulador);
        fact.setMontoImponible5(montoImponible5Acumulador);
        fact.setMontoTotal5(montoTotal5Acumulador);
        fact.setMontoIva10(montoIva10Acumulador);
        fact.setMontoImponible10(montoImponible10Acumulador);
        fact.setMontoTotal10(montoTotal10Acumulador);
        fact.setMontoTotalExento(montoExcentoAcumulador);
        fact.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        fact.setMontoIvaTotal(montoIvaTotalAcumulador);
        fact.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
    }

    public Date formatDate(String fechaFact, String formatoFecha) throws ParseException {
        Date formated = null;
        SimpleDateFormat sdf = new SimpleDateFormat(formatoFecha);
        try {
            formated = sdf.parse(fechaFact);
            return formated;
        } catch (ParseException e) {
            return null;
        }
    }

}
