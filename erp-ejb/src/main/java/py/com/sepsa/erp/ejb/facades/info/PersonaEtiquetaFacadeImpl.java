/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.PersonaEtiqueta;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaEtiquetaFacade", mappedName = "PersonaEtiquetaFacade")
@Local(PersonaEtiquetaFacade.class)
public class PersonaEtiquetaFacadeImpl extends FacadeImpl<PersonaEtiqueta, CommonParam> implements PersonaEtiquetaFacade {

    public PersonaEtiquetaFacadeImpl() {
        super(PersonaEtiqueta.class);
    }
}
