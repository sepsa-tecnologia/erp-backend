/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ClienteFacade extends Facade<Cliente, ClienteParam, UserInfoImpl> {
    
       public List<String> altaMasivaClientes(ClienteParam param, UserInfoImpl userInfo);
       
       public List<String> updateClienteMasivo(ClienteParam param, UserInfoImpl userInfo);
}
