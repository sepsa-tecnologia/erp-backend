/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de cliente servicio
 * @author Jonathan D. Bernal Fernández
 */
public class ClienteServicioParam extends CommonParam {
    
    public ClienteServicioParam() {
    }

    public ClienteServicioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Identificador de comercial
     */
    @QueryParam("idComercial")
    private Integer idComercial;
    
    /**
     * Identificador de tipo de negocio
     */
    @QueryParam("idTipoNegocio")
    private Integer idTipoNegocio;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * Producto
     */
    @QueryParam("producto")
    private String producto;
    
    /**
     * Servicio
     */
    @QueryParam("servicio")
    private String servicio;

    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public Integer getIdTipoNegocio() {
        return idTipoNegocio;
    }

    public void setIdTipoNegocio(Integer idTipoNegocio) {
        this.idTipoNegocio = idTipoNegocio;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToCreate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
