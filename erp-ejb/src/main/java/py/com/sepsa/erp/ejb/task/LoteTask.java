/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.facades.Facades;

/**
 *
 * @author Jonathan Bernal
 */
@Startup
@Singleton
@Log4j2
public class LoteTask {

    private static final AtomicBoolean busy = new AtomicBoolean(false);

    @EJB
    protected Facades facades;

    @Schedule(second = "0/5", minute = "*", hour = "*", persistent = false)
    public void task(Timer t) {

        if (!busy.compareAndSet(false, true)) {
            log.info("Existe un hilo de ejecución activo");
            return;
        }

        log.info("Iniciando el procesamiento de lotes ");
        try {
            LoteParam param = new LoteParam();
            param.setCodigoEstado("PENDIENTE");
            param.setFirstResult(0);
            param.setPageSize(1);
            List<Lote> lotes = facades.getLoteFacade().find(param);
            try {
                while (!lotes.isEmpty()) {
                    for (Lote lote : lotes) {
                        try {

                            log.info(String.format("Procesando lote ID:%d, TIPO: %s", lote.getId(), lote.getTipoLote().getCodigo()));

                            switch (lote.getTipoLote().getCodigo()) {
                                case "CARGA_MASIVA_FACTURAS":
                                    facades.getFacturaMasivaTask().procesar(lote);
                                    break;
                                case "CARGA_MASIVA_FACTURAS_ARZA":
                                    facades.getFacturaMasivaArzaTask().procesar(lote);
                                    break;
                                case "CARGA_MASIVA_FACTURAS_GAMBLING":
                                    facades.getFacturaMasivaGamblingTask().procesar(lote);
                                    break;
                                case "CARGA_MASIVA_NOTAS_CREDITO_GAMBLING":
                                    facades.getNotaCreditoMasivaGamblingTask().procesar(lote);
                                    break;
                                case "CARGA_MASIVA_NOTAS_DEBITO_GAMBLING":
                                    facades.getNotaDebitoMasivaGamblingTask().procesar(lote);
                                    break;
                                case "CARGA_MASIVA_NOTAS_REMISION_GAMBLING":
                                    facades.getNotaRemisionMasivaGamblingTask().procesar(lote);
                                    break;
                                default:
                                    log.info(String.format("No se ha podido identificar el TIPO_LOTE%s, ID:%d", lote.getTipoLote().getCodigo(), lote.getId()));
                                    LoteParam loteparam = new LoteParam();
                                    loteparam.setId(lote.getId());
                                    loteparam.setIdEmpresa(lote.getIdEmpresa());
                                    loteparam.setCodigoEstado("NO_IDENTIFICADO");
                                    loteparam.setNotificado(lote.getNotificado());
                                    facades.getLoteFacade().edit(loteparam, null);
                            }

                        } catch (Exception e) {
                            log.fatal("Error al procesar lote", e);
                        }
                    }
                    lotes = facades.getLoteFacade().find(param);
                }
            } catch (Exception e) {
                log.fatal("Error al procesar lotes", e);
            }
            busy.set(false);
        } catch (Throwable e) {
            busy.set(false);
        }
    }
}
