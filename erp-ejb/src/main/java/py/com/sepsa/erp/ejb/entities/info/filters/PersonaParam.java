/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contacto email
 *
 * @author Jonathan
 */
public class PersonaParam extends CommonParam {

    public PersonaParam() {
    }

    public PersonaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de persona
     */
    @QueryParam("idTipoPersona")
    private Integer idTipoPersona;

    /**
     * Código de tipo de persona
     */
    @QueryParam("codigoTipoPersona")
    private String codigoTipoPersona;

    /**
     * Identificador de dirección
     */
    @QueryParam("idDireccion")
    private Integer idDireccion;

    /**
     * Nombre
     */
    @QueryParam("nombre")
    private String nombre;

    /**
     * Apellido
     */
    @QueryParam("apellido")
    private String apellido;

    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;

    /**
     * Nombre fantasia
     */
    @QueryParam("nombreFantasia")
    private String nombreFantasia;

    private ContactoParam contacto;

    private DireccionParam direccion;

    private EmailParam email;
    
    private List<PersonaEmailParam> personaEmails;

    private TelefonoParam telefono;
    
    private List<PersonaTelefonoParam> personaTelefonos;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if(Assertions.isNull(idTipoPersona) && Assertions.isNullOrEmpty(codigoTipoPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de persona"));
        } else if (!Assertions.isNull(idTipoPersona)) {

            if (idTipoPersona.equals(1)) {//Fisica
                if (isNullOrEmpty(nombre)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre"));
                }

                if (isNullOrEmpty(apellido)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el apellido"));
                }
            }

            if (idTipoPersona.equals(2)) {//Juridica
                if (isNullOrEmpty(razonSocial)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere la razón social"));
                }

                if (isNullOrEmpty(nombreFantasia)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre fantasía"));
                }
            }
        } else if (!Assertions.isNullOrEmpty(codigoTipoPersona)) {

            if (codigoTipoPersona.equals("FISICA")) {//Fisica
                if (isNullOrEmpty(nombre)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre"));
                }

                if (isNullOrEmpty(apellido)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el apellido"));
                }
            }

            if (codigoTipoPersona.equals("JURIDICA")) {//Juridica
                if (isNullOrEmpty(razonSocial)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere la razón social"));
                }

                if (isNullOrEmpty(nombreFantasia)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre fantasía"));
                }
            }
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si la persona esta activa"));
        }

        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if (!isNull(email)) {
            email.setIdEmpresa(idEmpresa);
            email.isValidToCreate();
        }
        
        if (!isNullOrEmpty(personaEmails)) {
            for (PersonaEmailParam personaEmail : personaEmails) {
                personaEmail.setIdPersona(0);
                personaEmail.setIdEmpresa(idEmpresa);
                personaEmail.isValidToCreate();
            }
        }

        if (!isNull(telefono)) {
            telefono.setIdEmpresa(idEmpresa);
            telefono.isValidToCreate();
        }

        if (!isNullOrEmpty(personaTelefonos)) {
            for (PersonaTelefonoParam personaTelefono : personaTelefonos) {
                personaTelefono.setIdPersona(0);
                personaTelefono.setIdEmpresa(idEmpresa);
                personaTelefono.isValidToCreate();
            }
        }
        
        if (!isNull(direccion)) {
            direccion.setIdEmpresa(idEmpresa);
            direccion.isValidToCreate();
        }

        if (!isNull(contacto)) {
            contacto.setIdContacto(0);
            contacto.setIdEmpresa(idEmpresa);
            contacto.isValidToCreate();
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador"));
        }

        if(Assertions.isNull(idTipoPersona) && Assertions.isNullOrEmpty(codigoTipoPersona)) {
            
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de persona"));
            
        } else if (!Assertions.isNull(idTipoPersona)) {

            if (idTipoPersona.equals(1)) {//Fisica
                if (isNullOrEmpty(nombre)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre"));
                }

                if (isNullOrEmpty(apellido)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el apellido"));
                }
            }

            if (idTipoPersona.equals(2)) {//Juridica
                if (isNullOrEmpty(razonSocial)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere la razón social"));
                }

                if (isNullOrEmpty(nombreFantasia)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre fantasía"));
                }
            }
        } else if (!Assertions.isNullOrEmpty(codigoTipoPersona)) {

            if (codigoTipoPersona.equals("FISICA")) {//Fisica
                if (isNullOrEmpty(nombre)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre"));
                }

                if (isNullOrEmpty(apellido)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el apellido"));
                }
            }

            if (codigoTipoPersona.equals("JURIDICA")) {//Juridica
                if (isNullOrEmpty(razonSocial)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere la razón social"));
                }

                if (isNullOrEmpty(nombreFantasia)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se requiere el nombre fantasía"));
                }
            }
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si la persona esta activa"));
        }
        
        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if (!isNull(direccion)) {
            direccion.setIdEmpresa(idEmpresa);
            direccion.isValidToCreate();
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(email)) {
            if(email.tieneErrores()) {
                list.addAll(email.getErrores());
            }
        }
        
        if(!isNullOrEmpty(personaEmails)) {
            for (PersonaEmailParam personaEmail : personaEmails) {
                if(personaEmail.tieneErrores()) {
                    list.addAll(personaEmail.getErrores());
                }
            }
        }
        
        if(!isNull(telefono)) {
            if(telefono.tieneErrores()) {
                list.addAll(telefono.getErrores());
            }
        }
        
        if(!isNullOrEmpty(personaTelefonos)) {
            for (PersonaTelefonoParam personaTelefono : personaTelefonos) {
                if(personaTelefono.tieneErrores()) {
                    list.addAll(personaTelefono.getErrores());
                }
            }
        }
        
        if(!isNull(direccion)) {
            if(direccion.tieneErrores()) {
                list.addAll(direccion.getErrores());
            }
        }
        
        if(!isNull(contacto)) {
            if(contacto.tieneErrores()) {
                list.addAll(contacto.getErrores());
            }
        }
        
        return list;
    }
    
    

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(Integer idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCodigoTipoPersona() {
        return codigoTipoPersona;
    }

    public void setCodigoTipoPersona(String codigoTipoPersona) {
        this.codigoTipoPersona = codigoTipoPersona;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public void setNombreFantasia(String nombreFantasia) {
        this.nombreFantasia = nombreFantasia;
    }

    public void setContacto(ContactoParam contacto) {
        this.contacto = contacto;
    }

    public ContactoParam getContacto() {
        return contacto;
    }

    public void setDireccion(DireccionParam direccion) {
        this.direccion = direccion;
    }

    public DireccionParam getDireccion() {
        return direccion;
    }

    public EmailParam getEmail() {
        return email;
    }

    public void setEmail(EmailParam email) {
        this.email = email;
    }

    public TelefonoParam getTelefono() {
        return telefono;
    }

    public void setTelefono(TelefonoParam telefono) {
        this.telefono = telefono;
    }

    public List<PersonaEmailParam> getPersonaEmails() {
        return personaEmails;
    }

    public void setPersonaEmails(List<PersonaEmailParam> personaEmails) {
        this.personaEmails = personaEmails;
    }

    public List<PersonaTelefonoParam> getPersonaTelefonos() {
        return personaTelefonos;
    }

    public void setPersonaTelefonos(List<PersonaTelefonoParam> personaTelefonos) {
        this.personaTelefonos = personaTelefonos;
    }
}
