/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Email;
import py.com.sepsa.erp.ejb.entities.info.TipoEmail;
import py.com.sepsa.erp.ejb.entities.info.filters.EmailParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "EmailFacade", mappedName = "EmailFacade")
@Local(EmailFacade.class)
public class EmailFacadeImpl extends FacadeImpl<Email, EmailParam> implements EmailFacade {

    public EmailFacadeImpl() {
        super(Email.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @return bandera
     */
    @Override
    public Boolean validToCreate(EmailParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoEmail tipoEmail = facades
                .getTipoEmailFacade()
                .find(param.getIdTipoEmail(), param.getCodigoTipoEmail());
        
        if(tipoEmail == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de email"));
        } else {
            param.setIdTipoEmail(tipoEmail.getId());
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param Parámetros
     * @return bandera
     */
    @Override
    public Boolean validToEdit(EmailParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Email email = facades
                .getEmailFacade()
                .find(param.getId());
        
        if(email == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el email"));
        }
        
        TipoEmail tipoEmail = facades
                .getTipoEmailFacade()
                .find(param.getIdTipoEmail());
        
        if(tipoEmail == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de email"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de email
     * @param param Parámetros
     * @param userInfo Identificador de usuario
     * @return Email
     */
    @Override
    public Email create(EmailParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Email email = new Email();
        email.setEmail(param.getEmail());
        email.setIdTipoEmail(param.getIdTipoEmail());
        email.setPrincipal(param.getPrincipal());
        
        create(email);
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id", email.getId() + " ");
        atributos.put("id_tipo_email", email.getIdTipoEmail() + " ");
        atributos.put("email", email.getEmail() + " ");
        atributos.put("principal", email.getPrincipal() + " ");
        facades.getRegistroFacade().create("info", "email", atributos, userInfo);
        
        return email;
    }
    
    /**
     * Edita una instancia de email
     * @param param Parámetros
     * @param userInfo Identificador de usuario
     * @return Email
     */
    @Override
    public Email edit(EmailParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Email email = find(param.getId());
        email.setEmail(param.getEmail());
        email.setIdTipoEmail(param.getIdTipoEmail());
        email.setPrincipal(param.getPrincipal());
        
        edit(email);
        
        Map<String, String> pk = new HashMap();
        pk.put("id", email.getId() + " ");
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id_tipo_email", email.getIdTipoEmail() + " ");
        atributos.put("email", email.getEmail() + " ");
        atributos.put("principal", email.getPrincipal() + " ");
        facades.getRegistroFacade().edit("info", "email", atributos, userInfo, pk);
        
        return email;
    }
    
    /**
     * Obtiene la lista 
     * @param param parámtros
     * @return Lista
     */
    @Override
    public List<Email> find(EmailParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Email> root = cq.from(Email.class);
        cq.select(root);
        cq.orderBy(qb.asc(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdTipoEmail() != null) {
            predList.add(qb.equal(root.get("idTipoEmail"), param.getIdTipoEmail()));
        }

        if (param.getEmail() != null && !param.getEmail().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("email")), String.format("%%%s%%", param.getEmail().trim().toUpperCase())));
        }

        if (param.getPrincipal() != null) {
            predList.add(qb.equal(root.get("principal"), param.getPrincipal()));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Email> emails = q.getResultList();

        return emails;
    }

    /**
     * Obtiene el tamaño de la lista
     * @param param parámetros
     * @return Tamaño de la lista
     */
    public Long findSize(EmailParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Email> root = cq.from(Email.class);
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdTipoEmail() != null) {
            predList.add(qb.equal(root.get("idTipoEmail"), param.getIdTipoEmail()));
        }

        if (param.getEmail() != null && !param.getEmail().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("email")), String.format("%%%s%%", param.getEmail().trim().toUpperCase())));
        }

        if (param.getPrincipal() != null) {
            predList.add(qb.equal(root.get("principal"), param.getPrincipal()));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Long result = ((Number) q.getSingleResult()).longValue();

        return result;
    }
}
