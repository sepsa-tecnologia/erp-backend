/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.entities.proceso.pojos.LotePojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.EnviarNotificacionAdapter;
import py.com.sepsa.erp.ejb.facades.Facades;

/**
 *
 * @author Jonathan Bernal
 */
@Startup
@Singleton
@Log4j2
public class NotificacionResultadoTask {

    @EJB
    protected Facades facades;

    @Schedule(second = "*/30", minute = "*", hour = "*", persistent = false)
    public void task(Timer t) {

        log.info("Iniciando notificación de lotes");

        LoteParam param = new LoteParam();
        param.setCodigoEstado("PROCESADO");
        param.setNotificado('N');
        param.setFirstResult(0);
        param.setPageSize(10);
        List<LotePojo> lotes = facades.getLoteFacade().findPojo(param);
        try {
            while (!lotes.isEmpty()) {
                for (LotePojo lote : lotes) {
                    try {

                        log.info(String.format("Notificando lote ID:%d, TIPO: %s", lote.getId(), lote.getCodigoTipoLote()));

                        String asunto = "Procesamiento de Lote Concluido";
                        String mensaje = String.format("Se ha concluido el procesamiento en lote del archivo <b>%s</b>. "
                                + "Puede acceder al mismo ingresando a la plataforma en el listado de lotes.", lote.getNombreArchivo());

                        EnviarNotificacionAdapter adapter = new EnviarNotificacionAdapter(asunto,
                                mensaje, lote.getEmail(), "MAIL_EDI", "P");
                        adapter.request();

                        LoteParam loteparam = new LoteParam();
                        loteparam.setId(lote.getId());
                        loteparam.setIdEmpresa(lote.getIdEmpresa());
                        loteparam.setCodigoEstado(lote.getCodigoEstado());
                        loteparam.setNotificado('S');
                        facades.getLoteFacade().edit(loteparam, null);

                    } catch (Exception e) {
                        log.fatal("Error al procesar lote", e);
                    }
                }
                lotes = facades.getLoteFacade().findPojo(param);
            }
        } catch (Exception e) {
            log.fatal("Error al procesar lotes", e);
        }
    }
}
