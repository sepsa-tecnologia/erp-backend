/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.Deposito;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.inventario.Inventario;
import py.com.sepsa.erp.ejb.entities.inventario.InventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioDetallePojo;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "InventarioDetalleFacade", mappedName = "InventarioDetalleFacade")
@javax.ejb.Local(InventarioDetalleFacade.class)
public class InventarioDetalleFacadeImp extends FacadeImpl<InventarioDetalle, InventarioDetalleParam> implements InventarioDetalleFacade {

    public InventarioDetalleFacadeImp() {
        super(InventarioDetalle.class);
    }
    
    @Override
    public Boolean validToCreate(InventarioDetalleParam param, boolean validarInventario) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        InventarioParam iparam = new InventarioParam();
        iparam.setId(param.getIdInventario());
        Long size = validarInventario
                ? facades.getInventarioFacade().findSize(iparam)
                : 0;
        if(validarInventario && size <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el inventario"));
        }
        
        InventarioDetalleParam idparam = new InventarioDetalleParam();
        idparam.setIdInventario(param.getIdInventario());
        idparam.setFechaVencimiento(param.getFechaVencimiento());
        idparam.setTieneFechaVencimiento(param.getFechaVencimiento() != null);
        Long idsize = findSize(param);
        
        if(idsize >= 0) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(InventarioDetalleParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        InventarioDetallePojo inventarioDetalle = findPojo(param.getIdInventario(), param.getFechaVencimiento());
        
        if(inventarioDetalle == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
        } else {
            param.setId(inventarioDetalle.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public InventarioDetalle create(InventarioDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        InventarioDetalle result = new InventarioDetalle();
        result.setIdInventario(param.getIdInventario());
        result.setFechaVencimiento(param.getFechaVencimiento());
        result.setCantidad(param.getCantidad());
        
        create(result);
        
        return result;
    }

    @Override
    public InventarioDetalle edit(InventarioDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        InventarioDetalle result = find(param.getId());
        result.setCantidad(param.getCantidad());
        
        edit(result);
        
        return result;
    }

    @Override
    public List<InventarioDetallePojo> findPojo(InventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(InventarioDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idInventario"),
                        getPath("root").get("fechaVencimiento"),
                        getPath("root").get("cantidad"),
                        getPath("inventario").get("idDepositoLogistico"),
                        getPath("depositoLogistico").get("codigo"),
                        getPath("depositoLogistico").get("descripcion"),
                        getPath("inventario").get("idProducto"),
                        getPath("producto").get("descripcion"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("codigoInterno"),
                        getPath("inventario").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public InventarioDetalle find(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, String codigoEstado, Date fechaVencimiento) {
        InventarioDetalleParam param = new InventarioDetalleParam();
        param.setIdDepositoLogistico(idDepositoLogistico);
        param.setIdProducto(idProducto);
        param.setIdEstado(idEstado);
        param.setCodigoEstado(codigoEstado);
        param.setFechaVencimiento(fechaVencimiento);
        param.setTieneFechaVencimiento(fechaVencimiento != null);
        return findFirst(param);
    }
    
    @Override
    public InventarioDetalle find(Integer idInventario, Date fechaVencimiento) {
        InventarioDetalleParam param = new InventarioDetalleParam();
        param.setIdInventario(idInventario);
        param.setFechaVencimiento(fechaVencimiento);
        param.setTieneFechaVencimiento(fechaVencimiento != null);
        return findFirst(param);
    }
    
    @Override
    public InventarioDetallePojo findPojo(Integer idInventario, Date fechaVencimiento) {
        InventarioDetalleParam param = new InventarioDetalleParam();
        param.setIdInventario(idInventario);
        param.setFechaVencimiento(fechaVencimiento);
        param.setTieneFechaVencimiento(fechaVencimiento != null);
        param.setListadoPojo(Boolean.TRUE);
        return findFirstPojo(param);
    }
    
    @Override
    public List<InventarioDetalle> find(InventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(InventarioDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, InventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<InventarioDetalle> root = cq.from(InventarioDetalle.class);
        Join<InventarioDetalle, Inventario> inventario = root.join("inventario");
        Join<InventarioDetalle, Deposito> depositoLogistico = inventario.join("depositoLogistico");
        Join<InventarioDetalle, Producto> producto = inventario.join("producto");
        Join<InventarioDetalle, Estado> estado = inventario.join("estado");
        
        find.addPath("root", root);
        find.addPath("inventario", inventario);
        find.addPath("depositoLogistico", depositoLogistico);
        find.addPath("producto", producto);
        find.addPath("estado", estado);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdInventario() != null) {
            predList.add(qb.equal(root.get("idInventario"), param.getIdInventario()));
        }
        
        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(inventario.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(inventario.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(inventario.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getTieneFechaVencimiento() != null) {
            predList.add(param.getTieneFechaVencimiento()
                    ? root.get("fechaVencimiento").isNotNull()
                    : root.get("fechaVencimiento").isNull());
        }

        if (param.getFechaVencimiento() != null) {
            predList.add(qb.equal(root.get("fechaVencimiento"), param.getFechaVencimiento()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);
        
        cq.orderBy(qb.asc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(InventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<InventarioDetalle> root = cq.from(InventarioDetalle.class);
        Join<InventarioDetalle, Inventario> inventario = root.join("inventario");
        Join<InventarioDetalle, Estado> estado = inventario.join("estado");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdInventario() != null) {
            predList.add(qb.equal(root.get("idInventario"), param.getIdInventario()));
        }

        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(inventario.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(inventario.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(inventario.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getTieneFechaVencimiento() != null) {
            predList.add(param.getTieneFechaVencimiento()
                    ? root.get("fechaVencimiento").isNotNull()
                    : root.get("fechaVencimiento").isNull());
        }

        if (param.getFechaVencimiento() != null) {
            predList.add(qb.equal(root.get("fechaVencimiento"), param.getFechaVencimiento()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
    
    @Override
    public InventarioDetalle registrarInventarioDetalle(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, Date fechaVencimiento, Integer nuevaCantidadInventarioDetalle, UserInfoImpl userInfo) {
        
        InventarioParam iparam = new InventarioParam();
        iparam.setIdDepositoLogistico(idDepositoLogistico);
        iparam.setIdProducto(idProducto);
        iparam.setIdEstado(idEstado);
        iparam.setIdEmpresa(userInfo.getIdEmpresa());
        InventarioPojo inventarioPojo = facades.getInventarioFacade().findFirstPojo(iparam);
        
        if(inventarioPojo == null) {
            Inventario inventario = new Inventario();
            inventario.setIdDepositoLogistico(idDepositoLogistico);
            inventario.setIdProducto(idProducto);
            inventario.setIdEstado(idEstado);
            inventario.setIdEmpresa(userInfo.getIdEmpresa());
            inventario.setCantidad(0);
            facades.getInventarioFacade().create(inventario);
            inventarioPojo = facades.getInventarioFacade().findFirstPojo(iparam);
        }
        
        InventarioDetalleParam param = new InventarioDetalleParam();
        param.setIdInventario(inventarioPojo.getId());
        param.setFechaVencimiento(fechaVencimiento);
        param.setTieneFechaVencimiento(fechaVencimiento != null);
        InventarioDetallePojo inventarioDetalle = findFirstPojo(param);
        
        InventarioDetalle result;
        
        Integer nuevaCantidadInventario;
        
        if(inventarioDetalle == null) {
            nuevaCantidadInventario = inventarioPojo.getCantidad() + nuevaCantidadInventarioDetalle;
            result = new InventarioDetalle();
            result.setIdInventario(inventarioPojo.getId());
            result.setFechaVencimiento(fechaVencimiento);
            result.setCantidad(nuevaCantidadInventarioDetalle);
            result = create(result);
        } else {
            nuevaCantidadInventario = inventarioPojo.getCantidad() + nuevaCantidadInventarioDetalle - inventarioDetalle.getCantidad();
            result = new InventarioDetalle();
            result.setId(inventarioDetalle.getId());
            result.setIdInventario(inventarioPojo.getId());
            result.setFechaVencimiento(fechaVencimiento);
            result.setCantidad(nuevaCantidadInventarioDetalle);
            result = edit(result);
        }
        
        Inventario inventario = new Inventario();
        inventario.setId(inventarioPojo.getId());
        inventario.setIdDepositoLogistico(idDepositoLogistico);
        inventario.setIdProducto(idProducto);
        inventario.setIdEstado(idEstado);
        inventario.setIdEmpresa(userInfo.getIdEmpresa());
        inventario.setCantidad(nuevaCantidadInventario);
        facades.getInventarioFacade().edit(inventario);
        
        return result;
    }
    
    @Override
    public void aumentarInventarioDetalle(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, Date fechaVencimiento, Integer cantidadAjuste, UserInfoImpl userInfo) {
        
        InventarioDetalleParam idparam = new InventarioDetalleParam();
        idparam.setIdDepositoLogistico(idDepositoLogistico);
        idparam.setIdProducto(idProducto);
        idparam.setIdEstado(idEstado);
        idparam.setFechaVencimiento(fechaVencimiento);
        idparam.setTieneFechaVencimiento(fechaVencimiento != null);
        idparam.setIdEmpresa(userInfo.getIdEmpresa());
        
        InventarioDetallePojo inventarioDetalle = findFirstPojo(idparam);
        
        Integer nuevaCantidadInventarioDetalle;
        
        if(inventarioDetalle == null) {
            nuevaCantidadInventarioDetalle = cantidadAjuste;
        } else {
            nuevaCantidadInventarioDetalle = inventarioDetalle.getCantidad() + cantidadAjuste;
        }
        
        registrarInventarioDetalle(idDepositoLogistico, idProducto, idEstado, fechaVencimiento, nuevaCantidadInventarioDetalle, userInfo);
    }
    
    @Override
    public void disminuirInventarioDetalle(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, Date fechaVencimiento, Integer cantidadAjuste, UserInfoImpl userInfo) {
        
        InventarioDetalleParam idparam = new InventarioDetalleParam();
        idparam.setIdDepositoLogistico(idDepositoLogistico);
        idparam.setIdProducto(idProducto);
        idparam.setIdEstado(idEstado);
        idparam.setFechaVencimiento(fechaVencimiento);
        idparam.setTieneFechaVencimiento(fechaVencimiento != null);
        idparam.setIdEmpresa(userInfo.getIdEmpresa());
        
        InventarioDetallePojo inventarioDetalle = findFirstPojo(idparam);
        
        Integer nuevaCantidadInventarioDetalle;
        
        if(inventarioDetalle == null) {
            nuevaCantidadInventarioDetalle = cantidadAjuste;
        } else {
            nuevaCantidadInventarioDetalle = inventarioDetalle.getCantidad() - cantidadAjuste;
        }
        
        registrarInventarioDetalle(idDepositoLogistico, idProducto, idEstado, fechaVencimiento, nuevaCantidadInventarioDetalle, userInfo);
    }
}
