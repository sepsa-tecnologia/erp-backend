/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Pais;
import py.com.sepsa.erp.ejb.entities.info.filters.PaisParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "PaisFacade", mappedName = "PaisFacade")
@Local(PaisFacade.class)
public class PaisFacadeImpl extends FacadeImpl<Pais, PaisParam> implements PaisFacade {

    public PaisFacadeImpl() {
        super(Pais.class);
    }

    @Override
    public Map<String, String> getMapConditions(Pais item) {
        Map<String, String> atributos = new HashMap();
        atributos.put("id", item.getId() + " ");
        atributos.put("codigo", item.getCodigo() + " ");
        atributos.put("descripcion", item.getDescripcion()+ " ");
        return atributos;
    }
    
    @Override
    public Map<String, String> getMapPk(Pais item) {
        Map<String, String> atributos = new HashMap();
        atributos.put("id", item.getId() + " ");
        return atributos;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(PaisParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(PaisParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Pais pais = find(param.getId());
        
        if(isNull(pais)) {
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el país"));
        } 
        
        return !param.tieneErrores();
    }
    
    @Override
    public Pais create(PaisParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Pais item = new Pais();
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        create(item);
        
        facades.getRegistroFacade().create("info", "configuracion", getMapConditions(item), userInfo);
        
        return item;
    }
    
    @Override
    public Pais edit(PaisParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Pais item = find(param.getId());
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        edit(item);
        
        facades.getRegistroFacade().edit("info", "pais", getMapConditions(item), userInfo, getMapPk(item));
        
        return item;
    }
    
    @Override
    public Pais find(Integer idEmpresa, Integer id, String codigo) {
        PaisParam param = new PaisParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        return findFirst(param);
    }
    
    /**
     * Obtiene la lista de tipo de tarifa
     * @param param parametros
     * @return Lista
     */
    @Override
    public List<Pais> find(PaisParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Pais.class);
        Root<Pais> root = cq.from(Pais.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Pais> result = q.getResultList();

        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de tipo de tarifa
     * @param param parametros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(PaisParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Pais> root = cq.from(Pais.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
