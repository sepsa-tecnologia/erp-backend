/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de solicitud de nota de crédito detalle
 * @author Jonathan D. Bernal Fernández
 */
public class SolicitudNotaCreditoDetalleParam extends CommonParam {
    
    public SolicitudNotaCreditoDetalleParam() {
    }

    public SolicitudNotaCreditoDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de solicitud nota de crédito
     */
    @QueryParam("idSolicitudNotaCredito")
    private Integer idSolicitudNotaCredito;
    
    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Identificador de factura de compra
     */
    @QueryParam("idFacturaCompra")
    private Integer idFacturaCompra;
    
    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;
    
    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;
    
    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private Integer cantidad;

    /**
     * Precio unitario con IVA
     */
    @QueryParam("precioUnitarioConIva")
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin IVA
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;
    
    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Código GTIN
     */
    @QueryParam("codigoGtin")
    private String codigoGtin;

    /**
     * Identificador de motivo de emisión snc
     */
    @QueryParam("idMotivoEmisionSnc")
    private Integer idMotivoEmisionSnc;

    /**
     * Observación
     */
    @QueryParam("observacion")
    private String observacion;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idSolicitudNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de solicitud de nota de crédito"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador nro de linea"));
        }
        
        if(isNull(idFactura) && isNull(idFacturaCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura/factura de compra"));
        }
        
        if(isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }
        
        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }
        
        if(isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de IVA"));
        }
        
        if(isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNull(idMotivoEmisionSnc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de motivo emisión snc"));
        }
        
        if (isNull(idProducto) && isNullOrEmpty(codigoGtin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador y/o código gtin del producto"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdSolicitudNotaCredito() {
        return idSolicitudNotaCredito;
    }

    public void setIdSolicitudNotaCredito(Integer idSolicitudNotaCredito) {
        this.idSolicitudNotaCredito = idSolicitudNotaCredito;
    }

    public void setIdMotivoEmisionSnc(Integer idMotivoEmisionSnc) {
        this.idMotivoEmisionSnc = idMotivoEmisionSnc;
    }

    public Integer getIdMotivoEmisionSnc() {
        return idMotivoEmisionSnc;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }
}
