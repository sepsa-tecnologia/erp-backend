/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Contacto;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefono;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefonoNotificacion;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.TipoNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaTelefonoNotificacionParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.PersonaTelefonoNotificacionPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaTelefonoNotificacionFacade", mappedName = "PersonaTelefonoNotificacionFacade")
@Local(PersonaTelefonoNotificacionFacade.class)
public class PersonaTelefonoNotificacionFacadeImpl extends FacadeImpl<PersonaTelefonoNotificacion, PersonaTelefonoNotificacionParam> implements PersonaTelefonoNotificacionFacade {

    public PersonaTelefonoNotificacionFacadeImpl() {
        super(PersonaTelefonoNotificacion.class);
    }
    
    /**
     * Obtiene la lista de contacto telefono notificacion asociada
     * @param idContacto Identificador de contacto
     * @param idTelefono Identificador de telefono
     * @return Contacto email notificacion
     */
    @Override
    public List<PersonaTelefonoNotificacionPojo> asociado(Integer idContacto, Integer idTelefono) {
        
        String sql = String.format("with ctn as (select ctn.*\n"
                + " from info.contacto_telefono_notificacion ctn\n"
                + "where ctn.id_contacto = %d\n"
                + "and ctn.id_telefono = %d)\n"
                + "select ctn.id_contacto,\n"
                + "coalesce(p.razon_social, p.nombre || ' ' || p.apellido),\n"
                + "ctn.id_telefono, t.prefijo, t.numero, tn.id, tn.descripcion,\n"
                + "coalesce(ctn.estado, 'I') as estado\n"
                + "from ctn\n"
                + "right join info.tipo_notificacion tn on tn.id = ctn.id_tipo_notificacion\n"
                + "left join info.persona p on p.id = ctn.id_contacto\n"
                + "left join info.telefono t on t.id = ctn.id_telefono\n"
                + "where tn.telefono = 'S'",
                idContacto, idTelefono);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<PersonaTelefonoNotificacionPojo> result = new ArrayList<>();
        
        for (Object[] object : list) {
            
            PersonaTelefonoNotificacionPojo pojo = new PersonaTelefonoNotificacionPojo();
            pojo.setIdContacto((Integer)object[0]);
            pojo.setContacto((String)object[1]);
            pojo.setIdTelefono((Integer)object[2]);
            pojo.setPrefijo((String)object[3]);
            pojo.setNumero((String)object[4]);
            pojo.setIdTipoNotificacion((Integer)object[5]);
            pojo.setTipoNotificacion((String)object[6]);
            pojo.setEstado((Character)object[7]);
            
            result.add(pojo);
        }
        
        return result;
    }
    
    /**
     * Obtiene la lista de contacto telefono notificacion
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public List<PersonaTelefonoNotificacion> find(PersonaTelefonoNotificacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(PersonaTelefonoNotificacion.class);
        Root<PersonaTelefonoNotificacion> root = cq.from(PersonaTelefonoNotificacion.class);
        Join<PersonaTelefonoNotificacion, TipoNotificacion> tipoNotificacion = root.join("tipoNotificacion");
        Join<PersonaTelefonoNotificacion, Telefono> telefono = root.join("telefono");
        Join<PersonaTelefonoNotificacion, Persona> persona = root.join("persona");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(persona.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(persona.get("id"), param.getIdPersona()));
        }
        
        if(param.getContacto()!= null && !param.getContacto().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(qb.coalesce(persona.<String>get("razonSocial"), qb.concat(qb.concat(persona.<String>get("nombre"), " "), persona.<String>get("apellido")))), String.format("%%%s%%", param.getContacto().trim().toUpperCase())));
        }

        if (param.getIdTelefono() != null) {
            predList.add(qb.equal(telefono.get("id"), param.getIdTelefono()));
        }

        if (param.getPrefijo() != null && !param.getPrefijo().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(telefono.<String>get("prefijo")), String.format("%%%s%%", param.getPrefijo().trim().toUpperCase())));
        }

        if (param.getNumero()!= null && !param.getNumero().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(telefono.<String>get("numero")), String.format("%%%s%%", param.getNumero().trim().toUpperCase())));
        }

        if (param.getIdTipoNotificacion() != null) {
            predList.add(qb.equal(tipoNotificacion.get("id"), param.getIdTipoNotificacion()));
        }
        
        if (param.getTipoNotificacion() != null && !param.getTipoNotificacion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(tipoNotificacion.<String>get("descripcion")), String.format("%%%s%%", param.getTipoNotificacion().trim().toUpperCase())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<PersonaTelefonoNotificacion> list = q.getResultList();

        return list;
    }
    
    /**
     * Obtiene el tamaño de la lista de contacto telefono notificacion
     * @param param Parámetros
     * @return Tamaño de lista
     */
    @Override
    public Long findSize(PersonaTelefonoNotificacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<PersonaTelefonoNotificacion> root = cq.from(PersonaTelefonoNotificacion.class);
        Join<PersonaTelefonoNotificacion, TipoNotificacion> tipoNotificacion = root.join("tipoNotificacion");
        Join<PersonaTelefonoNotificacion, Telefono> telefono = root.join("telefono");
        Join<PersonaTelefonoNotificacion, Persona> persona = root.join("persona");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(persona.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(persona.get("id"), param.getIdPersona()));
        }
        
        if(param.getContacto()!= null && !param.getContacto().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(qb.coalesce(persona.<String>get("razonSocial"), qb.concat(qb.concat(persona.<String>get("nombre"), " "), persona.<String>get("apellido")))), String.format("%%%s%%", param.getContacto().trim().toUpperCase())));
        }

        if (param.getIdTelefono() != null) {
            predList.add(qb.equal(telefono.get("id"), param.getIdTelefono()));
        }

        if (param.getPrefijo() != null && !param.getPrefijo().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(telefono.<String>get("prefijo")), String.format("%%%s%%", param.getPrefijo().trim().toUpperCase())));
        }

        if (param.getNumero()!= null && !param.getNumero().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(telefono.<String>get("numero")), String.format("%%%s%%", param.getNumero().trim().toUpperCase())));
        }

        if (param.getIdTipoNotificacion() != null) {
            predList.add(qb.equal(tipoNotificacion.get("id"), param.getIdTipoNotificacion()));
        }
        
        if (param.getTipoNotificacion() != null && !param.getTipoNotificacion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(tipoNotificacion.<String>get("descripcion")), String.format("%%%s%%", param.getTipoNotificacion().trim().toUpperCase())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
