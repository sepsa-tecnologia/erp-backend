/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Clase para el manejo de los parámetros de tipo documento prceso
 * @author Jonathan D. Bernal Fernández
 */
public class TipoDocumentoProcesoParam extends CommonParam {
    
    public TipoDocumentoProcesoParam() {
    }

    public TipoDocumentoProcesoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de archivo
     */
    @QueryParam("idTipoArchivo")
    protected Integer idTipoArchivo;
    
    /**
     * Código de tipo de archivo
     */
    @QueryParam("codigoTipoArchivo")
    protected String codigoTipoArchivo;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public String getCodigoTipoArchivo() {
        return codigoTipoArchivo;
    }

    public void setCodigoTipoArchivo(String codigoTipoArchivo) {
        this.codigoTipoArchivo = codigoTipoArchivo;
    }
}
