/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de factura compra detalle
 *
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaCompraDetalleParam extends CommonParam {

    public FacturaCompraDetalleParam() {
    }

    public FacturaCompraDetalleParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de factura de compra
     */
    @QueryParam("idFacturaCompra")
    private Integer idFacturaCompra;

    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;

    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;

    /**
     * Cantidad facturada
     */
    @QueryParam("cantidadFacturada")
    private Integer cantidadFacturada;

    /**
     * Precio unitario con iva
     */
    @QueryParam("precioUnitarioConIva")
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin iva
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;

    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;

    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;

    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idFacturaCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura de compra"));
        }

        if (isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de línea"));
        }

        if (isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }

        if (isNull(cantidadFacturada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad facturada"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }

        if (isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de IVA"));
        }

        if (isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }

        if (isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

        return !tieneErrores();
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getCantidadFacturada() {
        return cantidadFacturada;
    }

    public void setCantidadFacturada(Integer cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }
}
