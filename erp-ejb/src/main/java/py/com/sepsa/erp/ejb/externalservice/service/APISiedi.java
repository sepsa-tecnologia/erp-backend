package py.com.sepsa.erp.ejb.externalservice.service;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStreamResponse;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;
import static py.com.sepsa.erp.ejb.externalservice.service.APINotificacionServer.token;

/**
 * Cliente API Service para los servicios comunes
 *
 * @author Daniel F. Escauriza Arza
 */
@Log4j2
public class APISiedi extends API {

    //Parámetros de conexión
    private final static String BASE_API = "SiediMonitor/api";
    private final static String BASE_SIEDI_API = "siedicrud/api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;
    private final static int READ_TIMEOUT = 20 * 1000;
    private final static Scheme PROTOCOL = Scheme.HTTP;

    //Datos para enviar formulario HTTP
    private static final String BOUNDARY = "--------------------------" + System.currentTimeMillis();
    private static final String LINE_FEED = "\r\n";

    public static String token;
    public static String apiToken;

    /**
     * Obtiene el KUDE asociado a una factura
     *
     * @param cdc CDC de la factura
     * @param nombreArchivo Nombre del archivo
     * @return
     */
    public HttpStreamResponse obtenerDteKude(String cdc, String nombreArchivo) {
        HttpStreamResponse response = new HttpStreamResponse();

        String resource = String.format(Resource.OBTENER_DTE_KUDE.url,
                cdc, nombreArchivo);

        HttpURLConnection httpConn = GET(resource,
                ContentType.JSON, null);

        try {
            if (httpConn != null) {

                response = HttpStreamResponse.createInstance(httpConn);

                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Obtiene el KUDE asociado a una factura con parámetros
     *
     * @param cdc CDC de la factura
     * @param nombreArchivo Nombre del archivo
     * @param datos Datos
     * @return
     */
    public HttpStreamResponse obtenerDteKude(String cdc, String nombreArchivo, JsonObject datos, Map params) {
        HttpStreamResponse response = new HttpStreamResponse();

        String resource = String.format(Resource.OBTENER_DTE_KUDE.url,
                cdc, nombreArchivo);

        Map param = new HashMap();
        param = params;

        HttpURLConnection httpConn = GET(resource, ContentType.MULTIPART, param);

        try {
            if (httpConn != null) {

                JsonObject body = new JsonObject();
                if (datos != null) {
                    body.addProperty("datos", datos.toString());
                }

                this.addBody(httpConn, body.toString());

                response = HttpStreamResponse.createInstance(httpConn);

                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Obtiene el KUDE asociado a una factura con parámetros
     *
     * @param tokenSiedi token siedi
     * @param cdc CDC de la factura
     * @param nombreArchivo Nombre del archivo
     * @param datos Datos
     * @param params parametros
     * @return
     */
    public HttpStreamResponse obtenerDteKudeSiediApi(String tokenSiedi, String cdc, String nombreArchivo, JsonObject datos, Map params) {
        HttpStreamResponse response = new HttpStreamResponse();

        Map param = new HashMap();
        param = params;
        param.put("filename", nombreArchivo);
        param.put("cdc", cdc);

        HttpURLConnection httpConn = GET(Resource.OBTENER_DTE_KUDE_SIEDIAPI.url, ContentType.MULTIPART, param);

        httpConn.setRequestProperty("Authorization",
                tokenSiedi.startsWith("bearer ")
                || tokenSiedi.startsWith("Bearer ")
                ? tokenSiedi
                : String.format("Bearer %s", tokenSiedi));

        try {
            if (httpConn != null) {

                JsonObject body = new JsonObject();
                if (datos != null) {
                    body.addProperty("datos", datos.toString());
                }

                this.addBody(httpConn, body.toString());

                response = HttpStreamResponse.createInstance(httpConn);

                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Obtiene el XML asociado a una factura con parámetros
     *
     * @param tokenSiedi token siedi
     * @param cdc CDC de la factura
     * @param nombreArchivo Nombre del archivo
     * @param datos Datos
     * @param params parametros
     * @return
     */
    public HttpStreamResponse obtenerXmlSiediApi(String tokenSiedi, String cdc, String nombreArchivo, JsonObject datos, Map params) {
        HttpStreamResponse response = new HttpStreamResponse();

        Map param = new HashMap();
        param.put("filename", nombreArchivo);
        param.put("cdc", cdc);

        HttpURLConnection httpConn = GET(Resource.OBTENER_DTE_XML_SIEDIAPI.url, ContentType.XML, param);

        httpConn.setRequestProperty("Authorization",
                tokenSiedi.startsWith("bearer ")
                || tokenSiedi.startsWith("Bearer ")
                ? tokenSiedi
                : String.format("Bearer %s", tokenSiedi));

        try {
            if (httpConn != null) {

                JsonObject body = new JsonObject();
                if (datos != null) {
                    body.addProperty("datos", datos.toString());
                }

                this.addBody(httpConn, body.toString());

                response = HttpStreamResponse.createInstance(httpConn);

                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Obtiene el Xml asociado a una factura con parámetros
     *
     * @param cdc CDC de la factura
     * @param nombreArchivo Nombre del archivo
     * @param datos Datos
     * @return
     */
    public HttpStreamResponse obtenerDteXml(String cdc, String nombreArchivo, JsonObject datos, Map params) {
        HttpStreamResponse response = new HttpStreamResponse();

        String resource = String.format(Resource.OBTENER_DTE_XML.url,
                cdc, nombreArchivo);

        Map param = new HashMap();
        param = params;

        HttpURLConnection httpConn = GET(resource, ContentType.XML, param);

        try {
            if (httpConn != null) {

                JsonObject body = new JsonObject();
                if (datos != null) {
                    body.addProperty("datos", datos.toString());
                }

                this.addBody(httpConn, body.toString());

                response = HttpStreamResponse.createInstance(httpConn);

                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Cancela un DTE
     *
     * @param cdc CDC
     * @param motivo Motivo de emisión
     * @return Respuesta HTTP
     */
    public HttpStringResponse cancelarDte(String cdc, String motivo) {
        HttpStringResponse response = new HttpStringResponse();

        HttpURLConnection httpConn = POST(Resource.CANCELAR_DTE.url,
                ContentType.JSON);

        try {
            if (httpConn != null) {

                JsonObject obj = new JsonObject();

                if (cdc != null && !cdc.trim().isEmpty()) {
                    obj.addProperty("cdc", cdc);
                }

                if (motivo != null && !motivo.trim().isEmpty()) {
                    obj.addProperty("motivo", motivo);
                }

                JsonArray array = new JsonArray();
                array.add(obj);

                JsonObject body = new JsonObject();
                body.add("eventos", array);

                this.addBody(httpConn, body.toString());

                response = HttpStringResponse.createInstance(httpConn);

                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    public APISiedi(String host) {
        super(Service.EDI_SERVER, PROTOCOL, BASE_API, token, CONN_TIMEOUT,
                READ_TIMEOUT, BOUNDARY, LINE_FEED, null, apiToken);

        List<Host> hosts = Host.createInstaces(host);

        this.host = hosts == null || hosts.isEmpty() ? null : hosts.get(0);
    }

    public APISiedi(String host, Boolean siediApi) {
        super(Service.EDI_SERVER, PROTOCOL, BASE_SIEDI_API, token, CONN_TIMEOUT, READ_TIMEOUT, BOUNDARY, LINE_FEED, null, apiToken);

        List<Host> hosts = Host.createInstaces(host);

        this.host = hosts == null || hosts.isEmpty() ? null : hosts.get(0);
    }

    /**
     * Recursos de conexión o servicios del API
     */
    public enum Resource {

        //Servicios
        LOGIN("Ingreso al sistema", "login"),
        OBTENER_DTE_KUDE("Obtiene el DTE asociado a una factura", "v1/consulta/dte/kude/%s/%s"),
        OBTENER_DTE_XML("Obtiene el DTE asociado a una factura", "v1/consulta/dte/xml/%s/%s"),
        OBTENER_DTE_KUDE_SIEDIAPI("Obtiene el DTE asociado a una factura desde el siediAPi", "v1/set/dte/kude"),
        OBTENER_DTE_XML_SIEDIAPI("Obtiene el DTE asociado a una factura desde el siediAPi", "v1/set/dte/xml"),
        CANCELAR_DTE("Cancela un DTE", "v1/operacion/cancelacion");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
