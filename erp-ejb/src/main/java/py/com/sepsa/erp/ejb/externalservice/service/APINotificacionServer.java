/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.externalservice.service;

import com.google.gson.JsonElement;
import java.net.HttpURLConnection;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;

/**
 * Cliente API Service
 *
 * @author Jonathan D. Bernal Fernández
 */
@Log4j2
public class APINotificacionServer extends API {

    //Datos de conexión al API REST
    private final static String BASE_API = "rest";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;
    private final static int READ_TIMEOUT = 20 * 1000;
    private final static Scheme PROTOCOL = Scheme.HTTP;

    //Datos para enviar formulario HTTP
    private static final String BOUNDARY = "--------------------------" + System.currentTimeMillis();
    private static final String LINE_FEED = "\r\n";

    public static String token;
    public static String apiToken;

    /**
     * Crea un mensaje de notificación
     *
     * @param object Mensaje de notificación
     * @return Resultado
     */
    public HttpStringResponse crearMensaje(JsonElement object) {

        HttpStringResponse response = new HttpStringResponse();

        HttpURLConnection httpConn = POST(Resource.MENSAJE.url, ContentType.JSON);

        try {
            if (httpConn != null) {

                this.addBody(httpConn, object.toString());

                response = this.resolve(httpConn, null);

                httpConn.disconnect();
            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Crea un mensaje de notificación
     *
     * @param object Mensaje de notificación
     * @return Resultado
     */
    public HttpStringResponse crearNotificacion(JsonElement object) {

        HttpStringResponse response = new HttpStringResponse();

        HttpURLConnection httpConn = POST(Resource.NOTIFICACION.url, ContentType.JSON);

        try {
            if (httpConn != null) {

                this.addBody(httpConn, object.toString());

                response = this.resolve(httpConn, null);

                httpConn.disconnect();
            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Constructor
     */
    public APINotificacionServer() {
        super(Service.EDI_SERVER, PROTOCOL, BASE_API, token, CONN_TIMEOUT,
                READ_TIMEOUT, BOUNDARY, LINE_FEED, null, apiToken);

        //List<Host> hosts = Host.createInstaces("0:localhost:8181");
        List<Host> hosts = Host.createInstaces("0#https://notificacion-dev.edi.com.py#443");
        //List<Host> hosts = Host.createInstaces("0:notificacion.edi.com.py:443");

        this.host = hosts == null || hosts.isEmpty() ? null : hosts.get(0);
    }

    /**
     * Recursos de conexión o servicios del API
     */
    public enum Resource {

        //Servicios
        PING("Realiza un ping al servidor", "v1/ping"),
        MENSAJE("Crea un mensaje de notificación", "v1/message"),
        NOTIFICACION("Crea una notificación", "v1/notification");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
