/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.ControlInventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ControlInventarioFacade extends Facade<ControlInventario, ControlInventarioParam, UserInfoImpl> {

    public ControlInventario createTemplate(ControlInventarioParam param, UserInfoImpl userInfo);

    public ControlInventario complete(ControlInventarioParam param, UserInfoImpl userInfo);

    public ControlInventario changeState(ControlInventarioParam param, UserInfoImpl userInfo);

    public byte[] getPdfControlInventario(ControlInventarioParam param, UserInfoImpl userInfo) throws Exception;

}
