/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contacto telefono
 *
 * @author Jonathan
 */
public class PersonaTelefonoNotificacionParam extends CommonParam {

    public PersonaTelefonoNotificacionParam() {
    }

    public PersonaTelefonoNotificacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;

    /**
     * Identificador de telefono
     */
    @QueryParam("idTelefono")
    private Integer idTelefono;

    /**
     * Contacto
     */
    @QueryParam("contacto")
    private String contacto;

    /**
     * Prefijo
     */
    @QueryParam("prefijo")
    private String prefijo;

    /**
     * Numero
     */
    @QueryParam("numero")
    private String numero;

    /**
     * Identificador de tipo de notificacion
     */
    @QueryParam("idTipoNotificacion")
    private Integer idTipoNotificacion;

    /**
     * Tipo de notificacion
     */
    @QueryParam("tipoNotificacion")
    private String tipoNotificacion;

    @Override
    public boolean isValidToCreate() {
        limpiarErrores();

        if (isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona"));
        }

        if (isNull(idTelefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de telefono"));
        }

        if (isNull(idTipoNotificacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de notificación"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si esta activo"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
