/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de producto precio
 * @author Jonathan
 */
public class ProductoPrecioParam extends CommonParam {

    public ProductoPrecioParam() {
    }

    public ProductoPrecioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Lista de identificadores excluidos
     */
    @QueryParam("idExcluidos")
    private List<Integer> idExcluidos;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Producto descripcion
     */
    @QueryParam("productoDescripcion")
    private String productoDescripcion;
    
    /**
     * Identificador de canal de venta
     */
    @QueryParam("idCanalVenta")
    private Integer idCanalVenta;
    
    /**
     * Identificador de canal de venta null
     */
    @QueryParam("idCanalVentaNull")
    private Boolean idCanalVentaNull;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de cliente null
     */
    @QueryParam("idClienteNull")
    private Boolean idClienteNull;
    
    /**
     * Canal de venta descripcion
     */
    @QueryParam("canalVentaDescripcion")
    private String canalVentaDescripcion;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Precio
     */
    @QueryParam("precio")
    private BigDecimal precio;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Fecha inicio
     */
    @QueryParam("fechaInicio")
    private Date fechaInicio;
    
    /**
     * Fecha inicio desde
     */
    @QueryParam("fechaInicioDesde")
    private Date fechaInicioDesde;
    
    /**
     * Fecha inicio hasta
     */
    @QueryParam("fechaInicioHasta")
    private Date fechaInicioHasta;
    
    /**
     * Fecha fin
     */
    @QueryParam("fechaFin")
    private Date fechaFin;
    
    /**
     * Fecha fin desde
     */
    @QueryParam("fechaFinDesde")
    private Date fechaFinDesde;
    
    /**
     * Fecha fin hasta
     */
    @QueryParam("fechaFinHasta")
    private Date fechaFinHasta;
    
    /**
     * Fecha fin null
     */
    @QueryParam("fechaFinNull")
    private Boolean fechaFinNull;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de moneda"));
        }
        
        if(isNull(precio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio"));
        }
        
        if(isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha inicio"));
        }
        
        if(!isNull(fechaInicio) && !isNull(fechaFin)) {
            
            Calendar from = Calendar.getInstance();
            from.setTime(fechaInicio);
            from.set(Calendar.HOUR_OF_DAY, 0);
            from.set(Calendar.MINUTE, 0);
            from.set(Calendar.SECOND, 0);
            fechaInicio = from.getTime();
            
            Calendar to = Calendar.getInstance();
            to.setTime(fechaFin);
            to.set(Calendar.HOUR_OF_DAY, 0);
            to.set(Calendar.MINUTE, 0);
            to.set(Calendar.SECOND, 0);
            fechaFin = to.getTime();
            
            if(from.compareTo(to) > 0) {
                addError(MensajePojo.createInstance().descripcion("La fecha de inicio no puede ser anterior a la fecha fin"));
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de moneda"));
        }
        
        if(isNull(precio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha inicio"));
        }
        
        if(!isNull(fechaInicio) && !isNull(fechaFin)) {
            
            Calendar from = Calendar.getInstance();
            from.setTime(fechaInicio);
            from.set(Calendar.HOUR_OF_DAY, 0);
            from.set(Calendar.MINUTE, 0);
            from.set(Calendar.SECOND, 0);
            fechaInicio = from.getTime();
            
            Calendar to = Calendar.getInstance();
            to.setTime(fechaFin);
            to.set(Calendar.HOUR_OF_DAY, 0);
            to.set(Calendar.MINUTE, 0);
            to.set(Calendar.SECOND, 0);
            fechaFin = to.getTime();
            
            if(from.compareTo(to) > 0) {
                addError(MensajePojo.createInstance().descripcion("La fecha de inicio no puede ser anterior a la fecha fin"));
            }
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public String getProductoDescripcion() {
        return productoDescripcion;
    }

    public void setProductoDescripcion(String productoDescripcion) {
        this.productoDescripcion = productoDescripcion;
    }

    public String getCanalVentaDescripcion() {
        return canalVentaDescripcion;
    }

    public void setCanalVentaDescripcion(String canalVentaDescripcion) {
        this.canalVentaDescripcion = canalVentaDescripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaInicioDesde() {
        return fechaInicioDesde;
    }

    public void setFechaInicioDesde(Date fechaInicioDesde) {
        this.fechaInicioDesde = fechaInicioDesde;
    }

    public Date getFechaInicioHasta() {
        return fechaInicioHasta;
    }

    public void setFechaInicioHasta(Date fechaInicioHasta) {
        this.fechaInicioHasta = fechaInicioHasta;
    }

    public Date getFechaFinDesde() {
        return fechaFinDesde;
    }

    public void setFechaFinDesde(Date fechaFinDesde) {
        this.fechaFinDesde = fechaFinDesde;
    }

    public Date getFechaFinHasta() {
        return fechaFinHasta;
    }

    public void setFechaFinHasta(Date fechaFinHasta) {
        this.fechaFinHasta = fechaFinHasta;
    }

    public void setIdExcluidos(List<Integer> idExcluidos) {
        this.idExcluidos = idExcluidos;
    }

    public List<Integer> getIdExcluidos() {
        return idExcluidos;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setFechaFinNull(Boolean fechaFinNull) {
        this.fechaFinNull = fechaFinNull;
    }

    public Boolean getFechaFinNull() {
        return fechaFinNull;
    }

    public void setIdCanalVentaNull(Boolean idCanalVentaNull) {
        this.idCanalVentaNull = idCanalVentaNull;
    }

    public Boolean getIdCanalVentaNull() {
        return idCanalVentaNull;
    }

    public void setIdClienteNull(Boolean idClienteNull) {
        this.idClienteNull = idClienteNull;
    }

    public Boolean getIdClienteNull() {
        return idClienteNull;
    }
}
