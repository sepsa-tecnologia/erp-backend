/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Motivo;
import py.com.sepsa.erp.ejb.entities.info.TipoMotivo;
import py.com.sepsa.erp.ejb.entities.info.filters.MotivoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MotivoFacade", mappedName = "MotivoFacade")
@Local(MotivoFacade.class)
public class MotivoFacadeImpl extends FacadeImpl<Motivo, MotivoParam> implements MotivoFacade {

    public MotivoFacadeImpl() {
        super(Motivo.class);
    }
    
    public Boolean validToCreate(MotivoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MotivoParam param1 = new MotivoParam();
        param1.setCodigo(param.getCodigo().trim());
        param1.setIdTipoMotivo(param.getIdTipoMotivo());
        
        Motivo motivo = findFirst(param1);
        
        if(motivo != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe otro registro con el mismo código"));
        }
        
        TipoMotivo tipoMotivo = facades.getTipoMotivoFacade()
                .find(param.getIdTipoMotivo(), param.getCodigoTipoMotivo());
        
        if(tipoMotivo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de motivo"));
        } else {
            param.setIdTipoMotivo(tipoMotivo.getId());
            param.setCodigoTipoMotivo(tipoMotivo.getCodigo());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(MotivoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Motivo motivo = find(param.getId());
        
        if(motivo == null) {
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el motivo"));
            
        } else if (!motivo.getCodigo().equals(param.getCodigo().trim())) {
            
            MotivoParam param1 = new MotivoParam();
            param1.setCodigo(param.getCodigo().trim());
            param1.setIdTipoMotivo(param.getIdTipoMotivo());

            Motivo m = findFirst(param1);

            if(m != null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe otro registro con el mismo código"));
            }
        }
        
        TipoMotivo tipoMotivo = facades.getTipoMotivoFacade()
                .find(param.getIdTipoMotivo(), param.getCodigoTipoMotivo());
        
        if(tipoMotivo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de motivo"));
        } else {
            param.setIdTipoMotivo(tipoMotivo.getId());
            param.setCodigoTipoMotivo(tipoMotivo.getCodigo());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Motivo create(MotivoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Motivo result = new Motivo();
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setIdTipoMotivo(param.getIdTipoMotivo());
        
        create(result);
        
        return result;
    }

    @Override
    public Motivo edit(MotivoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Motivo result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setIdTipoMotivo(param.getIdTipoMotivo());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public MotivoPojo find(Integer id, String codigoMotivo, String codigoTipoMotivo) {
        MotivoParam param = new MotivoParam();
        param.setId(id);
        param.setCodigoTipoMotivo(codigoTipoMotivo);
        param.setCodigo(codigoMotivo);
        param.setActivo('S');
        param.setFirstResult(0);
        param.setPageSize(1);
        List<MotivoPojo> list = findPojoAlternative(param);
        return list == null || list.isEmpty() ? null : list.get(0);
    }
    
    @Override
    public List<MotivoPojo> findPojoAlternative(MotivoParam param) {

        AbstractFind find = new AbstractFind(MotivoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigo"),
                        getPath("root").get("activo"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<MotivoPojo> findPojo(MotivoParam param) {

        AbstractFind find = new AbstractFind(MotivoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigo"),
                        getPath("root").get("idTipoMotivo"),
                        getPath("tipoMotivo").get("descripcion"),
                        getPath("tipoMotivo").get("codigo"),
                        getPath("root").get("activo"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("empresa").get("codigo"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<Motivo> find(MotivoParam param) {

        AbstractFind find = new AbstractFind(Motivo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, MotivoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Motivo> root = cq.from(Motivo.class);
        Join<Motivo, TipoMotivo> tipoMotivo = root.join("tipoMotivo");
        Join<Motivo, Empresa> empresa = root.join("empresa", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("tipoMotivo", tipoMotivo);
        find.addPath("empresa", empresa);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoMotivo() != null) {
            predList.add(qb.equal(tipoMotivo.get("id"), param.getIdTipoMotivo()));
        }

        if (param.getCodigoTipoMotivo() != null && !param.getCodigoTipoMotivo().trim().isEmpty()) {
            predList.add(qb.equal(tipoMotivo.get("codigo"), param.getCodigoTipoMotivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de estado
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(MotivoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Motivo> root = cq.from(Motivo.class);
        Join<Motivo, TipoMotivo> tm = root.join("tipoMotivo");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoMotivo() != null) {
            predList.add(qb.equal(tm.get("id"), param.getIdTipoMotivo()));
        }

        if (param.getCodigoTipoMotivo() != null && !param.getCodigoTipoMotivo().trim().isEmpty()) {
            predList.add(qb.equal(tm.get("codigo"), param.getCodigoTipoMotivo().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
