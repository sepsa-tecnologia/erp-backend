/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.pojos;

import java.io.Serializable;

/**
 * POJO para el manejo de Vendedor
 *
 * @author Gustavo Benítez
 */
public class VendedorPojo implements Serializable {

    /**
     * Identificador del vendedor
     */
    private Integer id;
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    /**
     * Empresa
     */
    private String empresa;
    /**
     * Identificador de tipo de persona
     */
    private Integer idTipoPersona;
    /**
     * Tipo persona
     */
    private String tipoPersona;
    /**
     * Nombre del Vendedor
     */
    private String nombre;
    /**
     * Apellido
     */
    private String apellido;
    /**
     * activo
     */
    private Character activo;

    private Integer idPersona;

//<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(Integer idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    //</editor-fold>
}
