/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de usuario local
 * @author Jonathan
 */
public class UsuarioPerfilParam extends CommonParam {
    
    public UsuarioPerfilParam() {
    }

    public UsuarioPerfilParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    @QueryParam("usuario")
    private String usuario;
    
    /**
     * Identificador de perfil
     */
    @QueryParam("idPerfil")
    private Integer idPerfil;

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idPerfil)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de perfil"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de estado"));
        }
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idPerfil)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de perfil"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de estado"));
        }
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToListRelacionado() {
        
        super.isValidToList();
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }
}
