/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class VehiculoTrasladoParam extends CommonParam {
    
    public VehiculoTrasladoParam(){
    }
    
    public VehiculoTrasladoParam(String bruto){
        super(bruto);
    }
    
    @QueryParam("tipoVehiculo")
    private String tipoVehiculo;
    
    @QueryParam("marca")
    private String marca;
    
    @QueryParam("tipoIdentificacion")
    private Integer tipoIdentificacion;
    
    @QueryParam("nroIdentificacion")
    private String nroIdentificacion;
    
    @QueryParam("datoAdicional")
    private String datoAdicional;
    
    @QueryParam("nroMatricula")
    private String nroMatricula;
    
    @QueryParam("nroVuelo")
    private String nroVuelo;
    

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(Integer tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNroIdentificacion() {
        return nroIdentificacion;
    }

    public void setNroIdentificacion(String nroIdentificacion) {
        this.nroIdentificacion = nroIdentificacion;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getNroMatricula() {
        return nroMatricula;
    }

    public void setNroMatricula(String nroMatricula) {
        this.nroMatricula = nroMatricula;
    }

    public String getNroVuelo() {
        return nroVuelo;
    }

    public void setNroVuelo(String nroVuelo) {
        this.nroVuelo = nroVuelo;
    }
    
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(tipoVehiculo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de vehículo"));
        }
        
        if(isNullOrEmpty(marca)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la marca del vehículo"));
        } else if(marca.trim().length() > 10){
            addError(MensajePojo.createInstance()
                    .descripcion("La marca del vehículo no debe superar los 10 caracteres"));
        }
        
        if(isNull(tipoIdentificacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de identificación del vehículo"));
        }
        
        if(!isNull(tipoIdentificacion) && tipoIdentificacion == 1){
            if (isNullOrEmpty(nroIdentificacion)){
                addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de identificación del vehículo"));
            }
            this.nroMatricula = null;
            
        }
        
        if(!isNull(tipoIdentificacion) && tipoIdentificacion == 2){
            if (isNullOrEmpty(nroMatricula)){
                addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de matrícula del vehículo"));
            }
            this.nroIdentificacion = null; 
        }
                  
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador del vehículo"));
        }
        
        if(isNullOrEmpty(tipoVehiculo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de vehículo"));
        }
        
        if(isNullOrEmpty(marca)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la marca del vehículo"));
        }
        
        if(isNull(tipoIdentificacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de identificación del vehículo"));
        }
        
        if(!isNull(tipoIdentificacion) && tipoIdentificacion == 1){
            if (isNullOrEmpty(nroIdentificacion)){
                addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de identificación del vehículo"));
            }
            this.nroMatricula = null;
            
        }
        
        if(!isNull(tipoIdentificacion) && tipoIdentificacion == 2){
            if (isNullOrEmpty(nroMatricula)){
                addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de matrícula del vehículo"));
            }
            this.nroIdentificacion = null; 
        }
                  
        return !tieneErrores();
    }
  
    
    
}
