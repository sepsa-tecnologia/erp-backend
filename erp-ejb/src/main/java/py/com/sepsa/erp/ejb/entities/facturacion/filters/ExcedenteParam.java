/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Clase para el manejo de los parámetros de excedente
 * @author Jonathan D. Bernal Fernández
 */
public class ExcedenteParam extends CommonParam {
    
    public ExcedenteParam() {
    }

    public ExcedenteParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de documento
     */
    @QueryParam("idTipoDocumento")
    private Integer idTipoDocumento;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }
}
