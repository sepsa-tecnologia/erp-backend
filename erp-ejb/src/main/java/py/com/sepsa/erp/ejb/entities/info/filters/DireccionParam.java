/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de direccion
 * @author Jonathan
 */
public class DireccionParam extends CommonParam {
    
    public DireccionParam() {
    }

    public DireccionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Latitud
     */
    @QueryParam("latitud")
    private String latitud;
    
    /**
     * Longitud
     */
    @QueryParam("longitud")
    private String longitud;
    
    /**
     * Dirección
     */
    @QueryParam("direccion")
    private String direccion;
    
    /**
     * Nro de Dirección
     */
    @QueryParam("numero")
    private Integer numero;
    
    /**
     * Observacion
     */
    @QueryParam("observacion")
    private String observacion;
    
    /**
     * Identificador de departamento
     */
    @QueryParam("idDepartamento")
    private Integer idDepartamento;
    
    /**
     * Identificador de distrito
     */
    @QueryParam("idDistrito")
    private Integer idDistrito;
    
    /**
     * Identificador de ciudad
     */
    @QueryParam("idCiudad")
    private Integer idCiudad;
    
    /**
     * Bandera de validación de País
     */
    @QueryParam("extranjero")
    private String extranjero;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(latitud)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la latitud"));
        }
        
        if(isNullOrEmpty(longitud)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la longitud"));
        }
        
        if(isNullOrEmpty(direccion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la dirección"));
        }
        
        if(isNull(numero)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de dirección"));
        }
        
        if (isNull(extranjero) || extranjero.equalsIgnoreCase("N")){
            if(!isNull(idDepartamento) || !isNull(idDistrito) || !isNull(idCiudad)) {
                if(isNull(idDepartamento) || isNull(idDistrito) || isNull(idCiudad)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el departamento, distrito y ciudad en conjunto"));
                }
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNullOrEmpty(latitud)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la latitud"));
        }
        
        if(isNullOrEmpty(longitud)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la longitud"));
        }
        
        if(isNullOrEmpty(direccion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la dirección"));
        }
        
        if(isNull(numero)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de dirección"));
        }
        
        if(!isNull(idDepartamento) || !isNull(idDistrito) || !isNull(idCiudad)) {
            if(isNull(idDepartamento) || isNull(idDistrito) || isNull(idCiudad)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el departamento, distrito y ciudad en conjunto"));
            }
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getNumero() {
        return numero;
    }
    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public String getExtranjero() {
        return extranjero;
    }

    public void setExtranjero(String extranjero) {
        this.extranjero = extranjero;
    }
    
}
