/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.MenuPerfil;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuPerfilParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface MenuPerfilFacade extends Facade<MenuPerfil, MenuPerfilParam, UserInfoImpl> {

    public Boolean validToEdit(MenuPerfilParam param);

    public List<MenuPerfil> findRelacionados(MenuPerfilParam param);

    public Long findRelacionadosSize(MenuPerfilParam param);

    public Boolean validToCreate(MenuPerfilParam param, boolean validarIdUsuario);
    
}
