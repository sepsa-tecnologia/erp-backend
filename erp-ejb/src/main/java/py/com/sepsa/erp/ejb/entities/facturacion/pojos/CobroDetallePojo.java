/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad cobro detalle
 *
 * @author Jonathan
 */
public class CobroDetallePojo {

    public CobroDetallePojo(Integer id, Integer idCobro, Integer nroLinea,
            Integer idFactura, String nroFactura, Date fechaFactura,
            Integer idTipoCobro, String tipoCobro, String codigoTipoCobro,
            Integer idCheque, BigDecimal montoCheque, String nroCheque,
            Date fechaEmisionCheque, BigDecimal montoCobro, Integer idEstado,
            String estado, String codigoEstado, Integer idConceptoCobro,
            String conceptoCobro, Integer idEntidadFinanciera,
            String entidadFinanciera, String descripcion, String nroRecibo) {
        this.id = id;
        this.idCobro = idCobro;
        this.nroLinea = nroLinea;
        this.idFactura = idFactura;
        this.nroFactura = nroFactura;
        this.fechaFactura = fechaFactura;
        this.idTipoCobro = idTipoCobro;
        this.tipoCobro = tipoCobro;
        this.codigoTipoCobro = codigoTipoCobro;
        this.idCheque = idCheque;
        this.montoCheque = montoCheque;
        this.nroCheque = nroCheque;
        this.fechaEmisionCheque = fechaEmisionCheque;
        this.montoCobro = montoCobro;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.idConceptoCobro = idConceptoCobro;
        this.conceptoCobro = conceptoCobro;
        this.idEntidadFinanciera = idEntidadFinanciera;
        this.entidadFinanciera = entidadFinanciera;
        this.descripcion = descripcion;
        this.nroRecibo = nroRecibo;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de cobro
     */
    private Integer idCobro;

    /**
     * Nro de linea
     */
    private Integer nroLinea;

    /**
     * Identificador de factura
     */
    private Integer idFactura;

    /**
     * Número de factura
     */
    private String nroFactura;

    /**
     * Número de recibo
     */
    private String nroRecibo;

    /**
     * Fecha de factura
     */
    private Date fechaFactura;

    /**
     * Identificador de tipo de cobro
     */
    private Integer idTipoCobro;

    /**
     * Tipo de cobro
     */
    private String tipoCobro;
    
    /**
     * Código de tipo de cobro
     */
    private String codigoTipoCobro;

    /**
     * Identificador de cheque
     */
    private Integer idCheque;

    /**
     * Nro cheque
     */
    private String nroCheque;
    
    /**
     * Fecha cheque
     */
    private Date fechaEmisionCheque;

    /**
     * Monto cobro
     */
    private BigDecimal montoCobro;

    /**
     * Monto cheque
     */
    private BigDecimal montoCheque;

    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;

    /**
     * Identificador de concepto de cobro
     */
    private Integer idConceptoCobro;

    /**
     * Concepto de cobro
     */
    private String conceptoCobro;

    /**
     * Identificador de entidad financiera
     */
    private Integer idEntidadFinanciera;

    /**
     * Entidad financiera
     */
    private String entidadFinanciera;

    /**
     * Descripción
     */
    private String descripcion;
    
    public Integer getIdCobro() {
        return idCobro;
    }

    public void setIdCobro(Integer idCobro) {
        this.idCobro = idCobro;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdTipoCobro() {
        return idTipoCobro;
    }

    public void setIdTipoCobro(Integer idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    public String getTipoCobro() {
        return tipoCobro;
    }

    public void setTipoCobro(String tipoCobro) {
        this.tipoCobro = tipoCobro;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getEstado() {
        return estado;
    }

    public Integer getIdConceptoCobro() {
        return idConceptoCobro;
    }

    public void setIdConceptoCobro(Integer idConceptoCobro) {
        this.idConceptoCobro = idConceptoCobro;
    }

    public String getConceptoCobro() {
        return conceptoCobro;
    }

    public void setConceptoCobro(String conceptoCobro) {
        this.conceptoCobro = conceptoCobro;
    }

    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

    public String getNroCheque() {
        return nroCheque;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setEntidadFinanciera(String entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public String getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setCodigoTipoCobro(String codigoTipoCobro) {
        this.codigoTipoCobro = codigoTipoCobro;
    }

    public String getCodigoTipoCobro() {
        return codigoTipoCobro;
    }

    public void setFechaEmisionCheque(Date fechaEmisionCheque) {
        this.fechaEmisionCheque = fechaEmisionCheque;
    }

    public Date getFechaEmisionCheque() {
        return fechaEmisionCheque;
    }

    public void setMontoCheque(BigDecimal montoCheque) {
        this.montoCheque = montoCheque;
    }

    public BigDecimal getMontoCheque() {
        return montoCheque;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }
}
