/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Configuracion;
import py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionValorParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "ConfiguracionValorFacade", mappedName = "ConfiguracionValorFacade")
@Local(ConfiguracionValorFacade.class)
public class ConfiguracionValorFacadeImpl extends FacadeImpl<ConfiguracionValor, ConfiguracionValorParam> implements ConfiguracionValorFacade {

    public ConfiguracionValorFacadeImpl() {
        super(ConfiguracionValor.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdConfiguracion Validar identificador de configuración
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(ConfiguracionValorParam param, boolean validarIdConfiguracion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }
        
        Configuracion configuracion = facades.getConfiguracionFacade()
                .find(param.getIdEmpresa(), param.getIdConfiguracion(),
                        param.getCodigoConfiguracion());
        
        if(validarIdConfiguracion && isNull(configuracion)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la configuración"));
        } else if(!isNull(configuracion)) {
            param.setIdConfiguracion(configuracion.getId());
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(ConfiguracionValorParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }
        
        ConfiguracionValor configuracionValor = find(param.getId());
        
        if(isNull(configuracionValor)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de configuración valor"));
        }
        
        Configuracion configuracion = facades.getConfiguracionFacade()
                .find(param.getIdEmpresa(), param.getIdConfiguracion(),
                        param.getCodigoConfiguracion());
        
        if(isNull(configuracion)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la configuración"));
        } else {
            param.setIdConfiguracion(configuracion.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public ConfiguracionValor create(ConfiguracionValorParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        ConfiguracionValor item = new ConfiguracionValor();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setIdConfiguracion(param.getIdConfiguracion());
        item.setValor(param.getValor());
        item.setActivo(param.getActivo());
        create(item);
        
        return item;
    }
    
    @Override
    public String getCVValor(Integer idEmpresa, String codigoConfiguracion) {
        
        ConfiguracionValorParam param = new ConfiguracionValorParam();
        param.setIdEmpresa(idEmpresa);
        param.setCodigoConfiguracion(codigoConfiguracion);
        param.setActivo('S');
        
        ConfiguracionValor configuracion = findFirst(param);
        return configuracion == null ? null : configuracion.getValor();
    }
    
    @Override
    public Integer getCVInteger(Integer idEmpresa, String codigoConfiguracion, Integer default_) {
        String valor = getCVValor(idEmpresa, codigoConfiguracion);
        return Units.execute(default_, () -> Integer.valueOf(valor.trim()));
    }
    
    @Override
    public String getCVString(Integer idEmpresa, String codigoConfiguracion, String default_) {
        String valor = getCVValor(idEmpresa, codigoConfiguracion);
        return valor == null ? default_ : valor;
    }
    
    @Override
    public Boolean getCVBoolean(Integer idEmpresa, String codigoConfiguracion, Boolean default_) {
        String valor = getCVValor(idEmpresa, codigoConfiguracion);
        return Units.execute(default_, () -> Boolean.valueOf(valor.trim()));
    }
    
    @Override
    public ConfiguracionValor edit(ConfiguracionValorParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ConfiguracionValor item = find(param.getId());
        item.setIdConfiguracion(param.getIdConfiguracion());
        item.setValor(param.getValor());
        item.setActivo(param.getActivo());
        edit(item);
        
        return item;
    }

    @Override
    public List<ConfiguracionValor> find(ConfiguracionValorParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(ConfiguracionValor.class);
        Root<ConfiguracionValor> root = cq.from(ConfiguracionValor.class);
        Join<ConfiguracionValor, Configuracion> configuracion = root.join("configuracion");
        Join<Configuracion, Empresa> empresa = root.join("empresa");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdConfiguracion() != null) {
            predList.add(qb.equal(root.get("idConfiguracion"), param.getIdConfiguracion()));
        }

        if (param.getCodigoConfiguracion() != null && !param.getCodigoConfiguracion().trim().isEmpty()) {
            predList.add(qb.equal(configuracion.get("codigo"), param.getCodigoConfiguracion().trim()));
        }

        if (param.getValor() != null && !param.getValor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("valor")), String.format("%%%s%%", param.getValor().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.asc(root.get("activo")), qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<ConfiguracionValor> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ConfiguracionValorParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ConfiguracionValor> root = cq.from(ConfiguracionValor.class);
        Join<ConfiguracionValor, Configuracion> configuracion = root.join("configuracion");
        Join<Configuracion, Empresa> empresa = root.join("empresa");
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdConfiguracion() != null) {
            predList.add(qb.equal(root.get("idConfiguracion"), param.getIdConfiguracion()));
        }

        if (param.getCodigoConfiguracion() != null && !param.getCodigoConfiguracion().trim().isEmpty()) {
            predList.add(qb.equal(configuracion.get("codigo"), param.getCodigoConfiguracion().trim()));
        }

        if (param.getValor() != null && !param.getValor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("valor")), String.format("%%%s%%", param.getValor().trim().toUpperCase())));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
