/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.LocalEntrega;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LocalEntregaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "LocalEntregaFacade", mappedName = "LocalEntregaFacade")
@Local(LocalEntregaFacade.class)
public class LocalEntregaFacadeImpl extends FacadeImpl<LocalEntrega, LocalEntregaParam> implements LocalEntregaFacade {

    public LocalEntregaFacadeImpl() {
        super(LocalEntrega.class);
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @param validarIdNc validar id nc
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(LocalEntregaParam param, boolean validarIdNr) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        return !param.tieneErrores();
    }

    /**
     * Crea una instancia de NotaCreditoDetalle
     *
     * @param param parámetros
     * @param userInfo Usuario
     * @return Instancia
     */
    @Override
    public LocalEntrega create(LocalEntregaParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        LocalEntrega le = new LocalEntrega();
        le.setDireccion(param.getDireccion());
        le.setNroCasa(param.getNroCasa());
        create(le);
        
        return le;
    }

    @Override
    public List<LocalEntrega> find(LocalEntregaParam param) {

        AbstractFind find = new AbstractFind(LocalEntrega.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, LocalEntregaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<LocalEntrega> root = cq.from(LocalEntrega.class);

        find.addPath("root", root);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(
                qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(LocalEntregaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<LocalEntrega> root = cq.from(LocalEntrega.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
