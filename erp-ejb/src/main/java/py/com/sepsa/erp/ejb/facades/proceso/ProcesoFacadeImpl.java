/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.FrecuenciaEjecucion;
import py.com.sepsa.erp.ejb.entities.proceso.Proceso;
import py.com.sepsa.erp.ejb.entities.proceso.TipoProceso;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ProcesoFacade", mappedName = "ProcesoFacade")
@Local(ProcesoFacade.class)
public class ProcesoFacadeImpl extends FacadeImpl<Proceso, ProcesoParam> implements ProcesoFacade {

    public ProcesoFacadeImpl() {
        super(Proceso.class);
    }

    public Boolean validToCreate(ProcesoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ProcesoParam param1 = new ProcesoParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigo(param.getCodigo().trim());
        
        Proceso item = findFirst(param1);
        
        if(!isNull(item)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        TipoProceso tipoProceso = facades.getTipoProcesoFacade()
                .find(param.getIdTipoProceso(), param.getCodigoTipoProceso());
        
        if(isNull(tipoProceso)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de proceso"));
        } else {
            
            param.setIdTipoProceso(tipoProceso.getId());
            
            if(Objects.equals(tipoProceso.getCodigo(), "EJECUCION_PERIODICA")
                    && (isNull(param.getFrecuenciaEjecucion()) && isNull(param.getIdFrecuenciaEjecucion()))) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("Debe indicar la frecuencia de ejecución"));
            }
        }
        
        if(!isNull(param.getIdFrecuenciaEjecucion())) {
            
            FrecuenciaEjecucion frecuenciaEjecucion = facades.getFrecuenciaEjecucionFacade().find(param.getIdFrecuenciaEjecucion());
            
            if(isNull(frecuenciaEjecucion)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la frecuencia de ejecución"));
            }
        }
        
        if(!isNull(param.getFrecuenciaEjecucion())) {
            facades.getFrecuenciaEjecucionFacade().validToCreate(param.getFrecuenciaEjecucion());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(ProcesoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Proceso tp = find(param.getId());
        
        if(isNull(tp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el proceso"));
        } else if(!tp.getCodigo().equals(param.getCodigo().trim())) {
            
            ProcesoParam param1 = new ProcesoParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setCodigo(param.getCodigo().trim());

            Proceso item = findFirst(param1);

            if(!isNull(item)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        TipoProceso tipoProceso = facades.getTipoProcesoFacade()
                .find(param.getIdTipoProceso(), param.getCodigoTipoProceso());
        
        if(isNull(tipoProceso)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de proceso"));
        } else {
            
            param.setIdTipoProceso(tipoProceso.getId());
            
            if(Objects.equals(tipoProceso.getCodigo(), "EJECUCION_PERIODICA")
                    && isNull(param.getIdFrecuenciaEjecucion())) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("Debe indicar la frecuencia de ejecución"));
            }
        }
        
        if(!isNull(param.getIdFrecuenciaEjecucion())) {
            
            FrecuenciaEjecucion fe = facades.getFrecuenciaEjecucionFacade().find(param.getIdFrecuenciaEjecucion());
            
            if(isNull(fe)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la frecuencia de ejecución"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Proceso create(ProcesoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        if(!isNull(param.getFrecuenciaEjecucion())) {
            FrecuenciaEjecucion fe = facades.getFrecuenciaEjecucionFacade().create(param.getFrecuenciaEjecucion(), userInfo);
            param.setIdFrecuenciaEjecucion(Units.execute(() -> fe.getId()));
        }
        
        Proceso item = new Proceso();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setDescripcion(param.getDescripcion().trim());
        item.setCodigo(param.getCodigo().trim());
        item.setIdTipoProceso(param.getIdTipoProceso());
        item.setActivo(param.getActivo());
        item.setIdFrecuenciaEjecucion(param.getIdFrecuenciaEjecucion());
        
        create(item);
        
        return item;
    }

    @Override
    public Proceso edit(ProcesoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Proceso item = find(param.getId());
        item.setDescripcion(param.getDescripcion().trim());
        item.setCodigo(param.getCodigo().trim());
        item.setIdTipoProceso(param.getIdTipoProceso());
        item.setActivo(param.getActivo());
        item.setIdFrecuenciaEjecucion(param.getIdFrecuenciaEjecucion());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<Proceso> find(ProcesoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(Proceso.class);
        Root<Proceso> root = cq.from(Proceso.class);
        Join<Proceso, TipoProceso> tipoProceso = root.join("tipoProceso");
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getIdTipoProceso() != null) {
            predList.add(qb.equal(root.get("idTipoProceso"), param.getIdTipoProceso()));
        }
        
        if(param.getCodigoTipoProceso() != null && !param.getCodigoTipoProceso().trim().isEmpty()) {
            predList.add(qb.equal(tipoProceso.get("codigo"), param.getCodigoTipoProceso().trim()));
        }
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.asc(root.get("activo")),
                qb.desc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Proceso> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ProcesoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Proceso> root = cq.from(Proceso.class);
        Join<Proceso, TipoProceso> tipoProceso = root.join("tipoProceso");
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getIdTipoProceso() != null) {
            predList.add(qb.equal(root.get("idTipoProceso"), param.getIdTipoProceso()));
        }
        
        if(param.getCodigoTipoProceso() != null && !param.getCodigoTipoProceso().trim().isEmpty()) {
            predList.add(qb.equal(tipoProceso.get("codigo"), param.getCodigoTipoProceso().trim()));
        }
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
