/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.pojos;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class UsuarioPerfilPojo {

    public UsuarioPerfilPojo() {
    }

    public UsuarioPerfilPojo(Integer id, Integer idUsuario, String usuario,
            Integer idPerfil, String codigoPerfil, String perfil,
            Integer idEstado, String estado, String codigoEstado) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idPerfil = idPerfil;
        this.codigoPerfil = codigoPerfil;
        this.perfil = perfil;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    private String usuario;
    
    /**
     * Identificador de perfil
     */
    private Integer idPerfil;
    
    /**
     * Código de perfil
     */
    private String codigoPerfil;
    
    /**
     * Perfil
     */
    private String perfil;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getCodigoPerfil() {
        return codigoPerfil;
    }

    public void setCodigoPerfil(String codigoPerfil) {
        this.codigoPerfil = codigoPerfil;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }
}
