/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.math.BigDecimal;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de producto relacionado
 * @author Williams Vera
 */
public class ProductoRelacionadoParam extends CommonParam {
    
    public ProductoRelacionadoParam() {
    }

    public ProductoRelacionadoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de Producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador del producto relacionado
     */
    @QueryParam("idProductoRelacionado")
    private Integer idProductoRelacionado;
      
    /**
     * Porcentaje de impuesto del producto relacionado
     */
    @QueryParam("porcentajeRelacionado")
    private BigDecimal porcentajeRelacionado;
    
    

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idProductoRelacionado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador del producto relacionado"));
        }
    
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(porcentajeRelacionado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor del porcentaje relacionado"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idProductoRelacionado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador del producto relacionado"));
        }
     
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(porcentajeRelacionado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor del porcentaje relacionado"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToListRelacionado() {
        
        super.isValidToList();
         
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador del producto"));
        }
    
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProductoRelacionado() {
        return idProductoRelacionado;
    }

    public void setIdProductoRelacionado(Integer idProductoRelacionado) {
        this.idProductoRelacionado = idProductoRelacionado;
    }

    public BigDecimal getPorcentajeRelacionado() {
        return porcentajeRelacionado;
    }

    public void setPorcentajeRelacionado(BigDecimal porcentajeRelacionado) {
        this.porcentajeRelacionado = porcentajeRelacionado;
    }
    
}
