/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * POJO para la entidad historico liquidacion
 * @author Jonathan
 */
public class HistoricoLiquidacionPojo implements Serializable {

    public HistoricoLiquidacionPojo() {
    }
    
    public HistoricoLiquidacionPojo(Integer idMoneda, String moneda, Date fechaDesde,
            Date fechaHasta, Integer idContrato, Integer idProducto, String producto,
            Integer idServicio, String servicio, Integer idHistoricoLiquidacion,
            Date fechaProceso, BigDecimal montoTotalDetalle, BigDecimal montoTotalExcedente,
            BigDecimal montoDescuentoGeneral, BigDecimal montoDescuentoExcedente,
            BigDecimal montoDescuentoDetalle, BigDecimal montoTotalLiquidado,
            Character montoMinimo, Character bonificado, Integer idMontoMinimo,
            Character cobroAdelantado, Integer idLiquidacion,
            Integer idCliente, String cliente) {
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.idContrato = idContrato;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.fechaProceso = fechaProceso;
        this.montoTotalDetalle = montoTotalDetalle;
        this.montoTotalExcedente = montoTotalExcedente;
        this.montoDescuentoGeneral = montoDescuentoGeneral;
        this.montoDescuentoExcedente = montoDescuentoExcedente;
        this.montoDescuentoDetalle = montoDescuentoDetalle;
        this.montoTotalLiquidado = montoTotalLiquidado;
        this.montoMinimo = montoMinimo;
        this.bonificado = bonificado;
        this.idMontoMinimo = idMontoMinimo;
        this.cobroAdelantado = cobroAdelantado;
        this.idLiquidacion = idLiquidacion;
        this.idCliente = idCliente;
        this.cliente = cliente;
    }
    
    /**
     * Identificador
     */
    private Integer idHistoricoLiquidacion;
    
    /**
     * Fecha de proceso
     */
    private Date fechaProceso;
    
    /**
     * Monto total detalle
     */
    private BigDecimal montoTotalDetalle;
    
    /**
     * Monto total excedente
     */
    private BigDecimal montoTotalExcedente;
    
    /**
     * Monto descuento general
     */
    private BigDecimal montoDescuentoGeneral;
    
    /**
     * Monto descuento excedente
     */
    private BigDecimal montoDescuentoExcedente;
    
    /**
     * Monto descuento detalle
     */
    private BigDecimal montoDescuentoDetalle;
    
    /**
     * Monto total liquidado
     */
    private BigDecimal montoTotalLiquidado;
    
    /**
     * Monto minimo
     */
    private Character montoMinimo;
    
    /**
     * Bonificador
     */
    private Character bonificado;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Identificador de monto minimo
     */
    private Integer idMontoMinimo;
    
    /**
     * Cobro adelantado
     */
    private Character cobroAdelantado;
    
    /**
     * Identificador de liquidacion
     */
    private Integer idLiquidacion;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Moneda
     */
    private String moneda;
    
    /**
     * Identificador de periodo de liquidación
     */
    private Integer idPeriodoLiquidacion;
    
    /**
     * Fecha desde
     */
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    private Date fechaHasta;
    
    /**
     * Identificador de contrato
     */
    private Integer idContrato;
    
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Servicio
     */
    private String servicio;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Identificador de grupo
     */
    private Integer idGrupo;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Comercial
     */
    private String comercial;
    
    /**
     * Cantidad de líneas
     */
    private Number cantLineas;
    
    /**
     * Linea
     */
    private BigDecimal linea;
    
    /**
     * Descuento
     */
    private BigDecimal descuento;
    
    /**
     * Total
     */
    private BigDecimal total;
    
    /**
     * Detalles
     */
    private List<LiquidacionDetallePojo> detalles;

    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public BigDecimal getMontoTotalDetalle() {
        return montoTotalDetalle;
    }

    public void setMontoTotalDetalle(BigDecimal montoTotalDetalle) {
        this.montoTotalDetalle = montoTotalDetalle;
    }

    public BigDecimal getMontoTotalExcedente() {
        return montoTotalExcedente;
    }

    public void setMontoTotalExcedente(BigDecimal montoTotalExcedente) {
        this.montoTotalExcedente = montoTotalExcedente;
    }

    public BigDecimal getMontoDescuentoGeneral() {
        return montoDescuentoGeneral;
    }

    public void setMontoDescuentoGeneral(BigDecimal montoDescuentoGeneral) {
        this.montoDescuentoGeneral = montoDescuentoGeneral;
    }

    public BigDecimal getMontoDescuentoExcedente() {
        return montoDescuentoExcedente;
    }

    public void setMontoDescuentoExcedente(BigDecimal montoDescuentoExcedente) {
        this.montoDescuentoExcedente = montoDescuentoExcedente;
    }

    public BigDecimal getMontoDescuentoDetalle() {
        return montoDescuentoDetalle;
    }

    public void setMontoDescuentoDetalle(BigDecimal montoDescuentoDetalle) {
        this.montoDescuentoDetalle = montoDescuentoDetalle;
    }

    public BigDecimal getMontoTotalLiquidado() {
        return montoTotalLiquidado;
    }

    public void setMontoTotalLiquidado(BigDecimal montoTotalLiquidado) {
        this.montoTotalLiquidado = montoTotalLiquidado;
    }

    public Character getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(Character montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getComercial() {
        return comercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public Number getCantLineas() {
        return cantLineas;
    }

    public void setCantLineas(Number cantLineas) {
        this.cantLineas = cantLineas;
    }

    public void setBonificado(Character bonificado) {
        this.bonificado = bonificado;
    }

    public Character getBonificado() {
        return bonificado;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setLinea(BigDecimal linea) {
        this.linea = linea;
    }

    public BigDecimal getLinea() {
        return linea;
    }

    public void setDescuento(BigDecimal descuento) {
        this.descuento = descuento;
    }

    public BigDecimal getDescuento() {
        return descuento;
    }

    public void setDetalles(List<LiquidacionDetallePojo> detalles) {
        this.detalles = detalles;
    }

    public List<LiquidacionDetallePojo> getDetalles() {
        return detalles;
    }

    public void setIdGrupo(Integer idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Integer getIdGrupo() {
        return idGrupo;
    }

    public void setIdPeriodoLiquidacion(Integer idPeriodoLiquidacion) {
        this.idPeriodoLiquidacion = idPeriodoLiquidacion;
    }

    public Integer getIdPeriodoLiquidacion() {
        return idPeriodoLiquidacion;
    }
}
