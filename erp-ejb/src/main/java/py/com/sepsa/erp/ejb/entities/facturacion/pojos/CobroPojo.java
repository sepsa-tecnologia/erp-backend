/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad cobro
 * @author Jonathan
 */
public class CobroPojo {

    public CobroPojo() {
    }

    public CobroPojo(Integer id, Integer idEmpresa, String empresa,
            Integer idCliente, String razonSocial, String nroDocumento, Integer idLugarCobro,
            String lugarCobro, Date fecha, BigDecimal montoCobro,
            String nroRecibo, Integer idTalonario, Integer idEstado,
            String estado, String codigoEstado, Integer idTransaccionRed,
            String idTransaccionRedPago, String redPago,
            Integer idMoneda, String moneda, String codigoMoneda,Integer idMotivoAnulacion,
            String motivoAnulacion, String observacionAnulacion,
            Date fechaEnvio, Character enviado, Character digital) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idCliente = idCliente;
        this.razonSocial = razonSocial;
        this.nroDocumento = nroDocumento;
        this.idLugarCobro = idLugarCobro;
        this.lugarCobro = lugarCobro;
        this.fecha = fecha;
        this.montoCobro = montoCobro;
        this.nroRecibo = nroRecibo;
        this.idTalonario = idTalonario;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.idTransaccionRed = idTransaccionRed;
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.codigoMoneda = codigoMoneda;
        this.idMotivoAnulacion = idMotivoAnulacion;
        this.motivoAnulacion = motivoAnulacion;
        this.observacionAnulacion = observacionAnulacion;
        this.fechaEnvio = fechaEnvio;
        this.enviado = enviado;
        this.digital = digital;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Razon social
     */
    private String razonSocial;
    
    /**
     * Nro de documento
     */
    private String nroDocumento;
    
    /**
     * Identificador de lugar de cobro
     */
    private Integer idLugarCobro;
    
    /**
     * Lugar de cobro
     */
    private String lugarCobro;
    
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * Monto de cobro
     */
    private BigDecimal montoCobro;
    
    /**
     * Nro de recibo
     */
    private String nroRecibo;
    
    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Identificador de transaccion red
     */
    private Integer idTransaccionRed;
    
    /**
     * Identificador de transacción de red de pago
     */
    private String idTransaccionRedPago;
    
    /**
     * Red de pago
     */
    private String redPago;
    
    /**
     * Motivo de anulación
     */
    private String motivoAnulacion;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Moneda
     */
    private String moneda;
    
    /**
     * Código de moneda
     */
    private String codigoMoneda;
    
    /**
     * Observación anulación
     */
    private String observacionAnulacion;
    
    /**
     * Fecha de envío
     */
    private Date fechaEnvio;
    
    /**
     * Enviado
     */
    private Character enviado;
    
    /**
     * Digital
     */
    private Character digital;
    
    /**
     * Saldo cliente
     */
    private BigDecimal saldoCliente;
    
    /**
     * Identificador de motivo de anulación
     */
    private Integer idMotivoAnulacion; 
    /**
     * Identificador de transaccion
     */
    private Integer idTransaccion;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdLugarCobro() {
        return idLugarCobro;
    }

    public void setIdLugarCobro(Integer idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    public String getLugarCobro() {
        return lugarCobro;
    }

    public void setLugarCobro(String lugarCobro) {
        this.lugarCobro = lugarCobro;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public Integer getIdTransaccionRed() {
        return idTransaccionRed;
    }

    public void setIdTransaccionRed(Integer idTransaccionRed) {
        this.idTransaccionRed = idTransaccionRed;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Character getEnviado() {
        return enviado;
    }

    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    public void setSaldoCliente(BigDecimal saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public BigDecimal getSaldoCliente() {
        return saldoCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getDigital() {
        return digital;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setRedPago(String redPago) {
        this.redPago = redPago;
    }

    public String getRedPago() {
        return redPago;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public String getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setIdTransaccionRedPago(String idTransaccionRedPago) {
        this.idTransaccionRedPago = idTransaccionRedPago;
    }

    public String getIdTransaccionRedPago() {
        return idTransaccionRedPago;
    }
    
    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }
}
