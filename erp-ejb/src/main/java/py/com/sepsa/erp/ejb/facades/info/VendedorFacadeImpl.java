/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.filters.VendedorParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.Vendedor;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.VendedorPojo;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Gustavo Benítez
 */
@Stateless(name = "VendedorFacade", mappedName = "VendedorFacade")
@Local(VendedorFacade.class)
public class VendedorFacadeImpl extends FacadeImpl<Vendedor, VendedorParam> implements VendedorFacade {

    public VendedorFacadeImpl() {
        super(Vendedor.class);
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param Parámetros
     * @param validarPersona Validar persona
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(VendedorParam param, Boolean validarPersona) {
        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        if (param.getIdEmpresa() != null) {
            Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());

            if (empresa == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la empresa"));
            }
        }

        if(param.getPersona() == null) {
            PersonaParam personaParam = new PersonaParam();
            personaParam.setId(param.getIdPersona());
            personaParam.setIdEmpresa(param.getIdEmpresa());
            Persona persona = facades.getPersonaFacade().findFirst(personaParam);
            if (validarPersona && persona == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la persona"));
            }
        }

        return !param.tieneErrores();
    }

    @Override
    public Vendedor create(VendedorParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        Integer idPersona;

        if (param.getPersona() != null) {
            Persona p = facades.getPersonaFacade().create(param.getPersona(), userInfo);
            idPersona = p.getId();
        } else {
            idPersona = param.getIdPersona();
        }

        Vendedor item = new Vendedor();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setIdPersona(idPersona);
        item.setActivo(param.getActivo());
        create(item);
        return item;
    }

    /**
     * Verifica si el objeto es válido para editar
     *
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToEdit(VendedorParam param) {
        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        if (param.getIdEmpresa() != null) {
            Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());

            if (empresa == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la empresa"));
            }
        }

        PersonaParam personaParam = new PersonaParam();
        Persona persona = facades.getPersonaFacade().findFirst(personaParam);

        if (persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }

        return !param.tieneErrores();
    }

    @Override
    public Vendedor edit(VendedorParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Vendedor item = find(param.getId());
        item.setIdEmpresa(param.getIdEmpresa());
        item.setIdPersona(param.getIdPersona());
        item.setActivo(param.getActivo());
        edit(item);
        return item;
    }

    @Override
    public List<Vendedor> find(VendedorParam param) {

        String where = "true";

        if (param.getId() != null) {
            where = String.format(" %s and v.id = %d", where, param.getId());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and v.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdPersona() != null) {
            where = String.format(" %s and v.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getActivo() != null) {
            where = String.format(" %s and v.activo = '%s'", where, param.getActivo());
        }

        String sql = String.format("SELECT v.id, v.id_empresa,\n"
                + "e.descripcion as empresa, v.id_persona, p.nombre, p.apellido, v.activo\n"
                + "FROM info.vendedor v\n"
                + "left join info.persona p on p.id = v.id_persona \n"
                + "left join info.empresa e on e.id = v.id_empresa \n"
                + "where %s order by v.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Vendedor.class);
        List<Vendedor> result = q.getResultList();

        return result;
    }

    @Override
    public List<VendedorPojo> findPojo(VendedorParam param) {

        String where = "true";

        if (param.getId() != null) {
            where = String.format(" %s and v.id = %d", where, param.getId());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and v.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdPersona() != null) {
            where = String.format(" %s and v.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getActivo() != null) {
            where = String.format(" %s and v.activo = '%s'", where, param.getActivo());
        }

        String sql = String.format("SELECT v.id, v.id_empresa,\n"
                + "e.descripcion as empresa, v.id_persona, p.nombre, p.apellido, v.activo\n"
                + "FROM info.vendedor v\n"
                + "left join info.persona p on p.id = v.id_persona \n"
                + "left join info.empresa e on e.id = v.id_empresa \n"
                + "where %s order by v.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        List<Object[]> list = q.getResultList();
        List<VendedorPojo> result = new ArrayList<>();

        for (Object[] objects : list) {
            int i = 0;
            VendedorPojo item = new VendedorPojo();
            item.setId((Integer) objects[i++]);
            item.setIdEmpresa((Integer) objects[i++]);
            item.setEmpresa((String) objects[i++]);
            item.setIdPersona((Integer) objects[i++]);
            item.setNombre((String) objects[i++]);
            item.setApellido((String) objects[i++]);
            item.setActivo((Character) objects[i++]);
            result.add(item);
        }

        return result;
    }

    @Override
    public Long findSize(VendedorParam param) {

        String where = "true";

        if (param.getId() != null) {
            where = String.format(" %s and v.id = %d", where, param.getId());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and v.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdPersona() != null) {
            where = String.format(" %s and v.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getActivo() != null) {
            where = String.format(" %s and v.activo = '%s'", where, param.getActivo());
        }
        String sql = String.format("select count(distinct v.id)\n"
                + "from info.vendedor v\n"
                + "left join info.persona p on p.id = v.id_persona \n"
                + "left join info.empresa e on e.id = v.id_empresa \n"
                + "where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number) object).longValue();

        return result;
    }
}
