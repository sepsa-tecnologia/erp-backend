/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import com.google.gson.JsonElement;
import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioPerfil;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioPerfilParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface UsuarioPerfilFacade extends Facade<UsuarioPerfil, UsuarioPerfilParam, UserInfoImpl> {

    public Boolean validToEdit(UsuarioPerfilParam param);

    public List<UsuarioPerfil> findRelacionados(UsuarioPerfilParam param);

    public Long findRelacionadosSize(UsuarioPerfilParam param);

    public Boolean validToCreate(UsuarioPerfilParam param, boolean validarIdUsuario);
    
    public JsonElement findPermisosById(UsuarioPerfilParam param);
    
}
