/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigInteger;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de usuario local
 * @author Jonathan
 */
public class TalonarioLocalParam extends CommonParam {
    
    public TalonarioLocalParam() {
    }

    public TalonarioLocalParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de talonario
     */
    @QueryParam("idTalonario")
    private Integer idTalonario;
    
    /**
     * Identificador de local
     */
    @QueryParam("idLocal")
    private Integer idLocal;
    
    /**
     * GLN de local
     */
    @QueryParam("glnLocal")
    private BigInteger glnLocal;
    
    /**
     * Identificador externo de local
     */
    @QueryParam("idExternoLocal")
    private String idExternoLocal;
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Local Externo
     */
    @QueryParam("localExterno")
    private Character localExterno;

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idLocal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de local"));
        }
        
        if(isNull(idTalonario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de talonario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToListRelacionado() {
        
        super.isValidToList();
        
        if(isNull(idTalonario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de talonario"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCreateMasivo() {
        
        limpiarErrores();
        
        if(isNull(idTalonario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de talonario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idLocal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de local"));
        }
        
        if(isNull(idTalonario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de talonario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public BigInteger getGlnLocal() {
        return glnLocal;
    }

    public void setGlnLocal(BigInteger glnLocal) {
        this.glnLocal = glnLocal;
    }

    public String getIdExternoLocal() {
        return idExternoLocal;
    }

    public void setIdExternoLocal(String idExternoLocal) {
        this.idExternoLocal = idExternoLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setLocalExterno(Character localExterno) {
        this.localExterno = localExterno;
    }

    public Character getLocalExterno() {
        return localExterno;
    }
}
