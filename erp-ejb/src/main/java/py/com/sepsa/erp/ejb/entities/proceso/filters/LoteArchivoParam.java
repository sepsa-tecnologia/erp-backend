/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de lote
 *
 * @author Jonathan
 */
public class LoteArchivoParam extends CommonParam {

    public LoteArchivoParam() {
    }

    public LoteArchivoParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de lote
     */
    private Integer idLote;

    /**
     * Nombre de archivo
     */
    private String nombreArchivo;

    /**
     * Contenido del lote
     */
    private String contenido;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        codigoEstado = "PENDIENTE";

        if (isNull(idLote)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de lote"));
        }

        if (isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código del reporte"));
        }

        if (isNullOrEmpty(contenido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el contenido en base64"));
        }

        if (isNullOrEmpty(nombreArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre del archivo"));
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de proceso"));
        }

        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdLote() {
        return idLote;
    }

    public void setIdLote(Integer idLote) {
        this.idLote = idLote;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

}
