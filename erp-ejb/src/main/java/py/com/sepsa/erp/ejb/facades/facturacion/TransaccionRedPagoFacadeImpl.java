/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.MensajeRedPago;
import py.com.sepsa.erp.ejb.entities.facturacion.OperacionRedPago;
import py.com.sepsa.erp.ejb.entities.facturacion.RedPago;
import py.com.sepsa.erp.ejb.entities.facturacion.TransaccionRedPago;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TransaccionRedParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TransaccionRedPagoFacade", mappedName = "TransaccionRedPagoFacade")
@Local(TransaccionRedPagoFacade.class)
public class TransaccionRedPagoFacadeImpl extends FacadeImpl<TransaccionRedPago, TransaccionRedParam> implements TransaccionRedPagoFacade {

    public TransaccionRedPagoFacadeImpl() {
        super(TransaccionRedPago.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return bandera
     */
    @Override
    public Boolean validToCreate(TransaccionRedParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MensajeRedPago mensajeRedPago = facades
                .getMensajeRedPagoFacade()
                .find(param.getIdMensaje());
        
        if(mensajeRedPago == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el mensaje de red de pago"));
        }
        
        OperacionRedPago operacionRedPago = facades
                .getOperacionRedPagoFacade()
                .find(param.getIdOperacion());
        
        if(operacionRedPago == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la operación de red de pago"));
        }
        
        RedPago redPago = facades
                .getRedPagoFacade()
                .find(param.getIdRed());
        
        if(redPago == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la red de pago"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia
     * @param param parámetros
     * @param userInfo Usuario
     * @return instancia
     */
    @Override
    public TransaccionRedPago create(TransaccionRedParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TransaccionRedPago transaccionRedPago = new TransaccionRedPago();
        transaccionRedPago.setIdEmpresa(userInfo.getIdEmpresa());
        transaccionRedPago.setFecha(param.getFecha());
        transaccionRedPago.setIdMensaje(param.getIdMensaje());
        transaccionRedPago.setIdOperacion(param.getIdOperacion());
        transaccionRedPago.setIdRed(param.getIdRed());
        transaccionRedPago.setIdTransaccion(param.getIdTransaccion());
        create(transaccionRedPago);
        
        return transaccionRedPago;
    }
}
