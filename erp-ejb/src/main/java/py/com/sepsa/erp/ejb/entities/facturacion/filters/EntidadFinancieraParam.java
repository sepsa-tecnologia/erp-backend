/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
public class EntidadFinancieraParam extends CommonParam {

    public EntidadFinancieraParam() {
    }

    public EntidadFinancieraParam(String bruto) {
        super(bruto);
    }

    /**
     * Nombre
     */
    @QueryParam("nombre")
    private String nombre;

    /**
     * Apellido
     */
    @QueryParam("apellido")
    private String apellido;

    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;

    /**
     * Nombre fantasia
     */
    @QueryParam("nombreFantasia")
    private String nombreFantasia;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public void setNombreFantasia(String nombreFantasia) {
        this.nombreFantasia = nombreFantasia;
    }
}
