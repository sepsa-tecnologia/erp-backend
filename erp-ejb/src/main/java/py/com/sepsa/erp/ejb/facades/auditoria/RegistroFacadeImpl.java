/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.Calendar;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.auditoria.Registro;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "RegistroFacade", mappedName = "RegistroFacade")
@Local(RegistroFacade.class)
public class RegistroFacadeImpl extends FacadeImpl<Registro, CommonParam> implements RegistroFacade {

    public RegistroFacadeImpl() {
        super(Registro.class);
    }

    /**
     * Metodo para la cracion de registros en auditoria de un servicio post
     *
     * @param esquema Esquema
     * @param tabla Tabla
     * @param atributos Atributos del objeto creado
     * @param userInfo Información de usuario
     */
    @Override
    public void create(String esquema, String tabla, Map<String, String> atributos, UserInfoImpl userInfo) {
        String keys = "", values = "";
        for (Map.Entry<String, String> entry : atributos.entrySet()) {
            if (entry.getValue() == null
                    || entry.getValue().trim().isEmpty()
                    || entry.getValue().trim().equalsIgnoreCase("null")) {
                continue;
            }

            keys += String.format("%s, ", entry.getKey().trim());
            values += String.format("'%s', ", entry.getValue().trim());
        }
        keys = keys.replaceFirst(", $", "");
        values = values.replaceFirst(", $", "");

        String sql = "INSERT INTO " + esquema + "." + tabla + "(" + keys + ") " + "VALUES(" + values + ")" + ";";
        //Setea los elementos de la tabla auditoria del esquema de Registro
        Registro auditoria = new Registro();
        auditoria.setIdOperacion(1);
        auditoria.setIdUsuario(userInfo.getId());
        auditoria.setSql(sql);
        auditoria.setFechaInsercion(Calendar.getInstance().getTime());
        create(auditoria);
    }

    /**
     * Metodo para la cracion de registros en auditoria de un servicio put
     *
     * @param esquema Esquema
     * @param tabla Tabla
     * @param atributos Atributos del objeto creado
     * @param userInfo Información de usuario
     * @param pk PK
     */
    @Override
    public void edit(String esquema, String tabla, Map<String, String> atributos, UserInfoImpl userInfo, Map<String, String> pk) {
        String values = "", conditions = "";

        for (Map.Entry<String, String> entry : pk.entrySet()) {
            if (entry.getValue() == null
                    || entry.getValue().trim().isEmpty()
                    || entry.getValue().trim().equalsIgnoreCase("null")) {
                continue;
            }

            conditions += String.format("%s = '%s' and ", entry.getKey().trim(), entry.getValue().trim());
        }

        for (Map.Entry<String, String> entry : atributos.entrySet()) {
            if (entry.getValue() == null
                    || entry.getValue().trim().isEmpty()
                    || entry.getValue().trim().equalsIgnoreCase("null")) {
                continue;
            }

            values += String.format("%s = '%s', ", entry.getKey(), entry.getValue());
        }
        conditions = conditions.replaceFirst(" and $", "");
        values = values.replaceFirst(", $", "");

        String sql = "UPDATE " + esquema + "." + tabla + " SET " + values + " WHERE " + conditions + ";";

        //Setea los elementos de la tabla auditoria del esquema de Registro
        Registro auditoria = new Registro();
        auditoria.setIdOperacion(3);
        auditoria.setIdUsuario(userInfo.getId());
        auditoria.setSql(sql);
        auditoria.setFechaInsercion(Calendar.getInstance().getTime());
        create(auditoria);
    }

    /**
     * Metodo para la cracion de registros en auditoria de un servicio delete
     *
     * @param esquema Esquema
     * @param tabla Tabla
     * @param userInfo Usuario
     * @param conditions
     */
    @Override
    public void delete(String esquema, String tabla, UserInfoImpl userInfo, Map<String, String> conditions) {
        String condition = "";
        for (Map.Entry<String, String> entry : conditions.entrySet()) {
            if (entry.getValue() == null
                    || entry.getValue().trim().isEmpty()
                    || entry.getValue().trim().equalsIgnoreCase("null")) {
                continue;
            }

            condition += String.format("%s = '%s' and", entry.getKey().trim(), entry.getValue().trim());
        }
        condition = condition.replaceFirst(" and $", "");

        String sql = "DELETE FROM " + esquema + "." + tabla + " WHERE " + condition + ";";

        //Setea los elementos de la tabla auditoria del esquema de Registro
        Registro auditoria = new Registro();
        auditoria.setIdOperacion(2);
        auditoria.setIdUsuario(userInfo.getId());
        auditoria.setSql(sql);
        auditoria.setFechaInsercion(Calendar.getInstance().getTime());
        create(auditoria);
    }

}
