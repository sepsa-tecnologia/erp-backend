/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoCobro;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoCobroParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoCobroFacade", mappedName = "TipoCobroFacade")
@Local(TipoCobroFacade.class)
public class TipoCobroFacadeImpl extends FacadeImpl<TipoCobro, TipoCobroParam> implements TipoCobroFacade {

    public TipoCobroFacadeImpl() {
        super(TipoCobro.class);
    }

    public Boolean validToCreate(TipoCobroParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoCobroParam param1 = new TipoCobroParam();
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(TipoCobroParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoCobro tipoCobro = find(param.getId());
        
        if(tipoCobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de cobro"));
        } else if(!tipoCobro.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoCobroParam param1 = new TipoCobroParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public TipoCobro create(TipoCobroParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoCobro tipoCobro = new TipoCobro();
        tipoCobro.setCodigo(param.getCodigo().trim());
        tipoCobro.setDescripcion(param.getDescripcion().trim());
        create(tipoCobro);
        
        return tipoCobro;
    }
    
    @Override
    public TipoCobro edit(TipoCobroParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoCobro tipoCobro = find(param.getId());
        tipoCobro.setCodigo(param.getCodigo().trim());
        tipoCobro.setDescripcion(param.getDescripcion().trim());
        edit(tipoCobro);
        
        return tipoCobro;
    }
    
    @Override
    public TipoCobro find(Integer id, String codigo) {
        TipoCobroParam param = new TipoCobroParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoCobro> find(TipoCobroParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoCobro.class);
        Root<TipoCobro> root = cq.from(TipoCobro.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<TipoCobro> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoCobroParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoCobro> root = cq.from(TipoCobro.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
