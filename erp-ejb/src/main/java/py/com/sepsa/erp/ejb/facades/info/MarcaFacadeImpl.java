/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Marca;
import py.com.sepsa.erp.ejb.entities.info.filters.MarcaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MarcaFacade", mappedName = "MarcaFacade")
@Local(MarcaFacade.class)
public class MarcaFacadeImpl extends FacadeImpl<Marca, MarcaParam> implements MarcaFacade {

    public MarcaFacadeImpl() {
        super(Marca.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(MarcaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MarcaParam param1 = new MarcaParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe una marca con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(MarcaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Marca marca = find(param.getId());
        
        if(marca == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la marca"));
        } else if(!marca.getCodigo().equals(param.getCodigo().trim())) {
        
            MarcaParam param1 = new MarcaParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe una marca con el código"));
            }
            
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Marca findOrCreate(MarcaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MarcaParam param1 = new MarcaParam();
        param1.setIdEmpresa(userInfo.getIdEmpresa());
        param1.setId(param.getId());
        param1.setCodigo(param.getCodigo());
        param1.isValidToList();
        
        Marca result = findFirst(param1);
        
        if(result == null) {
            result = create(param, userInfo);
        }
        
        return result;
    }
    
    @Override
    public Marca create(MarcaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Marca marca = new Marca();
        marca.setIdEmpresa(param.getIdEmpresa());
        marca.setDescripcion(param.getDescripcion().trim());
        marca.setCodigo(param.getCodigo().trim());

        create(marca);
        
        return marca;
    }
    
    @Override
    public Marca edit(MarcaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Marca marca = find(param.getId());
        marca.setDescripcion(param.getDescripcion().trim());
        marca.setCodigo(param.getCodigo().trim());
        
        edit(marca);
        
        return marca;
    }
    
    @Override
    public List<Marca> find(MarcaParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(Marca.class);
        Root<Marca> root = cq.from(Marca.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Marca> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(MarcaParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Marca> root = cq.from(Marca.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
