/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.pojos;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class UsuarioEmpresaPojo {

    public UsuarioEmpresaPojo() {
    }

    public UsuarioEmpresaPojo(Integer id, Integer idUsuario, String usuario,
            Integer idEmpresa, String codigoEmpresa, String empresa,
            Character activo, Character facturable, Integer idClienteSepsa) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idEmpresa = idEmpresa;
        this.codigoEmpresa = codigoEmpresa;
        this.empresa = empresa;
        this.activo = activo;
        this.facturable = facturable;
        this.idClienteSepsa = idClienteSepsa;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    private String usuario;
    
    /**
     * Identificador de cliente sepsa
     */
    private Integer idClienteSepsa;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Código de empresa
     */
    private String codigoEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de tipo de empresa
     */
    private Integer idTipoEmpresa;
    
    /**
     * Código de tipo de empresa
     */
    private String codigoTipoEmpresa;
    
    /**
     * Código de inventario
     */
    private Character moduloInventario;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Facturable
     */
    private Character facturable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setFacturable(Character facturable) {
        this.facturable = facturable;
    }

    public Character getFacturable() {
        return facturable;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setIdTipoEmpresa(Integer idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    public Integer getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    public void setModuloInventario(Character moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public Character getModuloInventario() {
        return moduloInventario;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }
}
