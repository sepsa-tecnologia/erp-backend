/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.Retencion;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionDetalleParam;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "RetencionDetalleFacade", mappedName = "RetencionDetalleFacade")
@Local(RetencionDetalleFacade.class)
public class RetencionDetalleFacadeImpl extends FacadeImpl<RetencionDetalle, RetencionDetalleParam> implements RetencionDetalleFacade {

    public RetencionDetalleFacadeImpl() {
        super(RetencionDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdRetencion validar idretencion
     * @return bandera
     */
    @Override
    public Boolean validToCreate(RetencionDetalleParam param, Boolean validarIdRetencion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Retencion retencion = facades.getRetencionFacade().find(param.getIdRetencion());
        
        if(validarIdRetencion && retencion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la retención"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "RETENCION_DETALLE");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        Factura factura = facades.getFacturaFacade().find(param.getIdFactura());
        
        if(factura == null) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura"));
            
        } else {
            if(factura.getFecha().after(param.getFecha())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la retención no puede ser anterior a la factura"));
            }

            if(!factura.getMontoTotalFactura().equals(param.getMontoTotal())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto total de la factura no corresponde con lo informado"));
            }

            if(!factura.getMontoIvaTotal().equals(param.getMontoIva())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto IVA total de la factura no corresponde con lo informado"));
            }

            if(!factura.getMontoImponibleTotal().equals(param.getMontoImponible())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto imponible total de la factura no corresponde con lo informado"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public RetencionDetalle create(RetencionDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, Boolean.TRUE)) {
            return null;
        }
        
        RetencionDetalle retencionDetalle = new RetencionDetalle();
        retencionDetalle.setIdRetencion(param.getIdRetencion());
        retencionDetalle.setIdFactura(param.getIdFactura());
        retencionDetalle.setMontoImponible(param.getMontoImponible());
        retencionDetalle.setMontoIva(param.getMontoIva());
        retencionDetalle.setMontoTotal(param.getMontoTotal());
        retencionDetalle.setMontoRetenido(param.getMontoRetenido());
        retencionDetalle.setIdEstado(param.getIdEstado());
        create(retencionDetalle);
        
        facades.getFacturaFacade().actualizarSaldoFactura(
                    param.getIdFactura(),
                    param.getMontoRetenido());
        
        return retencionDetalle;
    }

    @Override
    public List<RetencionDetalle> find(RetencionDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(RetencionDetalle.class);
        Root<RetencionDetalle> root = cq.from(RetencionDetalle.class);
        Join<RetencionDetalle, Estado> estado = root.join("estado");
                        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdRetencion() != null) {
            predList.add(qb.equal(root.get("idRetencion"), param.getIdRetencion()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<RetencionDetalle> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(RetencionDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<RetencionDetalle> root = cq.from(RetencionDetalle.class);
        Join<RetencionDetalle, Estado> estado = root.join("estado");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdRetencion() != null) {
            predList.add(qb.equal(root.get("idRetencion"), param.getIdRetencion()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
