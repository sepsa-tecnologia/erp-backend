/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de telefono
 * @author Jonathan
 */
public class TelefonoParam extends CommonParam {
    
    public TelefonoParam() {
    }

    public TelefonoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Prefino número
     */
    @QueryParam("prefijoNumero")
    private String prefijoNumero;
    
    /**
     * Numero
     */
    @QueryParam("numero")
    private String numero;
    
    /**
     * Prefijo
     */
    @QueryParam("prefijo")
    private String prefijo;
    
    /**
     * Principal
     */
    @QueryParam("principal")
    private Character principal;
    
    /**
     * Identificador de tipo de telefono
     */
    @QueryParam("idTipoTelefono")
    private Integer idTipoTelefono;
    
    /**
     * Código de tipo de telefono
     */
    @QueryParam("codigoTipoTelefono")
    private String codigoTipoTelefono;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoTelefono) && isNullOrEmpty(codigoTipoTelefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de telefono"));
        }
        
        if(isNull(principal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el telefono es principal"));
        }
        
        if(isNullOrEmpty(prefijo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el prefijo"));
        }
        
        if(isNullOrEmpty(numero)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idTipoTelefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de telefono"));
        }
        
        if(isNull(idTipoTelefono) && isNullOrEmpty(codigoTipoTelefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de telefono"));
        }
        
        if(isNull(principal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el telefono es principal"));
        }
        
        if(isNullOrEmpty(prefijo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el prefino"));
        }
        
        if(isNullOrEmpty(numero)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getIdTipoTelefono() {
        return idTipoTelefono;
    }

    public void setIdTipoTelefono(Integer idTipoTelefono) {
        this.idTipoTelefono = idTipoTelefono;
    }

    public void setPrefijoNumero(String prefijoNumero) {
        this.prefijoNumero = prefijoNumero;
    }

    public String getPrefijoNumero() {
        return prefijoNumero;
    }

    public void setCodigoTipoTelefono(String codigoTipoTelefono) {
        this.codigoTipoTelefono = codigoTipoTelefono;
    }

    public String getCodigoTipoTelefono() {
        return codigoTipoTelefono;
    }
}
