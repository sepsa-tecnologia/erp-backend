/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.pojos;

/**
 * POJO para la entidad config bonificacion
 * @author Jonathan
 */
public class ConfigBonificacionPojo {
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de producto bonificador
     */
    private Integer idProductoBonificador;
    
    /**
     * Producto bonificador
     */
    private String productoBonificador;
    
    /**
     * Identificador de servicio bonificador
     */
    private Integer idServicioBonificador;
    
    /**
     * Servicio bonificador
     */
    private String servicioBonificador;
    
    /**
     * Identificador de producto bonificado
     */
    private Integer idProductoBonificado;
    
    /**
     * Producto bonificado
     */
    private String productoBonificado;
    
    /**
     * Identificador de servicio bonificado;
     */
    private Integer idServicioBonificado;
    
    /**
     * Servicio bonificado
     */
    private String servicioBonificado;
    
    /**
     * Valor
     */
    private Number valor;
    
    /**
     * Porcentual
     */
    private Character porcentual;
    
    /**
     * Activo
     */
    private Character activo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdProductoBonificador() {
        return idProductoBonificador;
    }

    public void setIdProductoBonificador(Integer idProductoBonificador) {
        this.idProductoBonificador = idProductoBonificador;
    }

    public Integer getIdServicioBonificador() {
        return idServicioBonificador;
    }

    public void setIdServicioBonificador(Integer idServicioBonificador) {
        this.idServicioBonificador = idServicioBonificador;
    }

    public Integer getIdProductoBonificado() {
        return idProductoBonificado;
    }

    public void setIdProductoBonificado(Integer idProductoBonificado) {
        this.idProductoBonificado = idProductoBonificado;
    }

    public Integer getIdServicioBonificado() {
        return idServicioBonificado;
    }

    public void setIdServicioBonificado(Integer idServicioBonificado) {
        this.idServicioBonificado = idServicioBonificado;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public String getProductoBonificador() {
        return productoBonificador;
    }

    public void setProductoBonificador(String productoBonificador) {
        this.productoBonificador = productoBonificador;
    }

    public String getServicioBonificador() {
        return servicioBonificador;
    }

    public void setServicioBonificador(String servicioBonificador) {
        this.servicioBonificador = servicioBonificador;
    }

    public String getProductoBonificado() {
        return productoBonificado;
    }

    public void setProductoBonificado(String productoBonificado) {
        this.productoBonificado = productoBonificado;
    }

    public String getServicioBonificado() {
        return servicioBonificado;
    }

    public void setServicioBonificado(String servicioBonificado) {
        this.servicioBonificado = servicioBonificado;
    }

    public void setValor(Number valor) {
        this.valor = valor;
    }

    public Number getValor() {
        return valor;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Character getPorcentual() {
        return porcentual;
    }
}
