/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de notaDebito notificacion
 *
 * @author Williams Vera
 */
public class NotaDebitoNotificacionParam extends CommonParam {

    public NotaDebitoNotificacionParam() {
    }

    public NotaDebitoNotificacionParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de NC
     */
    @QueryParam("idNotaDebito")
    private Integer idNotaDebito;

    /**
     * Identificador de tipo de notificación
     */
    @QueryParam("idTipoNotificacion")
    private Integer idTipoNotificacion;

    /**
     * Código de tipo de notificación
     */
    @QueryParam("codigoTipoNotificacion")
    private String codigoTipoNotificacion;

    /**
     * Email
     */
    @QueryParam("email")
    private String email;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idNotaDebito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de notaDebito"));
        }

        if(isNull(idTipoNotificacion) && isNullOrEmpty(codigoTipoNotificacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo notificación"));
        }

        if (isNullOrEmpty(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdNotaDebito() {
        return idNotaDebito;
    }

    public void setIdNotaDebito(Integer idNotaDebito) {
        this.idNotaDebito = idNotaDebito;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
