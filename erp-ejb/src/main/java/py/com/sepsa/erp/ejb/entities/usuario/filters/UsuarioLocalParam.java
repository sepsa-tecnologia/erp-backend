/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.filters;

import java.math.BigInteger;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de usuario local
 * @author Jonathan
 */
public class UsuarioLocalParam extends CommonParam {
    
    public UsuarioLocalParam() {
    }

    public UsuarioLocalParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    @QueryParam("usuario")
    private String usuario;
    
    /**
     * Identificador de local
     */
    @QueryParam("idLocal")
    private Integer idLocal;
    
    /**
     * GLN de local
     */
    @QueryParam("glnLocal")
    private BigInteger glnLocal;
    
    /**
     * Identificador externo de local
     */
    @QueryParam("idExternoLocal")
    private String idExternoLocal;
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Local Externo
     */
    @QueryParam("localExterno")
    private Character localExterno;

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idLocal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de local"));
        }
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToListRelacionado() {
        
        super.isValidToList();
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCreateMasivo() {
        
        limpiarErrores();
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idLocal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de local"));
        }
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public BigInteger getGlnLocal() {
        return glnLocal;
    }

    public void setGlnLocal(BigInteger glnLocal) {
        this.glnLocal = glnLocal;
    }

    public String getIdExternoLocal() {
        return idExternoLocal;
    }

    public void setIdExternoLocal(String idExternoLocal) {
        this.idExternoLocal = idExternoLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setLocalExterno(Character localExterno) {
        this.localExterno = localExterno;
    }

    public Character getLocalExterno() {
        return localExterno;
    }
}
