
package py.com.sepsa.erp.ejb.externalservice.notificacion;

/**
 * POJO para la entidad tipo contenido
 * @author Daniel F. Escauriza Arza
 */
public class ContentType {

    /**
     * Identificador de la entidad tipo contenido
     */
    private Integer id;
    
    /**
     * Código del tipo de contenido
     */
    private String code;
    
    /**
     * Descripción del tipo de contenido
     */
    private String description;

    /**
     * Obtiene el identificador de la entidad tipo contenido
     * @return Identificador de la entidad tipo contenido
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador de la entidad tipo contenido
     * @param id Identificador de la entidad tipo contenido
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene el código del tipo de contenido
     * @return Código del tipo de contenido
     */
    public String getCode() {
        return code;
    }

    /**
     * Setea el código del tipo de contenido
     * @param code Código del tipo de contenido
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtiene la descripción del tipo de contenido
     * @return Descripción del tipo de contenido
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción del tipo de contenido
     * @param description Descripción del tipo de contenido
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Constructor de ContentType
     * @param id Identificador de la entidad tipo contenido
     * @param code Código del tipo de contenido
     * @param description Descripción del tipo de contenido
     */
    public ContentType(Integer id, String code, String description) {
        this.id = id;
        this.code = code;
        this.description = description;
    }

    /**
     * Constructor de ContentType
     * @param id Identificador de la entidad tipo contenido
     */
    public ContentType(Integer id) {
        this.id = id;
    }

    /**
     * Constructor de ContentType
     */
    public ContentType() {}
    
}
