/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.io.Serializable;

/**
 * POJO para la clase liquidación
 * @author Jonathan
 */
public class LiquidacionPojo implements Serializable {
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de historico liquidación
     */
    private Integer idHistoricoLiquidacion;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Moneda
     */
    private String moneda;
    
    /**
     * Identificador de periodo de liquidación
     */
    private Integer idPeriodoLiquidacion;
    
    /**
     * Identificador de contrato
     */
    private Integer idContrato;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Razón social
     */
    private String razonSocial;
    
    /**
     * Requiere OC
     */
    private Character requiereOc;
    
    /**
     * Facturación electrónica
     */
    private Character factElectronica;
    
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Servicio
     */
    private String servicio;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Cantidad
     */
    private Number cantidad;
    
    /**
     * Historico liquidación
     */
    private HistoricoLiquidacionPojo historicoLiquidacion;

    /*public void completarDatos(Facades facades) {
        
        if(idHistoricoLiquidacion != null) {
            
            HistoricoLiquidacionParam param = new HistoricoLiquidacionParam();
            param.setIdHistoricoLiquidacion(idHistoricoLiquidacion);
            param.isValidToList();
            List<HistoricoLiquidacionPojo> list = facades
                    .getHistoricoLiquidacionFacade()
                    .find(param);
            
            historicoLiquidacion = list == null || list.isEmpty() ? null : list.get(0);
        }
    }*/
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setIdPeriodoLiquidacion(Integer idPeriodoLiquidacion) {
        this.idPeriodoLiquidacion = idPeriodoLiquidacion;
    }

    public Integer getIdPeriodoLiquidacion() {
        return idPeriodoLiquidacion;
    }

    public void setCantidad(Number cantidad) {
        this.cantidad = cantidad;
    }

    public Number getCantidad() {
        return cantidad;
    }

    public void setRequiereOc(Character requiereOc) {
        this.requiereOc = requiereOc;
    }

    public Character getRequiereOc() {
        return requiereOc;
    }

    public void setFactElectronica(Character factElectronica) {
        this.factElectronica = factElectronica;
    }

    public Character getFactElectronica() {
        return factElectronica;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setHistoricoLiquidacion(HistoricoLiquidacionPojo historicoLiquidacion) {
        this.historicoLiquidacion = historicoLiquidacion;
    }

    public HistoricoLiquidacionPojo getHistoricoLiquidacion() {
        return historicoLiquidacion;
    }
}
