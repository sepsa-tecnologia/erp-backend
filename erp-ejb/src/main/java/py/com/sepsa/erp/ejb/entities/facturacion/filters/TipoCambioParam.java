/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
public class TipoCambioParam extends CommonParam {
    
    public TipoCambioParam() {
    }

    public TipoCambioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;

    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;

    /**
     * Valor de compra
     */
    @QueryParam("compra")
    private BigDecimal compra;

    /**
     * Valor de venta
     */
    @QueryParam("venta")
    private BigDecimal venta;

    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }
        
        if(isNull(compra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor de compra"));
        }
        
        if(isNull(venta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor de venta"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }
        
        if(isNull(compra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor de compra"));
        }
        
        if(isNull(venta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor de venta"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public BigDecimal getCompra() {
        return compra;
    }

    public void setCompra(BigDecimal compra) {
        this.compra = compra;
    }

    public BigDecimal getVenta() {
        return venta;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }
}
