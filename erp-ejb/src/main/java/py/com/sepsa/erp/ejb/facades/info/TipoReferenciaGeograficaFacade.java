/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.TipoReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface TipoReferenciaGeograficaFacade extends Facade<TipoReferenciaGeografica, TipoReferenciaGeograficaParam, UserInfoImpl> {
    
    public TipoReferenciaGeografica find(Integer id, String codigo);
}
