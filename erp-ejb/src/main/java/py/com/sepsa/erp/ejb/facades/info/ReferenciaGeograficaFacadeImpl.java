/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.TipoReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.ReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ReferenciaGeograficaFacade", mappedName = "ReferenciaGeograficaFacade")
@Local(ReferenciaGeograficaFacade.class)
public class ReferenciaGeograficaFacadeImpl extends FacadeImpl<ReferenciaGeografica, ReferenciaGeograficaParam> implements ReferenciaGeograficaFacade {

    public ReferenciaGeograficaFacadeImpl() {
        super(ReferenciaGeografica.class);
    }
    
    public boolean validToCreate(ReferenciaGeograficaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ReferenciaGeografica rg = find(null, param.getCodigo().trim());
        
        if(!isNull(rg)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        if(!isNull(param.getIdPadre())) {
            
            ReferenciaGeografica padre = find(param.getIdPadre(), param.getCodigoPadre());
            
            if(isNull(padre)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el padre"));
            } else {
                param.setIdPadre(padre.getId());
            }
        }
        
        TipoReferenciaGeografica trg = facades.getTipoReferenciaGeograficaFacade()
                .find(param.getIdTipoReferenciaGeografica(), param.getCodigoTipoReferenciaGeografica());
        
        if(isNull(trg)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de referencia geográfica"));
        } else {
            param.setIdTipoReferenciaGeografica(trg.getId());
        }
        
        return !param.tieneErrores();
    }
    
    public boolean validToEdit(ReferenciaGeograficaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ReferenciaGeografica rg = find(param.getId());
        
        if(isNull(rg)) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
            
        } else if (!rg.getCodigo().equals(param.getCodigo().trim())) {
            
            ReferenciaGeografica rg1 = find(null, param.getCodigo().trim());
        
            if(!isNull(rg1)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        if(!isNull(param.getIdPadre())) {
            
            ReferenciaGeografica padre = find(param.getIdPadre(), param.getCodigoPadre());
            
            if(isNull(padre)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el padre"));
            } else {
                param.setIdPadre(padre.getId());
            }
        }
        
        TipoReferenciaGeografica trg = facades.getTipoReferenciaGeograficaFacade()
                .find(param.getIdTipoReferenciaGeografica(), param.getCodigoTipoReferenciaGeografica());
        
        if(isNull(trg)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de referencia geográfica"));
        } else {
            param.setIdTipoReferenciaGeografica(trg.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public ReferenciaGeografica create(ReferenciaGeograficaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        ReferenciaGeografica result = new ReferenciaGeografica();
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        result.setIdPadre(param.getIdPadre());
        result.setIdTipoReferenciaGeografica(param.getIdTipoReferenciaGeografica());
        
        create(result);
        
        return result;
    }

    @Override
    public ReferenciaGeografica edit(ReferenciaGeograficaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ReferenciaGeografica result = find(param.getId());
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        result.setIdPadre(param.getIdPadre());
        result.setIdTipoReferenciaGeografica(param.getIdTipoReferenciaGeografica());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public ReferenciaGeografica find(Integer id, String codigo) {
        ReferenciaGeograficaParam param = new ReferenciaGeograficaParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<ReferenciaGeografica> find(ReferenciaGeograficaParam param) {
        
        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and rf.id = %d", where, param.getId());
        }
        
        if(param.getIdTipoReferenciaGeografica() != null) {
            where = String.format("%s and rf.id_tipo_referencia_geografica = %d", where, param.getIdTipoReferenciaGeografica());
        }
        
        if(param.getIdPadre() != null) {
            where = String.format("%s and rf.id_padre = %d", where, param.getIdPadre());
        }
        
        if(param.getTienePadre() != null) {
            where = param.getTienePadre()
                    ? String.format("%s and not (rf.id_padre is null)", where)
                    : String.format("%s and rf.id_padre is null", where);
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            where = String.format("%s and rf.codigo = '%s'", where, param.getCodigo().trim());
        }
        
        if(param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format("%s and rf.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }
        
        String sql = String.format("select rf.*"
                + " from info.referencia_geografica rf"
                + " left join info.referencia_geografica rfp on rfp.id = rf.id_padre"
                + " where %s order by rf.descripcion offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, ReferenciaGeografica.class);
        
        List<ReferenciaGeografica> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ReferenciaGeograficaParam param) {
        
        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and rf.id = %d", where, param.getId());
        }
        
        if(param.getIdTipoReferenciaGeografica() != null) {
            where = String.format("%s and rf.id_tipo_referencia_geografica = %d", where, param.getIdTipoReferenciaGeografica());
        }
        
        if(param.getIdPadre() != null) {
            where = String.format("%s and rf.id_padre = %d", where, param.getIdPadre());
        }
        
        if(param.getTienePadre() != null) {
            where = param.getTienePadre()
                    ? String.format("%s and not (rf.id_padre is null)", where)
                    : String.format("%s and rf.id_padre is null", where);
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            where = String.format("%s and rf.codigo = '%s'", where, param.getCodigo().trim());
        }
        
        if(param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format("%s and rf.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }
        
        String sql = String.format("select count(rf.id)"
                + " from info.referencia_geografica rf"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
