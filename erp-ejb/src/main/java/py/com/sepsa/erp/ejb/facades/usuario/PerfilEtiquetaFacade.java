/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.PerfilEtiqueta;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Local
public interface PerfilEtiquetaFacade extends Facade<PerfilEtiqueta, CommonParam, UserInfoImpl> {
    
}
