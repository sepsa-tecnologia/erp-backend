/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.pojos;

import java.io.Serializable;

/**
 * POJO para el manejo de cliente servicio
 * @author Jonathan D. Bernal Fernández
 */
public class ClienteServicioPojo implements Serializable {

    public ClienteServicioPojo() {
    }
    
    public ClienteServicioPojo(Integer idCliente, String razonSocial,
            Integer idProducto, String producto,
            Integer idServicio, String servicio, Character estado) {
        this.idCliente = idCliente;
        this.razonSocial = razonSocial;
        this.idProducto = idProducto;
        this.producto = producto;
        this.idServicio = idServicio;
        this.servicio = servicio;
        this.estado = estado;
    }
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Razon social
     */
    private String razonSocial;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Servicio
     */
    private String servicio;
    
    /**
     * Estado
     */
    private Character estado;
    
    /**
     * Tiene
     */
    public Boolean tiene;

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }
    public void setTiene(Boolean tiene) {
        this.tiene = tiene;
    }

    public Boolean getTiene() {
        return tiene;
    }
}
