/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de menu
 * @author Jonathan
 */
public class MenuParam extends CommonParam {

    public MenuParam() {
    }

    public MenuParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Titulo
     */
    @QueryParam("titulo")
    private String titulo;
    
    /**
     * Identificador de padre
     */
    @QueryParam("idPadre")
    private Integer idPadre;
    
    /**
     * Orden
     */
    @QueryParam("orden")
    private Integer orden;
    
    /**
     * Identificador de tipo menu
     */
    @QueryParam("idTipoMenu")
    private Integer idTipoMenu;
    
    /**
     * Código de tipo menu
     */
    @QueryParam("codigoTipoMenu")
    private String codigoTipoMenu;
    
    /**
     * URL
     */
    @QueryParam("url")
    private String url;
    
    /**
     * Tiene padre
     */
    @QueryParam("tienePadre")
    private Boolean tienePadre;
    
    /**
     * Tiene hijo
     */
    @QueryParam("tieneHijo")
    private Boolean tieneHijo;
    
    private List<MenuPerfilParam> menuPerfiles;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNullOrEmpty(titulo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el titulo"));
        }
        
        if(isNullOrEmpty(url)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la url"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(orden)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el orden"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(idTipoMenu) && isNullOrEmpty(codigoTipoMenu)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de menú"));
        }
        
        if(!isNullOrEmpty(menuPerfiles)) {
            for (MenuPerfilParam item : menuPerfiles) {
                item.setIdMenu(0);
                item.setIdEmpresa(idEmpresa);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de menú"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNullOrEmpty(titulo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el titulo"));
        }
        
        if(isNullOrEmpty(url)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la url"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(orden)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el orden"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(idTipoMenu) && isNullOrEmpty(codigoTipoMenu)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de menú"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNullOrEmpty(menuPerfiles)) {
            for (MenuPerfilParam item : menuPerfiles) {
                list.addAll(item.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setTienePadre(Boolean tienePadre) {
        this.tienePadre = tienePadre;
    }

    public Boolean getTienePadre() {
        return tienePadre;
    }

    public void setTieneHijo(Boolean tieneHijo) {
        this.tieneHijo = tieneHijo;
    }

    public Boolean getTieneHijo() {
        return tieneHijo;
    }

    public void setCodigoTipoMenu(String codigoTipoMenu) {
        this.codigoTipoMenu = codigoTipoMenu;
    }

    public String getCodigoTipoMenu() {
        return codigoTipoMenu;
    }

    public void setIdTipoMenu(Integer idTipoMenu) {
        this.idTipoMenu = idTipoMenu;
    }

    public Integer getIdTipoMenu() {
        return idTipoMenu;
    }

    public List<MenuPerfilParam> getMenuPerfiles() {
        return menuPerfiles;
    }

    public void setMenuPerfiles(List<MenuPerfilParam> menuPerfiles) {
        this.menuPerfiles = menuPerfiles;
    }
 
}
