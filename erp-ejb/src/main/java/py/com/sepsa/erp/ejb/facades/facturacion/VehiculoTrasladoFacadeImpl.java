/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.VehiculoTraslado;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.VehiculoTrasladoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "VehiculoTrasladoFacade", mappedName = "VehiculoTrasladoFacade")
@Local(VehiculoTrasladoFacade.class)
public class VehiculoTrasladoFacadeImpl extends FacadeImpl<VehiculoTraslado, VehiculoTrasladoParam> implements VehiculoTrasladoFacade {

    public VehiculoTrasladoFacadeImpl() {
        super(VehiculoTraslado.class);
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @param validarIdNc validar id nc
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(VehiculoTrasladoParam param, boolean validarIdNr) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        return !param.tieneErrores();
    }

    
    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(VehiculoTrasladoParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        VehiculoTraslado vt = find(param.getId());

        if (vt == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el vehículo"));
        } 

        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de NotaCreditoDetalle
     *
     * @param param parámetros
     * @param userInfo Usuario
     * @return Instancia
     */
    @Override
    public VehiculoTraslado create(VehiculoTrasladoParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        VehiculoTraslado vt = new VehiculoTraslado();
        vt.setMarca(param.getMarca());
        vt.setTipoVehiculo(param.getTipoVehiculo());
        vt.setTipoIdentificacion(param.getTipoIdentificacion());
        vt.setNroIdentificacion(param.getNroIdentificacion());
        vt.setNroMatricula(param.getNroMatricula());
        vt.setNroVuelo(param.getNroVuelo());
        vt.setIdEmpresa(param.getIdEmpresa());
        vt.setActivo('S');
        
        create(vt);
        
        return vt;
    }
    
    @Override
    public VehiculoTraslado edit(VehiculoTrasladoParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        VehiculoTraslado vt = new VehiculoTraslado();
        vt.setId(param.getId());
        vt.setMarca(param.getMarca());
        vt.setTipoVehiculo(param.getTipoVehiculo());
        vt.setTipoIdentificacion(param.getTipoIdentificacion());
        vt.setNroIdentificacion(param.getNroIdentificacion());
        vt.setNroMatricula(param.getNroMatricula());
        vt.setNroVuelo(param.getNroVuelo());
        vt.setIdEmpresa(param.getIdEmpresa());
        vt.setActivo(param.getActivo());
        
        edit(vt);

        return vt;
    }

    @Override
    public List<VehiculoTraslado> find(VehiculoTrasladoParam param) {

        AbstractFind find = new AbstractFind(VehiculoTraslado.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, VehiculoTrasladoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<VehiculoTraslado> root = cq.from(VehiculoTraslado.class);

        find.addPath("root", root);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getMarca()!= null && !param.getMarca().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("marca")),
                    String.format("%%%s%%", param.getMarca().trim().toLowerCase())));
        }
        
        if (param.getNroMatricula()!= null && !param.getNroMatricula().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("nroMatricula")),
                    String.format("%%%s%%", param.getNroMatricula().trim())));
        }
        
        if (param.getTipoIdentificacion()!= null) {
            predList.add(qb.equal(root.get("tipoIdentificacion"), param.getTipoIdentificacion()));
        }
        
        if (param.getNroIdentificacion()!= null && !param.getNroIdentificacion().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("nroIdentificacion")),
                    String.format("%%%s%%", param.getNroIdentificacion().trim())));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(
                qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(VehiculoTrasladoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<VehiculoTraslado> root = cq.from(VehiculoTraslado.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getMarca()!= null && !param.getMarca().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("marca")),
                    String.format("%%%s%%", param.getMarca().trim().toLowerCase())));
        }
        
        if (param.getNroMatricula()!= null && !param.getNroMatricula().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("nroMatricula")),
                    String.format("%%%s%%", param.getNroMatricula().trim())));
        }
        
        if (param.getTipoIdentificacion()!= null) {
            predList.add(qb.equal(root.get("tipoIdentificacion"), param.getTipoIdentificacion()));
        }
        
        if (param.getNroIdentificacion()!= null && !param.getNroIdentificacion().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("nroIdentificacion")),
                    String.format("%%%s%%", param.getNroIdentificacion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
