/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.auditoria.AuditoriaDetalle;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Marca;
import py.com.sepsa.erp.ejb.entities.info.Metrica;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.TipoProducto;
import py.com.sepsa.erp.ejb.entities.info.filters.MetricaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoRelacionadoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ProductoFacade", mappedName = "ProductoFacade")
@Local(ProductoFacade.class)
public class ProductoFacadeImpl extends FacadeImpl<Producto, ProductoParam> implements ProductoFacade {

    public ProductoFacadeImpl() {
        super(Producto.class);
    }

    @Override
    public Map<String, String> getMapConditions(Producto item) {
        Map<String, String> atributos = super.getMapConditions(item);

        atributos.put("id", item.getId() + " ");
        atributos.put("id_empresa", item.getIdEmpresa() + " ");
        atributos.put("codigo_gtin", item.getCodigoGtin() + " ");
        atributos.put("codigo_interno", item.getCodigoInterno() + " ");
        atributos.put("cuenta_contable", item.getCuentaContable() + " ");
        atributos.put("descripcion", item.getDescripcion() + " ");
        atributos.put("activo", item.getActivo() + " ");
        atributos.put("id_marca", item.getIdMarca() + " ");
        atributos.put("id_metrica", item.getIdMetrica() + " ");
        atributos.put("fecha_insercion", item.getFechaInsercion() + " ");
        atributos.put("multiplo_umv", item.getMultiploUmv() + " ");
        atributos.put("porcentaje_impuesto", item.getPorcentajeImpuesto() + " ");
        atributos.put("umv", item.getUmv() + " ");

        return atributos;
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(ProductoParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        ProductoParam param1 = new ProductoParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigoGtinEq(param.getCodigoGtin().trim());

        Long size = findSize(param1);

        if (size > 0 && !param.getCodigoGtin().equalsIgnoreCase("0")) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un producto con el código gtin"));
        }
        
//        if (param.getNroLote() != null || param.getFechaVencimientoLote() != null) {
//            ProductoParam param2 = new ProductoParam();
//            param2.setIdEmpresa(param.getIdEmpresa());
//            param2.setNroLote(param.getNroLote());
//            param2.setFechaVencimientoLote(param.getFechaVencimientoLote());
//
//            Long size2 = findSize(param2);
//
//            if (size2 > 0) {
//                param.addError(MensajePojo.createInstance()
//                        .descripcion("Ya existe un producto con ese número/fechaVencimiento de lote"));
//            }
//            
//        }

        if (!isNull(param.getIdMarca())) {
            Marca marca = facades.getMarcaFacade().find(param.getIdMarca());

            if (isNull(marca)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la marca"));
            } else {
                param.setIdMarca(marca.getId());
            }
        }

        if (!isNull(param.getIdMetrica())) {
            MetricaParam filter = new MetricaParam();
            
            filter.setId(param.getIdMetrica());
            Metrica metrica = facades.getMetricaFacade().findFirst(filter);

            if (isNull(metrica)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la métrica"));
            } else {
                param.setIdMetrica(metrica.getId());
                param.setCodigoMetrica(metrica.getCodigo());
            }
        }

        if (!isNull(param.getMarca())) {
            facades.getMarcaFacade().validToCreate(param.getMarca());
        }

        return !param.tieneErrores();
    }

    /**
     * Verifica si el objeto es válido para editar
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(ProductoParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        Producto producto = find(param.getId());

        if (producto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el producto"));
        } else if (!producto.getCodigoGtin().equals(param.getCodigoGtin().trim())) {

            ProductoParam param1 = new ProductoParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setCodigoGtinEq(param.getCodigoGtin().trim());

            Long size = findSize(param1);

            if (size > 0 && !param.getCodigoGtin().equalsIgnoreCase("0")) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un producto con el código gtin"));
            }
        }

        if (!isNull(param.getIdMarca())) {
            Marca marca = facades.getMarcaFacade().find(param.getIdMarca());

            if (isNull(marca)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la marca"));
            } else {
                param.setIdMarca(marca.getId());
            }
        }

        if (!isNull(param.getIdMetrica())) {
            MetricaParam filter = new MetricaParam();
            
            filter.setId(param.getIdMetrica());
            Metrica metrica = facades.getMetricaFacade().findFirst(filter);

            if (isNull(metrica)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la métrica"));
            } else {
                param.setIdMetrica(metrica.getId());
                param.setCodigoMetrica(metrica.getCodigo());
            }
        } 
        
//        if (param.getNroLote() != null || param.getFechaVencimientoLote() != null) {
//            ProductoParam param2 = new ProductoParam();
//            param2.setIdEmpresa(param.getIdEmpresa());
//            param2.setNroLote(param.getNroLote());
//            param2.setFechaVencimientoLote(param.getFechaVencimientoLote());
//
//            Long size2 = findSize(param2);
//
//            if (size2 > 0) {
//                param.addError(MensajePojo.createInstance()
//                        .descripcion("Ya existe un producto con ese número/fechaVencimiento de lote"));
//            }
//        }

        return !param.tieneErrores();
    }
    
    
    public void relacionarProducto(ProductoParam param, UserInfoImpl userInfo){
        
        ProductoRelacionadoParam pr = new ProductoRelacionadoParam();
        pr.setIdProducto(param.getIdPadre());
        pr.setIdProductoRelacionado(param.getId());
        pr.setActivo('S');
        pr.setIdEmpresa(param.getIdEmpresa());
        pr.setPorcentajeRelacionado(param.getPorcentajeRelacionado());
        
        facades.getProductoRelacionadoFacade().create(pr, userInfo);
        
    }

    @Override
    public Producto create(ProductoParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        if (!isNull(param.getMarca())) {
            Marca marca = facades.getMarcaFacade().findOrCreate(param.getMarca(), userInfo);
            param.setIdMarca(marca == null ? null : marca.getId());
        }

        Producto producto = new Producto();
        producto.setIdEmpresa(param.getIdEmpresa());
        producto.setIdMarca(param.getIdMarca());
        producto.setDescripcion(param.getDescripcion());
        producto.setCodigoGtin(param.getCodigoGtin().trim());
        producto.setCodigoInterno(param.getCodigoInterno().trim());
        producto.setCuentaContable(param.getCuentaContable() == null
                ? null
                : param.getCuentaContable().trim());
        producto.setPorcentajeImpuesto(param.getPorcentajeImpuesto());
        producto.setActivo(param.getActivo());
        producto.setIdMetrica(param.getIdMetrica());
        producto.setUmv(param.getUmv());
        producto.setMultiploUmv(param.getMultiploUmv());
        producto.setFechaInsercion(Calendar.getInstance().getTime());
        producto.setNroLote(param.getNroLote());
        producto.setFechaVencimientoLote(param.getFechaVencimientoLote());
        producto.setIdTipoProducto(param.getIdTipoProducto());
        producto.setIdPadre(param.getIdPadre());

        Map<String, String> atributos = getMapConditions(producto);
        AuditoriaDetalle ad = facades.getValorColumnaFacade().nuevaInstancia("info.producto", atributos, userInfo);

        producto.setIdAuditoria(ad.getIdAuditoria());
        producto.setIdAuditoriaDetalle(ad.getId());
        create(producto);

        if (param.getProductosRelacionados() != null && !param.getProductosRelacionados().isEmpty()){
            for (ProductoParam p : param.getProductosRelacionados()){
                p.setIdPadre(producto.getId());
                relacionarProducto(p, userInfo);
            }
            
        }
        
        return producto;
    }

    @Override
    public Producto edit(ProductoParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Producto producto = find(param.getId());
        producto.setIdEmpresa(param.getIdEmpresa());
        producto.setIdMarca(param.getIdMarca());
        producto.setDescripcion(param.getDescripcion().trim());
        producto.setCodigoGtin(param.getCodigoGtin().trim());
        producto.setCodigoInterno(param.getCodigoInterno().trim());
        producto.setCuentaContable(param.getCuentaContable() == null
                ? null
                : param.getCuentaContable().trim());
        producto.setPorcentajeImpuesto(param.getPorcentajeImpuesto());
        producto.setActivo(param.getActivo());
        producto.setIdMetrica(param.getIdMetrica());
        producto.setUmv(param.getUmv());
        producto.setMultiploUmv(param.getMultiploUmv());
        producto.setNroLote(param.getNroLote());
        producto.setFechaVencimientoLote(param.getFechaVencimientoLote());
        producto.setIdTipoProducto(param.getIdTipoProducto());
        
        Map<String, String> atributos = getMapConditions(producto);
        AuditoriaDetalle ad = facades.getValorColumnaFacade().editarInstancia(producto.getIdAuditoria(), "info.producto", atributos, userInfo);

        producto.setIdAuditoria(ad.getIdAuditoria());
        producto.setIdAuditoriaDetalle(ad.getId());
        edit(producto);

        return producto;
    }

    @Override
    public Producto find(Integer idEmpresa, Integer id, String codigoGtin) {
        ProductoParam param = new ProductoParam();
        param.setIdEmpresa(idEmpresa);
        param.setId(id);
        param.setCodigoGtinEq(codigoGtin);
        param.isValidToList();

        return findFirst(param);
    }

    @Override
    public ProductoPojo findPojo(Integer idEmpresa, Integer id, String codigoGtin) {
        ProductoParam param = new ProductoParam();
        param.setIdEmpresa(idEmpresa);
        param.setId(id);
        param.setCodigoGtinEq(codigoGtin);
        param.setListadoPojo(Boolean.TRUE);
        param.isValidToList();

        return findFirstPojo(param);
    }

    @Override
    public List<ProductoPojo> findPojo(ProductoParam param) {

        AbstractFind find = new AbstractFind(ProductoPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idMarca"),
                        getPath("marca").get("descripcion"),
                        getPath("root").get("idMetrica"),
                        getPath("metrica").get("codigo"),
                        getPath("metrica").get("descripcion"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigoGtin"),
                        getPath("root").get("codigoInterno"),
                        getPath("root").get("activo"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("porcentajeImpuesto"),
                        getPath("root").get("multiploUmv"),
                        getPath("root").get("umv"),
                        getPath("root").get("cuentaContable"),
                        getPath("root").get("idAuditoria"),
                        getPath("root").get("idAuditoriaDetalle"),
                        getPath("root").get("fechaVencimientoLote"),
                        getPath("root").get("nroLote"),
                        getPath("root").get("idTipoProducto"),
                        getPath("tipoProducto").get("codigo"),
                        getPath("root").get("idPadre"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<Producto> find(ProductoParam param) {

        AbstractFind find = new AbstractFind(Producto.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, ProductoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Producto> root = cq.from(Producto.class);
        Join<Producto, Empresa> empresa = root.join("empresa");
        Join<Producto, Metrica> metrica = root.join("metrica", JoinType.LEFT);
        Join<Producto, Marca> marca = root.join("marca", JoinType.LEFT);
        Join<Producto, TipoProducto> tipoProducto = root.join("tipoProducto", JoinType.LEFT);

        find.addPath("root", root);
        find.addPath("empresa", empresa);
        find.addPath("metrica", metrica);
        find.addPath("marca", marca);
        find.addPath("tipoProducto", tipoProducto);

        find.select(cq, qb);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdMarca() != null) {
            predList.add(qb.equal(root.get("idMarca"), param.getIdMarca()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getPorcentajeImpuesto() != null) {
            predList.add(qb.equal(root.get("porcentajeImpuesto"), param.getPorcentajeImpuesto()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().toLowerCase())));
        }

        if (param.getCodigoGtinEq() != null && !param.getCodigoGtinEq().trim().isEmpty()) {
            predList.add(qb.equal(root.<String>get("codigoGtin"), param.getCodigoGtinEq().trim()));
        }
        
        if (param.getCodigoInternoEq()!= null && !param.getCodigoInternoEq().trim().isEmpty()) {
            predList.add(qb.equal(root.<String>get("codigoInterno"), param.getCodigoInternoEq().trim()));
        }

        if (param.getCuentaContable() != null && !param.getCuentaContable().trim().isEmpty()) {
            predList.add(qb.equal(root.<String>get("cuentaContable"), param.getCuentaContable().trim()));
        }

        if (param.getCodigoGtin() != null && !param.getCodigoGtin().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("codigoGtin"),
                    String.format("%%%s%%", param.getCodigoGtin().trim())));
        }

        if (param.getCodigoInterno() != null && !param.getCodigoInterno().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("codigoInterno"),
                    String.format("%%%s%%", param.getCodigoInterno().trim())));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getNroLote() != null && !param.getNroLote().isEmpty()) {
             predList.add(qb.like(qb.lower(root.<String>get("nroLote")),
                    String.format("%%%s%%", param.getNroLote().trim().toLowerCase())));
             if (param.getFechaVencimientoLote() == null) {
                 predList.add(qb.isNull(root.<Date>get("fechaVencimientoLote")));
             }         
        }
        
        if (param.getFechaVencimientoLote() != null && param.getNroLote() != null && !param.getNroLote().isEmpty()) {
            predList.add(qb.equal(root.<Date>get("fechaVencimientoLote"), param.getFechaVencimientoLote()));
        } else {
            if (param.getFechaVencimientoLote() != null) {
                predList.add(qb.equal(root.<Date>get("fechaVencimientoLote"), param.getFechaVencimientoLote()));
                predList.add(qb.isNull(qb.lower(root.<String>get("nroLote"))));
            }
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        cq.orderBy(qb.desc(root.get("id")));

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ProductoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Producto> root = cq.from(Producto.class);

        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdMarca() != null) {
            predList.add(qb.equal(root.get("idMarca"), param.getIdMarca()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getPorcentajeImpuesto() != null) {
            predList.add(qb.equal(root.get("porcentajeImpuesto"), param.getPorcentajeImpuesto()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().toLowerCase())));
        }

        if (param.getCodigoGtinEq() != null && !param.getCodigoGtinEq().trim().isEmpty()) {
            predList.add(qb.equal(root.<String>get("codigoGtin"), param.getCodigoGtinEq().trim()));
        }
        
        if (param.getCodigoInternoEq()!= null && !param.getCodigoInternoEq().trim().isEmpty()) {
            predList.add(qb.equal(root.<String>get("codigoInterno"), param.getCodigoInternoEq().trim()));
        }

        if (param.getCuentaContable() != null && !param.getCuentaContable().trim().isEmpty()) {
            predList.add(qb.equal(root.<String>get("cuentaContable"), param.getCuentaContable().trim()));
        }

        if (param.getCodigoGtin() != null && !param.getCodigoGtin().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("codigoGtin"),
                    String.format("%%%s%%", param.getCodigoGtin().trim())));
        }

        if (param.getCodigoInterno() != null && !param.getCodigoInterno().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("codigoInterno"),
                    String.format("%%%s%%", param.getCodigoInterno().trim())));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getNroLote() != null && !param.getNroLote().isEmpty()) {
             predList.add(qb.like(qb.lower(root.<String>get("nroLote")),
                    String.format("%%%s%%", param.getNroLote().trim().toLowerCase())));
             if (param.getFechaVencimientoLote() == null) {
                 predList.add(qb.isNull(root.<Date>get("fechaVencimientoLote")));
             }         
        }
        
        if (param.getFechaVencimientoLote() != null && param.getNroLote() != null && !param.getNroLote().isEmpty()) {
            predList.add(qb.equal(root.<Date>get("fechaVencimientoLote"), param.getFechaVencimientoLote()));
        } else {
            if (param.getFechaVencimientoLote() != null) {
                predList.add(qb.equal(root.<Date>get("fechaVencimientoLote"), param.getFechaVencimientoLote()));
                predList.add(qb.isNull(qb.lower(root.<String>get("nroLote"))));
            }
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number) object).longValue();

        return result;
    }
}
