/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 *
 * @author Gustavo Benitez
 */
public class ConfiguracionGeneralPojo {

    /**
     * Identificador de Configuracion
     */
    private Integer idConfiguracion;
    /**
     * Clave Api Token Almacenada
     */
    private String valor;
    /**
     * Identificador del Cliente Sepsa
     */
    private Integer idClienteSepsa;
    /**
     * Codigo e Configuracion
     */
    private String codigoConfiguracion;

    public ConfiguracionGeneralPojo() {
    }

    public ConfiguracionGeneralPojo(Integer idConfiguracion, String valor, Integer idClienteSepsa, String codigoConfiguracion) {
        this.idConfiguracion = idConfiguracion;
        this.valor = valor;
        this.idClienteSepsa = idClienteSepsa;
        this.codigoConfiguracion = codigoConfiguracion;
    }

    public Integer getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(Integer idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public String getCodigoConfiguracion() {
        return codigoConfiguracion;
    }

    public void setCodigoConfiguracion(String codigoConfiguracion) {
        this.codigoConfiguracion = codigoConfiguracion;
    }
}
