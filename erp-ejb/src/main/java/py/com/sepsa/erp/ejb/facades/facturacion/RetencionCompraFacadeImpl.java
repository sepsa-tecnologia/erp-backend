/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Retencion;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionCompraParam;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "RetencionCompraFacade", mappedName = "RetencionCompraFacade")
@Local(RetencionCompraFacade.class)
public class RetencionCompraFacadeImpl extends FacadeImpl<RetencionCompra, RetencionCompraParam> implements RetencionCompraFacade {

    public RetencionCompraFacadeImpl() {
        super(RetencionCompra.class);
    }

    public Boolean validToCreate(RetencionCompraParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(isNull(persona)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "RETENCION");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        for (RetencionCompraDetalleParam detalle : param.getRetencionCompraDetalles()) {
            facades.getRetencionCompraDetalleFacade().validToCreate(detalle, false);
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToCancel(RetencionCompraParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "RETENCION_COMPRA");
        
        if(motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        }
        
        Retencion retencion = facades.getRetencionFacade().find(param.getId());

        if (retencion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la retención de compra"));
        }

        return !param.tieneErrores();
    }
    
    public Boolean validToDelete(RetencionCompraParam param) {

        if (!param.isValidToDelete()) {
            return Boolean.FALSE;
        }
        
        Long size = facades.getRetencionCompraFacade().findSize(param);

        if (size <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existen retenciones de compra asociadas"));
        }

        return !param.tieneErrores();
    }
    
    @Override
    public RetencionCompra create(RetencionCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        RetencionCompra retencion = new RetencionCompra();
        retencion.setIdEmpresa(userInfo.getIdEmpresa());
        retencion.setFechaInsercion(Calendar.getInstance().getTime());
        retencion.setFecha(param.getFecha());
        retencion.setIdPersona(param.getIdPersona());
        retencion.setMontoImponibleTotal(param.getMontoImponibleTotal());
        retencion.setMontoIvaTotal(param.getMontoIvaTotal());
        retencion.setMontoTotal(param.getMontoTotal());
        retencion.setMontoRetenidoTotal(param.getMontoRetenidoTotal());
        retencion.setNroRetencion(param.getNroRetencion());
        retencion.setPorcentajeRetencion(param.getPorcentajeRetencion());
        retencion.setIdEstado(param.getIdEstado());
        create(retencion);
        
        for (RetencionCompraDetalleParam detalle : param.getRetencionCompraDetalles()) {
            detalle.setIdRetencionCompra(retencion.getId());
            facades.getRetencionCompraDetalleFacade().create(detalle, userInfo);
        }
        
        return retencion;
    }
    
    @Override
    public RetencionCompra anular(RetencionCompraParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        RetencionCompraDetalleParam param1 = new RetencionCompraDetalleParam();
        param1.setIdEmpresa(userInfo.getIdEmpresa());
        param1.setIdRetencionCompra(param.getId());
        param1.setFirstResult(0);
        param1.setPageSize(100);

        List<RetencionCompraDetalle> list = facades.getRetencionCompraDetalleFacade().find(param1);

        for (RetencionCompraDetalle retencionDetalle : list) {

            EstadoPojo estado = facades.getEstadoFacade().find(null, "ANULADO", "RETENCION_DETALLE");
            retencionDetalle.setIdEstado(estado.getId());
            facades.getRetencionCompraDetalleFacade().edit(retencionDetalle);

            facades.getFacturaCompraFacade().actualizarSaldoFactura(
                    retencionDetalle.getIdFacturaCompra(),
                    retencionDetalle.getMontoRetenido().negate());

        }

        EstadoPojo estado = facades.getEstadoFacade().find(null, "ANULADO", "RETENCION");
        
        RetencionCompra retencion = find(param.getId());
        retencion.setIdEstado(estado.getId());
        retencion.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
        retencion.setObservacionAnulacion(param.getObservacionAnulacion());

        edit(retencion);

        return retencion;
    }
    
    @Override
    public Boolean delete(RetencionCompraParam param, UserInfoImpl userInfo) {

        if (!validToDelete(param)) {
            return Boolean.FALSE;
        }

        param.setIdEmpresa(userInfo.getIdEmpresa());
        param.setFirstResult(0);
        param.setPageSize(100);
        List<RetencionCompra> list = find(param);
        
        for (RetencionCompra item : list) {
            
            RetencionCompraDetalleParam param1 = new RetencionCompraDetalleParam();
            param1.setIdRetencionCompra(item.getId());
            param1.setFirstResult(0);
            param1.setPageSize(100);

            List<RetencionCompraDetalle> list1 = facades.getRetencionCompraDetalleFacade().find(param1);

            for (RetencionCompraDetalle item1 : list1) {

                facades.getRetencionCompraDetalleFacade().remove(item1);

                facades.getFacturaCompraFacade().actualizarSaldoFactura(
                        item1.getIdFacturaCompra(),
                        item1.getMontoRetenido().negate());

            }

            RetencionCompra retencion = find(item.getId());

            remove(retencion);
        }

        return Boolean.TRUE;
    }
    
    @Override
    public List<RetencionCompra> find(RetencionCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Retencion.class);
        Root<RetencionCompra> root = cq.from(RetencionCompra.class);
        Join<RetencionCompra, RetencionCompraDetalle> detalles = root.join("retencionCompraDetalles", JoinType.LEFT);
        
        cq.select(root).distinct(true);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(detalles.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }
        
        if (param.getNroRetencion() != null && !param.getNroRetencion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroRetencion")), String.format("%%%s%%", param.getNroRetencion().trim().toUpperCase())));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.desc(root.get("fecha")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<RetencionCompra> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(RetencionCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<RetencionCompra> root = cq.from(RetencionCompra.class);
        Join<RetencionCompra, RetencionCompraDetalle> detalles = root.join("retencionCompraDetalles", JoinType.LEFT);
        
        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(detalles.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }
        
        if (param.getNroRetencion() != null && !param.getNroRetencion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroRetencion")), String.format("%%%s%%", param.getNroRetencion().trim().toUpperCase())));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
