/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.TipoProceso;
import py.com.sepsa.erp.ejb.entities.proceso.filters.TipoProcesoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoProcesoFacade", mappedName = "TipoProcesoFacade")
@Local(TipoProcesoFacade.class)
public class TipoProcesoFacadeImpl extends FacadeImpl<TipoProceso, TipoProcesoParam> implements TipoProcesoFacade {

    public TipoProcesoFacadeImpl() {
        super(TipoProceso.class);
    }

    public Boolean validToCreate(TipoProcesoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoProcesoParam param1 = new TipoProcesoParam();
        param1.setCodigo(param.getCodigo().trim());
        
        TipoProceso item = findFirst(param1);
        
        if(!isNull(item)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(TipoProcesoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoProceso tp = find(param.getId());
        
        if(isNull(tp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo proceso"));
        } else if(!tp.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoProcesoParam param1 = new TipoProcesoParam();
            param1.setCodigo(param.getCodigo().trim());

            TipoProceso item = findFirst(param1);

            if(!isNull(item)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public TipoProceso create(TipoProcesoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoProceso item = new TipoProceso();
        item.setDescripcion(param.getDescripcion().trim());
        item.setCodigo(param.getCodigo().trim());
        
        create(item);
        
        return item;
    }

    @Override
    public TipoProceso edit(TipoProcesoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoProceso item = find(param.getId());
        item.setDescripcion(param.getDescripcion().trim());
        item.setCodigo(param.getCodigo().trim());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public TipoProceso find(Integer id, String codigo) {
        TipoProcesoParam param = new TipoProcesoParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoProceso> find(TipoProcesoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(TipoProceso.class);
        Root<TipoProceso> root = cq.from(TipoProceso.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<TipoProceso> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(TipoProcesoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<TipoProceso> root = cq.from(TipoProceso.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
