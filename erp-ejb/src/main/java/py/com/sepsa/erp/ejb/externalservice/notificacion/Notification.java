
package py.com.sepsa.erp.ejb.externalservice.notificacion;

import java.util.Date;

/**
 * POJO de la entidad notificación
 * @author Daniel F. Escauriza Arza
 */
public class Notification {
    
    /**
     * Identificador de la entidad notificación
     */
    private Integer id;
    
    /**
     * Datos del tipo de notificación
     */
    private NotificationType notificationType;
    
    /**
     * Fecha de inserción de la notificación
     */
    private Date insertDate;
    
    /**
     * Fecha de envío de la notificación
     */
    private Date sendDate;
    
    /**
     * Asunto de la notificación
     */
    private String subject;
    
    /**
     * Mensaje de la notificación
     */
    private String message;
    
    /**
     * Destino de la notificación
     */
    private String to;
    
    /**
     * Observación
     */
    private String observation;
    
    /**
     * Estado de la notificación
     */
    private String state;
    
    /**
     * Obtiene el identificador de la entidad notificación
     * @return Identificador de la entidad notificación
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador de la entidad notificación
     * @param id Identificador de la entidad notificación
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * Obtiene los datos del tipo de notificación
     * @return Datos del tipo de notificación
     */
    public NotificationType getNotificationType() {
        return notificationType;
    }

    /**
     * Setea los datos del tipo de notificación
     * @param notificationType Datos del tipo de notificación
     */
    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * Obtiene la fecha de inserción de la notificación
     * @return Fecha de inserción de la notificación
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * Setea la fecha de inserción de la notificación
     * @param insertDate Fecha de inserción de la notificación
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * Obtiene la fecha de envío de la notificación
     * @return Fecha de envío de la notificación
     */
    public Date getSendDate() {
        return sendDate;
    }

    /**
     * Setea la fecha de envío de la notificación
     * @param sendDate Fecha de envío de la notificación
     */
    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    /**
     * Obtiene el asunto de la notificación
     * @return asunto de la notificación
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Setea el asunto de la notificación
     * @param subject asunto de la notificación
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Obtiene el mensaje de la notificación
     * @return Mensaje de la notificación
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setea el mensaje de la notificación
     * @param message Mensaje de la notificación
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Obtiene el destino de la notificación
     * @return Destino de la notificación
     */
    public String getTo() {
        return to;
    }

    /**
     * Setea el destino de la notificación
     * @param to Destino de la notificación
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * Obtiene la observación
     * @return Observación
     */
    public String getObservation() {
        return observation;
    }

    /**
     * Setea la observación
     * @param observation Observación
     */
    public void setObservation(String observation) {
        this.observation = observation;
    }

    /**
     * Obtiene el estado de la entidad notificación
     * @return Estado de la entidad notificación
     */
    public String getState() {
        return state;
    }

    /**
     * Setea el estado de la entidad notificación
     * @param state Estado de la entidad notificación
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Constructor de Notification
     * @param id Identificador de la entidad notificación
     * @param notificationTypeId Identificador de la entidad tipo notificación
     * @param notificationTypeCode Código de la notificación
     * @param notificationTypeDescription Descripción de la notificación
     * @param insertDate Fecha de inserción de la notificación
     * @param sendDate Fecha de envío de la notificación
     * @param subject Asunto de la notificación
     * @param message Mensaje de la notificación
     * @param to Destino de la notificación
     * @param observation Observación
     * @param state Estado de la entidad notificación
     */
    public Notification(Integer id, Integer notificationTypeId, 
            String notificationTypeCode, String notificationTypeDescription, 
            Date insertDate, Date sendDate, String subject, String message, 
            String to, String observation, Character state) {
        this.id = id;
        this.notificationType = new NotificationType(notificationTypeId, 
                notificationTypeCode, notificationTypeDescription, null, null);
        this.insertDate = insertDate;
        this.sendDate = sendDate;
        this.subject = subject;
        this.message = message;
        this.to = to;
        this.observation = observation;
        this.state = state == null ? null : state.toString();
    }

    /**
     * Constructor de Notification
     * @param id Identificador de la entidad notificación
     * @param notificationType Datos del tipo de notificación
     * @param insertDate Fecha de inserción de la notificación
     * @param sendDate Fecha de envío de la notificación
     * @param subject Asunto de la notificación
     * @param message Mensaje de la notificación
     * @param to Destino de la notificación
     * @param observation Observación
     * @param state Estado de la entidad notificación
     */
    public Notification(Integer id, NotificationType notificationType, 
            Date insertDate, Date sendDate, String subject, String message, 
            String to, String observation, String state) {
        this.id = id;
        this.notificationType = notificationType;
        this.insertDate = insertDate;
        this.sendDate = sendDate;
        this.subject = subject;
        this.message = message;
        this.to = to;
        this.observation = observation;
        this.state = state;
    }

    /**
     * Constructor de Notification
     * @param id Identificador de la entidad notificación
     * @param message Mensaje de la notificación
     */
    public Notification(Integer id, String message) {
        this.id = id;
        this.message = message;
    }
    
    /**
     * Constructor de Notification
     */
    public Notification() {}
    
}
