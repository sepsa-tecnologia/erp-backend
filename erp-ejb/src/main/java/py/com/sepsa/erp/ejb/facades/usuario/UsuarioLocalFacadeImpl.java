/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioLocal;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioLocalParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioLocalPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "UsuarioLocalFacade", mappedName = "UsuarioLocalFacade")
@javax.ejb.Local(UsuarioLocalFacade.class)
public class UsuarioLocalFacadeImpl extends FacadeImpl<UsuarioLocal, UsuarioLocalParam> implements UsuarioLocalFacade {

    public UsuarioLocalFacadeImpl() {
        super(UsuarioLocal.class);
    }

    @Override
    public Boolean validToCreate(UsuarioLocalParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        UsuarioLocalParam param1 = new UsuarioLocalParam();
        param1.setIdUsuario(param.getIdUsuario());
        param1.setIdLocal(param.getIdLocal());
        UsuarioLocal item = findFirst(param1);
        
        if(item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe el registro"));
        }
        
        Local local = facades.getLocalFacade().find(param.getIdLocal());
        
        if(local == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el local"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(UsuarioLocalParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
   
        UsuarioLocalParam param1 = new UsuarioLocalParam();
        param1.setIdUsuario(param.getIdUsuario());
        param1.setIdLocal(param.getIdLocal());
        UsuarioLocal item = findFirst(param1);

        if(item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro"));
        } else {
            param.setId(item.getId());
        }
        
        Local local = facades.getLocalFacade().find(param.getIdLocal());
        
        if(local == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el local"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public UsuarioLocal create(UsuarioLocalParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        UsuarioLocal item = new UsuarioLocal();
        item.setIdUsuario(param.getIdUsuario());
        item.setIdLocal(param.getIdLocal());
        item.setActivo(param.getActivo());
        
        create(item);
        
        return item;
    }
    
    @Override
    public UsuarioLocal edit(UsuarioLocalParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        UsuarioLocal item = find(param.getId());
        item.setIdUsuario(param.getIdUsuario());
        item.setIdLocal(param.getIdLocal());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public void asociarMasivo(UsuarioLocalParam param, UserInfoImpl userInfo) {
        
        param.setFirstResult(0);
        param.setPageSize(500);
        List<UsuarioLocal> lista = findRelacionados(param);

        for (UsuarioLocal item : lista) {

            UsuarioLocalParam param1 = new UsuarioLocalParam();
            param1.setIdUsuario(item.getIdUsuario());
            param1.setIdLocal(item.getIdLocal());
            param1.setActivo(param.getActivo());

            editarOCrear(param1, userInfo);
        }
        
    }
    
    @Override
    public List<UsuarioLocalPojo> findPojo(UsuarioLocalParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ul.id = %d", where, param.getId());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and ul.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format("%s and ul.id_local = %d", where, param.getIdLocal());
        }
        
        if(!Assertions.isNullOrEmpty(param.getIdExternoLocal())) {
            where = String.format("%s and l.id_externo = '%s'", where, param.getIdExternoLocal());
        }
        
        if(!Assertions.isNull(param.getGlnLocal())) {
            where = String.format("%s and l.gln = '%s'", where, param.getGlnLocal());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ul.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select ul.id, ul.id_usuario, u.usuario,"
                + " ul.id_local, l.gln, l.id_externo, l.descripcion, ul.activo"
                + " from usuario.usuario_local ul"
                + " join usuario.usuario u on u.id = ul.id_usuario"
                + " join info.local l on l.id = ul.id_local"
                + " where %s order by ul.activo, ul.id_local offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();

        List<UsuarioLocalPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            UsuarioLocalPojo item = new UsuarioLocalPojo();
            item.setId((Integer)objects[i++]);
            item.setIdUsuario((Integer)objects[i++]);
            item.setUsuario((String)objects[i++]);
            item.setIdLocal((Integer)objects[i++]);
            item.setGln(objects[i++] == null ? null : new BigInteger(objects[i-1] + ""));
            item.setIdExterno((String)objects[i++]);
            item.setLocal((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public List<UsuarioLocal> find(UsuarioLocalParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ul.id = %d", where, param.getId());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and ul.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format("%s and ul.id_local = %d", where, param.getIdLocal());
        }
        
        if(!Assertions.isNullOrEmpty(param.getIdExternoLocal())) {
            where = String.format("%s and l.id_externo = '%s'", where, param.getIdExternoLocal());
        }
        
        if(!Assertions.isNull(param.getGlnLocal())) {
            where = String.format("%s and l.gln = '%s'", where, param.getGlnLocal());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ul.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select ul.*"
                + " from usuario.usuario_local ul"
                + " left join usuario.usuario u on u.id = ul.id_usuario"
                + " left join info.local l on l.id = ul.id_local"
                + " where %s order by ul.activo, ul.id_local offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, UsuarioLocal.class);
        
        List<UsuarioLocal> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(UsuarioLocalParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ul.id = %d", where, param.getId());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and ul.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format("%s and ul.id_local = %d", where, param.getIdLocal());
        }
        
        if(!Assertions.isNullOrEmpty(param.getIdExternoLocal())) {
            where = String.format("%s and l.id_externo = '%s'", where, param.getIdExternoLocal());
        }
        
        if(!Assertions.isNull(param.getGlnLocal())) {
            where = String.format("%s and l.gln = '%s'", where, param.getGlnLocal());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ul.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(ul.id)"
                + " from usuario.usuario_local ul"
                + " left join usuario.usuario u on u.id = ul.id_usuario"
                + " left join info.local l on l.id = ul.id_local"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }

    @Override
    public List<UsuarioLocal> findRelacionados(UsuarioLocalParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format(" %s and l.id = %d", where, param.getIdLocal());
        }
        
        if(param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getLocalExterno() != null) {
            where = String.format(" %s and l.externo = '%s'", where, param.getLocalExterno());
        }
        
        String sql = String.format("select coalesce(ul.id, 0) as id, coalesce(ul.id_usuario, %d) as id_usuario, l.id as id_local, coalesce(ul.activo, 'N') as activo\n"
                + "from info.local l\n"
                + "left join usuario.usuario_local ul on ul.id_local = l.id and ul.id_usuario = %d\n"
                + "left join usuario.usuario u on u.id = ul.id_usuario\n"
                + "where %s order by l.id limit %d offset %d", param.getIdUsuario(), param.getIdUsuario(), where, param.getPageSize(), param.getFirstResult());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<UsuarioLocal> result = Lists.empty();
        
        for (Object[] objects : list) {
            int i = 0;
            UsuarioLocal ul = new UsuarioLocal();
            ul.setId((Integer)objects[i++]);
            ul.setIdUsuario((Integer)objects[i++]);
            ul.setIdLocal((Integer)objects[i++]);
            ul.setActivo((Character)objects[i++]);
            ul.setLocal(facades.getLocalFacade().find(ul.getIdLocal()));
            ul.setUsuario(facades.getUsuarioFacade().find(ul.getIdUsuario()));
            result.add(ul);
        }
        
        return result;
    }

    @Override
    public Long findRelacionadosSize(UsuarioLocalParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format(" %s and l.id = %d", where, param.getIdLocal());
        }
        
        if(param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getLocalExterno() != null) {
            where = String.format(" %s and l.externo = '%s'", where, param.getLocalExterno());
        }
        
        String sql = String.format("select count(l)\n"
                + "from info.local l\n"
                + "left join usuario.usuario_local ul on ul.id_local = l.id and ul.id_usuario = %d\n"
                + "left join usuario.usuario u on u.id = ul.id_usuario\n"
                + "where %s", param.getIdUsuario(), where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
