/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.Cobro;
import py.com.sepsa.erp.ejb.entities.facturacion.CobroDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.LugarCobro;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.RedPago;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoCambio;
import py.com.sepsa.erp.ejb.entities.facturacion.TransaccionRedPago;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.CobroPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;
import py.com.sepsa.erp.ejb.entities.info.filters.EstadoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.facades.WatermarkUtils;
import py.com.sepsa.erp.ejb.utils.AwsUtils;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.sign.utils.FirmarPdfUtils;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import py.com.sepsa.utils.zip.ZipUtils;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "CobroFacade", mappedName = "CobroFacade")
@Local(CobroFacade.class)
@Log4j2
public class CobroFacadeImpl extends FacadeImpl<Cobro, CobroParam> implements CobroFacade {

    public CobroFacadeImpl() {
        super(Cobro.class);
    }

    public CobroParam obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente) {
        Date fecha = facades.getTalonarioFacade().ultimaFechaDoc(talonario, 2);
        String nroRecibo = facades.getTalonarioFacade().sigNumRecibo(talonario);

        CobroParam item = new CobroParam();

        item.setIdTalonario(talonario.getId());
        item.setNroRecibo(nroRecibo);
        item.setFecha(fecha);

        return item;
    }

    /**
     * Verifica si el objeto es válido para editar
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(CobroParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        Cobro cobro = facades.getCobroFacade().find(param.getId());

        if (cobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cobro"));
        }

        return !param.tieneErrores();
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(CobroParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "COBRO");

        if (estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado de cobro"));
        } else {
            param.setIdEstado(estado.getId());
        }

        LugarCobro lugarCobro = facades.getLugarCobroFacade()
                .find(param.getIdLugarCobro(), param.getCodigoLugarCobro());

        if (lugarCobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el lugar de cobro indicado"));
        } else {
            param.setIdLugarCobro(lugarCobro.getId());
        }

        Moneda moneda = facades.getMonedaFacade()
                .find(param.getIdMoneda(), param.getCodigoMoneda());

        if (moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda indicada"));
        } else {

            param.setIdMoneda(moneda.getId());
            param.setCodigoMoneda(moneda.getCodigo());

            if (!param.getCodigoMoneda().equals("PYG")) {

                TipoCambio tipoCambio = param.getIdTipoCambio() == null
                        ? null
                        : facades.getTipoCambioFacade().find(param.getIdTipoCambio());

                if (tipoCambio == null) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar un tipo de cambio correcto"));
                }
            }
        }

        Cliente cliente = param.getIdCliente() == null
                ? null
                : facades.getClienteFacade().find(param.getIdCliente());

        if (param.getIdCliente() != null && cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }

        TalonarioParam param2 = new TalonarioParam();
        param2.setId(param.getIdTalonario());
        TalonarioPojo talonario = facades.getTalonarioFacade().findFirstPojo(param2);

        if (talonario == null) {

            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario indicado"));

        } else if (!facades.getTalonarioFacade()
                .nroDocEnRango(talonario, param.getNroRecibo())) {

            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de recibo no se encuentra en el rango del talonario"));
        }

        TransaccionRedPago transaccionRedPago = param.getIdTransaccionRed() == null
                ? null
                : facades.getTransaccionRedPagoFacade().find(param.getIdTransaccionRed());

        if (param.getIdTransaccionRed() != null && transaccionRedPago == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la transacción de red indicada"));
        }

        if (param.getTransaccionRed() != null) {
            facades
                    .getTransaccionRedPagoFacade()
                    .validToCreate(param.getTransaccionRed());
        }

        for (CobroDetalleParam detalle : param.getCobroDetalles()) {
            facades.getCobroDetalleFacade().validToCreate(detalle, false);
        }
        
        BigDecimal totalDetalles = BigDecimal.ZERO;

        for (CobroDetalleParam detalle : param.getCobroDetalles()) {
            totalDetalles = totalDetalles.add(param.getMontoCobro());
        }

        BigDecimal result = param.getMontoCobro().subtract(totalDetalles);

        if (result.compareTo(BigDecimal.ONE) > -1) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El monto del cobro no coincide con la sumatoria de los detalles."));
        }

        CobroParam item = obtenerDatosCrear(talonario, null);

        if (!item.getNroRecibo().equals(param.getNroRecibo())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro del recibo debe ser correlativo"));
        }

        /*if (item.getFecha().after(param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El cobro no puede tener fecha inferior a una creada previamente"));
        }*/
        return !param.tieneErrores();
    }

    public Boolean validToCancel(CobroParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "COBRO");

        if (motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        }

        Cobro cobro = facades.getCobroFacade().find(param.getId());

        if (cobro == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el cobro"));
        }

        return !param.tieneErrores();
    }

    @Override
    public Cobro create(CobroParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        Integer idTransaccionRed;

        if (param.getTransaccionRed() != null) {
            TransaccionRedPago transaccionRed = facades
                    .getTransaccionRedPagoFacade()
                    .create(param.getTransaccionRed(), userInfo);

            idTransaccionRed = transaccionRed == null ? null : transaccionRed.getId();
        } else {
            idTransaccionRed = param.getIdTransaccionRed();
        }

        Cobro cobro = new Cobro();
        cobro.setIdEmpresa(userInfo.getIdEmpresa());
        cobro.setEnviado(param.getEnviado());
        cobro.setIdEstado(param.getIdEstado());
        cobro.setFecha(param.getFecha());
        cobro.setFechaEnvio(param.getFechaEnvio());
        cobro.setIdCliente(param.getIdCliente());
        cobro.setIdLugarCobro(param.getIdLugarCobro());
        cobro.setIdTalonario(param.getIdTalonario());
        cobro.setIdTransaccionRed(idTransaccionRed);
        cobro.setMontoCobro(param.getMontoCobro());
        cobro.setMontoCobroFicticio(param.getMontoCobroFicticio());
        cobro.setNroRecibo(param.getNroRecibo());
        cobro.setDigital(param.getDigital());
        cobro.setIdMoneda(param.getIdMoneda());
        cobro.setIdTipoCambio(param.getIdTipoCambio());
        create(cobro);

        param.setId(cobro.getId());
        param.setIdEmpresa(cobro.getIdEmpresa());

        for (CobroDetalleParam detalle : param.getCobroDetalles()) {
            detalle.setIdCobro(cobro.getId());
            facades.getCobroDetalleFacade().create(detalle, userInfo);
        }

        if (cobro.getDigital().equals('S')) {
            try {
                upload(param, userInfo);
            } catch (Exception e) {
                log.fatal("Error al hacer upload de recibo", e);
            }
        }

        return cobro;
    }

    /**
     * Edita una instancia de documento
     *
     * @param param parámetros
     * @return Instancia
     */
    public Cobro edit(CobroParam param) {

        if (!validToEdit(param)) {
            return null;
        }

        Cobro cobro = facades.getCobroFacade().find(param.getId());
        cobro.setFecha(param.getFecha());
        cobro.setNroRecibo(param.getNroRecibo());
        edit(cobro);

        return cobro;
    }

    public Boolean validToGetPdf(CobroParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        Cobro cobro = find(param.getId());

        if (cobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cobro"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToGetPdfMasivo(CobroParam param) {

        if (!param.isValidToGetPdfMasivo()) {
            return Boolean.FALSE;
        }

        for (Integer idCobro : param.getIdCobros()) {
            CobroParam cparam = new CobroParam();
            cparam.setId(idCobro);
            Long size = findSize(cparam);

            if (size <= 0L) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cobro"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToUpload(CobroParam param) {

        if (!param.isValidToUpload()) {
            return Boolean.FALSE;
        }

        Cobro cobro = facades.getCobroFacade().find(param.getId());

        if (cobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cobro"));
        } else {
            param.setFecha(cobro.getFecha());
            if (!cobro.getDigital().equals('S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El recibo no es digital"));
            }
        }

        return !param.tieneErrores();
    }

    @Override
    public Boolean upload(CobroParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToUpload(param)) {
            return Boolean.FALSE;
        }

        Closer closer = Closer.instance();

        try {
            byte[] signed = generateSignedReceipt(param, closer);

            if (signed != null) {
                String fileName = getPathRecibo(param);
                Repositorio repositorio = facades.getRepositorioFacade().getRepositorio(param.getIdEmpresa(), "RECIBO");
                facades.getRepositorioFacade().guardar(repositorio, fileName, signed, "application/pdf");
            } else {
                return Boolean.FALSE;
            }

        } finally {
            closer.close();
        }

        return Boolean.TRUE;
    }

    private String getPathRecibo(Cobro cobro) {
        CobroParam param = new CobroParam();
        param.setId(cobro.getId());
        param.setIdEmpresa(cobro.getIdEmpresa());
        param.setFecha(cobro.getFecha());
        return getPathRecibo(param);
    }

    private String getPathRecibo(CobroParam param) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fileName = String.format("recibo/%d/%s/%d.pdf",
                param.getIdEmpresa(), sdf.format(param.getFecha()), param.getId());
        return fileName;
    }

    /**
     * Genera el recibo
     *
     * @param param Param
     * @param closer Closer
     * @return Bytes del recibo
     * @throws java.lang.Exception
     */
    public byte[] generateSignedReceipt(CobroParam param, Closer closer) throws Exception {
        String firmaDigitalRecibo = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "FIRMA_DIGITAL_RECIBO");

        JsonObject json = null;

        if (firmaDigitalRecibo == null || firmaDigitalRecibo.trim().isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el parámetro de firma digital"));
            return null;
        }

        try {
            json = new JsonParser().parse(firmaDigitalRecibo).getAsJsonObject();
        } catch (Exception e) {
            log.fatal("Error", e);
            param.addError(MensajePojo.createInstance()
                    .descripcion("El formato de firma digital de recibo es incorrecto"));
            return null;
        }

        byte[] archivo = generateReceipt(param, closer);
        FirmarPdfUtils fpu = new FirmarPdfUtils(json);
        return fpu.signPdf(archivo);
    }

    /**
     * Genera el recibo
     *
     * @param param Param
     * @param closer Closer
     * @return Bytes del recibo
     * @throws java.lang.Exception
     */
    public byte[] generateReceipt(CobroParam param, Closer closer) throws Exception {

        byte[] result = null;

        CobroParam cparam = new CobroParam();
        cparam.setId(param.getId());
        cparam.setFirstResult(0);
        cparam.setPageSize(1000);
        CobroPojo cobro = findFirstPojo(cparam);

        InputStream is = null;
        Boolean jdbc = Boolean.FALSE;

        String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_RECIBO");

        if (templateFacturaAutoimpresor != null && !templateFacturaAutoimpresor.trim().isEmpty()) {
            is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
            jdbc = Boolean.FALSE;
        }

        if (isNull(is)) {
            String templateFacturaAutoimpresorJdbc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_RECIBO_JDBC");
            is = facades.getReportUtils().getResource(templateFacturaAutoimpresorJdbc, closer);
            jdbc = Boolean.TRUE;
        }

        if (isNull(is)) {
            param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para recibo"));
        } else {
            byte[] template = IOUtils.toByteArray(is);
            result = generarPdfCobro(closer, template, cobro, jdbc);
        }

        return result;
    }

    @Override
    public byte[] getPdfCobro(CobroParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        Cobro cobro = find(param.getId());

        byte[] result = getPdfCobroBytes(param, cobro);

        return result;
    }

    @Override
    public byte[] getPdfCobroMasivo(CobroParam param, UserInfoImpl userInfo) throws Exception {
        
        if (!validToGetPdfMasivo(param)) {
            return null;
        }

        byte[] result;
        File tempFile = File.createTempFile(UUID.randomUUID().toString(), null);
        ZipUtils zip = new ZipUtils(tempFile.getAbsolutePath());
        for (Integer idCobro : param.getIdCobros()) {
            Cobro cobro = find(idCobro);
            CobroParam cparam = new CobroParam();
            cparam.setId(idCobro);
            byte[] recibo = getPdfCobroBytes(cparam, cobro);
               
            zip.addOrReplaceEntry(String.format("%d.pdf", cobro.getId()),
                    new ByteArrayInputStream(recibo));
        }
    

        result = Files.readAllBytes(tempFile.toPath());
        tempFile.delete();

        return result;
    }

    private byte[] getPdfCobroBytes(CobroParam param, Cobro cobro) throws Exception {
        byte[] result = null;

        Closer closer = Closer.instance();

        try {
            if (cobro.getDigital().equals('N')) {

                result = generateReceipt(param, closer);

            } else {

                try {
                    String fileName = getPathRecibo(cobro);
                    Repositorio repositorio = facades.getRepositorioFacade().getRepositorio(param.getIdEmpresa(), "RECIBO");
                    result = AwsUtils.getFile(repositorio, fileName);
                } catch (Exception e) {
                    log.fatal("", e);
                }

                if (result == null) {
                    result = generateSignedReceipt(param, closer);
                }
            }
        } finally {
            closer.close();
        }

        //agrega una marca de agua si el cobro esta anulado
        if( (cobro.getEstado() != null) && cobro.getEstado().getCodigo().equals("ANULADO")){
            try (ByteArrayOutputStream baos = WatermarkUtils.addWatermarkToExistingPdf(result ,"ANULADO", null)) {
                result = baos.toByteArray();
            }
        }
        
        return result;
    }

    private byte[] generarPdfCobro(Closer closer, byte[] template, CobroPojo cobro, Boolean jdbc) throws Exception {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
            py.com.sepsa.erp.reporte.pojos.cobro.CobroParam params = facades.getReportUtils().getCobroParam(closer, cobro);
            if (jdbc) {
                //Obtenemos una conexion del DataSource
                Context ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                try (Connection connection = ds.getConnection()) {
                    return JasperReporteGenerator.exportReportToPdfBytes(params, connection, bais);
                }
            } else {
                return JasperReporteGenerator.exportReportToPdfBytes(params, bais);
            }
        }
    }

    @Override
    public Cobro anular(CobroParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        CobroDetalleParam param1 = new CobroDetalleParam();
        param1.setIdEmpresa(userInfo.getIdEmpresa());
        param1.setIdCobro(param.getId());
        param1.setFirstResult(0);
        param1.setPageSize(100);
        List<CobroDetalle> list = facades.getCobroDetalleFacade().find(param1);

        Integer idCliente = null;

        for (CobroDetalle cobroDetalle : list) {
            if(cobroDetalle.getIdFactura()!=null){
                facades.getFacturaFacade().actualizarSaldoFactura(
                        cobroDetalle.getIdFactura(),
                        cobroDetalle.getMontoCobro().negate());

                Factura factura = facades
                        .getFacturaFacade()
                        .find(cobroDetalle.getIdFactura());
                factura.setCobrado('N');
                facades.getFacturaFacade().edit(factura);

                idCliente = factura.getIdCliente() == null
                        ? idCliente
                        : factura.getIdCliente();
            }

            EstadoPojo estado = facades.getEstadoFacade().find(null, "ANULADO", "COBRO_DETALLE");
            cobroDetalle.setIdEstado(estado.getId());
            facades.getCobroDetalleFacade().edit(cobroDetalle);
        }

        EstadoPojo estado = facades.getEstadoFacade().find(null, "ANULADO", "COBRO");

        Cobro cobro = find(param.getId());
        cobro.setIdEstado(estado.getId());
        cobro.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
        cobro.setObservacionAnulacion(param.getObservacionAnulacion());

        edit(cobro);

        return cobro;
    }
    
    
    public Boolean validToGetEmitidas(CobroParam param) {
       
        EstadoParam filtro = new EstadoParam();
        filtro.setCodigoTipoEstado("COBRO");
        if (param.getIdEstado() == null) {
            if (param.getCodigoEstado() == null) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código o identificador de Estado"));
            } else {
                filtro.setCodigoEstado(param.getCodigoEstado());
            }
        } else {
            filtro.setIdEstado(param.getIdEstado());
        }
        
        Estado estado = facades.getEstadoFacade().findFirst(filtro);
        
        if (estado == null){
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
       
        return !param.tieneErrores();
    }
    
    
    /**
     * Obtiene la cantidad de facturas emitidas.
     *
     * @param param Parámetros
     * @return result
     */
    public Long cantidadCobrosEmitidas(CobroParam param) {

        if (!validToGetEmitidas(param)) {
            return null;
        }

        String sql = String.format("select count(*) \n"
                + "from facturacion.cobro c\n"
                + "where c.id_empresa = %d \n"
                + "	and id_estado = %d \n"
                + "	and fecha_insercion >= NOW() - INTERVAL '30 days';", param.getIdEmpresa(), param.getIdEstado());

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;

    }

    @Override
    public List<CobroPojo> findPojo(CobroParam param) {

        AbstractFind find = new AbstractFind(CobroPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idCliente"),
                        getPath("cliente").get("razonSocial"),
                        getPath("cliente").get("nroDocumento"),
                        getPath("root").get("idLugarCobro"),
                        getPath("lugarCobro").get("descripcion"),
                        getPath("root").get("fecha"),
                        getPath("root").get("montoCobro"),
                        getPath("root").get("nroRecibo"),
                        getPath("root").get("idTalonario"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("idTransaccionRed"),
                        getPath("transaccionRed").get("idTransaccion"),
                        getPath("redPago").get("nombre"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("descripcion"),
                        getPath("moneda").get("codigo"),
                        getPath("root").get("idMotivoAnulacion"),
                        getPath("motivoAnulacion").get("descripcion"),
                        getPath("root").get("observacionAnulacion"),
                        getPath("root").get("fechaEnvio"),
                        getPath("root").get("enviado"),
                        getPath("root").get("digital")
                ).distinct(true);
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<Cobro> find(CobroParam param) {

        AbstractFind find = new AbstractFind(Cobro.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, CobroParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Cobro> root = cq.from(Cobro.class);
        Join<Cobro, Empresa> empresa = root.join("empresa");
        Join<Cobro, Cliente> cliente = root.join("cliente");
        Join<Cobro, LugarCobro> lugarCobro = root.join("lugarCobro");
        Join<Cobro, Estado> estado = root.join("estado");
        Join<Cobro, Moneda> moneda = root.join("moneda");
        Join<Cobro, MotivoAnulacion> motivoAnulacion = root.join("motivoAnulacion", JoinType.LEFT);
        Join<Cobro, TransaccionRedPago> transaccionRed = root.join("transaccionRed", JoinType.LEFT);
        Join<TransaccionRedPago, RedPago> red = transaccionRed.join("red", JoinType.LEFT);
        Join<Cobro, CobroDetalle> cobroDetalles = root.join("cobroDetalles", JoinType.LEFT);
        Join<CobroDetalle, Factura> factura = cobroDetalles.join("factura", JoinType.LEFT);

        find.addPath("root", root);
        find.addPath("empresa", empresa);
        find.addPath("cliente", cliente);
        find.addPath("lugarCobro", lugarCobro);
        find.addPath("estado", estado);
        find.addPath("moneda", moneda);
        find.addPath("motivoAnulacion", motivoAnulacion);
        find.addPath("transaccionRed", transaccionRed);
        find.addPath("redPago", red);
        find.addPath("factura", factura);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(factura.get("id"), param.getIdFactura()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }

        if (param.getIdLugarCobro() != null) {
            predList.add(qb.equal(root.get("idLugarCobro"), param.getIdLugarCobro()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getNroRecibo() != null && !param.getNroRecibo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroRecibo"), param.getNroRecibo().trim()));
        }

        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getIdTransaccionRed() != null) {
            predList.add(qb.equal(root.get("idTransaccionRed"), param.getIdTransaccionRed()));
        }

        if (param.getEnviado() != null) {
            predList.add(qb.equal(root.get("enviado"), param.getEnviado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(root.get("fecha")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }
    
    /**
     * Obtiene el monto total de ventas.
     *
     * @param param Parámetros
     * @return result
     */
    public Long montoTotalVentas(CobroParam param) {

        if (param.getIdEmpresa() == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Empresa"));
            return null;
        }

        String codigoEstado = "APROBADO";
        String codigoTipoEstado = "COBRO";
        EstadoParam estadoFilter = new EstadoParam();
        estadoFilter.setCodigoEstado(codigoEstado);
        estadoFilter.setCodigoTipoEstado(codigoTipoEstado);

        Estado estado = facades.getEstadoFacade().findFirst(estadoFilter);

        String sql = String.format("select \n" +
                                    "sum (case \n" +
                                    "       when c.id_tipo_cambio is null then cd.monto_cobro\n" +
                                    " 	    else cd.monto_cobro_guaranies\n" +
                                    "	 end) as monto_total	\n" +
                                    "from facturacion.cobro c \n" +
                                    "join facturacion.cobro_detalle cd  on c.id = cd.id_cobro \n" +
                                    "where c.id_empresa = %d\n" +
                                    "	and c.id_estado = %d\n" +
                                    "	AND c.fecha_insercion >= NOW() - INTERVAL '30 days'", param.getIdEmpresa(), estado.getId());

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    
    }

    @Override
    public Long findSize(CobroParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Cobro> root = cq.from(Cobro.class);
        Join<Cobro, Empresa> empresa = root.join("empresa");
        Join<Cobro, CobroDetalle> cobroDetalles = root.join("cobroDetalles", JoinType.LEFT);
        Join<CobroDetalle, Factura> factura = cobroDetalles.join("factura", JoinType.LEFT);

        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(factura.get("id"), param.getIdFactura()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }

        if (param.getIdLugarCobro() != null) {
            predList.add(qb.equal(root.get("idLugarCobro"), param.getIdLugarCobro()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getNroRecibo() != null && !param.getNroRecibo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroRecibo"), param.getNroRecibo().trim()));
        }

        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getIdTransaccionRed() != null) {
            predList.add(qb.equal(root.get("idTransaccionRed"), param.getIdTransaccionRed()));
        }

        if (param.getEnviado() != null) {
            predList.add(qb.equal(root.get("enviado"), param.getEnviado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
