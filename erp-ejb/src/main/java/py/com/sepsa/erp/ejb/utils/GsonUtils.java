/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Modifier;
import py.com.sepsa.utils.gson.SepsaExclusionStrategy;

/**
 * Clase para el manejo de utilizades de Gson
 * @author Jonathan D. Bernal Fernández
 */
public class GsonUtils {
    
    /**
     * Genera un Gson por defecto
     * @return Gson
     */
    public static Gson generateDetault() {
        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL)
                .setExclusionStrategies(new SepsaExclusionStrategy())
                .setDateFormat("yyyy-MM-dd HH:mm:ssZ")
                .create();
    }
}
