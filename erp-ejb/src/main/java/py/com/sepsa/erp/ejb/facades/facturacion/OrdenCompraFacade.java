/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface OrdenCompraFacade extends Facade<OrdenCompra, OrdenCompraParam, UserInfoImpl> {

    public Boolean validToCreate(UserInfoImpl userInfo, OrdenCompraParam param);

    public OrdenCompra updateFileProcess(OrdenCompraParam param, UserInfoImpl userInfo);

    public Long maxId();

    public OrdenCompra changeState(OrdenCompraParam param, UserInfoImpl userInfo);

    public OrdenCompra cancel(OrdenCompraParam param, UserInfoImpl userInfo);

    public OrdenCompra enable(OrdenCompraParam param, UserInfoImpl userInfo);

    public byte[] getPdfOrdenCompra(OrdenCompraParam param, UserInfoImpl userInfo) throws Exception;
    
}