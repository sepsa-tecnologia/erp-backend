/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contrato
 * @author Jonathan D. Bernal Fernández
 */
public class ContratoParam extends CommonParam {
    
    public ContratoParam() {
    }

    public ContratoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de comercial
     */
    @QueryParam("idComercial")
    private Integer idComercial;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * Nro de contrato
     */
    @QueryParam("nroContrato")
    private String nroContrato;
    
    /**
     * Fecha de inicio
     */
    @QueryParam("fechaInicio")
    private Date fechaInicio;
    
    /**
     * Fecha de inicio desde
     */
    @QueryParam("fechaInicioDesde")
    private Date fechaInicioDesde;
    
    /**
     * Fecha de inicio hasta
     */
    @QueryParam("fechaInicioHasta")
    private Date fechaInicioHasta;
    
    /**
     * Fecha de fin
     */
    @QueryParam("fechaFin")
    private Date fechaFin;
    
    /**
     * Fecha de fin desde
     */
    @QueryParam("fechaFinDesde")
    private Date fechaFinDesde;
    
    /**
     * Fecha de fin hasta
     */
    @QueryParam("fechaFinHasta")
    private Date fechaFinHasta;
    
    /**
     * Firmado
     */
    @QueryParam("firmado")
    private Character firmado;
    
    /**
     * Documento avalatorio
     */
    @QueryParam("documentoAval")
    private Character documentoAval;
    
    /**
     * Estado cliente
     */
    @QueryParam("estadoCliente")
    private Character estadoCliente;
    
    /**
     * Renovación automatica
     */
    @QueryParam("renovacionAutomatica")
    private Character renovacionAutomatica;
    
    /**
     * Débito automatico
     */
    @QueryParam("debitoAutomatico")
    private Character debitoAutomatico;
    
    /**
     * Plazo rescision
     */
    @QueryParam("plazoRescision")
    private Integer plazoRescision;
    
    /**
     * Observacion
     */
    @QueryParam("observacion")
    private String observacion;
    
    /**
     * Cobro adelantado
     */
    @QueryParam("cobroAdelantado")
    private Character cobroAdelantado;
    
    /**
     * Identificador de servicios
     */
    private List<ContratoServicioParam> servicios;
    
    public boolean isValidToInactivate() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de contrato"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToRenew() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de contrato"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de moneda"));
        }
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente"));
        }
        
        if(isNullOrEmpty(nroContrato)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de contrato"));
        }
        
        if(isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de inicio"));
        }
        
        if(isNull(fechaFin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha fin"));
        }
        
        if(isNull(firmado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el contrato esta firmado"));
        }
        
        if(isNull(documentoAval)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el contrato tiene documento avalatorio"));
        }
        
        if(isNull(renovacionAutomatica)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el contrato tiene renovación automatica"));
        }
        
        if(isNull(debitoAutomatico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el contrato tiene debito automatico"));
        }
        
        if(isNull(idEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado del contrato"));
        }
        
        if(isNull(cobroAdelantado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el contrato tiene cobro adelantado"));
        }
        
        if(isNullOrEmpty(servicios)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los servicios asociados al contrato"));
        } else {
            for (ContratoServicioParam servicio : servicios) {
                servicio.setIdContrato(0);
                servicio.setIdProducto(idProducto);
                servicio.setIdEmpresa(idEmpresa);
                servicio.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = new ArrayList<>();
        
        if(servicios != null) {
            for (ContratoServicioParam servicio : servicios) {
                result.addAll(servicio.getErrores());
            }
        }
        
        return result;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNroContrato() {
        return nroContrato;
    }

    public void setNroContrato(String nroContrato) {
        this.nroContrato = nroContrato;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Character getFirmado() {
        return firmado;
    }

    public void setFirmado(Character firmado) {
        this.firmado = firmado;
    }

    public Character getDocumentoAval() {
        return documentoAval;
    }

    public void setDocumentoAval(Character documentoAval) {
        this.documentoAval = documentoAval;
    }

    public Character getRenovacionAutomatica() {
        return renovacionAutomatica;
    }

    public void setRenovacionAutomatica(Character renovacionAutomatica) {
        this.renovacionAutomatica = renovacionAutomatica;
    }

    public Character getDebitoAutomatico() {
        return debitoAutomatico;
    }

    public void setDebitoAutomatico(Character debitoAutomatico) {
        this.debitoAutomatico = debitoAutomatico;
    }

    public Integer getPlazoRescision() {
        return plazoRescision;
    }

    public void setPlazoRescision(Integer plazoRescision) {
        this.plazoRescision = plazoRescision;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public void setServicios(List<ContratoServicioParam> servicios) {
        this.servicios = servicios;
    }

    public List<ContratoServicioParam> getServicios() {
        return servicios;
    }

    public Date getFechaInicioDesde() {
        return fechaInicioDesde;
    }

    public void setFechaInicioDesde(Date fechaInicioDesde) {
        this.fechaInicioDesde = fechaInicioDesde;
    }

    public Date getFechaInicioHasta() {
        return fechaInicioHasta;
    }

    public void setFechaInicioHasta(Date fechaInicioHasta) {
        this.fechaInicioHasta = fechaInicioHasta;
    }

    public Date getFechaFinDesde() {
        return fechaFinDesde;
    }

    public void setFechaFinDesde(Date fechaFinDesde) {
        this.fechaFinDesde = fechaFinDesde;
    }

    public Date getFechaFinHasta() {
        return fechaFinHasta;
    }

    public void setFechaFinHasta(Date fechaFinHasta) {
        this.fechaFinHasta = fechaFinHasta;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public void setEstadoCliente(Character estadoCliente) {
        this.estadoCliente = estadoCliente;
    }

    public Character getEstadoCliente() {
        return estadoCliente;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
