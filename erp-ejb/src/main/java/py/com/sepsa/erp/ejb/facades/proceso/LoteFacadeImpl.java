/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.TipoLote;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.entities.proceso.pojos.LotePojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LoteFacade", mappedName = "LoteFacade")
@Local(LoteFacade.class)
public class LoteFacadeImpl extends FacadeImpl<Lote, LoteParam> implements LoteFacade {

    public LoteFacadeImpl() {
        super(Lote.class);
    }

    public Boolean validToCreate(LoteParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        LoteParam param1 = new LoteParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setIdUsuario(param.getIdUsuario());
        param1.setIdTipoLote(param.getIdTipoLote());
        param1.setCodigoTipoLote(param.getCodigoTipoLote());
        param1.setNombreArchivo(param.getNombreArchivo().trim());
        param1.setCodigoEstado("PENDIENTE");
        Lote item = findFirst(param1);
        if (isNull(item)) {
            param1.setCodigoEstado("EN_CURSO");
            item = findFirst(param1);
        }

        if (!isNull(item)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro en curso con el mismo nombre de archivo"));
        }

        EstadoPojo estado = facades.getEstadoFacade()
                .find(param.getIdEstado(), param.getCodigoEstado(), "LOTE");

        if (isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }

        TipoLote tipoLote = facades.getTipoLoteFacade()
                .find(param.getIdTipoLote(), param.getCodigoTipoLote());

        if (isNull(tipoLote)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de lote"));
        } else {
            param.setIdTipoLote(tipoLote.getId());
            param.setCodigoTipoLote(tipoLote.getCodigo());
        }

        for (LoteArchivoParam archivo : param.getArchivos()) {
            facades.getLoteArchivoFacade().validToCreate(archivo, Boolean.FALSE);
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(LoteParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        LoteParam param1 = new LoteParam();
        param1.setId(param.getId());
        param1.setIdEmpresa(param.getIdEmpresa());
        Long size = findSize(param1);

        if (size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de lote"));
        }

        EstadoPojo estado = facades.getEstadoFacade()
                .find(param.getIdEstado(), param.getCodigoEstado(), "LOTE");

        if (isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }

        return !param.tieneErrores();
    }

    @Override
    public Lote create(LoteParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        Lote item = new Lote();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setIdTipoLote(param.getIdTipoLote());
        item.setIdEstado(param.getIdEstado());
        item.setIdUsuario(param.getIdUsuario());
        item.setEmail(param.getEmail().trim());
        item.setNotificado('N');
        item.setNombreArchivo(param.getNombreArchivo().trim());
        item.setObservacion(param.getObservacion());
        item.setFechaInsercion(Calendar.getInstance().getTime());

        create(item);

        for (LoteArchivoParam archivo : param.getArchivos()) {
            archivo.setIdLote(item.getId());
            facades.getLoteArchivoFacade().create(archivo, userInfo);
        }

        return item;
    }

    @Override
    public Lote edit(LoteParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Lote item = find(param.getId());
        item.setNotificado(param.getNotificado());
        item.setIdEstado(param.getIdEstado());
        if (param.getFechaProcesamiento() != null) {
            item.setFechaProcesamiento(param.getFechaProcesamiento());
        }
        if (param.getResultado() != null) {
            item.setResultado(param.getResultado());
        }
        if (param.getFechaProcesamiento() != null) {
            item.setFechaProcesamiento(param.getFechaProcesamiento());
        }

        edit(item);

        return item;
    }

    @Override
    public List<LotePojo> findPojo(LoteParam param) {

        AbstractFind find = new AbstractFind(LotePojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("codigo"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idTipoLote"),
                        getPath("tipoLote").get("codigo"),
                        getPath("tipoLote").get("descripcion"),
                        getPath("root").get("idUsuario"),
                        getPath("usuario").get("usuario"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("codigo"),
                        getPath("estado").get("descripcion"),
                        getPath("root").get("nombreArchivo"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("fechaProcesamiento"),
                        getPath("root").get("notificado"),
                        getPath("root").get("email"),
                        getPath("root").get("observacion"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<Lote> find(LoteParam param) {

        AbstractFind find = new AbstractFind(Lote.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, LoteParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Lote> root = cq.from(Lote.class);
        Join<Lote, TipoLote> tipoLote = root.join("tipoLote");
        Join<Lote, Estado> estado = root.join("estado");
        Join<Lote, Estado> empresa = root.join("empresa");
        Join<Lote, Estado> usuario = root.join("usuario");

        find.addPath("root", root);
        find.addPath("tipoLote", tipoLote);
        find.addPath("estado", estado);
        find.addPath("empresa", empresa);
        find.addPath("usuario", usuario);

        find.select(cq, qb);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(root.get("idUsuario"), param.getIdUsuario()));
        }

        if (param.getIdTipoLote() != null) {
            predList.add(qb.equal(root.get("idTipoLote"), param.getIdTipoLote()));
        }

        if (param.getNombreArchivo() != null) {
            predList.add(qb.equal(root.get("nombreArchivo"), param.getNombreArchivo()));
        }

        if (param.getCodigoTipoLote() != null && !param.getCodigoTipoLote().trim().isEmpty()) {
            predList.add(qb.equal(tipoLote.get("codigo"), param.getCodigoTipoLote().trim()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getNotificado() != null) {
            predList.add(qb.equal(root.get("notificado"), param.getNotificado()));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(LoteParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Lote> root = cq.from(Lote.class);
        Join<Lote, TipoLote> tipoLote = root.join("tipoLote");
        Join<Lote, Estado> estado = root.join("estado");

        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(root.get("idUsuario"), param.getIdUsuario()));
        }

        if (param.getNombreArchivo() != null) {
            predList.add(qb.equal(root.get("nombreArchivo"), param.getNombreArchivo()));
        }

        if (param.getIdTipoLote() != null) {
            predList.add(qb.equal(root.get("idTipoLote"), param.getIdTipoLote()));
        }

        if (param.getCodigoTipoLote() != null && !param.getCodigoTipoLote().trim().isEmpty()) {
            predList.add(qb.equal(tipoLote.get("codigo"), param.getCodigoTipoLote().trim()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getNotificado() != null) {
            predList.add(qb.equal(root.get("notificado"), param.getNotificado()));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number) object).longValue();

        return result;
    }

    @Override
    public List<LoteArchivo> obtenerLoteArchivo(LoteParam param) {
        LoteArchivoParam lap = new LoteArchivoParam();
        lap.setIdLote(param.getId());
        lap.setIdEmpresa(param.getIdEmpresa());
        lap.setFirstResult(0);
        lap.setPageSize(100);
        List<LoteArchivo> result = facades.getLoteArchivoFacade().find(lap);
        if(result == null || result.isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentran archivos asociados al lote"));
        }
        return result;
    }

    /**
     * Obtiene el resultado de procesamiento de un lote
     *
     * @param param Parámetros
     * @return Resultado
     */
    @Override
    public LotePojo obtenerResultado(LoteParam param) {

        if (!param.isValidToGetResult()) {
            return null;
        }

        String where = "true";

        if (param.getId() != null) {
            where = String.format(" %s and l.id = %d", where, param.getId());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }

        String sql = String.format("select id, nombre_archivo, resultado"
                + " from proceso.lote l"
                + " where %s order by l.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        LotePojo result = null;

        for (Object[] object : list) {
            result = new LotePojo();
            result.setId((Integer) object[0]);
            result.setNombreArchivo((String) object[1]);
            result.setResultado((String) object[2]);
        }

        if (result == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra ningún registro"));
        }

        return result;
    }
}
