
package py.com.sepsa.erp.ejb.externalservice.notificacion;

/**
 * POJO de la entidad evento
 * @author Daniel F. Escauriza Arza
 */
public class Event {
    
    /**
     * Identificador de la entidad evento
     */
    private Integer id;
    
    /**
     * Código del evento
     */
    private String code;
    
    /**
     * Descripción del evento
     */
    private String description;
    
    /**
     * Bandera que indica si los mensajes del evento se agrupan
     */
    private String group;

    /**
     * Obtiene el identificador de la entidad evento
     * @return Identificador de la entidad evento
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador de la entidad evento
     * @param id Identificador de la entidad evento
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene el código del evento
     * @return Código del evento
     */
    public String getCode() {
        return code;
    }

    /**
     * Setea el código del evento
     * @param code Código del evento
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtiene la descripción del evento
     * @return Descripción del evento
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción del evento
     * @param description Descripción del evento
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Obtiene la bandera que indica si los mensajes del evento se agrupan
     * @return Bandera que indica si los mensajes del evento se agrupan
     */
    public String getGroup() {
        return group;
    }

    /**
     * Setea la bandera que indica si los mensajes del evento se agrupan
     * @param group Bandera que indica si los mensajes del evento se agrupan
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * Constructor de Event
     * @param id Identificador de la entidad evento
     * @param code Código del evento
     * @param description Descripción del evento
     * @param group Bandera que indica si los mensajes del evento se agrupan
     */
    public Event(Integer id, String code, String description, Character group) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.group = group == null ? "N" : group.toString();
    }

    /**
     * Constructor de Event
     * @param id Identificador de la entidad evento
     */
    public Event(Integer id) {
        this.id = id;
    }
    
    /**
     * Constructor de Event
     */
    public Event() {}
    
}
