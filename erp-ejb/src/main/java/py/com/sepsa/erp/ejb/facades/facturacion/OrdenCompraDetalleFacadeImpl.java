/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraDetallePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraPojo;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "OrdenCompraDetalleFacade", mappedName = "OrdenCompraDetalleFacade")
@Local(OrdenCompraDetalleFacade.class)
public class OrdenCompraDetalleFacadeImpl extends FacadeImpl<OrdenCompraDetalle, OrdenCompraDetalleParam> implements OrdenCompraDetalleFacade {

    public OrdenCompraDetalleFacadeImpl() {
        super(OrdenCompraDetalle.class);
    }

    @Override
    public Boolean validToCreate(UserInfoImpl userInfo, OrdenCompraDetalleParam param, boolean validarIdOrdenCompra) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getIdOrdenCompra());
        Long ordenCompraSize = facades.getOrdenCompraFacade().findSize(ocparam);
        
        if(validarIdOrdenCompra && ordenCompraSize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la orden de compra"));
        }
            
        if (!isNull(param.getIdProducto()) && !isNullOrEmpty(param.getCodigoGtin())){
            
            ProductoPojo producto = facades.getProductoFacade().findPojo(param.getIdEmpresa(),
                    param.getIdProducto(), param.getCodigoGtin());

            if(producto == null) {
                String msg = "";

                if(param.getIdProducto() != null) {
                    msg = String.format("%s IdProducto:%d", msg, param.getIdProducto());
                }

                if(param.getCodigoGtin() != null && !param.getCodigoGtin().trim().isEmpty()) {
                    msg = String.format("%s CódigoGtin:%s", msg, param.getCodigoGtin().trim());
                }

                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el producto. " + msg));
            } else {
                param.setIdProducto(producto.getId());
            }
        }
        if(param.getIdDepositoLogistico() != null) {
            
            DepositoLogisticoParam pparam = new DepositoLogisticoParam();
            pparam.setId(param.getIdDepositoLogistico());
            Long dlsize = facades.getDepositoLogisticoFacade().findSize(pparam);
            
            if(dlsize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el depósito logistico"));
            }
        }
        
        if(param.getIdEstadoInventario() != null || param.getCodigoEstadoInventario() != null) {
            
            EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstadoInventario(), param.getCodigoEstadoInventario(), "INVENTARIO");

            if(estado == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado de inventario"));
            } else {
                param.setIdEstadoInventario(estado.getId());
                param.setCodigoEstadoInventario(estado.getCodigo());
            }
        }
        
        if(!isNull(userInfo) && userInfo.getTieneModuloInventario()
                && param.getRecibido().equals('S')
                && param.getCodigoEstadoOrdenCompra() != null) {

            switch(param.getCodigoEstadoOrdenCompra()) {
                case "PENDIENTE_PREPARACION":
                case "PENDIENTE_FACTURACION":
                    
                    List<OrdenCompraDetallePojo> list = getDisponible(
                            param.getIdLocalDestino(), param.getIdProducto(),
                            param.getIdEmpresa());

                    //Validamos cantidad disponible vs confirmada
                    if(list == null || list.isEmpty()) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("No se puede confirmar una cantidad superior a la disponible"));
                    } else {
                        for (OrdenCompraDetallePojo item : list) {
                            if(item.getCantidadDisponible().intValue() < param.getCantidadConfirmada()) {
                                param.addError(MensajePojo.createInstance()
                                        .descripcion("No se puede confirmar una cantidad superior a la disponible"));
                                break;
                            }
                        }
                    }
                    break;
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Boolean validToEdit(UserInfoImpl userInfo, OrdenCompraDetalleParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
        ocdparam.setId(param.getId());
        OrdenCompraDetallePojo ocd = findFirstPojo(ocdparam);
        
        if(ocd == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la orden de compra detalle"));
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getIdOrdenCompra());
        OrdenCompraPojo ordenCompra = facades.getOrdenCompraFacade().findFirstPojo(ocparam);
        if(ordenCompra == null) {
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
        }
        
        ProductoPojo producto = facades.getProductoFacade().findPojo(param.getIdEmpresa(),
                param.getIdProducto(), param.getCodigoGtin());

        if(producto == null) {
            String msg = "";
            
            if(param.getIdProducto() != null) {
                msg = String.format("%s IdProducto:%d", msg, param.getIdProducto());
            }
            
            if(param.getCodigoGtin() != null && !param.getCodigoGtin().trim().isEmpty()) {
                msg = String.format("%s CódigoGtin:%s", msg, param.getCodigoGtin().trim());
            }
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto. " + msg));
        } else {
            param.setIdProducto(producto.getId());
        }
        
        if(param.getIdDepositoLogistico() != null) {
            
            DepositoLogisticoParam pparam = new DepositoLogisticoParam();
            pparam.setId(param.getIdDepositoLogistico());
            Long dlsize = facades.getDepositoLogisticoFacade().findSize(pparam);
            
            if(dlsize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el depósito logistico"));
            }
        }
        
        if(param.getIdEstadoInventario() != null || param.getCodigoEstadoInventario() != null) {
            
            EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstadoInventario(), param.getCodigoEstadoInventario(), "INVENTARIO");

            if(estado == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado de inventario"));
            } else {
                param.setIdEstadoInventario(estado.getId());
                param.setCodigoEstadoInventario(estado.getCodigo());
            }
        }
        
        if(ocd != null && userInfo.getTieneModuloInventario()
                && param.getRecibido().equals('S')
                && param.getCodigoEstadoOrdenCompra() != null) {

            switch(param.getCodigoEstadoOrdenCompra()) {
                case "PENDIENTE_PREPARACION":
                case "PENDIENTE_FACTURACION":
                
                    ocdparam = new OrdenCompraDetalleParam();
                    ocdparam.setId(param.getId());
                    ocdparam.setIdOrdenCompra(param.getIdOrdenCompra());
                    ocdparam.setIdEmpresa(param.getIdEmpresa());
                    List<OrdenCompraDetallePojo> list = getByOrdenCompra(ocdparam);
                    
                    Integer cantidadAsignada = getCantidadOrdenCompraDetalle(param.getId());
                    
                    for (OrdenCompraDetallePojo item : list) {
                        if(item.getId().equals(param.getId())) {
                            
                            //Validamos cantidad disponible vs confirmada
                            if(param.getCodigoEstadoOrdenCompra().equals("PENDIENTE_PREPARACION")
                                    && (item.getCantidadDisponible().intValue() + cantidadAsignada) < param.getCantidadConfirmada()) {
                                param.addError(MensajePojo.createInstance()
                                        .descripcion("No se puede confirmar una cantidad superior a la disponible"));
                                break;
                            }
                            
                            //Validamos cantidad confirmada vs preparada
                            if(param.getCodigoEstadoOrdenCompra().equals("PENDIENTE_FACTURACION")
                                    && ocd.getCantidadConfirmada() < param.getCantidadConfirmada()) {
                                param.addError(MensajePojo.createInstance()
                                        .descripcion("No se puede preparar una cantidad mayor a la asignada"));
                                break;
                            }
                        }
                    }
                break;
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public OrdenCompraDetalle create(OrdenCompraDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(userInfo, param, true)) {
            return null;
        }
        
        OrdenCompraDetalle item = new OrdenCompraDetalle();
        item.setIdOrdenCompra(param.getIdOrdenCompra());
        item.setIdProducto(param.getIdProducto());
        item.setIdDepositoLogistico(param.getIdDepositoLogistico());
        item.setIdEstadoInventario(param.getIdEstadoInventario());
        item.setCantidadSolicitada(param.getCantidadSolicitada());
        item.setCantidadConfirmada(param.getCantidadConfirmada());
        item.setPorcentajeIva(param.getPorcentajeIva());
        item.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva());
        item.setPrecioUnitarioSinIva(param.getPrecioUnitarioConIva());
        item.setMontoIva(param.getMontoIva());
        item.setMontoImponible(param.getMontoImponible());
        item.setMontoTotal(param.getMontoTotal());
        item.setObservacion(param.getObservacion());
        item.setFechaVencimiento(param.getFechaVencimiento());
        
        create(item);
        
        return item;
    }

    @Override
    public OrdenCompraDetalle edit(OrdenCompraDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(userInfo, param)) {
            return null;
        }
        
        OrdenCompraDetalle item = find(param.getId());
        item.setIdOrdenCompra(param.getIdOrdenCompra());
        item.setIdProducto(param.getIdProducto());
        item.setIdDepositoLogistico(param.getIdDepositoLogistico());
        item.setIdEstadoInventario(param.getIdEstadoInventario());
        item.setCantidadSolicitada(param.getCantidadSolicitada());
        item.setCantidadConfirmada(param.getCantidadConfirmada());
        item.setPorcentajeIva(param.getPorcentajeIva());
        item.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva());
        item.setPrecioUnitarioSinIva(param.getPrecioUnitarioConIva());
        item.setMontoIva(param.getMontoIva());
        item.setMontoImponible(param.getMontoImponible());
        item.setMontoTotal(param.getMontoTotal());
        item.setObservacion(param.getObservacion());
        item.setFechaVencimiento(param.getFechaVencimiento());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<OrdenCompraDetallePojo> findPojo(OrdenCompraDetalleParam param) {

        AbstractFind find = new AbstractFind(OrdenCompraDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idOrdenCompra"),
                        getPath("root").get("idProducto"),
                        getPath("root").get("cantidadSolicitada"),
                        getPath("root").get("cantidadConfirmada"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<OrdenCompraDetalle> find(OrdenCompraDetalleParam param) {

        AbstractFind find = new AbstractFind(OrdenCompraDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, OrdenCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<OrdenCompraDetalle> root = cq.from(OrdenCompraDetalle.class);
        
        find.addPath("root", root);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(OrdenCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<OrdenCompraDetalle> root = cq.from(OrdenCompraDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<OrdenCompraDetallePojo> getByOrdenCompra(OrdenCompraDetalleParam param) {
        
        if(!param.isValidToGetByOrdenCompra()) {
            return null;
        }
        
        String where = String.format("ocd.id_orden_compra = %d", param.getIdOrdenCompra());
        
        if(param.getId() != null) {
            where = String.format("%s and ocd.id = %d", where, param.getId());
        }
        
        String sql = String.format("select ocd.id, ocd.id_orden_compra, ocd.id_producto, p.descripcion as producto, p.codigo_gtin, p.codigo_interno,\n"
                + "ocd.cantidad_solicitada, ocd.cantidad_confirmada, coalesce(sum(case when dl is null then 0 else i.cantidad end ), 0) as cantidad_disponible,\n"
                + "ocd.porcentaje_iva, ocd.precio_unitario_con_iva, ocd.precio_unitario_sin_iva, ocd.monto_iva,\n"
                + "ocd.monto_imponible, ocd.monto_total, ocd.observacion\n"
                + "from facturacion.orden_compra_detalle ocd\n"
                + "join facturacion.orden_compra oc on oc.id = ocd.id_orden_compra\n"
                + "join info.producto p on p.id = ocd.id_producto\n"
                + "left join inventario.inventario i on i.id_producto = ocd.id_producto and i.id_empresa = %d\n"
                + "left join info.estado e on e.id = i.id_estado and e.codigo = 'DISPONIBLE'\n"
                + "left join inventario.deposito_logistico dl on dl.id = i.id_deposito_logistico and dl.id_local = oc.id_local_destino\n"
                + "where ((not e is null and not i is null) or i is null) and %s\n"
                + "group by ocd.id, p.id\n"
                + "order by ocd.id", param.getIdEmpresa(), where);
        
        Query q = getEntityManager().createNativeQuery(sql);
        List<Object[]> list = q.getResultList();
        
        List<OrdenCompraDetallePojo> result = new ArrayList<>();
        for (Object[] objects : list) {
            int i = 0;
            OrdenCompraDetallePojo item = new OrdenCompraDetallePojo();
            item.setId((Integer)objects[i++]);
            item.setIdOrdenCompra((Integer)objects[i++]);
            item.setIdProducto((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigoGtin((String)objects[i++]);
            item.setCodigoInterno((String)objects[i++]);
            item.setCantidadSolicitada((Integer)objects[i++]);
            item.setCantidadConfirmada((Integer)objects[i++]);
            item.setCantidadDisponible((Number)objects[i++]);
            item.setPorcentajeIva((Integer)objects[i++]);
            item.setPrecioUnitarioConIva((BigDecimal)objects[i++]);
            item.setPrecioUnitarioSinIva((BigDecimal)objects[i++]);
            item.setMontoIva((BigDecimal)objects[i++]);
            item.setMontoImponible((BigDecimal)objects[i++]);
            item.setMontoTotal((BigDecimal)objects[i++]);
            item.setObservacion((String)objects[i++]);
            result.add(item);
        }
        return result;
    }
    
    public Integer getCantidadOrdenCompraDetalle(Integer idOrdenCompraDetalle) {
        
        String sql = String.format("select sum(cantidad)\n"
                + "from inventario.orden_compra_detalle_inventario_detalle\n"
                + "where id_orden_compra_detalle = %d", idOrdenCompraDetalle);
        
        Query q = getEntityManager().createNativeQuery(sql);
        Object object = q.getSingleResult();
        
        return object == null ? 0 : ((Number)object).intValue();
    }
    
    @Override
    public List<OrdenCompraDetallePojo> getDisponible(Integer idLocal, Integer idProducto, Integer idEmpresa) {
        
        String sql = String.format("select i.id_producto, p.descripcion as producto, p.codigo_gtin, p.codigo_interno,\n"
                + "coalesce(sum(case when dl is null then 0 else i.cantidad end ), 0) as cantidad_disponible\n"
                + "from inventario.inventario i\n"
                + "join info.producto p on (p.id = i.id_producto)\n"
                + "join info.estado e on e.id = i.id_estado and e.codigo = 'DISPONIBLE'\n"
                + "join inventario.deposito_logistico dl on dl.id = i.id_deposito_logistico\n"
                + "where ((not e is null and not i is null) or i is null)\n"
                + "and i.id_empresa = %d and i.id_producto = %d and dl.id_local = %d\n"
                + "group by p.id, i.id_producto", idEmpresa, idProducto, idLocal);
        
        Query q = getEntityManager().createNativeQuery(sql);
        List<Object[]> list = q.getResultList();
        
        List<OrdenCompraDetallePojo> result = new ArrayList<>();
        for (Object[] objects : list) {
            int i = 0;
            OrdenCompraDetallePojo item = new OrdenCompraDetallePojo();
            item.setIdProducto((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigoGtin((String)objects[i++]);
            item.setCodigoInterno((String)objects[i++]);
            item.setCantidadDisponible((Number)objects[i++]);
            result.add(item);
        }
        return result;
    }
    
    @Override
    public List<OrdenCompraDetallePojo> getDisponibleDetalle(OrdenCompraDetalleParam param) {
        
        if(!param.isValidToGetByOrdenCompra()) {
            return null;
        }
        
        String where = String.format("ide.cantidad > 0 and ocd.id_orden_compra = %d and i.id_empresa = %d",
                param.getIdOrdenCompra(), param.getIdEmpresa());
        
        if(param.getId() != null) {
            where = String.format("%s and ocd.id = %d", where, param.getId());
        }
        
        String sql = String.format("select ocd.id as id_orden_compra_detalle, ocd.id_orden_compra, i.id as id_inventario, i.id_estado,\n"
                + "i.id_producto, i.id_deposito_logistico, i.id_empresa, ide.id as id_inventario_detalle, ocd.cantidad_solicitada,\n"
                + "ocd.cantidad_confirmada, coalesce(ide.cantidad, 0) as cantidad_disponible, ide.fecha_vencimiento\n"
                + "from facturacion.orden_compra_detalle ocd\n"
                + "join facturacion.orden_compra oc on oc.id = ocd.id_orden_compra\n"
                + "join inventario.inventario i on i.id_producto = ocd.id_producto\n"
                + "join inventario.inventario_detalle ide on ide.id_inventario = i.id\n"
                + "join info.estado e on e.id = i.id_estado and e.codigo = 'DISPONIBLE'\n"
                + "join inventario.deposito_logistico dl on dl.id = i.id_deposito_logistico and dl.id_local = oc.id_local_destino\n"
                + "where %s order by ide.fecha_vencimiento asc nulls last", where);
        
        Query q = getEntityManager().createNativeQuery(sql);
        List<Object[]> list = q.getResultList();
        
        List<OrdenCompraDetallePojo> result = new ArrayList<>();
        for (Object[] objects : list) {
            int i = 0;
            OrdenCompraDetallePojo item = new OrdenCompraDetallePojo();
            item.setId((Integer)objects[i++]);
            item.setIdOrdenCompra((Integer)objects[i++]);
            item.setIdInventario((Integer)objects[i++]);
            item.setIdEstadoInventario((Integer)objects[i++]);
            item.setIdProducto((Integer)objects[i++]);
            item.setIdDepositoLogistico((Integer)objects[i++]);
            item.setIdEmpresa((Integer)objects[i++]);
            item.setIdInventarioDetalle((Integer)objects[i++]);
            item.setCantidadSolicitada((Integer)objects[i++]);
            item.setCantidadConfirmada((Integer)objects[i++]);
            item.setCantidadDisponible((Integer)objects[i++]);
            item.setFechaVencimiento((Date)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
}
