/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

/**
 * POJO para la entidad factura detalle
 * @author Williams Vera
 */
public class FacturaParametroAdicionalPojo {

    public FacturaParametroAdicionalPojo(Integer id, Integer idFactura, Integer idParametroAdicional,
            String codigoParametroAdicional, String valor, String datoAdicional,
            String codigoTipoDatoParametroAdicional, String codigoTipoParametroAdicional) {
        this.id = id;
        this.idFactura = idFactura;
        this.idParametroAdicional = idParametroAdicional;
        this.codigoParametroAdicional = codigoParametroAdicional;
        this.valor = valor;
        this.datoAdicional = datoAdicional;
        this.codigoTipoDatoParametroAdicional = codigoTipoDatoParametroAdicional;
        this.codigoTipoParametroAdicional = codigoTipoParametroAdicional;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de factura
     */
    private Integer idFactura;

    /**
     * Identificador de parametro adicional
     */
    private Integer idParametroAdicional;

    /**
     * Codigo Parametro Adicional
     */
    private String codigoParametroAdicional;

    /**
     * Valor
     */
    private String valor;

    /**
     * Dato adicional
     */
    private String datoAdicional;

    /**
     * Codigo de tipo de dato parametro adicional
     */
    private String codigoTipoDatoParametroAdicional;
    
    /**
     * Codigo de tipo de parametro adicional
     */
    private String codigoTipoParametroAdicional;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdParametroAdicional() {
        return idParametroAdicional;
    }

    public void setIdParametroAdicional(Integer idParametroAdicional) {
        this.idParametroAdicional = idParametroAdicional;
    }

    public String getCodigoParametroAdicional() {
        return codigoParametroAdicional;
    }

    public void setCodigoParametroAdicional(String codigoParametroAdicional) {
        this.codigoParametroAdicional = codigoParametroAdicional;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getCodigoTipoDatoParametroAdicional() {
        return codigoTipoDatoParametroAdicional;
    }

    public void setCodigoTipoDatoParametroAdicional(String codigoTipoDatoParametroAdicional) {
        this.codigoTipoDatoParametroAdicional = codigoTipoDatoParametroAdicional;
    }

    public String getCodigoTipoParametroAdicional() {
        return codigoTipoParametroAdicional;
    }

    public void setCodigoTipoParametroAdicional(String codigoTipoParametroAdicional) {
        this.codigoTipoParametroAdicional = codigoTipoParametroAdicional;
    }

}
