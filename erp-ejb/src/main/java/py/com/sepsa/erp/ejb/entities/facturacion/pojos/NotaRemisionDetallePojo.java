/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad factura detalle
 * @author Williams Vera
 */
public class NotaRemisionDetallePojo {
    
    public NotaRemisionDetallePojo(Integer id, Integer idNotaRemision, Integer nroLinea,
            BigDecimal cantidad,  String descripcion, Integer idProducto,Integer idMetrica, String codigoMetrica,
            Date fechaVencimientoLote, String nroLote) {
        this.idNotaRemision = idNotaRemision;
        this.nroLinea = nroLinea;
        this.cantidad = cantidad;
        this.descripcion = descripcion;
        this.idProducto = idProducto;
        this.idMetrica = idMetrica;
        this.codigoMetrica = codigoMetrica;
        this.fechaVencimientoLote = fechaVencimientoLote;
        this.nroLote = nroLote;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de nota de créditp
     */
    private Integer idNotaRemision;
    
    /**
     * Nro nota de crédito
     */
    private String nroNotaRemision;
    
    /**
     * Fecha nota de crédito
     */
    private Date fechaNotaRemision;
    
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Id de producto
     */
    private Integer idProducto;
    
    /**
     * Unidad de medida 
     */
    private Integer idMetrica;
    
    private String codigoMetrica;
    /**
     * Fecha vencimiento lote
     */
    private Date fechaVencimientoLote;
    
    /**
     * Nro lote
     */
    private String nroLote;

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    }
    
   
    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Integer idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public Date getFechaNotaRemision() {
        return fechaNotaRemision;
    }

    public void setFechaNotaRemision(Date fechaNotaRemision) {
        this.fechaNotaRemision = fechaNotaRemision;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Integer idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

}
