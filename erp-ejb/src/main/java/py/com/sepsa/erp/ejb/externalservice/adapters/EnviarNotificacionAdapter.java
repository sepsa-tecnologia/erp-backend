package py.com.sepsa.erp.ejb.externalservice.adapters;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;
import static py.com.sepsa.erp.ejb.externalservice.http.ResponseCode.OK;
import py.com.sepsa.erp.ejb.externalservice.notificacion.Notification;
import py.com.sepsa.erp.ejb.externalservice.notificacion.NotificationType;
import py.com.sepsa.erp.ejb.externalservice.service.APINotificacionServer;

/**
 *
 * @author Jonathan D. Bernal Fernandez
 */
public class EnviarNotificacionAdapter extends AbstractAdapterImpl<Notification, HttpStringResponse> {
    
    /**
     * Destino
     */
    private String para;
    
    /**
     * Código de tipo de notificación
     */
    private String codigoTipoNotificacion;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Asunto
     */
    private String asunto;
    
    /**
     * Mensaje
     */
    private String mensaje;

    /**
     * Realiza una petición al servicio
     *
     */
    @Override
    public void request() {
        
        APINotificacionServer api = new APINotificacionServer();
        
        JsonElement json = createJson();
        
            
        this.resp = api.crearNotificacion(json);

        JsonObject payload;

        switch(resp.getRespCode()) {
            case OK:
                payload = new JsonParser().parse(resp.getPayload()).getAsJsonObject();
                setData(payload);
                return;
        }
    }
    
    /**
     * Crea el JSON petición 
     * @return JSON
     */
    private JsonElement createJson() {
        
        NotificationType nt = new NotificationType();
        nt.setCode(codigoTipoNotificacion);
        
        Notification notification = new Notification();
        notification.setNotificationType(nt);
        notification.setTo(para);
        notification.setMessage(mensaje);
        notification.setSubject(asunto);
        notification.setState(estado);
        
        Gson gson = new Gson();
        
        JsonElement result = gson.toJsonTree(notification, Notification.class);
        
        return result;
    }

    public EnviarNotificacionAdapter(String asunto, String mensaje, String para,
            String codigoTipoNotificacion, String estado) {
        super(new HttpStringResponse());
        this.para = para;
        this.codigoTipoNotificacion = codigoTipoNotificacion;
        this.estado = estado;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    
}
