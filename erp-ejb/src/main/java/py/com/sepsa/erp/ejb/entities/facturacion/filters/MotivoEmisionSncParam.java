/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
public class MotivoEmisionSncParam extends CommonParam {
    
    public MotivoEmisionSncParam() {
    }

    public MotivoEmisionSncParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Código XML
     */
    @QueryParam("codigoXml")
    protected String codigoXml;
    
    /**
     * Código TXT
     */
    @QueryParam("codigoTxt")
    protected String codigoTxt;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigoXml)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código XML"));
        }
        
        if(isNullOrEmpty(codigoTxt)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código TXT"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigoXml)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código XML"));
        }
        
        if(isNullOrEmpty(codigoTxt)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código TXT"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    public String getCodigoXml() {
        return codigoXml;
    }

    public void setCodigoXml(String codigoXml) {
        this.codigoXml = codigoXml;
    }

    public String getCodigoTxt() {
        return codigoTxt;
    }

    public void setCodigoTxt(String codigoTxt) {
        this.codigoTxt = codigoTxt;
    }
}