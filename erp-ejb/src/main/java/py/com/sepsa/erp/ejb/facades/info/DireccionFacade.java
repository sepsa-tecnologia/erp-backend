/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Direccion;
import py.com.sepsa.erp.ejb.entities.info.filters.DireccionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface DireccionFacade extends Facade<Direccion, DireccionParam, UserInfoImpl> {

    public Boolean validToCreate(DireccionParam param);

    public Boolean validToEdit(DireccionParam param);
    
}
