/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de empresa
 * @author Jonathan
 */
public class EmpresaParam extends CommonParam {
    
    public EmpresaParam() {
    }

    public EmpresaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Identificador de tipo de empresa
     */
    @QueryParam("idTipoEmpresa")
    private Integer idTipoEmpresa;
    
    /**
     * Código de tipo de empresa
     */
    @QueryParam("codigoTipoEmpresa")
    private String codigoTipoEmpresa;
    
    /**
     * Identificador de cliente sepsa
     */
    @QueryParam("idClienteSepsa")
    private Integer idClienteSepsa;
    
    /**
     * Codigo de template
     */
    @QueryParam("idClienteSepsa")
    private String codigoTemplate;
    
    /**
     * Email de usuario
     */
    @QueryParam("usuario")
    private String usuario;
    
    /**
     * API url para url Destino
     */
    @QueryParam("apiUrl")
    private String apiUrl;
    
    /**
     * Facturador electrónico
     */
    @QueryParam("facturadorElectronico")
    private Character facturadorElectronico;
    
    /**
     * Lista de usuarios
     */
    private List<UsuarioParam> usuarios;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoEmpresa) && isNullOrEmpty(codigoTipoEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de empresa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción de la empresa"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de la empresa"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC de la empresa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idClienteSepsa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente sepsa"));
        }
        
//        if(isNull(usuario)) {
//            addError(MensajePojo.createInstance()
//                    .descripcion("Se debe indicar el usuario (Debe ser un correo electronico válido)"));
//        }
        
        if(isNull(apiUrl)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el API url."));
        }
        
        if(isNull(facturadorElectronico)) {
            facturadorElectronico = 'S';
        }
        
        if(!isNullOrEmpty(usuarios)) {
            for (UsuarioParam usuario : usuarios) {
                usuario.setIdEmpresa(0);
                usuario.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idTipoEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de empresa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción de la empresa"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de la empresa"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC de la empresa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idClienteSepsa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente sepsa"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNullOrEmpty(usuarios)) {
            for (UsuarioParam usuario : usuarios) {
                list.addAll(usuario.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    public void setIdTipoEmpresa(Integer idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }

    public void setUsuarios(List<UsuarioParam> usuarios) {
        this.usuarios = usuarios;
    }

    public List<UsuarioParam> getUsuarios() {
        return usuarios;
    }

    public String getCodigoTemplate() {
        return codigoTemplate;
    }

    public void setCodigoTemplate(String  codigoTemplate) {
        this.codigoTemplate = codigoTemplate;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public void setFacturadorElectronico(Character facturadorElectronico) {
        this.facturadorElectronico = facturadorElectronico;
    }

    public Character getFacturadorElectronico() {
        return facturadorElectronico;
    }
    
}
