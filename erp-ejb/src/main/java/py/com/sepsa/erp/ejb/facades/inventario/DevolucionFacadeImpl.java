/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.Devolucion;
import py.com.sepsa.erp.ejb.entities.inventario.DevolucionDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DevolucionDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DevolucionParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.DevolucionPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "DevolucionFacade", mappedName = "DevolucionFacade")
@Local(DevolucionFacade.class)
public class DevolucionFacadeImpl extends FacadeImpl<Devolucion, DevolucionParam> implements DevolucionFacade {

    public DevolucionFacadeImpl() {
        super(Devolucion.class);
    }
    
    public Boolean validToCreate(DevolucionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        if(param.getIdFactura() != null) {
            FacturaParam fparam = new FacturaParam();
            fparam.setId(param.getIdFactura());
            Long fsize = facades.getFacturaFacade().findSize(fparam);
            
            if(fsize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "DEVOLUCION");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(param.getDevolucionDetalles() != null) {
            for (DevolucionDetalleParam item : param.getDevolucionDetalles()) {
                facades.getDevolucionDetalleFacade().validToCreate(item, false);
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(DevolucionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        DevolucionParam dparam = new DevolucionParam();
        dparam.setId(param.getId());
        Long dsize = findSize(dparam);
        
        if(dsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la devolución"));
        }
        
        if(param.getIdFactura() != null) {
            FacturaParam fparam = new FacturaParam();
            fparam.setId(param.getIdFactura());
            Long fsize = facades.getFacturaFacade().findSize(fparam);
            
            if(fsize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "DEVOLUCION");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Devolucion create(DevolucionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Devolucion result = new Devolucion();
        result.setIdEmpresa(userInfo.getIdEmpresa());
        result.setAnulado(param.getAnulado());
        result.setIdEstado(param.getIdEstado());
        result.setIdFactura(param.getIdFactura());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        
        create(result);
        
        List<DevolucionDetalle> detalles = new ArrayList();
        
        if(param.getDevolucionDetalles() != null) {
            for (DevolucionDetalleParam detalle : param.getDevolucionDetalles()) {
                detalle.setIdDevolucion(result.getId());
                DevolucionDetalle item = facades.getDevolucionDetalleFacade().create(detalle, userInfo);
                detalles.add(item);
            }
        }
        
        if(userInfo.getTieneModuloInventario()) {
            facades.getInventarioUtils().actualizarDevolucion(userInfo, result, detalles);
        }
        
        return result;
    }

    /*@Override
    public Devolucion edit(DevolucionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Devolucion result = find(param.getId());
        result.setAnulado(param.getAnulado());
        result.setIdEstado(param.getIdEstado());
        result.setIdFactura(param.getIdFactura());
        
        edit(result);
        
        return result;
    }*/
    
    @Override
    public List<DevolucionPojo> findPojo(DevolucionParam param) {

        AbstractFind find = new AbstractFind(DevolucionPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("anulado"),
                        getPath("root").get("idFactura"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("codigo"),
                        getPath("estado").get("descripcion"),
                        getPath("root").get("fechaInsercion"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<Devolucion> find(DevolucionParam param) {

        AbstractFind find = new AbstractFind(Devolucion.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, DevolucionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Devolucion> root = cq.from(Devolucion.class);
        Join<Devolucion, Empresa> empresa = root.join("empresa");
        Join<Devolucion, Estado> estado = root.join("estado");
        
        find.addPath("root", root);
        find.addPath("estado", estado);
        find.addPath("empresa", empresa);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdEmpresa( )!= null) {
            predList.add(qb.equal(empresa.get("id"), param.getIdEmpresa()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(DevolucionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Devolucion> root = cq.from(Devolucion.class);
        Join<Devolucion, Estado> estado = root.join("estado");
        Join<Devolucion, Empresa> empresa = root.join("empresa");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdEmpresa( )!= null) {
            predList.add(qb.equal(empresa.get("id"), param.getIdEmpresa()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
