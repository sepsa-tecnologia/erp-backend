/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.io.Serializable;

/**
 * POJO para la clase liquidación
 * @author Jonathan
 */
public class LiquidacionDetalleCadenaPojo implements Serializable {
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Identificador de contrato
     */
    private Integer idContrato;
    
    /**
     * Identificador del producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

}
