/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de tipo de tarifa
 * @author Jonathan D. Bernal Fernández
 */
public class ServicioParam extends CommonParam {
    
    public ServicioParam() {
    }

    public ServicioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Código de producto
     */
    @QueryParam("codigoProducto")
    private String codigoProducto;
    
    /**
     * Producto
     */
    @QueryParam("producto")
    private String producto;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
