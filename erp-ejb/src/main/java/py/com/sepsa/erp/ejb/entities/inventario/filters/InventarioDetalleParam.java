/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class InventarioDetalleParam extends CommonParam {

    public InventarioDetalleParam() {
    }

    public InventarioDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de inventario
     */
    @QueryParam("idInventario")
    private Integer idInventario;
    
    /**
     * Identificador de depósito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Tiene fecha de vencimiento
     */
    @QueryParam("tieneFechaVencimiento")
    private Boolean tieneFechaVencimiento;
    
    /**
     * Fecha de vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;
    
    /**
     * Fecha de vencimiento desde
     */
    @QueryParam("fechaVencimientoDesde")
    private Date fechaVencimientoDesde;
    
    /**
     * Fecha de vencimiento hasta
     */
    @QueryParam("fechaVencimientoHasta")
    private Date fechaVencimientoHasta;
    
    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    protected Integer cantidad;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de inventario"));
        }
        
        if(isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de inventario"));
        }
        
        if(isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        return !tieneErrores();
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setFechaVencimientoHasta(Date fechaVencimientoHasta) {
        this.fechaVencimientoHasta = fechaVencimientoHasta;
    }

    public Date getFechaVencimientoHasta() {
        return fechaVencimientoHasta;
    }

    public void setFechaVencimientoDesde(Date fechaVencimientoDesde) {
        this.fechaVencimientoDesde = fechaVencimientoDesde;
    }

    public Date getFechaVencimientoDesde() {
        return fechaVencimientoDesde;
    }

    public void setTieneFechaVencimiento(Boolean tieneFechaVencimiento) {
        this.tieneFechaVencimiento = tieneFechaVencimiento;
    }

    public Boolean getTieneFechaVencimiento() {
        return tieneFechaVencimiento;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }
}
