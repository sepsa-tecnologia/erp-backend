/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.Perfil;
import py.com.sepsa.erp.ejb.entities.usuario.filters.PerfilParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface PerfilFacade extends Facade<Perfil, PerfilParam, UserInfoImpl> {

    public List<Perfil> getListPerfil(Integer idEmpresa, String codigoEmpresa, Integer userId);

    public List<Perfil> findPerfil(Integer perfilId);
    
    public Perfil find(Integer idEmpresa, Integer id, String codigo);
    
}
