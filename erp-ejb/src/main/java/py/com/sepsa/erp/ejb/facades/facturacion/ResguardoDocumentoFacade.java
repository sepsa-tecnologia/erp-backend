/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.io.IOException;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.ResguardoDocumento;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ResguardoDocumentoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ResguardoDocumentoFacade extends Facade<ResguardoDocumento, ResguardoDocumentoParam, UserInfoImpl> {

    public String generateDownloadUrl(ResguardoDocumentoParam param) throws IOException;
    
}
