/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class LocalEntregaParam extends CommonParam {
    
    public LocalEntregaParam(){
    }
    
    public LocalEntregaParam(String bruto){
        super(bruto);
    }
    
    @QueryParam("direccion")
    private String direccion;
    
    @QueryParam("nroCasa")
    private Integer nroCasa;
    
    @QueryParam("idDepartamento")
    private Integer idDepartamento;
    
    @QueryParam("idDistrito")
    private Integer idDistrito;
    
    @QueryParam("idCiudad")
    private Integer idCiudad;

    
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }
    
    
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(direccion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la dirección de entrega"));
        }
        
        if(isNull(nroCasa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de casa de entrega"));
        }
        
        if(isNull(idDepartamento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de departamento de entrega"));
        }
        
        if(isNull(idDistrito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de distrito de entrega"));
        }
        
        
        if(isNull(idCiudad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de ciudad de entrega"));
        }
        
        return !tieneErrores();
    }

    
}
