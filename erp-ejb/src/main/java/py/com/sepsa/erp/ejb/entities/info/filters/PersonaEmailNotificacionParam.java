/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contacto email
 * @author Jonathan
 */
public class PersonaEmailNotificacionParam extends CommonParam {
    
    public PersonaEmailNotificacionParam() {
    }

    public PersonaEmailNotificacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de contacto
     */
    @QueryParam("idContacto")
    private Integer idContacto;
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Identificador de email
     */
    @QueryParam("idEmail")
    private Integer idEmail;
    
    /**
     * Contacto
     */
    @QueryParam("contacto")
    private String contacto;
    
    /**
     * Email
     */
    @QueryParam("email")
    private String email;
    
    /**
     * Identificador de tipo de notificacion
     */
    @QueryParam("idTipoNotificacion")
    private Integer idTipoNotificacion;
    
    /**
     * Tipo de notificacion
     */
    @QueryParam("tipoNotificacion")
    private String tipoNotificacion;
    
    /**
     * Código de Tipo de notificacion
     */
    @QueryParam("codigoTipoNotificacion")
    private String codigoTipoNotificacion;
    
    /**
     * Contacto email activo
     */
    @QueryParam("contactoEmailActivo")
    private Character contactoEmailActivo;
    
    /**
     * Contacto activo
     */
    @QueryParam("contactoActivo")
    private Character contactoActivo;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de contacto"));
        }
        
        if(isNull(idEmail)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona"));
        }
        
        if(isNull(idTipoNotificacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de notificación"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public Character getContactoEmailActivo() {
        return contactoEmailActivo;
    }

    public void setContactoEmailActivo(Character contactoEmailActivo) {
        this.contactoEmailActivo = contactoEmailActivo;
    }

    public Character getContactoActivo() {
        return contactoActivo;
    }

    public void setContactoActivo(Character contactoActivo) {
        this.contactoActivo = contactoActivo;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }
}
