
package py.com.sepsa.erp.ejb.externalservice.notificacion;

import java.util.Date;

/**
 * POJO de la entidad mensaje
 * @author Daniel F. Escauriza Arza
 */
public class Message {
    
    /**
     * Identitificador de la entidad mensaje
     */
    private Integer id;
    
    /**
     * Datos de la notificación 
     */
    private Notification notification;
    
    /**
     * Datos del tipo de contenido
     */
    private ContentType contentType;
    
    /**
     * Datos del producto
     */
    private Product product;
    
    /**
     * Datos del evento
     */
    private Event event;
    
    /**
     * Fecha de registro del mensaje
     */
    private Date registerDate;
    
    /**
     * Fecha del proceso del mensaje
     */
    private Date processDate;
    
    /**
     * Mensaje
     */
    private String message;
    
    /**
     * Bandera que indica si el mensaje fue procesado
     */
    private String processed;
    
    /**
     * Destino del mensaje
     */
    private String to;
    
    /**
     * Código de agrupación de mensaje
     */
    private String groupCode;

    /**
     * Obtiene el identificador de la entidad mensaje
     * @return Identificador de la entidad mensaje
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador de la entidad mensaje
     * @param id Identificador de la entidad mensaje
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene los datos de la notificación
     * @return Datos de la notificación
     */
    public Notification getNotification() {
        return notification;
    }

    /**
     * Setea los datos de la notificación
     * @param notification Datos de la notificación
     */
    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    /**
     * Obtiene los datos del tipo de contenido
     * @return Datos del tipo de contenido
     */
    public ContentType getContentType() {
        return contentType;
    }

    /**
     * Setea los datos del tipo de contenido
     * @param contentType Datos del tipo de contenido
     */
    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    /**
     * Obtiene los datos del producto
     * @return Datos del producto
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Setea los datos del producto
     * @param product Datos del producto
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Obtiene los datos del evento
     * @return Datos del evento
     */
    public Event getEvent() {
        return event;
    }

    /**
     * Setea los datos del evento
     * @param event Datos del evento
     */
    public void setEvent(Event event) {
        this.event = event;
    }
    
    /**
     * Obtiene la fecha de registro del mensaje
     * @return Fecha de registro del mensaje
     */
    public Date getRegisterDate() {
        return registerDate;
    }

    /**
     * Setea la fecha de registro del mensaje
     * @param registerDate Fecha de registro del mensaje
     */
    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * Obtiene la fecha del proceso del mensaje
     * @return Fecha del proceso del mensaje
     */
    public Date getProcessDate() {
        return processDate;
    }

    /**
     * Setea la fecha del proceso del mensaje
     * @param processDate Fecha del proceso del mensaje
     */
    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    /**
     * Obtiene el mensaje
     * @return Mensaje
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setea el mensaje
     * @param message Mensaje
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Obtiene la bandera que indica si el mensaje fue procesado
     * @return Bandera que indica si el mensaje fue procesado
     */
    public String getProcessed() {
        return processed;
    }

    /**
     * Setea la bandera que indica si el mensaje fue procesado
     * @param processed Bandera que indica si el mensaje fue procesado
     */
    public void setProcessed(String processed) {
        this.processed = processed;
    }

    /**
     * Obtiene el destino del mensaje
     * @return Destino del mensaje
     */
    public String getTo() {
        return to;
    }

    /**
     * Setea el destino del mensaje
     * @param to Destino del mensaje
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * Obtiene el código de agrupación del mensaje
     * @return Código de agrupación del mensaje
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * Setea el código de agrupación del mensaje
     * @param groupCode Código de agrupación del mensaje
     */
    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * Constructor de Message
     * @param id Identificador de la entidad mensaje
     * @param notificationId Identificador de la entidad notificación
     * @param notificationMessage Mensaje de la notificación
     * @param contentTypeId Identificador de la entidad tipo contenido
     * @param contentTypeCode Código del tipo de contenido
     * @param contentTypeDescription Descripción del tipo de contenido
     * @param productId Identificador de la entidad producto
     * @param productCode Código del producto
     * @param productDescription Descripción del producto
     * @param productUrl URL del producto
     * @param eventId Identificador de la entidad evento
     * @param eventCode Código del evento
     * @param eventDescription Descripción del evento
     * @param registerDate Fecha de registro del mensaje
     * @param processDate Fecha de proceso del mensaje
     * @param message Mensaje
     * @param processed Bandera que indica si el mensaje fue procesado
     * @param to Destino del mensaje
     * @param groupCode Código de agrupación del mensaje
     */
    public Message(Integer id, Integer notificationId, 
            String notificationMessage, Integer contentTypeId, 
            String contentTypeCode, String contentTypeDescription, 
            Integer productId, String productCode, String productDescription, 
            String productUrl, Integer eventId, String eventCode, 
            String eventDescription, Date registerDate, Date processDate, 
            String message, Character processed, String to, String groupCode) {
        
        this.id = id;
        this.notification = new Notification(notificationId, 
                notificationMessage);
        this.contentType = new ContentType(contentTypeId, contentTypeCode, 
                contentTypeDescription);
        this.product = new Product(productId, productDescription, productCode, 
                null, null, productUrl);
        this.event = new Event(eventId, eventCode, eventDescription, null);
        this.registerDate = registerDate;
        this.processDate = processDate;
        this.message = message;
        this.processed = processed == null ? null : processed.toString();
        this.to = to;
        this.groupCode = groupCode;
    }
    
    /**
     * Constructor de Message
     * @param id Identificador de la entidad mensaje
     * @param notification Datos de la notificación
     * @param contentType Datos del tipo de contenido
     * @param product Datos del producto
     * @param event Datos del evento
     * @param registerDate Fecha de registro del mensaje
     * @param processDate Fecha del preceso del mensaje
     * @param message Mensaje
     * @param processed Bandera que indica si el mensaje fue procesado
     * @param to Destino del mensaje
     * @param groupCode Código de agrupación del mensaje
     */
    public Message(Integer id, Notification notification, 
            ContentType contentType, Product product, Event event, 
            Date registerDate, Date processDate, String message, 
            String processed, String to, String groupCode) {
        this.id = id;
        this.notification = notification;
        this.contentType = contentType;
        this.product = product;
        this.event = event;
        this.registerDate = registerDate;
        this.processDate = processDate;
        this.message = message;
        this.processed = processed;
        this.to = to;
        this.groupCode = groupCode;
    }

    /**
     * Constructor de Message
     */
    public Message() {}
    
}
