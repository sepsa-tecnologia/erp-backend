/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de excedente detalle
 * @author Jonathan D. Bernal Fernández
 */
public class ExcedenteDetalleParam extends CommonParam {
    
    public ExcedenteDetalleParam() {
    }

    public ExcedenteDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de historico liquidacion
     */
    @QueryParam("idHistoricoLiquidacion")
    private Integer idHistoricoLiquidacion;
    
    /**
     * Cantidad excedida
     */
    @QueryParam("cantidadExcedida")
    private Integer cantidadExcedida;
    
    /**
     * Monto excedente
     */
    @QueryParam("montoExcedente")
    private BigDecimal montoExcedente;
    
    /**
     * Identificador de excedente
     */
    @QueryParam("idExcedente")
    private Integer idExcedente;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }
    
    @Override
    public boolean isValidToCreate() {
        limpiarErrores();
        
        if(isNull(idHistoricoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de "
                            + "historico liquidación"));
        }
        
        if(isNull(cantidadExcedida)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar la cantidad excedida"));
        }
        
        if(isNull(montoExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto excedente"));
        }
        
        if(isNull(idExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de excedente"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de excedente detalle"));
        }
        
        if(isNull(idHistoricoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de "
                            + "historico liquidación"));
        }
        
        if(isNull(cantidadExcedida)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar la cantidad excedida"));
        }
        
        if(isNull(montoExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto excedente"));
        }
        
        if(isNull(idExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de excedente"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public void setCantidadExcedida(Integer cantidadExcedida) {
        this.cantidadExcedida = cantidadExcedida;
    }

    public Integer getCantidadExcedida() {
        return cantidadExcedida;
    }

    public BigDecimal getMontoExcedente() {
        return montoExcedente;
    }

    public void setMontoExcedente(BigDecimal montoExcedente) {
        this.montoExcedente = montoExcedente;
    }

    public Integer getIdExcedente() {
        return idExcedente;
    }

    public void setIdExcedente(Integer idExcedente) {
        this.idExcedente = idExcedente;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
