/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Direccion;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.DireccionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.info.pojos.DireccionPojo;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "DireccionFacade", mappedName = "DireccionFacade")
@Local(DireccionFacade.class)
public class DireccionFacadeImpl extends FacadeImpl<Direccion, DireccionParam> implements DireccionFacade {
    
    public DireccionFacadeImpl() {
        super(Direccion.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(DireccionParam param) {
       
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ReferenciaGeografica departamento = null;
        ReferenciaGeografica distrito = null;
        ReferenciaGeografica ciudad;
        
        if(param.getIdDepartamento() != null) {
            
            departamento = facades.getReferenciaGeograficaFacade() .find(param.getIdDepartamento());
            
            if(departamento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el departamento"));
            }
        }
        
        if(param.getIdDistrito() != null) {
            
            distrito = facades.getReferenciaGeograficaFacade().find(param.getIdDistrito());
            
            if(distrito == null) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el distrito"));
                
            } else if(departamento != null && !departamento.getId().equals(distrito.getIdPadre())) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("El distrito no está relacionado al departamento"));
            }
        }
        
        if(param.getIdCiudad() != null) {
            
            ciudad = facades.getReferenciaGeograficaFacade().find(param.getIdCiudad());
            
            if(ciudad == null) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la ciudad"));
                
            } else if(distrito != null && !distrito.getId().equals(ciudad.getIdPadre())) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("La ciudad no está relacionada al distrito"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param Parámetros
     * @return Bandera
     */
    @Override
    public Boolean validToEdit(DireccionParam param) {
       
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Direccion direccion = find(param.getId());
        
        if(direccion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro"));
        }
        
        ReferenciaGeografica departamento = null;
        ReferenciaGeografica distrito = null;
        ReferenciaGeografica ciudad;
        
        if(param.getIdDepartamento() != null) {
            
            departamento = facades.getReferenciaGeograficaFacade().find(param.getIdDepartamento());
            
            if(departamento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el departamento"));
            }
        }
        
        if(param.getIdDistrito() != null) {
            
            distrito = facades.getReferenciaGeograficaFacade().find(param.getIdDistrito());
            
            if(distrito == null) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el distrito"));
                
            } else if(departamento != null && !departamento.getId().equals(distrito.getIdPadre())) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("El distrito no está relacionado al departamento"));
            }
        }
        
        if(param.getIdCiudad() != null) {
            
            ciudad = facades.getReferenciaGeograficaFacade().find(param.getIdCiudad());
            
            if(ciudad == null) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la ciudad"));
                
            } else if(distrito != null && !distrito.getId().equals(ciudad.getIdPadre())) {
                
                param.addError(MensajePojo.createInstance()
                        .descripcion("La ciudad no está relacionada al distrito"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia
     * @param param Parámetros
     * @param userInfo Identificador de usuario
     * @return Direccion
     */
    @Override
    public Direccion create(DireccionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Direccion direccion = new Direccion();
        direccion.setDireccion(param.getDireccion());
        direccion.setLatitud(param.getLatitud());
        direccion.setLongitud(param.getLongitud());
        direccion.setNumero(param.getNumero());
        direccion.setObservacion(param.getObservacion());
        direccion.setIdDepartamento(param.getIdDepartamento());
        direccion.setIdDistrito(param.getIdDistrito());
        direccion.setIdCiudad(param.getIdCiudad());
        
        create(direccion);
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id", direccion.getId() + " ");
        atributos.put("numero", param.getNumero() + " ");
        atributos.put("observacion", param.getObservacion() + " ");
        atributos.put("latitud", param.getLatitud() + " ");
        atributos.put("longitud", param.getLongitud() + " ");
        atributos.put("direccion", param.getDireccion() + " ");
        atributos.put("id_departamento", param.getIdDepartamento() + " ");
        atributos.put("id_distrito", param.getIdDistrito() + " ");
        atributos.put("id_ciudad", param.getIdCiudad() + " ");

        facades.getRegistroFacade().create("info", "direccion", atributos, userInfo);
        
        return direccion;
    }
    
    /**
     * Edita una instancia
     * @param param Parámetros
     * @param userInfo Identificador de usuario
     * @return Direccion
     */
    @Override
    public Direccion edit(DireccionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Direccion direccion = find(param.getId());
        direccion.setDireccion(param.getDireccion());
        direccion.setLatitud(param.getLatitud());
        direccion.setLongitud(param.getLongitud());
        direccion.setNumero(param.getNumero());
        direccion.setObservacion(param.getObservacion());
        direccion.setIdDepartamento(param.getIdDepartamento());
        direccion.setIdDistrito(param.getIdDistrito());
        direccion.setIdCiudad(param.getIdCiudad());
        
        edit(direccion);
        
        Map<String, String> pk = new HashMap();
        pk.put("id", direccion.getId() + " ");
        Map<String, String> atributos = new HashMap();
        atributos.put("numero", param.getNumero() + " ");
        atributos.put("observacion", param.getObservacion() + " ");
        atributos.put("latitud", param.getLatitud() + " ");
        atributos.put("longitud", param.getLongitud() + " ");
        atributos.put("direccion", param.getDireccion() + " ");
        atributos.put("id_departamento", param.getIdDepartamento() + " ");
        atributos.put("id_distrito", param.getIdDistrito() + " ");
        atributos.put("id_ciudad", param.getIdCiudad() + " ");

        facades.getRegistroFacade().edit("info", "direccion", atributos, userInfo, pk);
        
        return direccion;
    }
    
    @Override
    public List<Direccion> find(DireccionParam param) {

        String where = "true";      

        if (param.getId() != null) {
            where = String.format("%s and d.id = '%d'", where, param.getId());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format("%s and p.id = '%d'", where, param.getIdPersona());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format("%s and d.observacion = '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getLatitud() != null && !param.getLatitud().trim().isEmpty()) {
            where = String.format("%s and d.latitud = '%s'", where, param.getLatitud().trim());
        }

        if (param.getLongitud() != null && !param.getLongitud().trim().isEmpty()) {
            where = String.format("%s and d.longitud = '%s'", where, param.getLongitud().trim());
        }

        if (param.getDireccion() != null && !param.getDireccion().trim().isEmpty()) {
            where = String.format("%s and d.direccion = '%%%s%%'", where, param.getDireccion().trim());
        }
        
        if (param.getIdDepartamento() != null) {
            where = String.format("%s and d.id_departamento = '%d'", where, param.getIdDepartamento());
        }
        
        if (param.getIdDistrito() != null) {
            where = String.format("%s and d.id_distrito = '%d'", where, param.getIdDistrito());
        }
        
        if (param.getIdCiudad() != null) {
            where = String.format("%s and d.id_ciudad = '%d'", where, param.getIdCiudad());
        }

        String sql = String.format("select d.*"
                + " from info.direccion d"
                + " left join info.persona p on p.id_direccion = d.id"
                + " left join info.referencia_geografica rfde on rfde.id = d.id_departamento"
                + " left join info.referencia_geografica rfdi on rfdi.id = d.id_distrito"
                + " left join info.referencia_geografica rfci on rfci.id = d.id_ciudad"
                + " where %s order by d.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Direccion.class);
        
        List<Direccion> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public List<DireccionPojo> findPojo(DireccionParam param) {

        String where = "true";      

        if (param.getId() != null) {
            where = String.format("%s and d.id = '%d'", where, param.getId());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format("%s and p.id = '%d'", where, param.getIdPersona());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format("%s and d.observacion = '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getLatitud() != null && !param.getLatitud().trim().isEmpty()) {
            where = String.format("%s and d.latitud = '%s'", where, param.getLatitud().trim());
        }

        if (param.getLongitud() != null && !param.getLongitud().trim().isEmpty()) {
            where = String.format("%s and d.longitud = '%s'", where, param.getLongitud().trim());
        }

        if (param.getDireccion() != null && !param.getDireccion().trim().isEmpty()) {
            where = String.format("%s and d.direccion = '%%%s%%'", where, param.getDireccion().trim());
        }
        
        if (param.getIdDepartamento() != null) {
            where = String.format("%s and d.id_departamento = '%d'", where, param.getIdDepartamento());
        }
        
        if (param.getIdDistrito() != null) {
            where = String.format("%s and d.id_distrito = '%d'", where, param.getIdDistrito());
        }
        
        if (param.getIdCiudad() != null) {
            where = String.format("%s and d.id_ciudad = '%d'", where, param.getIdCiudad());
        }

        String sql = String.format("select d.id, p.id as id_persona, d.numero, d.observacion, d.latitud, d.longitud,\n"
                + " d.direccion, d.id_departamento, rfde.codigo as codigo_departamento, d.id_distrito, rfdi.codigo as codigo_distrito,\n"
                + " d.id_ciudad, rfci.codigo as codigo_ciudad"
                + " from info.direccion d"
                + " left join info.persona p on p.id_direccion = d.id"
                + " left join info.referencia_geografica rfde on rfde.id = d.id_departamento"
                + " left join info.referencia_geografica rfdi on rfdi.id = d.id_distrito"
                + " left join info.referencia_geografica rfci on rfci.id = d.id_ciudad"
                + " where %s order by d.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<DireccionPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            DireccionPojo item = new DireccionPojo();
            item.setId((Integer)objects[i++]);
            item.setIdPersona((Integer)objects[i++]);
            item.setNumero((Integer)objects[i++]);
            item.setObservacion((String)objects[i++]);
            item.setLatitud((String)objects[i++]);
            item.setLongitud((String)objects[i++]);
            item.setDireccion((String)objects[i++]);
            item.setIdDepartamento((Integer)objects[i++]);
            item.setDepartamento((String)objects[i++]);
            item.setIdDistrito((Integer)objects[i++]);
            item.setDistrito((String)objects[i++]);
            item.setIdCiudad((Integer)objects[i++]);
            item.setCiudad((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public Long findSize(DireccionParam param) {

        String where = "true";      

        if (param.getId() != null) {
            where = String.format("%s and d.id = '%d'", where, param.getId());
        }

        if (param.getIdPersona() != null) {
            where = String.format("%s and p.id = '%d'", where, param.getIdPersona());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format("%s and d.observacion = '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getLatitud() != null && !param.getLatitud().trim().isEmpty()) {
            where = String.format("%s and d.latitud = '%s'", where, param.getLatitud().trim());
        }

        if (param.getLongitud() != null && !param.getLongitud().trim().isEmpty()) {
            where = String.format("%s and d.longitud = '%s'", where, param.getLongitud().trim());
        }

        if (param.getDireccion() != null && !param.getDireccion().trim().isEmpty()) {
            where = String.format("%s and d.direccion = '%%%s%%'", where, param.getDireccion().trim());
        }
        
        if (param.getIdDepartamento() != null) {
            where = String.format("%s and d.id_departamento = '%d'", where, param.getIdDepartamento());
        }
        
        if (param.getIdDistrito() != null) {
            where = String.format("%s and d.id_distrito = '%d'", where, param.getIdDistrito());
        }
        
        if (param.getIdCiudad() != null) {
            where = String.format("%s and d.id_ciudad = '%d'", where, param.getIdCiudad());
        }

        String sql = String.format("select count(d.id)"
                + " from info.direccion d"
                + " left join info.persona p on p.id_direccion = d.id"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
