/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.TipoArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.TipoDocumentoProceso;
import py.com.sepsa.erp.ejb.entities.proceso.filters.TipoDocumentoProcesoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoDocumentoProcesoFacade", mappedName = "TipoDocumentoProcesoFacade")
@Local(TipoDocumentoProcesoFacade.class)
public class TipoDocumentoProcesoFacadeImpl extends FacadeImpl<TipoDocumentoProceso, TipoDocumentoProcesoParam> implements TipoDocumentoProcesoFacade {

    public TipoDocumentoProcesoFacadeImpl() {
        super(TipoDocumentoProceso.class);
    }
    
    @Override
    public TipoDocumentoProceso find(Integer id, String codigo, Integer idTipoArchivo, String codigoTipoArchivo) {
        
        TipoDocumentoProcesoParam param = new TipoDocumentoProcesoParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.setIdTipoArchivo(idTipoArchivo);
        param.setCodigoTipoArchivo(codigoTipoArchivo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoDocumentoProceso> find(TipoDocumentoProcesoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoDocumentoProceso.class);
        Root<TipoDocumentoProceso> root = cq.from(TipoDocumentoProceso.class);
        Join<TipoDocumentoProceso, TipoArchivo> tipoArchivo = root.join("tipoArchivo");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getIdTipoArchivo() != null) {
            predList.add(qb.equal(tipoArchivo.get("id"), param.getIdTipoArchivo()));
        }
        
        if (param.getCodigoTipoArchivo() != null && !param.getCodigoTipoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(tipoArchivo.get("codigo"), param.getCodigoTipoArchivo().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<TipoDocumentoProceso> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoDocumentoProcesoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoDocumentoProceso> root = cq.from(TipoDocumentoProceso.class);
        Join<TipoDocumentoProceso, TipoArchivo> tipoArchivo = root.join("tipoArchivo");
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getIdTipoArchivo() != null) {
            predList.add(qb.equal(tipoArchivo.get("id"), param.getIdTipoArchivo()));
        }
        
        if (param.getCodigoTipoArchivo() != null && !param.getCodigoTipoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(tipoArchivo.get("codigo"), param.getCodigoTipoArchivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
