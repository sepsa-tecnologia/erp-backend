/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCuota;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCuotaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaCuotaPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaCuotaFacade", mappedName = "FacturaCuotaFacade")
@Local(FacturaCuotaFacade.class)
public class FacturaCuotaFacadeImpl extends FacadeImpl<FacturaCuota, FacturaCuotaParam> implements FacturaCuotaFacade {

    public FacturaCuotaFacadeImpl() {
        super(FacturaCuota.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdFactura validar identificador de factura
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(FacturaCuotaParam param, boolean validarIdFactura) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        FacturaParam param1 = new FacturaParam();
        param1.setId(param.getIdFactura());
        Long size = facades.getFacturaFacade().findSize(param1);
        
        if(validarIdFactura && size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }
            
        Moneda moneda = facades.getMonedaFacade()
                .find(param.getIdMoneda(), param.getCodigoMoneda());

        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        } else {
            param.setIdMoneda(moneda.getId());
            param.setCodigoMoneda(moneda.getCodigo());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public FacturaCuota create(FacturaCuotaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        FacturaCuota item = new FacturaCuota();
        item.setIdFactura(param.getIdFactura());
        item.setIdMoneda(param.getIdMoneda());
        item.setMonto(param.getMonto());
        item.setFechaVencimiento(param.getFechaVencimiento());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<FacturaCuotaPojo> findPojo(FacturaCuotaParam param) {

        AbstractFind find = new AbstractFind(FacturaCuotaPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idFactura"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("descripcion"),
                        getPath("moneda").get("codigo"),
                        getPath("root").get("monto"),
                        getPath("root").get("fechaVencimiento"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<FacturaCuota> find(FacturaCuotaParam param) {

        AbstractFind find = new AbstractFind(FacturaCuota.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, FacturaCuotaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<FacturaCuota> root = cq.from(FacturaCuota.class);
        Join<FacturaCuota, Moneda> moneda = root.join("moneda");
        
        find.addPath("root", root);
        find.addPath("moneda", moneda);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaCuotaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaCuota> root = cq.from(FacturaCuota.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
