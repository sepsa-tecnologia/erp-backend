/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionSnc;
import py.com.sepsa.erp.ejb.entities.facturacion.SolicitudNotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.SolicitudNotaCreditoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.SolicitudNotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "SolicitudNotaCreditoDetalleFacade", mappedName = "SolicitudNotaCreditoDetalleFacade")
@Local(SolicitudNotaCreditoDetalleFacade.class)
public class SolicitudNotaCreditoDetalleFacadeImpl extends FacadeImpl<SolicitudNotaCreditoDetalle, SolicitudNotaCreditoDetalleParam> implements SolicitudNotaCreditoDetalleFacade {

    public SolicitudNotaCreditoDetalleFacadeImpl() {
        super(SolicitudNotaCreditoDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdSnc validar id nc
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(SolicitudNotaCreditoDetalleParam param, boolean validarIdSnc) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        SolicitudNotaCredito snc = facades
                .getSolicitudNotaCreditoFacade()
                .find(param.getIdSolicitudNotaCredito());
        
        if(validarIdSnc && snc == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la solicitud de nota de crédito"));
        }
        
        if(!isNull(param.getIdFactura())) {
            
            Factura factura = facades.getFacturaFacade().find(param.getIdFactura());

            if(factura == null) {

                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura"));
            }
        }
        
        if(!isNull(param.getIdFacturaCompra())) {
            
            FacturaCompra facturaCompra = facades.getFacturaCompraFacade().find(param.getIdFacturaCompra());

            if(facturaCompra == null) {

                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura de compra"));
            }
        }
        
        if(!isNull(param.getIdMotivoEmisionSnc())) {
            
            MotivoEmisionSnc motivoEmisionSnc = facades.getMotivoEmisionSncFacade().find(param.getIdMotivoEmisionSnc());

            if(motivoEmisionSnc == null) {

                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el motivo de emisión snc"));
            }
        }
            
        Producto producto = facades.getProductoFacade().find(param.getIdEmpresa(),
                param.getIdProducto(), param.getCodigoGtin());

        if(producto == null) {
            
            String msg = "";
            
            if(param.getIdProducto() != null) {
                msg = String.format("%s IdProducto:%d", msg, param.getIdProducto());
            }
            
            if(param.getCodigoGtin() != null && !param.getCodigoGtin().trim().isEmpty()) {
                msg = String.format("%s CódigoGtin:%s", msg, param.getCodigoGtin().trim());
            }
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto. " + msg));
        } else {
            param.setIdProducto(producto.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public SolicitudNotaCreditoDetalle create(SolicitudNotaCreditoDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        SolicitudNotaCreditoDetalle ncd = new SolicitudNotaCreditoDetalle();
        ncd.setIdSolicitudNotaCredito(param.getIdSolicitudNotaCredito());
        ncd.setNroLinea(param.getNroLinea());
        ncd.setIdFactura(param.getIdFactura());
        ncd.setIdFacturaCompra(param.getIdFacturaCompra());
        ncd.setDescripcion(param.getDescripcion());
        ncd.setPorcentajeIva(param.getPorcentajeIva());
        ncd.setCantidad(param.getCantidad());
        ncd.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva().stripTrailingZeros());
        ncd.setPrecioUnitarioSinIva(param.getPrecioUnitarioSinIva().stripTrailingZeros());
        ncd.setMontoIva(param.getMontoIva().stripTrailingZeros());
        ncd.setMontoImponible(param.getMontoImponible().stripTrailingZeros());
        ncd.setMontoTotal(param.getMontoTotal().stripTrailingZeros());
        ncd.setIdProducto(param.getIdProducto());
        ncd.setIdMotivoEmisionSnc(param.getIdMotivoEmisionSnc());
        ncd.setObservacion(param.getObservacion());
        
        create(ncd);
        
        return ncd;
    }
    
    @Override
    public List<SolicitudNotaCreditoDetalle> find(SolicitudNotaCreditoDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(SolicitudNotaCreditoDetalle.class);
        Root<SolicitudNotaCreditoDetalle> root = cq.from(SolicitudNotaCreditoDetalle.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdSolicitudNotaCredito() != null) {
            predList.add(qb.equal(root.get("idSolicitudNotaCredito"), param.getIdSolicitudNotaCredito()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdMotivoEmisionSnc() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionSnc"), param.getIdMotivoEmisionSnc()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(root.get("idSolicitudNotaCredito")),
                qb.asc(root.get("nroLinea")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<SolicitudNotaCreditoDetalle> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(SolicitudNotaCreditoDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<SolicitudNotaCreditoDetalle> root = cq.from(SolicitudNotaCreditoDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdSolicitudNotaCredito() != null) {
            predList.add(qb.equal(root.get("idSolicitudNotaCredito"), param.getIdSolicitudNotaCredito()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdMotivoEmisionSnc() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionSnc"), param.getIdMotivoEmisionSnc()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();

        return result;
    }
}
