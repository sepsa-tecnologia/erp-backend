/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 * POJO para la entidad persona contacto
 * @author Jonathan
 */
public class PersonaContactoPojo {

    public PersonaContactoPojo(Integer idPersona, String persona,
            Integer idContacto, String contacto, Integer idTipoContacto,
            String tipoContacto, Integer idCargo, String cargo,
            Character estado) {
        this.idPersona = idPersona;
        this.idContacto = idContacto;
        this.persona = persona;
        this.contacto = contacto;
        this.idTipoContacto = idTipoContacto;
        this.tipoContacto = tipoContacto;
        this.idCargo = idCargo;
        this.cargo = cargo;
        this.estado = estado;
    }
    
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    
    /**
     * Persona
     */
    private String persona;
    
    /**
     * Contacto
     */
    private String contacto;
    
    /**
     * Identificador de tipo de contacto
     */
    private Integer idTipoContacto;
    
    /**
     * Tipo contacto
     */
    private String tipoContacto;
    
    /**
     * Identificador de cargo
     */
    private Integer idCargo;
    
    /**
     * Cargo
     */
    private String cargo;
    
    /**
     * Estado
     */
    private Character estado;

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getEstado() {
        return estado;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}
