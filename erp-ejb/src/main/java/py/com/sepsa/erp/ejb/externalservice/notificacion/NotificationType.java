
package py.com.sepsa.erp.ejb.externalservice.notificacion;

/**
 * POJO para la entidad tipo notificación
 * @author Daniel F. Escauriza Arza
 */
public class NotificationType {

    /**
     * Identificador de la entidad tipo notificación
     */
    private Integer id;
    
    /**
     * Código del tipo de notificación
     */
    private String code;
    
    /**
     * Descripción del tipo de notificación
     */
    private String description;
    
    /**
     * Frecuencia de ejecución
     */
    private Integer rate;
    
    /**
     * Código de proceso
     */
    private String processCode;

    /**
     * Obtiene el identificador de la entidad tipo notificación
     * @return Identificador de la entidad tipo notificación
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador de la entidad tipo notificación
     * @param id Identificador de la entidad tipo notificación
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene el código del tipo de notificación
     * @return Código del tipo de notificación
     */
    public String getCode() {
        return code;
    }

    /**
     * Setea el código del tipo de notificación
     * @param code Código del tipo de notificación
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtiene la descripción del tipo de notificación
     * @return Descripción del tipo de notificación
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción del tipo de notificación
     * @param description Descripción del tipo de notificación
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Obtiene la frecuencia de ejecución
     * @return Frecuencia de ejecución
     */
    public Integer getRate() {
        return rate;
    }

    /**
     * Setea la frecuencia de ejecución
     * @param rate Frecuencia de ejecución
     */
    public void setRate(Integer rate) {
        this.rate = rate;
    }

    /**
     * Obtiene el código del proceso
     * @return Código del proceso
     */
    public String getProcessCode() {
        return processCode;
    }

    /**
     * Setea el código del proceso
     * @param processCode Código del proceso
     */
    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }
    
    /**
     * Constructor de NotificationType
     * @param id Identificador de la entidad tipo notificación
     * @param code Código del tipo de notificación
     * @param description Descripción del tipo de notificación
     * @param rate Frecuencia de ejecución
     * @param processCode Código del proceso
     */
    public NotificationType(Integer id, String code, String description, 
            Integer rate, String processCode) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.rate = rate;
        this.processCode = processCode;
    }
    
    /**
     * Constructor de NotificationType
     * @param id Identificador de la entidad tipo notificación
     * @param code Código del tipo de notificación
     * @param description Descripción del tipo de notificación
     * @param rate Frecuencia de ejecución
     */
    public NotificationType(Integer id, String code, String description, 
            Integer rate) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.rate = rate;
    }

    /**
     * Constructor de NotificationType
     * @param id Identificador de la entidad tipo notificación
     */
    public NotificationType(Integer id) {
        this.id = id;
    }
    
    /**
     * Constructor de NotificationType
     */
    public NotificationType() {}
    
}
