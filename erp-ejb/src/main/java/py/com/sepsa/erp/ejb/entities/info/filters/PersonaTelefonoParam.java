/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contacto telefono
 *
 * @author Jonathan
 */
public class PersonaTelefonoParam extends CommonParam {
    
    public PersonaTelefonoParam() {
    }

    public PersonaTelefonoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Identificador de persona contacto
     */
    @QueryParam("idPersonaContacto")
    private Integer idPersonaContacto;

    /**
     * Identificador de cargo
     */
    @QueryParam("idCargo")
    private Integer idCargo;

    /**
     * Identificador de tipo de contacto
     */
    @QueryParam("idTipoContacto")
    private Integer idTipoContacto;

    /**
     * Identificador de contacto
     */
    @QueryParam("idContacto")
    private Integer idContacto;

    /**
     * Identificador de telefono
     */
    @QueryParam("idTelefono")
    private Integer idTelefono;

    /**
     * Contacto
     */
    @QueryParam("contacto")
    private String contacto;

    /**
     * Prefijo
     */
    @QueryParam("prefijo")
    private String prefijo;

    /**
     * Numero
     */
    @QueryParam("numero")
    private String numero;

    /**
     * Estado contacto
     */
    @QueryParam("contactoActivo")
    private Character contactoActivo;

    private TelefonoParam telefono;
    
    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona"));
        }

        if (isNull(idTelefono) && isNull(telefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el telefono"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si esta activo"));
        }

        if(!isNull(telefono)) {
            telefono.setIdEmpresa(idEmpresa);
            telefono.isValidToCreate();
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona"));
        }

        if (isNull(idTelefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de telefono"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si esta activo"));
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(telefono)) {
            list.addAll(telefono.getErrores());
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public void setContactoActivo(Character contactoActivo) {
        this.contactoActivo = contactoActivo;
    }

    public Character getContactoActivo() {
        return contactoActivo;
    }

    public void setTelefono(TelefonoParam telefono) {
        this.telefono = telefono;
    }

    public TelefonoParam getTelefono() {
        return telefono;
    }

    public void setIdPersonaContacto(Integer idPersonaContacto) {
        this.idPersonaContacto = idPersonaContacto;
    }

    public Integer getIdPersonaContacto() {
        return idPersonaContacto;
    }
}
