/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.set.NotaRemisionProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.NotaRemisionProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface NotaRemisionProcesamientoFacade extends Facade<NotaRemisionProcesamiento, NotaRemisionProcesamientoParam, UserInfoImpl> {
    
}
