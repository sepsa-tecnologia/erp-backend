/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.input.BOMInputStream;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.NaturalezaClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmision;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.MotivoEmisionInternoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoCambioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TipoCambioPojo;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Antonella Lucero
 */
public class NotaCreditoUtils {

    public static List<NotaCreditoParam> convertToList(Facades facades, NotaCreditoParam param) {
        List<NotaCreditoParam> lista = new ArrayList<>();
        try {

            List<NotaCreditoParam> ncl = new ArrayList<>();
            
            InputStreamReader input = new InputStreamReader(new BOMInputStream(new ByteArrayInputStream(param.getUploadedFileBytes()), false),StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                Pattern pattern = Pattern.compile(";|,");
                Matcher mather = pattern.matcher(line);
                if (mather.find() == true) {
                    lineas.add(line);
                }
            }
            for (int i = 0; i < lineas.size(); i++) {
                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    NotaCreditoParam nInicial = new NotaCreditoParam();
                    int errores = 0;
                    String[] stringArr = lineas.get(i).split(";|,");
                    String tipoLinea = stringArr.length <= 0 ? "" : stringArr[0];
                    String timb = stringArr.length <= 1 ? "" : stringArr[1];
                    String nroN = stringArr.length <= 2 ? "" : stringArr[2];
                    if (timb != null && !timb.trim().isEmpty()) {
                        nInicial.setTimbrado(timb);
                    } else {
                        lista.add(nInicial);
                        nInicial.setTimbrado("-");
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de timbrado de la nota de crédito"));
                        errores += 1;
                    }
                    if (nroN != null && !nroN.trim().isEmpty()) {
                        nInicial.setNroNotaCredito(nroN);
                    } else {
                        nInicial.setNroNotaCredito("-");
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de la nota de crédito"));
                        errores += 1;
                    }
                    if (!(tipoLinea != null && !tipoLinea.trim().isEmpty())) {
                        if (!lista.contains(nInicial)) {
                            lista.add(nInicial);
                        }
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar un Tipo de Línea"));
                        errores += 1;
                        continue;
                    } else {
                        if (errores > 0) {
                            continue;
                        }
                        //Datos de Cabecera
                        String timbrado = null;
                        String nroNCredito = null;
                        String fechaNc = null;
                        String codMoneda = null;
                        String nroDocumento = null;
                        String razonSocial = null;
                        String codSeguridad = null;
                        String codMotivoEmision = null;
                        String tipoCambio = null;
                        //Datos de Detalle
                        String timbradoFactura = null;
                        String nroFactura = null;
                        String fechaFactura = null;
                        String cdcFactura = null;
                        String codProducto = null;
                        String porcentajeImpuesto = null;
                        String porcentajeGrav = null;
                        String precioUnitarioConIva = null;
                        String descuentoUnitario = null;
                        String cantidad = null;
                        String descripcion = null;
                        String datoAdicional = null;
                        String email = null;
                        NotaCreditoParam nc = new NotaCreditoParam();
                        NotaCreditoDetalleParam ncd = new NotaCreditoDetalleParam();
                        switch (tipoLinea) {
                            case "1":
                                //TODO cabecera
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroNCredito = (String) stringArr[k];
                                            break;
                                        case 3:
                                            fechaNc = (String) stringArr[k];
                                            break;
                                        case 4:
                                            codMoneda = (String) stringArr[k];
                                            break;
                                        case 5:
                                            nroDocumento = (String) stringArr[k];
                                            break;
                                        case 6:
                                            razonSocial = (String) stringArr[k];
                                            break;
                                        case 7:
                                            codSeguridad = (String) stringArr[k];
                                            break;
                                        case 8:
                                            codMotivoEmision = (String) stringArr[k];
                                            break;
                                        case 9:
                                            tipoCambio = (String) stringArr[k];
                                            break;
                                        case 10:
                                            email = (String) stringArr[k];
                                            
                                    }
                                }
                                nc.setArchivoSet('S');
                                nc.setIdEmpresa(param.getIdEmpresa());
                                nc.setIdUsuario(param.getIdUsuario());
                                nc.setIdEncargado(param.getIdUsuario());
                                nc.setTimbrado(timbrado);
                                nc.setSgteNumeroNC(Boolean.FALSE);
                                nc.setNroNotaCredito(nroNCredito);
                                if (nc.getNroNotaCredito() != null) {
                                    if (nc.getNroNotaCredito().length() != 15 || !(nc.getNroNotaCredito().matches("^[0-9]{3}[\\-][0-9]{3}[\\-][0-9]{7}$"))) {
                                        lista.add(nc);
                                        nc.addError(MensajePojo.createInstance().descripcion("El formato del nro de nota de crédito de la cabecera es inválido"));
                                        continue;
                                    }
                                }
                                if (fechaNc != null && !fechaNc.trim().isEmpty()) {
                                    nc.setFecha(new SimpleDateFormat("dd/MM/yyyy").parse(fechaNc));
                                }
                                if (codMoneda != null && !codMoneda.trim().isEmpty()) {
                                    Moneda moneda = facades.getMonedaFacade().find(nc.getIdMoneda(), codMoneda);
                                    if (moneda == null) {
                                        lista.add(nc);
                                        nc.addError(MensajePojo.createInstance().descripcion("No existe la moneda"));
                                        continue;
                                    } else {
                                        nc.setIdMoneda(moneda.getId());
                                        nc.setCodigoMoneda(moneda.getCodigo());
                                        if (nc.getCodigoMoneda().equalsIgnoreCase("PYG")) {
                                            nc.setIdTipoCambio(null);
                                        } else {
                                            if (tipoCambio != null && !tipoCambio.trim().isEmpty()) {
                                                TipoCambioParam cambio = new TipoCambioParam();
                                                cambio.setCodigoMoneda(nc.getCodigoMoneda());
                                                try {
                                                    cambio.setCompra(new BigDecimal(tipoCambio.trim().replace(" ", "")));
                                                } catch (NumberFormatException e) {
                                                    String camb = tipoCambio.replace("\"", "").trim().replace(" ", "");

                                                    DecimalFormat decimalFormat = new DecimalFormat();
                                                    decimalFormat.setParseBigDecimal(true);
                                                    Number parsedCambio = decimalFormat.parse(camb);
                                                    BigDecimal bdCambio = (BigDecimal) parsedCambio;

                                                    cambio.setCompra(bdCambio);
                                                    cambio.setVenta(bdCambio);
                                                }
                                                cambio.setActivo('S');
                                                TipoCambioPojo tipoCambioM = facades.getTipoCambioFacade().findFirstPojo(cambio);
                                                if (tipoCambioM != null) {
                                                    nc.setIdTipoCambio(tipoCambioM.getId());
                                                } else {
                                                    lista.add(nc);
                                                    nc.addError(MensajePojo.createInstance().descripcion("Tipo de Cambio Inexistente"));
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                if (email != null && !email.trim().isEmpty()) {
                                    int j = 0;
                                    List<NotaCreditoNotificacionParam> listEmail = new ArrayList();
                                    for (StringTokenizer stringTokenizer = new StringTokenizer(email, "/ ,;"); stringTokenizer.hasMoreTokens(); j++) {
                                        String token = stringTokenizer.nextToken();
                                        NotaCreditoNotificacionParam ncnp = new NotaCreditoNotificacionParam();
                                        ncnp.setEmail(token);
                                        ncnp.setCodigoTipoNotificacion("APROBACION_DTE");
                                        listEmail.add(ncnp);
                                    }
                                    nc.setNotaCreditoNotificaciones(listEmail);
                                }
                                nc.setRuc(nroDocumento.replace(".", ""));
                                nc.setRazonSocial(razonSocial);
                                if (nc.getRazonSocial() != null && !nc.getRazonSocial().trim().isEmpty() && nc.getRuc() != null && !nc.getRuc().trim().isEmpty()) {
                                    if (nc.getRuc().contains("-")) {
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if (n != null) {
                                            nc.setIdNaturalezaCliente(n.getId());
                                        }
                                    } else {
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("NO_CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if (n != null) {
                                            nc.setIdNaturalezaCliente(n.getId());
                                        }
                                    }
                                    ClienteParam paramc = new ClienteParam();
                                    paramc.setIdEmpresa(nc.getIdEmpresa());
                                    paramc.setNroDocumentoEq(nc.getRuc());
                                    paramc.setRazonSocial(nc.getRazonSocial());
                                    Cliente cliente = facades.getClienteFacade().findFirst(paramc);
                                    if (cliente != null) {
                                        nc.setIdCliente(cliente.getIdCliente());
                                        nc.setIdNaturalezaCliente(cliente.getIdNaturalezaCliente());
                                    }
                                }
                                nc.setCodSeguridad(codSeguridad);
                                if (codMotivoEmision != null && !codMotivoEmision.trim().isEmpty()) {
                                    MotivoEmision motivoEmision = facades.getMotivoEmisionFacade().find(nc.getIdMotivoEmision(), codMotivoEmision);
                                    if (motivoEmision != null) {
                                        nc.setIdMotivoEmision(motivoEmision.getId());
                                        MotivoEmisionInternoParam motivIntern = new MotivoEmisionInternoParam();
                                        motivIntern.setActivo('S');
                                        motivIntern.setIdMotivoEmision(nc.getIdMotivoEmision());
                                        motivIntern.setIdEmpresa(nc.getIdEmpresa());
                                        MotivoEmisionInterno motivoEmisionInterno = facades.getMotivoEmisionInternoFacade().findFirst(motivIntern);
                                        if (motivoEmisionInterno != null) {
                                            nc.setIdMotivoEmisionInterno(motivoEmisionInterno.getId());
                                            nc.setCodigoMotivoEmisionInterno(motivoEmisionInterno.getCodigo());
                                        }
                                    }
                                }

                                UsuarioParam uparam = new UsuarioParam();
                                uparam.setId(param.getIdUsuario());
                                UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);
                                nc.setIdUsuario(usuario.getId());

                                TalonarioPojo talonario = null;
                                if (nc.getNroNotaCredito() != null && !nc.getNroNotaCredito().trim().isEmpty() && nc.getTimbrado() != null && !nc.getTimbrado().trim().isEmpty()) {
                                    TalonarioParam param2 = new TalonarioParam();
                                    param2.setIdEmpresa(nc.getIdEmpresa());
                                    param2.setTimbrado(nc.getTimbrado());
                                    param2.setActivo('S');
                                    param2.setNroSucursal(nc.getNroNotaCredito().substring(0, 3));
                                    param2.setNroPuntoVenta(nc.getNroNotaCredito().substring(4, 7));
                                    param2.setIdTipoDocumento(3);
                                    talonario = facades.getTalonarioFacade().findFirstPojo(param2);
                                    if (talonario != null) {
                                        nc.setIdTalonario(talonario.getId());
                                        nc.setDigital(talonario.getDigital());
                                    } else {
                                        lista.add(nc);
                                        nc.addError(MensajePojo.createInstance().descripcion("Nro de timbrado, nro de sucursal y nro de punto de venta inexistentes"));
                                        continue;
                                    }
                                }
                                ncl.add(nc);
                                break;
                            case "2":
                                //TODO detalle
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroNCredito = (String) stringArr[k];
                                            break;
                                        case 3:
                                            timbradoFactura = (String) stringArr[k];
                                            break;
                                        case 4:
                                            nroFactura = (String) stringArr[k];
                                            break;
                                        case 5:
                                            fechaFactura = (String) stringArr[k];
                                            break;
                                        case 6:
                                            cdcFactura = (String) stringArr[k];
                                            break;
                                        case 7:
                                            codProducto = (String) stringArr[k];
                                            break;
                                        case 8:
                                            porcentajeImpuesto = (String) stringArr[k];
                                            break;
                                        case 9:
                                            porcentajeGrav = (String) stringArr[k];
                                            break;
                                        case 10:
                                            precioUnitarioConIva = (String) stringArr[k];
                                            break;
                                        case 11:
                                            descuentoUnitario = (String) stringArr[k];
                                            break;
                                        case 12:
                                            cantidad = (String) stringArr[k];
                                            break;
                                        case 13:
                                            descripcion = (String) stringArr[k];
                                            break;
                                        case 14:
                                            datoAdicional = (String) stringArr[k];
                                            break;
                                    }
                                }
                                //guardar el detalle a la nc correspondiente   
                                boolean coincide = false;
                                for (NotaCreditoParam n : ncl) {
                                    if (n.getTimbrado() != null && !n.getTimbrado().trim().isEmpty() && timbrado != null && !timbrado.trim().isEmpty() && n.getNroNotaCredito() != null && !n.getNroNotaCredito().trim().isEmpty() && nroNCredito != null && !nroNCredito.trim().isEmpty()) {
                                        if (n.getTimbrado().equalsIgnoreCase(timbrado) && n.getNroNotaCredito().equalsIgnoreCase(nroNCredito)) {
                                            coincide = true;
                                            n.setDescripcion(descripcion);
                                            if (cdcFactura  != null && !cdcFactura.trim().isEmpty()){
                                                ncd.setFacturaDigital(n.getDigital());
                                            } else {
                                                ncd.setFacturaDigital('N');
                                            }
                                            n.getNotaCreditoDetalles().add(ncd);
                                            if (!lista.contains(n)) {
                                                lista.add(n);
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (coincide == false) {
                                    NotaCreditoParam nError = new NotaCreditoParam();
                                    if (nroNCredito != null && !nroNCredito.trim().isEmpty()) {
                                        nError.setNroNotaCredito(nroNCredito);
                                    } else {
                                        nError.setNroNotaCredito("-");
                                    }
                                    if (timbrado != null && !timbrado.trim().isEmpty()) {
                                        nError.setTimbrado(timbrado);
                                    } else {
                                        nError.setTimbrado("-");
                                    }
                                    lista.add(nError);
                                    nError.addError(MensajePojo.createInstance().descripcion("El detalle no coincide con ninguna cabecera"));
                                    continue;
                                }
                                ncd.setIdEmpresa(param.getIdEmpresa());
                                ncd.setNroLinea(2);
                                ncd.setNroNotaCredito(nroNCredito);
                                ncd.setNroFactura(nroFactura);
                                ncd.setTimbradoFactura(timbradoFactura);
                                if (fechaFactura != null && !fechaFactura.trim().isEmpty()) {
                                    ncd.setFechaFactura(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFactura));
                                }
                                ncd.setCdcFactura(cdcFactura);
                                ProductoParam prod = new ProductoParam();
                                if (codProducto != null && !codProducto.trim().isEmpty()) {
                                    prod.setCodigoInternoEq(codProducto);
                                    prod.setIdEmpresa(param.getIdEmpresa());
                                    prod.setActivo('S');
                                    ProductoPojo producto = facades.getProductoFacade().findFirstPojo(prod);
                                    ncd.setIdProducto(producto.getId());
                                    if (producto == null) {
                                        ncd.addError(MensajePojo.createInstance().descripcion("Producto no encontrado, verificar código interno"));
                                        continue;
                                    } else {
                                        ncd.setIdProducto(producto.getId());
                                    }
                                } else {
                                    ncd.addError(MensajePojo.createInstance().descripcion("Se debe indicar el código interno del producto"));
                                    continue;
                                }
                                ncd.setDescripcion(descripcion);
                                try {
                                    byte[] bytesDecodificados = Base64.getDecoder().decode(datoAdicional);
                                    String decoded = new String(bytesDecodificados);
                                    ncd.setDatoAdicional(decoded);
                                } catch (Exception e) {
                                    ncd.setDatoAdicional(datoAdicional);
                                }
                                if (precioUnitarioConIva != null && cantidad != null && descuentoUnitario != null) {
                                    if (!precioUnitarioConIva.trim().isEmpty() && !cantidad.trim().isEmpty() && !descuentoUnitario.trim().isEmpty()) {
                                        try {
                                            ncd.setPrecioUnitarioConIva(new BigDecimal(precioUnitarioConIva.trim().replace(" ", "")));
                                            ncd.setCantidad(new BigDecimal(cantidad.trim().replace(" ", "")));
                                            ncd.setMontoDescuentoParticular(new BigDecimal(descuentoUnitario.trim().replace(" ", "")));
                                        } catch (NumberFormatException e) {
                                            String precioU = precioUnitarioConIva.replace("\"", "").trim().replace(" ", "");
                                            String cantidad1 = cantidad.replace("\"", "").trim().replace(" ", "");
                                            String descuento1 = descuentoUnitario.replace("\"", "").trim().replace(" ", "");

                                            DecimalFormat decimalFormat = new DecimalFormat();
                                            decimalFormat.setParseBigDecimal(true);
                                            Number parsedPrecio = decimalFormat.parse(precioU);
                                            BigDecimal bdPrecio = (BigDecimal) parsedPrecio;

                                            Number parsedCantidad = decimalFormat.parse(cantidad1);
                                            BigDecimal bdCantidad = (BigDecimal) parsedCantidad;

                                            Number parsedDescuento = decimalFormat.parse(descuento1);
                                            BigDecimal bdDescuento = (BigDecimal) parsedDescuento;

                                            ncd.setPrecioUnitarioConIva(bdPrecio);
                                            ncd.setCantidad(bdCantidad);
                                            ncd.setMontoDescuentoParticular(bdDescuento);
                                        }
                                        ncd.setPorcentajeIva(Integer.parseInt(porcentajeImpuesto));
                                        if (ncd.getMontoDescuentoParticular() != null && (((ncd.getMontoDescuentoParticular().compareTo(BigDecimal.ZERO) == 1) || (ncd.getMontoDescuentoParticular().compareTo(BigDecimal.ZERO) == 0)) && (ncd.getMontoDescuentoParticular().compareTo(ncd.getPrecioUnitarioConIva()) == -1))) {
                                            BigDecimal aux = ncd.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                                            BigDecimal descuentoPorc = aux.divide(ncd.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                                            ncd.setDescuentoParticularUnitario(descuentoPorc);
                                        } else {
                                            ncd.setDescuentoParticularUnitario(BigDecimal.ZERO);
                                            ncd.setMontoDescuentoParticular(BigDecimal.ZERO);
                                        }
                                        BigDecimal precioUnitarioConIVA = ncd.getPrecioUnitarioConIva().subtract(ncd.getMontoDescuentoParticular());
                                        if (ncd.getPorcentajeIva() != null) {
                                            if (ncd.getPorcentajeIva() == 5) {
                                                BigDecimal iva = new BigDecimal("1.05");
                                                BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(iva, 5, RoundingMode.HALF_UP);
                                                ncd.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
                                                ncd.setMontoIva(precioUnitarioConIVA.subtract(ncd.getPrecioUnitarioSinIva()));
                                            } else if (ncd.getPorcentajeIva() == 10) {
                                                BigDecimal iva = new BigDecimal("1.1");
                                                BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(iva, 5, RoundingMode.HALF_UP);
                                                ncd.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
                                                ncd.setMontoIva(precioUnitarioConIVA.subtract(ncd.getPrecioUnitarioSinIva()));
                                            } else if (ncd.getPorcentajeIva() == 0) {
                                                ncd.setPrecioUnitarioSinIva(BigDecimal.ZERO);
                                                ncd.setMontoIva(BigDecimal.ZERO);
                                            }
                                        }
                                        if (porcentajeGrav != null) {
                                            ncd.setPorcentajeGravada(new BigDecimal(porcentajeGrav));
                                        } else {
                                            ncd.setPorcentajeGravada(new BigDecimal(100));
                                        }
                                        BigDecimal _montoTotal = ncd.getPrecioUnitarioConIva().subtract(ncd.getMontoDescuentoParticular()).multiply(ncd.getCantidad());
                                        BigDecimal _montoImponible = _montoTotal.multiply(ncd.getPorcentajeGravada()).multiply(new BigDecimal(100))
                                                .divide(new BigDecimal(10000).add(new BigDecimal(ncd.getPorcentajeIva()).multiply(ncd.getPorcentajeGravada())), 8, RoundingMode.HALF_UP);
                                        BigDecimal _montoIva = _montoImponible.multiply(new BigDecimal(ncd.getPorcentajeIva())).divide(new BigDecimal(100), 8, RoundingMode.HALF_UP);
                                        ncd.setMontoIva(_montoIva);
                                        ncd.setMontoImponible(_montoImponible);
                                        ncd.setMontoTotal(_montoTotal);

                                    }
                                }
                                break;
                            default:
                                if (!lista.contains(nInicial)) {
                                    lista.add(nInicial);
                                }
                                nInicial.addError(MensajePojo.createInstance().descripcion("Tipo de línea únicamente puede ser 1 o 2"));
                                break;

                        }
                    }
                }
            }
            boolean continuar = false;
            for (NotaCreditoParam nota : lista) {
                    for (NotaCreditoDetalleParam detalle : nota.getNotaCreditoDetalles()) {
                        if (detalle.getFacturaDigital()!='N') {
                            if (detalle.getCantidad() != null && detalle.getPrecioUnitarioConIva() != null) {
                                continuar = true;
                                FacturaParam f = new FacturaParam();
                                TalonarioParam param2 = new TalonarioParam();
                                if (detalle.getNroFactura() != null && !detalle.getNroFactura().trim().isEmpty()) {
                                    f.setNroFactura(detalle.getNroFactura());
                                    param2.setNroSucursal(f.getNroFactura().substring(0, 3));
                                    param2.setNroPuntoVenta(f.getNroFactura().substring(4, 7));
                                }
                                if (detalle.getFacturaDigital() != null) {
                                    f.setDigital(detalle.getFacturaDigital());
                                    param2.setDigital(f.getDigital());
                                }
                                if (detalle.getTimbradoFactura() != null && !detalle.getTimbradoFactura().trim().isEmpty()) {
                                    f.setTimbrado(detalle.getTimbradoFactura());
                                    param2.setTimbrado(f.getTimbrado());
                                }
                                param2.setIdEmpresa(nota.getIdEmpresa());
                                param2.setActivo('S');
                                param2.setIdTipoDocumento(1);
                                TalonarioPojo talonariof = facades.getTalonarioFacade().findFirstPojo(param2);
                                if (talonariof == null) {
                                    detalle.addError(MensajePojo.createInstance().descripcion("Nro de timbrado, nro de sucursal y nro de punto de venta inexistentes de la factura"));
                                    continuar = false;
                                } else {
                                    f.setIdTalonario(talonariof.getId());
                                    f.setIdEmpresa(nota.getIdEmpresa());
                                    FacturaPojo fact = facades.getFacturaFacade().findFirstPojo(f);
                                    if (fact != null) {
                                        detalle.setIdFactura(fact.getId());
                                    } else {
                                        detalle.addError(MensajePojo.createInstance().descripcion("La factura no existe"));
                                        continuar = false;
                                    }
                                }
                            } else {
                                continuar = false;
                            }
                        } else {
                            continuar = true;
                        }
                    }
                if (continuar == true) {
                    try {
                        calcularDetalle(nota, nota.getNotaCreditoDetalles());
                    }catch (Exception ex)  {
                        nota.addError(MensajePojo.createInstance().descripcion("Error al calcular los montos, verificar la cantidad y precio unitario de la nota de crédito"));
                        continue;
                    }
                }
            }

            return lista;
        } catch (Exception ex) {
            return lista;
        }

    }

    private static void calcularDetalle(NotaCreditoParam nc, List<NotaCreditoDetalleParam> listaDetalle) {
        BigDecimal totalDetalle = new BigDecimal("0");
        int i = 0;

        for (NotaCreditoDetalleParam info : listaDetalle) {
            i++;

            info.setNroLinea(i);
            info.setMontoIva(info.getMontoIva());
            info.setMontoImponible(info.getMontoImponible());
            info.setMontoTotal(info.getMontoTotal());
            info.setPrecioUnitarioSinIva(info.getPrecioUnitarioSinIva());

            totalDetalle = totalDetalle.add(info.getMontoTotal());
        }
        nc.setMontoTotalNotaCredito(totalDetalle);
        nc.setMontoTotalGuaranies(totalDetalle);
        nc.setNotaCreditoDetalles(listaDetalle);
        calcularTotalesGenerales(nc, nc.getNotaCreditoDetalles());

    }

    private static void calcularTotalesGenerales(NotaCreditoParam nc, List<NotaCreditoDetalleParam> listaDetalle) {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        nc.setMontoIva5(montoIva5Acumulador);
        nc.setMontoImponible5(montoImponible5Acumulador);
        nc.setMontoTotal5(montoTotal5Acumulador);
        nc.setMontoIva10(montoIva10Acumulador);
        nc.setMontoImponible10(montoImponible10Acumulador);
        nc.setMontoTotal10(montoTotal10Acumulador);
        nc.setMontoTotalExento(montoExcentoAcumulador);
        nc.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        nc.setMontoIvaTotal(montoIvaTotalAcumulador);
        nc.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
    }
}
