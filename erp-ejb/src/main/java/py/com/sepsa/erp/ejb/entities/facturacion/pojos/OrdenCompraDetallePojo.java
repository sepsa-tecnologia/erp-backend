/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class OrdenCompraDetallePojo {

    public OrdenCompraDetallePojo() {
    }

    public OrdenCompraDetallePojo(Integer id, Integer idOrdenCompra, Integer idProducto, Integer cantidadSolicitada, Integer cantidadConfirmada) {
        this.id = id;
        this.idOrdenCompra = idOrdenCompra;
        this.idProducto = idProducto;
        this.cantidadSolicitada = cantidadSolicitada;
        this.cantidadConfirmada = cantidadConfirmada;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de orden de compra
     */
    private Integer idOrdenCompra;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código GTIN
     */
    private String codigoGtin;
    
    /**
     * Código interno
     */
    private String codigoInterno;
    
    /**
     * Cantidad solicitada
     */
    private Integer cantidadSolicitada;
    
    /**
     * Cantidad confirmada
     */
    private Integer cantidadConfirmada;
    
    /**
     * Cantidad disponible
     */
    private Number cantidadDisponible;
    
    /**
     * Porcentaje de iva
     */
    private Integer porcentajeIva;
    
    /**
     * Precio unitario con IVA
     */
    private BigDecimal precioUnitarioConIva;
    
    /**
     * Precio unitario sin IVA
     */
    private BigDecimal precioUnitarioSinIva;
    
    /**
     * Monto IVA
     */
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Observacion
     */
    private String observacion;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Identificador de inventario
     */
    private Integer idInventario;
    
    /**
     * Identificador de inventario detalle
     */
    private Integer idInventarioDetalle;
    
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
    
    /**
     * Identificador de estado de inventario
     */
    private Integer idEstadoInventario;
    
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;

    public OrdenCompraDetalle getOrdenCompraDetalle() {
        OrdenCompraDetalle result = new OrdenCompraDetalle();
        result.setCantidadConfirmada(cantidadConfirmada);
        result.setCantidadSolicitada(cantidadSolicitada);
        result.setId(id);
        result.setIdOrdenCompra(idOrdenCompra);
        result.setIdProducto(idProducto);
        result.setMontoIva(montoIva);
        result.setMontoTotal(montoTotal);
        result.setObservacion(observacion);
        result.setPorcentajeIva(porcentajeIva);
        result.setPrecioUnitarioConIva(precioUnitarioConIva);
        result.setPrecioUnitarioSinIva(precioUnitarioSinIva);
        return result;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(Integer cantidadSolicitada) {
        this.cantidadSolicitada = cantidadSolicitada;
    }

    public Integer getCantidadConfirmada() {
        return cantidadConfirmada;
    }

    public void setCantidadConfirmada(Integer cantidadConfirmada) {
        this.cantidadConfirmada = cantidadConfirmada;
    }

    public Number getCantidadDisponible() {
        return cantidadDisponible;
    }

    public void setCantidadDisponible(Number cantidadDisponible) {
        this.cantidadDisponible = cantidadDisponible;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdInventarioDetalle(Integer idInventarioDetalle) {
        this.idInventarioDetalle = idInventarioDetalle;
    }

    public Integer getIdInventarioDetalle() {
        return idInventarioDetalle;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }
}
