/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraDetallePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraPojo;
import py.com.sepsa.erp.ejb.entities.inventario.Devolucion;
import py.com.sepsa.erp.ejb.entities.inventario.DevolucionDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventario;
import py.com.sepsa.erp.ejb.entities.inventario.OrdenCompraDetalleInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OrdenCompraDetalleInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.OrdenCompraDetalleInventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
@Log4j2
public class InventarioUtils implements Serializable {

    @EJB
    private Facades facades;

    public void anularOrdenCompra(UserInfoImpl userInfo, OrdenCompraPojo newOc) {

        String codigoTipoOperacion = "AJUSTE_ORDEN_COMPRA";
        OperacionInventario oi = facades.getOperacionInventarioFacade().create(
                userInfo, codigoTipoOperacion, "AJUSTE_ORDEN_COMPRA",
                "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());

        OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
        ocdidparam.setIdOrdenCompra(newOc.getId());
        ocdidparam.setFirstResult(0);
        ocdidparam.setPageSize(1000);
        List<OrdenCompraDetalleInventarioDetallePojo> list = facades.getOrdenCompraDetalleInventarioDetalleFacade().findPojo(ocdidparam);

        for (OrdenCompraDetalleInventarioDetallePojo ocdid : list) {

            if (ocdid.getCantidad() <= 0) {
                continue;
            }

            facades.getOperacionInventarioDetalleFacade().create(userInfo, oi.getId(),
                    codigoTipoOperacion, ocdid.getIdProducto(), ocdid.getCantidad(), ocdid.getFechaVencimiento(),
                    ocdid.getIdDepositoLogistico(), null, ocdid.getCodigoEstadoInventario(),
                    ocdid.getIdDepositoLogistico(), null, "DISPONIBLE");

            OrdenCompraDetalleInventarioDetalle item = new OrdenCompraDetalleInventarioDetalle();
            item.setId(ocdid.getId());
            item.setIdInventarioDetalle(ocdid.getIdInventarioDetalle());
            item.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());
            item.setCantidad(0);
            facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(item);
        }
    }

    public void actualizarDevolucion(UserInfoImpl userInfo,
            Devolucion newD, List<DevolucionDetalle> detalles) {

        if (newD.getAnulado().equals('S')) {
            return;
        }

        String codigoTipoOperacion = "ENTRADA";
        OperacionInventario oi = facades.getOperacionInventarioFacade().create(
                userInfo, codigoTipoOperacion, "RECEPCION_REVOLUCION",
                "CONFIRMADO", null, newD.getIdEmpresa());

        for (DevolucionDetalle detalle : detalles) {
            facades.getOperacionInventarioDetalleFacade().create(userInfo, oi.getId(),
                    codigoTipoOperacion, detalle.getIdProducto(),
                    detalle.getCantidadDevolucion(), detalle.getFechaVencimiento(), null, null, null,
                    detalle.getIdDepositoLogistico(), detalle.getIdEstadoInventario(), null);
        }
    }

    public void facturarOrdenCompra(UserInfoImpl userInfo, OrdenCompraPojo newOc) {

        String codigoTipoOperacion = "AJUSTE_ORDEN_COMPRA";
        OperacionInventario oi = facades.getOperacionInventarioFacade().create(
                userInfo, codigoTipoOperacion, "AJUSTE_ORDEN_COMPRA",
                "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());

        OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
        ocdidparam.setIdOrdenCompra(newOc.getId());
        ocdidparam.setFirstResult(0);
        ocdidparam.setPageSize(1000);
        List<OrdenCompraDetalleInventarioDetallePojo> list = facades.getOrdenCompraDetalleInventarioDetalleFacade().findPojo(ocdidparam);

        for (OrdenCompraDetalleInventarioDetallePojo ocdid : list) {

            if (ocdid.getCantidad() <= 0) {
                continue;
            }

            facades.getOperacionInventarioDetalleFacade().create(userInfo, oi.getId(),
                    codigoTipoOperacion, ocdid.getIdProducto(),
                    ocdid.getCantidad(), ocdid.getFechaVencimiento(),
                    ocdid.getIdDepositoLogistico(), null, ocdid.getCodigoEstadoInventario(),
                    ocdid.getIdDepositoLogistico(), null, "FACTURADO");

            OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
            ocdidItem.setId(ocdid.getId());
            ocdidItem.setIdInventarioDetalle(ocdid.getIdInventarioDetalle());
            ocdidItem.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());
            ocdidItem.setCantidad(0);
            facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);

            ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
            ocdidparam.setFechaVencimiento(ocdid.getFechaVencimiento());
            ocdidparam.setIdProducto(ocdid.getIdProducto());
            ocdidparam.setIdDepositoLogistico(ocdid.getIdDepositoLogistico());
            ocdidparam.setCodigoEstadoInventario("FACTURADO");
            ocdidparam.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());

            OrdenCompraDetalleInventarioDetallePojo item = facades.getOrdenCompraDetalleInventarioDetalleFacade().findFirstPojo(ocdidparam);
            if (item != null) {
                ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                ocdidItem.setId(item.getId());
                ocdidItem.setIdInventarioDetalle(item.getIdInventarioDetalle());
                ocdidItem.setIdOrdenCompraDetalle(item.getIdOrdenCompraDetalle());
                ocdidItem.setCantidad(item.getCantidad() + ocdid.getCantidad());
                facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);
            } else {
                ocdidparam.setCantidad(ocdid.getCantidad());
                facades.getOrdenCompraDetalleInventarioDetalleFacade().create(ocdidparam, userInfo);
            }
        }
    }

    public void actualizarOrdenCompra(UserInfoImpl userInfo,
            OrdenCompraPojo oldOc, OrdenCompraPojo newOc,
            Map<Integer, OrdenCompraDetallePojo> oldMap,
            Map<Integer, OrdenCompraDetalle> newMap) {

        if (newOc.getRecibido().equals('S')
                && (oldOc == null || oldOc.getCodigoEstado().equals("RECIBIDO"))
                && (newOc.getCodigoEstado().equals("PENDIENTE_PREPARACION")
                || newOc.getCodigoEstado().equals("PENDIENTE_FACTURACION")
                || newOc.getCodigoEstado().equals("FACTURADO"))) {

            String codigoTipoOperacion = "AJUSTE_ORDEN_COMPRA";
            OperacionInventario oi = facades.getOperacionInventarioFacade().create(
                    userInfo, codigoTipoOperacion, "AJUSTE_ORDEN_COMPRA",
                    "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());

            String codigoEstadoInventario;

            if (newOc.getCodigoEstado().equals("FACTURADO")) {
                codigoEstadoInventario = "FACTURADO";
            } else {
                codigoEstadoInventario = "COMPROMETIDO";
            }

            for (OrdenCompraDetalle newOcd : newMap.values()) {

                OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
                ocdparam.setIdEmpresa(newOc.getIdEmpresa());
                ocdparam.setIdOrdenCompra(newOcd.getIdOrdenCompra());
                ocdparam.setId(newOcd.getId());
                List<OrdenCompraDetallePojo> list = facades.getOrdenCompraDetalleFacade().getDisponibleDetalle(ocdparam);

                Integer var1 = newOcd.getCantidadConfirmada();

                for (OrdenCompraDetallePojo ocd : list) {

                    if (var1 <= 0) {
                        continue;
                    }

                    Integer var2;

                    if (ocd.getCantidadDisponible().intValue() > var1) {
                        var2 = var1;
                    } else {
                        var2 = ocd.getCantidadDisponible().intValue();
                    }

                    facades.getOperacionInventarioDetalleFacade().create(userInfo,
                            oi.getId(), codigoTipoOperacion, ocd.getIdProducto(), var2, ocd.getFechaVencimiento(),
                            ocd.getIdDepositoLogistico(), null, "DISPONIBLE",
                            ocd.getIdDepositoLogistico(), null, codigoEstadoInventario);

                    OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
                    ocdidparam.setIdProducto(ocd.getIdProducto());
                    ocdidparam.setIdDepositoLogistico(ocd.getIdDepositoLogistico());
                    ocdidparam.setCodigoEstadoInventario(codigoEstadoInventario);
                    ocdidparam.setFechaVencimiento(ocd.getFechaVencimiento());
                    ocdidparam.setIdOrdenCompraDetalle(newOcd.getId());

                    OrdenCompraDetalleInventarioDetallePojo item = facades.getOrdenCompraDetalleInventarioDetalleFacade().findFirstPojo(ocdidparam);
                    if (item != null) {
                        OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                        ocdidItem.setId(item.getId());
                        ocdidItem.setIdInventarioDetalle(item.getIdInventarioDetalle());
                        ocdidItem.setIdOrdenCompraDetalle(item.getIdOrdenCompraDetalle());
                        ocdidItem.setCantidad(item.getCantidad() + var2);
                        facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);
                    } else {
                        ocdidparam.setCantidad(var2);
                        facades.getOrdenCompraDetalleInventarioDetalleFacade().create(ocdidparam, userInfo);
                    }

                    var1 = var1 - var2;
                }

                if (var1 > 0) {
                    throw new RuntimeException("No existe inventario suficiente");
                }
            }
        }

        if (newOc.getRecibido().equals('S') && oldOc != null
                && oldOc.getCodigoEstado().equals("PENDIENTE_PREPARACION")
                && newOc.getCodigoEstado().equals("PENDIENTE_FACTURACION")) {

            String codigoTipoOperacion = "AJUSTE_ORDEN_COMPRA";
            OperacionInventario oi = facades.getOperacionInventarioFacade().create(
                    userInfo, codigoTipoOperacion, "AJUSTE_ORDEN_COMPRA",
                    "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());

            for (OrdenCompraDetalle newOcd : newMap.values()) {

                OrdenCompraDetallePojo oldOcd = oldMap.get(newOcd.getId());

                if (oldOcd != null && newOcd.getCantidadConfirmada() < oldOcd.getCantidadConfirmada()) {
                    OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
                    ocdidparam.setIdOrdenCompra(newOcd.getIdOrdenCompra());
                    ocdidparam.setIdOrdenCompraDetalle(newOcd.getId());
                    ocdidparam.setFirstResult(0);
                    ocdidparam.setPageSize(1000);
                    List<OrdenCompraDetalleInventarioDetallePojo> list = facades.getOrdenCompraDetalleInventarioDetalleFacade().findPojo(ocdidparam);

                    Integer var1 = oldOcd.getCantidadConfirmada() - newOcd.getCantidadConfirmada();

                    for (OrdenCompraDetalleInventarioDetallePojo ocdid : list) {

                        if (var1 <= 0) {
                            continue;
                        }

                        Integer var2;

                        if (ocdid.getCantidad() > var1) {
                            var2 = var1;
                        } else {
                            var2 = ocdid.getCantidad();
                        }

                        facades.getOperacionInventarioDetalleFacade().create(userInfo,
                                oi.getId(), codigoTipoOperacion, ocdid.getIdProducto(),
                                var2, ocdid.getFechaVencimiento(),
                                ocdid.getIdDepositoLogistico(), null, ocdid.getCodigoEstadoInventario(),
                                ocdid.getIdDepositoLogistico(), null, "DISPONIBLE");

                        OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                        ocdidItem.setId(ocdid.getId());
                        ocdidItem.setIdInventarioDetalle(ocdid.getIdInventarioDetalle());
                        ocdidItem.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());
                        ocdidItem.setCantidad(ocdid.getCantidad() - var2);
                        facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);

                        var1 = var1 - var2;
                    }
                } else if (oldOcd == null || newOcd.getCantidadConfirmada() > oldOcd.getCantidadConfirmada()) {

                    OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
                    ocdparam.setIdEmpresa(newOc.getIdEmpresa());
                    ocdparam.setIdOrdenCompra(newOcd.getIdOrdenCompra());
                    ocdparam.setId(newOcd.getId());
                    List<OrdenCompraDetallePojo> list = facades.getOrdenCompraDetalleFacade().getDisponibleDetalle(ocdparam);

                    Integer var1 = newOcd.getCantidadConfirmada() - (oldOcd == null ? 0 : oldOcd.getCantidadConfirmada());

                    for (OrdenCompraDetallePojo ocd : list) {

                        if (var1 <= 0) {
                            continue;
                        }

                        Integer var2;

                        if (ocd.getCantidadDisponible().intValue() > var1) {
                            var2 = var1;
                        } else {
                            var2 = ocd.getCantidadDisponible().intValue();
                        }

                        facades.getOperacionInventarioDetalleFacade().create(userInfo,
                                oi.getId(), codigoTipoOperacion, ocd.getIdProducto(),
                                var2, ocd.getFechaVencimiento(),
                                ocd.getIdDepositoLogistico(), null, "DISPONIBLE",
                                ocd.getIdDepositoLogistico(), null, "COMPROMETIDO");

                        OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
                        ocdidparam.setFechaVencimiento(ocd.getFechaVencimiento());
                        ocdidparam.setIdProducto(ocd.getIdProducto());
                        ocdidparam.setIdDepositoLogistico(ocd.getIdDepositoLogistico());
                        ocdidparam.setCodigoEstadoInventario("COMPROMETIDO");
                        ocdidparam.setIdOrdenCompraDetalle(newOcd.getId());

                        OrdenCompraDetalleInventarioDetallePojo item = facades.getOrdenCompraDetalleInventarioDetalleFacade().findFirstPojo(ocdidparam);
                        if (item != null) {
                            OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                            ocdidItem.setId(item.getId());
                            ocdidItem.setIdInventarioDetalle(item.getIdInventarioDetalle());
                            ocdidItem.setIdOrdenCompraDetalle(item.getIdOrdenCompraDetalle());
                            ocdidItem.setCantidad(item.getCantidad() + var2);
                            facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);
                        } else {
                            ocdidparam.setCantidad(var2);
                            facades.getOrdenCompraDetalleInventarioDetalleFacade().create(ocdidparam, userInfo);
                        }

                        var1 = var1 - var2;
                    }

                    if (var1 > 0) {
                        throw new RuntimeException("No existe inventario suficiente");
                    }
                }
            }
        }

        if (newOc.getRecibido().equals('S')
                && newOc.getCodigoEstado().equals("FACTURADO") && oldOc != null
                && (oldOc.getCodigoEstado().equals("PENDIENTE_PREPARACION")
                || oldOc.getCodigoEstado().equals("PENDIENTE_FACTURACION"))) {

            String codigoTipoOperacion = "AJUSTE_ORDEN_COMPRA";
            OperacionInventario oi = facades.getOperacionInventarioFacade().create(
                    userInfo, codigoTipoOperacion, "AJUSTE_ORDEN_COMPRA",
                    "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());

            OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
            ocdidparam.setIdOrdenCompra(newOc.getId());
            ocdidparam.setFirstResult(0);
            ocdidparam.setPageSize(1000);
            List<OrdenCompraDetalleInventarioDetallePojo> list = facades.getOrdenCompraDetalleInventarioDetalleFacade().findPojo(ocdidparam);

            for (OrdenCompraDetalleInventarioDetallePojo ocdid : list) {

                if (ocdid.getCantidad() <= 0) {
                    continue;
                }

                facades.getOperacionInventarioDetalleFacade().create(userInfo, oi.getId(), codigoTipoOperacion,
                        ocdid.getIdProducto(), ocdid.getCantidad(), ocdid.getFechaVencimiento(),
                        ocdid.getIdDepositoLogistico(), null, ocdid.getCodigoEstadoInventario(),
                        ocdid.getIdDepositoLogistico(), null, "FACTURADO");

                OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                ocdidItem.setId(ocdid.getId());
                ocdidItem.setIdInventarioDetalle(ocdid.getIdInventarioDetalle());
                ocdidItem.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());
                ocdidItem.setCantidad(0);
                facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);

                ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
                ocdidparam.setFechaVencimiento(ocdid.getFechaVencimiento());
                ocdidparam.setIdProducto(ocdid.getIdProducto());
                ocdidparam.setIdDepositoLogistico(ocdid.getIdDepositoLogistico());
                ocdidparam.setCodigoEstadoInventario("FACTURADO");
                ocdidparam.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());

                OrdenCompraDetalleInventarioDetallePojo item = facades.getOrdenCompraDetalleInventarioDetalleFacade().findFirstPojo(ocdidparam);
                if (item != null) {
                    ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                    ocdidItem.setId(item.getId());
                    ocdidItem.setIdInventarioDetalle(item.getIdInventarioDetalle());
                    ocdidItem.setIdOrdenCompraDetalle(item.getIdOrdenCompraDetalle());
                    ocdidItem.setCantidad(item.getCantidad() + ocdid.getCantidad());
                    facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);
                } else {
                    ocdidparam.setCantidad(ocdid.getCantidad());
                    facades.getOrdenCompraDetalleInventarioDetalleFacade().create(ocdidparam, userInfo);
                }
            }
        }

        if (newOc.getRecibido().equals('S') && oldOc != null
                && newOc.getCodigoEstado().equals(oldOc.getCodigoEstado())) {

            OperacionInventario oi = null;
            String codigoTipoOperacion = "AJUSTE_ORDEN_COMPRA";

            String nuevoEstadoInventario = null;

            switch (newOc.getCodigoEstado()) {
                case "PENDIENTE_PREPARACION":
                case "PENDIENTE_FACTURACION":
                    nuevoEstadoInventario = "COMPROMETIDO";
                    break;
                case "FACTURADO":
                    nuevoEstadoInventario = "FACTURADO";
                    break;
            }

            for (OrdenCompraDetalle newOcd : newMap.values()) {

                OrdenCompraDetallePojo oldOcd = oldMap.get(newOcd.getId());

                if (oldOcd != null && newOcd.getCantidadConfirmada() < oldOcd.getCantidadConfirmada()) {

                    if (oi == null) {
                        oi = facades.getOperacionInventarioFacade().create(
                                userInfo, codigoTipoOperacion, "AJUSTE_ORDEN_COMPRA",
                                "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());
                    }

                    OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
                    ocdidparam.setIdOrdenCompra(newOcd.getIdOrdenCompra());
                    ocdidparam.setIdOrdenCompraDetalle(newOcd.getId());
                    ocdidparam.setFirstResult(0);
                    ocdidparam.setPageSize(1000);
                    List<OrdenCompraDetalleInventarioDetallePojo> list = facades.getOrdenCompraDetalleInventarioDetalleFacade().findPojo(ocdidparam);

                    Integer var1 = oldOcd.getCantidadConfirmada() - newOcd.getCantidadConfirmada();

                    for (OrdenCompraDetalleInventarioDetallePojo ocdid : list) {

                        if (var1 <= 0) {
                            continue;
                        }

                        Integer var2;

                        if (ocdid.getCantidad() > var1) {
                            var2 = var1;
                        } else {
                            var2 = ocdid.getCantidad();
                        }

                        facades.getOperacionInventarioDetalleFacade().create(userInfo,
                                oi.getId(), codigoTipoOperacion, ocdid.getIdProducto(),
                                var2, ocdid.getFechaVencimiento(),
                                ocdid.getIdDepositoLogistico(), null, ocdid.getCodigoEstadoInventario(),
                                ocdid.getIdDepositoLogistico(), null, "DISPONIBLE");

                        OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                        ocdidItem.setId(ocdid.getId());
                        ocdidItem.setIdInventarioDetalle(ocdid.getIdInventarioDetalle());
                        ocdidItem.setIdOrdenCompraDetalle(ocdid.getIdOrdenCompraDetalle());
                        ocdidItem.setCantidad(ocdid.getCantidad() - var2);
                        facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);

                        var1 = var1 - var2;
                    }
                } else if (oldOcd == null || newOcd.getCantidadConfirmada() > oldOcd.getCantidadConfirmada()) {

                    if (oi == null) {
                        oi = facades.getOperacionInventarioFacade().create(
                                userInfo, "AJUSTE_ORDEN_COMPRA", "AJUSTE_ORDEN_COMPRA",
                                "CONFIRMADO", newOc.getId(), newOc.getIdEmpresa());
                    }

                    OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
                    ocdparam.setIdEmpresa(newOc.getIdEmpresa());
                    ocdparam.setIdOrdenCompra(newOcd.getIdOrdenCompra());
                    ocdparam.setId(newOcd.getId());
                    List<OrdenCompraDetallePojo> list = facades.getOrdenCompraDetalleFacade().getDisponibleDetalle(ocdparam);

                    Integer var1 = newOcd.getCantidadConfirmada() - (oldOcd == null ? 0 : oldOcd.getCantidadConfirmada());

                    for (OrdenCompraDetallePojo ocd : list) {

                        if (var1 <= 0) {
                            continue;
                        }

                        Integer var2;

                        if (ocd.getCantidadDisponible().intValue() > var1) {
                            var2 = var1;
                        } else {
                            var2 = ocd.getCantidadDisponible().intValue();
                        }

                        facades.getOperacionInventarioDetalleFacade().create(userInfo,
                                oi.getId(), codigoTipoOperacion,
                                ocd.getIdProducto(), var2, ocd.getFechaVencimiento(),
                                ocd.getIdDepositoLogistico(), null, "DISPONIBLE",
                                ocd.getIdDepositoLogistico(), null, nuevoEstadoInventario);

                        OrdenCompraDetalleInventarioDetalleParam ocdidparam = new OrdenCompraDetalleInventarioDetalleParam();
                        ocdidparam.setFechaVencimiento(ocd.getFechaVencimiento());
                        ocdidparam.setIdProducto(ocd.getIdProducto());
                        ocdidparam.setIdDepositoLogistico(ocd.getIdDepositoLogistico());
                        ocdidparam.setCodigoEstadoInventario(nuevoEstadoInventario);
                        ocdidparam.setIdOrdenCompraDetalle(newOcd.getId());

                        OrdenCompraDetalleInventarioDetallePojo item = facades.getOrdenCompraDetalleInventarioDetalleFacade().findFirstPojo(ocdidparam);
                        if (item != null) {
                            OrdenCompraDetalleInventarioDetalle ocdidItem = new OrdenCompraDetalleInventarioDetalle();
                            ocdidItem.setId(item.getId());
                            ocdidItem.setIdInventarioDetalle(item.getIdInventarioDetalle());
                            ocdidItem.setIdOrdenCompraDetalle(item.getIdOrdenCompraDetalle());
                            ocdidItem.setCantidad(item.getCantidad() + var2);
                            facades.getOrdenCompraDetalleInventarioDetalleFacade().edit(ocdidItem);
                        } else {
                            ocdidparam.setCantidad(var2);
                            facades.getOrdenCompraDetalleInventarioDetalleFacade().create(ocdidparam, userInfo);
                        }

                        var1 = var1 - var2;
                    }

                    if (var1 > 0) {
                        throw new RuntimeException("No existe inventario suficiente");
                    }
                }
            }
        }

        if (newOc.getRecibido().equals('N')
                && newOc.getRegistroInventario().equals('S')
                && newOc.getCodigoEstado().equals("CONFIRMADO")) {

            OperacionInventario oi = null;
            String codigoTipoOperacion = "ENTRADA";

            for (OrdenCompraDetalle detalle : newMap.values()) {

                if (detalle.getCantidadConfirmada() <= 0
                        || detalle.getIdProducto() == null
                        || detalle.getIdDepositoLogistico() == null
                        || detalle.getIdEstadoInventario() == null) {
                    continue;
                }

                if (oi == null) {
                    oi = facades.getOperacionInventarioFacade().create(
                            userInfo, codigoTipoOperacion, "RECEPCION_ORDEN_COMPRA",
                            "CONFIRMADO", null, newOc.getIdEmpresa());
                }

                facades.getOperacionInventarioDetalleFacade().create(userInfo,
                        oi.getId(), codigoTipoOperacion, detalle.getIdProducto(),
                        detalle.getCantidadConfirmada(), detalle.getFechaVencimiento(), null, null, null,
                        detalle.getIdDepositoLogistico(), detalle.getIdEstadoInventario(), null);
            }
        }
    }

    public void operacionInventarioDetalle(UserInfoImpl userInfo,
            OperacionInventarioDetalleParam newOid) {

        if (newOid.getCodigoEstadoOperacion().equals("CONFIRMADO")) {

            if (newOid.getIdDepositoOrigen() != null
                    && newOid.getIdEstadoInventarioOrigen() != null
                    && (newOid.getCodigoTipoOperacion().equals("ENTRADA")
                    || newOid.getCodigoTipoOperacion().equals("SALIDA")
                    || newOid.getCodigoTipoOperacion().equals("AJUSTE_ORDEN_COMPRA")
                    || newOid.getCodigoTipoOperacion().equals("MOVIMIENTO_INTERNO"))) {
                facades.getInventarioDetalleFacade().disminuirInventarioDetalle(
                        newOid.getIdDepositoOrigen(), newOid.getIdProducto(),
                        newOid.getIdEstadoInventarioOrigen(), newOid.getFechaVencimiento(),
                        newOid.getCantidad(), userInfo);
            }

            if (newOid.getIdDepositoDestino() != null
                    && newOid.getIdEstadoInventarioDestino() != null
                    && (newOid.getCodigoTipoOperacion().equals("ENTRADA")
                    || newOid.getCodigoTipoOperacion().equals("SALIDA")
                    || newOid.getCodigoTipoOperacion().equals("AJUSTE_ORDEN_COMPRA")
                    || newOid.getCodigoTipoOperacion().equals("MOVIMIENTO_INTERNO"))) {
                facades.getInventarioDetalleFacade().aumentarInventarioDetalle(
                        newOid.getIdDepositoDestino(), newOid.getIdProducto(),
                        newOid.getIdEstadoInventarioDestino(), newOid.getFechaVencimiento(),
                        newOid.getCantidad(), userInfo);
            }

            if (newOid.getIdDepositoDestino() != null
                    && newOid.getIdEstadoInventarioDestino() != null
                    && (newOid.getCodigoTipoOperacion().equals("AJUSTE_CONTROL_INVENTARIO")
                    || newOid.getCodigoTipoOperacion().equals("AJUSTE_INVENTARIO"))) {
                facades.getInventarioDetalleFacade().registrarInventarioDetalle(
                        newOid.getIdDepositoDestino(), newOid.getIdProducto(),
                        newOid.getIdEstadoInventarioDestino(), newOid.getFechaVencimiento(),
                        newOid.getCantidad(), userInfo);
            }
        }
    }
}
