/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ContratoServicioFacade extends Facade<ContratoServicio, ContratoServicioParam, UserInfoImpl> {

    public Boolean validToCreate(ContratoServicioParam param, Boolean validarIdContrato);

    public Boolean validToEdit(ContratoServicioParam param);

    public ContratoServicio find(Integer idContrato, Integer idServicio);

    public ContratoServicio renovar(Integer idContratoActual, Integer idContratoNuevo, Integer idServicio);

}
