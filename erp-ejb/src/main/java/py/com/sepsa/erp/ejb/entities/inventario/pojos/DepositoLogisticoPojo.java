/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.math.BigInteger;
import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class DepositoLogisticoPojo {

    public DepositoLogisticoPojo(Integer id, String descripcion, String codigo,
            Character activo, Date fechaInsercion, Integer idLocal, String local,
            Character localActivo, BigInteger gln, String idExterno,
            Integer idCliente, String cliente, Integer idEmpresa,
            String empresa) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.activo = activo;
        this.fechaInsercion = fechaInsercion;
        this.idLocal = idLocal;
        this.local = local;
        this.localActivo = localActivo;
        this.gln = gln;
        this.idExterno = idExterno;
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código
     */
    private String codigo;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;
    
    /**
     * Identificador de local
     */
    private Integer idLocal;
    
    /**
     * Local
     */
    private String local;
    
    /**
     * Local activo
     */
    private Character localActivo;
    
    /**
     * GLN
     */
    private BigInteger gln;
    
    /**
     * Identificador externo
     */
    private String idExterno;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Character getLocalActivo() {
        return localActivo;
    }

    public void setLocalActivo(Character localActivo) {
        this.localActivo = localActivo;
    }

    public BigInteger getGln() {
        return gln;
    }

    public void setGln(BigInteger gln) {
        this.gln = gln;
    }

    public String getIdExterno() {
        return idExterno;
    }

    public void setIdExterno(String idExterno) {
        this.idExterno = idExterno;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
