/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class RegistroComprobantePojo {
    
    /**
     * Bytes del archivo
     */
    private byte[] bytes;
    
    /**
     * Nombre del archivo
     */
    private String nombreArchivo;
    
    /**
     * Tipo de contenido
     */
    private String tipoContenido;
    
    /**
     * Código de tipo de registro
     */
    private Integer tipoRegistro;
    
    /**
     * Código tipo de identificación del comprador
     */
    private Integer tipoIdentificacionComprador;
    
    /**
     * Número de identificación del comprador
     */
    private String identificacionComprador;
    
    /**
     * Nombre o razón social del comprador
     */
    private String razonSocialComprador;
    
    /**
     * Código tipo de identificación del vendedor
     */
    private Integer tipoIdentificacionVendedor;
    
    /**
     * Número de identificación del vendedor
     */
    private String identificacionVendedor;
    
    /**
     * Nombre o razón social del vendedor
     */
    private String razonSocialVendedor;
    
    /**
     * Código de tipo de comprobante
     */
    private Integer tipoComprobante;
    
    /**
     * Fecha de emisión del comprobante
     */
    private Date fechaEmisionComprobante;
    
    /**
     * Número de timbrado
     */
    private Integer numeroTimbrado;
    
    /**
     * Número del comprobante
     */
    private String numeroComprobante;
    
    /**
     * Monto gravado al 10%
     */
    private BigDecimal montoIva10;
    
    /**
     * Monto gravado al 5%
     */
    private BigDecimal montoIva5;
    
    /**
     * Monto exento
     */
    private BigDecimal montoExento;
    
    /**
     * Monto total 
     */
    private BigDecimal montoTotal;
    
    /**
     * Código de condición de venta
     */
    private Integer condicionVenta;
    
    /**
     * Operación en moneda extrangera
     */
    private Character monedaExtrangera;
    
    /**
     * Número del comprobante de venta asociado
     */
    private String numeroComprobanteAsociado;
    
    /**
     * Tipo de cambio compra
     */
    private BigDecimal tipoCambioCompra;
    
    /**
     * Tipo de cambio venta
     */
    private BigDecimal tipoCambioVenta;
    
    /**
     * Timbrado del comprobante de venta asociado
     */
    private Integer timbradoComprobanteAsociado;

    public String generarComprobanteVenta(String imputaIva, String imputaIre,
            String imputaIrpRsp) {
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        tipoIdentificacionComprador = identificacionComprador.contains("-") && identificacionComprador.length() > 2
                ? 11
                : identificacionComprador.length() > 1
                ? 12
                : 15;
        
        BigDecimal mult = tipoCambioVenta == null
                    || tipoCambioVenta
                            .stripTrailingZeros()
                            .compareTo(BigDecimal.ZERO) <= 0
                    ? BigDecimal.ONE
                    : tipoCambioVenta.stripTrailingZeros();
        
        StringBuilder sb = new StringBuilder();
        sb.append("1").append(",");//Campo 1
        sb.append(tipoIdentificacionComprador).append(",");//Campo 2
        sb.append(identificacionComprador).append(",");//Campo 3
        sb.append("").append(",");//Campo 4
        sb.append(tipoComprobante).append(",");//Campo 5
        sb.append(sdf.format(fechaEmisionComprobante)).append(",");//Campo 6
        sb.append(numeroTimbrado).append(",");//Campo 7
        sb.append(numeroComprobante).append(",");//Campo 8
        sb.append(montoIva10.multiply(mult).toBigInteger()).append(",");//Campo 9
        sb.append(montoIva5.multiply(mult).toBigInteger()).append(",");//Campo 10
        sb.append(montoExento.multiply(mult).toBigInteger()).append(",");//Campo 11
        sb.append(montoTotal.multiply(mult).toBigInteger()).append(",");//Campo 12
        sb.append(condicionVenta).append(",");//Campo 13
        sb.append(monedaExtrangera).append(",");//Campo 14
        sb.append(imputaIva == null || imputaIva.trim().isEmpty()
                ? "N" : imputaIva.charAt(0)).append(",");//Campo 15
        sb.append(imputaIre == null || imputaIre.trim().isEmpty()
                ? "N" : imputaIre.charAt(0)).append(",");//Campo 16
        sb.append(imputaIrpRsp == null || imputaIrpRsp.trim().isEmpty()
                ? "N" : imputaIrpRsp.charAt(0)).append(",");//Campo 17
        sb.append(numeroComprobanteAsociado == null ? "" : numeroComprobanteAsociado).append(",");//Campo 18
        sb.append(timbradoComprobanteAsociado == null ? "" : timbradoComprobanteAsociado).append(",");//Campo 19
        
        return sb.toString();
    }
    
    public String generarComprobanteCompra(String imputaIva, String imputaIre,
            String imputaIrpRsp, String noImputa) {
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        tipoIdentificacionVendedor = identificacionVendedor.contains("-") && identificacionVendedor.length() > 2
                ? 11
                : identificacionVendedor.length() > 1
                ? 12
                : 15;
        
        BigDecimal mult = tipoCambioCompra == null
                    || tipoCambioCompra
                            .stripTrailingZeros()
                            .compareTo(BigDecimal.ZERO) <= 0
                    ? BigDecimal.ONE
                    : tipoCambioCompra.stripTrailingZeros();
        
        StringBuilder sb = new StringBuilder();
        sb.append("2").append(",");//Campo 1
        sb.append(tipoIdentificacionVendedor).append(",");//Campo 2
        sb.append(identificacionVendedor).append(",");//Campo 3
        sb.append(razonSocialVendedor).append(",");//Campo 4
        sb.append(tipoComprobante).append(",");//Campo 5
        sb.append(sdf.format(fechaEmisionComprobante)).append(",");//Campo 6
        sb.append(numeroTimbrado).append(",");//Campo 7
        sb.append(numeroComprobante).append(",");//Campo 8
        sb.append(montoIva10.multiply(mult).toBigInteger()).append(",");//Campo 9
        sb.append(montoIva5.multiply(mult).toBigInteger()).append(",");//Campo 10
        sb.append(montoExento.multiply(mult).toBigInteger()).append(",");//Campo 11
        sb.append(montoTotal.multiply(mult).toBigInteger()).append(",");//Campo 12
        sb.append(condicionVenta).append(",");//Campo 13
        sb.append(monedaExtrangera).append(",");//Campo 14
        sb.append(imputaIva == null || imputaIva.trim().isEmpty()
                ? "N" : imputaIva.charAt(0)).append(",");//Campo 15
        sb.append(imputaIre == null || imputaIre.trim().isEmpty()
                ? "N" : imputaIre.charAt(0)).append(",");//Campo 16
        sb.append(imputaIrpRsp == null || imputaIrpRsp.trim().isEmpty()
                ? "N" : imputaIrpRsp.charAt(0)).append(",");//Campo 17
        sb.append(noImputa == null || noImputa.trim().isEmpty()
                ? "N" : noImputa.charAt(0)).append(",");//Campo 18
        sb.append(numeroComprobanteAsociado == null ? "" : numeroComprobanteAsociado).append(",");//Campo 19
        sb.append(timbradoComprobanteAsociado == null ? "" : timbradoComprobanteAsociado).append(",");//Campo 20
        
        return sb.toString();
    }
    
    public Integer getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(Integer tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public Integer getTipoIdentificacionComprador() {
        return tipoIdentificacionComprador;
    }

    public void setTipoIdentificacionComprador(Integer tipoIdentificacionComprador) {
        this.tipoIdentificacionComprador = tipoIdentificacionComprador;
    }

    public String getIdentificacionComprador() {
        return identificacionComprador;
    }

    public void setIdentificacionComprador(String identificacionComprador) {
        this.identificacionComprador = identificacionComprador;
    }

    public String getRazonSocialComprador() {
        return razonSocialComprador;
    }

    public void setRazonSocialComprador(String razonSocialComprador) {
        this.razonSocialComprador = razonSocialComprador;
    }

    public Integer getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(Integer tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public Date getFechaEmisionComprobante() {
        return fechaEmisionComprobante;
    }

    public void setFechaEmisionComprobante(Date fechaEmisionComprobante) {
        this.fechaEmisionComprobante = fechaEmisionComprobante;
    }

    public Integer getNumeroTimbrado() {
        return numeroTimbrado;
    }

    public void setNumeroTimbrado(Integer numeroTimbrado) {
        this.numeroTimbrado = numeroTimbrado;
    }

    public String getNumeroComprobante() {
        return numeroComprobante;
    }

    public void setNumeroComprobante(String numeroComprobante) {
        this.numeroComprobante = numeroComprobante;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoExento() {
        return montoExento;
    }

    public void setMontoExento(BigDecimal montoExento) {
        this.montoExento = montoExento;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getCondicionVenta() {
        return condicionVenta;
    }

    public void setCondicionVenta(Integer condicionVenta) {
        this.condicionVenta = condicionVenta;
    }

    public Character getMonedaExtrangera() {
        return monedaExtrangera;
    }

    public void setMonedaExtrangera(Character monedaExtrangera) {
        this.monedaExtrangera = monedaExtrangera;
    }

    public String getNumeroComprobanteAsociado() {
        return numeroComprobanteAsociado;
    }

    public void setNumeroComprobanteAsociado(String numeroComprobanteAsociado) {
        this.numeroComprobanteAsociado = numeroComprobanteAsociado;
    }

    public Integer getTimbradoComprobanteAsociado() {
        return timbradoComprobanteAsociado;
    }

    public void setTimbradoComprobanteAsociado(Integer timbradoComprobanteAsociado) {
        this.timbradoComprobanteAsociado = timbradoComprobanteAsociado;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    public void setRazonSocialVendedor(String razonSocialVendedor) {
        this.razonSocialVendedor = razonSocialVendedor;
    }

    public void setIdentificacionVendedor(String identificacionVendedor) {
        this.identificacionVendedor = identificacionVendedor;
    }

    public String getRazonSocialVendedor() {
        return razonSocialVendedor;
    }

    public String getIdentificacionVendedor() {
        return identificacionVendedor;
    }

    public void setTipoIdentificacionVendedor(Integer tipoIdentificacionVendedor) {
        this.tipoIdentificacionVendedor = tipoIdentificacionVendedor;
    }

    public Integer getTipoIdentificacionVendedor() {
        return tipoIdentificacionVendedor;
    }

    public void setTipoCambioVenta(BigDecimal tipoCambioVenta) {
        this.tipoCambioVenta = tipoCambioVenta;
    }

    public BigDecimal getTipoCambioVenta() {
        return tipoCambioVenta;
    }

    public void setTipoCambioCompra(BigDecimal tipoCambioCompra) {
        this.tipoCambioCompra = tipoCambioCompra;
    }

    public BigDecimal getTipoCambioCompra() {
        return tipoCambioCompra;
    }
}
