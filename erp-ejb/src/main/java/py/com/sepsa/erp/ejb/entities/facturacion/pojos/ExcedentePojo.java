/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

/**
 * POJO para la entidad Excedente
 * @author Jonathan
 */
public class ExcedentePojo {
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Servicio
     */
    private String servicio;
    
    /**
     * Identificador de tipo de documento
     */
    private Integer idTipoDocumento;
    
    /**
     * Tipo de documento
     */
    private String tipoDocumento;
    
    /**
     * Cantidad máxima
     */
    private Integer cantidadMaxima;
    
    /**
     * Monto excedente
     */
    private Number montoExcedente;
    
    /**
     * Activo
     */
    private Character activo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getCantidadMaxima() {
        return cantidadMaxima;
    }

    public void setCantidadMaxima(Integer cantidadMaxima) {
        this.cantidadMaxima = cantidadMaxima;
    }

    public Number getMontoExcedente() {
        return montoExcedente;
    }

    public void setMontoExcedente(Number montoExcedente) {
        this.montoExcedente = montoExcedente;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }
}
