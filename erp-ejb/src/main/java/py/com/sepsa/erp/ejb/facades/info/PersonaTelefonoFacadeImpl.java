/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefono;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaTelefonoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaTelefonoFacade", mappedName = "PersonaTelefonoFacade")
@Local(PersonaTelefonoFacade.class)
public class PersonaTelefonoFacadeImpl extends FacadeImpl<PersonaTelefono, PersonaTelefonoParam> implements PersonaTelefonoFacade {
    
    public PersonaTelefonoFacadeImpl() {
        super(PersonaTelefono.class);
    }
    
    @Override
    public PersonaTelefono obtenerTelefonoCliente(Integer idCliente) {
        PersonaTelefonoParam param = new PersonaTelefonoParam();
        param.setActivo('S');
        param.setIdPersona(idCliente);
        return findFirst(param);
    }
    
    @Override
    public boolean validToCreate(PersonaTelefonoParam param, boolean validarIdPersona) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        if(param.getIdPersona() != null && param.getIdTelefono() != null) {
            PersonaTelefonoParam param1 = new PersonaTelefonoParam();
            param1.setIdPersona(param.getIdPersona());
            param1.setIdTelefono(param.getIdTelefono());
            Long size = findSize(param1);

            if (size > 0) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro"));
            }
        }

        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());

        if (validarIdPersona && persona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }

        if(param.getIdTelefono() != null) {
            Telefono telefono = facades.getTelefonoFacade().find(param.getIdTelefono());

            if (telefono == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el telefono"));
            }
        }
        
        if(param.getTelefono() != null) {
            facades.getTelefonoFacade().validToCreate(param.getTelefono());
        }

        return !param.tieneErrores();
    }

    @Override
    public boolean validToEdit(PersonaTelefonoParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        PersonaTelefonoParam param1 = new PersonaTelefonoParam();
        param1.setIdPersona(param.getIdPersona());
        param1.setIdTelefono(param.getIdTelefono());
        PersonaTelefono pt = findFirst(param1);

        if (pt == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe un registro"));
        } else {
            param.setId(pt.getId());
        }

        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());

        if (persona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }

        Telefono telefono = facades.getTelefonoFacade().find(param.getIdTelefono());

        if (telefono == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el telefono"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public PersonaTelefono create(PersonaTelefonoParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        Integer idTelefono = param.getIdTelefono();
        
        if(param.getTelefono() != null) {
            Telefono telefono = facades.getTelefonoFacade().create(param.getTelefono(), userInfo);
            idTelefono = telefono == null ? idTelefono : telefono.getId();
        }
        
        PersonaTelefono item = new PersonaTelefono();
        item.setIdPersona(param.getIdPersona());
        item.setIdTelefono(idTelefono);
        item.setActivo(param.getActivo());

        create(item);

        return item;
    }

    @Override
    public PersonaTelefono edit(PersonaTelefonoParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        PersonaTelefono item = find(param.getId());
        item.setIdPersona(param.getIdPersona());
        item.setIdTelefono(param.getIdTelefono());
        item.setActivo(param.getActivo());

        edit(item);

        return item;
    }

    @Override
    public List<PersonaTelefono> find(PersonaTelefonoParam param) {

        String where = "true";

        if(param.getIdEmpresa() != null) {
            where = String.format("%s and con.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getContacto() != null && !param.getContacto().trim().isEmpty()) {
            where = String.format("%s and coalesce(con.razon_social, con.nombre || ' ' || con.apellido) ilike '%%%s%%'", where, param.getContacto().trim());
        }
        
        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }

        if (param.getIdTelefono() != null) {
            where = String.format("%s and pt.id_telefono = %d", where, param.getIdTelefono());
        }

        if (param.getPrefijo() != null && !param.getPrefijo().trim().isEmpty()) {
            where = String.format("%s and t.prefijo ilike '%%%s%%'", where, param.getPrefijo().trim());
        }

        if (param.getNumero() != null && !param.getNumero().trim().isEmpty()) {
            where = String.format("%s and t.numero ilike '%%%s%%'", where, param.getNumero().trim());
        }

        if (param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }

        if (param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }

        if (param.getActivo() != null) {
            where = String.format("%s and pt.activo = '%s'", where, param.getActivo());
        }

        if (param.getContactoActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getContactoActivo());
        }

        if(param.getIdPersona() != null) {
            where = String.format("%s and pt.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getIdPersonaContacto() != null) {
            where = String.format("%s and pc.id_persona = %d ", where, param.getIdPersonaContacto());
        }

        String sql = String.format("select pt.*"
                + " from info.persona_telefono pt"
                + " left join info.contacto c on c.id_contacto = pt.id_persona"
                + " left join info.persona con on con.id = pt.id_persona"
                + " left join info.persona_contacto pc on pc.id_contacto = pt.id_persona"
                + " left join info.cargo cargo on cargo.id = c.id_cargo"
                + " left join info.tipo_contacto tc on tc.id = c.id_tipo_contacto"
                + " left join info.telefono t on t.id = pt.id_telefono"
                + " where %s order by c.activo asc, pc.id_persona asc offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, PersonaTelefono.class);

        List<PersonaTelefono> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(PersonaTelefonoParam param) {

        String where = "true";

        if(param.getIdEmpresa() != null) {
            where = String.format("%s and con.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getContacto() != null && !param.getContacto().trim().isEmpty()) {
            where = String.format("%s and coalesce(con.razon_social, con.nombre || ' ' || con.apellido) ilike '%%%s%%'", where, param.getContacto().trim());
        }

        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }
        
        if (param.getIdTelefono() != null) {
            where = String.format("%s and pt.id_telefono = %d", where, param.getIdTelefono());
        }

        if (param.getPrefijo() != null && !param.getPrefijo().trim().isEmpty()) {
            where = String.format("%s and t.prefijo ilike '%%%s%%'", where, param.getPrefijo().trim());
        }

        if (param.getNumero() != null && !param.getNumero().trim().isEmpty()) {
            where = String.format("%s and t.numero ilike '%%%s%%'", where, param.getNumero().trim());
        }

        if (param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }

        if (param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }

        if (param.getActivo() != null) {
            where = String.format("%s and pt.activo = '%s'", where, param.getActivo());
        }

        if (param.getContactoActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getContactoActivo());
        }

        if(param.getIdPersona() != null) {
            where = String.format("%s and pt.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getIdPersonaContacto() != null) {
            where = String.format("%s and pc.id_persona = %d ", where, param.getIdPersonaContacto());
        }

        String sql = String.format("select count(pt.*)"
                + " from info.persona_telefono pt"
                + " left join info.contacto c on c.id_contacto = pt.id_persona"
                + " left join info.persona con on con.id = pt.id_persona"
                + " left join info.persona_contacto pc on pc.id_contacto = pt.id_persona"
                + " left join info.telefono t on t.id = pt.id_telefono"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
