/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.io.Serializable;

/**
 * POJO para la entidad contacto telefono notificacion
 * @author Jonathan
 */
public class PersonaTelefonoNotificacionPojo implements Serializable {

    public PersonaTelefonoNotificacionPojo() {
    }

    public PersonaTelefonoNotificacionPojo(Integer idContacto, String contacto,
            Integer idTelefono, String prefijo, String numero,
            Integer idTipoNotificacion, String tipoNotificacion,
            Character estado) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idTelefono = idTelefono;
        this.prefijo = prefijo;
        this.numero = numero;
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.estado = estado;
    }
    
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    
    /**
     * Contacto
     */
    private String contacto;
    
    /**
     * Identificador de telefono
     */
    private Integer idTelefono;
    
    /**
     * Prefijo
     */
    private String prefijo;
    
    /**
     * Numero
     */
    private String numero;
    
    /**
     * Identificador de tipo de notificacion
     */
    private Integer idTipoNotificacion;
    
    /**
     * Tipo de notificacion
     */
    private String tipoNotificacion;
    
    /**
     * Estado
     */
    private Character estado;

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getEstado() {
        return estado;
    }
}
