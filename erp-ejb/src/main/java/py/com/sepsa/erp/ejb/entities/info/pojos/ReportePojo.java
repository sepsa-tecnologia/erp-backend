/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import com.google.gson.JsonElement;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class ReportePojo {

    public ReportePojo() {
    }

    public ReportePojo(Integer id, String descripcion, String codigo, Character activo, JsonElement parametros) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.activo = activo;
        this.parametros = parametros;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Descripción
     */
    private String descripcion;

    /**
     * Código
     */
    private String codigo;

    /**
     * Activo
     */
    private Character activo;

    /**
     * Código de tipo de reporte
     */
    private String codigoTipoReporte;
    
    /**
     * Formato
     */
    private String formato;
    
    /**
     * Tipo estado
     */
    private JsonElement parametros;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setParametros(JsonElement parametros) {
        this.parametros = parametros;
    }

    public JsonElement getParametros() {
        return parametros;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getFormato() {
        return formato;
    }

    public void setCodigoTipoReporte(String codigoTipoReporte) {
        this.codigoTipoReporte = codigoTipoReporte;
    }

    public String getCodigoTipoReporte() {
        return codigoTipoReporte;
    }
}
