/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.TelefonoParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de la entidad usuario
 * @author Jonathan
 */
public class UsuarioParam extends CommonParam {

    public UsuarioParam() {
    }

    public UsuarioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Usuario
     */
    @QueryParam("usuarioEq")
    private String usuarioEq;
    
    /**
     * Usuario
     */
    @QueryParam("usuario")
    private String usuario;

    /**
     * Contrasena
     */
    @QueryParam("contrasena")
    private String contrasena;
    
    /**
     * Hash
     */
    @QueryParam("hash")
    private String hash;
    
    /**
     * Identificador de Email
     */
    @QueryParam("idEmail")
    private Integer idEmail;
    
    /**
     * Identificador de Telefono
     */
    @QueryParam("idTelefono")
    private Integer idTelefono;
    
    /**
     * URL
     */
    @QueryParam("urlDestino")
    private String urlDestino;
    
    /**
     * Tiene persona fisica
     */
    @QueryParam("tienePersonaFisica")
    private Boolean tienePersonaFisica;
    
    /**
     * Identificador de persona fisica
     */
    @QueryParam("idPersonaFisica")
    private Integer idPersonaFisica;
    
    /**
     * Nombre de persona fisica
     */
    @QueryParam("nombrePersonaFisica")
    private String nombrePersonaFisica;
    
    /**
     * Nombre de persona fisica
     */
    @QueryParam("admin")
    private String admin;
    
    private PersonaParam personaFisica;
    
    private EmailParam email;
    
    private TelefonoParam telefono;
    
    private List<UsuarioPerfilParam> usuarioPerfiles;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }

    public void setPersonaFisica(PersonaParam personaFisica) {
        this.personaFisica = personaFisica;
    }

    public PersonaParam getPersonaFisica() {
        return personaFisica;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public void setUrlDestino(String urlDestino) {
        this.urlDestino = urlDestino;
    }

    public String getUrlDestino() {
        return urlDestino;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setTelefono(TelefonoParam telefono) {
        this.telefono = telefono;
    }

    public TelefonoParam getTelefono() {
        return telefono;
    }

    public void setEmail(EmailParam email) {
        this.email = email;
    }

    public EmailParam getEmail() {
        return email;
    }

    public void setUsuarioPerfiles(List<UsuarioPerfilParam> usuarioPerfiles) {
        this.usuarioPerfiles = usuarioPerfiles;
    }

    public List<UsuarioPerfilParam> getUsuarioPerfiles() {
        return usuarioPerfiles;
    }

    public void setNombrePersonaFisica(String nombrePersonaFisica) {
        this.nombrePersonaFisica = nombrePersonaFisica;
    }

    public String getNombrePersonaFisica() {
        return nombrePersonaFisica;
    }

    public void setUsuarioEq(String usuarioEq) {
        this.usuarioEq = usuarioEq;
    }

    public String getUsuarioEq() {
        return usuarioEq;
    }

    public void setTienePersonaFisica(Boolean tienePersonaFisica) {
        this.tienePersonaFisica = tienePersonaFisica;
    }

    public Boolean getTienePersonaFisica() {
        return tienePersonaFisica;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public boolean isValidToUpdateHash() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(usuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el usuario"));
        }
        
        if(isNullOrEmpty(hash)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el usuario"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToSendRecover() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(usuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el usuario"));
        }
        
        if(isNullOrEmpty(urlDestino)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la URL destino"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToGenerateBusinessToken() {
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario"));
        }
        
        if(isNull(idEmpresa) && isNullOrEmpty(codigoEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de empresa"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToRecover() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(contrasena)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la contraseña"));
        }
        
        if(isNullOrEmpty(hash)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el hash"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToLogin() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(contrasena)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la contraseña"));
        }
        
        if(isNullOrEmpty(usuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el usuario"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el identificador de empresa"));
        }
        
        if (isNullOrEmpty(usuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el usuario"));
        }
        
        if (isNullOrEmpty(contrasena)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar la contraseña"));
        }
        
        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el identificador/código de estado"));
        }
        
        if(!isNull(personaFisica)) {
            personaFisica.setIdEmpresa(idEmpresa);
            personaFisica.isValidToCreate();
        }
        
        if(!isNull(email)) {
            email.setIdEmpresa(idEmpresa);
            email.isValidToCreate();
        }
        
        if(!isNull(telefono)) {
            telefono.setIdEmpresa(idEmpresa);
            telefono.isValidToCreate();
        }
        
        if(!isNullOrEmpty(usuarioPerfiles)) {
            for (UsuarioPerfilParam item : usuarioPerfiles) {
                item.setIdUsuario(0);
                item.setIdEmpresa(idEmpresa);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el identificador"));
        }
        
        if (isNullOrEmpty(usuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el usuario"));
        }
        
        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el identificador/código de estado"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(personaFisica)) {
            list.addAll(personaFisica.getErrores());
        }
        
        if(!isNull(email)) {
            list.addAll(email.getErrores());
        }
        
        if(!isNull(telefono)) {
            list.addAll(telefono.getErrores());
        }
        
        if(!isNullOrEmpty(usuarioPerfiles)) {
            for (UsuarioPerfilParam item : usuarioPerfiles) {
                list.addAll(item.getErrores());
            }
        }
        
        return list;
    }
}
