/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.HistoricoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.HistoricoLiquidacionPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface HistoricoLiquidacionFacade extends Facade<HistoricoLiquidacion, HistoricoLiquidacionParam, UserInfoImpl> {

    public List<HistoricoLiquidacionPojo> findCadena(HistoricoLiquidacionParam param);

    public Long findCadenaSize(HistoricoLiquidacionParam param);

    public List<HistoricoLiquidacionPojo> findPeriodo(HistoricoLiquidacionParam param);

    public Long findPeriodoSize(HistoricoLiquidacionParam param);

    public Boolean validToCreate(HistoricoLiquidacionParam param, Boolean validarLiquidacion);
    
}