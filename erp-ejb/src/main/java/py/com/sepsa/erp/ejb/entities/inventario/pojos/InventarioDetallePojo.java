/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class InventarioDetallePojo {

    public InventarioDetallePojo(Integer id, Integer idInventario,
            Date fechaVencimiento, Integer cantidad,
            Integer idDepositoLogistico, String codigoDepositoLogistico,
            String depositoLogistico, Integer idProducto, String producto,
            String codigoGtin, String codigoInterno, Integer idEstado,
            String estado, String codigoEstado) {
        this.id = id;
        this.idInventario = idInventario;
        this.fechaVencimiento = fechaVencimiento;
        this.cantidad = cantidad;
        this.idDepositoLogistico = idDepositoLogistico;
        this.codigoDepositoLogistico = codigoDepositoLogistico;
        this.depositoLogistico = depositoLogistico;
        this.idProducto = idProducto;
        this.producto = producto;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de inventario
     */
    private Integer idInventario;
    
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;
    
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
    
    /**
     * Código de deposito logistico
     */
    private String codigoDepositoLogistico;
    
    /**
     * Descripción de deposito logistico
     */
    private String depositoLogistico;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Descripción del producto
     */
    private String producto;
    
    /**
     * Código GTIN
     */
    private String codigoGtin;
    
    /**
     * Código interno
     */
    private String codigoInterno;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Descripción del estado
     */
    private String estado;
    
    /**
     * Código del producto
     */
    private String codigoEstado;
    
    /**
     * Cantidad
     */
    private Integer cantidad;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public String getCodigoDepositoLogistico() {
        return codigoDepositoLogistico;
    }

    public void setCodigoDepositoLogistico(String codigoDepositoLogistico) {
        this.codigoDepositoLogistico = codigoDepositoLogistico;
    }

    public String getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(String depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }
}
