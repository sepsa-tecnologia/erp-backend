/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de orden de compra
 *
 * @author Jonathan D. Bernal Fernández
 */
public class OrdenCompraParam extends CommonParam {

    public OrdenCompraParam() {
    }

    public OrdenCompraParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Fecha recepción
     */
    @QueryParam("fechaRecepcion")
    private Date fechaRecepcion;

    /**
     * Fecha recepción desde
     */
    @QueryParam("fechaRecepcionDesde")
    private Date fechaRecepcionDesde;

    /**
     * Fecha recepción hasta
     */
    @QueryParam("fechaRecepcionHasta")
    private Date fechaRecepcionHasta;

    /**
     * Fecha inserción
     */
    @QueryParam("fechaInsercion")
    private Date fechaInsercion;

    /**
     * Fecha inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Nro de orden de compra
     */
    @QueryParam("nroOrdenCompra")
    private String nroOrdenCompra;

    /**
     * Recibido
     */
    @QueryParam("recibido")
    private Character recibido;
    
    /**
     * Código de estado excluido
     */
    @QueryParam("codigoEstadoExcluido")
    protected String codigoEstadoExcluido;
    
    /**
     * Registro inventario
     */
    @QueryParam("registroInventario")
    private Character registroInventario;

    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;

    /**
     * Archivo EDI
     */
    @QueryParam("archivoEdi")
    private Character archivoEdi;

    /**
     * Generado EDI
     */
    @QueryParam("generadoEdi")
    private Character generadoEdi;

    /**
     * Monto iva 5
     */
    @QueryParam("montoIva5")
    private BigDecimal montoIva5;
    
    /**
     * Monto imponible 5
     */
    @QueryParam("montoImponible5")
    private BigDecimal montoImponible5;
    
    /**
     * Monto total 5
     */
    @QueryParam("montoTotal5")
    private BigDecimal montoTotal5;
    
    /**
     * Monto iva 10
     */
    @QueryParam("montoIva10")
    private BigDecimal montoIva10;
    
    /**
     * Monto imponible 10
     */
    @QueryParam("montoImponible10")
    private BigDecimal montoImponible10;
    
    /**
     * Monto total 10
     */
    @QueryParam("montoTotal10")
    private BigDecimal montoTotal10;
    
    /**
     * Monto total exento
     */
    @QueryParam("montoTotalExento")
    private BigDecimal montoTotalExento;
    
    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto total orden compra
     */
    @QueryParam("montoTotalOrdenCompra")
    private BigDecimal montoTotalOrdenCompra;
    
    /**
     * Identificador de cliente origen
     */
    @QueryParam("idClienteOrigen")
    private Integer idClienteOrigen;
    
    /**
     * Identificador de local origen
     */
    @QueryParam("idLocalOrigen")
    private Integer idLocalOrigen;

    /**
     * Identificador de cliente destino
     */
    @QueryParam("idClienteDestino")
    private Integer idClienteDestino;

    /**
     * Identificador de local destino
     */
    @QueryParam("idLocalDestino")
    private Integer idLocalDestino;

    /**
     * Observación
     */
    @QueryParam("observacion")
    private String observacion;
    
    /**
     * Identificador de procesamiento de archivo
     */
    @QueryParam("idProcesamientoArchivo")
    private Integer idProcesamientoArchivo;
    
    /**
     * Hash de procesamiento de archivo
     */
    @QueryParam("hashProcesamientoArchivo")
    private String hashProcesamientoArchivo;

    /**
     * Lista de detalle de orden de compra
     */
    private List<OrdenCompraDetalleParam> ordenCompraDetalles;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(fechaRecepcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de recepción"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNull(archivoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo EDI"));
        }
        
        if(isNull(generadoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si ya fue generado el archivo EDI"));
        }
        
        if(isNull(registroInventario)) {
            registroInventario = 'N';
        }
        
        if(!isNull(archivoEdi) && archivoEdi.equals('S')
                && !isNull(recibido) && recibido.equals('S')
                && isNullOrEmpty(nroOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de orden de compra"));
        }
        
        if(isNull(recibido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento es recibido o no"));
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento esta anulado"));
        }

        if(isNull(montoIva5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 5%"));
        }
        
        if(isNull(montoImponible5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 5%"));
        }
        
        if(isNull(montoTotal5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 5%"));
        }
        
        if(isNull(montoIva10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 10%"));
        }
        
        if(isNull(montoImponible10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 10%"));
        }
        
        if(isNull(montoTotal10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 10%"));
        }
        
        if(isNull(montoTotalExento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excento"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA"));
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoTotalOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total orden de compra"));
        }
        
        if (isNullOrEmpty(ordenCompraDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la orden de compra"));
        } else {
            for (OrdenCompraDetalleParam item : ordenCompraDetalles) {
                item.setIdOrdenCompra(0);
                item.setIdEstado(idEstado);
                item.setCodigoEstado(codigoEstado);
                item.setIdEmpresa(idEmpresa);
                item.setRecibido(recibido);
                item.setIdLocalOrigen(idLocalOrigen);
                item.setIdLocalDestino(idLocalDestino);
                item.setRegistroInventario(registroInventario);
                item.isValidToCreate();
            }
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();

        if (!isNull(ordenCompraDetalles)) {
            for (OrdenCompraDetalleParam item : ordenCompraDetalles) {
                list.addAll(item.getErrores());
            }
        }

        return list;
    }

    public boolean isValidToUpdateState() {
        
        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToCancel() {
        
        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

        codigoEstado = "RECHAZADO";
        
        return !tieneErrores();
    }

    public boolean isValidToEnable() {
        
        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

        if (isNull(fechaRecepcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de recepción"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNull(archivoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo EDI"));
        }
        
        if(isNull(generadoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si ya fue generado el archivo EDI"));
        }
        
        if(isNull(registroInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la bandera de registro de inventario"));
        }
        
        if(isNull(recibido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento es recibido o no"));
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento esta anulado"));
        }

        if (isNullOrEmpty(nroOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de orden de compra"));
        }

        if(isNull(montoIva5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 5%"));
        }
        
        if(isNull(montoImponible5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 5%"));
        }
        
        if(isNull(montoTotal5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 5%"));
        }
        
        if(isNull(montoIva10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 10%"));
        }
        
        if(isNull(montoImponible10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 10%"));
        }
        
        if(isNull(montoTotal10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 10%"));
        }
        
        if(isNull(montoTotalExento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excento"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA"));
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoTotalOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total orden de compra"));
        }
        
        if (!isNull(ordenCompraDetalles)) {
            for (OrdenCompraDetalleParam item : ordenCompraDetalles) {
                item.setIdEstado(idEstado);
                item.setCodigoEstado(codigoEstado);
                item.setIdEmpresa(idEmpresa);
                item.setRecibido(recibido);
                item.setIdLocalOrigen(idLocalOrigen);
                item.setIdLocalDestino(idLocalDestino);
                item.setRegistroInventario(registroInventario);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToGetPdf() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToUpdateFileProcess() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }
        
        if(isNull(idProcesamientoArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento de archivo"));
        }
        
        return !tieneErrores();
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getNroOrdenCompra() {
        return nroOrdenCompra;
    }

    public void setNroOrdenCompra(String nroOrdenCompra) {
        this.nroOrdenCompra = nroOrdenCompra;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Integer getIdClienteOrigen() {
        return idClienteOrigen;
    }

    public void setIdClienteOrigen(Integer idClienteOrigen) {
        this.idClienteOrigen = idClienteOrigen;
    }

    public Integer getIdClienteDestino() {
        return idClienteDestino;
    }

    public void setIdClienteDestino(Integer idClienteDestino) {
        this.idClienteDestino = idClienteDestino;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setOrdenCompraDetalles(List<OrdenCompraDetalleParam> ordenCompraDetalles) {
        this.ordenCompraDetalles = ordenCompraDetalles;
    }

    public List<OrdenCompraDetalleParam> getOrdenCompraDetalles() {
        return ordenCompraDetalles;
    }

    public Date getFechaRecepcionDesde() {
        return fechaRecepcionDesde;
    }

    public void setFechaRecepcionDesde(Date fechaRecepcionDesde) {
        this.fechaRecepcionDesde = fechaRecepcionDesde;
    }

    public Date getFechaRecepcionHasta() {
        return fechaRecepcionHasta;
    }

    public void setFechaRecepcionHasta(Date fechaRecepcionHasta) {
        this.fechaRecepcionHasta = fechaRecepcionHasta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public String getHashProcesamientoArchivo() {
        return hashProcesamientoArchivo;
    }

    public void setHashProcesamientoArchivo(String hashProcesamientoArchivo) {
        this.hashProcesamientoArchivo = hashProcesamientoArchivo;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public void setMontoTotalOrdenCompra(BigDecimal montoTotalOrdenCompra) {
        this.montoTotalOrdenCompra = montoTotalOrdenCompra;
    }

    public BigDecimal getMontoTotalOrdenCompra() {
        return montoTotalOrdenCompra;
    }

    public Character getRecibido() {
        return recibido;
    }

    public void setRecibido(Character recibido) {
        this.recibido = recibido;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setRegistroInventario(Character registroInventario) {
        this.registroInventario = registroInventario;
    }

    public Character getRegistroInventario() {
        return registroInventario;
    }

    public void setCodigoEstadoExcluido(String codigoEstadoExcluido) {
        this.codigoEstadoExcluido = codigoEstadoExcluido;
    }

    public String getCodigoEstadoExcluido() {
        return codigoEstadoExcluido;
    }
}
