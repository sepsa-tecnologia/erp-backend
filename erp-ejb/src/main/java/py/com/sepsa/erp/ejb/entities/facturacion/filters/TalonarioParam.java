/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de talonario
 * @author Jonathan D. Bernal Fernández
 */
public class TalonarioParam extends CommonParam {
    
    public TalonarioParam() {
    }

    public TalonarioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de documento
     */
    @QueryParam("idTipoDocumento")
    private Integer idTipoDocumento;
    
    /**
     * Código de tipo de documento
     */
    @QueryParam("codigoTipoDocumento")
    private String codigoTipoDocumento;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de encargado
     */
    @QueryParam("idEncargado")
    private Integer idEncargado;
    
    /**
     * Identificador de encargado nulo
     */
    @QueryParam("idEncargadoONulo")
    private Integer idEncargadoONulo;
    
    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;
    
    /**
     * Identificador de local
     */
    @QueryParam("idLocal")
    private Integer idLocal;
    
    /**
     * Identificador de locales
     */
    @QueryParam("idLocales")
    private List<Integer> idLocales;
    
    /**
     * GLN de local
     */
    @QueryParam("glnLocal")
    private BigInteger glnLocal;
    
    /**
     * Identificador externo de local
     */
    @QueryParam("idExternoLocal")
    private String idExternoLocal;
    
    /**
     * Digital
     */
    @QueryParam("digital")
    private Character digital;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Timbrado
     */
    @QueryParam("timbrado")
    private String timbrado;
    
    /**
     * Inicio
     */
    @QueryParam("inicio")
    private Integer inicio;
    
    /**
     * Fin
     */
    @QueryParam("fin")
    private Integer fin;
    
    /**
     * Fecha vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;
    
    /**
     * Fecha vencimiento desde
     */
    @QueryParam("fechaVencimientoDesde")
    private Date fechaVencimientoDesde;
    
    /**
     * Fecha vencimiento hasta
     */
    @QueryParam("fechaVencimientoHasta")
    private Date fechaVencimientoHasta;
    
    /**
     * Nro de sucursal
     */
    @QueryParam("nroSucursal")
    private String nroSucursal;
    
    /**
     * Nro de punto de venta
     */
    @QueryParam("nroPuntoVenta")
    private String nroPuntoVenta;
    
    /**
     * Fecha de inicio
     */
    @QueryParam("fechaInicio")
    private Date fechaInicio;
    
    /**
     * Nro de resolución
     */
    @QueryParam("nroResolucion")
    private String nroResolucion;
    
    /**
     * Fecha de resolución
     */
    @QueryParam("fechaResolucion")
    private Date fechaResolucion;
    
    /**
     * Serie
     */
    @QueryParam("serie")
    private String serie;
    
    /**
     * Código Interno
     */
    @QueryParam("codigoInterno")
    private Integer codigoInterno;

    public boolean datosCrearValido() {
        
        limpiarErrores();
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es digital o no"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToCountPages() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de talonario"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoDocumento) && isNullOrEmpty(codigoTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de documento"));
        }
        
        if(isNull(inicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de inicio"));
        }
        
        if(isNull(fin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número fin"));
        }
        
        if(!isNull(inicio) && !isNull(fin) && inicio >= fin) {
            addError(MensajePojo.createInstance()
                    .descripcion("La numeración de inicio debe ser inferior al número final"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el talonario esta activo"));
        }
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el talonario es digital o no"));
        }
        
        if(isNullOrEmpty(nroSucursal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el nro de sucursal"));
        }
        
        if(isNullOrEmpty(nroPuntoVenta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de punto de venta"));
        }
        
        if(!isNull(idTipoDocumento) && idTipoDocumento != 2) {
            
            if(isNullOrEmpty(timbrado)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el timbrado"));
            }
            
            if(!isNull(digital) && digital.equals('N') && isNull(fechaVencimiento)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar la fecha de vencimiento"));
            }
        }
        
        if(!isNullOrEmpty(codigoTipoDocumento) && !codigoTipoDocumento.equals("RECIBO")) {
            
            if(isNullOrEmpty(timbrado)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el timbrado"));
            }
            
            if(!isNull(digital) && digital.equals('N') && isNull(fechaVencimiento)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar la fecha de vencimiento"));
            }
        }
        
        if(!isNull(idTipoDocumento) && idTipoDocumento == 2) {
            if(isNull(idEncargado) && isNull(idLocal)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el encargado o local asociado"));
            }
        }
        
        if(!isNullOrEmpty(codigoTipoDocumento) && codigoTipoDocumento.equals("RECIBO")) {
            if(isNull(idEncargado) && isNull(idLocal)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el encargado o local asociado"));
            }
        }
        
        if(!isNull(idTipoDocumento) && idTipoDocumento != 2 && isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de inicio"));
        }
        
        if(!isNull(codigoTipoDocumento) && !codigoTipoDocumento.equals("RECIBO") && isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de inicio"));
        }
       
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de talonario"));
        }
        
        if(isNull(inicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de inicio"));
        }
        
        if(isNull(fin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número fin"));
        }
        
        if(!isNull(inicio) && !isNull(fin) && inicio >= fin) {
            addError(MensajePojo.createInstance()
                    .descripcion("La numeración de inicio debe ser inferior al número final"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el talonario esta activo"));
        }
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el talonario es digital o no"));
        }
        
        if(isNull(idTipoDocumento) && isNullOrEmpty(codigoTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de documento"));
        }
        
        if(isNullOrEmpty(nroSucursal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el nro de sucursal"));
        }
        
        if(isNullOrEmpty(nroPuntoVenta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el nro de punto de venta"));
        }
        
        if(!isNull(idTipoDocumento) && idTipoDocumento != 2) {
            
            if(isNullOrEmpty(timbrado)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el timbrado"));
            }
            
            if(!isNull(digital) && digital.equals('N') && isNull(fechaVencimiento)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar la fecha de vencimiento"));
            }
        }
        
        if(!isNullOrEmpty(codigoTipoDocumento) && !codigoTipoDocumento.equals("RECIBO")) {
            
            if(isNullOrEmpty(timbrado)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el timbrado"));
            }
            
            if(!isNull(digital) && digital.equals('N') && isNull(fechaVencimiento)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar la fecha de vencimiento"));
            }
        }
        
        if(!isNull(idTipoDocumento) && idTipoDocumento == 2) {
            if(isNull(idEncargado) && isNull(idLocal)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el encargado o local asociado"));
            }
        }
        
        if(!isNullOrEmpty(codigoTipoDocumento) && codigoTipoDocumento.equals("RECIBO")) {
            if(isNull(idEncargado) && isNull(idLocal)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el encargado o local asociado"));
            }
        }
        
        if(!isNull(idTipoDocumento) && idTipoDocumento == 1 && isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de inicio"));
        }
        
        if(!isNull(codigoTipoDocumento) && codigoTipoDocumento.equals("FACTURA") && isNull(fechaInicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de inicio"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargadoONulo(Integer idEncargadoONulo) {
        this.idEncargadoONulo = idEncargadoONulo;
    }

    public Integer getIdEncargadoONulo() {
        return idEncargadoONulo;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public Integer getInicio() {
        return inicio;
    }

    public void setInicio(Integer inicio) {
        this.inicio = inicio;
    }

    public Integer getFin() {
        return fin;
    }

    public void setFin(Integer fin) {
        this.fin = fin;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getNroSucursal() {
        return nroSucursal;
    }

    public void setNroSucursal(String nroSucursal) {
        this.nroSucursal = nroSucursal;
    }

    public String getNroPuntoVenta() {
        return nroPuntoVenta;
    }

    public void setNroPuntoVenta(String nroPuntoVenta) {
        this.nroPuntoVenta = nroPuntoVenta;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getNroResolucion() {
        return nroResolucion;
    }

    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public BigInteger getGlnLocal() {
        return glnLocal;
    }

    public void setGlnLocal(BigInteger glnLocal) {
        this.glnLocal = glnLocal;
    }

    public String getIdExternoLocal() {
        return idExternoLocal;
    }

    public void setIdExternoLocal(String idExternoLocal) {
        this.idExternoLocal = idExternoLocal;
    }

    public void setFechaVencimientoHasta(Date fechaVencimientoHasta) {
        this.fechaVencimientoHasta = fechaVencimientoHasta;
    }

    public Date getFechaVencimientoHasta() {
        return fechaVencimientoHasta;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFechaVencimientoDesde(Date fechaVencimientoDesde) {
        this.fechaVencimientoDesde = fechaVencimientoDesde;
    }

    public Date getFechaVencimientoDesde() {
        return fechaVencimientoDesde;
    }

    public void setIdLocales(List<Integer> idLocales) {
        this.idLocales = idLocales;
    }

    public List<Integer> getIdLocales() {
        return idLocales;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public Integer getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(Integer codigoInterno) {
        this.codigoInterno = codigoInterno;
    }
    
}
