/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class OperacionInventarioDetallePojo {

    public OperacionInventarioDetallePojo(Integer id,
            Integer idOperacionInventario, Integer idProducto, String codigoGtin,
            String codigoInterno, String producto, Integer idTipoOperacion,
            String codigoTipoOperacion, String tipoOperacion,
            Integer idEstadoInventarioOrigen, String estadoInventarioOrigen,
            String codigoEstadoInventarioOrigen,
            Integer idEstadoInventarioDestino, String estadoInventarioDestino,
            String codigoEstadoInventarioDestino, Integer idDepositoOrigen,
            String depositoOrigen, Integer idLocalOrigen, String localOrigen,
            Integer idDepositoDestino, String depositoDestino,
            Integer idLocalDestino, String localDestino, Integer cantidad,
            Date fechaVencimiento) {
        this.id = id;
        this.idOperacionInventario = idOperacionInventario;
        this.idProducto = idProducto;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.producto = producto;
        this.idTipoOperacion = idTipoOperacion;
        this.codigoTipoOperacion = codigoTipoOperacion;
        this.tipoOperacion = tipoOperacion;
        this.idEstadoInventarioOrigen = idEstadoInventarioOrigen;
        this.estadoInventarioOrigen = estadoInventarioOrigen;
        this.codigoEstadoInventarioOrigen = codigoEstadoInventarioOrigen;
        this.idEstadoInventarioDestino = idEstadoInventarioDestino;
        this.estadoInventarioDestino = estadoInventarioDestino;
        this.codigoEstadoInventarioDestino = codigoEstadoInventarioDestino;
        this.idDepositoOrigen = idDepositoOrigen;
        this.depositoOrigen = depositoOrigen;
        this.idLocalOrigen = idLocalOrigen;
        this.localOrigen = localOrigen;
        this.idDepositoDestino = idDepositoDestino;
        this.depositoDestino = depositoDestino;
        this.idLocalDestino = idLocalDestino;
        this.localDestino = localDestino;
        this.cantidad = cantidad;
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de operacion de inventario
     */
    private Integer idOperacionInventario;

    /**
     * Identificador de producto
     */
    private Integer idProducto;

    /**
     * Código GTIN
     */
    private String codigoGtin;

    /**
     * Código interno
     */
    private String codigoInterno;

    /**
     * Descripción del producto
     */
    private String producto;
    
    /**
     * Identificador de tipo de operacion
     */
    private Integer idTipoOperacion;
    
    /**
     * Código de tipo de operacion
     */
    private String codigoTipoOperacion;
    
    /**
     * Tipo operación
     */
    private String tipoOperacion;
    
    /**
     * Identificador de estado inventario origen
     */
    private Integer idEstadoInventarioOrigen;

    /**
     * Estado de inventario origen
     */
    private String estadoInventarioOrigen;

    /**
     * Código de estado inventario origen
     */
    private String codigoEstadoInventarioOrigen;
    
    /**
     * Identificador de estado inventario destino
     */
    private Integer idEstadoInventarioDestino;

    /**
     * Estado de inventario destino
     */
    private String estadoInventarioDestino;

    /**
     * Código de estado inventario destino
     */
    private String codigoEstadoInventarioDestino;

    /**
     * Cantidad
     */
    private Integer cantidad;
    
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;
    
    /**
     * Identificador de deposito origen
     */
    private Integer idDepositoOrigen;
    
    /**
     * Depósito logistico
     */
    private String depositoOrigen;

    /**
     * Identificador de local origen
     */
    private Integer idLocalOrigen;

    /**
     * Local origen
     */
    private String localOrigen;

    /**
     * Identificador de depositoDestino
     */
    private Integer idDepositoDestino;
    
    /**
     * Depósito destino
     */
    private String depositoDestino;

    /**
     * Identificador de local destino
     */
    private Integer idLocalDestino;

    /**
     * Local destino
     */
    private String localDestino;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdOperacionInventario() {
        return idOperacionInventario;
    }

    public void setIdOperacionInventario(Integer idOperacionInventario) {
        this.idOperacionInventario = idOperacionInventario;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdEstadoInventarioOrigen() {
        return idEstadoInventarioOrigen;
    }

    public void setIdEstadoInventarioOrigen(Integer idEstadoInventarioOrigen) {
        this.idEstadoInventarioOrigen = idEstadoInventarioOrigen;
    }

    public String getEstadoInventarioOrigen() {
        return estadoInventarioOrigen;
    }

    public void setEstadoInventarioOrigen(String estadoInventarioOrigen) {
        this.estadoInventarioOrigen = estadoInventarioOrigen;
    }

    public String getCodigoEstadoInventarioOrigen() {
        return codigoEstadoInventarioOrigen;
    }

    public void setCodigoEstadoInventarioOrigen(String codigoEstadoInventarioOrigen) {
        this.codigoEstadoInventarioOrigen = codigoEstadoInventarioOrigen;
    }

    public Integer getIdEstadoInventarioDestino() {
        return idEstadoInventarioDestino;
    }

    public void setIdEstadoInventarioDestino(Integer idEstadoInventarioDestino) {
        this.idEstadoInventarioDestino = idEstadoInventarioDestino;
    }

    public String getEstadoInventarioDestino() {
        return estadoInventarioDestino;
    }

    public void setEstadoInventarioDestino(String estadoInventarioDestino) {
        this.estadoInventarioDestino = estadoInventarioDestino;
    }

    public String getCodigoEstadoInventarioDestino() {
        return codigoEstadoInventarioDestino;
    }

    public void setCodigoEstadoInventarioDestino(String codigoEstadoInventarioDestino) {
        this.codigoEstadoInventarioDestino = codigoEstadoInventarioDestino;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public Integer getIdDepositoOrigen() {
        return idDepositoOrigen;
    }

    public void setIdDepositoOrigen(Integer idDepositoOrigen) {
        this.idDepositoOrigen = idDepositoOrigen;
    }

    public String getDepositoOrigen() {
        return depositoOrigen;
    }

    public void setDepositoOrigen(String depositoOrigen) {
        this.depositoOrigen = depositoOrigen;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public String getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(String localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public String getDepositoDestino() {
        return depositoDestino;
    }

    public void setDepositoDestino(String depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public String getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(String localDestino) {
        this.localDestino = localDestino;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public String getCodigoTipoOperacion() {
        return codigoTipoOperacion;
    }

    public void setCodigoTipoOperacion(String codigoTipoOperacion) {
        this.codigoTipoOperacion = codigoTipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
}
