/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.task;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.input.BOMInputStream;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemision;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.utils.NotaRemisionUtils;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Antonella Lucero
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
@Log4j2
public class NotaRemisionMasivaGamblingTask extends AbstracProcesamientoLote {

    @Resource
    private UserTransaction userTransaction;

    @Override
    public void procesar(Lote lote) {

        LoteArchivo archivoNotaRemision = null;

        LoteArchivoParam laparam = new LoteArchivoParam();
        laparam.setIdLote(lote.getId());
        laparam.isValidToList();
        List<LoteArchivo> archivos = facades.getLoteArchivoFacade().find(laparam);
        for (LoteArchivo archivo : archivos) {
            switch (archivo.getCodigo()) {
                case "NOTA_REMISION":
                    archivoNotaRemision = archivo;
                    break;
            }
        }
        Map<String, String> numeros = new HashMap<>();
        Result result = convert(lote, lote.getIdEmpresa(), archivoNotaRemision, numeros);
        if (!result.isSuccess()) {
            String resultadoBase64 = Base64.getEncoder().encodeToString(result.getResult().getBytes(StandardCharsets.UTF_8));

            LoteParam loteparam = new LoteParam();
            loteparam.setId(lote.getId());
            loteparam.setIdEmpresa(lote.getIdEmpresa());
            loteparam.setCodigoEstado("PROCESADO");
            loteparam.setNotificado(lote.getNotificado());
            loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
            loteparam.setResultado(resultadoBase64);

            facades.getLoteFacade().edit(loteparam, null);
            return;
        }

        NotaRemisionParam param = new NotaRemisionParam();
        param.setIdEmpresa(lote.getIdEmpresa());
        param.setIdUsuario(lote.getIdUsuario());
        param.setUploadedFileBytes(result.getResult().getBytes(StandardCharsets.UTF_8));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<NotaRemisionParam> notaRemisionList = NotaRemisionUtils.convertToList(facades, param);
        String inicio = String.format("TIMBRADO;NRO_INTERNO;NRO_NOTA_REMISION;FECHA;ID_NOTA_REMISION;ESTADO_RESULTADO");
        StringBuilder buffer = new StringBuilder();
        buffer.append(inicio).append("\r\n");
        for (NotaRemisionParam nr : notaRemisionList) {
            
            String key = numeros.entrySet()
                        .stream()
                        .filter(item -> item.getValue().equals(nr.getNroNotaRemision()))
                        .map(item -> item.getKey())
                        .findFirst().get();
            
            if (nr.tieneErrores()) {
                String error = String.format("%s;%s;%s;-;-;ERROR;", nr.getTimbrado(), key, "---");
                error = error.concat("%s| ");
                for (MensajePojo mp : nr.getErrores()) {
                    error = String.format(error, mp.getDescripcion());
                    error = error.concat("%s| ");
                }
                error = error.substring(0, error.length() - 5);
                error = error.concat(";;");
                buffer.append(error).append("\r\n");
            } else {

                try {
                    userTransaction.begin();
                    
                    NotaRemision notaRemision = facades
                            .getNotaRemisionFacade()
                            .create(nr, null);

                    if (notaRemision == null) {
                        String error = String.format("%s;%s;%s;-;-;ERROR;", nr.getTimbrado(), key, "---");
                        error = error.concat("| ");
                        for (MensajePojo mp : nr.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {
                        String ok = String.format("%s;%s;%s;%s;%s;OK", nr.getTimbrado(), key, notaRemision.getNroNotaRemision(), sdf.format(nr.getFecha()), notaRemision.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }

                    userTransaction.commit();
                } catch (Exception exception) {
                    try {
                        userTransaction.rollback();
                    } catch (Exception e) {
                        log.fatal("Error", e);
                    }
                    String error = String.format("%s;%s;%s;-;-;ERROR;", nr.getTimbrado(), key, "---");
                    error = error.concat("| ");
                    error = error.concat(exception.getMessage());
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                }
            }
        }

        String resultadoBase64 = Base64.getEncoder().encodeToString(buffer.toString().getBytes(StandardCharsets.UTF_8));

        LoteParam loteparam = new LoteParam();
        loteparam.setId(lote.getId());
        loteparam.setIdEmpresa(lote.getIdEmpresa());
        loteparam.setCodigoEstado("PROCESADO");
        loteparam.setNotificado(lote.getNotificado());
        loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
        loteparam.setResultado(resultadoBase64);

        facades.getLoteFacade().edit(loteparam, null);
    }

    public Result convert(Lote lote, Integer idEmpresa, LoteArchivo archivoNotaRemision, Map<String,String> numeros) {
        byte[] bytesArchivo = Base64.getDecoder().decode(archivoNotaRemision.getContenido().getBytes(StandardCharsets.UTF_8));
        try {
            String result = new String(bytesArchivo, StandardCharsets.UTF_8);
            InputStreamReader input = new InputStreamReader(new BOMInputStream(new ByteArrayInputStream(bytesArchivo)));
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            Map<String, Integer> talonarios = new HashMap<>();
            String line;
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                Pattern pattern = Pattern.compile(";|,");
                Matcher mather = pattern.matcher(line);

                if (mather.find() == true) {
                    lineas.add(line);
                }
            }
            for (int i = 0; i < lineas.size(); i++) {
                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    String[] stringArr = lineas.get(i).split(";|,");
                    String tipoLinea = stringArr.length <= 0 ? "" : stringArr[0];
                    String timbrado = null;
                    String nroNRemision = null;
                    if (stringArr.length < 3) {
                        continue;
                    }
                    switch (tipoLinea) {
                        case "1":
                            for (int k = 0; k < 3; k++) {
                                switch (k) {
                                    case 1:
                                        timbrado = (String) stringArr[k];
                                        break;
                                    case 2:
                                        nroNRemision = (String) stringArr[k];
                                        break;
                                }
                            }
                            if (nroNRemision != null && !nroNRemision.trim().isEmpty()) {
                                String[] splitt = nroNRemision.split("-");
                                TalonarioPojo talonarioPojo = getTalonario(splitt[0], idEmpresa, timbrado);
                                if (talonarioPojo != null) {
                                    String newNumber = "";

                                    String firstNumber = facades.getTalonarioFacade().sigNumNotaRemision(talonarioPojo);

                                    int n = Integer.parseInt(firstNumber.substring(8));

                                    String talonarioNumbers = talonarioPojo.getNroSucursal() + "-" + talonarioPojo.getNroPuntoVenta() + "-";

                                    if (!talonarios.containsKey(talonarioNumbers)) {
                                        talonarios.put(talonarioNumbers, n);
                                    } else {
                                        int sum = talonarios.get(talonarioNumbers);
                                        n = sum+1;
                                        talonarios.replace(talonarioNumbers, n);
                                    }

                                    String nro = "" + n;
                                    while (nro.length() < 7) {
                                        nro = "0" + nro;
                                    }
                                    newNumber = talonarioNumbers + nro;

                                    if (!numeros.containsKey(nroNRemision)) {
                                        numeros.put(nroNRemision, newNumber);
                                    }
                                    result = result.replace(nroNRemision, numeros.getOrDefault(nroNRemision, ""));
                                    break;
                                } else {
                                    return Result.builder()
                                            .success(false)
                                            .result("No se encuentra ningún talonario registrado para la sucursal (su_id) = " + splitt[0])
                                            .build();
                                }
                            } else {
                                return Result.builder()
                                        .success(false)
                                        .result("No todos los registros cuentan con el número de sucursal (su_id)")
                                        .build();
                            }
                    }
                }
            }
            return Result.builder()
                    .success(true)
                    .result(result)
                    .build();
        } catch (IOException e) {
            return Result.builder()
                    .success(true)
                    .result(e.getMessage())
                    .build();
        }
    }

    private TalonarioPojo getTalonario(String suId, Integer idEmpresa, String timbrado) {
        TalonarioParam t = new TalonarioParam();
        t.setIdEmpresa(idEmpresa);
        t.setActivo('S');
        t.setTimbrado(timbrado);
        t.setIdTipoDocumento(4);
        t.setCodigoInterno(Integer.parseInt(suId));
        TalonarioPojo talonarioPojo = facades.getTalonarioFacade().findFirstPojo(t);
        return talonarioPojo;
    }

    @Data
    @Builder
    private static class Result {
        private boolean success;
        private String result;
    }
}
