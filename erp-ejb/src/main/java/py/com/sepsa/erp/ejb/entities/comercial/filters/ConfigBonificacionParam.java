/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de parámetros de config bonificacion
 * @author Jonathan
 */
public class ConfigBonificacionParam extends CommonParam {

    public ConfigBonificacionParam() {
    }

    public ConfigBonificacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de contrato bonificador
     */
    @QueryParam("idContratoBonificador")
    private Integer idContratoBonificador;

    /**
     * Identificador de producto bonificador
     */
    @QueryParam("idProductoBonificador")
    private Integer idProductoBonificador;
    
    /**
     * Identificador de servicio bonificador
     */
    @QueryParam("idServicioBonificador")
    private Integer idServicioBonificador;
    
    /**
     * Identificador de contrato bonificado
     */
    @QueryParam("idContratoBonificado")
    private Integer idContratoBonificado;
    
    /**
     * Identificador de producto bonificado
     */
    @QueryParam("idProductoBonificado")
    private Integer idProductoBonificado;
    
    /**
     * Identificador de servicio bonificado;
     */
    @QueryParam("idServicioBonificado")
    private Integer idServicioBonificado;
    
    /**
     * Valor
     */
    @QueryParam("valor")
    private BigDecimal valor;
    
    /**
     * Porcentual
     */
    @QueryParam("porcentual")
    private Character porcentual;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idProductoBonificador)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto bonificador"));
        }
        
        if(isNull(idServicioBonificador)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio bonificador"));
        }
        
        if(isNull(idProductoBonificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto bonificado"));
        }
        
        if(isNull(idServicioBonificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio bonificado"));
        }
        
        if(isNull(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor"));
        }
        
        if(isNull(porcentual)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es porcentual"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idProductoBonificador)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto bonificador"));
        }
        
        if(isNull(idServicioBonificador)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio bonificador"));
        }
        
        if(isNull(idProductoBonificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto bonificado"));
        }
        
        if(isNull(idServicioBonificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio bonificado"));
        }
        
        if(isNull(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor"));
        }
        
        if(isNull(porcentual)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es porcentual"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdProductoBonificador() {
        return idProductoBonificador;
    }

    public void setIdProductoBonificador(Integer idProductoBonificador) {
        this.idProductoBonificador = idProductoBonificador;
    }

    public Integer getIdServicioBonificador() {
        return idServicioBonificador;
    }

    public void setIdServicioBonificador(Integer idServicioBonificador) {
        this.idServicioBonificador = idServicioBonificador;
    }

    public Integer getIdProductoBonificado() {
        return idProductoBonificado;
    }

    public void setIdProductoBonificado(Integer idProductoBonificado) {
        this.idProductoBonificado = idProductoBonificado;
    }

    public Integer getIdServicioBonificado() {
        return idServicioBonificado;
    }

    public void setIdServicioBonificado(Integer idServicioBonificado) {
        this.idServicioBonificado = idServicioBonificado;
    }

    public Integer getIdContratoBonificador() {
        return idContratoBonificador;
    }

    public void setIdContratoBonificador(Integer idContratoBonificador) {
        this.idContratoBonificador = idContratoBonificador;
    }

    public Integer getIdContratoBonificado() {
        return idContratoBonificado;
    }

    public void setIdContratoBonificado(Integer idContratoBonificado) {
        this.idContratoBonificado = idContratoBonificado;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
