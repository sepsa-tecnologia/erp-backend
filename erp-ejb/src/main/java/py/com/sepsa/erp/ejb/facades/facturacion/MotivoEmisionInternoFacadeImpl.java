/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmision;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.MotivoEmisionInternoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MotivoEmisionInternoFacade", mappedName = "MotivoEmisionInternoFacade")
@Local(MotivoEmisionInternoFacade.class)
public class MotivoEmisionInternoFacadeImpl extends FacadeImpl<MotivoEmisionInterno, MotivoEmisionInternoParam> implements MotivoEmisionInternoFacade {

    public MotivoEmisionInternoFacadeImpl() {
        super(MotivoEmisionInterno.class);
    }
    
    public Boolean validToCreate(MotivoEmisionInternoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MotivoEmision motivoEmision = facades.getMotivoEmisionFacade()
                .find(param.getIdMotivoEmision(), param.getCodigoMotivoEmision());
        
        if(isNull(motivoEmision)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de emisión"));
        } else {
            param.setIdMotivoEmision(motivoEmision.getId());
        }
        
        MotivoEmisionInternoParam param1 = new MotivoEmisionInternoParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigo(param.getCodigo().trim());
        
        MotivoEmisionInterno mei = findFirst(param1);
        
        if(!isNull(mei)) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(MotivoEmisionInternoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        MotivoEmisionInterno item = find(param.getId());
        
        if(isNull(item)) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
            
        } else if (!item.getCodigo().equals(param.getCodigo().trim())) {
            
            MotivoEmisionInternoParam param1 = new MotivoEmisionInternoParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setCodigo(param.getCodigo().trim());

            MotivoEmisionInterno mei = findFirst(param1);

            if(!isNull(mei)) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
            }
        }
        
        MotivoEmision motivoEmision = facades.getMotivoEmisionFacade()
                .find(param.getIdMotivoEmision(), param.getCodigoMotivoEmision());
        
        if(isNull(motivoEmision)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de emisión"));
        } else {
            param.setIdMotivoEmision(motivoEmision.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public MotivoEmisionInterno create(MotivoEmisionInternoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MotivoEmisionInterno result = new MotivoEmisionInterno();
        result.setIdEmpresa(param.getIdEmpresa());
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        result.setActivo(param.getActivo());
        result.setIdMotivoEmision(param.getIdMotivoEmision());
        
        create(result);
        
        return result;
    }

    @Override
    public MotivoEmisionInterno edit(MotivoEmisionInternoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MotivoEmisionInterno result = find(param.getId());
        result.setIdEmpresa(param.getIdEmpresa());
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        result.setActivo(param.getActivo());
        result.setIdMotivoEmision(param.getIdMotivoEmision());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public MotivoEmisionInterno find(Integer idEmpresa, Integer id, String codigo) {
        
        MotivoEmisionInternoParam param = new MotivoEmisionInternoParam();
        param.setIdEmpresa(idEmpresa);
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<MotivoEmisionInterno> find(MotivoEmisionInternoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MotivoEmisionInterno.class);
        Root<MotivoEmisionInterno> root = cq.from(MotivoEmisionInterno.class);
        Join<MotivoEmisionInterno, MotivoEmision> motivoEmision = root.join("motivoEmision");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdMotivoEmision() != null) {
            predList.add(qb.equal(motivoEmision.get("id"), param.getIdMotivoEmision()));
        }
        
        if (param.getCodigoMotivoEmision() != null && !param.getCodigoMotivoEmision().trim().isEmpty()) {
            predList.add(qb.equal(motivoEmision.get("codigo"), param.getCodigoMotivoEmision().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);
        
        cq.orderBy(qb.asc(root.get("activo")),
                qb.asc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<MotivoEmisionInterno> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MotivoEmisionInternoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<MotivoEmisionInterno> root = cq.from(MotivoEmisionInterno.class);
        Join<MotivoEmisionInterno, MotivoEmision> motivoEmision = root.join("motivoEmision");
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getIdMotivoEmision() != null) {
            predList.add(qb.equal(motivoEmision.get("id"), param.getIdMotivoEmision()));
        }
        
        if (param.getCodigoMotivoEmision() != null && !param.getCodigoMotivoEmision().trim().isEmpty()) {
            predList.add(qb.equal(motivoEmision.get("codigo"), param.getCodigoMotivoEmision().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
