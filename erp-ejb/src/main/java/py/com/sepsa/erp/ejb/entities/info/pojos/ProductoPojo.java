/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class ProductoPojo {

    public ProductoPojo(Integer id, Integer idEmpresa, String empresa,
            Integer idMarca, String marca, Integer idMetrica, String codigoMetrica,
            String metrica, String descripcion, String codigoGtin, String codigoInterno,
            Character activo, Date fechaInsercion, Integer porcentajeImpuesto,
            BigDecimal multiploUmv, BigDecimal umv, String cuentaContable,
            Integer idAuditoria, Integer idAuditoriaDetalle, Date fechaVencimientoLote, String nroLote,
            Integer idTipoProducto,String codigoTipoProducto,Integer idPadre) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idMarca = idMarca;
        this.marca = marca;
        this.idMetrica = idMetrica;
        this.codigoMetrica = codigoMetrica;
        this.metrica = metrica;
        this.descripcion = descripcion;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.activo = activo;
        this.fechaInsercion = fechaInsercion;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.multiploUmv = multiploUmv;
        this.umv = umv;
        this.cuentaContable = cuentaContable;
        this.idAuditoria = idAuditoria;
        this.idAuditoriaDetalle = idAuditoriaDetalle;
        this.fechaVencimientoLote = fechaVencimientoLote;
        this.nroLote = nroLote;
        this.idTipoProducto = idTipoProducto;
        this.codigoTipoProducto = codigoTipoProducto;
        this.idPadre = idPadre;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de marca
     */
    private Integer idMarca;
    
    /**
     * Marca
     */
    private String marca;
    
    /**
     * Identificador de métrica
     */
    private Integer idMetrica;
    
    /**
     * Metrica
     */
    private String metrica;
    
    /**
     * Código de metrica
     */
    private String codigoMetrica;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código GTIN
     */
    private String codigoGtin;
    
    /**
     * Código interno
     */
    private String codigoInterno;
    
    /**
     * Fecha inserción
     */
    private Character activo;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;
    
    /**
     * Porcentaje de impuesto
     */
    private Integer porcentajeImpuesto;
    
    /**
     * Múltimo de UMV
     */
    private BigDecimal multiploUmv;
    
    /**
     * UMV
     */
    private BigDecimal umv;
    
    /**
     * Cuenta contable
     */
    private String cuentaContable;
    
    /**
     * Identificador de auditoria
     */
    private Integer idAuditoria;
    
    /**
     * Identificador de detalle de auditoria
     */
    private Integer idAuditoriaDetalle;
    
    /**
     * Fecha de vencimiento de lote 
     */
    private Date fechaVencimientoLote;
    
    /**
     * Número de lote
     */
    private String nroLote;
    /**
     * Identificador  de producto
     */
    private Integer idTipoProducto;
    /**
     * Código tipo de producto
     */
    private String codigoTipoProducto;
    /**
     * Identificador del producto padre
     */
    private Integer idPadre;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(Integer porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public Integer getIdAuditoria() {
        return idAuditoria;
    }

    public void setIdAuditoria(Integer idAuditoria) {
        this.idAuditoria = idAuditoria;
    }

    public Integer getIdAuditoriaDetalle() {
        return idAuditoriaDetalle;
    }

    public void setIdAuditoriaDetalle(Integer idAuditoriaDetalle) {
        this.idAuditoriaDetalle = idAuditoriaDetalle;
    }

    public Integer getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Integer idMarca) {
        this.idMarca = idMarca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Integer idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getMetrica() {
        return metrica;
    }

    public void setMetrica(String metrica) {
        this.metrica = metrica;
    }

    public BigDecimal getMultiploUmv() {
        return multiploUmv;
    }

    public void setMultiploUmv(BigDecimal multiploUmv) {
        this.multiploUmv = multiploUmv;
    }

    public BigDecimal getUmv() {
        return umv;
    }

    public void setUmv(BigDecimal umv) {
        this.umv = umv;
    }

    public String getCuentaContable() {
        return cuentaContable;
    }

    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    }

    public Integer getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(Integer idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public String getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    public void setCodigoTipoProducto(String codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }
    
}
