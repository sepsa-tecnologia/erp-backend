/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCompraParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaCompraDetalleFacade", mappedName = "FacturaCompraDetalleFacade")
@Local(FacturaCompraDetalleFacade.class)
public class FacturaCompraDetalleFacadeImpl extends FacadeImpl<FacturaCompraDetalle, FacturaCompraDetalleParam> implements FacturaCompraDetalleFacade {

    public FacturaCompraDetalleFacadeImpl() {
        super(FacturaCompraDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdFactura validar identificador de factura
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(FacturaCompraDetalleParam param, boolean validarIdFactura) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        FacturaCompraParam fcparam = new FacturaCompraParam();
        fcparam.setId(param.getIdFacturaCompra());
        Long fcsize = facades.getFacturaCompraFacade().findSize(fcparam);
        
        if(validarIdFactura && fcsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura de compra"));
        }
        
        if(param.getIdProducto() != null) {
            
            ProductoParam pparam = new ProductoParam();
            pparam.setId(param.getIdProducto());
            Long psize = facades.getProductoFacade().findSize(pparam);
            
            if(psize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el producto"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public FacturaCompraDetalle create(FacturaCompraDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        FacturaCompraDetalle item = new FacturaCompraDetalle();
        item.setIdFacturaCompra(param.getIdFacturaCompra());
        item.setNroLinea(param.getNroLinea());
        item.setDescripcion(param.getDescripcion());
        item.setPorcentajeIva(param.getPorcentajeIva());
        item.setCantidadFacturada(param.getCantidadFacturada());
        item.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva().stripTrailingZeros());
        item.setPrecioUnitarioSinIva(param.getPrecioUnitarioSinIva().stripTrailingZeros());
        item.setMontoIva(param.getMontoIva().stripTrailingZeros());
        item.setMontoImponible(param.getMontoImponible().stripTrailingZeros());
        item.setMontoTotal(param.getMontoTotal().stripTrailingZeros());
        item.setIdProducto(param.getIdProducto());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<FacturaCompraDetalle> find(FacturaCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(FacturaCompraDetalle.class);
        Root<FacturaCompraDetalle> root = cq.from(FacturaCompraDetalle.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(root.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<FacturaCompraDetalle> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaCompraDetalle> root = cq.from(FacturaCompraDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(root.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
