/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de transaccion red
 * @author Jonathan D. Bernal Fernández
 */
public class TransaccionRedParam extends CommonParam {
    
    public TransaccionRedParam() {
    }

    public TransaccionRedParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de red
     */
    @QueryParam("idRed")
    private Integer idRed;
    
    /**
     * Identificador de operación
     */
    @QueryParam("idOperacion")
    private Integer idOperacion;
    
    /**
     * Identificador de mensaje
     */
    @QueryParam("idMensaje")
    private Integer idMensaje;
    
    /**
     * Identificador de transaccion
     */
    @QueryParam("idTransaccion")
    private String idTransaccion;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idRed)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de red de pago"));
        }
        
        if(isNull(idOperacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de operación"));
        }
        
        if(isNull(idMensaje)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de mensaje"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de transacción"));
        }
        
        if(isNullOrEmpty(idTransaccion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de transacción"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdRed() {
        return idRed;
    }

    public void setIdRed(Integer idRed) {
        this.idRed = idRed;
    }

    public Integer getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(Integer idOperacion) {
        this.idOperacion = idOperacion;
    }

    public Integer getIdMensaje() {
        return idMensaje;
    }

    public void setIdMensaje(Integer idMensaje) {
        this.idMensaje = idMensaje;
    }

    public String getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(String idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
