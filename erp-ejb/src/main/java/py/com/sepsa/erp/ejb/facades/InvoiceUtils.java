/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaDetallePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
public class InvoiceUtils {
    
    @EJB
    private Facades facades;
    
    public NotaCreditoParam generateNcFromPojo(NotaCreditoParam param,
            FacturaPojo invoice) {
        
        param.setIdMoneda(invoice.getIdMoneda());
        param.setIdCliente(invoice.getIdCliente());
        param.setIdNaturalezaCliente(invoice.getIdNaturalezaCliente());
        param.setRazonSocial(invoice.getRazonSocial());
        param.setRuc(invoice.getRuc());
        param.setEmail(invoice.getEmail());
        param.setTelefono(invoice.getTelefono());
        param.setFecha(Calendar.getInstance().getTime());
        param.setDireccion(invoice.getDireccion());
        param.setNroCasa(invoice.getNroCasa());
        param.setIdDepartamento(invoice.getIdDepartamento());
        param.setIdDistrito(invoice.getIdDistrito());
        param.setIdCiudad(invoice.getIdCiudad());
        param.setAnulado('N');
        param.setImpreso('N');
        param.setEntregado('N');
        param.setArchivoEdi(invoice.getArchivoEdi());
        param.setArchivoSet(invoice.getArchivoSet());
        param.setMontoIva5(invoice.getMontoIva5());
        param.setMontoImponible5(invoice.getMontoImponible5());
        param.setMontoTotal5(invoice.getMontoTotal5());
        param.setMontoIva10(invoice.getMontoIva10());
        param.setMontoImponible10(invoice.getMontoImponible10());
        param.setMontoTotal10(invoice.getMontoTotal10());
        param.setMontoTotalExento(invoice.getMontoTotalExento());
        param.setMontoIvaTotal(invoice.getMontoIvaTotal());
        param.setMontoImponibleTotal(invoice.getMontoImponibleTotal());
        param.setMontoTotalNotaCredito(invoice.getMontoTotalFactura());
        param.setMontoTotalGuaranies(invoice.getMontoTotalGuaranies());
        
        FacturaDetalleParam fdparam = new FacturaDetalleParam();
        fdparam.setIdFactura(invoice.getId());
        fdparam.setFirstResult(0);
        fdparam.setPageSize(1000);
        List<FacturaDetallePojo> list = facades
                .getFacturaDetalleFacade()
                .findPojo(fdparam);
        
        List<NotaCreditoDetalleParam> detalles = new ArrayList<>();
        
        for (FacturaDetallePojo item : list) {
            NotaCreditoDetalleParam detalle = new NotaCreditoDetalleParam();
            detalle.setNroLinea(item.getNroLinea());
            detalle.setNroNotaCredito(param.getNroNotaCredito());
            detalle.setIdFactura(invoice.getId());
            detalle.setPorcentajeIva(item.getPorcentajeIva());
            detalle.setCantidad(item.getCantidad());
            detalle.setDescripcion(item.getDescripcion());
            BigDecimal precioUnitarioConIVA = item.getPrecioUnitarioConIva().
                    subtract(item.getMontoDescuentoParticular()).
                    subtract(item.getMontoDescuentoGlobal());
            detalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            BigDecimal precioUnitarioSinIVA = calcularPUSinIVA(item.getPorcentajeIva(), precioUnitarioConIVA);
            detalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            detalle.setMontoIva(item.getMontoIva());
            detalle.setMontoImponible(item.getMontoImponible());
            detalle.setMontoTotal(item.getMontoTotal());
            detalle.setDescuentoParticularUnitario(BigDecimal.ZERO);
            detalle.setMontoDescuentoParticular(BigDecimal.ZERO);
            detalles.add(detalle);
        }
        
        param.setNotaCreditoDetalles(detalles);
        
        return param;
    }
    
    public NotaDebitoParam generateNdFromPojo(NotaDebitoParam param,
            FacturaPojo invoice) {
        
        param.setIdMoneda(invoice.getIdMoneda());
        param.setIdCliente(invoice.getIdCliente());
        param.setIdNaturalezaCliente(invoice.getIdNaturalezaCliente());
        param.setRazonSocial(invoice.getRazonSocial());
        param.setRuc(invoice.getRuc());
        param.setEmail(invoice.getEmail());
        param.setTelefono(invoice.getTelefono());
        param.setFecha(Calendar.getInstance().getTime());
        param.setDireccion(invoice.getDireccion());
        param.setNroCasa(invoice.getNroCasa());
        param.setIdDepartamento(invoice.getIdDepartamento());
        param.setIdDistrito(invoice.getIdDistrito());
        param.setIdCiudad(invoice.getIdCiudad());
        param.setAnulado('N');
        param.setImpreso('N');
        param.setEntregado('N');
        param.setArchivoEdi(invoice.getArchivoEdi());
        param.setArchivoSet(invoice.getArchivoSet());
        param.setMontoIva5(invoice.getMontoIva5());
        param.setMontoImponible5(invoice.getMontoImponible5());
        param.setMontoTotal5(invoice.getMontoTotal5());
        param.setMontoIva10(invoice.getMontoIva10());
        param.setMontoImponible10(invoice.getMontoImponible10());
        param.setMontoTotal10(invoice.getMontoTotal10());
        param.setMontoTotalExento(invoice.getMontoTotalExento());
        param.setMontoIvaTotal(invoice.getMontoIvaTotal());
        param.setMontoImponibleTotal(invoice.getMontoImponibleTotal());
        param.setMontoTotalNotaDebito(invoice.getMontoTotalFactura());
        param.setMontoTotalGuaranies(invoice.getMontoTotalGuaranies());
        
        FacturaDetalleParam fdparam = new FacturaDetalleParam();
        fdparam.setIdFactura(invoice.getId());
        fdparam.setFirstResult(0);
        fdparam.setPageSize(1000);
        List<FacturaDetallePojo> list = facades
                .getFacturaDetalleFacade()
                .findPojo(fdparam);
        
        List<NotaDebitoDetalleParam> detalles = new ArrayList<>();
        
        for (FacturaDetallePojo item : list) {
            NotaDebitoDetalleParam detalle = new NotaDebitoDetalleParam();
            detalle.setNroLinea(item.getNroLinea());
            detalle.setNroNotaDebito(param.getNroNotaDebito());
            detalle.setIdFactura(invoice.getId());
            detalle.setPorcentajeIva(item.getPorcentajeIva());
            detalle.setCantidad(item.getCantidad());
            detalle.setDescripcion(item.getDescripcion());
            BigDecimal precioUnitarioConIVA = item.getPrecioUnitarioConIva().
                    subtract(item.getMontoDescuentoParticular()).
                    subtract(item.getMontoDescuentoGlobal());
            detalle.setPrecioUnitarioConIva(precioUnitarioConIVA);
            BigDecimal precioUnitarioSinIVA = calcularPUSinIVA(item.getPorcentajeIva(), precioUnitarioConIVA);
            detalle.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
            detalle.setMontoIva(item.getMontoIva());
            detalle.setMontoImponible(item.getMontoImponible());
            detalle.setMontoTotal(item.getMontoTotal());
            detalle.setDescuentoParticularUnitario(BigDecimal.ZERO);
            detalle.setMontoDescuentoParticular(BigDecimal.ZERO);
            detalles.add(detalle);
        }
        
        param.setNotaDebitoDetalles(detalles);
        
        return param;
    }
    
    /**
     * Método para calcular el precio unitario sin IVA
     *
     * @param porcentaje Porcentaje de impuesto
     * @param puConIva Precio unitario con IVA
     * @return Precio unitario sin IVA
     */
    private BigDecimal calcularPUSinIVA(Integer porcentaje, BigDecimal puConIva) {
        BigDecimal precioUnitSinIVA = new BigDecimal("0");
        if (porcentaje == 0) {
            precioUnitSinIVA = puConIva;
        }
        if (porcentaje == 5) {
            BigDecimal ivaValueCinco = new BigDecimal("1.05");
            precioUnitSinIVA = puConIva.divide(ivaValueCinco, 5, RoundingMode.HALF_UP);
        }
        if (porcentaje == 10) {
            BigDecimal ivaValueDiez = new BigDecimal("1.1");
            precioUnitSinIVA = puConIva.divide(ivaValueDiez, 5, RoundingMode.HALF_UP);
        }
        return precioUnitSinIVA;
    }
}
