/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import py.com.sepsa.erp.ejb.facades.*;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import py.com.sepsa.dtecore.utils.Module11;
import py.com.sepsa.erp.ejb.entities.info.filters.RucParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.RucPojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.ConsultaRucAdapter;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
public class RucFacade {

    @EJB
    private Facades facades;

    public Boolean validToQuery(RucParam param) {
        if (!param.isValidToQuery()) {
            return Boolean.FALSE;
        }

        return !param.tieneErrores();
    }

    public RucPojo find(RucParam param) {

        if (!validToQuery(param)) {
            return null;
        }

        RucPojo result = ConsultaRucAdapter.consultarRuc(param.getRuc());

        if (result == null) {
            result = new RucPojo();
        }

        result.setRuc(param.getRuc());
        result.setDvRuc(Module11.calculate(param.getRuc()));

        return result;
    }
}
