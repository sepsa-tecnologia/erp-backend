package py.com.sepsa.erp.ejb.externalservice.adapters;

import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;

/**
 * Adaptador genérico
 * @author Daniel Escauriza
 * @param <T> Clase genérica
 * @param <U> Tipo de respuesta
 */
public abstract class AbstractAdapterImpl<T, U> {
    
    /**
     * Bandera indicando el estado de la petición 
     */
    private Boolean success = false;
    
    /**
     * Código asociado al estado de la petición 
     */
    private Integer code;
    
    /**
     * Fecha y hora de proceso
     */
    private Date stamp;
    
    /**
     * Mensaje asociado a la petición 
     */
    private String message;
    
    /**
     * Carga util de la petición
     */
    private T payload;
    
    /**
     * Respuesta HTTP del API
     */
    protected U resp;

    /**
     * Setea el estado de la petición 
     * @param success Estado de petición
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * Obtiene el estado de la petición 
     * @return Estado de petición
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * Setea el mensaje asociado a la solicitud 
     * @param message Mensaje de respuesta
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Obtiene el mensaje asociado a la solicitud 
     * @return Mensaje de respuesta
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setea el código asociado a la solicitud 
     * @param code Código
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Obtiene el código asociado a la solicitud 
     * @return Código
     */
    public Integer getCode() {
        return code;
    }
    
    /**
     * Setea la fecha y hora de proceso
     * @param stamp Fecha y hora de proceso
     */
    public void setStamp(Date stamp) {
        this.stamp = stamp;
    }

    /**
     * Obtiene la fecha y hora de proceso
     * @return Fecha y hora de proceso
     */
    public Date getStamp() {
        return stamp;
    }

    /**
     * Setea la carga util de la petición 
     * @param payload Carga util
     */
    public void setPayload(T payload) {
        this.payload = payload;
    }

    /**
     * Obtiene la carga util de la petición 
     * @return Carga util
     */
    public T getPayload() {
        return payload;
    }

    /**
     * Obtiene la respuesta HTTP
     * @return Respuesta HTTP
     */
    public U getResp() {
        return resp;
    }

    /**
     * Setea la respuesta HTTP
     * @param resp Respuesta HTTP
     */
    public void setResp(U resp) {
        this.resp = resp;
    }
    
    /**
     * Setea los valores de la respuesta 
     * @param resp Respuesta del servicio
     */
    public void setData(JsonObject resp) {
        this.success = resp.has("success")
                ? resp.get("success").getAsBoolean()
                : null;
        JsonObject status = resp.has("status")
                ? resp.get("status").getAsJsonObject()
                : null;
        this.message = status == null || !status.has("description")
                ? null
                : status.get("description").getAsString();
        String stampAux = status == null || !status.has("stamp")
                ? null
                : status.get("stamp").getAsString();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
            this.stamp = sdf.parse(stampAux);
        } catch(Exception ex) {}
    }
    
    /**
     * Metodo que procesa la respuesta del servicio
     * @param resultClass Clase de resultado
     * @param response Respuesta
     * @return Respuesta
     */
    protected Object processSoapResponse(String response, Class... resultClass) {
        Object result = null;
        try {
            
            SOAPMessage soapMessage = MessageFactory
                    .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL)
                    .createMessage(null, 
                            new ByteArrayInputStream(response.getBytes()));
            
            Unmarshaller unmarshaller = JAXBContext
                    .newInstance(resultClass)
                    .createUnmarshaller();
            
            result = unmarshaller.unmarshal(soapMessage
                    .getSOAPBody()
                    .extractContentAsDocument());
            
        } catch(Exception ex) {}
        
        return result;
    }
    
    /**
     * Método para realizar la peticion
     * @throws Exception 
     */
    public abstract void request() throws Throwable;

    /**
     * Constructor de AbstractAdapterImpl
     * @param response Instancia de Respuesta
     */
    public AbstractAdapterImpl(U response) {
        this.resp = response;
    }
}
