/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de liquidacion detalle
 * @author Jonathan D. Bernal Fernández
 */
public class LiquidacionDetalleParam extends CommonParam {
    
    public LiquidacionDetalleParam() {
    }

    public LiquidacionDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de historico liquidacion
     */
    @QueryParam("idHistoricoLiquidacion")
    private Integer idHistoricoLiquidacion;
    
    /**
     * Número de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;
    
    /**
     * Identificador de monto tarifa
     */
    @QueryParam("idMontoTarifa")
    private Integer idMontoTarifa;
    
    /**
     * Identificador de descuento
     */
    @QueryParam("idDescuento")
    private Integer idDescuento;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de contrato
     */
    @QueryParam("idContrato")
    private Integer idContrato;
    
    /**
     * Identificador de cliente relacionado
     */
    @QueryParam("idClienteRel")
    private Integer idClienteRel;
    
    /**
     * Monto tarifa
     */
    @QueryParam("montoTarifa")
    private BigDecimal montoTarifa;
    
    /**
     * Monto descuento
     */
    @QueryParam("montoDescuento")
    private BigDecimal montoDescuento;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Ano
     */
    @QueryParam("ano")
    private String ano;
    
    /**
     * Mes
     */
    @QueryParam("mes")
    private String mes;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    /**
     * Verifica si el objeto es válido para listar la lista de liquidaciones por periodo
     * @return Bandera
     */
    public Boolean isValidToListPeriodo() {
        
        limpiarErrores();
        
        if(isNull(mes)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el mes"));
        }
        
        if(isNull(ano)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el año"));
        }
        
        super.isValidToList();
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        limpiarErrores();
        
        if(isNull(idHistoricoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de "
                            + "historico liquidación"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el nro de linea"));
        }
        
        if(isNull(montoTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto de tarifa"));
        }
        
        if(isNull(montoDescuento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto de descuento"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto total"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = super.getCustomErrores();
        
        return result;
    }
    
    @Override
    public boolean isValidToEdit() {
        limpiarErrores();
        
        if(isNull(idHistoricoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el identificador de "
                            + "historico liquidación"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el nro de linea"));
        }
        
        if(isNull(montoTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto de tarifa"));
        }
        
        if(isNull(montoDescuento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto de descuento"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de indicar el monto total"));
        }
        
        return !tieneErrores();
    }
    
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public void setIdMontoTarifa(Integer idMontoTarifa) {
        this.idMontoTarifa = idMontoTarifa;
    }

    public Integer getIdMontoTarifa() {
        return idMontoTarifa;
    }

    public Integer getIdClienteRel() {
        return idClienteRel;
    }

    public void setIdClienteRel(Integer idClienteRel) {
        this.idClienteRel = idClienteRel;
    }
    
    public void setIdDescuento(Integer idDescuento) {
        this.idDescuento = idDescuento;
    }

    public Integer getIdDescuento() {
        return idDescuento;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public BigDecimal getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(BigDecimal montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public BigDecimal getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(BigDecimal montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }
}
