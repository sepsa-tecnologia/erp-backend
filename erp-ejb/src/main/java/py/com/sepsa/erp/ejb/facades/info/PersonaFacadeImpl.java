/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Direccion;
import py.com.sepsa.erp.ejb.entities.info.Email;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.TipoPersona;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaTelefonoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaFacade", mappedName = "PersonaFacade")
@Local(PersonaFacade.class)
public class PersonaFacadeImpl extends FacadeImpl<Persona, PersonaParam> implements PersonaFacade {
    
    public PersonaFacadeImpl() {
        super(Persona.class);
    }

    @Override
    public Boolean validToCreate(PersonaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoPersona tipoPersona = facades.getTipoPersonaFacade()
                .find(param.getIdTipoPersona(), param.getCodigoTipoPersona());
        
        if(tipoPersona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo persona"));
        } else {
            param.setIdTipoPersona(tipoPersona.getId());
            param.setCodigoTipoPersona(tipoPersona.getCodigo());
        }
        
        if(param.getDireccion() != null) {
            facades.getDireccionFacade().validToCreate(param.getDireccion());
        }
        
        if(param.getContacto() != null) {
            facades.getContactoFacade().validToCreate(param.getContacto(), true);
        }
        
        if(param.getEmail() != null) {
            facades.getEmailFacade().validToCreate(param.getEmail());
        }
        
        if(param.getTelefono() != null) {
            facades.getTelefonoFacade().validToCreate(param.getTelefono());
        }
        
        if(param.getPersonaEmails() != null) {
            for (PersonaEmailParam personaEmail : param.getPersonaEmails()) {
                facades.getPersonaEmailFacade().validToCreate(personaEmail, false);
            }
        }
        
        if(param.getPersonaTelefonos() != null) {
            for (PersonaTelefonoParam personaTelefono : param.getPersonaTelefonos()) {
                facades.getPersonaTelefonoFacade().validToCreate(personaTelefono, false);
            }
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(PersonaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Persona persona = find(param.getId());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }
        
        TipoPersona tipoPersona = facades.getTipoPersonaFacade()
                .find(param.getIdTipoPersona(), param.getCodigoTipoPersona());
        
        if(tipoPersona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el tipo persona"));
        } else {
            param.setIdTipoPersona(tipoPersona.getId());
            param.setCodigoTipoPersona(tipoPersona.getCodigo());
        }
        
        if(param.getIdDireccion() != null) {
            
            Direccion direccion = facades.getDireccionFacade().find(param.getIdDireccion());
            
            if(direccion == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe la dirección"));
            }
            
            param.setDireccion(null);
            
        } else if(param.getDireccion() != null) {
            facades.getDireccionFacade().validToCreate(param.getDireccion());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Persona create(PersonaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Integer idDireccion = param.getIdDireccion();
        
        if(param.getDireccion() != null) {
            Direccion direccion = facades.getDireccionFacade().create(param.getDireccion(), userInfo);
            idDireccion = direccion == null ? idDireccion : direccion.getId();
        }
        
        Persona item = new Persona();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setIdTipoPersona(param.getIdTipoPersona());
        item.setRazonSocial(param.getRazonSocial());
        item.setNombreFantasia(param.getNombreFantasia());
        item.setNombre(param.getNombre());
        item.setApellido(param.getApellido());
        item.setActivo(param.getActivo());
        item.setIdDireccion(idDireccion);
        create(item);
        
        if(param.getContacto() != null) {
            param.getContacto().setIdContacto(item.getId());
            facades.getContactoFacade().create(param.getContacto(), userInfo);
        }
        
        if(param.getEmail() != null) {
            
            Email email = facades.getEmailFacade().create(param.getEmail(), userInfo);
            
            if(email != null) {
                PersonaEmailParam param1 = new PersonaEmailParam();
                param1.setIdEmail(email.getId());
                param1.setIdPersona(item.getId());
                param1.setActivo('S');
                facades.getPersonaEmailFacade().create(param1, userInfo);
            }
        }
        
        if(param.getPersonaEmails() != null) {
            for (PersonaEmailParam personaEmail : param.getPersonaEmails()) {
                personaEmail.setIdPersona(item.getId());
                facades.getPersonaEmailFacade().create(personaEmail, userInfo);
            }
        }
        
        if(param.getTelefono() != null) {
            
            Telefono telefono = facades.getTelefonoFacade().create(param.getTelefono(), userInfo);
            
            if(telefono != null) {
                PersonaTelefonoParam param1 = new PersonaTelefonoParam();
                param1.setIdTelefono(telefono.getId());
                param1.setIdPersona(item.getId());
                param1.setActivo('S');
                facades.getPersonaTelefonoFacade().create(param1, userInfo);
            }
        }
        
        
        if(param.getPersonaTelefonos() != null) {
            for (PersonaTelefonoParam personaTelefono : param.getPersonaTelefonos()) {
                personaTelefono.setIdPersona(item.getId());
                facades.getPersonaTelefonoFacade().create(personaTelefono, userInfo);
            }
        }
        
        return item;
    }
    
    @Override
    public Persona edit(PersonaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Integer idDireccion = param.getIdDireccion();
        
        if(param.getDireccion() != null) {
            Direccion direccion = facades.getDireccionFacade().create(param.getDireccion(), userInfo);
            idDireccion = direccion == null ? idDireccion : direccion.getId();
        }
        
        Persona item = find(param.getId());
        item.setIdTipoPersona(param.getIdTipoPersona());
        item.setRazonSocial(param.getRazonSocial());
        item.setNombreFantasia(param.getNombreFantasia());
        item.setNombre(param.getNombre());
        item.setApellido(param.getApellido());
        item.setActivo(param.getActivo());
        item.setIdDireccion(idDireccion);
        edit(item);
        
        return item;
    }
    
    @Override
    public List<Persona> find(PersonaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Persona> root = cq.from(Persona.class);
        cq.select(root);
        cq.orderBy(qb.asc(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdTipoPersona() != null) {
            predList.add(qb.equal(root.get("idTipoPersona").get("id"), param.getIdTipoPersona()));
        }

        if (param.getIdDireccion() != null) {
            predList.add(qb.equal(root.get("idDireccion").get("id"), param.getIdDireccion()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getNombre() != null && !param.getNombre().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombre")), String.format("%%%s%%", param.getNombre().trim().toUpperCase())));
        }

        if (param.getApellido() != null && !param.getApellido().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("apellido")), String.format("%%%s%%", param.getApellido().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")), String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getNombreFantasia() != null && !param.getNombreFantasia().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombreFantasia")), String.format("%%%s%%", param.getNombreFantasia().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Persona> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(PersonaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Persona> root = cq.from(Persona.class);
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdTipoPersona() != null) {
            predList.add(qb.equal(root.get("idTipoPersona").get("id"), param.getIdTipoPersona()));
        }

        if (param.getIdDireccion() != null) {
            predList.add(qb.equal(root.get("idDireccion").get("id"), param.getIdDireccion()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getNombre() != null && !param.getNombre().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombre")), String.format("%%%s%%", param.getNombre().trim().toUpperCase())));
        }

        if (param.getApellido() != null && !param.getApellido().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("apellido")), String.format("%%%s%%", param.getApellido().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")), String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getNombreFantasia() != null && !param.getNombreFantasia().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombreFantasia")), String.format("%%%s%%", param.getNombreFantasia().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        Long result = ((Number) q.getSingleResult()).longValue();

        return result;
    }
}
