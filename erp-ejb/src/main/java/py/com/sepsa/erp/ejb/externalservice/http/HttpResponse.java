package py.com.sepsa.erp.ejb.externalservice.http;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;

/**
 * Pojo para la respuesta HTTP
 * @author Daniel F. Escauriza Arza
 * @param <T>
 */
@Log4j2
public class HttpResponse<T> {
    
    /**
     * Código de respuesta.
     */
    private ResponseCode respCode;
    
    /**
     * Lista de cabeceras.
     */
    private Map<String, List<String>> headers = new HashMap<>();
    
    /**
     * Datos de respuesta.
     */
    protected T payload;
    
    /**
     * Tipo de contenido de la respuesta.
     */
    private String contentType;

    /**
     * Obtiene el código de la respuesta
     * @return Código de la respuesta
     */
    public ResponseCode getRespCode() {
        return respCode;
    }
    
    /**
     * Setea el código de la respuesta
     * @param respCode Código de la respuesta
     */
    public void setRespCode(ResponseCode respCode) {
        this.respCode = respCode;
    }
    
    /**
     * Obtiene los datos de la respuesta
     * @return Datos de la respuesta
     */
    public T getPayload() {
        return payload;
    }
    
    /**
     * Setea los datos de la respuesta
     * @param payload Datos de la respuesta
     */
    public void setPayload(T payload) {
        this.payload = payload;
    }

    /**
     * Setea el tipo de contenido de respuesta
     * @param contentType Tipo de contenido de respuesta
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * Obtiene el tipo de contenido de respuesta
     * @return Tipo de contenido de respuesta
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Setea la lista de cabeceras
     * @param headers Lista de cabeceras
     */
    public void setHeaders(Map<String, List<String>> headers) {
        this.headers = headers;
    }

    /**
     * Obtiene la lista de cabeceras
     * @return Lista de cabeceras
     */
    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    /**
     * Constructor de HttpResponse
     * @param respCode Código de respuesta
     * @param payload Payload de datos de respuesta
     * @param contentType Tipo de contenido de respuesta
     */
    public HttpResponse(ResponseCode respCode, T payload, String contentType) {
        this.respCode = respCode;
        this.payload = payload;
        this.contentType = contentType;
    }

    /**
     * Constructor de HttpResponse
     * @param respCode Código de respuesta
     * @param contentType Tipo de contenido de respuesta
     */
    public HttpResponse(ResponseCode respCode, String contentType) {
        this.respCode = respCode;
        this.contentType = contentType;
    }

    /**
     * Constructor de HttpResponse
     * @param respCode Código de respuesta
     * @param contentType Tipo de contenido de respuesta
     * @param headers Lista de cabeceras
     */
    public HttpResponse(ResponseCode respCode, String contentType,
            Map<String, List<String>> headers) {
        this.respCode = respCode;
        this.contentType = contentType;
        this.headers = headers;
    }
    
    /**
     * Constructor de HttpResponse (por default se usa el código ERROR)
     */
    public HttpResponse() {
        this.respCode = ResponseCode.ERROR;
    }
    
}