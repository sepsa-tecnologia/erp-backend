/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.EntidadFinanciera;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.EntidadFinancieraParam;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "EntidadFinancieraFacade", mappedName = "EntidadFinancieraFacade")
@Local(EntidadFinancieraFacade.class)
public class EntidadFinancieraFacadeImpl extends FacadeImpl<EntidadFinanciera, EntidadFinancieraParam> implements EntidadFinancieraFacade {

    public EntidadFinancieraFacadeImpl() {
        super(EntidadFinanciera.class);
    }

    @Override
    public List<EntidadFinanciera> find(EntidadFinancieraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(EntidadFinanciera.class);
        Root<EntidadFinanciera> root = cq.from(EntidadFinanciera.class);
        Join<EntidadFinanciera, Persona> persona = root.join("entidadFinanciera");

        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(persona.get("id"), param.getId()));
        }

        if (param.getNombre() != null && !param.getNombre().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("nombre")), String.format("%%%s%%", param.getNombre().trim().toUpperCase())));
        }

        if (param.getApellido() != null && !param.getApellido().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("apellido")), String.format("%%%s%%", param.getApellido().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("razonSocial")), String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getNombreFantasia() != null && !param.getNombreFantasia().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("nombreFantasia")), String.format("%%%s%%", param.getNombreFantasia().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<EntidadFinanciera> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(EntidadFinancieraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<EntidadFinanciera> root = cq.from(EntidadFinanciera.class);
        Join<EntidadFinanciera, Persona> persona = root.join("entidadFinanciera");

        cq.select(qb.count(persona.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(persona.get("id"), param.getId()));
        }

        if (param.getNombre() != null && !param.getNombre().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("nombre")), String.format("%%%s%%", param.getNombre().trim().toUpperCase())));
        }

        if (param.getApellido() != null && !param.getApellido().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("apellido")), String.format("%%%s%%", param.getApellido().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("razonSocial")), String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getNombreFantasia() != null && !param.getNombreFantasia().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(persona.<String>get("nombreFantasia")), String.format("%%%s%%", param.getNombreFantasia().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
