/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.Devolucion;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DevolucionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface DevolucionFacade extends Facade<Devolucion, DevolucionParam, UserInfoImpl> {
    
}
