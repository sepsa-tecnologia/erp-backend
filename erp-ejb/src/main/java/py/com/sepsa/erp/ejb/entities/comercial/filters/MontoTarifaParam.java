/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.math.BigDecimal;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de monto tarifa
 * @author Jonathan D. Bernal Fernández
 */
public class MontoTarifaParam extends TarifaParam {
    
    public MontoTarifaParam() {
    }

    public MontoTarifaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tarifa
     */
    @QueryParam("idTarifa")
    private Integer idTarifa;
    
    /**
     * Tarifa
     */
    @QueryParam("tarifa")
    private String tarifa;
    
    /**
     * Cantidad inicial
     */
    @QueryParam("cantidadInicial")
    private Integer cantidadInicial;
    
    /**
     * Cantidad inicial desde
     */
    @QueryParam("cantidadInicialDesde")
    private Integer cantidadInicialDesde;
    
    /**
     * Cantidad inicial hasta
     */
    @QueryParam("cantidadInicialHasta")
    private Integer cantidadInicialHasta;
    
    /**
     * Cantidad final
     */
    @QueryParam("cantidadFinal")
    private Integer cantidadFinal;
    
    /**
     * Cantidad final desde
     */
    @QueryParam("cantidadFinalDesde")
    private Integer cantidadFinalDesde;
    
    /**
     * Cantidad final hasta
     */
    @QueryParam("cantidadFinalHasta")
    private Integer cantidadFinalHasta;
    
    /**
     * Monto
     */
    @QueryParam("monto")
    private BigDecimal monto;
    
    /**
     * Estado tarifa
     */
    @QueryParam("estadoTarifa")
    private Character estadoTarifa;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tarifa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(monto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tarifa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(monto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Integer getIdTarifa() {
        return idTarifa;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setEstadoTarifa(Character estadoTarifa) {
        this.estadoTarifa = estadoTarifa;
    }

    public Character getEstadoTarifa() {
        return estadoTarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public String getTarifa() {
        return tarifa;
    }

    public Integer getCantidadInicialDesde() {
        return cantidadInicialDesde;
    }

    public void setCantidadInicialDesde(Integer cantidadInicialDesde) {
        this.cantidadInicialDesde = cantidadInicialDesde;
    }

    public Integer getCantidadInicialHasta() {
        return cantidadInicialHasta;
    }

    public void setCantidadInicialHasta(Integer cantidadInicialHasta) {
        this.cantidadInicialHasta = cantidadInicialHasta;
    }

    public Integer getCantidadFinalDesde() {
        return cantidadFinalDesde;
    }

    public void setCantidadFinalDesde(Integer cantidadFinalDesde) {
        this.cantidadFinalDesde = cantidadFinalDesde;
    }

    public Integer getCantidadFinalHasta() {
        return cantidadFinalHasta;
    }

    public void setCantidadFinalHasta(Integer cantidadFinalHasta) {
        this.cantidadFinalHasta = cantidadFinalHasta;
    }
}
