package py.com.sepsa.erp.ejb.externalservice.adapters;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;
import static py.com.sepsa.erp.ejb.externalservice.http.ResponseCode.OK;
import py.com.sepsa.erp.ejb.externalservice.notificacion.ContentType;
import py.com.sepsa.erp.ejb.externalservice.notificacion.Event;
import py.com.sepsa.erp.ejb.externalservice.notificacion.Message;
import py.com.sepsa.erp.ejb.externalservice.notificacion.Notification;
import py.com.sepsa.erp.ejb.externalservice.notificacion.NotificationType;
import py.com.sepsa.erp.ejb.externalservice.notificacion.Product;
import py.com.sepsa.erp.ejb.externalservice.service.APINotificacionServer;

/**
 *
 * @author Jonathan D. Bernal Fernandez
 */
public class EnviarMensajeAdapter extends AbstractAdapterImpl<Message, HttpStringResponse> {

    /**
     * Tipo de contenido
     */
    private ContentTypeEnum contentType;
    
    /**
     * Destino
     */
    private String para;
    
    /**
     * Código de tipo de notificación
     */
    private String codigoTipoNotificacion;
    
    /**
     * Mensaje
     */
    private String mensaje;
    
    /**
     * Código Producto
     */
    private String codigoProducto;
    
    /**
     * Código Evento
     */
    private String codigoEvento;
    
    /**
     * Código de agrupación
     */
    private String codigoAgrupacion;

    /**
     * Realiza una petición al servicio
     *
     */
    @Override
    public void request() {
        
        APINotificacionServer api = new APINotificacionServer();
        
        JsonElement json = createJson();
        
            
        this.resp = api.crearMensaje(json);

        JsonObject payload;

        switch(resp.getRespCode()) {
            case OK:
                payload = new JsonParser().parse(resp.getPayload()).getAsJsonObject();
                setData(payload);
                return;
        }
    }
    
    /**
     * Crea el JSON petición 
     * @return JSON
     */
    private JsonElement createJson() {
        
        ContentType ct = new ContentType();
        ct.setCode(contentType.name());
        
        Product p = new Product();
        p.setCode(codigoProducto);
        
        Event e = new Event();
        e.setCode(codigoEvento);
        
        NotificationType nt = new NotificationType();
        nt.setCode(codigoTipoNotificacion);
        
        Notification n = new Notification();
        n.setNotificationType(nt);
        
        Message message = new Message();
        message.setContentType(ct);
        message.setProduct(p);
        message.setEvent(e);
        message.setNotification(n);
        message.setMessage(mensaje);
        message.setTo(para);
        message.setGroupCode(codigoAgrupacion);
        
        Gson gson = new Gson();
        
        JsonElement result = gson.toJsonTree(message, Message.class);
        
        return result;
    }
    
    /**
     * Constructor
     * @param contenType Content Type
     * @param para Destino
     * @param codigoTipoNotificacion Código de tipo de notificación
     * @param mensaje Mensaje
     * @param codigoProducto Código Producto
     * @param codigoEvento Código Evento
     * @param codigoAgrupacion Código de agrupación
     */
    public EnviarMensajeAdapter(ContentTypeEnum contenType, String para,
            String codigoTipoNotificacion, String mensaje,
            String codigoProducto, String codigoEvento,
            String codigoAgrupacion) {
        super(new HttpStringResponse());
        
        this.contentType = contenType;
        this.para = para;
        this.codigoTipoNotificacion = codigoTipoNotificacion;
        this.mensaje = mensaje;
        this.codigoProducto = codigoProducto;
        this.codigoEvento = codigoEvento;
        this.codigoAgrupacion = codigoAgrupacion;
    }
    
    public enum ContentTypeEnum {
        JSON
    }

    public ContentTypeEnum getContentType() {
        return contentType;
    }

    public void setContentType(ContentTypeEnum contentType) {
        this.contentType = contentType;
    }

    public String getPara() {
        return para;
    }

    public void setPara(String para) {
        this.para = para;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getCodigoEvento() {
        return codigoEvento;
    }

    public void setCodigoEvento(String codigoEvento) {
        this.codigoEvento = codigoEvento;
    }

    public String getCodigoAgrupacion() {
        return codigoAgrupacion;
    }

    public void setCodigoAgrupacion(String codigoAgrupacion) {
        this.codigoAgrupacion = codigoAgrupacion;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }
}
