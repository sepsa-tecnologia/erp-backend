/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los motivos de descuento
 * @author Jonathan D. Bernal Fernández
 */
public class MotivoDescuentoParam extends CommonParam {
    
    public MotivoDescuentoParam() {
    }

    public MotivoDescuentoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Rango desde
     */
    @QueryParam("rangoDesde")
    private BigDecimal rangoDesde;
    
    /**
     * Rango hasta
     */
    @QueryParam("rangoHasta")
    private BigDecimal rangoHasta;
    
    /**
     * Porcentual
     */
    @QueryParam("porcentual")
    private Character porcentual;
    
    /**
     * Identificador de tipo de etiqueta
     */
    @QueryParam("idTipoEtiqueta")
    private Integer idTipoEtiqueta;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoEtiqueta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de etiqueta"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(porcentual)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el motivo de descuento es porcentual"));
        }
        
        if(isNull(rangoDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el rango hasta"));
        }
        
        if(isNull(rangoHasta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el rango desde"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idTipoEtiqueta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de etiqueta"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(porcentual)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el motivo de descuento es porcentual"));
        }
        
        if(isNull(rangoDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el rango hasta"));
        }
        
        if(isNull(rangoHasta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el rango desde"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        return !tieneErrores();
    }
    
    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    public BigDecimal getRangoDesde() {
        return rangoDesde;
    }

    public void setRangoDesde(BigDecimal rangoDesde) {
        this.rangoDesde = rangoDesde;
    }

    public BigDecimal getRangoHasta() {
        return rangoHasta;
    }

    public void setRangoHasta(BigDecimal rangoHasta) {
        this.rangoHasta = rangoHasta;
    }

    public Integer getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(Integer idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
