/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class MotivoPojo {
    
    public MotivoPojo(Integer id, String descripcion, String codigo, Character activo) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.activo = activo;
    }
    
    public MotivoPojo(Integer id, String descripcion, String codigo,
            Integer idTipoMotivo, String tipoMotivo, String codigoTipoMotivo,
            Character activo, Integer idEmpresa, String empresa,
            String codigoEmpresa) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.idTipoMotivo = idTipoMotivo;
        this.tipoMotivo = tipoMotivo;
        this.codigoTipoMotivo = codigoTipoMotivo;
        this.activo = activo;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.codigoEmpresa = codigoEmpresa;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código
     */
    private String codigo;
    
    /**
     * Identificador de tipo de motivo
     */
    private Integer idTipoMotivo;
    
    /**
     * Tipo motivo
     */
    private String tipoMotivo;
    
    /**
     * Código de tipo motivo
     */
    private String codigoTipoMotivo;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Código de empresa
     */
    private String codigoEmpresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdTipoMotivo() {
        return idTipoMotivo;
    }

    public void setIdTipoMotivo(Integer idTipoMotivo) {
        this.idTipoMotivo = idTipoMotivo;
    }

    public String getTipoMotivo() {
        return tipoMotivo;
    }

    public void setTipoMotivo(String tipoMotivo) {
        this.tipoMotivo = tipoMotivo;
    }

    public String getCodigoTipoMotivo() {
        return codigoTipoMotivo;
    }

    public void setCodigoTipoMotivo(String codigoTipoMotivo) {
        this.codigoTipoMotivo = codigoTipoMotivo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }
}
