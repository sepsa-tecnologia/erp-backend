/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.Excedente;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ExcedenteParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ExcedenteFacade", mappedName = "ExcedenteFacade")
@Local(ExcedenteFacade.class)
public class ExcedenteFacadeImpl extends FacadeImpl<Excedente, ExcedenteParam> implements ExcedenteFacade {

    public ExcedenteFacadeImpl() {
        super(Excedente.class);
    }
    
    @Override
    public List<Excedente> find(ExcedenteParam param) {
        
        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and e.id = %d", where, param.getId());
        }
        
        if(param.getIdTipoDocumento() != null) {
            where = String.format("%s and e.id_tipo_documento = %d", where, param.getIdTipoDocumento());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and e.id_servicio = %d", where, param.getIdServicio());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and e.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select e.*"
                + " from facturacion.excedente e"
                + " where %s order by e.estado offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Excedente.class);
        
        List<Excedente> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ExcedenteParam param) {
        
        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and e.id = %d", where, param.getId());
        }
        
        if(param.getIdTipoDocumento() != null) {
            where = String.format("%s and e.id_tipo_documento = %d", where, param.getIdTipoDocumento());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and e.id_servicio = %d", where, param.getIdServicio());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and e.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(e.id)"
                + " from facturacion.excedente e"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
