/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de monto minimo
 * @author Jonathan D. Bernal Fernández
 */
public class MontoMinimoParam extends CommonParam {
    
    public MontoMinimoParam() {
    }

    public MontoMinimoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Cliente
     */
    @QueryParam("cliente")
    private String cliente;
    
    /**
     * Monto
     */
    @QueryParam("monto")
    private BigDecimal monto;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(monto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(monto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCliente() {
        return cliente;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
