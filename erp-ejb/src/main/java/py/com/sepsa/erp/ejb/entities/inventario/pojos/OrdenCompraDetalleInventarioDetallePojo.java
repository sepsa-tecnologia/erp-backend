/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class OrdenCompraDetalleInventarioDetallePojo {

    public OrdenCompraDetalleInventarioDetallePojo() {
    }

    public OrdenCompraDetalleInventarioDetallePojo(Integer id,
            Integer idOrdenCompraDetalle, Integer idInventarioDetalle,
            Integer cantidad, Integer idEstadoInventario,
            String codigoEstadoInventario, String estadoInventario,
            Integer idProducto, Integer idDepositoLogistico, Date fechaVencimiento) {
        this.id = id;
        this.idOrdenCompraDetalle = idOrdenCompraDetalle;
        this.idInventarioDetalle = idInventarioDetalle;
        this.cantidad = cantidad;
        this.idEstadoInventario = idEstadoInventario;
        this.codigoEstadoInventario = codigoEstadoInventario;
        this.estadoInventario = estadoInventario;
        this.idProducto = idProducto;
        this.idDepositoLogistico = idDepositoLogistico;
        this.fechaVencimiento = fechaVencimiento;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de orden de compra detalle
     */
    private Integer idOrdenCompraDetalle;
    
    /**
     * Identificador de inventario detalle
     */
    private Integer idInventarioDetalle;
    
    /**
     * Cantidad
     */
    private Integer cantidad;
    
    /**
     * Identificador de estado de inventario
     */
    private Integer idEstadoInventario;
    
    /**
     * Código de estado inventario
     */
    private String codigoEstadoInventario;
    
    /**
     * Estado inventario
     */
    private String estadoInventario;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
    
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdOrdenCompraDetalle() {
        return idOrdenCompraDetalle;
    }

    public void setIdOrdenCompraDetalle(Integer idOrdenCompraDetalle) {
        this.idOrdenCompraDetalle = idOrdenCompraDetalle;
    }

    public Integer getIdInventarioDetalle() {
        return idInventarioDetalle;
    }

    public void setIdInventarioDetalle(Integer idInventarioDetalle) {
        this.idInventarioDetalle = idInventarioDetalle;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setEstadoInventario(String estadoInventario) {
        this.estadoInventario = estadoInventario;
    }

    public String getEstadoInventario() {
        return estadoInventario;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }
}
