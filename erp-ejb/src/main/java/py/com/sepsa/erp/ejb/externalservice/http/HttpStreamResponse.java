package py.com.sepsa.erp.ejb.externalservice.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;

/**
 * Respuesta HTTP en formato Stream
 * @author Daniel F. Escauriza Arza
 */
@Log4j2
public class HttpStreamResponse extends HttpResponse<byte[]> {
    
    /**
     * Crea una instancia de StringResponse
     * @param httpConn Conexión HTTP
     * @return Respuesta HTTP
     */
    public static HttpStreamResponse createInstance(HttpURLConnection httpConn) {
        
        //Respuesta genérica en caso de falla
        HttpStreamResponse response = new HttpStreamResponse();
        
        try {
            
            if(httpConn != null) {
                
                ResponseCode code = ResponseCode.OK;
                
                log.info(String.format("Código: %s ", httpConn.getResponseCode()));
                
                if(httpConn.getResponseCode() == 401) {
                    code = ResponseCode.UNAUTHORIZED;
                } else if(httpConn.getResponseCode() == 403) {
                    code = ResponseCode.FORBIDDEN;
                } else if(httpConn.getResponseCode() != 200) {
                    code = ResponseCode.ERROR;
                }
                
                if(httpConn.getInputStream() != null) {
                    
                    response = new HttpStreamResponse(httpConn.getInputStream(), 
                            code, httpConn.getContentType(),
                            httpConn.getHeaderFields());
                }

                httpConn.disconnect();
            }
            
        } catch(IOException ex) {
            log.fatal("Error", ex);
        }
        
        return response;
    }
    
    /**
     * Constructor de StringResponse
     * @param input Entrada de la respuesta
     * @param code Código de respuesta
     * @param contentType Tipo de contenido de respuesta
     */
    public HttpStreamResponse(InputStream input, ResponseCode code,
            String contentType) {
        
        super(code, null, contentType);
        
        try {
            
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = input.read(buffer);

            while (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);
                bytesRead = input.read(buffer);
            }

            output.flush();
            this.payload = output.toByteArray();
            
        } catch (IOException ex) {
            log.fatal("Error", ex);
        }
        
    }
    
    /**
     * Constructor de StringResponse
     * @param input Entrada de la respuesta
     * @param code Código de respuesta
     * @param contentType Tipo de contenido de respuesta
     * @param headers Lista de cabeceras
     */
    public HttpStreamResponse(InputStream input, ResponseCode code,
            String contentType, Map<String, List<String>> headers) {
        
        super(code, contentType, headers);
        
        try {
            
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = input.read(buffer);

            while (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);
                bytesRead = input.read(buffer);
            }

            output.flush();
            this.payload = output.toByteArray();
            
        } catch (IOException ex) {
            log.fatal("Error", ex);
        }
        
    }

    /**
     * Constructor de HttpStringResponse
     * @param payload Datos de entrada
     * @param contentType Tipo de contenido
     */
    public HttpStreamResponse(byte[] payload, String contentType) {
        super(ResponseCode.OK, payload, contentType);
    }

    /**
     * Constructor de HttpStringResponse
     * @param respCode Código de Respuesta
     * @param contentType Tipo de contenido
     */
    public HttpStreamResponse(ResponseCode respCode, String contentType) {
        super(respCode, null, contentType);
    }
    
    /**
     * Constructor de HttpStringResponse
     */
    public HttpStreamResponse() {
        super();
    }
    
}
