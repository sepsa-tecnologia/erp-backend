/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de parámetros de reporte
 *
 * @author Jonathan
 */
public class ReporteParam extends CommonParam {

    public ReporteParam() {
    }

    public ReporteParam(String bruto) {
        super(bruto);
    }

    /**
     * Código tipo reporte
     */
    @QueryParam("codigoTipoReporte")
    private String codigoTipoReporte;

    /**
     * Formato
     */
    @QueryParam("formato")
    private String formato;

    /**
     * Parámetros
     */
    private JsonElement parametros;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

        if (isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }

        if (isNullOrEmpty(codigoTipoReporte)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de tipo de reporte"));
        }

        if (isNullOrEmpty(formato)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el formato"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        if (isNull(parametros)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el objeto de parámetros"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

        if (isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        if (isNull(parametros)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el objeto de parámetros"));
        }

        return !tieneErrores();
    }

    public void setParametros(JsonElement parametros) {
        this.parametros = parametros;
    }

    public JsonElement getParametros() {
        return parametros;
    }

    public void setCodigoTipoReporte(String codigoTipoReporte) {
        this.codigoTipoReporte = codigoTipoReporte;
    }

    public String getCodigoTipoReporte() {
        return codigoTipoReporte;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getFormato() {
        return formato;
    }

}
