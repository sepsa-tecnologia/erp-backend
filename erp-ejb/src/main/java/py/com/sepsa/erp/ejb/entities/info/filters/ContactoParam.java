/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contacto email
 *
 * @author Jonathan
 */
public class ContactoParam extends CommonParam {

    public ContactoParam() {
    }

    public ContactoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de contacto
     */
    @QueryParam("idContacto")
    private Integer idContacto;

    /**
     * Identificador de tipo de cargo
     */
    @QueryParam("idTipoContacto")
    private Integer idTipoContacto;

    /**
     * Identificador de cargo
     */
    @QueryParam("idCargo")
    private Integer idCargo;

    /**
     * Prefijo numero
     */
    @QueryParam("prefijoNumero")
    private String prefijoNumero;

    private PersonaParam persona;

    private EmailParam email;

    private TelefonoParam telefono;
    
    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idContacto) && isNull(persona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de contacto o persona de contacto"));
        }

        if (isNull(idTipoContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de contacto"));
        }

        if (isNull(idCargo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de cargo"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si el contacto esta activo"));
        }

        if(!isNull(persona)) {
            persona.setIdEmpresa(idEmpresa);
            persona.isValidToCreate();
        }
        
        if (!isNull(email)) {
            email.setIdEmpresa(idEmpresa);
            email.isValidToCreate();
        }

        if (!isNull(telefono)) {
            telefono.setIdEmpresa(idEmpresa);
            telefono.isValidToCreate();
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(idContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de contacto"));
        }

        if (isNull(idTipoContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de contacto"));
        }

        if (isNull(idCargo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de cargo"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si el contacto esta activo"));
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(email)) {
            if(email.tieneErrores()) {
                list.addAll(email.getErrores());
            }
        }
        
        if(!isNull(telefono)) {
            if(telefono.tieneErrores()) {
                list.addAll(telefono.getErrores());
            }
        }
        
        if(!isNull(persona)) {
            if(persona.tieneErrores()) {
                list.addAll(persona.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public void setEmail(EmailParam email) {
        this.email = email;
    }

    public EmailParam getEmail() {
        return email;
    }

    public void setTelefono(TelefonoParam telefono) {
        this.telefono = telefono;
    }

    public TelefonoParam getTelefono() {
        return telefono;
    }

    public void setPersona(PersonaParam persona) {
        this.persona = persona;
    }

    public PersonaParam getPersona() {
        return persona;
    }

    public void setPrefijoNumero(String prefijoNumero) {
        this.prefijoNumero = prefijoNumero;
    }

    public String getPrefijoNumero() {
        return prefijoNumero;
    }
}
