/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.ClienteServicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteServicioParam;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ClienteServicioPojo;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ReporteClienteServicioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ClienteServicioFacade extends Facade<ClienteServicio, ClienteServicioParam, UserInfoImpl> {

    public List<ReporteClienteServicioPojo> findReporte(ClienteServicioParam param);
    
    public Long findReporteSize(ClienteServicioParam param);

    public List<ClienteServicioPojo> asociado(Integer idCliente, Integer idProducto, Integer idServicio);
    
}
