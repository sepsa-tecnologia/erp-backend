/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de retención compra detalle
 * @author Jonathan D. Bernal Fernández
 */
public class RetencionCompraDetalleParam extends CommonParam {
    
    public RetencionCompraDetalleParam() {
    }

    public RetencionCompraDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de retención de compra
     */
    @QueryParam("idRetencionCompra")
    private Integer idRetencionCompra;
    
    /**
     * Identificador de factura de factura
     */
    @QueryParam("idFacturaCompra")
    private Integer idFacturaCompra;
    
    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;
    
    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;
    
    /**
     * Monto retenido
     */
    @QueryParam("montoRetenido")
    private BigDecimal montoRetenido;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idRetencionCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de retención de compra"));
        }
        
        if(isNull(idFacturaCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura de compra"));
        }
        
        if(isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }
        
        if(isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if(isNull(montoRetenido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto retenido"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdRetencionCompra() {
        return idRetencionCompra;
    }

    public void setIdRetencionCompra(Integer idRetencionCompra) {
        this.idRetencionCompra = idRetencionCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getMontoRetenido() {
        return montoRetenido;
    }

    public void setMontoRetenido(BigDecimal montoRetenido) {
        this.montoRetenido = montoRetenido;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
