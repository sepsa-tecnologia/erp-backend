/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de procesamiento archivo detalle
 *
 * @author Jonathan
 */
public class ProcesamientoArchivoParam extends CommonParam {

    public ProcesamientoArchivoParam() {
    }

    public ProcesamientoArchivoParam(String bruto) {
        super(bruto);
    }

    /**
     * Nombre de archivo
     */
    @QueryParam("nombreArchivo")
    private String nombreArchivo;

    /**
     * Tamaño del archivo
     */
    @QueryParam("tamano")
    private Integer tamano;

    /**
     * Hash
     */
    @QueryParam("hash")
    private String hash;

    /**
     * Identificador de tipo de archivo
     */
    @QueryParam("idTipoArchivo")
    private Integer idTipoArchivo;

    /**
     * Código de tipo de archivo
     */
    @QueryParam("codigoTipoArchivo")
    private String codigoTipoArchivo;

    /**
     * Ruta archivo erroneo
     */
    @QueryParam("rutaArchivoErroneo")
    private String rutaArchivoErroneo;

    /**
     * Nro de documento
     */
    @QueryParam("nroDocumento")
    private String nroDocumento;

    /**
     * Confirmado
     */
    @QueryParam("confirmado")
    private Character confirmado;

    /**
     * Reenviado
     */
    @QueryParam("reenviado")
    private Character reenviado;

    /**
     * Identificador de tipo de documento
     */
    @QueryParam("idTipoDocumento")
    private Integer idTipoDocumento;

    /**
     * Código de tipo de documento
     */
    @QueryParam("codigoTipoDocumento")
    private String codigoTipoDocumento;

    /**
     * Identificador de local origen
     */
    @QueryParam("idLocalOrigen")
    private Integer idLocalOrigen;

    /**
     * Identificador de local destino
     */
    @QueryParam("idLocalDestino")
    private Integer idLocalDestino;

    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Lista de detalle de procesamiento de archivo
     */
    private List<ProcesamientoArchivoDetalleParam> procesamientoArchivoDetalles;
    
    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNullOrEmpty(nombreArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre de archivo"));
        }

        if (isNull(tamano)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tamaño del archivo"));
        }
        
        if (isNullOrEmpty(hash)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el hash del archivo"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código del estado"));
        }

        if (isNull(idTipoArchivo) && isNullOrEmpty(codigoTipoArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código del tipo de archivo"));
        }

        if (isNull(confirmado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta confirmado"));
        }

        if(!isNull(procesamientoArchivoDetalles)) {
            for (ProcesamientoArchivoDetalleParam procesamientoArchivoDetalle : procesamientoArchivoDetalles) {
                procesamientoArchivoDetalle.setIdProcesamientoArchivo(0);
                procesamientoArchivoDetalle.setIdEmpresa(idEmpresa);
                procesamientoArchivoDetalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento de archivo"));
        }

        if (isNullOrEmpty(nombreArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre de archivo"));
        }
        
        if (isNull(tamano)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tamaño del archivo"));
        }

        if (isNullOrEmpty(hash)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el hash del archivo"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código del estado"));
        }

        if (isNull(idTipoArchivo) && isNullOrEmpty(codigoTipoArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código del tipo de archivo"));
        }

        if (isNull(confirmado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta confirmado"));
        }

        if (isNull(reenviado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento ya fue reenviado"));
        }

        if (isNull(idTipoDocumento) && isNullOrEmpty(codigoTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de documento"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToResend() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento de archivo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(procesamientoArchivoDetalles)) {
            for (ProcesamientoArchivoDetalleParam procesamientoArchivoDetalle : procesamientoArchivoDetalles) {
                list.addAll(procesamientoArchivoDetalle.getErrores());
            }
        }
        
        return list;
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public String getCodigoTipoArchivo() {
        return codigoTipoArchivo;
    }

    public void setCodigoTipoArchivo(String codigoTipoArchivo) {
        this.codigoTipoArchivo = codigoTipoArchivo;
    }

    public String getRutaArchivoErroneo() {
        return rutaArchivoErroneo;
    }

    public void setRutaArchivoErroneo(String rutaArchivoErroneo) {
        this.rutaArchivoErroneo = rutaArchivoErroneo;
    }

    public Character getConfirmado() {
        return confirmado;
    }

    public void setConfirmado(Character confirmado) {
        this.confirmado = confirmado;
    }

    public Character getReenviado() {
        return reenviado;
    }

    public void setReenviado(Character reenviado) {
        this.reenviado = reenviado;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public void setProcesamientoArchivoDetalles(List<ProcesamientoArchivoDetalleParam> procesamientoArchivoDetalles) {
        this.procesamientoArchivoDetalles = procesamientoArchivoDetalles;
    }

    public List<ProcesamientoArchivoDetalleParam> getProcesamientoArchivoDetalles() {
        return procesamientoArchivoDetalles;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setTamano(Integer tamano) {
        this.tamano = tamano;
    }

    public Integer getTamano() {
        return tamano;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }
}
