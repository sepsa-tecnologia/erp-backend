/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Jonathan
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RucPojo {

    /**
     * RUC
     */
    private String ruc;

    /**
     * DV RUV
     */
    private String dvRuc;

    /**
     * Razón social sugerida
     */
    private String razonSocialSugerida;

    /**
     * Es persona jurídica
     */
    private Boolean esPersonaJuridica;

    /**
     * Es entidad pública
     */
    private Boolean esEntidadPublica;
}
