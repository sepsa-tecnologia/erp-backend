/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaDncp;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDncpParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaDncpPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaDncpFacade", mappedName = "FacturaDncpFacade")
@Local(FacturaDncpFacade.class)
public class FacturaDncpFacadeImpl extends FacadeImpl<FacturaDncp, FacturaDncpParam> implements FacturaDncpFacade {

    public FacturaDncpFacadeImpl() {
        super(FacturaDncp.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdFactura validar identificador de factura
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(FacturaDncpParam param, boolean validarIdFactura) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        FacturaParam param1 = new FacturaParam();
        param1.setId(param.getIdFactura());
        Long size = facades.getFacturaFacade().findSize(param1);
        
        if(validarIdFactura && size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public FacturaDncp create(FacturaDncpParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        FacturaDncp item = new FacturaDncp();
        item.setIdFactura(param.getIdFactura());
        item.setModalidad(param.getModalidad());
        item.setEntidad(param.getEntidad());
        item.setAnho(param.getAnho());
        item.setSecuencia(param.getSecuencia());
        item.setFechaEmision(param.getFechaEmision());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<FacturaDncpPojo> findPojo(FacturaDncpParam param) {

        AbstractFind find = new AbstractFind(FacturaDncpPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("idFactura"),
                        getPath("root").get("modalidad"),
                        getPath("root").get("secuencia"),
                        getPath("root").get("anho"),
                        getPath("root").get("entidad"),
                        getPath("root").get("fechaEmision"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<FacturaDncp> find(FacturaDncpParam param) {

        AbstractFind find = new AbstractFind(FacturaDncp.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, FacturaDncpParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<FacturaDncp> root = cq.from(FacturaDncp.class);
        
        find.addPath("root", root);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaDncpParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaDncp> root = cq.from(FacturaDncp.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
