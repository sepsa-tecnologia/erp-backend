/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros del reporte de clientes en mora
 * @author Jonathan D. Bernal Fernández
 */
public class ReporteClienteMoraParam extends CommonParam {
    
    public ReporteClienteMoraParam() {
    }

    public ReporteClienteMoraParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Tipo de reporte
     */
    @QueryParam("tipoReporte")
    private String tipoReporte;
    
    /**
     * Resumen
     */
    @QueryParam("resumen")
    private Boolean resumen;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToList() {
        
        limpiarErrores();
        
        super.isValidToList();
        
        if(isNull(fechaHasta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha hasta"));
        }
        
        if(isNull(resumen)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el reporte es de resumen o no"));
        }
        
        if(isNullOrEmpty(tipoReporte)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de reporte"));
        } else {
            switch(tipoReporte) {
                case "MORA":
                case "CLIENTE":
                case "COMERCIAL":
                    break;
                    
                default:
                    addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar un tipo de reporte válido"));
                    break;
            }
        }
        
        return !tieneErrores();
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public void setTipoReporte(String tipoReporte) {
        this.tipoReporte = tipoReporte;
    }

    public String getTipoReporte() {
        return tipoReporte;
    }

    public void setResumen(Boolean resumen) {
        this.resumen = resumen;
    }

    public Boolean getResumen() {
        return resumen;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToCreate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
