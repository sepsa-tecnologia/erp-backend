/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de producto
 * @author Jonathan D. Bernal Fernández
 */
public class ProductoComParam extends CommonParam {
    
    public ProductoComParam() {
    }

    public ProductoComParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Bonificable
     */
    @QueryParam("bonificable")
    private Character bonificable;
    
    /**
     * Facturable
     */
    @QueryParam("facturable")
    private Character facturable;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * URL
     */
    @QueryParam("url")
    private String url;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(bonificable)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el producto es bonificable"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Character getBonificable() {
        return bonificable;
    }

    public void setBonificable(Character bonificable) {
        this.bonificable = bonificable;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setFacturable(Character facturable) {
        this.facturable = facturable;
    }

    public Character getFacturable() {
        return facturable;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
