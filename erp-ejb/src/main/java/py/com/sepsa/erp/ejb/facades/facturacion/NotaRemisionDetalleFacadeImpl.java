/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemision;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemisionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaRemisionDetallePojo;
import py.com.sepsa.erp.ejb.entities.info.Metrica;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NotaRemisionDetalleFacade", mappedName = "NotaRemisionDetalleFacade")
@Local(NotaRemisionDetalleFacade.class)
public class NotaRemisionDetalleFacadeImpl extends FacadeImpl<NotaRemisionDetalle, NotaRemisionDetalleParam> implements NotaRemisionDetalleFacade {

    public NotaRemisionDetalleFacadeImpl() {
        super(NotaRemisionDetalle.class);
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @param validarIdNc validar id nc
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(NotaRemisionDetalleParam param, boolean validarIdNr) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        NotaRemision nr = facades
                .getNotaRemisionFacade()
                .find(param.getIdNotaRemision());

        if (validarIdNr && nr == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de remision"));
        }

        if (param.getIdProducto() != null) {

            Producto producto = facades.getProductoFacade().find(param.getIdProducto());

            if (producto == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el producto"));
            }
        }

        return !param.tieneErrores();
    }

    /**
     * Crea una instancia de NotaCreditoDetalle
     *
     * @param param parámetros
     * @param userInfo Usuario
     * @return Instancia
     */
    @Override
    public NotaRemisionDetalle create(NotaRemisionDetalleParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        NotaRemisionDetalle nrd = new NotaRemisionDetalle();
        nrd.setIdNotaRemision(param.getIdNotaRemision());
        nrd.setNroLinea(param.getNroLinea());
        nrd.setDescripcion(param.getDescripcion());
        nrd.setCantidad(param.getCantidad());
        nrd.setIdProducto(param.getIdProducto());
        
        create(nrd);
        
        return nrd;
    }

    @Override
    public List<NotaRemisionDetallePojo> findPojo(NotaRemisionDetalleParam param) {

        AbstractFind find = new AbstractFind(NotaRemisionDetallePojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idNotaRemision"),
                        getPath("root").get("nroLinea"),
                        getPath("root").get("cantidad"),
                        getPath("root").get("descripcion"),
                        getPath("producto").get("id"),
                        getPath("producto").get("idMetrica"),
                        getPath("producto").get("metrica").get("codigo"),
                        getPath("producto").get("fechaVencimientoLote"),
                        getPath("producto").get("nroLote"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<NotaRemisionDetalle> find(NotaRemisionDetalleParam param) {

        AbstractFind find = new AbstractFind(NotaRemisionDetalle.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, NotaRemisionDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<NotaRemisionDetalle> root = cq.from(NotaRemisionDetalle.class);
        Join<NotaRemisionDetalle, NotaRemision> nr = root.join("notaRemision", JoinType.LEFT);
        Join<NotaRemisionDetalle, Producto> producto = root.join("producto", JoinType.LEFT);
        Join<Producto, Metrica> metrica = producto.join("metrica", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("nr", nr);
        find.addPath("producto", producto);
        find.addPath("metrica", metrica);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdNotaRemision() != null) {
            predList.add(qb.equal(root.get("idNotaRemision"), param.getIdNotaRemision()));
        }

        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }

      


        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(nr.get("fecha")),
                qb.asc(nr.get("id")),
                qb.asc(root.get("nroLinea")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaRemisionDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaRemisionDetalle> root = cq.from(NotaRemisionDetalle.class);
        Join<NotaRemisionDetalle, NotaRemision> nr = root.join("notaRemision", JoinType.LEFT);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdNotaRemision() != null) {
            predList.add(qb.equal(root.get("idNotaRemision"), param.getIdNotaRemision()));
        }

        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
