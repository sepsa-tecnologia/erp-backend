/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de tipo etiqueta
 * @author Jonathan
 */
public class ProcesoParam extends CommonParam {

    public ProcesoParam() {
    }

    public ProcesoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de proceso
     */
    @QueryParam("idTipoProceso")
    private Integer idTipoProceso;
    
    /**
     * Código de tipo de proceso
     */
    @QueryParam("codigoTipoProceso")
    private String codigoTipoProceso;
    
    /**
     * Identificador de frecuencia ejecución
     */
    @QueryParam("idFrecuenciaEjecucion")
    private Integer idFrecuenciaEjecucion;
    
    /**
     * Frecuencia de ejecución
     */
    private FrecuenciaEjecucionParam frecuenciaEjecucion;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(idTipoProceso) && isNullOrEmpty(codigoTipoProceso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo proceso"));
        }
        
        if(isNull(frecuenciaEjecucion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la frecuencia de ejecución"));
        } else {
            frecuenciaEjecucion.isValidToCreate();
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de proceso"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(idTipoProceso) && isNullOrEmpty(codigoTipoProceso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo proceso"));
        }
        
        if(isNull(idFrecuenciaEjecucion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de frecuencia de ejecución"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(frecuenciaEjecucion)) {
            list.addAll(frecuenciaEjecucion.getErrores());
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoProceso() {
        return idTipoProceso;
    }

    public void setIdTipoProceso(Integer idTipoProceso) {
        this.idTipoProceso = idTipoProceso;
    }

    public String getCodigoTipoProceso() {
        return codigoTipoProceso;
    }

    public void setCodigoTipoProceso(String codigoTipoProceso) {
        this.codigoTipoProceso = codigoTipoProceso;
    }

    public Integer getIdFrecuenciaEjecucion() {
        return idFrecuenciaEjecucion;
    }

    public void setIdFrecuenciaEjecucion(Integer idFrecuenciaEjecucion) {
        this.idFrecuenciaEjecucion = idFrecuenciaEjecucion;
    }

    public FrecuenciaEjecucionParam getFrecuenciaEjecucion() {
        return frecuenciaEjecucion;
    }

    public void setFrecuenciaEjecucion(FrecuenciaEjecucionParam frecuenciaEjecucion) {
        this.frecuenciaEjecucion = frecuenciaEjecucion;
    }
}
