/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.ControlInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.ControlInventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ControlInventarioDetalleFacade extends Facade<ControlInventarioDetalle, ControlInventarioDetalleParam, UserInfoImpl> {

    public Boolean validToCreate(ControlInventarioDetalleParam param, boolean validarControlInventario);

    public List<ControlInventarioDetalle> findByIdControlInventario(Integer idControlInventario);

    public List<ControlInventarioDetallePojo> findByIdControlInventarioPojo(Integer idControlInventario);

}
