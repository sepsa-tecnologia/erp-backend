/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 * POJO para la entidad contacto telefono
 * @author Jonathan
 */
public class PersonaTelefonoPojo {

    public PersonaTelefonoPojo() {
    }

    public PersonaTelefonoPojo(Integer idContacto, String contacto, Integer idTelefono, String prefijo, String numero) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idTelefono = idTelefono;
        this.prefijo = prefijo;
        this.numero = numero;
    }
    
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    
    /**
     * Persona
     */
    private String persona;
    
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    
    /**
     * Contacto
     */
    private String contacto;
    
    /**
     * Identificador de telefono
     */
    private Integer idTelefono;
    
    /**
     * Prefijo
     */
    private String prefijo;
    
    /**
     * Numero
     */
    private String numero;
    
    /**
     * Identificador de tipo de contacto
     */
    private Integer idTipoContacto;
    
    /**
     * Tipo contacto
     */
    private String tipoContacto;
    
    /**
     * Identificador de cargo
     */
    private Integer idCargo;
    
    /**
     * Cargo
     */
    private String cargo;
    
    /**
     * Estado
     */
    private Character estado;

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }
}
