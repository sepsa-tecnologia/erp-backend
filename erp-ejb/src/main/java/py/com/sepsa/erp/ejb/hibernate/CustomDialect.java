/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.hibernate;

import java.util.List;
import org.hibernate.dialect.PostgreSQL95Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import py.com.sepsa.erp.ejb.hibernate.types.JsonType;
import py.com.sepsa.erp.ejb.hibernate.types.StringArrayType;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class CustomDialect extends PostgreSQL95Dialect {

    public CustomDialect() {
        // Workaround, because "string_agg" function does not seem to be defined in the PostgreSQL dialect.
        registerFunction("string_agg", new StandardSQLFunction("string_agg", StandardBasicTypes.STRING));
        registerFunction("string_agg_distinct", new StandardSQLFunction("string_agg", StandardBasicTypes.STRING) {
            @Override
            public String render(Type firstArgumentType, List arguments, SessionFactoryImplementor sessionFactory) {
                final StringBuilder buf = new StringBuilder();
                buf.append(getRenderedName(arguments)).append('(');
                for (int i = 0; i < arguments.size(); i++) {
                    if(i == 0) {
                        buf.append("distinct ");
                    }
                    buf.append(arguments.get(i));
                    if (i < arguments.size() - 1) {
                        buf.append(", ");
                    }
                }
                return buf.append(')').toString();
            }
        });
        registerHibernateType(2003, StringArrayType.class.getName());
        registerHibernateType(1111, JsonType.class.getName());
    }

}
