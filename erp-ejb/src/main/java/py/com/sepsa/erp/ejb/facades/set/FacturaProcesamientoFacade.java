/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.set.FacturaProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.FacturaProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface FacturaProcesamientoFacade extends Facade<FacturaProcesamiento, FacturaProcesamientoParam, UserInfoImpl> {
    
}
