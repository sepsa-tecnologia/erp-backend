/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.auditoria.TipoAuditoria;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.TipoAuditoriaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "TipoAuditoriaFacade", mappedName = "TipoAuditoriaFacade")
@Local(TipoAuditoriaFacade.class)
public class TipoAuditoriaFacadeImpl extends FacadeImpl<TipoAuditoria, TipoAuditoriaParam> implements TipoAuditoriaFacade {

    public TipoAuditoriaFacadeImpl() {
        super(TipoAuditoria.class);
    }

    public Boolean validToCreate(TipoAuditoriaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoAuditoriaParam param1 = new TipoAuditoriaParam();
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(TipoAuditoriaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoAuditoria tipoAuditoria = find(param.getId());
        
        if(tipoAuditoria == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de auditoria"));
        } else if(!tipoAuditoria.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoAuditoriaParam param1 = new TipoAuditoriaParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public TipoAuditoria create(TipoAuditoriaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoAuditoria result = new TipoAuditoria();
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        create(result);
        
        return result;
    }
    
    @Override
    public TipoAuditoria edit(TipoAuditoriaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoAuditoria result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        edit(result);
        
        return result;
    }

    @Override
    public TipoAuditoria find(Integer id, String codigo) {
        TipoAuditoriaParam param = new TipoAuditoriaParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoAuditoria> find(TipoAuditoriaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoAuditoria.class);
        Root<TipoAuditoria> root = cq.from(TipoAuditoria.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<TipoAuditoria> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoAuditoriaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoAuditoria> root = cq.from(TipoAuditoria.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
