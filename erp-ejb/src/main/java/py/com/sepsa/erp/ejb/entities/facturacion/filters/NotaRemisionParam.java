/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import org.jboss.resteasy.annotations.providers.multipart.PartType;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de parámetros de Nota Remisión
 * @author Williams Vera
 */
public class NotaRemisionParam extends CommonParam{
    
    public NotaRemisionParam(){
        this.notaRemisionDetalles = new ArrayList<>();
    }
    
    public NotaRemisionParam(String bruto){
        super(bruto);
    }
    
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;
    
    @QueryParam("idNaturalezaCliente")
    private Integer idNaturalezaCliente;
    
    @QueryParam("codigoNaturalezaCliente")
    private String codigoNaturalezaCliente;
    
    @QueryParam("idTalonario")
    private Integer idTalonario;
    
    @QueryParam("nroNotaRemision")
    private String nroNotaRemision;
    
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha Hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Fecha insercion desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha insercion Hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    @QueryParam("razonSocial")
    private String razonSocial;
    
    @QueryParam("ruc")
    private String ruc;
    
    @QueryParam("anulado")
    private Character anulado;
    
    @QueryParam("archivoSet")
    private Character archivoSet;
    
    @QueryParam("generadoSet")
    private Character generadoSet;
    
    @QueryParam("digital")
    private Character digital;
    
    @QueryParam("estadoSincronizado")
    private Character estadoSincronizado;
    
    @QueryParam("observacion")
    private String observacion;
    
    @QueryParam("cdc")
    private String cdc;
    
    @QueryParam("codSeguridad")
    private String codSeguridad;
    
    @QueryParam("direccion")
    private String direccion;
    
    @QueryParam("nroCasa")
    private String nroCasa;
    
    @QueryParam("idDepartamento")
    private Integer idDepartamento;
    
    @QueryParam("idDistrito")
    private Integer idDistrito;
    
    @QueryParam("idCiudad")
    private Integer idCiudad;
    
    @QueryParam("serie")
    private String serie;
    
    @QueryParam("idProcesamiento")
    private Integer idProcesamiento;
    
    @QueryParam("idMotivoEmisionNr")
    private Integer idMotivoEmisionNr;
    
    @QueryParam("descripcionMotivoEmisionNr")
    private String descripcionMotivoEmisionNr;
    
    @QueryParam("km")
    private BigDecimal km;
    
    @QueryParam("tipoTransporte")
    private Integer tipoTransporte;
    
    @QueryParam("modalidadTransporte")
    private Integer modalidadTransporte;
    
    @QueryParam("responsableCostoFlete")
    private Integer responsableCostoFlete;
    
    @QueryParam("nroDespachoImportacion")
    private String nroDespachoImportacion;
    
    @QueryParam("fechaInicioTraslado")
    private Date fechaInicioTraslado;
    
    @QueryParam("fechaFinTraslado")
    private Date fechaFinTraslado;
    
    @QueryParam("fechaFuturaEmision")
    private Date fechaFuturaEmision;
    
    @QueryParam("responsableEmision")
    private Integer responsableEmision;
    
    @QueryParam("cdcFactura")
    private String cdcFactura;
    
    @QueryParam("timbradoFactura")
    private String timbradoFactura;
    
    @QueryParam("nroFactura")
    private String nroFactura;
    
    @QueryParam("fechaFactura")
    private Date fechaFactura;
    
    @QueryParam("tieneFactura")
    private Character tieneFactura;
    

    @QueryParam("imprimirOriginal")
    private Boolean imprimirOriginal;
    

    @QueryParam("imprimirDuplicado")
    private Boolean imprimirDuplicado;
    

    @QueryParam("imprimirTriplicado")
    private Boolean imprimirTriplicado;
    
        /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;
    
    /**
     * Código de motivo anulación
     */
    @QueryParam("codigoMotivoAnulacion")
    private String codigoMotivoAnulacion;

    /**
     * Motivo anulación
     */
    @QueryParam("motivoAnulacion")
    private String motivoAnulacion;
    
    /**
     * Observacion anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;
    /**
     * Ignorar periodo de anulación 
     */
    @QueryParam("ignorarPeriodoAnulacion")
    private Boolean ignorarPeriodoAnulacion;
    /**
     * Bandera para obtener kude del siediApi
     */
    @QueryParam("siediApi")
    private Boolean siediApi = Boolean.FALSE;
    
    @FormParam("uploadedFile")
    @PartType("application/octet-stream")
    private byte[] uploadedFileBytes;
    
    /**
     * Timbrado Nota Remisión
     */
    @QueryParam("timbrado")
    private String timbrado;
    
    private List<NotaRemisionDetalleParam> notaRemisionDetalles;
    
    private LocalEntregaParam localEntrega;
    
    private LocalSalidaParam localSalida;
    
    private VehiculoTrasladoParam vehiculoTraslado;
    
    private TransportistaParam transportista;
    
    private Boolean nroNotaCreditoDescendente = Boolean.TRUE;
    
    private Boolean idDescendente = Boolean.TRUE;
    
    /**
     * Bandera de formato ticket 
     */
    @QueryParam("ticket")
    private Boolean ticket = Boolean.FALSE;

    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTalonario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de talonario"));
        }
        
        
        if(isNull(idNaturalezaCliente) && isNullOrEmpty(codigoNaturalezaCliente)) {
            codigoNaturalezaCliente = "CONTRIBUYENTE";
        }
        
//        if(isNull(idCliente)) {
//            addError(MensajePojo.createInstance()
//                    .descripcion("Se debe indicar el identificador de cliente"));
//        }
        
        if(isNull(idMotivoEmisionNr)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de motivo de emision"));
        } else {
            if(idMotivoEmisionNr == 5){
                if(isNull(fechaFuturaEmision)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Para el motivo seleccionado, debe ingresar la fecha de emisión futura"));
                }
            }
            
            if (idMotivoEmisionNr == 5) {
                if (isNullOrEmpty(nroDespachoImportacion)) {
                     addError(MensajePojo.createInstance()
                    .descripcion("Cuando el motivo es Importación se debe indicar el número de despacho de importación"));
                }
            }
            
            if(idMotivoEmisionNr == 99) {
                if (isNullOrEmpty(descripcionMotivoEmisionNr)){
                    addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción del motivo de emisión"));
                }
            }
        }
        
        if (isNull(responsableEmision)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el responsable de la emisión de la nota de remisión"));
        }
        
        if (isNull(responsableCostoFlete)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el responsable del costo del flete"));
        }
        
        if(isNull(km)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la distancia reccorrida en Km"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de la nota de remisión"));
        } else {
            
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            
            if(cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la nota de remision no puede ser posterior a la fecha actual"));
            }
        }
        
        if(isNull(fechaInicioTraslado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha estimada de inicio de traslado"));
        }
        
        if(isNull(fechaFinTraslado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha estimada de fin de traslado"));
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de anulado"));
        }
        
        if(isNull(archivoSet)) {
            archivoSet = 'N';
        }
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de remisión es digital o no"));
        } else {
            estadoSincronizado = digital.equals('S') && archivoSet.equals('S') ? 'N' : 'S';
        }
        
        if(!isNull(digital) && isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            if(digital.equals('S')) {
                codigoEstado = "PENDIENTE";
            } else {
                codigoEstado = "APROBADO";
            }
        }
       
        if(isNull(tieneFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de remisión tiene asociada una factura o no"));
        }
        
        if(isNullOrEmpty(nroNotaRemision)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de la nota de remisión"));
        }
        
        if(isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del cliente"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el ruc/ci"));
        }
        
        if(isNullOrEmpty(observacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar alguna observación"));
        }
        
        if(isNull(tipoTransporte)){
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de transporte"));
        }
        
        if(isNull(modalidadTransporte)){
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la modalidad de transporte"));
        }
        
        if(isNullOrEmpty(notaRemisionDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la NR"));
        } else {
            for (NotaRemisionDetalleParam detalle : notaRemisionDetalles) {
                detalle.setIdNotaRemision(0);
                detalle.setCantidad(detalle.getCantidad());
                detalle.setIdProducto(detalle.getIdProducto());
                detalle.setNroLinea(detalle.getNroLinea());
               if(!detalle.isValidToCreate()){
                   for(MensajePojo mp : detalle.getErrores()){
                       addError(mp);
                   }
                   
               }
            }
        }
        
        if (isNull(localEntrega)){
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los datos del local de entrega"));
        } else {
            if (!localEntrega.isValidToCreate()){
                 for(MensajePojo mp : localEntrega.getErrores()){
                       addError(mp);
                   }
            }
        }
        
        if (isNull(localSalida)){
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los datos del local de salida"));
        } else {
            if (!localSalida.isValidToCreate()){
                 for(MensajePojo mp : localSalida.getErrores()){
                       addError(mp);
                   }
            }
        }
        
        if (isNull(vehiculoTraslado)){
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los datos del vehículo de traslado"));
        } else {
            if (isNull(vehiculoTraslado.getId())){
                vehiculoTraslado.setIdEmpresa(this.idEmpresa);
            }
            if (!vehiculoTraslado.isValidToCreate()){
                 for(MensajePojo mp : vehiculoTraslado.getErrores()){
                       addError(mp);
                   }
            }
        }
        
        if (isNull(transportista)){
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los datos del transportista"));
        } else {
            if (isNull(transportista.getId())){
                transportista.setIdEmpresa(this.idEmpresa);
            }
            if (!transportista.isValidToCreate()){
                 for(MensajePojo mp : transportista.getErrores()){
                       addError(mp);
                   }
            }
        }            
             
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {

        List<MensajePojo> result = new ArrayList<>();

        if (!isNullOrEmpty(notaRemisionDetalles)) {
            for (NotaRemisionDetalleParam detalle : notaRemisionDetalles) {
                result.addAll(detalle.getErrores());
            }
        }

        return result;
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de remisión"));
        }
        
        if(isNull(archivoSet)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo SET"));
        }
        
        if(isNull(generadoSet)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el archivo SET esta generado"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToGetPdf() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito"));
        }
        
        if(imprimirOriginal == null) {
            imprimirOriginal = Boolean.TRUE;
        }
        
        if(imprimirDuplicado == null) {
            imprimirDuplicado = Boolean.TRUE;
        }
        
        if(imprimirTriplicado == null) {
            imprimirTriplicado = Boolean.TRUE;
        }
        
        if(!imprimirOriginal
                && !imprimirDuplicado
                && !imprimirTriplicado) {
            imprimirOriginal = Boolean.TRUE;
        }
        
        if (ticket == null) {
            ticket = Boolean.FALSE;
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCancel() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de remisión"));
        }
        
        if(isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identiifcador de motivo de anulación"));
        }
        
        return !tieneErrores();
    }
        
        
    
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public String getCodigoNaturalezaCliente() {
        return codigoNaturalezaCliente;
    }

    public void setCodigoNaturalezaCliente(String codigoNaturalezaCliente) {
        this.codigoNaturalezaCliente = codigoNaturalezaCliente;
    }
    
    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdMotivoEmisionNr() {
        return idMotivoEmisionNr;
    }

    public void setIdMotivoEmisionNr(Integer idMotivoEmisionNr) {
        this.idMotivoEmisionNr = idMotivoEmisionNr;
    }

    public String getDescripcionMotivoEmisionNr() {
        return descripcionMotivoEmisionNr;
    }

    public void setDescripcionMotivoEmisionNr(String descripcionMotivoEmisionNr) {
        this.descripcionMotivoEmisionNr = descripcionMotivoEmisionNr;
    }

    public BigDecimal getKm() {
        return km;
    }

    public void setKm(BigDecimal km) {
        this.km = km;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public List<NotaRemisionDetalleParam> getNotaRemisionDetalles() {
        return notaRemisionDetalles;
    }

    public void setNotaRemisionDetalles(List<NotaRemisionDetalleParam> notaRemisionDetalles) {
        this.notaRemisionDetalles = notaRemisionDetalles;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public LocalEntregaParam getLocalEntrega() {
        return localEntrega;
    }

    public void setLocalEntrega(LocalEntregaParam localEntrega) {
        this.localEntrega = localEntrega;
    }

    public LocalSalidaParam getLocalSalida() {
        return localSalida;
    }

    public void setLocalSalida(LocalSalidaParam localSalida) {
        this.localSalida = localSalida;
    }

    public VehiculoTrasladoParam getVehiculoTraslado() {
        return vehiculoTraslado;
    }

    public void setVehiculoTraslado(VehiculoTrasladoParam vehiculoTraslado) {
        this.vehiculoTraslado = vehiculoTraslado;
    }

    public TransportistaParam getTransportista() {
        return transportista;
    }

    public void setTransportista(TransportistaParam transportista) {
        this.transportista = transportista;
    }

    public Integer getTipoTransporte() {
        return tipoTransporte;
    }

    public void setTipoTransporte(Integer tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }

    public Integer getModalidadTransporte() {
        return modalidadTransporte;
    }

    public void setModalidadTransporte(Integer modalidadTransporte) {
        this.modalidadTransporte = modalidadTransporte;
    }

    public Integer getResponsableCostoFlete() {
        return responsableCostoFlete;
    }

    public void setResponsableCostoFlete(Integer responsableCostoFlete) {
        this.responsableCostoFlete = responsableCostoFlete;
    }

    public String getNroDespachoImportacion() {
        return nroDespachoImportacion;
    }

    public void setNroDespachoImportacion(String nroDespachoImportacion) {
        this.nroDespachoImportacion = nroDespachoImportacion;
    }

    public Date getFechaInicioTraslado() {
        return fechaInicioTraslado;
    }

    public void setFechaInicioTraslado(Date fechaInicioTraslado) {
        this.fechaInicioTraslado = fechaInicioTraslado;
    }

    public Date getFechaFinTraslado() {
        return fechaFinTraslado;
    }

    public void setFechaFinTraslado(Date fechaFinTraslado) {
        this.fechaFinTraslado = fechaFinTraslado;
    }

    public Integer getResponsableEmision() {
        return responsableEmision;
    }

    public void setResponsableEmision(Integer responsableEmision) {
        this.responsableEmision = responsableEmision;
    }

    public Date getFechaFuturaEmision() {
        return fechaFuturaEmision;
    }

    public void setFechaFuturaEmision(Date fechaFuturaEmision) {
        this.fechaFuturaEmision = fechaFuturaEmision;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public Character getTieneFactura() {
        return tieneFactura;
    }

    public void setTieneFactura(Character tieneFactura) {
        this.tieneFactura = tieneFactura;
    }

    public Boolean getNroNotaCreditoDescendente() {
        return nroNotaCreditoDescendente;
    }

    public void setNroNotaCreditoDescendente(Boolean nroNotaCreditoDescendente) {
        this.nroNotaCreditoDescendente = nroNotaCreditoDescendente;
    }

    public Boolean getIdDescendente() {
        return idDescendente;
    }

    public void setIdDescendente(Boolean idDescendente) {
        this.idDescendente = idDescendente;
    }

    public Boolean getImprimirOriginal() {
        return imprimirOriginal;
    }

    public void setImprimirOriginal(Boolean imprimirOriginal) {
        this.imprimirOriginal = imprimirOriginal;
    }

    public Boolean getImprimirDuplicado() {
        return imprimirDuplicado;
    }

    public void setImprimirDuplicado(Boolean imprimirDuplicado) {
        this.imprimirDuplicado = imprimirDuplicado;
    }

    public Boolean getImprimirTriplicado() {
        return imprimirTriplicado;
    }

    public void setImprimirTriplicado(Boolean imprimirTriplicado) {
        this.imprimirTriplicado = imprimirTriplicado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getCodigoMotivoAnulacion() {
        return codigoMotivoAnulacion;
    }

    public void setCodigoMotivoAnulacion(String codigoMotivoAnulacion) {
        this.codigoMotivoAnulacion = codigoMotivoAnulacion;
    }

    public String getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(Boolean siediApi) {
        this.siediApi = siediApi;
    }

    public Boolean getTicket() {
        return ticket;
    }

    public void setTicket(Boolean ticket) {
        this.ticket = ticket;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public byte[] getUploadedFileBytes() {
        return uploadedFileBytes;
    }

    public void setUploadedFileBytes(byte[] uploadedFileBytes) {
        this.uploadedFileBytes = uploadedFileBytes;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }
    
}
