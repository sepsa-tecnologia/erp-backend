/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.ReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ReferenciaGeograficaFacade extends Facade<ReferenciaGeografica, ReferenciaGeograficaParam, UserInfoImpl> {

    public ReferenciaGeografica find(Integer id, String codigo);
    
}
