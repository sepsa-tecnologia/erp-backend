/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface FacturaFacade extends Facade<Factura, FacturaParam, UserInfoImpl> {

    public FacturaPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente);

    public List<FacturaPojo> findMonto(FacturaParam param);

    public Long findMontoSize(FacturaParam param);

    public FacturaPojo findMontoSumatoria(FacturaParam param);

    public Factura anular(FacturaParam param, UserInfoImpl userInfo);
    
    public Factura cancelarAnulacion(FacturaParam param, UserInfoImpl userInfo);

    public byte[] getPdfFactura(FacturaParam param, UserInfoImpl userInfo) throws Exception;

    public byte[] getXmlFactura(FacturaParam param, UserInfoImpl userInfo) throws Exception;
    
    public void actualizarSaldoFactura(Integer idFactura, BigDecimal saldo);

    public byte[] getXlsReporteVenta(FacturaParam param, UserInfoImpl userInfo) throws Exception;
    
    public byte[] getXlsEstadoCuenta(FacturaParam param, UserInfoImpl userInfo);

    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param);

    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param);

    public Boolean reenviarFacturasRechazadas(FacturaParam param);
    
    public List<String> createFacturaMasiva(List<FacturaParam> fpl, List<FacturaDetalleParam> fdpl, UserInfoImpl userInfo);
    
    public Boolean reenviarFacturaMasiva(FacturaParam param, UserInfoImpl userInfo);
    
    public Boolean actualizarCliente(FacturaParam param, UserInfoImpl userInfo);

    public void generarFactura(Integer id, UserInfoImpl userInfo);
    
    public Long cantidadFacturasEmitidas(FacturaParam param);
    
    public Long montoTotalVentas(FacturaParam param);
    
    public JsonObject cantidadFacturasEmitidasPorEstado(FacturaParam param);
    
    public JsonArray productosMasFacturados(FacturaParam param);
    
    public JsonArray clientesMasFacturados(FacturaParam param);
}
