/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de usuario local
 * @author Jonathan
 */
public class MenuPerfilParam extends CommonParam {
    
    public MenuPerfilParam() {
    }

    public MenuPerfilParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de menú
     */
    @QueryParam("idMenu")
    private Integer idMenu;
    
    /**
     * Código de menu
     */
    @QueryParam("codigoMenu")
    private String codigoMenu;
    
    /**
     * Identificador de perfil
     */
    @QueryParam("idPerfil")
    private Integer idPerfil;
    
    /**
     * Código de perfil
     */
    @QueryParam("codigoPerfil")
    private String codigoPerfil;

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idPerfil) && isNullOrEmpty(codigoPerfil)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de perfil"));
        }
        
        if(isNull(idMenu) && isNullOrEmpty(codigoMenu)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de menú"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idPerfil) && isNullOrEmpty(codigoPerfil)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de perfil"));
        }
        
        if(isNull(idMenu) && isNullOrEmpty(codigoMenu)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de menú"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToListRelacionado() {
        
        super.isValidToList();
        
        if(isNull(idMenu)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de menú"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu) {
        this.idMenu = idMenu;
    }

    public String getCodigoMenu() {
        return codigoMenu;
    }

    public void setCodigoMenu(String codigoMenu) {
        this.codigoMenu = codigoMenu;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getCodigoPerfil() {
        return codigoPerfil;
    }

    public void setCodigoPerfil(String codigoPerfil) {
        this.codigoPerfil = codigoPerfil;
    }
}
