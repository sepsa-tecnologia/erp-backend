/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import py.com.sepsa.erp.ejb.entities.info.Email;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.filters.EmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EstadoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioEmpresaParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioLocalParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioPerfilParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.EncargadoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.EnviarNotificacionAdapter;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Sergio D. Riveros Vazquez
 */
@Stateless(name = "UsuarioFacade", mappedName = "UsuarioFacade")
@Local(UsuarioFacade.class)
public class UsuarioFacadeImpl extends FacadeImpl<Usuario, UsuarioParam> implements UsuarioFacade {

    public UsuarioFacadeImpl() {
        super(Usuario.class);
    }

    @Override
    public Boolean validToCreate(UsuarioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "USUARIO");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado de usuario"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        UsuarioPojo usuario = findUsuarioPojo(param.getUsuario().trim());
        
        if(usuario != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un usuario con el mismo nombre"));
        }
        
        if(!isNull(param.getIdPersonaFisica())) {
            Persona persona = facades.getPersonaFacade().find(param.getIdPersonaFisica());
            
            if(isNull(persona)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la persona fisica"));
            }
        }
        
        if(!isNull(param.getIdEmail())) {
            Email email = facades.getEmailFacade().find(param.getIdEmail());
            
            if(isNull(email)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el email"));
            }
        }
        
        if(!isNull(param.getIdTelefono())) {
            Telefono telefono = facades.getTelefonoFacade().find(param.getIdTelefono());
            
            if(isNull(telefono)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el telefono"));
            }
        }
        
        if(!isNull(param.getPersonaFisica())) {
            facades.getPersonaFacade().validToCreate(param.getPersonaFisica());
        }
        
        if(!isNull(param.getEmail())) {
            facades.getEmailFacade().validToCreate(param.getEmail());
        }
        
        if(!isNull(param.getTelefono())) {
            facades.getTelefonoFacade().validToCreate(param.getTelefono());
        }
        
        if(!isNullOrEmpty(param.getUsuarioPerfiles())) {
            for (UsuarioPerfilParam item : param.getUsuarioPerfiles()) {
                facades.getUsuarioPerfilFacade().validToCreate(item, false);
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(UsuarioParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Usuario usuario = findUsuario(param.getId());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un usuario"));
        } else {
            UsuarioPojo usuario1 = findUsuarioPojo(param.getUsuario().trim());
        
            if(usuario1 != null && !usuario.getId().equals(usuario1.getId())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un usuario con el mismo nombre"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "USUARIO");
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Usuario create(UsuarioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        if(!isNull(param.getPersonaFisica())) {
            Persona persona = facades.getPersonaFacade().create(param.getPersonaFisica(), userInfo);
            param.setIdPersonaFisica(persona == null ? null : persona.getId());
        }
        
        if(!isNull(param.getEmail())) {
            Email email = facades.getEmailFacade().create(param.getEmail(), userInfo);
            param.setIdEmail(email == null ? null : email.getId());
        }
        
        if(!isNull(param.getTelefono())) {
            Telefono telefono = facades.getTelefonoFacade().create(param.getTelefono(), userInfo);
            param.setIdTelefono(telefono == null ? null : telefono.getId());
        }
        
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String contrasena = encoder.encode(param.getContrasena());
        
        Usuario item = new Usuario();
        item.setUsuario(param.getUsuario().trim());
        item.setContrasena(contrasena);
        item.setIdEmail(param.getIdEmail());
        item.setIdTelefono(param.getIdTelefono());
        item.setIdEstado(param.getIdEstado());
        item.setHash("N/A");
        item.setIdPersonaFisica(param.getIdPersonaFisica());
        create(item);
        
        if(!isNullOrEmpty(param.getUsuarioPerfiles())) {
            for (UsuarioPerfilParam up : param.getUsuarioPerfiles()) {
                up.setIdUsuario(item.getId());
                facades.getUsuarioPerfilFacade().create(up, userInfo);
            }
        }
        
        UsuarioEmpresaParam ueparam = new UsuarioEmpresaParam();
        ueparam.setIdUsuario(item.getId());
        ueparam.setIdEmpresa(param.getIdEmpresa());
        ueparam.setActivo('S');
        facades.getUsuarioEmpresaFacade().create(ueparam, userInfo);
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id", item.getId() + " ");
        atributos.put("usuario", item.getUsuario() + " ");
        atributos.put("contrasena", item.getContrasena()+ " ");
        atributos.put("id_email", item.getIdEmail() + " ");
        atributos.put("hash", item.getHash() + " ");
        atributos.put("id_telefono", item.getIdTelefono() + " ");
        atributos.put("id_estado", item.getIdEstado() + " ");
        atributos.put("id_persona_fisica", item.getIdPersonaFisica() + " ");

        facades.getRegistroFacade().create("usuario", "usuario", atributos, userInfo);
        
        return item;
    }
    
    @Override
    public Usuario edit(UsuarioParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Usuario item = findUsuario(param.getId());
        item.setUsuario(param.getUsuario().trim());
        item.setIdEmail(param.getIdEmail());
        item.setIdEstado(param.getIdEstado());
        item.setIdTelefono(param.getIdTelefono());
        item.setIdPersonaFisica(param.getIdPersonaFisica());
        edit(item);
        
        Map<String, String> pk = new HashMap();
        pk.put("id", item.getId() + " ");

        Map<String, String> atributos = new HashMap();
        atributos.put("usuario", item.getUsuario() + " ");
        atributos.put("contrasena", item.getContrasena()+ " ");
        atributos.put("id_email", item.getIdEmail() + " ");
        atributos.put("hash", item.getHash() + " ");
        atributos.put("id_telefono", item.getIdTelefono() + " ");
        atributos.put("id_estado", item.getIdEstado() + " ");
        atributos.put("id_persona_fisica", item.getIdPersonaFisica() + " ");

        facades.getRegistroFacade().edit("usuario", "usuario", atributos, userInfo, pk);
        
        return item;
    }
    
    public Usuario edit(Usuario param, UserInfoImpl userInfo) {
        
        UsuarioParam param1 = new UsuarioParam();
        param1.setId(param.getId());
        param1.setIdPersonaFisica(param.getIdPersonaFisica());
        param1.setUsuario(param.getUsuario());
        param1.setHash(param.getHash());
        param1.setIdEmail(param.getIdEmail());
        param1.setIdTelefono(param.getIdTelefono());
        param1.setIdEstado(param.getIdEstado());
        
        return edit(param1, userInfo);
    }

    @Override
    public Usuario findUsuario(String userName) {
        UsuarioParam param = new UsuarioParam();
        param.setUsuario(userName);
        return findFirst(param);
    }

    @Override
    public UsuarioPojo findUsuarioPojo(String userName) {
        UsuarioParam param = new UsuarioParam();
        param.setUsuarioEq(userName);
        return findFirstPojo(param);
    }
    
    /**
     * Obtiene un usuario
     *
     * @param hash Hash
     * @return Usuario
     */
    @Override
    public Usuario findHash(String hash) {
        Usuario user = null;
        if (hash != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.select(root);
            cq.where(qb.equal(root.get("hash"), hash.trim()));

            Query q = getEntityManager().createQuery(cq)
                    .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

            List list = q.getResultList();
            if (!list.isEmpty()) {
                user = (Usuario) list.get(0);
            }
        }
        return user;
    }

    /**
     * Obtiene un usuario con un idUsuario como parametro
     *
     * @param userId Identificador del usuario buscado
     * @return Usuario
     */
    @Override
    public Usuario findUsuario(Integer userId) {
        Usuario user = null;
        if (userId != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Usuario> root = cq.from(Usuario.class);
            cq.select(root);
            cq.orderBy(qb.asc(root.get("id")));

            cq.where(qb.equal(root.get("id"), userId));

            Query q = getEntityManager().createQuery(cq)
                    .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

            List list = q.getResultList();
            if (!list.isEmpty()) {
                user = (Usuario) list.get(0);
            }
        }
        return user;
    }
    
    @Override
    public List<UsuarioPojo> findPojo(UsuarioParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format(" %s and u.id = %d", where, param.getId());
        }
        
        if(param.getUsuario() != null && !param.getUsuario().trim().isEmpty()) {
            where = String.format(" %s and u.usuario ilike '%%%s%%'", where, param.getUsuario().trim());
        }
        
        if(param.getUsuarioEq() != null && !param.getUsuarioEq().trim().isEmpty()) {
            where = String.format(" %s and u.usuario = '%s'", where, param.getUsuarioEq().trim());
        }
        
        if(param.getTienePersonaFisica() != null) {
            where = String.format(" %s and %s", where, param.getTienePersonaFisica()
                    ? "not (pf is null)"
                    : "pf is null");
        }
        
        if(param.getIdPersonaFisica() != null) {
            where = String.format(" %s and pf.id = %d", where, param.getIdPersonaFisica());
        }
        
        if(param.getNombrePersonaFisica() != null) {
            where = String.format(" %s and (pf.nombre || ' ' || pf.apellido) ilike '%%%s%%'", where, param.getNombrePersonaFisica());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format(" %s and u.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and ue.id_empresa = %d", where, param.getIdEmpresa());
        }

        String sql = String.format("select distinct u.id, u.usuario, u.contrasena,\n"
                + "u.id_estado, e.codigo as codigo_estado, e.descripcion as estado,\n"
                + "u.id_email, u.id_telefono, u.hash, u.id_persona_fisica,\n"
                + "(pf.nombre || ' ' || pf.apellido) as persona_fisica,\n"
                + "pf.nombre, pf.apellido,\n"
                + "string_agg(e2.descripcion, ', ') as empresa\n"
                + "from usuario.usuario u\n"
                + "join info.estado e on e.id = u.id_estado\n"
                + "left join usuario.usuario_empresa ue on ue.id_usuario = u.id and ue.activo = 'S'\n"
                + "left join info.persona pf on pf.id = u.id_persona_fisica\n"
                + "left join info.empresa e2 on ue.id_empresa = e2.id\n"
                + "where %s\n"
                + "group by u.id, pf.id, e.id\n"
                + "order by u.id_estado, u.id offset %d limit %d", where,
                param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.setHint(REFRESH_HINT, REFRESH_HINT_VAL).getResultList();
        
        List<UsuarioPojo> result = new ArrayList<>();
        for (Object[] objects : list) {
            int i = 0;
            UsuarioPojo item = new UsuarioPojo();
            item.setId((Integer)objects[i++]);
            item.setUsuario((String)objects[i++]);
            item.setContrasena((String)objects[i++]);
            item.setIdEstado((Integer)objects[i++]);
            item.setCodigoEstado((String)objects[i++]);
            item.setEstado((String)objects[i++]);
            item.setIdEmail((Integer)objects[i++]);
            item.setIdTelefono((Integer)objects[i++]);
            item.setHash((String)objects[i++]);
            item.setIdPersonaFisica((Integer)objects[i++]);
            item.setPersonaFisica((String)objects[i++]);
            item.setNombre((String)objects[i++]);
            item.setApellido((String)objects[i++]);
            item.setEmpresa((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public List<Usuario> find(UsuarioParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format(" %s and u.id = %d", where, param.getId());
        }
        
        if(param.getUsuario() != null && !param.getUsuario().trim().isEmpty()) {
            where = String.format(" %s and u.usuario ilike '%%%s%%'", where, param.getUsuario().trim());
        }
        
        if(param.getUsuarioEq() != null && !param.getUsuarioEq().trim().isEmpty()) {
            where = String.format(" %s and u.usuario = '%s'", where, param.getUsuarioEq().trim());
        }
        
        if(param.getTienePersonaFisica() != null) {
            where = String.format(" %s and %s", where, param.getTienePersonaFisica()
                    ? "not (pf is null)"
                    : "pf is null");
        }
        
        if(param.getIdPersonaFisica() != null) {
            where = String.format(" %s and pf.id = %d", where, param.getIdPersonaFisica());
        }
        
        if(param.getNombrePersonaFisica() != null) {
            where = String.format(" %s and (pf.nombre || pf.apellido) ilike '%%%s%%'", where, param.getNombrePersonaFisica());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format(" %s and u.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and ue.id_empresa = %d", where, param.getIdEmpresa());
        }
      
        
        String sql = String.format("select distinct u.*\n"
                + "from usuario.usuario u\n"
                + "left join usuario.usuario_empresa ue on ue.id_usuario = u.id and ue.activo = 'S'\n"
                + "left join info.persona pf on pf.id = u.id_persona_fisica\n"
                + "where %s\n"
                + "order by u.id_estado offset %d limit %d", where,
                param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Usuario.class);
        
        List<Usuario> result = q.setHint(REFRESH_HINT, REFRESH_HINT_VAL).getResultList();
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de usuarios
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(UsuarioParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format(" %s and u.id = %d", where, param.getId());
        }
        
        if(param.getUsuario() != null && !param.getUsuario().trim().isEmpty()) {
            where = String.format(" %s and u.usuario ilike '%%%s%%'", where, param.getUsuario().trim());
        }
        
        if(param.getUsuarioEq() != null && !param.getUsuarioEq().trim().isEmpty()) {
            where = String.format(" %s and u.usuario = '%s'", where, param.getUsuarioEq().trim());
        }
        
        if(param.getTienePersonaFisica() != null) {
            where = String.format(" %s and %s", where, param.getTienePersonaFisica()
                    ? "not (pf is null)"
                    : "pf is null");
        }
        
        if(param.getIdPersonaFisica() != null) {
            where = String.format(" %s and pf.id = %d", where, param.getIdPersonaFisica());
        }
        
        if(param.getNombrePersonaFisica() != null) {
            where = String.format(" %s and (pf.nombre || pf.apellido) ilike '%%%s%%'", where, param.getNombrePersonaFisica());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format(" %s and u.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and ue.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select count(distinct u.id)\n"
                + "from usuario.usuario u\n"
                + "left join usuario.usuario_empresa ue on ue.id_usuario = u.id and ue.activo = 'S'\n"
                + "left join info.persona pf on pf.id = u.id_persona_fisica\n"
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
    
    /**
     * Obtiene la lista de encargado
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public List<EncargadoPojo> findEncargados(CommonParam param) {

        String where = "";
        
        if(param.getId() != null) {
            where = String.format(" %s and p.id = %d", where, param.getId());
        }
        
        String sql = String.format("select distinct p.id,\n"
                + "coalesce(p.razon_social, p.nombre || ' ' || p.apellido)\n"
                + "from usuario.usuario u\n"
                + "join usuario.usuario_perfil up on up.id_usuario = u.id\n"
                + "join usuario.perfil per on per.id = up.id_perfil\n"
                + "join info.persona p on p.id = u.id_persona_fisica\n"
                + "where per.descripcion in ('ROLE_FAC', 'ROLE_COBRO') %s\n"
                + "order by 2 offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();

        List<EncargadoPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            EncargadoPojo item = new EncargadoPojo();
            item.setId((Integer)objects[0]);
            item.setRazonSocial((String)objects[1]);
            result.add(item);
        }
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de encargado
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findEncargadosSize(CommonParam param) {

        String where = "";
        
        if(param.getId() != null) {
            where = String.format(" %s and p.id = %d", where, param.getId());
        }
        
        String sql = String.format("select count(distinct p.id)\n"
                + "from usuario.usuario u\n"
                + "join usuario.usuario_perfil up on up.id_usuario = u.id\n"
                + "join usuario.perfil per on per.id = up.id_perfil\n"
                + "join info.persona p on p.id = u.id_persona_fisica\n"
                + "where per.descripcion in ('ROLE_FAC', 'ROLE_COBRO') %s\n", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public boolean validToSendRecover(UsuarioParam param) {
        
        if(!param.isValidToSendRecover()) {
            return Boolean.FALSE;
        }
        
        UsuarioPojo usuario = findUsuarioPojo(param.getUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        } else {
            
            param.setId(usuario.getId());
            
            if(usuario.getIdEmail() == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El usuario no tiene asociado un mail"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public boolean validToRecover(UsuarioParam param) throws Exception {
        
        if(!param.isValidToRecover()) {
            return Boolean.FALSE;
        }
        
        Usuario usuario = findHash(param.getHash());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un usuario asociado al usuario"));
        } else {
            
            param.setId(usuario.getId());
            
            if(usuario.getIdEmail() == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El usuario no tiene asociado un mail"));
            }
            
            param.setUrlDestino("--");
            param.setUsuario(usuario.getUsuario());
            String hash = getUserHash(param);
            
            if(usuario.getHash() == null || hash == null
                    || usuario.getHash().length() < 10
                    || !usuario.getHash().equals(hash)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Hash inválido o no corresponde al usuario"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Boolean notificarRecuperacion(UsuarioParam param) {
        
        if(!validToSendRecover(param)) {
            return null;
        }
        
        Usuario usuario = find(param.getId());
        
        String correo ="";
        if(usuario.getEmail()!=null){
            correo = usuario.getEmail().getEmail();
        } else {
            EmailParam emailParam = new EmailParam();
            emailParam.setId(usuario.getIdEmail());
            Email email = facades.getEmailFacade().findFirst(emailParam);
            correo = email.getEmail();
        }
        
        String url = String.format("%s?h=%s",
                param.getUrlDestino(), param.getHash());
        
        String asunto = "Cambio de contraseña";
        String mensaje = String.format("Para restablecer la contraseña haga click en el siguiente <a href=\"%s\">enlace</a>. " +
                "Si usted no ha solicitado esto, favor sólo ignore este mensaje.", url);
        
        EnviarNotificacionAdapter adapter = new EnviarNotificacionAdapter(asunto,
                mensaje, correo, "MAIL_EDI", "P");
        adapter.request();
        
        return adapter.getSuccess();
    }
    
    public Usuario updateUserHash(UsuarioParam param) {
        
        if(!validToSendRecover(param)) {
            return null;
        }
        
        Usuario usuario = find(param.getId());
        usuario.setHash(param.getHash());
        edit(usuario);
        
        return usuario;
    }
    
    @Override
    public Boolean enviarCorreoRecuperacion(UsuarioParam param) throws Exception {
        
        String hash = getUserHash(param);
        if(hash != null) {
            param.setHash(hash);
            Boolean success = notificarRecuperacion(param);
            if(success) {
                Usuario usuario = updateUserHash(param);
                return usuario != null;
            }
        }
        return Boolean.FALSE;
    }
    
    @Override
    public Boolean recuperarContrasena(UsuarioParam param) throws Exception {
        
        if(!validToRecover(param)) {
            return Boolean.FALSE;
        }
        
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String contraseñaEncriptada = encoder.encode(param.getContrasena());
        
        Usuario usuario = find(param.getId());
        usuario.setHash("N/A");
        usuario.setContrasena(contraseñaEncriptada);
        edit(usuario);
        
        return Boolean.TRUE;
    }
    
    /**
     *
     * @param param
     * @return
     * @throws Exception
     */
    @Override
    public Boolean validarHash(UsuarioParam param) throws Exception {
        if(!validToRecover(param)){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
    
    public String getUserHash(UsuarioParam param) throws Exception {
        
        if(!validToSendRecover(param)) {
             return null;
        }
        
        Usuario usuario = find(param.getId());
        
        String key = String.format("_k3y-%d-%s_", usuario.getId(),
                usuario.getUsuario());
        
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(key.getBytes(StandardCharsets.UTF_8));
        String sha256Hash = bytesToHex(hash);
        
        return sha256Hash;
    }
    
    public String bytesToHex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte byt : bytes) {
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

    @Override
    public UsuarioPojo findPojoCompleto(UsuarioPojo usuario) {

        if (usuario != null) {
            UsuarioPerfilParam upparam = new UsuarioPerfilParam();
            upparam.setCodigoEstado("ACTIVO");
            upparam.setIdUsuario(usuario.getId());
            upparam.setPageSize(100);
            usuario.setUsuarioPerfiles(facades.getUsuarioPerfilFacade().findPojo(upparam));
        }

        if (usuario != null) {
            UsuarioLocalParam ulparam = new UsuarioLocalParam();
            ulparam.setActivo('S');
            ulparam.setIdUsuario(usuario.getId());
            ulparam.setPageSize(100);
            usuario.setUsuarioLocales(facades.getUsuarioLocalFacade().findPojo(ulparam));
        }

        if (usuario != null) {
            UsuarioEmpresaParam ueparam = new UsuarioEmpresaParam();
            ueparam.setActivo('S');
            ueparam.setIdUsuario(usuario.getId());
            ueparam.setPageSize(100);
            usuario.setUsuarioEmpresas(facades.getUsuarioEmpresaFacade().findPojo(ueparam));
        }
        
        return usuario;
    }
    
    @Override
    public List<UsuarioPojo> bloquearUsuarios(Integer idClienteSepsa, UserInfoImpl userInfo) {
        String bandera = "ACTIVO";
        List<UsuarioPojo> usuarios = facades.getEmpresaFacade().findUsuarios(idClienteSepsa, bandera);
        
        EstadoParam estadoParam = new EstadoParam();
        estadoParam.setCodigoTipoEstado("USUARIO");
        estadoParam.setCodigo("BLOQUEADO");
        estadoParam.setActivo('S');
        
        Estado estado = facades.getEstadoFacade().findFirst(estadoParam);
            
        for (UsuarioPojo usuario : usuarios) {
            Usuario item = findUsuario(usuario.getId());
            item.setIdEstado(estado.getId());
            edit(item, userInfo);
        }
        
        EmpresaParam empresa = new EmpresaParam();
        empresa.setIdClienteSepsa(idClienteSepsa);
        empresa.isValidToList();
        List<Empresa> empresas = facades.getEmpresaFacade().find(empresa);
        Empresa empreEdit = empresas.get(0);
        empreEdit.setActivo('B');
        facades.getEmpresaFacade().edit(empreEdit);
        
        bandera = "BLOQUEADO";
        return facades.getEmpresaFacade().findUsuarios(idClienteSepsa, bandera);
    }
    
    @Override
    public List<UsuarioPojo> desbloquearUsuarios(Integer idClienteSepsa, UserInfoImpl userInfo) {
        String bandera = "BLOQUEADO";
        List<UsuarioPojo> usuarios = facades.getEmpresaFacade().findUsuarios(idClienteSepsa, bandera);
        
        EstadoParam estadoParam = new EstadoParam();
        estadoParam.setCodigoTipoEstado("USUARIO");
        estadoParam.setCodigo("ACTIVO");
        estadoParam.setActivo('S');
        
        Estado estado = facades.getEstadoFacade().findFirst(estadoParam);

        for (UsuarioPojo usuario : usuarios) {
            Usuario item = findUsuario(usuario.getId());
            item.setIdEstado(estado.getId());
            edit(item, userInfo);
        }
        
        EmpresaParam empresa = new EmpresaParam();
        empresa.setIdClienteSepsa(idClienteSepsa);
        empresa.isValidToList();
        List<Empresa> empresas = facades.getEmpresaFacade().find(empresa);
        Empresa empreEdit = empresas.get(2);
        empreEdit.setActivo('S');
        facades.getEmpresaFacade().edit(empreEdit);
        
        bandera = "ACTIVO";
        return facades.getEmpresaFacade().findUsuarios(idClienteSepsa, bandera);
    }
}
