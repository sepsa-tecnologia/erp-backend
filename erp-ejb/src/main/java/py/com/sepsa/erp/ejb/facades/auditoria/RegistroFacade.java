/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.Map;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.auditoria.Registro;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Local
public interface RegistroFacade extends Facade<Registro, CommonParam, UserInfoImpl> {

    public void create(String esquema, String tabla, Map<String, String> atributos, UserInfoImpl userInfo);

    public void edit(String esquema, String tabla, Map<String, String> atributos, UserInfoImpl userInfo, Map<String, String> pk);

    public void delete(String esquema, String tabla, UserInfoImpl userInfo, Map<String, String> conditions);
    
}
