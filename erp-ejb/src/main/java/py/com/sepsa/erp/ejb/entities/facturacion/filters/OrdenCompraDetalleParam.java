/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de orden de compra detalle
 *
 * @author Jonathan D. Bernal Fernández
 */
public class OrdenCompraDetalleParam extends CommonParam {

    public OrdenCompraDetalleParam() {
    }

    public OrdenCompraDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de orden de compra
     */
    @QueryParam("idOrdenCompra")
    private Integer idOrdenCompra;
    
    /**
     * Recibido
     */
    @QueryParam("recibido")
    private Character recibido;
    
    /**
     * Identificador de local destino
     */
    @QueryParam("idLocalDestino")
    private Integer idLocalDestino;
    
    /**
     * Identificador de local origen
     */
    @QueryParam("idLocalOrigen")
    private Integer idLocalOrigen;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Código GTIN
     */
    @QueryParam("codigoGtin")
    private String codigoGtin;

    /**
     * Cantidad solicitada
     */
    @QueryParam("cantidadSolicitada")
    private Integer cantidadSolicitada;

    /**
     * Cantidad confirmada
     */
    @QueryParam("cantidadConfirmada")
    private Integer cantidadConfirmada;
    
    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;

    /**
     * Precio unitario con iva
     */
    @QueryParam("precioUnitarioConIva")
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin iva
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;

    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;

    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;

    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;

    /**
     * Observación
     */
    @QueryParam("observacion")
    private String observacion;

    /**
     * Código de estado de orden de compra
     */
    @QueryParam("codigoEstadoOrdenCompra")
    private String codigoEstadoOrdenCompra;
    
    /**
     * Identificador de deposito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;

    /**
     * Identificador de estado inventario
     */
    @QueryParam("idEstadoInventario")
    private Integer idEstadoInventario;

    /**
     * Código de estado inventario
     */
    @QueryParam("codigoEstadoInventario")
    private String codigoEstadoInventario;
    
    /**
     * Registro inventario
     */
    @QueryParam("registroInventario")
    private Character registroInventario;
    
    /**
     * Fecha de vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

//        if (isNull(idProducto) && isNullOrEmpty(codigoGtin)) {
//            addError(MensajePojo.createInstance()
//                    .descripcion("Se debe indicar el identificador y/o código gtin del producto"));
//        }

        if (isNull(cantidadSolicitada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad solicitada"));
        }

        if (isNull(cantidadConfirmada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad confirmada"));
        }
        
        if (isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }

        if (isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA"));
        }

        if (isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }

        if (isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }

        if (isNull(registroInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la bandera de registro de inventario"));
        } else {
            if(registroInventario.equals('S')) {
                if(isNull(idDepositoLogistico)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el identificador de depósito logístico"));
                }

                if(isNull(idEstadoInventario) && isNullOrEmpty(codigoEstadoInventario)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el identificador/código de estado de inventario"));
                }
            }
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }

        if (isNull(idOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

        if (isNull(idProducto) && isNullOrEmpty(codigoGtin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador y/o código gtin del producto"));
        }

        if (isNull(cantidadSolicitada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad solicitada"));
        }

        if (isNull(cantidadConfirmada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad confirmada"));
        }
        
        if (isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }

        if (isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA"));
        }

        if (isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }

        if (isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if (isNull(registroInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la bandera de registro de inventario"));
        } else {
            if(registroInventario.equals('S')) {
                if(isNull(idDepositoLogistico)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el identificador de depósito logístico"));
                }

                if(isNull(idEstadoInventario) && isNullOrEmpty(codigoEstadoInventario)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el identificador/código de estado de inventario"));
                }
            }
        }

        return !tieneErrores();
    }

    public boolean isValidToGetByOrdenCompra() {

        limpiarErrores();

        if (isNull(idOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

        return !tieneErrores();
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getCantidadSolicitada() {
        return cantidadSolicitada;
    }

    public void setCantidadSolicitada(Integer cantidadSolicitada) {
        this.cantidadSolicitada = cantidadSolicitada;
    }

    public Integer getCantidadConfirmada() {
        return cantidadConfirmada;
    }

    public void setCantidadConfirmada(Integer cantidadConfirmada) {
        this.cantidadConfirmada = cantidadConfirmada;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setCodigoEstadoOrdenCompra(String codigoEstadoOrdenCompra) {
        this.codigoEstadoOrdenCompra = codigoEstadoOrdenCompra;
    }

    public String getCodigoEstadoOrdenCompra() {
        return codigoEstadoOrdenCompra;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setRecibido(Character recibido) {
        this.recibido = recibido;
    }

    public Character getRecibido() {
        return recibido;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public void setRegistroInventario(Character registroInventario) {
        this.registroInventario = registroInventario;
    }

    public Character getRegistroInventario() {
        return registroInventario;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }
}
