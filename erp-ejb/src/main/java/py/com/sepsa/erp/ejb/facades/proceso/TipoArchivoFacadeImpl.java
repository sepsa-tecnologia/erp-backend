/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.TipoArchivo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoArchivoFacade", mappedName = "TipoArchivoFacade")
@Local(TipoArchivoFacade.class)
public class TipoArchivoFacadeImpl extends FacadeImpl<TipoArchivo, CommonParam> implements TipoArchivoFacade {

    public TipoArchivoFacadeImpl() {
        super(TipoArchivo.class);
    }

    /*public Boolean validToCreate(TipoProcesoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoProcesoParam param1 = new TipoProcesoParam();
        param1.setCodigo(param.getCodigo().trim());
        
        TipoProceso item = findFirst(param1);
        
        if(!isNull(item)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(TipoProcesoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoProceso tp = find(param.getId());
        
        if(isNull(tp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo proceso"));
        } else if(!tp.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoProcesoParam param1 = new TipoProcesoParam();
            param1.setCodigo(param.getCodigo().trim());

            TipoProceso item = findFirst(param1);

            if(!isNull(item)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public TipoProceso create(TipoProcesoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoProceso item = new TipoProceso();
        item.setDescripcion(param.getDescripcion().trim());
        item.setCodigo(param.getCodigo().trim());
        
        create(item);
        
        return item;
    }

    @Override
    public TipoProceso edit(TipoProcesoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoProceso item = find(param.getId());
        item.setDescripcion(param.getDescripcion().trim());
        item.setCodigo(param.getCodigo().trim());
        
        edit(item);
        
        return item;
    }*/
    
    @Override
    public TipoArchivo find(Integer id, String codigo) {
        CommonParam param = new CommonParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoArchivo> find(CommonParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(TipoArchivo.class);
        Root<TipoArchivo> root = cq.from(TipoArchivo.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<TipoArchivo> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(CommonParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<TipoArchivo> root = cq.from(TipoArchivo.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
