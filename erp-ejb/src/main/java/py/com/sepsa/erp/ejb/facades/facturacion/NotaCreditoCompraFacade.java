/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface NotaCreditoCompraFacade extends Facade<NotaCreditoCompra, NotaCreditoCompraParam, UserInfoImpl> {

    public NotaCreditoCompra anular(NotaCreditoCompraParam param, UserInfoImpl userInfo);

    public Boolean delete(NotaCreditoCompraParam param, UserInfoImpl userInfo);

    public List<RegistroComprobantePojo> generarComprobanteCompra(ReporteComprobanteParam param);

    public Integer generarComprobanteCompraSize(ReporteComprobanteParam param);
    
}