/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.injection;

import py.com.sepsa.utils.injection.Jndi;

/**
 *
 * @author Jonathan Bernal
 */
public final class Injection {

    private static final String EAR_NAME = "erp-ejb-core-1.0.0";

    private static final String EJB_NAME = "py.com.sepsa.erp-erp-ejb-1.0.0";

    public static final <T> T bean(Class<T> beanClass) {
        try {
            String jndi = String.format("java:global/%s/%s/%s", EAR_NAME, EJB_NAME, beanClass.getSimpleName());
            return Jndi.lookup(jndi);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
