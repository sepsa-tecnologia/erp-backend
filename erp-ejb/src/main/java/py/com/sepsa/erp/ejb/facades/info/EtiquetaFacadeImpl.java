/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Etiqueta;
import py.com.sepsa.erp.ejb.entities.info.TipoEtiqueta;
import py.com.sepsa.erp.ejb.entities.info.filters.EtiquetaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "EtiquetaFacade", mappedName = "EtiquetaFacade")
@Local(EtiquetaFacade.class)
public class EtiquetaFacadeImpl extends FacadeImpl<Etiqueta, EtiquetaParam> implements EtiquetaFacade{
    
    public EtiquetaFacadeImpl() {
        super(Etiqueta.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(EtiquetaParam param) {
        
        Boolean valid = param.isValidToCreate();
        
        TipoEtiqueta tipoEtiqueta = param.getIdTipoEtiqueta() == null
                ? null
                : facades.getTipoEtiquetaFacade()
                        .find(param.getIdTipoEtiqueta());
        
        EtiquetaParam param1 = new EtiquetaParam();
        param1.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        param1.setCodigo(param.getCodigo());
        Long size = findSize(param);
        
        if(tipoEtiqueta == null || size > 0) {
            valid = false;
        }
        
        return valid;
    }
    
    /**
     * Crea una instancia de Etiqueta
     * @param param parámetros
     * @return Etiqueta
     */
    public Etiqueta create(EtiquetaParam param) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Etiqueta etiqueta = new Etiqueta();
        etiqueta.setCodigo(param.getCodigo().trim());
        etiqueta.setDescripcion(param.getDescripcion().trim());
        etiqueta.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        create(etiqueta);
        
        return etiqueta;
    }
    
    /**
     * Obtiene la lista de tarifa
     * @param param parametros
     * @return Lista
     */
    @Override
    public List<Etiqueta> find(EtiquetaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Etiqueta.class);
        Root<Etiqueta> root = cq.from(Etiqueta.class);
        Join<Etiqueta, TipoEtiqueta> tipoEtiqueta = root.join("tipoEtiqueta");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdTipoEtiqueta() != null) {
            predList.add(qb.equal(tipoEtiqueta.get("id"), param.getIdTipoEtiqueta()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getTipoEtiqueta()!= null && !param.getTipoEtiqueta().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(tipoEtiqueta.<String>get("descripcion")), String.format("%%%s%%", param.getTipoEtiqueta().trim().toUpperCase())));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Etiqueta> result = q.getResultList();

        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de tarifa
     * @param param parametros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(EtiquetaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Etiqueta> root = cq.from(Etiqueta.class);
        Join<Etiqueta, TipoEtiqueta> tipoEtiqueta = root.join("tipoEtiqueta");
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdTipoEtiqueta() != null) {
            predList.add(qb.equal(tipoEtiqueta.get("id"), param.getIdTipoEtiqueta()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getTipoEtiqueta()!= null && !param.getTipoEtiqueta().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(tipoEtiqueta.<String>get("descripcion")), String.format("%%%s%%", param.getTipoEtiqueta().trim().toUpperCase())));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
