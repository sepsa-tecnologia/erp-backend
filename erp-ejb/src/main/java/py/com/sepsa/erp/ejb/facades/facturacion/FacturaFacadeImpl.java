/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.amazonaws.util.IOUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import py.com.sepsa.dtecore.v150.handler.Document;
import py.com.sepsa.erp.dte.generator.FacturaGeneratorRunnable;
import static py.com.sepsa.utils.misc.Lists.join;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.NaturalezaClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.CobroDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaDncp;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoOperacionCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoTransaccion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCuotaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDescuentoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParametroAdicionalParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TipoCambioPojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefono;
import py.com.sepsa.erp.ejb.entities.info.Vendedor;
import py.com.sepsa.erp.ejb.entities.info.filters.EstadoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.set.DetalleProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.KudeDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.adapters.XmlDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.facades.WatermarkUtils;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.erp.reporte.pojos.factura.EstadoCuentaDetalle;
import py.com.sepsa.erp.reporte.pojos.factura.EstadoCuentaParam;
import py.com.sepsa.erp.reporte.pojos.factura.FacturaAutoimpresorParam;
import py.com.sepsa.erp.reporte.pojos.factura.ReporteVentaFacturaParam;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.misc.Dates;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.misc.Strings;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.client.ConnectionPojo;
import py.com.sepsa.utils.rest.client.body.BodyResponse;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import py.com.sepsa.utils.siediApi.pojos.ResultadoOperacionSiediApi;
import py.com.sepsa.utils.siediApi.remote.LoginServiceSiediApi;
import py.com.sepsa.utils.siediApi.remote.SiediApiServiceClient;
import py.com.sepsa.utils.siedimonitor.pojos.DetalleResultado;
import py.com.sepsa.utils.siedimonitor.pojos.ResultadoOperacion;
import py.com.sepsa.utils.siedimonitor.remote.EventoService;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaFacade", mappedName = "FacturaFacade")
@Local(FacturaFacade.class)
@Log4j2
public class FacturaFacadeImpl extends FacadeImpl<Factura, FacturaParam> implements FacturaFacade {

    public FacturaFacadeImpl() {
        super(Factura.class);
    }

    /**
     * Obtiene los datos como para crear
     *
     * @param talonario talonario
     * @param idCliente Identificador de cliente
     * @return Datos
     */
    @Override
    public FacturaPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente) {

        Date fecha = facades.getTalonarioFacade().ultimaFechaDoc(talonario, 1);
        String nroFactura = facades.getTalonarioFacade().sigNumFactura(talonario);

        String razonSocial = null;
        String ruc = null;
        String direccion = null;
        Integer nroCasa = null;
        Integer idDepartamento = null;
        Integer idDistrito = null;
        Integer idCiudad = null;
        String email = null;
        String telefono = null;

        Cliente cliente = idCliente == null
                ? null
                : facades.getClienteFacade().find(idCliente);

        if (cliente != null) {
            email = Units.execute(() -> facades.getPersonaEmailFacade().obtenerEmailCliente(idCliente).getEmail().getEmail());
            telefono = Units.execute(() -> {
                PersonaTelefono item = facades.getPersonaTelefonoFacade().obtenerTelefonoCliente(idCliente);
                return String.format("%s%s", item.getTelefono().getPrefijo(), item.getTelefono().getNumero());
            });
            razonSocial = cliente.getRazonSocial();
            ruc = cliente.getNroDocumento();
            direccion = Units.execute(() -> cliente.getPersona().getDireccion().getDireccion());
            nroCasa = Units.execute(() -> cliente.getPersona().getDireccion().getNumero());
            idDepartamento = Units.execute(() -> cliente.getPersona().getDireccion().getDepartamento().getId());
            idDistrito = Units.execute(() -> cliente.getPersona().getDireccion().getDistrito().getId());
            idCiudad = Units.execute(() -> cliente.getPersona().getDireccion().getCiudad().getId());
        }

        StringTokenizer st = new StringTokenizer(nroFactura, "-");

        String establecimiento = st.nextToken();
        String puntoExpedicion = st.nextToken();
        String nroDocumento = "0000000" + st.nextToken();
        nroDocumento = nroDocumento.substring(nroDocumento.length() - 7, nroDocumento.length());

        FacturaPojo item = new FacturaPojo();
        item.setIdTalonario(talonario.getId());
        item.setFechaVencimientoTimbrado(talonario.getFechaVencimiento());
        item.setTimbrado(talonario.getTimbrado());
        item.setFecha(fecha);
        item.setEstablecimiento(establecimiento);
        item.setPuntoExpedicion(puntoExpedicion);
        item.setNroDocumento(nroDocumento);
        item.setNroFactura(nroFactura);
        item.setIdCliente(idCliente);
        item.setRazonSocial(razonSocial);
        item.setDireccion(direccion);
        item.setNroCasa(nroCasa);
        item.setIdDepartamento(idDepartamento);
        item.setIdDistrito(idDistrito);
        item.setIdCiudad(idCiudad);
        item.setEmail(email);
        item.setTelefono(telefono);
        item.setRuc(ruc);

        return item;
    }

    @Override
    public Boolean actualizarCliente(FacturaParam param, UserInfoImpl userInfo) {

        if (!validToUpdateClient(param)) {
            return Boolean.FALSE;
        }

        Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());

        String set = String.format("id_cliente = %d", param.getIdCliente());

        String sql = String.format("UPDATE facturacion.factura SET %s WHERE id_cliente is null and ruc = '%s' and id_empresa = %d", set, cliente.getNroDocumento(), param.getIdEmpresa());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        q.executeUpdate();

        return Boolean.TRUE;
    }

    public Boolean validToUpdateClient(FacturaParam param) {

        if (!param.isValidToUpdateClient()) {
            return Boolean.FALSE;
        }

        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());

        if (empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }

        Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());

        if (cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }

        if (empresa != null && cliente != null && !empresa.getId().equals(cliente.getIdEmpresa())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El cliente no corresponde a la empresa"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(FacturaParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        Factura factura = find(param.getId());

        if (factura == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        } else {
            if (isNullOrEmpty(param.getRuc())) {
                param.setRuc(factura.getRuc());
            }
            if (isNullOrEmpty(param.getRazonSocial())) {
                param.setRazonSocial(factura.getRazonSocial());
            }

            if (isNull(param.getIdNaturalezaCliente())) {
                param.setIdNaturalezaCliente(factura.getIdNaturalezaCliente());
            }

            if (isNull(param.getCodigoNaturalezaCliente())) {
                param.setCodigoNaturalezaCliente(factura.getNaturalezaCliente().getCodigo());
            }

            Boolean validarFormatoRuc = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "VALIDAR_FORMATO_RUC", Boolean.TRUE);
            if (validarFormatoRuc) {
                if (param.getCodigoNaturalezaCliente().equalsIgnoreCase("CONTRIBUYENTE")) {
                    if ((param.getRuc().length() < 7 || param.getRuc().length() > 10)) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("Se requiere un nro de RUC válido"));
                    }
                } else {
                    if ((param.getRuc().length() < 1 || param.getRuc().length() > 20)) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("Se requiere un nro de documento válido"));
                    }
                }
            }

            if (isNullOrEmpty(param.getEmail())) {
                param.setEmail(factura.getEmail());
            }
            if (isNullOrEmpty(param.getTelefono())) {
                param.setTelefono(factura.getTelefono());
            }
            if (isNullOrEmpty(param.getObservacion())) {
                param.setObservacion(factura.getObservacion());
            }

        }

        if (param.getIdEstado() != null || param.getCodigoEstado() != null) {
            EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                    param.getCodigoEstado(), "FACTURA");

            if (estado == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
            } else {
                param.setIdEstado(estado.getId());
                param.setCodigoEstado(estado.getCodigo());

                if ((estado.getCodigo().equals("APROBADO")
                        || estado.getCodigo().equals("CANCELADO")
                        || estado.getCodigo().equals("INUTILIZADO"))) {
                    if (factura != null && !factura.getRuc().equals(param.getRuc())) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion(String.format("No se puede editar "
                                        + "el RUC de una factura con estado %s",
                                        estado.getCodigo())));
                    }
                }
            }
        }

        if (param.getIdProcesamiento() != null) {

            Procesamiento procesamiento = facades.getProcesamientoFacade()
                    .find(param.getIdProcesamiento());

            if (procesamiento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el registro de procesamiento"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToGetPdf(FacturaParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        Long fsize = findSize(param);

        if (fsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToCancel(FacturaParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), param.getCodigoMotivoAnulacion(), "FACTURA");

        if (motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        } else {
            param.setIdMotivoAnulacion(motivoAnulacion.getId());
            param.setCodigoMotivoAnulacion(motivoAnulacion.getCodigo());
            param.setMotivoAnulacion(motivoAnulacion.getDescripcion());
        }

        FacturaParam fparam = new FacturaParam();
        fparam.setId(param.getId());
        FacturaPojo factura = facades.getFacturaFacade().findFirstPojo(fparam);

        if (factura == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura"));
        } else {

            param.setNroFactura(factura.getNroFactura());

            if (Objects.equals(factura.getAnulado(), 'S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La factura ya se encuentra anulada"));
            }

            String diaDeclaracionMes = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "DIA_DECLARACION_MES");

            Integer diaDeclaracion = Units.execute(() -> Integer.valueOf(diaDeclaracionMes));

            if ((param.getIgnorarPeriodoAnulacion() == null
                    || !param.getIgnorarPeriodoAnulacion())
                    && !isNull(diaDeclaracion)) {

                Calendar inv = Calendar.getInstance();
                inv.setTime(factura.getFecha());
                inv.add(Calendar.DAY_OF_MONTH, diaDeclaracion);
                inv.set(Calendar.HOUR_OF_DAY, 0);
                inv.set(Calendar.MINUTE, 0);
                inv.set(Calendar.SECOND, 0);

                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);

                if (inv.compareTo(now) < 0) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Sólo se pueden anular facturas que esten dentro del periodo de anulación"));
                }
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToUndoCancel(FacturaParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        FacturaParam fparam = new FacturaParam();
        fparam.setId(param.getId());
        FacturaPojo factura = facades.getFacturaFacade().findFirstPojo(fparam);

        if (factura == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura"));
        }

        String diaDeclaracionMes = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "DIA_DECLARACION_MES");

        Integer diaDeclaracion = Units.execute(() -> Integer.valueOf(diaDeclaracionMes));

        if ((param.getIgnorarPeriodoAnulacion() == null
                || !param.getIgnorarPeriodoAnulacion())
                && !isNull(diaDeclaracion)) {

            Calendar inv = Calendar.getInstance();
            inv.setTime(factura.getFecha());
            inv.add(Calendar.DAY_OF_MONTH, diaDeclaracion);
            inv.set(Calendar.HOUR_OF_DAY, 0);
            inv.set(Calendar.MINUTE, 0);
            inv.set(Calendar.SECOND, 0);

            Calendar now = Calendar.getInstance();
            now.set(Calendar.HOUR_OF_DAY, 0);
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);

            if (inv.compareTo(now) < 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Sólo se pueden regenerar facturas que estén dentro del periodo de anulación"));
            }
        }
        return !param.tieneErrores();
    }

    public Boolean validToCreate(UserInfoImpl userInfo, FacturaParam param) {

        if (param.getIdTipoFactura() != null || param.getCodigoTipoFactura() != null) {
            TipoFactura tipoFactura = facades
                    .getTipoFacturaFacade()
                    .find(param.getIdTipoFactura(), param.getCodigoTipoFactura());

            if (tipoFactura != null) {
                param.setIdTipoFactura(tipoFactura.getId());
                param.setCodigoTipoFactura(tipoFactura.getCodigo());

                String cobrado = facades.getConfiguracionValorFacade().getCVString(param.getIdEmpresa(), "ASIGNAR_FACTURAS_CONTADO_COBRADO", "N");

                if (param.getCodigoTipoFactura().equalsIgnoreCase("CONTADO") && cobrado != null && cobrado.equalsIgnoreCase("S")) {
                    param.setCobrado('S');
                } else {
                    param.setCobrado('N');
                }
            } else {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el tipo de factura"));
            }
        }

        if (param.getIdTipoTransaccion() != null || param.getCodigoTipoTransaccion() != null) {
            TipoTransaccion tipoTransaccion = facades
                    .getTipoTransaccionFacade()
                    .find(param.getIdTipoTransaccion(), param.getCodigoTipoTransaccion());

            if (tipoTransaccion != null) {
                param.setIdTipoTransaccion(tipoTransaccion.getId());
                param.setCodigoTipoTransaccion(tipoTransaccion.getCodigo());
            } else {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el tipo de transacción"));
            }
        }

        if (param.getIdVendedor() != null) {
            Vendedor vendedor = facades
                    .getVendedorFacade()
                    .find(param.getIdVendedor());

            if (vendedor == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el vendedor"));
            }
        }

        if ("CREDITO".equals(param.getCodigoTipoFactura())) {

            TipoOperacionCredito toc = facades.getTipoOperacionCreditoFacade()
                    .find(param.getIdTipoOperacionCredito(),
                            param.getCodigoTipoOperacionCredito());

            if (toc != null) {
                param.setIdTipoOperacionCredito(toc.getId());
                param.setCodigoTipoOperacionCredito(toc.getCodigo());
            }
        } else {
            param.setIdTipoOperacionCredito(null);
            param.setCodigoTipoOperacionCredito(null);
            param.setDiasCredito(null);
            param.setCantidadCuotas(null);
            //param.setCobrado('S');
        }

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        UsuarioParam uparam = new UsuarioParam();
        uparam.setId(param.getIdUsuario());
        UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);

        if (usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo usuario"));
        }

        NaturalezaCliente naturaleza = facades.getNaturalezaClienteFacade()
                .find(param.getIdNaturalezaCliente(), param.getCodigoNaturalezaCliente());

        if (naturaleza == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la naturaleza del cliente"));
        } else {
            param.setIdNaturalezaCliente(naturaleza.getId());
            param.setCodigoNaturalezaCliente(naturaleza.getCodigo());

            Boolean validarFormatoRuc = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "VALIDAR_FORMATO_RUC", Boolean.TRUE);
            if (validarFormatoRuc) {
                if (param.getCodigoNaturalezaCliente().equalsIgnoreCase("CONTRIBUYENTE")) {
                    if ((param.getRuc().length() < 7 || param.getRuc().length() > 10)) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("Se requiere un nro de RUC válido"));
                    }
                } else {
                    if ((param.getRuc().length() < 1 || param.getRuc().length() > 20)) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("Se requiere un nro de documento válido"));
                    }
                }
            }
        }

        if ("CREDITO".equals(param.getCodigoTipoFactura())) {

            TipoOperacionCredito toc = facades.getTipoOperacionCreditoFacade()
                    .find(param.getIdTipoOperacionCredito(),
                            param.getCodigoTipoOperacionCredito());

            if (toc == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el tipo de operación a crédito"));
            }
        }

        TipoCambioPojo tipoCambio = null;
        if (param.getIdTipoCambio() != null) {
            tipoCambio = facades.getTipoCambioFacade().findFirst(param.getIdTipoCambio());
            if (tipoCambio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el tipo de cambio"));
            }
        }

        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda(), param.getCodigoMoneda());

        if (moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
            param.setCodigoMoneda(moneda.getCodigo());

            if (param.getCodigoMoneda().equals("PYG")) {
                param.setIdTipoCambio(null);
            } else if (tipoCambio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el tipo de cambio"));
            } else if (!tipoCambio.getCodigoMoneda().equals(param.getCodigoMoneda())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La moneda de la factura y tipo de cambio no coinciden"));
            }
        }

        TalonarioPojo talonario;

        if (param.getIdTalonario() != null) {
            TalonarioParam param2 = new TalonarioParam();
            param2.setIdEmpresa(param.getIdEmpresa());
            param2.setId(param.getIdTalonario());
            talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        } else {
            TalonarioParam param2 = new TalonarioParam();
            param2.setIdEmpresa(param.getIdEmpresa());
            param2.setTimbrado(param.getTimbrado());
            param2.setDigital(param.getDigital());
            param2.setNroSucursal(param.getEstablecimiento());
            param2.setNroPuntoVenta(param.getPuntoExpedicion());
            talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        }

        if (talonario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }

        if (!isNull(param.getIdOrdenCompra())) {

            OrdenCompra ordenCompra = facades.getOrdenCompraFacade().find(param.getIdOrdenCompra());

            if (isNull(ordenCompra)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
            }
        }

        if (!isNull(param.getOrdenCompra())) {
            facades.getOrdenCompraFacade().validToCreate(userInfo, param.getOrdenCompra());
        }

        Usuario encargado = facades.getUsuarioFacade().find(param.getIdEncargado());

        if (isNull(encargado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el encargado"));
        }

        ClienteParam paramc = new ClienteParam();
        paramc.setIdEmpresa(param.getIdEmpresa());
        paramc.setNroDocumentoEq(param.getRuc());
        Cliente cliente = facades.getClienteFacade().findFirst(paramc);

        ReferenciaGeograficaParam param3 = new ReferenciaGeograficaParam();

        param3.setId(param.getIdDepartamento());
        Long sizeDepartamento = param.getIdDepartamento() == null
                ? null
                : facades.getReferenciaGeograficaFacade().findSize(param3);

        param3.setId(param.getIdDistrito());
        Long sizeDistrito = param.getIdDistrito() == null
                ? null
                : facades.getReferenciaGeograficaFacade().findSize(param3);

        param3.setId(param.getIdCiudad());
        Long sizeCiudad = param.getIdCiudad() == null
                ? null
                : facades.getReferenciaGeograficaFacade().findSize(param3);

        if (cliente != null) {
            param.setIdCliente(cliente.getIdCliente());
        }

        if (param.getIdDepartamento() != null && sizeDepartamento <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el departamento"));
        }

        if (param.getIdDistrito() != null && sizeDistrito <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el distrito"));
        }

        if (param.getIdCiudad() != null && sizeCiudad <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la ciudad"));
        }

        String ruc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");

        if (ruc == null || ruc.trim().isEmpty()) {
            ruc = "";
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el valor de configuración de RUC"));
        }

        if (param.getDigital().equals('S')) {

            String codSeguridad = "000000000" + String.valueOf(param.getFecha().getTime());
            codSeguridad = codSeguridad.substring(codSeguridad.length() - 9, codSeguridad.length());
            param.setCodSeguridad(codSeguridad);

            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(param.getNroFactura(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch (j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            int i = 0;

            String dRucEm = "";
            String dDVEmi = "";

            for (StringTokenizer stringTokenizer = new StringTokenizer(ruc, "-"); stringTokenizer.hasMoreTokens(); i++) {

                String token = stringTokenizer.nextToken();

                switch (i) {
                    case 0:
                        dRucEm = token;
                        break;

                    case 1:
                        dDVEmi = token;
                        break;
                }
            }

            String tipoContribuyente = "";

            if (dRucEm.length() < 8) {
                tipoContribuyente = String.valueOf(1);
            } else {
                tipoContribuyente = String.valueOf(2);
            }

            String cdc = Document.getCDCWithoutDv("01", dRucEm, dDVEmi, dEst, dPunt, nroDoc,
                    tipoContribuyente, param.getFecha(), "1", param.getCodSeguridad());
            cdc = Document.getCDC(cdc);
            param.setCdc(cdc);
        }

        Date date = Calendar.getInstance().getTime();
        TalonarioParam param1 = new TalonarioParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setId(param.getIdTalonario());
        param1.setIdTipoDocumento(1);
        param1.setDigital(param.getDigital());
        param1.setFecha(date);
        TalonarioPojo talonarioC = facades.getTalonarioFacade().getTalonario(param1);

        if (talonarioC == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un talonario activo para el tipo de documento factura"));
        } else {
            if (!talonarioC.getId().equals(param.getIdTalonario())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El talonario seleccionado no corresponde con el talonario activo"));
            }

            if (!facades.getTalonarioFacade().fechaDocumentoEnRango(talonario, param.getFecha())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la factura debe estar en el rango de vigencia del timbrado"));
            }
        }

        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "FACTURA");

        if (estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }

        FacturaPojo item = obtenerDatosCrear(talonario, null);

        if (param.getAsignarNroFactura() != null
                && param.getAsignarNroFactura()) {
            param.setNroFactura(item.getNroFactura());
        }

        Boolean correlativoNroFactura = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "TALONARIO_CORRELATIVO_FACTURA", Boolean.TRUE);

        if (correlativoNroFactura) {
            if (!item.getNroFactura().equals(param.getNroFactura())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El nro de la factura debe ser correlativo"));
            }
        }

        FacturaParam factRepetido = new FacturaParam();
        factRepetido.setIdTalonario(talonario.getId());
        factRepetido.setIdEmpresa(param.getIdEmpresa());
        factRepetido.setNroFacturaEq(param.getNroFactura());
        FacturaPojo factOc = facades.getFacturaFacade().findFirstPojo(factRepetido);
        if (factOc != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de la factura ya existen en el talonario"));
        }
        if (param.getDigital().equals('N') && item.getFecha().after(param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La factura no puede tener fecha anterior a una creada previamente"));
        }

        Boolean fechaFuturaFactura = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "FECHA_FUTURA_FACTURA", Boolean.FALSE);

        if (!fechaFuturaFactura) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);

            if (cal.getTime().compareTo(param.getFecha()) < 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la factura no puede ser posterior a la fecha actual"));
            }
        }

        if (!facades.getTalonarioFacade().nroDocEnRango(talonario, param.getNroFactura())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de factura no se encuentra en el rango del talonario"));
        }

        for (FacturaDetalleParam detalle : param.getFacturaDetalles()) {
            facades.getFacturaDetalleFacade().validToCreate(detalle, Boolean.FALSE);
        }

        if (!isNull(param.getFacturaDescuentos())) {
            for (FacturaDescuentoParam detalle : param.getFacturaDescuentos()) {
                facades.getFacturaDescuentoFacade().validToCreate(detalle, Boolean.FALSE);
            }
        }

        if (!isNull(param.getFacturaCuotas())) {
            for (FacturaCuotaParam detalle : param.getFacturaCuotas()) {
                facades.getFacturaCuotaFacade().validToCreate(detalle, Boolean.FALSE);
            }
        }

        if (!isNull(param.getFacturaDncp())) {
            facades.getFacturaDncpFacade().validToCreate(param.getFacturaDncp(), Boolean.FALSE);
        }

        if (!isNull(param.getFacturaNotificaciones())) {
            for (FacturaNotificacionParam detalle : param.getFacturaNotificaciones()) {
                facades.getFacturaNotificacionFacade().validToCreate(detalle, Boolean.FALSE);
            }
        }

        if (!isNull(param.getParametrosAdicionales())) {
            for (FacturaParametroAdicionalParam fpa : param.getParametrosAdicionales()) {
                fpa.setIdEmpresa(param.getIdEmpresa());
                fpa.setIdFactura(0);
                facades.getFacturaParametroAdicionalFacade().validToCreate(fpa, false);
            }
        }

        return !param.tieneErrores();
    }

    @Override
    public Boolean reenviarFacturasRechazadas(FacturaParam param) {

        if (!param.isValidToResendRejected()) {
            return Boolean.FALSE;
        }

        List<FacturaPojo> list = findRejected(param);

        for (FacturaPojo factura : list) {
            FacturaParam item = new FacturaParam();
            item.setId(factura.getId());
            item.setRuc(param.getRuc());
            item.setGeneradoSet('N');
            item.setEstadoSincronizado('N');
            editAlt(item);
        }

        return Boolean.TRUE;
    }

    private List<FacturaPojo> findRejected(FacturaParam param) {

        String where = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getCodigoResultado() != null && !param.getCodigoResultado().trim().isEmpty()) {
            where = String.format("%s and dp.codigo_resultado = '%s'", where, param.getCodigoResultado().trim());
        }

        if (param.getFechaInsercionDesde() != null) {
            where = String.format("%s and f.fecha_insercion >= '%s'", where, sdf.format(param.getFechaInsercionDesde()));
        }

        String sql = String.format("select f.id, f.digital, f.generado_set, f.estado_sincronizado\n"
                + "from facturacion.factura f\n"
                + "join info.estado e on f.id_estado=e.id\n"
                + "join set.detalle_procesamiento dp on dp.id_procesamiento=f.id_procesamiento\n"
                + "where f.estado_sincronizado='S' and f.archivo_set='S' and f.generado_set='S'\n"
                + "and e.codigo='RECHAZADO' and f.digital='S' %s\n"
                + "offset %d limit %d", where, param.getFirstResult(), param.getPageSize());

        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<FacturaPojo> result = new ArrayList<>();

        for (Object[] objects : list) {
            int i = 0;
            FacturaPojo item = new FacturaPojo();
            item.setId((Integer) objects[i++]);
            item.setDigital((Character) objects[i++]);
            item.setGeneradoSet((Character) objects[i++]);
            item.setEstadoSincronizado((Character) objects[i++]);
            result.add(item);
        }

        return result;
    }

    public Boolean editAlt(FacturaParam param) {

        if (param.getId() == null) {
            return Boolean.FALSE;
        }

        String set = String.format("id = %d", param.getId());

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            set = String.format("%s, ruc = '%s'", set, param.getRuc().trim());
        }

        if (param.getArchivoEdi() != null) {
            set = String.format("%s, archivo_edi = '%s'", set, param.getArchivoEdi());
        }

        if (param.getArchivoSet() != null) {
            set = String.format("%s, archivo_set = '%s'", set, param.getArchivoSet());
        }

        if (param.getGeneradoEdi() != null) {
            set = String.format("%s, generado_edi = '%s'", set, param.getGeneradoEdi());
        }

        if (param.getGeneradoSet() != null) {
            set = String.format("%s, generado_set = '%s'", set, param.getGeneradoSet());
        }

        if (param.getEstadoSincronizado() != null) {
            set = String.format("%s, estado_sincronizado = '%s'", set, param.getEstadoSincronizado());
        }

        if (param.getIdProcesamiento() != null) {
            set = String.format("%s, id_procesamiento = %d", set, param.getIdProcesamiento());
        }

        String sql = String.format("UPDATE facturacion.factura SET %s WHERE id = %d", set, param.getId());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        q.executeUpdate();

        return Boolean.TRUE;
    }

    @Override
    public Factura edit(FacturaParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Factura factura = find(param.getId());

        if(param.getRuc() != null) {
            factura.setRuc(param.getRuc());
        }
        factura.setEmail(param.getEmail());
        factura.setTelefono(param.getTelefono());
        if(param.getRazonSocial() != null) {
            factura.setRazonSocial(param.getRazonSocial());
        }
        factura.setObservacion(param.getObservacion());
        if(param.getIdNaturalezaCliente() != null) {
            factura.setIdNaturalezaCliente(param.getIdNaturalezaCliente());
        }
        factura.setImpreso(param.getImpreso());
        factura.setEntregado(param.getEntregado());
        factura.setFechaEntrega(param.getFechaEntrega());
        factura.setArchivoEdi(param.getArchivoEdi());
        factura.setArchivoSet(param.getArchivoSet());
        factura.setGeneradoEdi(param.getGeneradoEdi());
        factura.setEstadoSincronizado(factura.getDigital().equals('S')
                && param.getArchivoSet().equals('S')
                && !factura.getGeneradoSet().equals(param.getGeneradoSet())
                ? 'N'
                : param.getEstadoSincronizado() != null
                ? param.getEstadoSincronizado()
                : factura.getEstadoSincronizado());
        factura.setGeneradoSet(param.getGeneradoSet());
        factura.setIdEstado(param.getIdEstado() != null ? param.getIdEstado() : factura.getIdEstado());
        factura.setIdProcesamiento(param.getIdProcesamiento() != null ? param.getIdProcesamiento() : factura.getIdProcesamiento());

        edit(factura);

        return factura;
    }

    @Override
    public Factura cancelarAnulacion(FacturaParam param, UserInfoImpl userInfo) {
        if (!validToUndoCancel(param)) {
            return null;
        }

        String codigoEstado = "APROBADO";

        Factura factura = find(param.getId());

        if (factura.getDigital().equals('N')) {
            EstadoPojo estado = facades.getEstadoFacade().find(null, codigoEstado, "FACTURA");

            factura.setAnulado('N');
            factura.setIdEstado(estado.getId());
            factura.setIdMotivoAnulacion(null);
            factura.setObservacionAnulacion(null);

            edit(factura);

            return factura;

        } else {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Solo se puede cancelar la anulación de una factura no digital"));
            return null;
        }
    }

    @Override
    public Factura anular(FacturaParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        Boolean anular = Boolean.FALSE;
        String codigoEstado = "CANCELADO";

        Factura factura = find(param.getId());

        if (factura.getDigital().equals('N')) {

            anular = Boolean.TRUE;

        } else {

            String urlSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI_JSON");

            if (urlSiedi == null || urlSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance().descripcion("No existe la configuración HOST_SIEDI_JSON"));
                return null;
            }

            EventoService service = new EventoService(userInfo.getToken(), urlSiedi);

            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(factura.getNroFactura(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch (j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            BodyResponse<ResultadoOperacion> result = null;
            BodyResponse<List<ResultadoOperacionSiediApi>> result1 = null;
            String siediApi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API");
            String userSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
            String passSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
            String siediApiHost = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");

            if (factura.getEstado().getCodigo().startsWith("APROBADO")) {

                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.cancelarDteSiediApi(factura.getCdc(),
                            param.getMotivoAnulacion());
                    codigoEstado = "CANCELADO";
                } else {
                    result = service.cancelarDte(factura.getCdc(),
                            param.getMotivoAnulacion());
                    codigoEstado = "CANCELADO";
                }
            } else {
                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.inutilizarSiediApi(factura.getTalonario().getTimbrado(),
                            dEst, dPunt, nroDoc, nroDoc, 1, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                } else {
                    result = service.inutilizar(factura.getTalonario().getTimbrado(),
                            dEst, dPunt, nroDoc, nroDoc, 1, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                }
            }
            if (result != null) {
                if (!result.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result.getPayload().getOperaciones() != null
                            && !result.getPayload().getOperaciones().isEmpty()
                            && result.getPayload().getOperaciones().get(0).getEstado().equalsIgnoreCase("Aprobado")) {
                        anular = Boolean.TRUE;
                    }
                }
            } else if (result1 != null) {
                if (!result1.getSuccess()) {
                    if (result1.getPayload() != null && !result1.getPayload().isEmpty()) {
                        for (ResultadoOperacionSiediApi r : result1.getPayload()) {
                            if (!r.getResultado().getDetalles().isEmpty()) {
                                for (DetalleResultado dr : r.getResultado().getDetalles()) {
                                    param.addError(MensajePojo.createInstance()
                                            .descripcion(dr.getMensaje()));
                                }
                            }
                        }
                    } else {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("Se produjo un error al anular el documento"));
                    }
                } else {
                     if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Rechazado")))) {

                        for (DetalleResultado dr : result1.getPayload().get(0).getResultado().getDetalles()) {
                           if ("4003".equals(dr.getCodigo())) {
                               anular = Boolean.TRUE;
                           } else {
                                param.addError(MensajePojo.createInstance()
                            .descripcion(dr.getMensaje()));
                           }     
                       } 
                    } else if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Aprobado"))
                            || (result1.getPayload().get(0).getResultado().getDetalles() != null
                            && result1.getPayload().get(0).getResultado().getDetalles().stream().filter(item -> item.getCodigo().equals("4003")).count() > 0))) {
                        anular = Boolean.TRUE;
                    } else {
                        if (!result1.getPayload().isEmpty()) {
                            for (ResultadoOperacionSiediApi r : result1.getPayload()) {
                                if (!r.getResultado().getDetalles().isEmpty()) {
                                    for (DetalleResultado dr : r.getResultado().getDetalles()) {
                                        param.addError(MensajePojo.createInstance()
                                                .descripcion(dr.getMensaje()));
                                    }
                                }
                            }
                        } else {
                            param.addError(MensajePojo.createInstance()
                                    .descripcion("Se produjo un error al anular el documento"));
                        }

                    }
                }
            } else {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Se produjo un error al anular el documento"));
            }
        }

        if (anular) {

            EstadoPojo estado = facades.getEstadoFacade().find(null, codigoEstado, "FACTURA");

            factura.setAnulado('S');
            factura.setIdEstado(estado.getId());
            factura.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
            factura.setObservacionAnulacion(param.getObservacionAnulacion());

            edit(factura);

            return factura;
        } else {
            return null;
        }
    }

    private String getCDCWithoutDv(FacturaParam param) {

        String establecimiento = "";
        String puntoExpedicion = "";
        String numeroDocumento = "";

        int i = 0;
        for (StringTokenizer stringTokenizer = new StringTokenizer(param.getNroFactura(), "-"); stringTokenizer.hasMoreTokens(); i++) {
            String token = stringTokenizer.nextToken();
            switch (i) {
                case 0:
                    establecimiento = token;
                    break;
                case 1:
                    puntoExpedicion = token;
                    break;
                case 2:
                    numeroDocumento = token;
                    break;
            }
        }

        String rucEmisor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");
        String ruc = "";
        String dvRuc = "";

        int j = 0;
        for (StringTokenizer stringTokenizer = new StringTokenizer(rucEmisor, "-"); stringTokenizer.hasMoreTokens(); j++) {
            String token = stringTokenizer.nextToken();
            switch (j) {
                case 0:
                    ruc = token;
                    break;
                case 1:
                    dvRuc = token;
                    break;
            }
        }

        String tipoDocumento = "01";//Factura
        String tipoContribuyente = null;
        if (ruc.length() < 8) {
            tipoContribuyente = "1"; //Persona Física
        } else {
            tipoContribuyente = "2"; //Persona Jurídica
        }
        String tipoEmision = "1";//Normal
        String codSeguridad = "000000000" + param.getCodSeguridad();
        codSeguridad = codSeguridad.substring(codSeguridad.length() - 9, codSeguridad.length());

        return Document.getCDCWithoutDv(tipoDocumento, ruc, dvRuc,
                establecimiento, puntoExpedicion, numeroDocumento,
                tipoContribuyente, param.getFecha(), tipoEmision, codSeguridad);
    }

    private String getCDC(FacturaParam param) {
        String cdcWithoutDv = getCDCWithoutDv(param);
        return Document.getCDC(cdcWithoutDv);
    }

    @Override
    public Factura create(FacturaParam param, UserInfoImpl userInfo) {

        if (!isNull(userInfo) && isNull(param.getIdEncargado())) {
            param.setIdEncargado(userInfo.getId());
        }

        if (!validToCreate(userInfo, param)) {
            return null;
        }

        if (!isNull(param.getOrdenCompra())) {
            OrdenCompra ordenCompra = facades.getOrdenCompraFacade().create(param.getOrdenCompra(), userInfo);
            param.setIdOrdenCompra(ordenCompra == null ? null : ordenCompra.getId());
        }

        param.setCodSeguridad(Strings.randomNumeric(9));
        param.setCdc(getCDC(param));

        Factura factura = new Factura();
        factura.setIdEmpresa(param.getIdEmpresa());
        factura.setIdUsuario(param.getIdUsuario());
        factura.setAnulado(param.getAnulado());
        factura.setCdc(param.getCdc());
        factura.setCobrado(param.getCobrado());
        factura.setCodSeguridad(param.getCodSeguridad());
        factura.setIdTipoOperacionCredito(param.getIdTipoOperacionCredito());
        factura.setIdNaturalezaCliente(param.getIdNaturalezaCliente());
        factura.setDiasCredito(param.getDiasCredito());
        factura.setCantidadCuotas(param.getCantidadCuotas());
        factura.setDigital(param.getDigital());
        factura.setDireccion(param.getDireccion());
        factura.setEntregado(param.getEntregado());
        factura.setFecha(param.getFecha());
        factura.setFechaEntrega(param.getFechaEntrega());
        factura.setFechaVencimiento(param.getFechaVencimiento());
        factura.setIdCiudad(param.getIdCiudad());
        factura.setIdCliente(param.getIdCliente());
        factura.setIdDepartamento(param.getIdDepartamento());
        factura.setIdDistrito(param.getIdDistrito());
        factura.setIdMoneda(param.getIdMoneda());
        factura.setIdTalonario(param.getIdTalonario());
        factura.setIdTipoCambio(param.getIdTipoCambio());
        factura.setIdTipoFactura(param.getIdTipoFactura());
        factura.setImpreso(param.getImpreso());
        factura.setMontoImponible10(param.getMontoImponible10().stripTrailingZeros());
        factura.setMontoImponible5(param.getMontoImponible5().stripTrailingZeros());
        factura.setMontoIva10(param.getMontoIva10().stripTrailingZeros());
        factura.setMontoIva5(param.getMontoIva5().stripTrailingZeros());
        factura.setMontoTotal10(param.getMontoTotal10().stripTrailingZeros());
        factura.setMontoTotal5(param.getMontoTotal5().stripTrailingZeros());
        factura.setMontoTotalExento(param.getMontoTotalExento().stripTrailingZeros());
        factura.setMontoTotalDescuentoParticular(param.getMontoTotalDescuentoParticular().stripTrailingZeros());
        factura.setMontoTotalDescuentoGlobal(param.getMontoTotalDescuentoGlobal().stripTrailingZeros());
        factura.setMontoImponibleTotal(param.getMontoImponibleTotal().stripTrailingZeros());
        factura.setMontoIvaTotal(param.getMontoIvaTotal().stripTrailingZeros());
        factura.setMontoTotalFactura(param.getMontoTotalFactura().stripTrailingZeros());
        factura.setMontoTotalGuaranies(param.getMontoTotalGuaranies().stripTrailingZeros());
        factura.setPorcentajeDescuentoGlobal(param.getPorcentajeDescuentoGlobal().stripTrailingZeros());
        factura.setSaldo(param.getMontoTotalGuaranies().stripTrailingZeros());
        factura.setNroCasa(param.getNroCasa());
        factura.setNroFactura(param.getNroFactura());
        factura.setObservacion(param.getObservacion());
        factura.setRazonSocial(param.getRazonSocial());
        factura.setRuc(param.getRuc());
        factura.setTelefono(param.getTelefono());
        factura.setEmail(param.getEmail());
        factura.setIdEncargado(param.getIdEncargado());
        factura.setIdOrdenCompra(param.getIdOrdenCompra());
        factura.setCompraPublica(param.getCompraPublica());
        factura.setArchivoEdi(param.getArchivoEdi());
        factura.setArchivoSet(param.getArchivoSet());
        factura.setGeneradoEdi('N');
        factura.setGeneradoSet('N');
        factura.setEstadoSincronizado(param.getEstadoSincronizado());
        factura.setCodSeguridad(param.getCodSeguridad());
        factura.setCdc(param.getCdc());
        factura.setIdEstado(param.getIdEstado());
        factura.setFechaInsercion(Calendar.getInstance().getTime());
        factura.setIdTipoOperacion(param.getIdTipoOperacion());
        factura.setIdVendedor(param.getIdVendedor());

        if (param.getIdTipoTransaccion() != null) {
            factura.setIdTipoTransaccion(param.getIdTipoTransaccion());
        }

        if (param.getIdLocalOrigen() != null) {
            factura.setIdLocalOrigen(param.getIdLocalOrigen());
        }

        if (param.getIdLocalDestino() != null) {
            factura.setIdLocalDestino(param.getIdLocalDestino());
        }

        create(factura);

        for (FacturaDetalleParam detalle : param.getFacturaDetalles()) {
            detalle.setIdFactura(factura.getId());
            facades.getFacturaDetalleFacade().create(detalle, userInfo);
        }

        if (!isNull(param.getFacturaDescuentos())) {
            for (FacturaDescuentoParam detalle : param.getFacturaDescuentos()) {
                detalle.setIdFactura(factura.getId());
                facades.getFacturaDescuentoFacade().create(detalle, userInfo);
            }
        }

        if (!isNull(param.getFacturaCuotas())) {
            for (FacturaCuotaParam detalle : param.getFacturaCuotas()) {
                detalle.setIdFactura(factura.getId());
                facades.getFacturaCuotaFacade().create(detalle, userInfo);
            }
        }

        if (!isNull(param.getFacturaDncp())) {
            param.getFacturaDncp().setIdFactura(factura.getId());
            facades.getFacturaDncpFacade().create(param.getFacturaDncp(), userInfo);
        }

        if (!isNull(param.getFacturaNotificaciones())) {
            for (FacturaNotificacionParam detalle : param.getFacturaNotificaciones()) {
                detalle.setIdFactura(factura.getId());
                facades.getFacturaNotificacionFacade().create(detalle, userInfo);
            }
        }

        if (factura.getIdOrdenCompra() != null) {
            OrdenCompraParam ocparam = new OrdenCompraParam();
            ocparam.setId(factura.getIdOrdenCompra());
            ocparam.setCodigoEstado("FACTURADO");
            facades.getOrdenCompraFacade().changeState(ocparam, userInfo);
        }

        if (!isNull(param.getCobro())) {
            for (CobroDetalleParam cdp : param.getCobro().getCobroDetalles()) {
                cdp.setIdFactura(factura.getId());
            }
            facades.getCobroFacade().create(param.getCobro(), userInfo);
        }

        if (!isNull(param.getParametrosAdicionales())) {
            for (FacturaParametroAdicionalParam fpa : param.getParametrosAdicionales()) {
                fpa.setIdFactura(factura.getId());
                facades.getFacturaParametroAdicionalFacade().create(fpa, userInfo);
            }
        }

        return factura;
    }

    @Override
    public void generarFactura(Integer id, UserInfoImpl userInfo) {
        try {
            FacturaGeneratorRunnable acpvr = new FacturaGeneratorRunnable(userInfo.getToken(), userInfo.getIdEmpresa());
            acpvr.generarFactura(id);
        } catch (Exception e) {
            log.fatal("", e);
        }
    }

    @Override
    public void actualizarSaldoFactura(Integer idFactura, BigDecimal saldo) {

        if (idFactura == null) {
            return;
        }

        Factura factura = find(idFactura);
        BigDecimal temp = factura.getSaldo();
        temp = temp.subtract(saldo);

        Character cobrado = temp.compareTo(BigDecimal.ONE) > 0 ? 'N' : 'S';

        factura.setSaldo(temp);
        factura.setCobrado(cobrado);

        edit(factura);
    }

    @Override
    public List<FacturaPojo> findPojo(FacturaParam param) {

        AbstractFind find = new AbstractFind(FacturaPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idCliente"),
                        getPath("root").get("idTalonario"),
                        getPath("talonario").get("fechaVencimiento"),
                        getPath("talonario").get("fechaInicio"),
                        getPath("talonario").get("timbrado"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("descripcion"),
                        getPath("moneda").get("codigo"),
                        getPath("root").get("idTipoCambio"),
                        getPath("root").get("idTipoFactura"),
                        getPath("tipoFactura").get("descripcion"),
                        getPath("tipoFactura").get("codigo"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("nroFactura"),
                        getPath("root").get("serie"),
                        getPath("root").get("fecha"),
                        getPath("root").get("fechaVencimiento"),
                        getPath("root").get("razonSocial"),
                        getPath("root").get("direccion"),
                        getPath("root").get("idDepartamento"),
                        getPath("root").get("idDistrito"),
                        getPath("root").get("idCiudad"),
                        getPath("root").get("nroCasa"),
                        getPath("root").get("ruc"),
                        getPath("root").get("telefono"),
                        getPath("root").get("email"),
                        getPath("root").get("anulado"),
                        getPath("root").get("cobrado"),
                        getPath("root").get("impreso"),
                        getPath("root").get("entregado"),
                        getPath("root").get("digital"),
                        getPath("root").get("archivoEdi"),
                        getPath("root").get("archivoSet"),
                        getPath("root").get("generadoEdi"),
                        getPath("root").get("generadoSet"),
                        getPath("root").get("estadoSincronizado"),
                        getPath("root").get("compraPublica"),
                        getPath("root").get("fechaEntrega"),
                        getPath("root").get("diasCredito"),
                        getPath("root").get("cantidadCuotas"),
                        getPath("root").get("idTipoOperacionCredito"),
                        getPath("tipoOperacionCredito").get("descripcion"),
                        getPath("tipoOperacionCredito").get("codigo"),
                        getPath("root").get("idNaturalezaCliente"),
                        getPath("root").get("observacion"),
                        getPath("root").get("idOrdenCompra"),
                        getPath("ordenCompra").get("nroOrdenCompra"),
                        getPath("ordenCompra").get("fechaRecepcion"),
                        getPath("root").get("montoIva5"),
                        getPath("root").get("montoImponible5"),
                        getPath("root").get("montoTotal5"),
                        getPath("root").get("montoIva10"),
                        getPath("root").get("montoImponible10"),
                        getPath("root").get("montoTotal10"),
                        getPath("root").get("montoTotalExento"),
                        getPath("root").get("montoIvaTotal"),
                        getPath("root").get("montoTotalDescuentoGlobal"),
                        getPath("root").get("montoTotalDescuentoParticular"),
                        getPath("root").get("montoImponibleTotal"),
                        getPath("root").get("montoTotalFactura"),
                        getPath("root").get("montoTotalGuaranies"),
                        getPath("root").get("cdc"),
                        getPath("root").get("codSeguridad"),
                        getPath("root").get("idProcesamiento"),
                        getPath("root").get("saldo"),
                        getPath("dncp").get("modalidad"),
                        getPath("dncp").get("entidad"),
                        getPath("dncp").get("anho"),
                        getPath("dncp").get("secuencia"),
                        getPath("dncp").get("fechaEmision"),
                        getPath("root").get("idTipoTransaccion"),
                        getPath("root").get("idTipoOperacion"),
                        getPath("root").get("porcentajeDescuentoGlobal")
                );
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<Factura> find(FacturaParam param) {

        AbstractFind find = new AbstractFind(Factura.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, FacturaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Factura> root = cq.from(Factura.class);
        Join<Factura, Empresa> empresa = root.join("empresa");
        Join<Factura, Moneda> moneda = root.join("moneda");
        Join<Factura, TipoFactura> tipoFactura = root.join("tipoFactura");
        Join<Factura, TipoOperacionCredito> tipoOperacionCredito = root.join("tipoOperacionCredito", JoinType.LEFT);
        Join<Factura, Estado> estado = root.join("estado", JoinType.LEFT);
        Join<Factura, OrdenCompra> ordenCompra = root.join("ordenCompra", JoinType.LEFT);
        Join<Factura, FacturaDncp> dncp = root.join("facturaDncp", JoinType.LEFT);
        Join<Factura, Talonario> talonario = root.join("talonario");
        Join<Factura, Procesamiento> procesamiento = null;
        Join<Procesamiento, DetalleProcesamiento> detalleProcesamiento = null;

        find.addPath("root", root);
        find.addPath("moneda", moneda);
        find.addPath("empresa", empresa);
        find.addPath("tipoFactura", tipoFactura);
        find.addPath("tipoOperacionCredito", tipoOperacionCredito);
        find.addPath("estado", estado);
        find.addPath("ordenCompra", ordenCompra);
        find.addPath("talonario", talonario);
        find.addPath("dncp", dncp);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdLocalTalonario() != null) {
            predList.add(qb.equal(talonario.get("idLocal"), param.getIdLocalTalonario()));
        }

        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }

        if (param.getIdNaturalezaCliente() != null) {
            predList.add(qb.equal(root.get("idNaturalezaCliente"), param.getIdNaturalezaCliente()));
        }

        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            predList.add(qb.equal(ordenCompra.get("nroOrdenCompra"), param.getNroOrdenCompra().trim()));
        }

        if (param.getTieneOrdenCompra() != null) {
            predList.add(param.getTieneOrdenCompra() ? ordenCompra.isNotNull() : ordenCompra.isNull());
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getTimbrado() != null && !param.getTimbrado().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(talonario.<String>get("timbrado")),
                    String.format("%%%s%%", param.getTimbrado().trim().toUpperCase())));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }

        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
            predList.add(root.get("idCliente").in(param.getIdClientes()));
        }

        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }

        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }

        if (param.getIdTipoFactura() != null) {
            predList.add(qb.equal(root.get("idTipoFactura"), param.getIdTipoFactura()));
        }

        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getEntregado() != null) {
            predList.add(qb.equal(root.get("entregado"), param.getEntregado()));
        }

        if (param.getImpreso() != null) {
            predList.add(qb.equal(root.get("impreso"), param.getImpreso()));
        }

        if (param.getCobrado() != null) {
            predList.add(qb.equal(root.get("cobrado"), param.getCobrado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }

        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }

        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }

        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }

        if (param.getCompraPublica() != null) {
            predList.add(qb.equal(root.get("compraPublica"), param.getCompraPublica()));
        }

        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }

        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroFactura")),
                    String.format("%%%s%%", param.getNroFactura().trim().toUpperCase())));
        }

        if (param.getNroFacturaEq() != null && !param.getNroFacturaEq().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroFactura"), param.getNroFacturaEq().trim()));
        }

        if (param.getCodigoResultado() != null && !param.getCodigoResultado().trim().isEmpty()) {
            procesamiento = root.join("procesamiento", JoinType.LEFT);
            detalleProcesamiento = procesamiento.join("detalleProcesamientos", JoinType.LEFT);
            predList.add(qb.equal(detalleProcesamiento.get("codigoResultado"), param.getCodigoResultado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(param.getNroFacturaDescendente() ? qb.desc(root.get("fecha")) : qb.asc(root.get("fecha")),
                param.getNroFacturaDescendente() ? qb.desc(root.get("nroFactura")) : qb.asc(root.get("nroFactura")),
                param.getIdDescendente() ? qb.desc(root.get("id")) : qb.asc(root.get("id")));

        cq.where(pred);

        if (detalleProcesamiento != null) {
            cq.groupBy(root.get("id"), moneda.get("id"), estado.get("id"),
                    talonario.get("id"), ordenCompra.get("id"), tipoFactura.get("id"),
                    empresa.get("id"), tipoOperacionCredito.get("id"));
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Factura> root = cq.from(Factura.class);
        Join<Factura, Empresa> empresa = root.join("empresa");
        Join<Factura, Estado> estado = root.join("estado", JoinType.LEFT);
        Join<Factura, OrdenCompra> ordenCompra = root.join("ordenCompra", JoinType.LEFT);
        Join<Factura, Talonario> talonario = root.join("talonario");
        Join<Factura, Procesamiento> procesamiento;
        Join<Procesamiento, DetalleProcesamiento> detalleProcesamiento;

        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdLocalTalonario() != null) {
            predList.add(qb.equal(talonario.get("idLocal"), param.getIdLocalTalonario()));
        }

        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }

        if (param.getIdNaturalezaCliente() != null) {
            predList.add(qb.equal(root.get("idNaturalezaCliente"), param.getIdNaturalezaCliente()));
        }

        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            predList.add(qb.equal(ordenCompra.get("nroOrdenCompra"), param.getNroOrdenCompra().trim()));
        }

        if (param.getTieneOrdenCompra() != null) {
            predList.add(param.getTieneOrdenCompra() ? ordenCompra.isNotNull() : ordenCompra.isNull());
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getTimbrado() != null && !param.getTimbrado().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(talonario.<String>get("timbrado")),
                    String.format("%%%s%%", param.getTimbrado().trim().toUpperCase())));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }

        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
            predList.add(root.get("idCliente").in(param.getIdClientes()));
        }

        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }

        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }

        if (param.getIdTipoFactura() != null) {
            predList.add(qb.equal(root.get("idTipoFactura"), param.getIdTipoFactura()));
        }

        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getEntregado() != null) {
            predList.add(qb.equal(root.get("entregado"), param.getEntregado()));
        }

        if (param.getImpreso() != null) {
            predList.add(qb.equal(root.get("impreso"), param.getImpreso()));
        }

        if (param.getCobrado() != null) {
            predList.add(qb.equal(root.get("cobrado"), param.getCobrado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }

        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }

        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }

        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }

        if (param.getCompraPublica() != null) {
            predList.add(qb.equal(root.get("compraPublica"), param.getCompraPublica()));
        }

        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }

        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroFactura")),
                    String.format("%%%s%%", param.getNroFactura().trim().toUpperCase())));
        }

        if (param.getNroFacturaEq() != null && !param.getNroFacturaEq().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroFactura"), param.getNroFacturaEq().trim()));
        }

        if (param.getCodigoResultado() != null && !param.getCodigoResultado().trim().isEmpty()) {
            procesamiento = root.join("procesamiento", JoinType.LEFT);
            detalleProcesamiento = procesamiento.join("detalleProcesamientos", JoinType.LEFT);
            predList.add(qb.equal(detalleProcesamiento.get("codigoResultado"), param.getCodigoResultado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }

    @Override
    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param) {

        String where = "true";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = '%d'", where, param.getIdEmpresa());
        }

        if (param.getIdCliente() != null) {
            where = String.format("%s and f.id_cliente = '%d'", where, param.getIdCliente());
        }

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        String sql = String.format("select f.ruc, 109 as tipo_comprobante, f.fecha, t.timbrado, "
                + "f.nro_factura, f.monto_total_10, f.monto_total_5, "
                + "f.monto_total_exento, f.monto_total_factura, "
                + "case when f.id_tipo_factura = 1 then 2 else 1 end as tipo_factura, "
                + "case when m.codigo in ('PYG','Gs.') then 'N' else 'S' end  as codigo_moneda, "
                + "tc.venta, tc.compra "
                + "from facturacion.factura f "
                + "join comercial.moneda m on m.id = f.id_moneda "
                + "join facturacion.talonario t on t.id = f.id_talonario "
                + "left join facturacion.tipo_cambio tc on tc.id = f.id_tipo_cambio "
                + "where %s offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<RegistroComprobantePojo> result = new ArrayList<>();

        for (Object[] objects : list) {
            int i = 0;
            RegistroComprobantePojo pojo = new RegistroComprobantePojo();
            pojo.setIdentificacionComprador((String) objects[i++]);
            pojo.setTipoComprobante((Integer) objects[i++]);
            pojo.setFechaEmisionComprobante((Date) objects[i++]);
            pojo.setNumeroTimbrado(new Integer((String) objects[i++]));
            pojo.setNumeroComprobante((String) objects[i++]);
            pojo.setMontoIva10(new BigDecimal(objects[i++] + ""));
            pojo.setMontoIva5(new BigDecimal(objects[i++] + ""));
            pojo.setMontoExento(new BigDecimal(objects[i++] + ""));
            pojo.setMontoTotal(new BigDecimal(objects[i++] + ""));
            pojo.setCondicionVenta((Integer) objects[i++]);
            pojo.setMonedaExtrangera(((String) objects[i++]).charAt(0));
            pojo.setTipoCambioVenta(new BigDecimal("0" + (objects[i++] == null ? "0" : objects[i])));
            pojo.setTipoCambioCompra(new BigDecimal("0" + (objects[i] == null ? "0" : objects[i])));
            result.add(pojo);
        }

        return result;
    }

    @Override
    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param) {

        String where = "true";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = '%d'", where, param.getIdEmpresa());
        }

        if (param.getIdCliente() != null) {
            where = String.format("%s and f.id_cliente = '%d'", where, param.getIdCliente());
        }

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        String sql = String.format("select count(f.*) "
                + "from facturacion.factura f "
                + "where %s ", where);

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Integer result = object == null
                ? 0
                : ((Number) object).intValue();

        return result;
    }

    @Override
    public List<FacturaPojo> findMonto(FacturaParam param) {

        String where = "true ";

        if (param.getId() != null) {
            where = String.format("%s and f.id = %d", where, param.getId());
        }

        if (param.getIdEstado() != null) {
            where = String.format("%s and f.id_estado = %d", where, param.getIdEstado());
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEstado().trim());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdMoneda() != null) {
            where = String.format("%s and f.id_moneda = %d", where, param.getIdMoneda());
        }

        if (param.getIdCliente() != null) {
            where = String.format("%s and f.id_cliente = %d", where, param.getIdCliente());
        }

        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
            where = String.format("%s and f.id_cliente in (%s)", where, join(param.getIdClientes()));
        }

        if (param.getAnulado() != null) {
            where = String.format("%s and f.anulado = '%s'", where, param.getAnulado());
        }

        if (param.getEntregado() != null) {
            where = String.format("%s and f.entregado = '%s'", where, param.getEntregado());
        }

        if (param.getCobrado() != null) {
            where = String.format("%s and f.cobrado = '%s'", where, param.getCobrado());
        }

        if (param.getArchivoEdi() != null) {
            where = String.format("%s and f.archivo_edi = '%s'", where, param.getArchivoEdi());
        }

        if (param.getArchivoSet() != null) {
            where = String.format("%s and f.archivo_set = '%s'", where, param.getArchivoSet());
        }

        if (param.getGeneradoEdi() != null) {
            where = String.format("%s and f.generado_edi = '%s'", where, param.getGeneradoEdi());
        }

        if (param.getGeneradoSet() != null) {
            where = String.format("%s and f.generado_set = '%s'", where, param.getGeneradoSet());
        }

        if (param.getCompraPublica() != null) {
            where = String.format("%s and f.compra_publica = '%s'", where, param.getCompraPublica());
        }

        if (param.getEstadoSincronizado() != null) {
            where = String.format("%s and f.estado_sincronizado = '%s'", where, param.getEstadoSincronizado());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format("%s and f.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }

        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            where = String.format("%s and oc.nro_orden_compra = '%s'", where, param.getNroOrdenCompra().trim());
        }

        if (param.getIdLocalTalonario() != null) {
            where = String.format("%s and t.id_local = %d", where, param.getIdLocalTalonario());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            where = String.format("%s and f.fecha_vencimiento >= '%s'", where, sdf.format(param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            where = String.format("%s and f.fecha_vencimiento <= '%s'", where, sdf.format(param.getFechaVencimientoHasta()));
        }

        if (param.getTieneRetencion() != null) {
            where = String.format("%s and %s", where, param.getTieneRetencion()
                    ? "not (rd.monto is null)"
                    : "rd.monto is null");
        }

        if (param.getTieneOrdenCompra() != null) {
            where = String.format("%s and %s", where, param.getTieneOrdenCompra()
                    ? "not (oc.id is null)"
                    : "oc.id is null");
        }

        if (param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {
            where = String.format("%s and to_char(f.fecha, 'yyyy-MM') = '%s'", where, param.getAnhoMes());
        }

        String where2 = "true ";

        if (param.getTieneSaldo() != null) {
            where2 = String.format("%s and f.saldo %s", where2,
                    param.getTieneSaldo() ? "> 0" : "<= 0");
        }

        String orderBy = "";

        if (param.getNroFacturaDescendente() != null) {
            orderBy = String.format("%s%s, ", orderBy, param.getNroFacturaDescendente()
                    ? "f.nro_factura desc"
                    : "f.nro_factura asc");
        }

        if (param.getIdDescendente() != null) {
            orderBy = String.format("%s %s, ", orderBy, param.getIdDescendente()
                    ? "f.id desc"
                    : "f.id asc");
        }

        orderBy = String.format("%s f.fecha desc", orderBy);

        String sql = String.format("with f as (with\n"
                + "nc as (select ncd.id_factura, sum(ncd.monto_total) as monto from facturacion.nota_credito_detalle ncd, facturacion.nota_credito nc where ncd.id_nota_credito = nc.id and nc.anulado = 'N' group by ncd.id_factura),\n"
                + "cd as (select cd.id_factura, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura),\n"
                + "rd as (select rd.id_factura, sum(rd.monto_retenido) as monto from facturacion.retencion_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura)\n"
                + "select distinct f.id, f.id_cliente, tf.codigo as codigo_tipo_factura, oc.nro_orden_compra, f.id_talonario, t.timbrado, l.descripcion as descripcion_local_talonario, f.id_moneda, m.codigo as codigo_moneda,\n"
                + "f.id_tipo_cambio, tc.venta, tc.compra, f.id_tipo_factura, f.id_estado, e.codigo as codigo_estado, f.nro_factura, f.fecha, f.fecha_vencimiento, f.razon_social, f.direccion, f.ruc, f.telefono, f.email, f.anulado,\n"
                + "f.cobrado, f.impreso, f.entregado, f.digital, f.archivo_edi, f.archivo_set, f.generado_edi, f.generado_set, f.estado_sincronizado, f.fecha_entrega, f.dias_credito, f.observacion, f.monto_iva_5, f.monto_imponible_5, f.monto_total_5,\n"
                + "f.monto_iva_10, f.monto_imponible_10, f.monto_total_10, f.monto_total_exento, f.monto_iva_total, f.monto_imponible_total, f.monto_total_factura, f.monto_total_guaranies, f.cdc, f.id_procesamiento,\n"
                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
                + "coalesce(cd.monto, 0) as monto_cobro,\n"
                + "coalesce(rd.monto, 0) as monto_retencion,\n"
                + "f.saldo,\n"
                + "p.id as id_comercial, coalesce(p.razon_social, p.nombre || ' ' || p.apellido) as comercial,\n"
                + "f.id_departamento, f.id_distrito, f.id_ciudad, f.direccion as dir, f.nro_casa \n"
                + "from facturacion.factura f\n"
                + "join facturacion.talonario t on t.id = f.id_talonario\n"
                + "left join info.local l on l.id = t.id_local\n"
                + "left join facturacion.orden_compra oc on oc.id = f.id_orden_compra\n"
                + "join comercial.moneda m on m.id = f.id_moneda\n"
                + "join facturacion.tipo_factura tf on tf.id = f.id_tipo_factura\n"
                + "left join info.estado e on e.id = f.id_estado\n"
                + "left join facturacion.tipo_cambio tc on tc.id = f.id_tipo_cambio\n"
                + "left join nc on nc.id_factura = f.id\n"
                + "left join rd on rd.id_factura = f.id\n"
                + "left join cd on cd.id_factura = f.id\n"
                + "left join comercial.cliente c on c.id_cliente = f.id_cliente\n"
                + "left join info.persona p on p.id = c.id_comercial\n"
                + "where %s) select * from f where %s order by %s offset %d limit %d",
                where, where2, orderBy, param.getFirstResult(), param.getPageSize());

        Query q = getEntityManager().createNativeQuery(sql);

        List<FacturaPojo> result = new ArrayList<>();

        List<Object[]> list = q.getResultList();

        for (Object[] objects : list) {
            int i = 0;
            FacturaPojo pojo = new FacturaPojo();
            pojo.setId((Integer) objects[i++]);
            pojo.setIdCliente((Integer) objects[i++]);
            pojo.setCodigoTipoFactura((String) objects[i++]);
            pojo.setNroOrdenCompra((String) objects[i++]);
            pojo.setIdTalonario((Integer) objects[i++]);
            pojo.setTimbrado((String) objects[i++]);
            pojo.setDescripcionLocalTalonario((String) objects[i++]);
            pojo.setIdMoneda((Integer) objects[i++]);
            pojo.setCodigoMoneda((String) objects[i++]);
            pojo.setIdTipoCambio((Integer) objects[i++]);
            pojo.setTipoCambioVenta((Number) objects[i++]);
            pojo.setTipoCambioCompra((Number) objects[i++]);
            pojo.setIdTipoFactura((Integer) objects[i++]);
            pojo.setIdEstado((Integer) objects[i++]);
            pojo.setCodigoEstado((String) objects[i++]);
            pojo.setNroFactura((String) objects[i++]);
            pojo.setFecha((Date) objects[i++]);
            pojo.setFechaVencimiento((Date) objects[i++]);
            pojo.setRazonSocial((String) objects[i++]);
            pojo.setDireccion((String) objects[i++]);
            pojo.setRuc((String) objects[i++]);
            pojo.setTelefono((String) objects[i++]);
            pojo.setEmail((String) objects[i++]);
            pojo.setAnulado((Character) objects[i++]);
            pojo.setCobrado((Character) objects[i++]);
            pojo.setImpreso((Character) objects[i++]);
            pojo.setEntregado((Character) objects[i++]);
            pojo.setDigital((Character) objects[i++]);
            pojo.setArchivoEdi((Character) objects[i++]);
            pojo.setArchivoSet((Character) objects[i++]);
            pojo.setGeneradoEdi((Character) objects[i++]);
            pojo.setGeneradoSet((Character) objects[i++]);
            pojo.setEstadoSincronizado((Character) objects[i++]);
            pojo.setFechaEntrega((Date) objects[i++]);
            pojo.setDiasCredito((Integer) objects[i++]);
            pojo.setObservacion((String) objects[i++]);
            pojo.setMontoIva5((BigDecimal) objects[i++]);
            pojo.setMontoImponible5((BigDecimal) objects[i++]);
            pojo.setMontoTotal5((BigDecimal) objects[i++]);
            pojo.setMontoIva10((BigDecimal) objects[i++]);
            pojo.setMontoImponible10((BigDecimal) objects[i++]);
            pojo.setMontoTotal10((BigDecimal) objects[i++]);
            pojo.setMontoTotalExento((BigDecimal) objects[i++]);
            pojo.setMontoIvaTotal((BigDecimal) objects[i++]);
            pojo.setMontoImponibleTotal((BigDecimal) objects[i++]);
            pojo.setMontoTotalFactura((BigDecimal) objects[i++]);
            pojo.setMontoTotalGuaranies((BigDecimal) objects[i++]);
            pojo.setCdc((String) objects[i++]);
            pojo.setIdProcesamiento((Integer) objects[i++]);
            pojo.setMontoNotaCredito((BigDecimal) objects[i++]);
            pojo.setMontoCobro((BigDecimal) objects[i++]);
            pojo.setMontoRetencion((BigDecimal) objects[i++]);
            pojo.setSaldo((BigDecimal) objects[i++]);
            pojo.setIdComercial((Integer) objects[i++]);
            pojo.setComercial((String) objects[i++]);
            pojo.setIdDepartamento((Integer) objects[i++]);
            pojo.setIdDistrito((Integer) objects[i++]);
            pojo.setIdCiudad((Integer) objects[i++]);
            pojo.setDireccion((String) objects[i++]);
            pojo.setNroCasa((Integer) objects[i++]);
            result.add(pojo);
        }

        return result;
    }

    /**
     * Obtiene el tamaño de la lista de facturas con montos
     *
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public Long findMontoSize(FacturaParam param) {

        String where = "true ";

        if (param.getId() != null) {
            where = String.format("%s and f.id = %d", where, param.getId());
        }

        if (param.getIdEstado() != null) {
            where = String.format("%s and f.id_estado = %d", where, param.getIdEstado());
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEstado().trim());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdMoneda() != null) {
            where = String.format("%s and f.id_moneda = %d", where, param.getIdMoneda());
        }

        if (param.getIdCliente() != null) {
            where = String.format("%s and f.id_cliente = %d", where, param.getIdCliente());
        }

        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
            where = String.format("%s and f.id_cliente in (%s)", where, join(param.getIdClientes()));
        }

        if (param.getAnulado() != null) {
            where = String.format("%s and f.anulado = '%s'", where, param.getAnulado());
        }

        if (param.getEntregado() != null) {
            where = String.format("%s and f.estado = '%s'", where, param.getEntregado());
        }

        if (param.getCobrado() != null) {
            where = String.format("%s and f.cobrado = '%s'", where, param.getCobrado());
        }

        if (param.getArchivoEdi() != null) {
            where = String.format("%s and f.archivo_edi = '%s'", where, param.getArchivoEdi());
        }

        if (param.getArchivoSet() != null) {
            where = String.format("%s and f.archivo_set = '%s'", where, param.getArchivoSet());
        }

        if (param.getGeneradoEdi() != null) {
            where = String.format("%s and f.generado_edi = '%s'", where, param.getGeneradoEdi());
        }

        if (param.getGeneradoSet() != null) {
            where = String.format("%s and f.generado_set = '%s'", where, param.getGeneradoSet());
        }

        if (param.getCompraPublica() != null) {
            where = String.format("%s and f.compra_publica = '%s'", where, param.getCompraPublica());
        }

        if (param.getEstadoSincronizado() != null) {
            where = String.format("%s and f.estado_sincronizado = '%s'", where, param.getEstadoSincronizado());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format("%s and f.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }

        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            where = String.format("%s and oc.nro_orden_compra = '%s'", where, param.getNroOrdenCompra().trim());
        }

        if (param.getIdLocalTalonario() != null) {
            where = String.format("%s and t.id_local = %d", where, param.getIdLocalTalonario());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            where = String.format("%s and f.fecha_vencimiento >= '%s'", where, sdf.format(param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            where = String.format("%s and f.fecha_vencimiento <= '%s'", where, sdf.format(param.getFechaVencimientoHasta()));
        }

        if (param.getTieneRetencion() != null) {
            where = String.format("%s and %s", where, param.getTieneRetencion()
                    ? "not (rd.monto is null)"
                    : "rd.monto is null");
        }

        if (param.getTieneOrdenCompra() != null) {
            where = String.format("%s and %s", where, param.getTieneOrdenCompra()
                    ? "not (oc.id is null)"
                    : "oc.id is null");
        }

        if (param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {
            where = String.format("%s and to_char(f.fecha, 'yyyy-MM') = '%s'", where, param.getAnhoMes());
        }

        String where2 = "true ";

        if (param.getTieneSaldo() != null) {
            where2 = String.format("%s and f.saldo %s", where2,
                    param.getTieneSaldo() ? "> 0" : "<= 0");
        }

        String sql = String.format("with f as (with\n"
                + "nc as (select ncd.id_factura, sum(ncd.monto_total) as monto from facturacion.nota_credito_detalle ncd, facturacion.nota_credito nc where ncd.id_nota_credito = nc.id and nc.anulado = 'N' group by ncd.id_factura),\n"
                + "cd as (select cd.id_factura, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura),\n"
                + "rd as (select rd.id_factura, sum(rd.monto_retenido) as monto from facturacion.retencion_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura)\n"
                + "select f.id,\n"
                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
                + "coalesce(cd.monto, 0) as monto_cobro,\n"
                + "coalesce(rd.monto, 0) as monto_retencion,\n"
                + "f.saldo\n"
                + "from facturacion.factura f\n"
                + "join facturacion.talonario t on t.id = f.id_talonario\n"
                + "left join info.estado e on e.id = f.id_estado\n"
                + "left join facturacion.orden_compra oc on oc.id = f.id_orden_compra\n"
                + "left join nc on nc.id_factura = f.id\n"
                + "left join rd on rd.id_factura = f.id\n"
                + "left join cd on cd.id_factura = f.id\n"
                + "where %s) select count(*) from f where %s", where, where2);

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }

    /**
     * Obtiene el tamaño de la lista de facturas con montos
     *
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public FacturaPojo findMontoSumatoria(FacturaParam param) {

        String where = "true ";

        if (param.getIdCliente() != null) {
            where = String.format("%s and f.id_cliente = %d", where, param.getIdCliente());
        }

        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
            where = String.format("%s and f.id_cliente in (%s)", where, join(param.getIdClientes()));
        }

        if (param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getEntregado() != null) {
            where = String.format("%s and f.estado = '%s'", where, param.getEntregado());
        }

        if (param.getCobrado() != null) {
            where = String.format("%s and f.cobrado = '%s'", where, param.getCobrado());
        }

        if (param.getArchivoEdi() != null) {
            where = String.format("%s and f.archivo_edi = '%s'", where, param.getArchivoEdi());
        }

        if (param.getArchivoSet() != null) {
            where = String.format("%s and f.archivo_set = '%s'", where, param.getArchivoSet());
        }

        if (param.getGeneradoEdi() != null) {
            where = String.format("%s and f.generado_edi = '%s'", where, param.getGeneradoEdi());
        }

        if (param.getGeneradoSet() != null) {
            where = String.format("%s and f.generado_set = '%s'", where, param.getGeneradoSet());
        }

        if (param.getCompraPublica() != null) {
            where = String.format("%s and f.compra_publica = '%s'", where, param.getCompraPublica());
        }

        if (param.getEstadoSincronizado() != null) {
            where = String.format("%s and f.estado_sincronizado = '%s'", where, param.getEstadoSincronizado());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
        }

        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (param.getFechaDesde() != null) {
            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            where = String.format("%s and f.fecha_vencimiento >= '%s'", where, sdf.format(param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            where = String.format("%s and f.fecha_vencimiento <= '%s'", where, sdf.format(param.getFechaVencimientoHasta()));
        }

        String where2 = "true ";

        if (param.getTieneSaldo() != null) {
            where2 = String.format("%s and f.saldo %s", where2,
                    param.getTieneSaldo() ? "> 0" : "<= 0");
        }

        String sql = String.format("with f as (with\n"
                + "nc as (select ncd.id_factura, sum(ncd.monto_total) as monto from facturacion.nota_credito_detalle ncd, facturacion.nota_credito nc where ncd.id_nota_credito = nc.id and nc.anulado = 'N' group by ncd.id_factura),\n"
                + "cd as (select cd.id_factura, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura),\n"
                + "rd as (select rd.id_factura, sum(rd.monto_retenido) as monto from facturacion.retencion_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura)\n"
                + "select f.id, f.id_cliente, f.id_moneda, m.descripcion as moneda, f.id_tipo_factura, tf.descripcion as tipo_factura,\n"
                + "f.nro_factura, f.fecha, f.fecha_vencimiento, f.razon_social, f.direccion, f.ruc, f.telefono, f.anulado,\n"
                + "f.cobrado, f.impreso, f.entregado, f.digital, f.fecha_entrega, f.dias_credito, f.observacion, f.monto_iva_5, f.monto_imponible_5, f.monto_total_5,\n"
                + "f.monto_iva_10, f.monto_imponible_10, f.monto_total_10, f.monto_total_exento, f.monto_iva_total, f.monto_imponible_total, f.monto_total_factura, f.cdc,\n"
                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
                + "coalesce(cd.monto, 0) as monto_cobro,\n"
                + "coalesce(rd.monto, 0) as monto_retencion,\n"
                + "f.monto_total_factura - coalesce(nc.monto, 0) - coalesce(cd.monto, 0) - coalesce(rd.monto, 0) as saldo\n"
                + "from facturacion.factura f\n"
                + "left join comercial.moneda m on m.id = f.id_moneda\n"
                + "left join facturacion.tipo_factura tf on tf.id = f.id_tipo_factura\n"
                + "left join nc on nc.id_factura = f.id\n"
                + "left join rd on rd.id_factura = f.id\n"
                + "left join cd on cd.id_factura = f.id\n"
                + "where %s) select sum(f.monto_total_factura) as monto_total_factura,\n"
                + "sum(f.monto_nota_credito) as monto_nota_credito, sum(f.monto_cobro) as monto_cobro,\n"
                + "sum(f.monto_retencion) as monto_retencion, sum(f.saldo) as saldo, count(*) as cantidad from f where %s", where, where2);

        Query q = getEntityManager().createNativeQuery(sql);

        FacturaPojo pojo = new FacturaPojo();
        pojo.setMontoTotalFactura(BigDecimal.ZERO);
        pojo.setMontoNotaCredito(BigDecimal.ZERO);
        pojo.setMontoCobro(BigDecimal.ZERO);
        pojo.setMontoRetencion(BigDecimal.ZERO);
        pojo.setSaldo(BigDecimal.ZERO);
        pojo.setCantidad(0);

        List<Object[]> list = q.getResultList();

        for (Object[] objects : list) {
            pojo.setMontoTotalFactura((BigDecimal) objects[0]);
            pojo.setMontoNotaCredito((BigDecimal) objects[1]);
            pojo.setMontoCobro((BigDecimal) objects[2]);
            pojo.setMontoRetencion((BigDecimal) objects[3]);
            pojo.setSaldo((BigDecimal) objects[4]);
            pojo.setCantidad(((Number) objects[5]).intValue());
        }

        return pojo;
    }

    @Override
    public byte[] getXmlFactura(FacturaParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        FacturaPojo factura = findFirstPojo(param);

        Closer closer = Closer.instance();

        try {

            String hostSiedi = null;
            Boolean siediApi = param.getSiediApi();
            if (siediApi) {
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");
            } else {
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI");
            }

            if (hostSiedi == null || hostSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra valor para el parámetro HOST_SIEDI/SIEDI_API_HOST"));
                return null;
            }

            Boolean ticket = param.getTicket();
            String cdc = factura.getCdc();

            String nombreArchivo = String.format("F_%s.txt", cdc);
            JsonObject object = null;
            if (siediApi) {

                ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                String userSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                String passSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);

                if (tokenSiedi == null) {

                    param.addError(MensajePojo.createInstance()
                            .descripcion("Credenciales de siediApi incorrectas."));
                    return null;
                }

                return XmlDteAdapter.obtener(tokenSiedi, hostSiedi, cdc, nombreArchivo, object, ticket, true);
            } else {
                return XmlDteAdapter.obtener(null, hostSiedi, cdc, nombreArchivo, object, ticket, false);
            }

        } finally {
            closer.close();
        }
    }

    @Override
    public byte[] getPdfFactura(FacturaParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        FacturaPojo factura = findFirstPojo(param);

        byte[] result = null;

        Closer closer = Closer.instance();

        try {
            if (factura.getDigital().equals('N')) {

                InputStream is = null;
                Boolean jdbc = Boolean.FALSE;

                String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_FACTURA_AUTOIMPRESOR");

                if (templateFacturaAutoimpresor != null && !templateFacturaAutoimpresor.trim().isEmpty()) {
                    is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
                    jdbc = Boolean.FALSE;
                }

                if (isNull(is)) {
                    String templateFacturaAutoimpresorJdbc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_FACTURA_AUTOIMPRESOR_JDBC");
                    is = facades.getReportUtils().getResource(templateFacturaAutoimpresorJdbc, closer);
                    jdbc = Boolean.TRUE;
                }

                if (isNull(is)) {
                    param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para factura autoimpresor"));
                } else {

                    byte[] template = IOUtils.toByteArray(is);

                    List<byte[]> bytesList = Lists.empty();

                    //FacturaAutoimpresorParam faparams = facades.getReportUtils().getParam(closer, factura);
                    if (param.getImprimirOriginal()) {
                        try ( ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
                            FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
                            params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.ORIGINAL);
                            // byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
                            byte[] bytes = generarPdfFactura(closer, template, params, jdbc);
                            if (bytes != null && bytes.length > 0) {
                                bytesList.add(bytes);
                            }
                        }
                    }

                    if (param.getImprimirDuplicado()) {
                        try ( ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
                            FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
                            params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.DUPLICADO);
                            //byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
                            byte[] bytes = generarPdfFactura(closer, template, params, jdbc);
                            if (bytes != null && bytes.length > 0) {
                                bytesList.add(bytes);
                            }
                        }
                    }

                    if (param.getImprimirTriplicado()) {
                        try ( ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
                            FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
                            params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.TRIPLICADO);
                            //byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
                            byte[] bytes = generarPdfFactura(closer, template, params, jdbc);
                            if (bytes != null && bytes.length > 0) {
                                bytesList.add(bytes);
                            }
                        }
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    facades.getReportUtils().merge(bytesList, baos, null);
                    result = baos.toByteArray();
                }

            } else {
                String hostSiedi = null;
                Boolean siediApi = param.getSiediApi() != null
                        ? param.getSiediApi()
                        : facades.getConfiguracionValorFacade().getCVString(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API", "N").equalsIgnoreCase("S");
                if (siediApi) {
                    hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");
                } else {
                    hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI");
                }

                if (hostSiedi == null || hostSiedi.trim().isEmpty()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se encuentra valor para el parámetro HOST_SIEDI/SIEDI_API_HOST"));
                    return null;
                }

                FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
                params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.ORIGINAL);
                Map<String, Object> map = params.toMap();
                JsonObject object = new JsonObject();
                for (Map.Entry<String, Object> entry : map.entrySet()) {

                    if (entry.getValue() != null) {

                        if (entry.getValue() instanceof String) {
                            object.addProperty(entry.getKey(), (String) entry.getValue());
                        }

                        if (entry.getValue() instanceof Character) {
                            object.addProperty(entry.getKey(), (Character) entry.getValue());
                        }

                        if (entry.getValue() instanceof Number) {
                            object.addProperty(entry.getKey(), (Number) entry.getValue());
                        }

                        if (entry.getValue() instanceof Boolean) {
                            object.addProperty(entry.getKey(), (Boolean) entry.getValue());
                        }

                        if (entry.getValue() instanceof JsonArray) {
                            object.add(entry.getKey(), (JsonArray) entry.getValue());
                        }
                    }
                }
                Boolean ticket = param.getTicket();
                String cdc = factura.getCdc();
                String nombreArchivo = String.format("F_%s.txt", cdc);
                if (siediApi) {

                    ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                    String userSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                    String passSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);

                    if (tokenSiedi == null) {

                        param.addError(MensajePojo.createInstance()
                                .descripcion("Credenciales de siediApi ncorrectas."));
                        return null;
                    }
                    result = KudeDteAdapter.obtener(tokenSiedi, hostSiedi, cdc, nombreArchivo, object, ticket, true);
                } else {
                    result = KudeDteAdapter.obtener(null, hostSiedi, cdc, nombreArchivo, object, ticket, false);
                }

            }
        } finally {
            closer.close();
        }

        //agrega una marca de agua si la factura esta anulada
        if ((factura.getAnulado() != null) && factura.getAnulado().equals('S')) {
            try ( ByteArrayOutputStream baos = WatermarkUtils.addWatermarkToExistingPdf(result, "ANULADO", 46)) {
                result = baos.toByteArray();
            }
        }

        return result;
    }

    private byte[] generarPdfFactura(Closer closer, byte[] template, FacturaAutoimpresorParam params, Boolean jdbc) throws Exception {
        try ( ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
            if (jdbc) {
                //Obtenemos una conexion del DataSource
                Context ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                try ( Connection connection = ds.getConnection()) {
                    return JasperReporteGenerator.exportReportToPdfBytes(params, connection, bais);
                }
            } else {
                return JasperReporteGenerator.exportReportToPdfBytes(params, bais);
            }
        }
    }

    @Override
    public byte[] getXlsReporteVenta(FacturaParam param, UserInfoImpl userInfo) throws Exception {

        if (!param.isValidToGetReporteVentaCliente()) {
            return null;
        }

        byte[] result = null;

        Closer closer = Closer.instance();

        try {

            ReporteVentaFacturaParam params = facades.getReportUtils().getReporteVentaFacturaParam(closer, param);

            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_REPORTE_VENTA_FACTURA");
            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);

            if (isNull(is)) {
                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el reporte de venta"));
            } else {

                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                configuration.setOnePagePerSheet(false);
                configuration.setDetectCellType(true);
                //borrar espacio entre filas
                configuration.setRemoveEmptySpaceBetweenRows(true);
                //borrar espacio entre columnas
                configuration.setRemoveEmptySpaceBetweenColumns(true);
                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
                configuration.setIgnoreCellBorder(false);
                configuration.setWhitePageBackground(false);

                //Obtenemos una conexion del DataSource
                Context ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                try ( Connection connection = ds.getConnection()) {
                    result = JasperReporteGenerator.exportReportToXlsxBytes(params, connection, is, configuration);
                }
            }

        } finally {
            closer.close();
        }

        return result;
    }

    @Override
    public byte[] getXlsEstadoCuenta(FacturaParam param, UserInfoImpl userInfo) {

        if (!param.isValidToGetEstadoCuenta()) {
            return null;
        }

        byte[] result = null;

        Closer closer = Closer.instance();

        try {

            Comparator<EstadoCuentaDetalle> comparadorMultiple = Comparator.comparing(EstadoCuentaDetalle::getNroFacturaAsociada)
                    .thenComparing(Comparator.comparing(EstadoCuentaDetalle::getFechaEmision));

            String cliente = null;

            List<EstadoCuentaDetalle> detalles = Lists.empty();

            int i = 0;
            param.setAnulado('N');
            param.setFirstResult(i);
            param.setPageSize(10);

            List<FacturaPojo> facturas = facades.getFacturaFacade().findPojo(param);

            while (!facturas.isEmpty()) {

                for (FacturaPojo factura : facturas) {

                    cliente = factura.getRazonSocial();

                    EstadoCuentaDetalle item = new EstadoCuentaDetalle();
                    item.setIdFacturaAsociada(factura.getId());
                    item.setNroFacturaAsociada(factura.getNroFactura());
                    item.setNroComprobante(factura.getNroFactura());
                    item.setTipoComprobante("Factura");
                    item.setFechaEmision(factura.getFecha());
                    item.setFechaVencimiento(factura.getFechaVencimiento());
                    item.setDiasAtraso(Dates.diference(Calendar.getInstance().getTime(), factura.getFechaVencimiento(), TimeUnit.DAYS));
                    item.setImporte(factura.getMontoTotalFactura());

                    List<EstadoCuentaDetalle> lista = Lists.empty();
                    lista.add(item);

                    RetencionDetalleParam rparam = new RetencionDetalleParam();
                    rparam.setCodigoEstado("CONFIRMADO");
                    rparam.setIdFactura(factura.getId());
                    rparam.setFirstResult(0);
                    rparam.setPageSize(30000);
                    List<RetencionDetalle> retenciones = facades.getRetencionDetalleFacade().find(rparam);

                    for (RetencionDetalle retencion : retenciones) {
                        EstadoCuentaDetalle item1 = new EstadoCuentaDetalle();
                        item1.setIdFacturaAsociada(factura.getId());
                        item1.setNroFacturaAsociada(factura.getNroFactura());
                        item1.setNroComprobante(retencion.getRetencion().getNroRetencion());
                        item1.setTipoComprobante("Retención");
                        item1.setFechaEmision(retencion.getRetencion().getFecha());
                        item1.setImporte(retencion.getMontoRetenido().negate());
                        lista.add(item1);
                    }

                    NotaCreditoDetalleParam ncparam = new NotaCreditoDetalleParam();
                    ncparam.setIdFactura(factura.getId());
                    ncparam.setFirstResult(0);
                    ncparam.setPageSize(30000);
                    List<NotaCreditoDetalle> notaCreditoDetalles = facades.getNotaCreditoDetalleFacade().find(ncparam);

                    for (NotaCreditoDetalle notaCreditoDetalle : notaCreditoDetalles) {

                        if (!notaCreditoDetalle.getNotaCredito().getAnulado().equals('N')) {
                            continue;
                        }

                        EstadoCuentaDetalle item1 = new EstadoCuentaDetalle();
                        item1.setIdFacturaAsociada(factura.getId());
                        item1.setNroFacturaAsociada(factura.getNroFactura());
                        item1.setNroComprobante(notaCreditoDetalle.getNotaCredito().getNroNotaCredito());
                        item1.setTipoComprobante("Nota de Crédito");
                        item1.setFechaEmision(notaCreditoDetalle.getNotaCredito().getFecha());
                        item1.setImporte(notaCreditoDetalle.getMontoTotal().negate());
                        lista.add(item1);
                    }

                    CobroDetalleParam cparam = new CobroDetalleParam();
                    cparam.setIdFactura(factura.getId());
                    cparam.setCodigoEstado("COBRADO");
                    cparam.setFirstResult(0);
                    cparam.setPageSize(30000);
                    List<CobroDetalle> cobroDetalles = facades.getCobroDetalleFacade().find(cparam);

                    for (CobroDetalle cobroDetalle : cobroDetalles) {
                        EstadoCuentaDetalle item1 = new EstadoCuentaDetalle();
                        item1.setIdFacturaAsociada(factura.getId());
                        item1.setNroFacturaAsociada(factura.getNroFactura());
                        item1.setNroComprobante(cobroDetalle.getCobro().getNroRecibo());
                        item1.setTipoComprobante("Recibo");
                        item1.setFechaEmision(cobroDetalle.getCobro().getFecha());
                        item1.setImporte(cobroDetalle.getMontoCobro().negate());
                        lista.add(item1);
                    }

                    lista = lista.stream().sorted(comparadorMultiple).collect(Collectors.toList());

                    for (int j = 0; j < lista.size(); j++) {

                        EstadoCuentaDetalle actual = lista.get(j);
                        EstadoCuentaDetalle anterior = j - 1 < 0 ? null : lista.get(j - 1);

                        if (j == 0) {
                            actual.setSaldoFactura(actual.getImporte());
                            continue;
                        }

                        actual.setSaldoFactura(anterior.getSaldoFactura().add(actual.getImporte()));

                    }

                    detalles.addAll(lista);
                }

                i = i + 10;

                param.setFirstResult(i);

                facturas = facades.getFacturaFacade().findPojo(param);
            }

            detalles = detalles.stream().sorted(comparadorMultiple).collect(Collectors.toList());

            for (int j = 0; j < detalles.size(); j++) {

                EstadoCuentaDetalle actual = detalles.get(j);
                EstadoCuentaDetalle anterior = j - 1 < 0 ? null : detalles.get(j - 1);

                if (j == 0) {
                    actual.setSaldoTotal(actual.getImporte());
                    continue;
                }

                actual.setSaldoTotal(anterior.getSaldoTotal().add(actual.getImporte()));

            }

            EstadoCuentaParam params = facades.getReportUtils().getEstadoCuentaParam(closer, cliente,
                    param.getIdEmpresa(), param.getFechaDesde(), param.getFechaHasta(),
                    detalles.isEmpty() ? BigDecimal.ZERO : detalles.get(detalles.size() - 1).getSaldoTotal(), detalles);

            String templateEstadoCuenta = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_ESTADO_CUENTA");
            InputStream is = facades.getReportUtils().getResource(templateEstadoCuenta, closer);

            if (isNull(is)) {
                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el estado de cuenta"));
            } else {

                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                configuration.setOnePagePerSheet(false);
                configuration.setDetectCellType(false);
                //borrar espacio entre filas
                configuration.setRemoveEmptySpaceBetweenRows(true);
                //borrar espacio entre columnas
                configuration.setRemoveEmptySpaceBetweenColumns(true);
                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
                configuration.setIgnoreCellBorder(false);
                configuration.setWhitePageBackground(false);

                result = JasperReporteGenerator.exportReportToXlsxBytes(params, is, configuration);
            }

        } finally {
            closer.close();
        }

        return result;
    }

    @Override
    public List<String> createFacturaMasiva(List<FacturaParam> fpl, List<FacturaDetalleParam> fdpl, UserInfoImpl userInfo) {

        //Tratamiento para cabecera
        List<String> result = new ArrayList();

        Boolean correlativoNroFactura = facades.getConfiguracionValorFacade().getCVBoolean(userInfo.getIdEmpresa(), "ASIGNAR_NUMERACION_FACTURA", Boolean.FALSE);

        for (FacturaParam facturaParam : fpl) {
            if (!facturaParam.tieneErrores()) {
                FacturaParam fpNuevo = new FacturaParam();
                ClienteParam cp = new ClienteParam();
                List<Cliente> lista = new ArrayList<>();

                //Se obtiene la lista de clientes con el filtro de RUC asignado
                cp.setNroDocumentoEq(facturaParam.getRuc());
                cp.setIdEmpresa(userInfo.getIdEmpresa());
                lista = facades.getClienteFacade().find(cp);
                //Si el identificador de cliente no es null

                TalonarioParam tp = new TalonarioParam();
                FacturaPojo fp = new FacturaPojo();

                String establecimiento = "";
                String puntoExpedicion = "";

                int i = 0;
                for (StringTokenizer stringTokenizer = new StringTokenizer(facturaParam.getNroFactura(), "-"); stringTokenizer.hasMoreTokens(); i++) {
                    String token = stringTokenizer.nextToken();
                    switch (i) {
                        case 0:
                            establecimiento = token;
                            break;
                        case 1:
                            puntoExpedicion = token;
                            break;
                    }
                }

                tp.setDigital(facturaParam.getDigital());
                tp.setIdEmpresa(userInfo.getIdEmpresa());
                tp.setNroSucursal(establecimiento);
                tp.setNroPuntoVenta(puntoExpedicion);
                Date date = Calendar.getInstance().getTime();
                tp.setFecha(date);
                tp.setIdTipoDocumento(1);
                TalonarioPojo talonario = facades
                        .getTalonarioFacade()
                        .getTalonario(tp);

                if (talonario != null) {
                    //TODO: buscar el id moneda por el cod moneda
                    //Se setean valores de cabecera de factura
                    if (!lista.isEmpty() && lista.get(0).getIdCliente() != null) {
                        fp = facades.getFacturaFacade().obtenerDatosCrear(talonario, lista.get(0).getIdCliente());
                        fpNuevo.setIdCliente(lista.get(0).getIdCliente());
                        fpNuevo.setIdNaturalezaCliente(lista.get(0).getIdNaturalezaCliente());
                    }
                    try {
                        ClienteParam paramc = new ClienteParam();
                        paramc.setIdEmpresa(userInfo.getIdEmpresa());
                        paramc.setNroDocumentoEq(fp.getRuc());
                        Cliente cliente = facades.getClienteFacade().findFirst(paramc);
                        if (cliente != null) {
                            fp.setIdCliente(cliente.getIdCliente());
                            fp.setRuc(cliente.getNroDocumento());
                            fp.setRazonSocial(cliente.getRazonSocial());
                        } else {
                            ClienteParam clienteCreate = new ClienteParam();
                            PersonaParam personaCreate = new PersonaParam();
                            clienteCreate.setCodigoEstado("ACTIVO");
                            clienteCreate.setFactElectronica('S');
                            personaCreate.setActivo('S');
                            String[] documento = fp.getRuc().trim().split("-");
                            if (documento.length > 1) {
                                if (Integer.parseInt(documento[0]) > 20000000) {
                                    personaCreate.setCodigoTipoPersona("JURIDICA");
                                    personaCreate.setNombreFantasia(fp.getRazonSocial());
                                    personaCreate.setRazonSocial(fp.getRazonSocial());
                                } else {
                                    personaCreate.setCodigoTipoPersona("FISICA");
                                    String[] nombreCompleto = fp.getRazonSocial().trim().split(" ");
                                    if (nombreCompleto.length > 2) {
                                        personaCreate.setNombre(String.format("%s %s", nombreCompleto[0], nombreCompleto[1]));
                                        personaCreate.setApellido(String.format("%s", nombreCompleto[2]));
                                    } else if (nombreCompleto.length == 2) {
                                        personaCreate.setNombre(nombreCompleto[0]);
                                        personaCreate.setApellido(nombreCompleto[1]);
                                    } else {
                                        personaCreate.setNombre(nombreCompleto[0]);
                                        personaCreate.setApellido(nombreCompleto[0]);
                                    }
                                }
                                clienteCreate.setCodigoNaturalezaCliente("CONTRIBUYENTE");
                                clienteCreate.setRazonSocial(fp.getRazonSocial());
                                clienteCreate.setNroDocumento(fp.getRuc());
                                clienteCreate.setCodigoTipoDocumento("RUC");
                                clienteCreate.setIdEmpresa(userInfo.getIdEmpresa());

                            } else {
                                String[] nombreCompleto = fp.getRazonSocial().trim().split(" ");
                                if (nombreCompleto.length > 2) {
                                    personaCreate.setNombre(String.format("%s %s", nombreCompleto[0], nombreCompleto[1]));
                                    personaCreate.setApellido(String.format("%s", nombreCompleto[2]));
                                } else if (nombreCompleto.length == 2) {
                                    personaCreate.setNombre(nombreCompleto[0]);
                                    personaCreate.setApellido(nombreCompleto[1]);
                                } else {
                                    personaCreate.setNombre(nombreCompleto[0]);
                                    personaCreate.setApellido(nombreCompleto[0]);
                                }
                                clienteCreate.setCodigoNaturalezaCliente("NO_CONTRIBUYENTE");
                                clienteCreate.setNroDocumento(fp.getRuc());
                                clienteCreate.setCodigoTipoDocumento("CI");
                                clienteCreate.setIdEmpresa(userInfo.getIdEmpresa());
                                personaCreate.setCodigoTipoPersona("FISICA");
                            }
                            clienteCreate.setPersona(personaCreate);
                            Cliente cliente2 = facades.getClienteFacade().create(clienteCreate, userInfo);
                            if (cliente2 != null) {
                                fp.setIdCliente(cliente2.getIdCliente());
                            }
                        }

                    } catch (Exception e) {

                    }
                    fpNuevo.setIdEmpresa(userInfo.getIdEmpresa());
                    fpNuevo.setIdMoneda(1);
                    fpNuevo.setNroFactura(facturaParam.getNroFactura());
                    fpNuevo.setAsignarNroFactura(correlativoNroFactura);
                    fpNuevo.setRazonSocial(facturaParam.getRazonSocial());
                    fpNuevo.setRuc(facturaParam.getRuc());
                    if (fpNuevo.getRuc().contains("-")) {
                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                        nat.setCodigo("CONTRIBUYENTE");
                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                        if (n != null) {
                            fpNuevo.setIdNaturalezaCliente(n.getId());
                        }
                    } else {
                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                        nat.setCodigo("NO_CONTRIBUYENTE");
                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                        if (n != null) {
                            fpNuevo.setIdNaturalezaCliente(n.getId());
                        }
                    }
                    fpNuevo.setTelefono(facturaParam.getTelefono());
                    fpNuevo.setEmail(facturaParam.getEmail());
                    fpNuevo.setDigital(facturaParam.getDigital());
                    if (facturaParam.getDigital() == 'S') {
                        fpNuevo.setArchivoSet('S');
                    }
                    fpNuevo.setAnulado('N');
                    fpNuevo.setCobrado('N');
                    fpNuevo.setImpreso('N');
                    fpNuevo.setEntregado('P');
                    fpNuevo.setIdTalonario(talonario.getId());
                    fpNuevo.setIdUsuario(facturaParam.getIdUsuario());
                    fpNuevo.setIdTipoFactura(facturaParam.getCodigoTipoFactura().equalsIgnoreCase("CONTADO") ? 2 : 1);
                    if (fpNuevo.getIdTipoFactura().equals(1)) {
                        fpNuevo.setFecha(facturaParam.getFecha());
                        fpNuevo.setCodigoTipoOperacionCredito("PLAZO");
                        fpNuevo.setDiasCredito(facturaParam.getDiasCredito());

                        Calendar cal = Calendar.getInstance();
                        cal.setTime(facturaParam.getFecha());
                        cal.add(Calendar.DAY_OF_MONTH, facturaParam.getDiasCredito());
                        fpNuevo.setFechaVencimiento(cal.getTime());
                    } else {
                        fpNuevo.setFecha(facturaParam.getFecha());
                        fpNuevo.setFechaVencimiento(facturaParam.getFecha());
                    }

                    List<FacturaDetalleParam> listaDetalleFactura = fdpl
                            .stream()
                            .filter(c -> c.getNroFactura().equals(facturaParam.getNroFactura()))
                            .collect(Collectors.toList());

                    calcularDetalle(fpNuevo, listaDetalleFactura);

                    //Tipo de transaccion
                    String codigoTipoTrans = facades.getConfiguracionValorFacade().getCVString(userInfo.getIdEmpresa(), "TIPO_TRANSACCION", "MIXTO");

                    if (codigoTipoTrans != null && !codigoTipoTrans.trim().isEmpty()) {
                        fpNuevo.setCodigoTipoTransaccion(codigoTipoTrans);
                    }

                    Factura factura = facades
                            .getFacturaFacade()
                            .create(fpNuevo, userInfo);
                    if (factura == null) {
                        String error = String.format("%s;ERROR;", facturaParam.getNroFactura());
                        error = error.concat("%s| ");
                        for (MensajePojo mp : fpNuevo.getErrores()) {
                            error = String.format(error, mp.getDescripcion());
                            error = error.concat("%s| ");
                        }
                        //Al final se eliminan los ultimos 3 caracteres sobrantes %s;
                        error = error.substring(0, error.length() - 5);
                        error = error.concat(";;");
                        result.add(error);
                    } else {
                        String ok = String.format("%s;OK;;%s", facturaParam.getNroFactura(), factura.getId().toString());
                        result.add(ok);
                    }
                } else {
                    String error = String.format("%s;ERROR;No se encuentra registrado un talonario válido;", facturaParam.getNroFactura());
                    result.add(error);
                }
            } else {
                String error = String.format("%s;ERROR;", facturaParam.getNroFactura());
                error = error.concat("%s| ");
                for (MensajePojo mp : facturaParam.getErrores()) {
                    error = String.format(error, mp.getDescripcion());
                    error = error.concat("%s| ");
                }
                //Al final se eliminan los ultimos 3 caracteres sobrantes %s;
                error = error.substring(0, error.length() - 5);
                error = error.concat(";;");
                result.add(error);
            }
        }

        return result;
    }

    @Override
    public Boolean reenviarFacturaMasiva(FacturaParam param, UserInfoImpl userInfo) {

        if (!param.isValidToMasiveResend()) {
            return Boolean.FALSE;
        }

        for (Integer idFactura : param.getIdFacturas()) {

            Factura factura = find(idFactura);
            if (factura == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura"));
                continue;
            }
            factura.setEntregado('P');
            edit(factura);
        }

        return !param.tieneErrores();
    }

    public void calcularDetalle(FacturaParam fpNuevo, List<FacturaDetalleParam> listaDetalleFactura) {
        BigDecimal totalFactura = new BigDecimal("0");
        int i = 0;

        for (FacturaDetalleParam info : listaDetalleFactura) {
            i++;
            info.setPorcentajeGravada(new BigDecimal(100));
            // Se calcula el monto total del detalle
            BigDecimal cantidad = info.getCantidad();
            BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
            BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);

            if (cantidad.compareTo(BigDecimal.ZERO) < 0) {
                MensajePojo msj = new MensajePojo();
                msj.setDescripcion(String.format("Los detalles deben tener canitdad mayor a cero. Cantidad ingresada : %s ", cantidad.toString()));
                fpNuevo.addError(msj);
                continue;
            }

            // Se calcula el monto base
            BigDecimal auxA = new BigDecimal(info.getPorcentajeIva()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxB = info.getPorcentajeGravada().divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
            BigDecimal auxC = auxA.multiply(auxB);
            BigDecimal pfinal = auxC.add(BigDecimal.ONE);
            BigDecimal montoBase = montoPrecioXCantidad.divide(pfinal, 5, RoundingMode.HALF_UP);

            // Se calcula el monto exento gravado
            BigDecimal auxD = new BigDecimal("100").subtract(info.getPorcentajeGravada());
            BigDecimal auxF = auxD.divide(new BigDecimal("100"));
            BigDecimal montoExentoGravado = montoBase.multiply(auxF);

            BigDecimal montoImponible = montoBase.multiply(auxB);

            //Se calcula el monto iva
            // Integer aux3 = info.getPorcentajeIva() / 100;
            BigDecimal montoIva = montoImponible.multiply(auxA);

            //Se suman los montos para el total
            BigDecimal total = (montoExentoGravado.add(montoImponible)).add(montoIva);

            //Precio unitario
            BigDecimal precioUnitarioSinIva = precioUnitarioConIVA.divide(pfinal, 5, RoundingMode.HALF_UP);

            info.setNroLinea(i);
            info.setMontoIva(montoIva);
            info.setMontoImponible(montoImponible);
            info.setMontoExentoGravado(montoExentoGravado);
            info.setMontoTotal(total);
            info.setPrecioUnitarioSinIva(precioUnitarioSinIva);
            info.setDescuentoParticularUnitario(BigDecimal.valueOf(0));
            info.setDescuentoGlobalUnitario(BigDecimal.valueOf(0));
            info.setMontoDescuentoParticular(BigDecimal.valueOf(0));
            info.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
            totalFactura = totalFactura.add(total);
        }
        fpNuevo.setMontoTotalFactura(totalFactura);
        fpNuevo.setMontoTotalGuaranies(totalFactura);
        fpNuevo.setFacturaDetalles(listaDetalleFactura);
        calcularTotalesGenerales(fpNuevo, fpNuevo.getFacturaDetalles());

    }

    public void calcularTotalesGenerales(FacturaParam fpNuevo, List<FacturaDetalleParam> listaDetalle) {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        fpNuevo.setMontoIva5(montoIva5Acumulador);
        fpNuevo.setMontoImponible5(montoImponible5Acumulador);
        fpNuevo.setMontoTotal5(montoTotal5Acumulador);
        fpNuevo.setMontoIva10(montoIva10Acumulador);
        fpNuevo.setMontoImponible10(montoImponible10Acumulador);
        fpNuevo.setMontoTotal10(montoTotal10Acumulador);
        fpNuevo.setMontoTotalExento(montoExcentoAcumulador);
        fpNuevo.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        fpNuevo.setMontoIvaTotal(montoIvaTotalAcumulador);
        fpNuevo.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
        fpNuevo.setMontoTotalDescuentoParticular(new BigDecimal("0"));
    }
    
    
    
    /**
     * Obtiene la cantidad de facturas emitidas y aprobadas.
     *
     * @param param Parámetros
     * @return result
     */
    public Long cantidadFacturasEmitidas(FacturaParam param) {

        if (param.getIdEmpresa() == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Empresa"));
            return null;
        }
        
        String where = "true ";

        String sql = String.format("select count(*) from facturacion.factura f \n" +
                                    "left join info.estado e \n" +
                                    "	on f.id_estado = e.id " +
                                    "where %s \n" +
                                    "and f.id_empresa = %d \n" +
                                    "and anulado = 'N'\n" +
                                    "and e.codigo = 'APROBADO'\n" +
                                    "and fecha_insercion >= NOW() - INTERVAL '30 days';", where, param.getIdEmpresa());

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    
    }
    
    
    /**
     * Obtiene la cantidad de facturas emitidas.
     *
     * @param param Parámetros
     * @return result
     */
    public JsonObject cantidadFacturasEmitidasPorEstado(FacturaParam param) {

        if (param.getIdEmpresa() == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Empresa"));
            return null;
        }

        String sql = String.format( "SELECT \n" +
                                    "	 e.id,\n" +
                                    "   (case \n" +
                                    "       when e.descripcion = 'Aprobado' then 'aprobadas'\n" +
                                    "       when e.descripcion = 'Pendiente' then 'pendientes'\n" +
                                    "       when e.descripcion = 'Rechazado' then 'rechazadas'\n" +
                                    "    end) as estado_descripcion,\n"+
                                    "    COUNT(f.id_estado) AS cantidad_facturas\n" +
                                    "FROM info.estado e\n" +
                                    "LEFT JOIN facturacion.factura f\n" +
                                    "    ON e.id = f.id_estado\n" +
                                    "WHERE \n" +
                                    "f.fecha_insercion >= NOW() - INTERVAL '30 days'  -- Filtro opcional\n" +
                                    "   AND f.id_empresa = %d \n" +
                                    "   AND f.anulado = 'N'\n" +
                                    "   AND f.generado_set = 'S'\n" +
                                    "   AND e.descripcion in ('Aprobado','Pendiente','Rechazado')\n" +
                                    "GROUP BY e.id,e.descripcion\n" +
                                    "ORDER BY e.descripcion;", param.getIdEmpresa());

        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        JsonObject result = new JsonObject();
        
        for (Object[] objects : list) {
            result.addProperty(objects[1].toString(),objects[2].toString());
        }
        
        return result;
        
    }
    
    /**
     * Obtiene la cantidad de productos vendidos
     *
     * @param param Parámetros
     * @return result
     */
    public JsonArray productosMasFacturados(FacturaParam param) {
             
        String sql = String.format(
                "select fd.id_producto, \n"
                + "	coalesce (fd.descripcion,p.descripcion,'--') as producto,\n"
                + "	sum(fd.cantidad) as cantidad \n"
                + "from facturacion.factura_detalle fd\n"
                + "join facturacion.factura f on fd.id_factura = f.id\n"
                + "left join info.producto p on fd.id_producto  = p.id\n"
                + "where f.id_empresa = %d \n"
                + "	and f.anulado = 'N'\n"
                + "	AND f.fecha_insercion >= NOW() - INTERVAL '30 days'\n"
                + "group by fd.id_producto, producto \n"
                + "order by cantidad desc limit 5", param.getIdEmpresa());
        
        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        
        JsonArray result2 = new JsonArray();
        
        for (Object[] objects : list) {
            JsonObject result = new JsonObject();
            result.addProperty("descripcion",objects[1].toString());
            result.addProperty("cantidad",objects[2].toString());
            result2.add(result);
        }
        
        return result2;
    }
    
     /**
     * Obtiene los clientes con mas facturas
     *
     * @param param Parámetros
     * @return result
     */
    public JsonArray clientesMasFacturados(FacturaParam param) {
        
        String codigoEstado = "APROBADO";
        String codigoTipoEstado = "FACTURA";
        EstadoParam estadoFilter = new EstadoParam();
        estadoFilter.setCodigoEstado(codigoEstado);
        estadoFilter.setCodigoTipoEstado(codigoTipoEstado);
       
        Estado estado = facades.getEstadoFacade().findFirst(estadoFilter);
        
        String sql = String.format(
                "select \n" +
                "f.id_cliente,\n" +
                "coalesce (c.razon_social ,f.razon_social ,'--') as cliente,\n" +
                "sum (case \n" +
                "       when f.id_tipo_cambio is null then f.monto_total_factura \n" +
                " 	else f.monto_total_guaranies\n" +
                "	 end) as monto_total	\n" +
                "from facturacion.factura f \n" +
                "left join comercial.cliente c on f.id_cliente  = c.id_cliente \n" +
                "where f.id_empresa = %d\n" +
                "	and f.anulado = 'N'\n" +
                "	and f.id_estado = %d\n" +
                "	and f.generado_set = 'S'\n" +
                "	\n" +
                "group by f.id_cliente , cliente\n" +
                "order by monto_total desc limit 5;", param.getIdEmpresa(),estado.getId());
        
        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        
        JsonArray result2 = new JsonArray();
        
        for (Object[] objects : list) {
            JsonObject result = new JsonObject();
            result.addProperty("descripcion",objects[1].toString());
            result.addProperty("cantidad",objects[2].toString());
            result2.add(result);
        }
        
        return result2;
    }
    
    /**
     * Obtiene el monto total de ventas.
     *
     * @param param Parámetros
     * @return result
     */
    public Long montoTotalVentas(FacturaParam param) {

        if (param.getIdEmpresa() == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Empresa"));
            return null;
        }

        String codigoEstado = "APROBADO";
        String codigoTipoEstado = "FACTURA";
        EstadoParam estadoFilter = new EstadoParam();
        estadoFilter.setCodigoEstado(codigoEstado);
        estadoFilter.setCodigoTipoEstado(codigoTipoEstado);

        Estado estado = facades.getEstadoFacade().findFirst(estadoFilter);

        String sql = String.format("select \n"
                + "	 sum (case \n"
                + "               when f.id_tipo_cambio is null then f.monto_total_factura \n"
                + "               else f.monto_total_guaranies\n"
                + "            end) as monto_total	\n"
                + "from facturacion.factura f \n"
                + "left join comercial.cliente c on f.id_cliente  = c.id_cliente \n"
                + "where f.id_empresa = %d \n"
                + "   and f.anulado = 'N' \n"
                + "   and f.id_estado = %d \n"
                + "   and f.fecha_insercion >= NOW() - INTERVAL '30 days'", param.getIdEmpresa(), estado.getId());

        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    
    }

    public Factura find(Integer id) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Factura.class);
        Root<Factura> root = cq.from(Factura.class);

        cq.select(root);

        cq.where(qb.equal(root.get("id"), id));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(0)
                .setMaxResults(1)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<Factura> result = q.getResultList();

        return result == null || result.isEmpty() ? null : result.get(0);
    }

}
