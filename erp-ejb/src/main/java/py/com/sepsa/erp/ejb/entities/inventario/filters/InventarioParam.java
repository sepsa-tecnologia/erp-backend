/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class InventarioParam extends CommonParam {

    public InventarioParam() {
    }

    public InventarioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de local
     */
    @QueryParam("idLocal")
    private Integer idLocal;
    
    /**
     * Identificador de deposito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;
    
    /**
     * Identificador de depositos logisticos
     */
    @QueryParam("idDepositoLogisticos")
    private List<Integer> idDepositoLogisticos;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de productos
     */
    @QueryParam("idProductos")
    private List<Integer> idProductos;
    
    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private Integer cantidad;
    
    /**
     * Lista de detalle de inventario
     */
    private List<InventarioDetalleParam> inventarioDetalles;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idDepositoLogistico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de depósito logistico"));
        }
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        if(!isNullOrEmpty(inventarioDetalles)) {
            for (InventarioDetalleParam item : inventarioDetalles) {
                item.setIdInventario(0);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNullOrEmpty(inventarioDetalles)) {
            for (InventarioDetalleParam detalle : inventarioDetalles) {
                list.addAll(detalle.getErrores());
            }
        }
        
        return list;
    }
    
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idDepositoLogistico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de depósito logistico"));
        }
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(idEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }
        
        if(isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        return !tieneErrores();
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setInventarioDetalles(List<InventarioDetalleParam> inventarioDetalles) {
        this.inventarioDetalles = inventarioDetalles;
    }

    public List<InventarioDetalleParam> getInventarioDetalles() {
        return inventarioDetalles;
    }

    public void setIdProductos(List<Integer> idProductos) {
        this.idProductos = idProductos;
    }

    public List<Integer> getIdProductos() {
        return idProductos;
    }

    public void setIdDepositoLogisticos(List<Integer> idDepositoLogisticos) {
        this.idDepositoLogisticos = idDepositoLogisticos;
    }

    public List<Integer> getIdDepositoLogisticos() {
        return idDepositoLogisticos;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdLocal() {
        return idLocal;
    }
}
