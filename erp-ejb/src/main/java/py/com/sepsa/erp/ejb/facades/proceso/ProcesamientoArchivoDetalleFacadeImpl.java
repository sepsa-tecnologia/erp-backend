/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivoDetalle;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesamientoArchivoDetalleParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ProcesamientoArchivoDetalleFacade", mappedName = "ProcesamientoArchivoDetalleFacade")
@Local(ProcesamientoArchivoDetalleFacade.class)
public class ProcesamientoArchivoDetalleFacadeImpl extends FacadeImpl<ProcesamientoArchivoDetalle, ProcesamientoArchivoDetalleParam> implements ProcesamientoArchivoDetalleFacade {

    public ProcesamientoArchivoDetalleFacadeImpl() {
        super(ProcesamientoArchivoDetalle.class);
    }

    @Override
    public Boolean validToCreate(ProcesamientoArchivoDetalleParam param, boolean validarIdProcesamientoArchivo) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ProcesamientoArchivo procesamientoArchivo = facades.getProcesamientoArchivoFacade().find(param.getIdProcesamientoArchivo());
        
        if(validarIdProcesamientoArchivo && isNull(procesamientoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el procesamiento de archivo"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public ProcesamientoArchivoDetalle create(ProcesamientoArchivoDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        ProcesamientoArchivoDetalle item = new ProcesamientoArchivoDetalle();
        item.setIdProcesamientoArchivo(param.getIdProcesamientoArchivo());
        item.setDescripcion(param.getDescripcion().trim());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<ProcesamientoArchivoDetalle> find(ProcesamientoArchivoDetalleParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(ProcesamientoArchivoDetalle.class);
        Root<ProcesamientoArchivoDetalle> root = cq.from(ProcesamientoArchivoDetalle.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<ProcesamientoArchivoDetalle> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ProcesamientoArchivoDetalleParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<ProcesamientoArchivoDetalle> root = cq.from(ProcesamientoArchivoDetalle.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
