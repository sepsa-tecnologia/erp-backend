/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemision;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaRemisionPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;

import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Williams Vera
 */
@Local
public interface NotaRemisionFacade extends Facade<NotaRemision, NotaRemisionParam, UserInfoImpl> {

    public NotaRemisionPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente);
    
    public byte[] getPdfNotaRemision(NotaRemisionParam param)throws Exception;
    
    public NotaRemision anular(NotaRemisionParam param, UserInfoImpl userInfo);
    
    public byte[] getXmlNotaRemision(NotaRemisionParam param, UserInfoImpl userInfo) throws Exception;
    
    @Override
    public List<NotaRemisionPojo> findPojo(NotaRemisionParam param);
    
}