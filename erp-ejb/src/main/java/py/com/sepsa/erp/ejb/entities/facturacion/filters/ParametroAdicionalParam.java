/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class ParametroAdicionalParam extends CommonParam  {
    
      public ParametroAdicionalParam() {
    }

    public ParametroAdicionalParam(String bruto) {
        super(bruto);
    }
    
    @QueryParam("codigoTipoParametroAdicional")
    private String codigoTipoParametroAdicional;
    
    @QueryParam("codigoTipoDatoParametroAdicional")
    private String codigoTipoDatoParametroAdicional;
    
    @QueryParam("mandatorio")
    private Character mandatorio;
    
    @QueryParam("visible")
    private Character visible;
    
    @QueryParam("valorDefecto")
    private String valorDefecto;
    
    @QueryParam("datosAdicionales")
    private String datosAdicionales;
    
    
    
      /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe el identificador de empresa"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado Activo"));
        }
        
        if(isNull(codigoTipoParametroAdicional)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de tipo de parametro"));
        }
        
        if(isNullOrEmpty(codigoTipoDatoParametroAdicional)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de tipo de dato del parametro"));
        }
        
        if(isNull(mandatorio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el tipo de parametro"));
        }
        
        if(isNull(visible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de Visible"));
        }
        
        return !tieneErrores();
    }

    public String getCodigoTipoParametroAdicional() {
        return codigoTipoParametroAdicional;
    }

    public void setCodigoTipoParametroAdicional(String codigoTipoParametroAdicional) {
        this.codigoTipoParametroAdicional = codigoTipoParametroAdicional;
    }

    public Character getMandatorio() {
        return mandatorio;
    }

    public void setMandatorio(Character mandatorio) {
        this.mandatorio = mandatorio;
    }

    public Character getVisible() {
        return visible;
    }

    public void setVisible(Character visible) {
        this.visible = visible;
    }

    public String getValorDefecto() {
        return valorDefecto;
    }

    public void setValorDefecto(String valorDefecto) {
        this.valorDefecto = valorDefecto;
    }

    public String getDatosAdicionales() {
        return datosAdicionales;
    }

    public void setDatosAdicionales(String datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }

    public String getCodigoTipoDatoParametroAdicional() {
        return codigoTipoDatoParametroAdicional;
    }

    public void setCodigoTipoDatoParametroAdicional(String codigoTipoDatoParametroAdicional) {
        this.codigoTipoDatoParametroAdicional = codigoTipoDatoParametroAdicional;
    }
    
}
