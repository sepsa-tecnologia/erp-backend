/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.auditoria.Auditoria;
import py.com.sepsa.erp.ejb.entities.auditoria.AuditoriaDetalle;
import py.com.sepsa.erp.ejb.entities.auditoria.Operacion;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.AuditoriaDetalleParam;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.AuditoriaParam;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "AuditoriaDetalleFacade", mappedName = "AuditoriaDetalleFacade")
@Local(AuditoriaDetalleFacade.class)
public class AuditoriaDetalleFacadeImpl extends FacadeImpl<AuditoriaDetalle, AuditoriaDetalleParam> implements AuditoriaDetalleFacade {

    public AuditoriaDetalleFacadeImpl() {
        super(AuditoriaDetalle.class);
    }

    @Override
    public Boolean validToCreate(AuditoriaDetalleParam param, Boolean validarAuditoria) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Auditoria auditoria = facades.getAuditoriaFacade().find(param.getIdAuditoria());
        
        if(validarAuditoria && auditoria == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la auditoria"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        Operacion operacion = facades.getOperacionFacade()
                .find(param.getIdOperacion(), param.getCodigoOperacion());
        
        if(operacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la operación"));
        } else {
            param.setIdOperacion(operacion.getId());
            param.setCodigoOperacion(operacion.getCodigo());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public AuditoriaDetalle create(AuditoriaDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        AuditoriaDetalle result = new AuditoriaDetalle();
        result.setIdUsuario(param.getIdUsuario());
        result.setIdAuditoria(param.getIdAuditoria());
        result.setIdOperacion(param.getIdOperacion());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        result.setCodigo(param.getCodigo().trim());
        create(result);
        
        return result;
    }
    
    @Override
    public AuditoriaDetalle nuevaInstancia(String codigo, UserInfoImpl userInfo) {
        Auditoria auditoria = facades.getAuditoriaFacade().nuevaInstancia(userInfo);
        AuditoriaDetalleParam param = new AuditoriaDetalleParam();
        param.setIdAuditoria(auditoria.getId());
        param.setIdUsuario(userInfo.getId());
        param.setCodigoOperacion("ALTA");
        param.setCodigo(codigo);
        return create(param, userInfo);
    }
    
    @Override
    public AuditoriaDetalle editarInstancia(Integer idAuditoria, String codigo, UserInfoImpl userInfo) {
        
        if(idAuditoria == null) {
            Auditoria auditoria = facades.getAuditoriaFacade().nuevaInstancia(userInfo);
            idAuditoria = auditoria.getId();
        }
        
        AuditoriaDetalleParam param = new AuditoriaDetalleParam();
        param.setIdAuditoria(idAuditoria);
        param.setIdUsuario(userInfo.getId());
        param.setCodigoOperacion("MODIFICACION");
        param.setCodigo(codigo);
        return create(param, userInfo);
    }

    @Override
    public List<AuditoriaDetalle> find(AuditoriaDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(AuditoriaDetalle.class);
        Root<AuditoriaDetalle> root = cq.from(AuditoriaDetalle.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(root.get("idUsuario"), param.getIdUsuario()));
        }
        
        if (param.getIdOperacion() != null) {
            predList.add(qb.equal(root.get("idOperacion"), param.getIdOperacion()));
        }
        
        if (param.getIdAuditoria() != null) {
            predList.add(qb.equal(root.get("idAuditoria"), param.getIdAuditoria()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<AuditoriaDetalle> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(AuditoriaDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<AuditoriaDetalle> root = cq.from(AuditoriaDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(root.get("idUsuario"), param.getIdUsuario()));
        }
        
        if (param.getIdOperacion() != null) {
            predList.add(qb.equal(root.get("idOperacion"), param.getIdOperacion()));
        }
        
        if (param.getIdAuditoria() != null) {
            predList.add(qb.equal(root.get("idAuditoria"), param.getIdAuditoria()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
