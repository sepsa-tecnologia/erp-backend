/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.PlantillaCreacionEmpresa;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuEmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuPerfilParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PlantillaCreacionEmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.Menu;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "PlantillaCreacionEmpresaFacade", mappedName = "PlantillaCreacionEmpresaFacade")
@Local(PlantillaCreacionEmpresaFacade.class)
public class PlantillaCreacionEmpresaFacadeImpl extends FacadeImpl<PlantillaCreacionEmpresa, PlantillaCreacionEmpresaParam> implements PlantillaCreacionEmpresaFacade{
    
    public PlantillaCreacionEmpresaFacadeImpl() {
        super(PlantillaCreacionEmpresa.class);
    }

    public Boolean validToCreate(PlantillaCreacionEmpresaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(PlantillaCreacionEmpresaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
     
        return !param.tieneErrores();
    }
    
    @Override
    public PlantillaCreacionEmpresa create(PlantillaCreacionEmpresaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        PlantillaCreacionEmpresa empresa = new PlantillaCreacionEmpresa();
        empresa.setDescripcion(param.getDescripcion());
        empresa.setCodigo(param.getCodigo());
        empresa.setActivo(param.getActivo());
        
//        for(MenuEmpresaParam mp : param.getMenu().getHijos()){
//            menu.setHijos(setMenuHijos(mp.getHijos()));
//        }
//        
        create(empresa);
       
        return empresa;
    }
    
    //OBS: NO SE USA
    public List<Menu> setMenuHijos(List<MenuEmpresaParam> menu){
        List<Menu> hijos = new ArrayList();
        for (MenuEmpresaParam menuPadre : menu){
            Menu menuHijo = new Menu();
            menuHijo.setActivo(menuPadre.getActivo());
            menuHijo.setCodigo(menuPadre.getCodigo());
            menuHijo.setDescripcion(menuPadre.getDescripcion());
            menuHijo.setUrl(menuPadre.getUrl());
            menuHijo.setIdTipoMenu(menuPadre.getIdTipoMenu());
            menuHijo.setTitulo(menuPadre.getTitulo());
            if (menuPadre.getHijos()!= null && !menuPadre.getHijos().isEmpty()){
                menuHijo.setHijos(setMenuHijos(menuPadre.getHijos()));
            }
            hijos.add(menuHijo);
        }
        return hijos;
    }
    
    @Override
    public PlantillaCreacionEmpresa edit(PlantillaCreacionEmpresaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        PlantillaCreacionEmpresa empresa = find(param.getId());
        empresa.setDescripcion(param.getDescripcion());
        empresa.setCodigo(param.getCodigo());
        empresa.setActivo(param.getActivo());

        
        edit(empresa);
        
        return empresa;
    }
    
    @Override
    public PlantillaCreacionEmpresa find(Integer id, String codigo) {
        PlantillaCreacionEmpresaParam param = new PlantillaCreacionEmpresaParam();
        param.setId(id);
        param.setCodigo(codigo);
        return findFirst(param);
    }
    
    @Override
    public List<PlantillaCreacionEmpresa> find(PlantillaCreacionEmpresaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(PlantillaCreacionEmpresa.class);
        Root<PlantillaCreacionEmpresa> root = cq.from(PlantillaCreacionEmpresa.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<PlantillaCreacionEmpresa> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(PlantillaCreacionEmpresaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<PlantillaCreacionEmpresa> root = cq.from(PlantillaCreacionEmpresa.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
//    @Override
//    public List<EmpresaPojo> findCantidadEmpresas(EmpresaParam param) {
//        
//        String sql = "select e.id_cliente_sepsa, count(*)"
//                + " from info.empresa e"
//                + " where not e.id_cliente_sepsa is null"
//                + " and e.activo = 'S'"
//                + " group by e.id_cliente_sepsa;";
//        
//        Query q = getEntityManager().createNativeQuery(sql);
//        
//        List<Object[]> list = q.getResultList();
//        
//        List<EmpresaPojo> result = new ArrayList();
//        
//        for (Object[] objects : list) {
//            int i = 0;
//            EmpresaPojo item = new EmpresaPojo();
//            item.setIdClienteSepsa((Integer)objects[i++]);
//            item.setCantidad((Number)objects[i++]);
//            result.add(item);
//        }
//        
//        return result;
//    }
//    
//    @Override
//    public List<EmpresaPojo> findCantidadUsuarios(EmpresaParam param) {
//        
//        String sql = "select e.id_cliente_sepsa, count(*)"
//                + " from info.empresa e"
//                + " join usuario.usuario_empresa ue on ue.id_empresa = e.id"
//                + " join usuario.usuario u on ue.id_usuario = u.id"
//                + " join info.estado s on u.id_estado = s.id"
//                + " where not e.id_cliente_sepsa is null"
//                + " and e.activo = 'S'"
//                + " and ue.activo = 'S'"
//                + " and ue.facturable = 'S'"
//                + " and s.codigo = 'ACTIVO'"
//                + " group by e.id_cliente_sepsa;";
//        
//        Query q = getEntityManager().createNativeQuery(sql);
//        
//        List<Object[]> list = q.getResultList();
//        
//        List<EmpresaPojo> result = new ArrayList();
//        
//        for (Object[] objects : list) {
//            int i = 0;
//            EmpresaPojo item = new EmpresaPojo();
//            item.setIdClienteSepsa((Integer)objects[i++]);
//            item.setCantidad((Number)objects[i++]);
//            result.add(item);
//        }
//        
//        return result;
//    }
}
