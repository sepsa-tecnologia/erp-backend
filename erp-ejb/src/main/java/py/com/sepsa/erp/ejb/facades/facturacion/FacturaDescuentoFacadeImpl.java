/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Descuento;
import py.com.sepsa.erp.ejb.entities.comercial.TipoDescuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.DescuentoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaDescuento;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDescuentoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaDescuentoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaDescuentoFacade", mappedName = "FacturaDescuentoFacade")
@Local(FacturaDescuentoFacade.class)
public class FacturaDescuentoFacadeImpl extends FacadeImpl<FacturaDescuento, FacturaDescuentoParam> implements FacturaDescuentoFacade {

    public FacturaDescuentoFacadeImpl() {
        super(FacturaDescuento.class);
    }
    
    @Override
    public Boolean validToCreate(FacturaDescuentoParam param, boolean validarIdFactura) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        FacturaParam param1 = new FacturaParam();
        param1.setId(param.getIdFactura());
        Long size = facades.getFacturaFacade().findSize(param1);
        
        if(validarIdFactura && size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }
        
        if(!isNull(param.getIdDescuento())) {
            DescuentoParam param2 = new DescuentoParam();
            param2.setId(param.getIdDescuento());
            Long size1 = facades.getDescuentoFacade().findSize(param2);

            if(size1 <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el descuento"));
            }
        }
        
        if(!isNull(param.getDescuento())) {
            facades.getDescuentoFacade().validToCreate(param.getDescuento());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public FacturaDescuento create(FacturaDescuentoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        if(!isNull(param.getDescuento())) {
            Descuento descuento = facades.getDescuentoFacade().create(param.getDescuento(), userInfo);
            param.setIdDescuento(Units.execute(() -> descuento.getId()));
        }
        
        FacturaDescuento item = new FacturaDescuento();
        item.setIdFactura(param.getIdFactura());
        item.setIdDescuento(param.getIdDescuento());
        item.setActivo(param.getActivo());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<FacturaDescuentoPojo> findPojo(FacturaDescuentoParam param) {

        AbstractFind find = new AbstractFind(FacturaDescuentoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idFactura"),
                        getPath("root").get("idDescuento"),
                        getPath("descuento").get("descripcion"),
                        getPath("descuento").get("codigo"),
                        getPath("tipoDescuento").get("descripcion"),
                        getPath("tipoDescuento").get("codigo"),
                        getPath("root").get("activo"),
                        getPath("descuento").get("valorDescuento"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<FacturaDescuento> find(FacturaDescuentoParam param) {

        AbstractFind find = new AbstractFind(FacturaDescuento.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, FacturaDescuentoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<FacturaDescuento> root = cq.from(FacturaDescuento.class);
        Join<FacturaDescuento, Descuento> descuento = root.join("descuento", JoinType.LEFT);
        Join<Descuento, TipoDescuento> tipoDescuento = descuento.join("tipoDescuento", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("descuento", descuento);
        find.addPath("tipoDescuento", tipoDescuento);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdDescuento() != null) {
            predList.add(qb.equal(root.get("idDescuento"), param.getIdDescuento()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaDescuentoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaDescuento> root = cq.from(FacturaDescuento.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdDescuento() != null) {
            predList.add(qb.equal(root.get("idDescuento"), param.getIdDescuento()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
