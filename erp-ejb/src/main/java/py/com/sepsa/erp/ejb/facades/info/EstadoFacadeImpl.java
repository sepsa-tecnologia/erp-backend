/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.TipoEstado;
import py.com.sepsa.erp.ejb.entities.info.filters.EstadoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.MotivoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "EstadoFacade", mappedName = "EstadoFacade")
@Local(EstadoFacade.class)
public class EstadoFacadeImpl extends FacadeImpl<Estado, EstadoParam> implements EstadoFacade {

    public EstadoFacadeImpl() {
        super(Estado.class);
    }
    
    public Boolean validToCreate(EstadoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        EstadoParam param1 = new EstadoParam();
        param1.setCodigo(param.getCodigo().trim());
        param1.setIdTipoEstado(param.getIdTipoEstado());
        
        Estado estado = findFirst(param1);
        
        if(estado != null) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro con el mismo código"));
        }
        
        TipoEstado tipoEstado = facades.getTipoEstadoFacade().find(param.getIdTipoEstado());
        
        if(tipoEstado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el tipo de estado"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(EstadoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Estado estado = find(param.getId());
        
        if(estado == null) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
            
        } else if (!estado.getCodigo().equals(param.getCodigo().trim())) {
            
            EstadoParam param1 = new EstadoParam();
            param1.setCodigo(param.getCodigo().trim());
            param1.setIdTipoEstado(param.getIdTipoEstado());

            Estado e = findFirst(param1);

            if(e != null) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro con el mismo código"));
            }
        }
        
        TipoEstado tipoEstado = facades.getTipoEstadoFacade().find(param.getIdTipoEstado());
        
        if(tipoEstado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el tipo de estado"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Estado create(EstadoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Estado result = new Estado();
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setIdTipoEstado(param.getIdTipoEstado());
        
        create(result);
        
        return result;
    }

    @Override
    public Estado edit(EstadoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Estado result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setIdTipoEstado(param.getIdTipoEstado());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public EstadoPojo find(Integer id, String codigoEstado, String codigoTipoEstado) {
        EstadoParam param = new EstadoParam();
        param.setId(id);
        param.setCodigoTipoEstado(codigoTipoEstado);
        param.setCodigo(codigoEstado);
        param.setActivo('S');
        param.setFirstResult(0);
        param.setPageSize(1);
        List<EstadoPojo> list = findPojoAlternative(param);
        return list == null || list.isEmpty() ? null : list.get(0);
    }
    
    @Override
    public List<EstadoPojo> findPojoAlternative(EstadoParam param) {

        AbstractFind find = new AbstractFind(EstadoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigo"),
                        getPath("root").get("activo"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<EstadoPojo> findPojo(EstadoParam param) {

        AbstractFind find = new AbstractFind(EstadoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigo"),
                        getPath("root").get("activo"),
                        getPath("root").get("idTipoEstado"),
                        getPath("tipoEstado").get("descripcion"),
                        getPath("tipoEstado").get("codigo"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<Estado> find(EstadoParam param) {

        AbstractFind find = new AbstractFind(Estado.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, EstadoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Estado> root = cq.from(Estado.class);
        Join<Estado, TipoEstado> tipoEstado = root.join("tipoEstado");
        
        find.addPath("root", root);
        find.addPath("tipoEstado", tipoEstado);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoEstado() != null) {
            predList.add(qb.equal(tipoEstado.get("id"), param.getIdTipoEstado()));
        }

        if (param.getCodigoTipoEstado() != null && !param.getCodigoTipoEstado().trim().isEmpty()) {
            predList.add(qb.equal(tipoEstado.get("codigo"), param.getCodigoTipoEstado().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de estado
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(EstadoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Estado> root = cq.from(Estado.class);
        Join<Estado, TipoEstado> te = root.join("tipoEstado");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoEstado() != null) {
            predList.add(qb.equal(te.get("id"), param.getIdTipoEstado()));
        }

        if (param.getCodigoTipoEstado() != null && !param.getCodigoTipoEstado().trim().isEmpty()) {
            predList.add(qb.equal(te.get("codigo"), param.getCodigoTipoEstado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
