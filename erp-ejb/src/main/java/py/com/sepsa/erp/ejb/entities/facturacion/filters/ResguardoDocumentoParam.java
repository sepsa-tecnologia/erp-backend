/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de resguardo de documento
 * @author Jonathan D. Bernal Fernández
 */
public class ResguardoDocumentoParam extends CommonParam {
    
    
    public ResguardoDocumentoParam() {
    }

    public ResguardoDocumentoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de repositorio
     */
    @QueryParam("idRepositorio")
    private Integer idRepositorio;
    
    /**
     * Identificador de tipo de documento
     */
    @QueryParam("idTipoDocumento")
    private Integer idTipoDocumento;
    
    /**
     * Código de tipo de documento
     */
    @QueryParam("codigoTipoDocumento")
    private String codigoTipoDocumento;
    
    /**
     * Nombre
     */
    @QueryParam("nombreArchivo")
    private String nombreArchivo;
    
    /**
     * URL
     */
    @QueryParam("url")
    private String url;
    
    /**
     * Fecha de inserción
     */
    @QueryParam("fechaInsercion")
    private Date fechaInsercion;
    
    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoDocumento) && isNullOrEmpty(codigoTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de tipo de documento"));
        }
        
        if(isNull(idRepositorio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de repositorio"));
        }
        
        if(isNullOrEmpty(url)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la URL"));
        }
        
        if(isNullOrEmpty(nombreArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el nombre de archivo"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToGenerateDownloadUrl() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de resguardo de documento"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @return bandera
     */
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador del repositorio"));
        }
        
        if(isNull(idTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de documento"));
        }
        
        if(isNull(idRepositorio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de repositorio"));
        }
        
        if(isNullOrEmpty(url)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la URL"));
        }
        
        if(isNullOrEmpty(nombreArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el nombre de archivo"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdRepositorio() {
        return idRepositorio;
    }

    public void setIdRepositorio(Integer idRepositorio) {
        this.idRepositorio = idRepositorio;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }
}
