/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.amazonaws.util.IOUtils;
import com.google.gson.JsonObject;
import com.lowagie.text.PageSize;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import py.com.sepsa.dtecore.v150.handler.Document;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmision;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebitoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefono;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.KudeDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.adapters.XmlDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.facades.WatermarkUtils;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.erp.reporte.pojos.notadebito.NotaDebitoAutoimpresorParam;
import py.com.sepsa.erp.reporte.pojos.notadebito.ReporteVentaNotaDebitoParam;
import py.com.sepsa.utils.rest.client.ConnectionPojo;
import py.com.sepsa.utils.rest.client.body.BodyResponse;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import py.com.sepsa.utils.siediApi.pojos.ResultadoOperacionSiediApi;
import py.com.sepsa.utils.siediApi.remote.LoginServiceSiediApi;
import py.com.sepsa.utils.siediApi.remote.SiediApiServiceClient;
import py.com.sepsa.utils.siedimonitor.pojos.DetalleResultado;
import py.com.sepsa.utils.siedimonitor.pojos.ResultadoOperacion;
import py.com.sepsa.utils.siedimonitor.remote.EventoService;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "NotaDebitoFacade", mappedName = "NotaDebitoFacade")
@javax.ejb.Local(NotaDebitoFacade.class)
public class NotaDebitoFacadeImpl extends FacadeImpl<NotaDebito, NotaDebitoParam> implements NotaDebitoFacade {

    public NotaDebitoFacadeImpl() {
        super(NotaDebito.class);
    }
    
    /**
     * Obtiene los datos como para crear
     * @param talonario talonario
     * @param idCliente Identificador de cliente
     * @return Datos
     */
    @Override
    public NotaDebitoPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente) {
        
        Date fecha = facades.getTalonarioFacade().ultimaFechaDoc(talonario, 3);
        String nroNotaDebito = facades.getTalonarioFacade().sigNumNotaDebito(talonario);

        String razonSocial = null;
        String ruc = null;
        String direccion = null;
        Integer nroCasa = null;
        Integer idDepartamento = null;
        Integer idDistrito = null;
        Integer idCiudad = null;
        String email = null;
        String telefono = null;

        Cliente cliente = idCliente == null
                ? null
                : facades.getClienteFacade().find(idCliente);

        if (cliente != null) {
            email = Units.execute(()-> facades.getPersonaEmailFacade().obtenerEmailCliente(idCliente).getEmail().getEmail());
            telefono = Units.execute(()-> {
                PersonaTelefono item = facades.getPersonaTelefonoFacade().obtenerTelefonoCliente(idCliente);
                return String.format("%s%s", item.getTelefono().getPrefijo(), item.getTelefono().getNumero());
            });
            razonSocial = cliente.getRazonSocial();
            ruc = cliente.getNroDocumento();
            direccion = Units.execute(()-> cliente.getPersona().getDireccion().getDireccion());
            nroCasa = Units.execute(()-> cliente.getPersona().getDireccion().getNumero());
            idDepartamento = Units.execute(()-> cliente.getPersona().getDireccion().getDepartamento().getId());
            idDistrito = Units.execute(()-> cliente.getPersona().getDireccion().getDistrito().getId());
            idCiudad = Units.execute(()-> cliente.getPersona().getDireccion().getCiudad().getId());
        }
        
        NotaDebitoPojo item = new NotaDebitoPojo();
        item.setDigital(talonario.getDigital());
        item.setIdTalonario(talonario.getId());
        item.setFechaVencimientoTimbrado(talonario.getFechaVencimiento());
        item.setTimbrado(talonario.getTimbrado());
        item.setFecha(fecha);
        item.setNroNotaDebito(nroNotaDebito);
        item.setRazonSocial(razonSocial);
        item.setRuc(ruc);
        item.setDireccion(direccion);
        item.setNroCasa(nroCasa);
        item.setIdDepartamento(idDepartamento);
        item.setIdDistrito(idDistrito);
        item.setIdCiudad(idCiudad);
        item.setEmail(email);
        item.setTelefono(telefono);
        
        return item;
    }
    
    @Override
    public NotaDebito createFromInvoice(NotaDebitoParam ncparam, UserInfoImpl userInfo) {
        
        TalonarioParam tparam = new TalonarioParam();
        tparam.setIdEmpresa(ncparam.getIdEmpresa());
        tparam.setDigital(ncparam.getDigital());
        tparam.setFecha(Calendar.getInstance().getTime());
        tparam.setIdTipoDocumento(3);
        tparam.setId(ncparam.getIdTalonario());
        tparam.setIdEncargado(ncparam.getIdEncargado());
        tparam.setIdUsuario(ncparam.getIdUsuario());
        tparam.setIdExternoLocal(ncparam.getIdExternoLocal());
        tparam.setIdLocal(ncparam.getIdLocal());
        tparam.setIdLocales(ncparam.getIdLocales());
        
        if(!validToCreateFromInvoice(tparam, ncparam)) {
            return null;
        }
        
        FacturaParam fparam = new FacturaParam();
        fparam.setId(ncparam.getIdFactura());
        FacturaPojo invoice = facades.getFacturaFacade().findFirstPojo(fparam);
        
        NotaDebitoParam param = facades
                .getInvoiceUtils()
                .generateNdFromPojo(ncparam, invoice);
        
        return create(param, userInfo);
    }
    
    public Boolean validToCreateFromInvoice(TalonarioParam tparam, NotaDebitoParam ncparam) {
        
        if(!ncparam.isValidToCreateFromInvoice()) {
            return Boolean.FALSE;
        }
        
        if(!tparam.datosCrearValido()) {
            for (MensajePojo error : tparam.getErrores()) {
                ncparam.addError(error);
            }
        } else {
            TalonarioPojo talonario = facades
                    .getTalonarioFacade()
                    .getTalonario(tparam);
            
            if(talonario == null) {
                ncparam.addError(MensajePojo.createInstance()
                        .descripcion("No existe se encuentra un talonario disponible"));
            } else {
                NotaDebitoPojo item = facades
                        .getNotaDebitoFacade()
                        .obtenerDatosCrear(talonario, tparam.getIdCliente());
                ncparam.setDigital(item.getDigital());
                ncparam.setIdTalonario(item.getIdTalonario());
                ncparam.setNroNotaDebito(item.getNroNotaDebito());
            }
        }
        
        FacturaParam fparam = new FacturaParam();
        fparam.setId(ncparam.getIdFactura());
        Long size = facades.getFacturaFacade().findSize(fparam);
        
        if(size <= 0) {
            ncparam.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la factura"));
        }
        
        return !ncparam.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToEdit(NotaDebitoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        NotaDebito nc = facades
                .getNotaDebitoFacade()
                .find(param.getId());
        
        if(nc == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de débito"));
        }
        
        if(param.getIdEstado() != null || param.getCodigoEstado() != null) {
            EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                    param.getCodigoEstado(), "NOTA_DEBITO");

            if(estado == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
            } else {
                param.setIdEstado(estado.getId());
                param.setCodigoEstado(estado.getCodigo());
            }
        }
        
        if(param.getIdProcesamiento() != null) {
            
            Procesamiento procesamiento = facades.getProcesamientoFacade()
                    .find(param.getIdProcesamiento());

            if(procesamiento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el registro de procesamiento"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToGetPdf(NotaDebitoParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        NotaDebito notaDebito = find(param.getId());

        if (notaDebito == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de débito"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToCancel(NotaDebitoParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "NOTA_DEBITO");

        if (motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        } else {
            param.setIdMotivoAnulacion(motivoAnulacion.getId());
            param.setCodigoMotivoAnulacion(motivoAnulacion.getCodigo());
            param.setMotivoAnulacion(motivoAnulacion.getDescripcion());
        }
        
        NotaDebito notaDebito = facades.getNotaDebitoFacade().find(param.getId());

        if (notaDebito == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la nota de débito"));
        } else {

            if (Objects.equals(notaDebito.getAnulado(), 'S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La nota de débito ya se encuentra anulada"));
            }
            
            String diaDeclaracionMes = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "DIA_DECLARACION_MES");

            Integer diaDeclaracion = Units.execute(() -> Integer.valueOf(diaDeclaracionMes));

            if ((param.getIgnorarPeriodoAnulacion() == null
                    || !param.getIgnorarPeriodoAnulacion())
                    && !isNull(diaDeclaracion)) {

                Calendar inv = Calendar.getInstance();
                inv.setTime(notaDebito.getFecha());
                inv.add(Calendar.DAY_OF_MONTH, diaDeclaracion);
                inv.set(Calendar.HOUR_OF_DAY, 0);
                inv.set(Calendar.MINUTE, 0);
                inv.set(Calendar.SECOND, 0);
                
                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);

                if (inv.compareTo(now) < 0) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Sólo se pueden anular NDs que esten dentro del periodo de anulación"));
                }
            }
        }

        return !param.tieneErrores();
    }
    
    public Boolean validToCreate(NotaDebitoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda(), param.getCodigoMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
            param.setCodigoMoneda(moneda.getCodigo());
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "NOTA_DEBITO");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        UsuarioParam uparam = new UsuarioParam();
        uparam.setId(param.getIdUsuario());
        UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);

        if (usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo usuario"));
        }
        
        TalonarioPojo talonario;
        
        if(param.getIdTalonario() != null) {
            TalonarioParam param2 = new TalonarioParam();
            param2.setIdEmpresa(param.getIdEmpresa());
            param2.setId(param.getIdTalonario());
            talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        } else {
            TalonarioParam param2 = new TalonarioParam();
            param2.setIdEmpresa(param.getIdEmpresa());
            param2.setTimbrado(param.getTimbrado());
            param2.setDigital(param.getDigital());
            param2.setNroSucursal(param.getEstablecimiento());
            param2.setNroPuntoVenta(param.getPuntoExpedicion());
            talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        }
        
        Cliente cliente = param.getIdCliente() == null
                ? null
                : facades.getClienteFacade().find(param.getIdCliente());
        
        ReferenciaGeografica departamento = param.getIdDepartamento() == null
                ? null
                : facades.getReferenciaGeograficaFacade().find(param.getIdDepartamento());
        
        ReferenciaGeografica distrito = param.getIdDistrito()== null
                ? null
                : facades.getReferenciaGeograficaFacade().find(param.getIdDistrito());
        
        ReferenciaGeografica ciudad = param.getIdCiudad()== null
                ? null
                : facades.getReferenciaGeograficaFacade().find(param.getIdCiudad());
        
        if(param.getIdCliente() != null && cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        if(param.getIdDepartamento() != null && departamento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el departamento"));
        }
        
        if(param.getIdDistrito()!= null && distrito == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el distrito"));
        }
        
        if(param.getIdCiudad()!= null && ciudad == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la ciudad"));
        }
        
        String ruc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");
        
        if(ruc == null || ruc.trim().isEmpty()) {
            ruc = "";
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el valor de configuración de RUC"));
        }
        

        
        if(param.getIdMotivoEmisionInterno() != null
                || param.getCodigoMotivoEmisionInterno() != null) {
            
            MotivoEmisionInterno motivoEmisionInterno = facades
                    .getMotivoEmisionInternoFacade()
                    .find(param.getIdEmpresa(), param.getIdMotivoEmisionInterno(),
                            param.getCodigoMotivoEmisionInterno());
            
            if(isNull(motivoEmisionInterno)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el motivo de emisión interno"));
            } else {
                param.setIdMotivoEmision(motivoEmisionInterno.getIdMotivoEmision());
            }
        }
        
        Usuario encargado = facades.getUsuarioFacade().find(param.getIdEncargado());
        
        if(isNull(encargado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el encargado"));
        }
        
        Date date = Calendar.getInstance().getTime();
        TalonarioParam param1 = new TalonarioParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setId(param.getIdTalonario());
        param1.setIdTipoDocumento(6);
        param1.setDigital(param.getDigital());
        param1.setFecha(date);
        TalonarioPojo talonarioC = facades.getTalonarioFacade().getTalonario(param1);
        
        if(talonarioC == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un talonario activo para el tipo de documento nota de débito"));
        } else {
            if(!talonarioC.getId().equals(param.getIdTalonario())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El identificador de talonario no corresponde con el activo"));
            }
            
            if (!facades.getTalonarioFacade().fechaDocumentoEnRango(talonario, param.getFecha())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la nota de débito debe estar en el rango de vigencia del timbrado"));
            }
        }
        
        if(!facades.getTalonarioFacade().nroDocEnRango(talonario, param.getNroNotaDebito())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de nota de débito no se encuentra en el rango del talonario"));
        }
        
        NotaDebitoPojo item = obtenerDatosCrear(talonario, null);
        
        Boolean correlativoNroNotaDebito = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "TALONARIO_CORRELATIVO_NOTA_DEBITO", Boolean.TRUE);
        
        if (correlativoNroNotaDebito) {
            if (!item.getNroNotaDebito().equals(param.getNroNotaDebito())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El nro de la nota de débito debe ser correlativo"));
            }
        }
        
        NotaDebitoParam nDebitoRepetido = new NotaDebitoParam();
        nDebitoRepetido.setIdTalonario(talonario.getId());
        nDebitoRepetido.setIdEmpresa(param.getIdEmpresa());
        nDebitoRepetido.setNroNotaDebito(param.getNroNotaDebito());
        NotaDebitoPojo NDOc = facades.getNotaDebitoFacade().findFirstPojo(nDebitoRepetido);
        if(NDOc!=null){
            param.addError(MensajePojo.createInstance()
                        .descripcion("El nro de la nota de débito ya existe en el talonario"));
        }
        
        if(param.getDigital().equals('N') && item.getFecha().after(param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La nota de débito no puede tener fecha a una creada previamente"));
        }
        
        if(!validarFechaNotaDebito(param)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La nota de débito no puede tener fecha anterior a la última fecha de la factura"));
        }
        
        for (NotaDebitoDetalleParam detalle : param.getNotaDebitoDetalles()) {
            facades.getNotaDebitoDetalleFacade().validToCreate(detalle, false);
        }
        
        
        if(param.getDigital().equals('S')) {
            
            String codSeguridad =  "000000000" + String.valueOf(param.getFecha().getTime());
            codSeguridad = codSeguridad.substring(codSeguridad.length() - 9, codSeguridad.length());
            param.setCodSeguridad(codSeguridad);
                
            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(param.getNroNotaDebito(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch(j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            int i = 0;

            String dRucEm = "";
            String dDVEmi = "";
            
            for (StringTokenizer stringTokenizer = new StringTokenizer(ruc, "-"); stringTokenizer.hasMoreTokens(); i++) {

                String token = stringTokenizer.nextToken();

                switch(i) {
                    case 0:
                        dRucEm = token;
                        break;

                    case 1:
                        dDVEmi = token;
                        break;
                }
            }
            
            String tipoContribuyente = null;
            if (dRucEm.length()<8){
                tipoContribuyente = "1"; //Persona Física
            } else {
                tipoContribuyente = "2"; //Persona Jurídica
            }
            
            String cdc = Document.getCDCWithoutDv("06", dRucEm, dDVEmi, dEst, dPunt, nroDoc,
                    tipoContribuyente, param.getFecha(), "1", param.getCodSeguridad());
            cdc = Document.getCDC(cdc);
            param.setCdc(cdc);
        }
        
        return !param.tieneErrores();
    }
    
    private Boolean validarFechaNotaDebito(NotaDebitoParam param) {
        
        Date fechaFactura = null;
        
        for (NotaDebitoDetalleParam item : param.getNotaDebitoDetalles()) {
            
            if(item.getIdFactura() != null) {
                Factura factura = facades.getFacturaFacade().find(item.getIdFactura());

                if(factura != null) {
                    fechaFactura = fechaFactura == null
                            ? factura.getFecha()
                            : factura.getFecha().after(fechaFactura)
                            ? factura.getFecha()
                            : fechaFactura;
                }
            }
        }
        
        return fechaFactura == null || fechaFactura.compareTo(param.getFecha()) <= 0;
    }
    
    /**
     * Edita una instancia
     * @param param parámetros
     * @return instancia
     */
    @Override
    public NotaDebito edit(NotaDebitoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        NotaDebito nd = facades
                .getNotaDebitoFacade()
                .find(param.getId());
        
        nd.setObservacion(param.getObservacion());
        nd.setImpreso(param.getImpreso());
        nd.setEntregado(param.getEntregado());
        nd.setFechaEntrega(param.getFechaEntrega());
        nd.setArchivoEdi(param.getArchivoEdi());
        nd.setArchivoSet(param.getArchivoSet());
        nd.setGeneradoEdi(param.getGeneradoEdi());
        nd.setEstadoSincronizado(nd.getDigital().equals('S')
                && param.getArchivoSet().equals('S')
                && !nd.getGeneradoSet().equals(param.getGeneradoSet())
                ? 'N'
                : param.getEstadoSincronizado() != null
                        ? param.getEstadoSincronizado()
                        : nd.getEstadoSincronizado());
        nd.setGeneradoSet(param.getGeneradoSet());
        nd.setIdEstado(param.getIdEstado() != null ? param.getIdEstado() : nd.getIdEstado());
        nd.setIdProcesamiento(param.getIdProcesamiento());
        
        edit(nd);
        
        return nd;
    }
    
    @Override
    public NotaDebito anular(NotaDebitoParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        Boolean anular = Boolean.FALSE;
        String codigoEstado = "CANCELADO";

        NotaDebito notaDebito = find(param.getId());

        if (notaDebito.getDigital().equals('N')) {

            anular = Boolean.TRUE;

        } else {

            String urlSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI_JSON");

            if (urlSiedi == null || urlSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance().descripcion("No existe la configuración HOST_SIEDI_JSON"));
                return null;
            }

            EventoService service = new EventoService(userInfo.getToken(), urlSiedi);

            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(notaDebito.getNroNotaDebito(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch (j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            BodyResponse<ResultadoOperacion> result = null;
            BodyResponse<List<ResultadoOperacionSiediApi>> result1 = null;

            String siediApi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API");     
            String userSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
            String passSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
            String siediApiHost  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");         
            if (notaDebito.getEstado().getCodigo().startsWith("APROBADO")) {
                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.cancelarDteSiediApi(notaDebito.getCdc(),
                           param.getMotivoAnulacion());
                   codigoEstado = "CANCELADO";
                } else {
                    result = service.cancelarDte(notaDebito.getCdc(),
                        param.getMotivoAnulacion());
                    codigoEstado = "CANCELADO";
                }

            } else {
                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.inutilizarSiediApi(notaDebito.getTalonario().getTimbrado(),
                        dEst, dPunt, nroDoc, nroDoc, 6, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                } else {
                    result = service.inutilizar(notaDebito.getTalonario().getTimbrado(),
                        dEst, dPunt, nroDoc, nroDoc, 6, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                }
            }
            if (result != null) {
                
                if (!result.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Rechazado")))) {

                        for (DetalleResultado dr : result1.getPayload().get(0).getResultado().getDetalles()) {
                           if ("4003".equals(dr.getCodigo())) {
                               anular = Boolean.TRUE;
                           } else {
                                param.addError(MensajePojo.createInstance()
                            .descripcion(dr.getMensaje()));
                           }     
                       } 
                    } else if (result.getPayload().getOperaciones() != null
                            && !result.getPayload().getOperaciones().isEmpty()
                            && result.getPayload().getOperaciones().get(0).getEstado().equalsIgnoreCase("Aprobado")) {
                        anular = Boolean.TRUE;
                    }
                }
            } else if (result1 != null){
                 if (!result1.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Aprobado"))
                            || (result1.getPayload().get(0).getResultado().getDetalles() != null
                            && result1.getPayload().get(0).getResultado().getDetalles().stream().filter(item -> item.getCodigo().equals("4003")).count() > 0))) {
                        anular = Boolean.TRUE;
                    } 
                }
            } else {
                param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
            }
        }

        if (anular) {

            EstadoPojo estado = facades.getEstadoFacade().find(null, codigoEstado, "NOTA_DEBITO");

            notaDebito.setAnulado('S');
            notaDebito.setIdEstado(estado.getId());
            notaDebito.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
            notaDebito.setObservacionAnulacion(param.getObservacionAnulacion());

            edit(notaDebito);

            NotaDebitoDetalleParam param1 = new NotaDebitoDetalleParam();
            param1.setIdNotaDebito(param.getId());
            param1.isValidToList();

            List<NotaDebitoDetalle> list = facades.getNotaDebitoDetalleFacade().find(param1);

            for (NotaDebitoDetalle item : list) {

                if(item.getIdFactura() != null) {
                    facades.getFacturaFacade().actualizarSaldoFactura(
                            item.getIdFactura(),
                            item.getMontoTotal());
                }
            }
            
            return notaDebito;
        } else {
            return null;
        }
    }
    
    @Override
    public NotaDebito create(NotaDebitoParam param, UserInfoImpl userInfo) {
        
        if(!isNull(userInfo) && isNull(param.getIdEncargado())) {
            param.setIdEncargado(userInfo.getId());
        }
        
        if(!validToCreate(param)) {
            return null;
        }
        
        NotaDebito nc = new NotaDebito();
        nc.setIdEmpresa(param.getIdEmpresa());
        nc.setIdUsuario(param.getIdUsuario());
        nc.setAnulado(param.getAnulado());
        nc.setCdc(param.getCdc());
        nc.setCodSeguridad(param.getCodSeguridad());
        nc.setDigital(param.getDigital());
        nc.setDireccion(param.getDireccion());
        nc.setEntregado(param.getEntregado());
        nc.setFecha(param.getFecha());
        nc.setFechaEntrega(param.getFechaEntrega());
        nc.setArchivoEdi(param.getArchivoEdi());
        nc.setArchivoSet(param.getArchivoSet());
        nc.setGeneradoEdi('N');
        nc.setGeneradoSet('N');
        nc.setEstadoSincronizado(param.getEstadoSincronizado());
        nc.setIdCiudad(param.getIdCiudad());
        nc.setIdCliente(param.getIdCliente());
        nc.setIdDepartamento(param.getIdDepartamento());
        nc.setIdDistrito(param.getIdDistrito());
        nc.setIdMoneda(param.getIdMoneda());
        nc.setIdEncargado(param.getIdEncargado());
        nc.setIdMotivoEmision(param.getIdMotivoEmision());
        nc.setIdMotivoEmisionInterno(param.getIdMotivoEmisionInterno());
        nc.setIdTalonario(param.getIdTalonario());
        nc.setImpreso(param.getImpreso());
        nc.setMontoImponible10(param.getMontoImponible10().stripTrailingZeros());
        nc.setMontoImponible5(param.getMontoImponible5().stripTrailingZeros());
        nc.setMontoImponibleTotal(param.getMontoImponibleTotal().stripTrailingZeros());
        nc.setMontoIva10(param.getMontoIva10().stripTrailingZeros());
        nc.setMontoIva5(param.getMontoIva5().stripTrailingZeros());
        nc.setMontoIvaTotal(param.getMontoIvaTotal().stripTrailingZeros());
        nc.setMontoTotal10(param.getMontoTotal10().stripTrailingZeros());
        nc.setMontoTotal5(param.getMontoTotal5().stripTrailingZeros());
        nc.setMontoTotalExento(param.getMontoTotalExento().stripTrailingZeros());
        nc.setMontoTotalNotaDebito(param.getMontoTotalNotaDebito().stripTrailingZeros());
        nc.setMontoTotalGuaranies(param.getMontoTotalGuaranies().stripTrailingZeros());
        nc.setMontoTotalDescuentoGlobal(param.getMontoTotalDescuentoGlobal());
        nc.setPorcentajeDescuentoGlobal(param.getPorcentajeDescuentoGlobal());
        nc.setNroCasa(param.getNroCasa());
        nc.setNroNotaDebito(param.getNroNotaDebito());
        nc.setObservacion(param.getObservacion());
        nc.setRazonSocial(param.getRazonSocial());
        nc.setRuc(param.getRuc());
        nc.setTelefono(param.getTelefono());
        nc.setEmail(param.getEmail());
        nc.setIdEstado(param.getIdEstado());
        nc.setFechaInsercion(Calendar.getInstance().getTime());
        nc.setIdNaturalezaCliente(param.getIdNaturalezaCliente());
        nc.setIdTipoCambio(param.getIdTipoCambio());
        
        create(nc);
        
        for (NotaDebitoDetalleParam detalle : param.getNotaDebitoDetalles()) {
            detalle.setIdNotaDebito(nc.getId());
            facades.getNotaDebitoDetalleFacade().create(detalle, userInfo);
        }
        
        if (!isNull(param.getNotaDebitoNotificaciones())) {
            for (NotaDebitoNotificacionParam detalle : param.getNotaDebitoNotificaciones()) {
                detalle.setIdNotaDebito(nc.getId());
                facades.getNotaDebitoNotificacionFacade().create(detalle, userInfo);
            }
        }
        
        return nc;
    }
    
    @Override
    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param) {
        
        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and nd.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format(" %s and nd.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and nd.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and nd.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select distinct nd.ruc, 110 as tipo_comprobante, nd.fecha, t.timbrado,\n"
                + "nd.nro_nota_debito, nd.monto_total_10, nd.monto_total_5,\n"
                + "nd.monto_total_exento, nd.monto_total_nota_debito,\n"
                + "case when m.codigo in ('PYG','Gs.') then 'N' else 'S' end,\n"
                + "f.nro_factura, tf.timbrado as timbrado_factura\n"
                + "from facturacion.nota_debito nd\n"
                + "left join facturacion.nota_debito_detalle ndd on ndd.id_nota_debito = nd.id\n"
                + "left join facturacion.factura f on f.id = ndd.id_factura\n"
                + "left join facturacion.talonario tf on tf.id = f.id_talonario\n"
                + "left join comercial.moneda m on m.id = f.id_moneda\n"
                + "left join facturacion.talonario t on t.id = f.id_talonario\n"
                + "where %s offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<RegistroComprobantePojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            RegistroComprobantePojo pojo = new RegistroComprobantePojo();
            pojo.setIdentificacionComprador((String) objects[i++]);
            pojo.setTipoComprobante((Integer) objects[i++]);
            pojo.setFechaEmisionComprobante((Date) objects[i++]);
            pojo.setTimbradoComprobanteAsociado(new Integer((String) objects[i++]));
            pojo.setNumeroComprobante((String) objects[i++]);
            pojo.setMontoIva10(new BigDecimal(objects[i++] + ""));
            pojo.setMontoIva5(new BigDecimal(objects[i++] + ""));
            pojo.setMontoExento(new BigDecimal(objects[i++] + ""));
            pojo.setMontoTotal(new BigDecimal(objects[i++] + ""));
            pojo.setMonedaExtrangera(((String)objects[i++]).charAt(0));
            pojo.setNumeroComprobanteAsociado((String) objects[i++]);
            pojo.setTimbradoComprobanteAsociado(new Integer((String) objects[i++]));
            result.add(pojo);
        }

        return result;
    }

    @Override
    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param) {
        
        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and nd.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format(" %s and nd.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and nd.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and nd.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select count(distinct(nd.id, f.id)) "
                + "from facturacion.nota_debito nd "
                + "left join facturacion.nota_debito_detalle ndd on ndd.id_nota_debito = nd.id "
                + "left join facturacion.factura f on f.id = ncd.id_factura "
                + "where %s ", where);
        
        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Integer result = object == null
                ? 0
                : ((Number)object).intValue();

        return result;
    }
    
    @Override
    public List<NotaDebitoPojo> findPojo(NotaDebitoParam param) {

        AbstractFind find = new AbstractFind(NotaDebitoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idCliente"),
                        getPath("root").get("nroNotaDebito"),
                        getPath("root").get("idTalonario"),
                        getPath("talonario").get("fechaVencimiento"),
                        getPath("talonario").get("fechaInicio"),
                        getPath("talonario").get("timbrado"),
                        getPath("talonario").get("serie"),
                        getPath("local").get("descripcion"),
                        getPath("root").get("fecha"),
                        getPath("root").get("razonSocial"),
                        getPath("root").get("direccion"),
                        getPath("root").get("nroCasa"),
                        getPath("root").get("idDepartamento"),
                        getPath("root").get("idDistrito"),
                        getPath("root").get("idCiudad"),
                        getPath("root").get("ruc"),
                        getPath("root").get("telefono"),
                        getPath("root").get("email"),
                        getPath("root").get("observacion"),
                        getPath("root").get("anulado"),
                        getPath("root").get("digital"),
                        getPath("root").get("archivoSet"),
                        getPath("root").get("archivoEdi"),
                        getPath("root").get("generadoSet"),
                        getPath("root").get("generadoEdi"),
                        getPath("root").get("estadoSincronizado"),
                        getPath("root").get("entregado"),
                        getPath("root").get("impreso"),
                        getPath("estado").get("id"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("idProcesamiento"),
                        getPath("root").get("cdc"),
                        getPath("root").get("codSeguridad"),
                        getPath("root").get("idNaturalezaCliente"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("descripcion"),
                        getPath("moneda").get("codigo"),
                        getPath("root").get("idMotivoEmision"),
                        getPath("motivoEmision").get("descripcion"),
                        getPath("motivoEmision").get("codigo"),
                        getPath("root").get("idMotivoEmisionInterno"),
                        getPath("motivoEmisionInterno").get("descripcion"),
                        getPath("motivoEmisionInterno").get("codigo"),
                        getPath("root").get("montoIva5"),
                        getPath("root").get("montoImponible5"),
                        getPath("root").get("montoTotal5"),
                        getPath("root").get("montoIva10"),
                        getPath("root").get("montoImponible10"),
                        getPath("root").get("montoTotal10"),
                        getPath("root").get("montoTotalExento"),
                        getPath("root").get("montoIvaTotal"),
                        getPath("root").get("montoImponibleTotal"),
                        getPath("root").get("montoTotalNotaDebito"),
                        qb.function("string_agg", String.class, getPath("factura").get("nroFactura"), qb.literal(", ")),
                        getPath("root").get("montoTotalDescuentoGlobal"),
                        getPath("root").get("porcentajeDescuentoGlobal"),
                        getPath("root").get("idTipoCambio"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<NotaDebito> find(NotaDebitoParam param) {

        AbstractFind find = new AbstractFind(NotaDebito.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, NotaDebitoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<NotaDebito> root = cq.from(NotaDebito.class);
        Join<NotaDebito, Empresa> empresa = root.join("empresa");
        Join<NotaDebito, Estado> estado = root.join("estado", JoinType.LEFT);
        Join<NotaDebito, MotivoEmision> motivoEmision = root.join("motivoEmision", JoinType.LEFT);
        Join<NotaDebito, MotivoEmisionInterno> motivoEmisionInterno = root.join("motivoEmisionInterno", JoinType.LEFT);
        Join<NotaDebito, Moneda> moneda = root.join("moneda", JoinType.LEFT);
        Join<NotaDebito, NotaDebitoDetalle> notaDebitoDetalles = root.join("notaDebitoDetalles", JoinType.LEFT);
        Join<NotaDebitoDetalle, Factura> factura = notaDebitoDetalles.join("factura", JoinType.LEFT);
        Join<NotaDebito, Talonario> talonario = root.join("talonario", JoinType.LEFT);
        Join<Talonario, Local> local = talonario.join("local", JoinType.LEFT);

        find.addPath("root", root);
        find.addPath("empresa", empresa);
        find.addPath("talonario", talonario);
        find.addPath("factura", factura);
        find.addPath("estado", estado);
        find.addPath("local", local);
        find.addPath("motivoEmision", motivoEmision);
        find.addPath("motivoEmisionInterno", motivoEmisionInterno);
        find.addPath("moneda", moneda);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(factura.get("id"), param.getIdFactura()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdLocalTalonario() != null) {
            predList.add(qb.equal(talonario.get("idLocal"), param.getIdLocalTalonario()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getEntregado() != null) {
            predList.add(qb.equal(root.get("entregado"), param.getEntregado()));
        }
        
        if (param.getImpreso() != null) {
            predList.add(qb.equal(root.get("impreso"), param.getImpreso()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }

        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }

        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }

        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }

        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }
        
        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }
        
        if (param.getIdMotivoEmision() != null) {
            predList.add(qb.equal(root.get("idMotivoEmision"), param.getIdMotivoEmision()));
        }
        
        if (param.getIdMotivoEmisionInterno() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionInterno"), param.getIdMotivoEmisionInterno()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroNotaDebito() != null && !param.getNroNotaDebito().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroNotaDebito")),
                    String.format("%%%s%%", param.getNroNotaDebito().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(param.getNroNotaDebitoDescendente() ? qb.desc(root.get("fecha")) : qb.asc(root.get("fecha")),
                param.getNroNotaDebitoDescendente() ? qb.desc(root.get("nroNotaDebito")) : qb.asc(root.get("nroNotaDebito")),
                param.getIdDescendente() ? qb.desc(root.get("id")) : qb.asc(root.get("id")));
        
        cq.where(pred);

        cq.groupBy(root.get("id"),
                estado.get("id"),
                talonario.get("id"),
                motivoEmision.get("id"),
                motivoEmisionInterno.get("id"),
                moneda.get("id"),
                local.get("id"));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaDebitoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaDebito> root = cq.from(NotaDebito.class);
        Join<NotaDebito, Empresa> empresa = root.join("empresa");
        Join<NotaDebito, Estado> estado = root.join("estado", JoinType.LEFT);
        Join<NotaDebito, NotaDebitoDetalle> notaDebitoDetalles = root.join("notaDebitoDetalles", JoinType.LEFT);
        Join<NotaDebitoDetalle, Factura> factura = notaDebitoDetalles.join("factura", JoinType.LEFT);
        Join<NotaDebito, Talonario> talonario = root.join("talonario", JoinType.LEFT);
        
        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(factura.get("id"), param.getIdFactura()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdLocalTalonario() != null) {
            predList.add(qb.equal(talonario.get("idLocal"), param.getIdLocalTalonario()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getEntregado() != null) {
            predList.add(qb.equal(root.get("entregado"), param.getEntregado()));
        }
        
        if (param.getImpreso() != null) {
            predList.add(qb.equal(root.get("impreso"), param.getImpreso()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }

        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }

        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }

        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }
        
        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }
        
        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getIdMotivoEmision() != null) {
            predList.add(qb.equal(root.get("idMotivoEmision"), param.getIdMotivoEmision()));
        }
        
        if (param.getIdMotivoEmisionInterno() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionInterno"), param.getIdMotivoEmisionInterno()));
        }
        
        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroNotaDebito() != null && !param.getNroNotaDebito().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroNotaDebito")),
                    String.format("%%%s%%", param.getNroNotaDebito().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public byte[] getXmlNotaDebito(NotaDebitoParam param) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        NotaDebitoPojo nc = findFirstPojo(param);
        
        Closer closer = Closer.instance();

        try {

            String hostSiedi = null;
            Boolean siediApi = param.getSiediApi();
            if(siediApi){
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");
            } else {
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI");
            }
            
            if (hostSiedi == null || hostSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra valor para el parámetro HOST_SIEDI/SIEDI_API_HOST"));
                return null;
            }
        
            Boolean ticket = param.getTicket();               
            String cdc = nc.getCdc();
            
            String nombreArchivo = String.format("ND_%s.txt", cdc);
            JsonObject object = null;
            if (siediApi){

                ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                String userSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                String passSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);

              if (tokenSiedi == null){

                    param.addError(MensajePojo.createInstance()
                            .descripcion("Credenciales de siediApi incorrectas."));
                    return null;
                }
                
                return XmlDteAdapter.obtener(tokenSiedi,hostSiedi, cdc, nombreArchivo, object,ticket, true);
            } else {
                return XmlDteAdapter.obtener(null,hostSiedi, cdc, nombreArchivo, object,ticket,false);
            }
   
        } finally {
            closer.close();
        }
    }


    @Override
    public byte[] getPdfNotaDebito(NotaDebitoParam param) throws Exception {
        
        if (!validToGetPdf(param)) {
            return null;
        }
        
        NotaDebito notaDebito = find(param.getId());

        byte[] result = null;
        
        Closer closer = Closer.instance();
        
        try {
            if(notaDebito.getDigital().equals('N')) {

                String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_NOTA_DEBITO_AUTOIMPRESOR");
                InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
                
                if (isNull(is)) {
                    param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para nota de débito autoimpresor"));
                } else {
                    
                    byte[] template = IOUtils.toByteArray(is);
                    
                    List<byte[]> bytesList = Lists.empty();
                    
                    if(param.getImprimirOriginal()) {
                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
                            NotaDebitoAutoimpresorParam params = facades.getReportUtils().getParam(closer, notaDebito);
                            params.setTipoNotaDebito(NotaDebitoAutoimpresorParam.TipoNotaDebito.ORIGINAL);
                            byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
                            if(bytes != null && bytes.length > 0) {
                                bytesList.add(bytes);
                            }
                        }
                    }
                    
                    if(param.getImprimirDuplicado()) {
                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
                            NotaDebitoAutoimpresorParam params = facades.getReportUtils().getParam(closer, notaDebito);
                            params.setTipoNotaDebito(NotaDebitoAutoimpresorParam.TipoNotaDebito.DUPLICADO);
                            byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
                            if(bytes != null && bytes.length > 0) {
                                bytesList.add(bytes);
                            }
                        }
                    }
                    
                    if(param.getImprimirTriplicado()) {
                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
                            NotaDebitoAutoimpresorParam params = facades.getReportUtils().getParam(closer, notaDebito);
                            params.setTipoNotaDebito(NotaDebitoAutoimpresorParam.TipoNotaDebito.TRIPLICADO);
                            byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
                            if(bytes != null && bytes.length > 0) {
                                bytesList.add(bytes);
                            }
                        }
                    }
                    
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    facades.getReportUtils().merge(bytesList, baos, PageSize.LEGAL);
                    result = baos.toByteArray();
                }

            } else {
                String hostSiedi;
                boolean siediApi = Boolean.TRUE.equals(param.getSiediApi())
                        || "S".equalsIgnoreCase(facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API"));

                String claveHost = siediApi ? "SIEDI_API_HOST" : "HOST_SIEDI";
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), claveHost);
                  
                if (hostSiedi == null || hostSiedi.trim().isEmpty()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se encuentra valor para el parámetro HOST_SIEDI/SIEDI_API_HOST"));
                    return null;
                }
                                   
                String cdc = notaDebito.getCdc();
                Boolean ticket = param.getTicket();
                String nombreArchivo = String.format("ND_%s.txt", cdc);
                JsonObject object = null;
                if (siediApi){
                    
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                    String userSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                    String passSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);

                    if (tokenSiedi == null){
                        param.addError(MensajePojo.createInstance()
                              .descripcion("Credenciales de siediApi ncorrectas."));
                        return null;
                    }
                    result = KudeDteAdapter.obtener(tokenSiedi,hostSiedi, cdc, nombreArchivo, object,ticket, true);
                } else {
                    result = KudeDteAdapter.obtener(null,hostSiedi, cdc, nombreArchivo, object,ticket,false);
                }
            }
        } finally {
            closer.close();
        }

        // agrega una marca de agua si la notaDebito esta anulada
        if(notaDebito.getAnulado() != null && notaDebito.getAnulado().equals('S')){
            try (ByteArrayOutputStream baos = WatermarkUtils.addWatermarkToExistingPdf(result, "ANULADO", null)) {
                result = baos.toByteArray();
            }
        }
           
        
        return result;
    }
    
    @Override
    public byte[] getXlsReporteVenta(NotaDebitoParam param, UserInfoImpl userInfo) throws Exception {

        if (!param.isValidToGetReporteVenta()) {
            return null;
        }

        byte[] result = null;

        Closer closer = Closer.instance();

        try {

            param.setFirstResult(0);
            param.setPageSize(100000);

            List<NotaDebitoPojo> notaDebitos = facades.getNotaDebitoFacade().findPojo(param);

            ReporteVentaNotaDebitoParam params = facades.getReportUtils().getReporteVentaNotaDebitoParam(closer, param.getIdEmpresa(), param.getFechaDesde(), param.getFechaHasta(), notaDebitos);

            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_REPORTE_VENTA_ND");
            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);

            if (isNull(is)) {
                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el reporte de venta"));
            } else {
                
                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
                configuration.setOnePagePerSheet(false);
                configuration.setDetectCellType(true);
                //borrar espacio entre filas
                configuration.setRemoveEmptySpaceBetweenRows(true);
                //borrar espacio entre columnas
                configuration.setRemoveEmptySpaceBetweenColumns(true);
                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
                configuration.setIgnoreCellBorder(false);
                configuration.setWhitePageBackground(false);

                //Obtenemos una conexion del DataSource
                Context ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                try (Connection connection = ds.getConnection()) {
                    result = JasperReporteGenerator.exportReportToXlsxBytes(params, connection, is, configuration);
                }
            }

        } finally {
            closer.close();
        }

        return result;
    }
}
