/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.Date;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.PeriodoLiquidacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface PeriodoLiquidacionFacade extends Facade<PeriodoLiquidacion, PeriodoLiquidacionParam, UserInfoImpl> {

    public PeriodoLiquidacion find(Date date);
    
}
