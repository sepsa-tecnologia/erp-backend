/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.utils.NotaDebitoUtils;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
@Log4j2
public class NotaDebitoMasivaGamblingTask extends AbstracProcesamientoLote {

    @Resource
    private UserTransaction userTransaction;

    @Override
    public void procesar(Lote lote) {

        LoteArchivo archivoFacturas = null;
        LoteArchivo archivoClientes = null;
        LoteArchivo archivoNotaDebito = null;
        LoteArchivo archivoFacturasExternas = null;

        LoteArchivoParam laparam = new LoteArchivoParam();
        laparam.setIdLote(lote.getId());
        laparam.isValidToList();
        List<LoteArchivo> archivos = facades.getLoteArchivoFacade().find(laparam);
        for (LoteArchivo archivo : archivos) {
            switch (archivo.getCodigo()) {
                case "FACTURAS":
                    archivoFacturas = archivo;
                    break;
                case "CLIENTES":
                    archivoClientes = archivo;
                    break;
                case "NOTA_DEBITO":
                    archivoNotaDebito = archivo;
                    break;
                case "FACTURAS_EXTERNAS":
                    archivoFacturasExternas = archivo;
                    break;
            }
        }

        Result result = convert(lote, archivoNotaDebito, archivoClientes, archivoFacturas, archivoFacturasExternas);
        if (!result.isSuccess()) {
            String resultadoBase64 = Base64.getEncoder().encodeToString(result.getResult().getBytes(StandardCharsets.UTF_8));

            LoteParam loteparam = new LoteParam();
            loteparam.setId(lote.getId());
            loteparam.setIdEmpresa(lote.getIdEmpresa());
            loteparam.setCodigoEstado("PROCESADO");
            loteparam.setNotificado(lote.getNotificado());
            loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
            loteparam.setResultado(resultadoBase64);

            facades.getLoteFacade().edit(loteparam, null);
            return;
        }

        NotaDebitoParam param = new NotaDebitoParam();
        param.setIdEmpresa(lote.getIdEmpresa());
        param.setIdUsuario(lote.getIdUsuario());
        param.setUploadedFileBytes(result.getResult().getBytes(StandardCharsets.UTF_8));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<NotaDebitoParam> notaDebitoList = NotaDebitoUtils.convertToList(facades, param);
        String inicio = String.format("TIMBRADO;NRO_NOTA_DEBITO;FECHA;ID_NOTA_DEBITO;ESTADO_RESULTADO");
        StringBuilder buffer = new StringBuilder();
        buffer.append(inicio).append("\r\n");
        for (NotaDebitoParam nc : notaDebitoList) {
            if (nc.tieneErrores()) {
                String error = String.format("%s;%s;-;-;ERROR;", nc.getTimbrado(), nc.getNroNotaDebito());
                error = error.concat("%s| ");
                for (MensajePojo mp : nc.getErrores()) {
                    error = String.format(error, mp.getDescripcion());
                    error = error.concat("%s| ");
                }
                error = error.substring(0, error.length() - 5);
                error = error.concat(";;");
                buffer.append(error).append("\r\n");
            } else {

                try {
                    userTransaction.begin();

                    NotaDebito notaDebito = facades
                            .getNotaDebitoFacade()
                            .create(nc, null);

                    if (notaDebito == null) {
                        String error = String.format("%s;%s;-;-;ERROR;", nc.getTimbrado(), nc.getNroNotaDebito());
                        error = error.concat("| ");
                        for (MensajePojo mp : nc.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {

                        String ok = String.format("%s;%s;%s;%s;OK", nc.getTimbrado(), nc.getNroNotaDebito(), sdf.format(nc.getFecha()), notaDebito.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }

                    userTransaction.commit();
                } catch (Exception exception) {
                    try {
                        userTransaction.rollback();
                    } catch (Exception e) {
                        log.fatal("Error", e);
                    }
                    String error = String.format("%s;%s;-;-;ERROR;", nc.getTimbrado(), nc.getNroNotaDebito());
                    error = error.concat("| ");
                    error = error.concat(exception.getMessage());
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                }
            }
        }

        String resultadoBase64 = Base64.getEncoder().encodeToString(buffer.toString().getBytes(StandardCharsets.UTF_8));

        LoteParam loteparam = new LoteParam();
        loteparam.setId(lote.getId());
        loteparam.setIdEmpresa(lote.getIdEmpresa());
        loteparam.setCodigoEstado("PROCESADO");
        loteparam.setNotificado(lote.getNotificado());
        loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
        loteparam.setResultado(resultadoBase64);

        facades.getLoteFacade().edit(loteparam, null);
    }

    public Result convert(Lote lote, LoteArchivo file, LoteArchivo fileClient, LoteArchivo fileFacturas, LoteArchivo fileFacturasExternas) {
        String timbrado = facades.getConfiguracionValorFacade().getCVValor(lote.getIdEmpresa(), "TIMBRADO_NOTA_DEBITO_MASIVA");
        if (timbrado == null || timbrado.trim().isEmpty()) {
            return Result.builder()
                    .success(false)
                    .result("La configuración para el timbrado de carga de nota de débito masiva es inexistente")
                    .build();
        }
        List<Cliente> clientes = getListClientes(fileClient);
        Map<String, String> facturas = getListFacturas(fileFacturas);
        if (clientes == null || clientes.isEmpty()) {
            return Result.builder()
                    .success(false)
                    .result("No se encuentra ningún cliente valido en el archivo indicado")
                    .build();
        }
        if (facturas == null || facturas.isEmpty()) {
            return Result.builder()
                    .success(false)
                    .result("No se encuentra ninguna factura valida en el archivo indicado")
                    .build();
        }
        List<FacturaExterna> facturasExternas = null;
        if (fileFacturasExternas != null) {
            facturasExternas = getListFacturasExternas(fileFacturasExternas);
        }
         
        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(file.getContenido())));  StringWriter writer = new StringWriter()) {
            int numSheets = workbook.getNumberOfSheets();
            for (int sheetNumber = 0; sheetNumber < numSheets; sheetNumber++) {

                Sheet sheet = workbook.getSheetAt(sheetNumber);

                int rowIndex = 0;
                int linea = 1; // Contador de línea
                for (Row row : sheet) {
                    if (rowIndex < 1) { // Omitir la primera fila
                        rowIndex++;
                        continue;
                    }

                    String nro = getCellValueAsString(row.getCell(1)).trim();
                    String nroNCHolder = nro;
                    while (nro.length() < 7) {
                        nro = "0" + nro;
                    }
                    if (getCellValueAsString(row.getCell(29)) != null && !getCellValueAsString(row.getCell(29)).trim().isEmpty()) {
                        String sucursalCode = getSucursales(lote.getIdEmpresa(), Integer.parseInt(getCellValueAsString(row.getCell(29))), timbrado);
                        if (sucursalCode != null && !sucursalCode.trim().isEmpty()) {
                            String nroNotaDebito = sucursalCode + nro;
                            String fecha = formatearFecha(getCellValueAsString(row.getCell(3)));
                            String codigoIsoMoneda = "PYG";
                            int idcli = Integer.parseInt(getCellValueAsString(row.getCell(12)));
                            String rucCi = "";
                            String dirCli = null;
                            String emailCli = "";
                            for (Cliente cli : clientes) {
                                if (idcli == cli.getIdCliente()) {
                                    rucCi = cli.getNroDocumento();
                                    if (cli.getDireccion() != null) {
                                        dirCli = cli.getDireccion();
                                    }
                                    if (cli.getEmail() != null) {
                                        emailCli = cli.getEmail().replace(",", "/").replace(";", "/");
                                    }
                                    break;
                                }
                            }
                            String razonSocial = Normalizer.normalize(getCellValueAsString(row.getCell(31)).trim().replace(",", "").replace(";", ""), Normalizer.Form.NFD)
                                    .replaceAll("[^\\p{ASCII}]", ""); //Quitamos acentos
                            String codSeguridad = generarNumeroRandom();

                            String codigoMotivoEmision = Normalizer.normalize(getCellValueAsString(row.getCell(32)).trim().replace(" ", "_").replace(",", ""), Normalizer.Form.NFD)
                                    .replaceAll("[^\\p{ASCII}]", "")
                                    .toUpperCase(); //Normalizamos a mayusculas y quitamos acentos
                            if (!codigoMotivoEmision.equalsIgnoreCase("DEVOLUCION") || !codigoMotivoEmision.equalsIgnoreCase("BONIFICACION")) {
                                codigoMotivoEmision = "DEVOLUCION";
                            }
                            String tipoCambio = "";

                            String nroFactura = facturas.getOrDefault(nroNCHolder, "").trim().replace(",", "").replace(";", "");
                            if(nroFactura.contains("/")){
                                String[] nFPrimero = nroFactura.split("/");
                                nroFactura = nFPrimero[0];
                            }
                            String timbradoFactura = "";
                            String fechaFactura = "";
                            String cdcFactura = "";
                            if (nroFactura != null && !nroFactura.trim().isEmpty()) {
                                while (nroFactura.length() < 7) {
                                    nroFactura = "0" + nroFactura;
                                }
                                nroFactura = sucursalCode + nroFactura;
                                Factura factura = getFactura(lote.getIdEmpresa(), Integer.parseInt(getCellValueAsString(row.getCell(29))), nroFactura);
                                if (factura != null) {
                                    timbradoFactura = factura.getTalonario().getTimbrado();
                                    nroFactura = factura.getNroFactura();
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                    fechaFactura = formatter.format(factura.getFecha());
                                    cdcFactura = factura.getCdc();
                                } else {
                                    if (facturasExternas != null && !facturasExternas.isEmpty()) {
                                        for (FacturaExterna item : facturasExternas) {
                                            if (item.nroNotaDebito.equalsIgnoreCase(nroNCHolder)) {
                                                timbradoFactura = item.getTimbrado();
                                                nroFactura = item.getNroFactura();
                                                fechaFactura = item.getFecha();
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (facturasExternas != null && !facturasExternas.isEmpty()) {
                                    for (FacturaExterna item : facturasExternas) {
                                        if (item.nroNotaDebito.equalsIgnoreCase(nroNCHolder)) {
                                            timbradoFactura = item.getTimbrado();
                                            nroFactura = item.getNroFactura();
                                            fechaFactura = item.getFecha();
                                        }
                                    }
                                }
                            }
                            String codigoProducto = "001";
                            String porcentajeImpuesto = "0";
                            String porcentajeGravada = "100";
                            
                            String descuentoUnitario = "";
                            String cantidad = "";
                            String precioUnitarioConIva = "";
                            try{
                                cantidad = getCellValueAsString(row.getCell(11));
                            } catch(Exception e){
                                log.fatal(e);
                            }
                            if (cantidad.isEmpty() || Integer.parseInt(cantidad) == 0){
                                cantidad = "1";
                                descuentoUnitario = "0";
                                precioUnitarioConIva = getCellValueAsString(row.getCell(23));
                            } else {
                                precioUnitarioConIva = getCellValueAsString(row.getCell(7));
                                descuentoUnitario = calculoDescuento(row.getCell(7), row.getCell(8));
                            }
                            
                            try{
                                porcentajeImpuesto = getCellValueAsString(row.getCell(5));
                            } catch(Exception e){
                                log.fatal(e);
                            }
                            
                            String cartonDesde = getCellValueAsString(row.getCell(9));
                            String cartonHasta = getCellValueAsString(row.getCell(10));
                            String fechaSorteo = getCellValueAsString(row.getCell(34));
                            String descripcion = "Cartones de Seneté";
                            try {
                                int tipoMovimiento = Integer.parseInt(getCellValueAsString(row.getCell(2)));
                                if (tipoMovimiento == 7 || tipoMovimiento == 8) {
                                    String bitacora = getCellValueAsString(row.getCell(20));
                                    if (bitacora != null && !bitacora.trim().isEmpty()) {
                                        descripcion = bitacora.replace(",","");
                                    }
                                }
                            } catch (Exception e) {
                                log.fatal(e);
                            }
                            JsonObject object = new JsonObject();
                            object.addProperty("CARTON_DESDE", cartonDesde);
                            object.addProperty("CARTON_HASTA", cartonHasta);
                            object.addProperty("FECHA_SORTEO", fechaSorteo);
                            if (dirCli != null) {
                                object.addProperty("DIREC_CLIENTE", dirCli);
                            }
                            if (emailCli != null) {
                                object.addProperty("CORREO_CLIENTE", emailCli);
                            }
                            String datoAdicional = Base64.getEncoder().encodeToString(object.toString().getBytes());

                            // Primera línea
                            String linea1 = 1 + "," + timbrado + "," + nroNotaDebito + "," + fecha + "," + codigoIsoMoneda + "," + rucCi + "," + razonSocial + "," + codSeguridad + "," + codigoMotivoEmision + "," + tipoCambio;
                            writer.write(linea1 + "\n");

                            // Segunda línea
                            String linea2 = 2 + "," + timbrado + "," + nroNotaDebito + "," + timbradoFactura + "," + nroFactura + "," + fechaFactura + "," + cdcFactura + "," + codigoProducto + "," + porcentajeImpuesto + "," + porcentajeGravada + "," + precioUnitarioConIva + "," + descuentoUnitario + "," + cantidad + "," + descripcion + "," + datoAdicional;
                            writer.write(linea2 + "\n");
                        } else {
                            return Result.builder()
                                    .success(false)
                                    .result("No se encuentra ningún talonario registrado para la sucursal (su_id) = " + getCellValueAsString(row.getCell(29)))
                                    .build();
                        }
                    } else {
                        return Result.builder()
                                .success(false)
                                .result("No todos los registros cuentan con el número de sucursal (su_id)")
                                .build();
                    }
                    linea++;
                    rowIndex++;

                }
            }
            return Result.builder()
                    .success(true)
                    .result(writer.toString())
                    .build();
        } catch (IOException e) {
            return Result.builder()
                    .success(true)
                    .result(e.getMessage())
                    .build();
        }
    }

    public List<Cliente> getListClientes(LoteArchivo fileClient) {
        List<Cliente> clientes = new ArrayList<>();
        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(fileClient.getContenido())))) {
            Sheet sheet = workbook.getSheetAt(0);
            int rowIndex = 0;
            for (Row row : sheet) {
                if (rowIndex < 1) {
                    rowIndex++;
                    continue;
                }
                try {
                    Cliente c = new Cliente();
                    if (getCellValueAsString(row.getCell(0)) != null && !getCellValueAsString(row.getCell(0)).trim().isEmpty() && getCellValueAsString(row.getCell(2)) != null && !getCellValueAsString(row.getCell(2)).trim().isEmpty()) {
                        c.setIdCliente(Integer.parseInt(getCellValueAsString(row.getCell(0))));
                        c.setNroDocumento(getCellValueAsString(row.getCell(2)).replace(",", "").replace(".", "").trim());
                        if (getCellValueAsString(row.getCell(12)) != null && !getCellValueAsString(row.getCell(12)).trim().isEmpty()){
                            c.setDireccion(getCellValueAsString(row.getCell(12)).replace(",", "").replace(".", "").trim());
                        }
                        if (getCellValueAsString(row.getCell(13)) != null && !getCellValueAsString(row.getCell(13)).trim().isEmpty()){
                            c.setEmail(getCellValueAsString(row.getCell(13)).trim().replace(",", "/").replace(" ", "/"));
                        }
                        if (!clientes.contains(c)) {
                            clientes.add(c);
                        }
                    }
                } catch (Exception e) {
                }

                rowIndex++;
            }
            return clientes;
        } catch (Exception e) {
            log.fatal("Error", e);
            return null;
        }
    }

    public List<FacturaExterna> getListFacturasExternas(LoteArchivo fileFacturas) {
        List<FacturaExterna> facturas = new ArrayList<>();
        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(fileFacturas.getContenido())))) {
            Sheet sheet = workbook.getSheetAt(0);
            int rowIndex = 0;
            for (Row row : sheet) {
                if (rowIndex < 1) {
                    rowIndex++;
                    continue;
                }
                try {
                    FacturaExterna f = new FacturaExterna();
                    if (getCellValueAsString(row.getCell(1)) != null && !getCellValueAsString(row.getCell(1)).trim().isEmpty()
                            && getCellValueAsString(row.getCell(2)) != null && !getCellValueAsString(row.getCell(2)).trim().isEmpty()
                            && getCellValueAsString(row.getCell(3)) != null && !getCellValueAsString(row.getCell(3)).trim().isEmpty()) {
                        f.setNroNotaDebito(getCellValueAsString(row.getCell(1)));
                        f.setTimbrado(getCellValueAsString(row.getCell(2)).replace(",", "").replace(".", "").trim());
                        f.setNroFactura(getCellValueAsString(row.getCell(3)).replace(",", "").replace(".", "").trim());
                        f.setFecha(formatearFecha(getCellValueAsString(row.getCell(4)).replace(",", "").replace(".", "").trim()));
                        if (getCellValueAsString(row.getCell(5)) != null && !getCellValueAsString(row.getCell(5)).trim().isEmpty()) {
                            f.setCdc(getCellValueAsString(row.getCell(5)).replace(",", "").replace(".", "").trim());
                        }
                        if (!facturas.contains(f)) {
                            facturas.add(f);
                        }
                    }
                } catch (Exception e) {
                }

                rowIndex++;
            }
            return facturas;
        } catch (Exception e) {
            log.fatal("Error", e);
            return null;
        }
    }
    
    public Map<String, String> getListFacturas(LoteArchivo fileFacturas) {
        Map<String, String> facturas = new HashMap<>();
        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(fileFacturas.getContenido())))) {
            Sheet sheet = workbook.getSheetAt(0);
            int rowIndex = 0;
            for (Row row : sheet) {
                if (rowIndex < 1) {
                    rowIndex++;
                    continue;
                }
                if (getCellValueAsString(row.getCell(12)) != null && !getCellValueAsString(row.getCell(12)).trim().isEmpty()) {
                    if (getCellValueAsString(row.getCell(12)).equalsIgnoreCase("Devolución") || getCellValueAsString(row.getCell(12)).equalsIgnoreCase("Nota de Dèbito") || getCellValueAsString(row.getCell(12)).equalsIgnoreCase("Nota de Debito")) {
                        facturas.put(getCellValueAsString(row.getCell(13)).trim(), getCellValueAsString(row.getCell(14)).trim());
                    }
                }
                rowIndex++;
            }
            return facturas;
        } catch (Exception e) {
            log.fatal("Error", e);
            return null;
        }
    }

    private static String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    return sdf.format(cell.getDateCellValue());
                } else {
                    return String.valueOf((long) cell.getNumericCellValue());
                }
            default:
                return "";
        }
    }

    private static String calculoDescuento(Cell precioCell, Cell descuentoPorcentajeCell) {
        if (precioCell == null || descuentoPorcentajeCell == null) {
            return "0";
        }
        double price = precioCell.getNumericCellValue();
        double descuentoPorcentaje = descuentoPorcentajeCell.getNumericCellValue();
        return String.valueOf((long) (price * descuentoPorcentaje / 100));
    }

    private String getSucursales(Integer idEmpresa, int idSuc, String timbrado) {
        String sucursales = "";

        TalonarioParam t = new TalonarioParam();
        t.setIdEmpresa(idEmpresa);
        t.setActivo('S');
        t.setTimbrado(timbrado);
        t.setIdTipoDocumento(6);
        t.setCodigoInterno(idSuc);
        TalonarioPojo talonarioPojo = facades.getTalonarioFacade().findFirstPojo(t);
        if (talonarioPojo != null) {
            sucursales = talonarioPojo.getNroSucursal() + "-" + talonarioPojo.getNroPuntoVenta() + "-";
        }

        return sucursales;
    }

    public Factura getFactura(Integer idEmpresa, int idSuc, String nroFactura) {
        String timbrado = facades.getConfiguracionValorFacade().getCVValor(idEmpresa, "TIMBRADO_FACTURA_MASIVA");
        FacturaParam f = new FacturaParam();
        f.setNroFactura(nroFactura);
        f.setIdEmpresa(idEmpresa);
        TalonarioParam t = new TalonarioParam();
        t.setIdEmpresa(f.getIdEmpresa());
        t.setActivo('S');
        t.setTimbrado(timbrado);
        String[] nros = nroFactura.split("-");
        t.setNroSucursal(nros[0]);
        t.setNroPuntoVenta(nros[1]);
        t.setIdTipoDocumento(1);
        t.setCodigoInterno(idSuc);
        TalonarioPojo talonario = facades.getTalonarioFacade().findFirstPojo(t);
        if (talonario != null) {
            f.setIdTalonario(talonario.getId());
            Factura factura = facades.getFacturaFacade().findFirst(f);
            return factura;
        } else {
            return null;
        }
    }

    private static String generarNumeroRandom() {
        Random random = new Random();
        return String.format("%09d", random.nextInt(1000000000));
    }
    
    public String formatearFecha(String fecha) {
        try {
            int dias = Integer.parseInt(fecha);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate fechaInicial = LocalDate.of(1899, 12, 30);
            LocalDate fechaFinal = fechaInicial.plusDays(dias);
            return fechaFinal.format(formatter);
        } catch (NumberFormatException e) {
            return fecha;
        }
    }

    @Data
    @Builder
    private static class Result {

        private boolean success;
        private String result;
    }

    @Data
    private class Cliente {

        private Integer idCliente;
        private String nroDocumento;
        private String direccion;
        private String email;
    }
    
     @Data
    private class FacturaExterna {
        
        private String nroNotaDebito;
        private String timbrado;
        private String nroFactura;
        private String fecha;
        private String cdc;
    }
}
