/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface OperacionInventarioFacade extends Facade<OperacionInventario, OperacionInventarioParam, UserInfoImpl> {

    public OperacionInventario changeState(OperacionInventarioParam param, UserInfoImpl userInfo) throws Exception;

    public OperacionInventario create(UserInfoImpl userInfo, String codigoTipoOperacion, String codigoMotivo, String codigoEstado, Integer idOrdenCompra, Integer idEmpresa);

}
