/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros del reporte de proveedores operativos por comprador
 * @author Jonathan D. Bernal Fernández
 */
public class ProvOpPorCompParam extends CommonParam {
    
    public ProvOpPorCompParam() {
    }

    public ProvOpPorCompParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Ano
     */
    @QueryParam("ano")
    private String ano;
    
    /**
     * Mes
     */
    @QueryParam("mes")
    private String mes;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de comprador
     */
    @QueryParam("idComprador")
    private Integer idComprador;
    
    /**
     * Identificador de grupo de comprador
     */
    @QueryParam("idGrupoComprador")
    private Integer idGrupoComprador;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    /**
     * Verifica si el objeto es válido para listar la lista de liquidaciones por periodo
     * @return Bandera
     */
    public Boolean isValidToListPeriodo() {
        
        limpiarErrores();
        
        if(isNull(mes)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el mes"));
        }
        
        if(isNull(ano)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el año"));
        }
        
        if(isNull(idComprador) && isNull(idGrupoComprador)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe de insicar el identificador de comprador y/o identificador de grupo del comprador"));
        }
        
        super.isValidToList();
        
        return !tieneErrores();
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public Integer getIdGrupoComprador() {
        return idGrupoComprador;
    }

    public void setIdGrupoComprador(Integer idGrupoComprador) {
        this.idGrupoComprador = idGrupoComprador;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToCreate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
