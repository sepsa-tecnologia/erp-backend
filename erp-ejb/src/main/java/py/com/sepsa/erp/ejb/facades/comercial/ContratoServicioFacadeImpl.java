/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicio;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicioPK;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicioTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.MontoMinimo;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioTarifaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "ContratoServicioFacade", mappedName = "ContratoServicioFacade")
@Local(ContratoServicioFacade.class)
public class ContratoServicioFacadeImpl extends FacadeImpl<ContratoServicio, ContratoServicioParam> implements ContratoServicioFacade {

    public ContratoServicioFacadeImpl() {
        super(ContratoServicio.class);
    }

    @Override
    public Map<String, String> getMapConditions(ContratoServicio item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.

        map.put("id_contrato", item.getContratoServicioPK().getIdContrato() + "");
        map.put("id_servicio", item.getContratoServicioPK().getIdServicio() + "");
        map.put("id_monto_minimo", item.getIdMontoMinimo() + "");
        map.put("monto_acuerdo", item.getMontoAcuerdo() == null ? "" : item.getMontoAcuerdo().toPlainString());
        map.put("fecha_vencimiento_acuerdo", item.getFechaVencimientoAcuerdo() + "");
        map.put("activo", item.getActivo() + "");

        return map;
    }

    @Override
    public Map<String, String> getMapPk(ContratoServicio item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.

        pk.put("id_contrato", item.getContratoServicioPK().getIdContrato() + "");
        pk.put("id_servicio", item.getContratoServicioPK().getIdServicio() + "");

        return pk;
    }

    /**
     * Realiza la búsqueda de un contrato servicio
     *
     * @param idContrato Identificador de contrato
     * @param idServicio Identificador de servicio
     * @return Contrato servicio
     */
    @Override
    public ContratoServicio find(Integer idContrato, Integer idServicio) {
        return super.find(new ContratoServicioPK(idContrato, idServicio));
    }

    /**
     * Verifica si el objeto es valido para crear
     *
     * @param param Parametros
     * @param validarIdContrato Validar id contrato
     * @return Estado
     */
    @Override
    public Boolean validToCreate(ContratoServicioParam param,
            Boolean validarIdContrato) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        ContratoServicio contratoServicio = find(param.getIdContrato(),
                param.getIdServicio());

        if (contratoServicio != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe el contrato servicio"));
        }

        Contrato contrato = facades
                .getContratoFacade()
                .find(param.getIdContrato());

        if (validarIdContrato && contrato == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato"));
        }

        MontoMinimo montoMinimo = param.getIdMontoMinimo() == null
                ? null
                : facades.getMontoMinimoFacade().find(param.getIdMontoMinimo());

        if (param.getIdMontoMinimo() != null && montoMinimo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el monto minimo"));
        }

        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());

        if (servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        } else if (contrato != null
                && !servicio.getIdProducto().equals(contrato.getIdProducto())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto del servicio no corresponde con el producto del contrato"));
        }

        for (ContratoServicioTarifaParam tarifa : param.getTarifas()) {
            facades
                    .getContratoServicioTarifaFacade()
                    .validToCreate(tarifa, Boolean.FALSE);
        }

        return !param.tieneErrores();
    }

    /**
     * Verifica si el objeto es valido para editar
     *
     * @param param Parametros
     * @return Estado
     */
    @Override
    public Boolean validToEdit(ContratoServicioParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        ContratoServicio contratoServicio = find(param.getIdContrato(),
                param.getIdServicio());

        if (contratoServicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato servicio"));
        }

        Contrato contrato = facades
                .getContratoFacade()
                .find(param.getIdContrato());

        if (contrato == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato"));
        }

        MontoMinimo montoMinimo = param.getIdMontoMinimo() == null
                ? null
                : facades.getMontoMinimoFacade().find(param.getIdMontoMinimo());

        if (param.getIdMontoMinimo() != null && montoMinimo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el monto minimo"));
        }

        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());

        if (servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        } else if (contrato != null
                && !servicio.getIdProducto().equals(contrato.getIdProducto())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El producto del servicio no corresponde con el producto del contrato"));
        }

        return !param.tieneErrores();
    }

    @Override
    public ContratoServicio create(ContratoServicioParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, Boolean.TRUE)) {
            return null;
        }

        ContratoServicio item = new ContratoServicio(
                param.getIdContrato(),
                param.getIdServicio());

        item.setMontoAcuerdo(param.getMontoAcuerdo());
        item.setFechaVencimientoAcuerdo(param.getFechaVencimientoAcuerdo());
        item.setIdMontoMinimo(param.getIdMontoMinimo());
        item.setActivo(param.getActivo());

        create(item);

        if (param.getTarifas() != null) {

            for (ContratoServicioTarifaParam tarifa : param.getTarifas()) {
                tarifa.setIdContrato(param.getIdContrato());
                facades.getContratoServicioTarifaFacade().editarOCrear(tarifa, userInfo);
            }
        }

        return item;
    }

    @Override
    public ContratoServicio edit(ContratoServicioParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        ContratoServicio item = find(
                param.getIdContrato(),
                param.getIdServicio());

        item.setMontoAcuerdo(param.getMontoAcuerdo());
        item.setFechaVencimientoAcuerdo(param.getFechaVencimientoAcuerdo());
        item.setIdMontoMinimo(param.getIdMontoMinimo());
        item.setActivo(param.getActivo());

        edit(item);

        return item;
    }

    @Override
    public List<ContratoServicio> find(ContratoServicioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(ContratoServicio.class);
        Root<ContratoServicio> root = cq.from(ContratoServicio.class);
        Join<ContratoServicio, Contrato> c = root.join("contrato");
        Join<ContratoServicio, Servicio> s = root.join("servicio");
        Join<ContratoServicio, MontoMinimo> mm = root.join("montoMinimo", JoinType.LEFT);
        Join<Servicio, ProductoCom> p = s.join("producto");

        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getIdContrato() != null) {
            predList.add(qb.equal(c.get("id"), param.getIdContrato()));
        }

        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }

        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }

        if (param.getServicio() != null && !param.getServicio().trim().isEmpty()) {
            predList.add(qb.like(s.<String>get("descripcion"),
                    String.format("%%%s%%", param.getServicio().trim())));
        }

        if (param.getProducto() != null && !param.getProducto().trim().isEmpty()) {
            predList.add(qb.like(p.<String>get("descripcion"),
                    String.format("%%%s%%", param.getProducto().trim())));
        }

        if (param.getFechaVencimientoAcuerdoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimientoAcuerdo"), param.getFechaVencimientoAcuerdoDesde()));
        }

        if (param.getFechaVencimientoAcuerdoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimientoAcuerdo"), param.getFechaVencimientoAcuerdoDesde()));
        }

        if (param.getFechaVencimientoAcuerdo() != null) {
            predList.add(qb.equal(root.get("fechaVencimientoAcuerdo"), param.getFechaVencimientoAcuerdo()));
        }

        if (param.getMontoAcuerdoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<BigDecimal>get("montoAcuerdo"), param.getMontoAcuerdoDesde()));
        }

        if (param.getMontoAcuerdoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<BigDecimal>get("montoAcuerdo"), param.getMontoAcuerdoHasta()));
        }

        if (param.getMontoAcuerdo() != null) {
            predList.add(qb.equal(root.get("montoAcuerdo"), param.getMontoAcuerdo()));
        }

        if (param.getIdMontoMinimo() != null) {
            predList.add(qb.equal(root.get("idMontoMinimo"), param.getIdMontoMinimo()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(c.get("idCliente"), param.getIdCliente()));
        }

        if (param.getEstadoContrato() != null) {
            predList.add(qb.equal(c.get("estado"), param.getEstadoContrato()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<ContratoServicio> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ContratoServicioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ContratoServicio> root = cq.from(ContratoServicio.class);
        Join<ContratoServicio, Contrato> c = root.join("contrato");
        Join<ContratoServicio, Servicio> s = root.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getIdContrato() != null) {
            predList.add(qb.equal(c.get("id"), param.getIdContrato()));
        }

        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }

        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }

        if (param.getServicio() != null && !param.getServicio().trim().isEmpty()) {
            predList.add(qb.like(s.<String>get("descripcion"),
                    String.format("%%%s%%", param.getServicio().trim())));
        }

        if (param.getProducto() != null && !param.getProducto().trim().isEmpty()) {
            predList.add(qb.like(p.<String>get("descripcion"),
                    String.format("%%%s%%", param.getProducto().trim())));
        }

        if (param.getFechaVencimientoAcuerdoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimientoAcuerdo"), param.getFechaVencimientoAcuerdoDesde()));
        }

        if (param.getFechaVencimientoAcuerdoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimientoAcuerdo"), param.getFechaVencimientoAcuerdoDesde()));
        }

        if (param.getFechaVencimientoAcuerdo() != null) {
            predList.add(qb.equal(root.get("fechaVencimientoAcuerdo"), param.getFechaVencimientoAcuerdo()));
        }

        if (param.getMontoAcuerdoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<BigDecimal>get("montoAcuerdo"), param.getMontoAcuerdoDesde()));
        }

        if (param.getMontoAcuerdoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<BigDecimal>get("montoAcuerdo"), param.getMontoAcuerdoHasta()));
        }

        if (param.getMontoAcuerdo() != null) {
            predList.add(qb.equal(root.get("montoAcuerdo"), param.getMontoAcuerdo()));
        }

        if (param.getIdMontoMinimo() != null) {
            predList.add(qb.equal(root.get("idMontoMinimo"), param.getIdMontoMinimo()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(c.get("idCliente"), param.getIdCliente()));
        }

        if (param.getEstadoContrato() != null) {
            predList.add(qb.equal(c.get("estado"), param.getEstadoContrato()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;

        return result;
    }

    /**
     * Renueva un contratoservicio
     *
     * @param idContratoActual Identificador de contrato actual
     * @param idContratoNuevo Identificador de contrato nuevo
     * @param idServicio Identificador de servicio
     * @return ContratoServicio
     */
    @Override
    public ContratoServicio renovar(Integer idContratoActual, Integer idContratoNuevo, Integer idServicio) {

        ContratoServicio contratoServicio = find(new ContratoServicioPK(idContratoActual, idServicio));
        contratoServicio.setActivo('N');
        edit(contratoServicio);

        contratoServicio.getContratoServicioPK().setIdContrato(idContratoNuevo);
        contratoServicio.setActivo('S');
        create(contratoServicio);

        ContratoServicioTarifaParam param = new ContratoServicioTarifaParam();
        param.setIdContrato(idContratoActual);
        param.setIdServicio(idServicio);
        param.setActivo('S');
        param.isValidToList();
        List<ContratoServicioTarifa> list = facades
                .getContratoServicioTarifaFacade()
                .find(param);

        for (ContratoServicioTarifa item : list) {
            facades.getContratoServicioTarifaFacade()
                    .renovar(idContratoActual, idContratoNuevo, idServicio,
                            item.getContratoServicioTarifaPK().getIdTarifa());
        }

        return contratoServicio;
    }
}
