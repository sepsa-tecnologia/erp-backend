/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoMotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.info.filters.MotivoAnulacionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MotivoAnulacionFacade", mappedName = "MotivoAnulacionFacade")
@Local(MotivoAnulacionFacade.class)
public class MotivoAnulacionFacadeImpl extends FacadeImpl<MotivoAnulacion, MotivoAnulacionParam> implements MotivoAnulacionFacade {

    public MotivoAnulacionFacadeImpl() {
        super(MotivoAnulacion.class);
    }
    
    public Boolean validToCreate(MotivoAnulacionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoMotivoAnulacion tma = facades.getTipoMotivoAnulacionFacade()
                .find(param.getIdTipoMotivoAnulacion(), param.getCodigoTipoMotivoAnulacion());
        
        if(isNull(tma)) {
           param.addError(MensajePojo.createInstance().descripcion("No existe el tipo de motivo de anulación"));
        } else {
            param.setIdTipoMotivoAnulacion(tma.getId());
            param.setCodigoTipoMotivoAnulacion(tma.getCodigo());
        }
        
        MotivoAnulacionParam param1 = new MotivoAnulacionParam();
        param1.setCodigo(param.getCodigo().trim());
        param1.setIdTipoMotivoAnulacion(param.getIdTipoMotivoAnulacion());
        
        MotivoAnulacion ma = findFirst(param1);
        
        if(!isNull(ma)) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(MotivoAnulacionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoMotivoAnulacion tma = facades.getTipoMotivoAnulacionFacade()
                .find(param.getIdTipoMotivoAnulacion(), param.getCodigoTipoMotivoAnulacion());
        
        if(isNull(tma)) {
           param.addError(MensajePojo.createInstance().descripcion("No existe el tipo de motivo de anulación"));
        } else {
            param.setIdTipoMotivoAnulacion(tma.getId());
            param.setCodigoTipoMotivoAnulacion(tma.getCodigo());
        }
        
        MotivoAnulacion item = find(param.getId());
        
        if(isNull(item)) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
            
        } else if (!item.getCodigo().equals(param.getCodigo().trim())
                || !item.getIdTipoMotivoAnulacion().equals(param.getIdTipoMotivoAnulacion())) {
            
            MotivoAnulacionParam param1 = new MotivoAnulacionParam();
            param1.setCodigo(param.getCodigo().trim());
            param1.setIdTipoMotivoAnulacion(param.getIdTipoMotivoAnulacion());

            MotivoAnulacion mei = findFirst(param1);

            if(!isNull(mei)) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public MotivoAnulacion create(MotivoAnulacionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MotivoAnulacion result = new MotivoAnulacion();
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        result.setIdTipoMotivoAnulacion(param.getIdTipoMotivoAnulacion());
        result.setActivo(param.getActivo());
        
        create(result);
        
        return result;
    }

    @Override
    public MotivoAnulacion edit(MotivoAnulacionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MotivoAnulacion result = find(param.getId());
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        result.setIdTipoMotivoAnulacion(param.getIdTipoMotivoAnulacion());
        result.setActivo(param.getActivo());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public MotivoAnulacion find(Integer id, String codigo, String codigoTipoMotivoAnulacion) {
        
        MotivoAnulacionParam param = new MotivoAnulacionParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.setCodigoTipoMotivoAnulacion(codigoTipoMotivoAnulacion);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<MotivoAnulacion> find(MotivoAnulacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MotivoAnulacion.class);
        Root<MotivoAnulacion> root = cq.from(MotivoAnulacion.class);
        Join<MotivoAnulacion, TipoMotivoAnulacion> tma = root.join("tipoMotivoAnulacion");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getIdTipoMotivoAnulacion() != null) {
            predList.add(qb.equal(tma.get("id"), param.getIdTipoMotivoAnulacion()));
        }
        
        if (param.getCodigoTipoMotivoAnulacion() != null && !param.getCodigoTipoMotivoAnulacion().trim().isEmpty()) {
            predList.add(qb.equal(tma.get("codigo"), param.getCodigoTipoMotivoAnulacion().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<MotivoAnulacion> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MotivoAnulacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<MotivoAnulacion> root = cq.from(MotivoAnulacion.class);
        Join<MotivoAnulacion, TipoMotivoAnulacion> tma = root.join("tipoMotivoAnulacion");
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getIdTipoMotivoAnulacion() != null) {
            predList.add(qb.equal(tma.get("id"), param.getIdTipoMotivoAnulacion()));
        }
        
        if (param.getCodigoTipoMotivoAnulacion() != null && !param.getCodigoTipoMotivoAnulacion().trim().isEmpty()) {
            predList.add(qb.equal(tma.get("codigo"), param.getCodigoTipoMotivoAnulacion().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
