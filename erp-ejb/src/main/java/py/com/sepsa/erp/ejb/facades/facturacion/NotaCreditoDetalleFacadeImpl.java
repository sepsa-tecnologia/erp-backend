/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaCreditoDetallePojo;
import py.com.sepsa.erp.ejb.entities.info.Metrica;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NotaCreditoDetalleFacade", mappedName = "NotaCreditoDetalleFacade")
@Local(NotaCreditoDetalleFacade.class)
public class NotaCreditoDetalleFacadeImpl extends FacadeImpl<NotaCreditoDetalle, NotaCreditoDetalleParam> implements NotaCreditoDetalleFacade {

    public NotaCreditoDetalleFacadeImpl() {
        super(NotaCreditoDetalle.class);
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @param validarIdNc validar id nc
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(NotaCreditoDetalleParam param, boolean validarIdNc) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        NotaCredito nc = facades
                .getNotaCreditoFacade()
                .find(param.getIdNotaCredito());

        if (validarIdNc && nc == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de crédito"));
        }

        if(param.getIdFactura() != null) {
            Factura factura = facades
                    .getFacturaFacade()
                    .find(param.getIdFactura());

            if (factura == null) {

                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura"));
            } else {

                param.setFacturaDigital(factura.getDigital());
                param.setCdcFactura(factura.getCdc());
                param.setNroFactura(factura.getNroFactura());
                param.setFechaFactura(factura.getFecha());
                param.setTimbradoFactura(factura.getTalonario().getTimbrado());
                
                if (factura.getAnulado().equals('S')) {

                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se puede crear una NC asociada a una factura anulada"));
                }

                if (factura.getMontoTotalDescuentoGlobal().compareTo(BigDecimal.ZERO) > 0) {
                    
                    if (factura.getSaldo().add(factura.getMontoTotalDescuentoGlobal()).compareTo(param.getMontoTotal()) < 0) {

                    param.addError(MensajePojo.createInstance()
                            .descripcion("El monto total no puede ser mayor al saldo de la factura"));
                    }
                    
                } else {
                    
                    BigDecimal resta = param.getMontoTotal().subtract(factura.getSaldo());
                    
                    if (resta.compareTo(BigDecimal.ONE) >= 0) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("El monto total no puede ser mayor al saldo de la factura"));
                    }
                }
                
            }
        }

        if (param.getIdServicio() != null) {

            Servicio servicio = facades.getServicioFacade().find(param.getIdServicio());

            if (servicio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el servicio"));
            }
        }

        if (param.getIdProducto() != null) {

            Producto producto = facades.getProductoFacade().find(param.getIdProducto());

            if (producto == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el producto"));
            }
        }

        if (param.getIdLiquidacion() != null) {

            Liquidacion liquidacion = facades.getLiquidacionFacade().find(param.getIdLiquidacion());

            if (liquidacion == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No exista la liquidación"));
            }
        }

        return !param.tieneErrores();
    }

    /**
     * Crea una instancia de NotaCreditoDetalle
     *
     * @param param parámetros
     * @param userInfo Usuario
     * @return Instancia
     */
    @Override
    public NotaCreditoDetalle create(NotaCreditoDetalleParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        NotaCreditoDetalle ncd = new NotaCreditoDetalle();
        ncd.setIdNotaCredito(param.getIdNotaCredito());
        ncd.setNroLinea(param.getNroLinea());
        ncd.setIdFactura(param.getIdFactura());
        ncd.setDescripcion(param.getDescripcion());
        ncd.setDatoAdicional(param.getDatoAdicional());
        ncd.setPorcentajeIva(param.getPorcentajeIva());
        ncd.setPorcentajeGravada(param.getPorcentajeGravada());
        ncd.setCantidad(param.getCantidad());
        ncd.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva().stripTrailingZeros());
        ncd.setPrecioUnitarioSinIva(param.getPrecioUnitarioSinIva().stripTrailingZeros());
        ncd.setMontoIva(param.getMontoIva().stripTrailingZeros());
        ncd.setMontoImponible(param.getMontoImponible().stripTrailingZeros());
        ncd.setMontoTotal(param.getMontoTotal().stripTrailingZeros());
        ncd.setIdLiquidacion(param.getIdLiquidacion());
        ncd.setIdServicio(param.getIdServicio());
        ncd.setIdProducto(param.getIdProducto());
        ncd.setFacturaDigital(param.getFacturaDigital());
        ncd.setCdcFactura(param.getCdcFactura());
        ncd.setTimbradoFactura(param.getTimbradoFactura());
        ncd.setNroFactura(param.getNroFactura());
        ncd.setFechaFactura(param.getFechaFactura());
        ncd.setDescuentoParticularUnitario(param.getDescuentoParticularUnitario());
        ncd.setMontoDescuentoParticular(param.getMontoDescuentoParticular());
        ncd.setMontoExentoGravado(param.getMontoExentoGravado());

        create(ncd);

        facades.getFacturaFacade().actualizarSaldoFactura(
                param.getIdFactura(),
                param.getMontoTotal());

        return ncd;
    }

    @Override
    public List<NotaCreditoDetallePojo> findPojo(NotaCreditoDetalleParam param) {

        AbstractFind find = new AbstractFind(NotaCreditoDetallePojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idNotaCredito"),
                        getPath("root").get("nroLinea"),
                        getPath("root").get("cantidad"),
                        getPath("root").get("idFactura"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("porcentajeIva"),
                        getPath("root").get("precioUnitarioConIva"),
                        getPath("root").get("precioUnitarioSinIva"),
                        getPath("root").get("montoIva"),
                        getPath("root").get("montoImponible"),
                        getPath("root").get("montoTotal"),
                        getPath("nc").get("nroNotaCredito"),
                        getPath("nc").get("fecha"),
                        getPath("nc").get("razonSocial"),
                        getPath("nc").get("ruc"),
                        getPath("root").get("timbradoFactura"),
                        qb.coalesce(getPath("root").get("nroFactura"), getPath("factura").get("nroFactura")),
                        qb.coalesce(getPath("root").get("fechaFactura"), getPath("factura").get("fecha")),
                        qb.coalesce(getPath("root").get("cdcFactura"), getPath("factura").get("cdc")),
                        qb.coalesce(getPath("root").get("facturaDigital"), getPath("factura").get("digital")),
                        getPath("nc").get("anulado"),
                        getPath("metrica").get("codigo"),
                        getPath("metrica").get("descripcion"),
                        getPath("root").get("montoDescuentoParticular"),
                        getPath("root").get("descuentoParticularUnitario"),
                        getPath("producto").get("fechaVencimientoLote"),
                        getPath("producto").get("nroLote"),
                        getPath("root").get("datoAdicional"),
                        getPath("producto").get("codigoInterno"),
                        getPath("root").get("porcentajeGravada"),
                        getPath("root").get("montoExentoGravado"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<NotaCreditoDetalle> find(NotaCreditoDetalleParam param) {

        AbstractFind find = new AbstractFind(NotaCreditoDetalle.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, NotaCreditoDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<NotaCreditoDetalle> root = cq.from(NotaCreditoDetalle.class);
        Join<NotaCreditoDetalle, NotaCredito> nc = root.join("notaCredito", JoinType.LEFT);
        Join<NotaCreditoDetalle, Factura> factura = root.join("factura", JoinType.LEFT);
        Join<NotaCreditoDetalle, Producto> producto = root.join("producto", JoinType.LEFT);
        Join<Producto, Metrica> metrica = producto.join("metrica", JoinType.LEFT);
        find.addPath("root", root);
        find.addPath("nc", nc);
        find.addPath("factura", factura);
        find.addPath("producto", producto);
        find.addPath("metrica",metrica);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdNotaCredito() != null) {
            predList.add(qb.equal(root.get("idNotaCredito"), param.getIdNotaCredito()));
        }

        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }

        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }

        if (param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

            Date date = null;

            try {
                date = sdf.parse(param.getAnhoMes());
            } catch (Exception e) {
            }

            if (date != null) {
                Calendar from = Calendar.getInstance();
                from.setTime(date);
                from.set(Calendar.DAY_OF_MONTH, 1);
                from.set(Calendar.HOUR, 0);
                from.set(Calendar.MINUTE, 0);
                from.set(Calendar.SECOND, 0);

                Calendar to = Calendar.getInstance();
                to.setTime(date);
                to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
                to.set(Calendar.HOUR, 23);
                to.set(Calendar.MINUTE, 59);
                to.set(Calendar.SECOND, 59);

                predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), from.getTime()));
                predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), to.getTime()));
            }
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(nc.get("fecha")),
                qb.asc(nc.get("id")),
                qb.asc(root.get("nroLinea")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaCreditoDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaCreditoDetalle> root = cq.from(NotaCreditoDetalle.class);
        Join<NotaCreditoDetalle, NotaCredito> nc = root.join("notaCredito", JoinType.LEFT);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdNotaCredito() != null) {
            predList.add(qb.equal(root.get("idNotaCredito"), param.getIdNotaCredito()));
        }

        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }

        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }

        if (param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

            Date date = null;

            try {
                date = sdf.parse(param.getAnhoMes());
            } catch (Exception e) {
            }

            if (date != null) {
                Calendar from = Calendar.getInstance();
                from.setTime(date);
                from.set(Calendar.DAY_OF_MONTH, 1);
                from.set(Calendar.HOUR, 0);
                from.set(Calendar.MINUTE, 0);
                from.set(Calendar.SECOND, 0);

                Calendar to = Calendar.getInstance();
                to.setTime(date);
                to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
                to.set(Calendar.HOUR, 23);
                to.set(Calendar.MINUTE, 59);
                to.set(Calendar.SECOND, 59);

                predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), from.getTime()));
                predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), to.getTime()));
            }
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
