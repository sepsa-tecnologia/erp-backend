/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.MontoMinimo;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MontoMinimoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "MontoMinimoFacade", mappedName = "MontoMinimoFacade")
@Local(MontoMinimoFacade.class)
public class MontoMinimoFacadeImpl extends FacadeImpl<MontoMinimo, MontoMinimoParam> implements MontoMinimoFacade {

    public MontoMinimoFacadeImpl() {
        super(MontoMinimo.class);
    }

    @Override
    public Map<String, String> getMapConditions(MontoMinimo item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("id_cliente", item.getIdCliente() + "");
        map.put("id_servicio", item.getIdServicio() + "");
        map.put("monto", item.getMonto() == null ? "" : item.getMonto().toPlainString());
        map.put("activo", item.getActivo() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(MontoMinimo item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(MontoMinimoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        }
        
        Cliente cliente = param.getIdCliente() == null
                ? null
                : facades.getClienteFacade().find(param.getIdCliente());
        
        if(cliente == null && param.getIdCliente() != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(MontoMinimoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        MontoMinimo montoMinimo = facades
                .getMontoMinimoFacade()
                .find(param.getId());
        
        if(montoMinimo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el monto minimo"));
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        }
        
        Cliente cliente = param.getIdCliente() == null
                ? null
                : facades.getClienteFacade().find(param.getIdCliente());
        
        if(cliente == null && param.getIdCliente() != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        return Boolean.TRUE;
    }
    
    /**
     * Crea una instancia 
     * @param param parámetrps
     * @return Instancia
     */
    @Override
    public MontoMinimo create(MontoMinimoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MontoMinimo minimo = new MontoMinimo();
        minimo.setIdCliente(param.getIdCliente());
        minimo.setIdServicio(param.getIdServicio());
        minimo.setActivo(param.getActivo());
        minimo.setMonto(param.getMonto());
        create(minimo);
        
        return minimo;
    }
    
    @Override
    public MontoMinimo edit(MontoMinimoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MontoMinimo minimo = new MontoMinimo();
        minimo.setIdCliente(param.getIdCliente());
        minimo.setIdServicio(param.getIdServicio());
        minimo.setActivo(param.getActivo());
        minimo.setMonto(param.getMonto());
        create(minimo);
        
        return minimo;
    }
    
    @Override
    public List<MontoMinimo> find(MontoMinimoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MontoMinimo.class);
        Root<MontoMinimo> root = cq.from(MontoMinimo.class);
        Join<MontoMinimo, Cliente> c = root.join("cliente", JoinType.LEFT);
        Join<MontoMinimo, Servicio> s = root.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");
        
        cq.select(root);
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getCliente() != null && !param.getCliente().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("razonSocial"),
                    String.format("%%%s%%", param.getCliente().trim())));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(c.get("id"), param.getIdCliente()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<MontoMinimo> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(MontoMinimoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<MontoMinimo> root = cq.from(MontoMinimo.class);
        Join<MontoMinimo, Cliente> c = root.join("cliente", JoinType.LEFT);
        Join<MontoMinimo, Servicio> s = root.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");
        
        cq.select(qb.count(root));
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getCliente() != null && !param.getCliente().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("razonSocial"),
                    String.format("%%%s%%", param.getCliente().trim())));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(c.get("id"), param.getIdCliente()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
