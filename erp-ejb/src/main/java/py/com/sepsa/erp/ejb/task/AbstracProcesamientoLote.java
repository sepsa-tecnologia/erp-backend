/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import javax.ejb.EJB;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.facades.Facades;

/**
 *
 * @author Jonathan Bernal
 */
public abstract class AbstracProcesamientoLote {

    @EJB
    protected Facades facades;

    public abstract void procesar(Lote lote);
}
