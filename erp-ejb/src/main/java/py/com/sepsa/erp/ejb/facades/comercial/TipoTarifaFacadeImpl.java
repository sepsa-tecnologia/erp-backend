/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.TipoTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.TipoTarifaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "TipoTarifaFacade", mappedName = "TipoTarifaFacade")
@Local(TipoTarifaFacade.class)
public class TipoTarifaFacadeImpl extends FacadeImpl<TipoTarifa, TipoTarifaParam> implements TipoTarifaFacade {

    public TipoTarifaFacadeImpl() {
        super(TipoTarifa.class);
    }

    @Override
    public Map<String, String> getMapConditions(TipoTarifa item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("descripcion", item.getDescripcion() + "");
        map.put("activo", item.getActivo() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(TipoTarifa item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @return Estado
     */
    public Boolean validToCreate(TipoTarifaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de Tipo Tarifa
     * @param param Parámetros
     * @return TipoTarifa
     */
    @Override
    public TipoTarifa create(TipoTarifaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoTarifa tipoTarifa = new TipoTarifa();
        tipoTarifa.setDescripcion(param.getDescripcion());
        tipoTarifa.setActivo(param.getActivo());
        
        create(tipoTarifa);
        
        return tipoTarifa;
    }
    
    @Override
    public List<TipoTarifa> find(TipoTarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoTarifa.class);
        Root<TipoTarifa> root = cq.from(TipoTarifa.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<TipoTarifa> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoTarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoTarifa> root = cq.from(TipoTarifa.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
