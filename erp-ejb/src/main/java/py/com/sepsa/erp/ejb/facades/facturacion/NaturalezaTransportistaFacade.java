/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NaturalezaTransportista;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Antonella Lucero
 */
@Local
public interface NaturalezaTransportistaFacade extends Facade<NaturalezaTransportista, CommonParam, UserInfoImpl> {

    public NaturalezaTransportista find(Integer id, String codigo);
    
}