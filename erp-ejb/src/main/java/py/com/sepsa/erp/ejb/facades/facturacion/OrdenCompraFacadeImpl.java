/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraDetallePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraPojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.filters.LocalParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.LocalPojo;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Dates;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "OrdenCompraFacade", mappedName = "OrdenCompraFacade")
@javax.ejb.Local(OrdenCompraFacade.class)
public class OrdenCompraFacadeImpl extends FacadeImpl<OrdenCompra, OrdenCompraParam> implements OrdenCompraFacade {

    public OrdenCompraFacadeImpl() {
        super(OrdenCompra.class);
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param userInfo Información del usuario
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToEdit(UserInfoImpl userInfo, OrdenCompraParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getId());
        OrdenCompraPojo ordenCompra = findFirstPojo(ocparam);
        if(ordenCompra == null) {
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "ORDEN_COMPRA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(!isNull(param.getIdLocalOrigen())) {
            
            LocalParam lparam = new LocalParam();
            lparam.setId(param.getIdLocalOrigen());
            LocalPojo lpojo = facades.getLocalFacade().findFirstPojo(lparam);
            
            if(isNull(lpojo)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el local origen"));
            } else {
                param.setIdClienteOrigen(lpojo.getIdPersona());
            }
        }
        
        if(!isNull(param.getIdClienteOrigen())) {
            
            ClienteParam cparam = new ClienteParam();
            cparam.setIdCliente(param.getIdClienteOrigen());
            Long csize = facades.getClienteFacade().findSize(cparam);
            
            if(csize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente origen"));
            }
        }
        
        if(!isNull(param.getIdLocalDestino())) {
            
            LocalParam lparam = new LocalParam();
            lparam.setId(param.getIdLocalDestino());
            LocalPojo lpojo = facades.getLocalFacade().findFirstPojo(lparam);
            
            if(isNull(lpojo)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el local destino"));
            } else {
                param.setIdClienteDestino(lpojo.getIdPersona());
            }
        }
        
        if(!isNull(param.getIdClienteDestino())) {
            
            ClienteParam cparam = new ClienteParam();
            cparam.setIdCliente(param.getIdClienteDestino());
            Long csize = facades.getClienteFacade().findSize(cparam);
            
            if(csize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente destino"));
            }
        }
        
        if(param.getOrdenCompraDetalles() != null) {
            for (OrdenCompraDetalleParam item : param.getOrdenCompraDetalles()) {
                item.setCodigoEstadoOrdenCompra(param.getCodigoEstado());
                if(item.getId() == null) {
                    facades.getOrdenCompraDetalleFacade().validToCreate(userInfo, item, true);
                } else {
                    facades.getOrdenCompraDetalleFacade().validToEdit(userInfo, item);
                }
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para cambiar el estado
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToChangeState(OrdenCompraParam param) {
        
        if(!param.isValidToUpdateState()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getId());
        Long ocsize = findSize(ocparam);
        
        if(ocsize <= 0) {
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "ORDEN_COMPRA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para anular
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToCancel(OrdenCompraParam param) {
        
        if(!param.isValidToCancel()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getId());
        OrdenCompraPojo ordenCompra = findFirstPojo(ocparam);
        
        if(ordenCompra == null) {
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
        } else if (ordenCompra.getAnulado().equals('S')
                || ordenCompra.getCodigoEstado().equals("RECHAZADO")) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La orden de compra ya se encuentra anulada"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "ORDEN_COMPRA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para anular
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToEnable(OrdenCompraParam param) {
        
        if(!param.isValidToEnable()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getId());
        OrdenCompraPojo ordenCompra = findFirstPojo(ocparam);
        
        if(ordenCompra == null) {
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la orden de compra"));
        } else if (!ordenCompra.getAnulado().equals('S')
                || !ordenCompra.getCodigoEstado().equals("RECHAZADO")) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La orden de compra no se encuentra anulada"));
        } else {
            param.setCodigoEstado(ordenCompra.getRecibido().equals('S') ? "RECIBIDO" : "ENVIADO");
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "ORDEN_COMPRA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Boolean validToCreate(UserInfoImpl userInfo, OrdenCompraParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "ORDEN_COMPRA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        if(!isNull(param.getIdClienteOrigen())) {
            
            ClienteParam cparam = new ClienteParam();
            cparam.setIdCliente(param.getIdClienteOrigen());
            Long clienteSize = facades.getClienteFacade().findSize(cparam);
            
            if(clienteSize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente origen"));
            }
        }
        
        if(!isNull(param.getIdLocalOrigen())) {
            
            LocalParam lparam = new LocalParam();
            lparam.setId(param.getIdLocalOrigen());
            Long localSize = facades.getLocalFacade().findSize(lparam);
            
            if(localSize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el local origen"));
            }
        }
        
        if(!isNull(param.getIdClienteDestino())) {
            
            ClienteParam cparam = new ClienteParam();
            cparam.setIdCliente(param.getIdClienteDestino());
            Long clienteSize = facades.getClienteFacade().findSize(cparam);
            
            if(clienteSize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente destino"));
            }
        }
        
        if(!isNull(param.getIdLocalDestino())) {
            
            LocalParam lparam = new LocalParam();
            lparam.setId(param.getIdLocalDestino());
            Long localSize = facades.getLocalFacade().findSize(lparam);
            
            if(localSize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el local destino"));
            }
        }
        
        if(!isNullOrEmpty(param.getHashProcesamientoArchivo())) {
            
            OrdenCompraParam param1 = new OrdenCompraParam();
            param1.setHashProcesamientoArchivo(param.getHashProcesamientoArchivo());
            param1.setCodigoEstadoExcluido("RECHAZADO");
            Long size = findSize(param1);
            
            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe una orden de compra registrada con el mismo hash de archivo"));
            }
        }
        
        if(param.getRecibido().equals('N')) {

            String id = String.valueOf(maxId() + 1L);
            String date = Dates.formatToString(Calendar.getInstance().getTime(), Dates.DateFormat.DATE_SHORT_ALT);
            String nroOrdenCompra = String.format("OC-%s-%s", date, id);
            
            param.setNroOrdenCompra(nroOrdenCompra);
        }
        
        if(param.getOrdenCompraDetalles() != null) {
            for (OrdenCompraDetalleParam item : param.getOrdenCompraDetalles()) {
                item.setCodigoEstadoOrdenCompra(param.getCodigoEstado());
                facades.getOrdenCompraDetalleFacade().validToCreate(userInfo, item, Boolean.FALSE);
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToUpdateFileProcess(OrdenCompraParam param) {
        
        if(!param.isValidToUpdateFileProcess()) {
            return Boolean.FALSE;
        }
        
        OrdenCompra ordenCompra = find(param.getId());
        
        if(isNull(ordenCompra)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la orden de compra"));
        }
        
        ProcesamientoArchivo procesamientoArchivo = facades.getProcesamientoArchivoFacade().find(param.getIdProcesamientoArchivo());
        
        if(isNull(procesamientoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el procesamiento de archivo"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToGetPdf(OrdenCompraParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        Long fsize = findSize(param);

        if (fsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la orden de compra"));
        }

        return !param.tieneErrores();
    }
    
    @Override
    public OrdenCompra edit(OrdenCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(userInfo, param)) {
            return null;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getId());
        OrdenCompraPojo oldOc = userInfo.getTieneModuloInventario()
                ? findFirstPojo(ocparam)
                : null;
        
        OrdenCompra item = find(param.getId());
        item.setNroOrdenCompra(param.getNroOrdenCompra());
        item.setFechaRecepcion(param.getFechaRecepcion());
        item.setIdEstado(param.getIdEstado());
        item.setRecibido(param.getRecibido());
        item.setRegistroInventario(param.getRegistroInventario());
        item.setGeneradoEdi(param.getGeneradoEdi());
        item.setArchivoEdi(param.getArchivoEdi());
        item.setIdClienteOrigen(param.getIdClienteOrigen());
        item.setIdLocalOrigen(param.getIdLocalOrigen());
        item.setIdClienteDestino(param.getIdClienteDestino());
        item.setIdLocalDestino(param.getIdLocalDestino());
        item.setObservacion(param.getObservacion());
        item.setMontoIva5(param.getMontoIva5());
        item.setMontoImponible5(param.getMontoImponible5());
        item.setMontoTotal5(param.getMontoTotal5());
        item.setMontoIva10(param.getMontoIva10());
        item.setMontoImponible10(param.getMontoImponible10());
        item.setMontoTotal10(param.getMontoTotal10());
        item.setMontoTotalExento(param.getMontoTotalExento());
        item.setMontoIvaTotal(param.getMontoIvaTotal());
        item.setMontoImponibleTotal(param.getMontoImponibleTotal());
        item.setMontoTotalOrdenCompra(param.getMontoTotalOrdenCompra());
        
        edit(item);
        
        OrdenCompraPojo newOc = userInfo.getTieneModuloInventario()
                ? findFirstPojo(ocparam)
                : null;
        
        Map<Integer, OrdenCompraDetalle> newMap = new HashMap<>();
        Map<Integer, OrdenCompraDetallePojo> oldMap = new HashMap<>();

        if(param.getOrdenCompraDetalles() != null) {
            for (OrdenCompraDetalleParam detalle : param.getOrdenCompraDetalles()) {
                
                OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
                ocdparam.setId(detalle.getId());
                OrdenCompraDetallePojo oldOcd = userInfo.getTieneModuloInventario()
                        && detalle.getId() != null
                        ? facades.getOrdenCompraDetalleFacade().findFirstPojo(ocdparam)
                        : null;
                
                OrdenCompraDetalle newOcd;
                
                if(detalle.getId() == null) {
                    newOcd = facades.getOrdenCompraDetalleFacade().create(detalle, userInfo);
                    ocdparam.setId(newOcd.getId());
                } else {
                    newOcd = facades.getOrdenCompraDetalleFacade().edit(detalle, userInfo);
                }
                
                oldMap.put(detalle.getId(), oldOcd);
                newMap.put(detalle.getId(), newOcd);
            }
        } else {
            OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
            ocdparam.setIdOrdenCompra(param.getId());
            List<OrdenCompraDetallePojo> list = userInfo.getTieneModuloInventario()
                    ? facades.getOrdenCompraDetalleFacade().findPojo(ocdparam)
                    : null;
            if(list != null) {
                for (OrdenCompraDetallePojo detalle : list) {
                    oldMap.put(detalle.getId(), detalle);
                    newMap.put(detalle.getId(), detalle.getOrdenCompraDetalle());
                }
            }
        }
        
        if(userInfo.getTieneModuloInventario()) {
            facades.getInventarioUtils().actualizarOrdenCompra(userInfo, oldOc, newOc, oldMap, newMap);
        }
        
        return item;
    }
    
    @Override
    public OrdenCompra changeState(OrdenCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToChangeState(param)) {
            return null;
        }
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(param.getId());
        OrdenCompraPojo oldOc = userInfo.getTieneModuloInventario()
                ? findFirstPojo(ocparam)
                : null;
        
        OrdenCompra item = find(param.getId());
        item.setIdEstado(param.getIdEstado());
        
        edit(item);
        
        OrdenCompraPojo newOc = userInfo.getTieneModuloInventario()
                ? findFirstPojo(ocparam)
                : null;
        
        if(userInfo.getTieneModuloInventario()
                && item.getRecibido().equals('S')
                && param.getCodigoEstado().equals("FACTURADO")) {
            facades.getInventarioUtils().facturarOrdenCompra(userInfo, newOc);
        } else if(userInfo.getTieneModuloInventario()) {
            facades.getInventarioUtils().actualizarOrdenCompra(userInfo, oldOc, newOc, null, null);
        }
        
        return item;
    }
    
    @Override
    public OrdenCompra cancel(OrdenCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToCancel(param)) {
            return null;
        }
        
        OrdenCompra item = find(param.getId());
        item.setIdEstado(param.getIdEstado());
        item.setAnulado('S');
        
        edit(item);
        
        if(userInfo.getTieneModuloInventario()
                && item.getRecibido().equals('S')) {
            OrdenCompraParam ocparam = new OrdenCompraParam();
            ocparam.setId(param.getId());
            OrdenCompraPojo newOc = findFirstPojo(ocparam);
            facades.getInventarioUtils().anularOrdenCompra(userInfo, newOc);
        }
        
        return item;
    }
    
    @Override
    public OrdenCompra enable(OrdenCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToEnable(param)) {
            return null;
        }
        
        OrdenCompra item = find(param.getId());
        item.setIdEstado(param.getIdEstado());
        item.setAnulado('N');
        
        edit(item);
        
        return item;
    }
    
    @Override
    public OrdenCompra updateFileProcess(OrdenCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToUpdateFileProcess(param)) {
            return null;
        }
        
        OrdenCompra item = find(param.getId());
        item.setIdProcesamientoArchivo(param.getIdProcesamientoArchivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public OrdenCompra create(OrdenCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(userInfo, param)) {
            return null;
        }
        
        OrdenCompra ordeCompra = new OrdenCompra();
        ordeCompra.setIdEmpresa(userInfo.getIdEmpresa());
        ordeCompra.setFechaInsercion(Calendar.getInstance().getTime());
        ordeCompra.setRecibido(param.getRecibido());
        ordeCompra.setAnulado(param.getAnulado());
        ordeCompra.setRegistroInventario(param.getRegistroInventario());
        ordeCompra.setGeneradoEdi(param.getGeneradoEdi());
        ordeCompra.setArchivoEdi(param.getArchivoEdi());
        ordeCompra.setNroOrdenCompra(param.getNroOrdenCompra());
        ordeCompra.setFechaRecepcion(param.getFechaRecepcion());
        ordeCompra.setIdEstado(param.getIdEstado());
        ordeCompra.setIdClienteOrigen(param.getIdClienteOrigen());
        ordeCompra.setIdLocalOrigen(param.getIdLocalOrigen());
        ordeCompra.setIdClienteDestino(param.getIdClienteDestino());
        ordeCompra.setIdLocalDestino(param.getIdLocalDestino());
        ordeCompra.setObservacion(param.getObservacion());
        ordeCompra.setMontoIva5(param.getMontoIva5());
        ordeCompra.setMontoImponible5(param.getMontoImponible5());
        ordeCompra.setMontoTotal5(param.getMontoTotal5());
        ordeCompra.setMontoIva10(param.getMontoIva10());
        ordeCompra.setMontoImponible10(param.getMontoImponible10());
        ordeCompra.setMontoTotal10(param.getMontoTotal10());
        ordeCompra.setMontoTotalExento(param.getMontoTotalExento());
        ordeCompra.setMontoIvaTotal(param.getMontoIvaTotal());
        ordeCompra.setMontoImponibleTotal(param.getMontoImponibleTotal());
        ordeCompra.setMontoTotalOrdenCompra(param.getMontoTotalOrdenCompra());
        
        create(ordeCompra);
        
        OrdenCompraParam ocparam = new OrdenCompraParam();
        ocparam.setId(ordeCompra.getId());
        OrdenCompraPojo newOc = findFirstPojo(ocparam);
        
        Map<Integer, OrdenCompraDetalle> newMap = new HashMap<>();
        Map<Integer, OrdenCompraDetallePojo> oldMap = new HashMap<>();
        
        if(param.getOrdenCompraDetalles() != null) {
            for (OrdenCompraDetalleParam item : param.getOrdenCompraDetalles()) {
                item.setIdOrdenCompra(ordeCompra.getId());
                OrdenCompraDetalle newOcd = facades.getOrdenCompraDetalleFacade().create(item, userInfo);
                oldMap.put(newOcd.getId(), null);
                newMap.put(newOcd.getId(), newOcd);
            }
        }
        
        if(userInfo.getTieneModuloInventario()) {
            facades.getInventarioUtils().actualizarOrdenCompra(userInfo, null, newOc, oldMap, newMap);
        }
        
        return ordeCompra;
    }
    
    @Override
    public List<OrdenCompraPojo> findPojo(OrdenCompraParam param) {

        AbstractFind find = new AbstractFind(OrdenCompraPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("idClienteOrigen"),
                        getPath("clienteOrigen").get("razonSocial"),
                        getPath("root").get("idClienteDestino"),
                        getPath("clienteDestino").get("razonSocial"),
                        getPath("root").get("idLocalOrigen"),
                        getPath("localOrigen").get("descripcion"),
                        getPath("root").get("idLocalDestino"),
                        getPath("localDestino").get("descripcion"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("fechaRecepcion"),
                        getPath("root").get("nroOrdenCompra"),
                        getPath("root").get("observacion"),
                        getPath("root").get("recibido"),
                        getPath("root").get("anulado"),
                        getPath("root").get("registroInventario"),
                        getPath("root").get("archivoEdi"),
                        getPath("root").get("generadoEdi"),
                        getPath("root").get("idProcesamientoArchivo"),
                        getPath("root").get("montoIva5"),
                        getPath("root").get("montoImponible5"),
                        getPath("root").get("montoTotal5"),
                        getPath("root").get("montoIva10"),
                        getPath("root").get("montoImponible10"),
                        getPath("root").get("montoTotal10"),
                        getPath("root").get("montoTotalExento"),
                        getPath("root").get("montoIvaTotal"),
                        getPath("root").get("montoImponibleTotal"),
                        getPath("root").get("montoTotalOrdenCompra"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<OrdenCompra> find(OrdenCompraParam param) {

        AbstractFind find = new AbstractFind(OrdenCompra.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, OrdenCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<OrdenCompra> root = cq.from(OrdenCompra.class);
        Join<OrdenCompra, Estado> estado = root.join("estado");
        Join<OrdenCompra, Empresa> empresa = root.join("empresa");
        Join<OrdenCompra, Local> localOrigen = root.join("localOrigen", JoinType.LEFT);
        Join<OrdenCompra, Local> localDestino = root.join("localDestino", JoinType.LEFT);
        Join<OrdenCompra, Cliente> clienteOrigen = root.join("clienteOrigen", JoinType.LEFT);
        Join<OrdenCompra, Cliente> clienteDestino = root.join("clienteDestino", JoinType.LEFT);
        Join<OrdenCompra, ProcesamientoArchivo> procesamientoArchivo = root.join("procesamientoArchivo", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("estado", estado);
        find.addPath("empresa", empresa);
        find.addPath("localOrigen", localOrigen);
        find.addPath("localDestino", localDestino);
        find.addPath("clienteOrigen", clienteOrigen);
        find.addPath("clienteDestino", clienteDestino);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado() .trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getCodigoEstadoExcluido() != null && !param.getCodigoEstadoExcluido() .trim().isEmpty()) {
            predList.add(qb.notEqual(estado.get("codigo"), param.getCodigoEstadoExcluido().trim()));
        }
        
        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroOrdenCompra"), param.getNroOrdenCompra().trim()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }
        
        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }
        
        if (param.getRecibido() != null) {
            predList.add(qb.equal(root.get("recibido"), param.getRecibido()));
        }
        
        if (param.getIdClienteOrigen() != null) {
            predList.add(qb.equal(root.get("idClienteOrigen"), param.getIdClienteOrigen()));
        }
        
        if (param.getIdLocalOrigen() != null) {
            predList.add(qb.equal(root.get("idLocalOrigen"), param.getIdLocalOrigen()));
        }
        
        if (param.getIdClienteDestino() != null) {
            predList.add(qb.equal(root.get("idClienteDestino"), param.getIdClienteDestino()));
        }
        
        if (param.getIdLocalDestino() != null) {
            predList.add(qb.equal(root.get("idLocalDestino"), param.getIdLocalDestino()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getFechaRecepcionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaRecepcion"), param.getFechaRecepcionDesde()));
        }
        
        if (param.getFechaRecepcionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaRecepcion"), param.getFechaRecepcionHasta()));
        }
        
        if (param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }

        if (param.getHashProcesamientoArchivo() != null && !param.getHashProcesamientoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(procesamientoArchivo.get("hash"), param.getHashProcesamientoArchivo().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
                
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(OrdenCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<OrdenCompra> root = cq.from(OrdenCompra.class);
        Join<OrdenCompra, Estado> estado = root.join("estado");
        Join<OrdenCompra, ProcesamientoArchivo> procesamientoArchivo = root.join("procesamientoArchivo", JoinType.LEFT);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado() .trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getCodigoEstadoExcluido() != null && !param.getCodigoEstadoExcluido() .trim().isEmpty()) {
            predList.add(qb.notEqual(estado.get("codigo"), param.getCodigoEstadoExcluido().trim()));
        }
        
        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroOrdenCompra"), param.getNroOrdenCompra().trim()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }
        
        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }
        
        if (param.getRecibido() != null) {
            predList.add(qb.equal(root.get("recibido"), param.getRecibido()));
        }
        
        if (param.getIdClienteOrigen() != null) {
            predList.add(qb.equal(root.get("idClienteOrigen"), param.getIdClienteOrigen()));
        }
        
        if (param.getIdLocalOrigen() != null) {
            predList.add(qb.equal(root.get("idLocalOrigen"), param.getIdLocalOrigen()));
        }
        
        if (param.getIdClienteDestino() != null) {
            predList.add(qb.equal(root.get("idClienteDestino"), param.getIdClienteDestino()));
        }
        
        if (param.getIdLocalDestino() != null) {
            predList.add(qb.equal(root.get("idLocalDestino"), param.getIdLocalDestino()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getFechaRecepcionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaRecepcion"), param.getFechaRecepcionDesde()));
        }
        
        if (param.getFechaRecepcionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaRecepcion"), param.getFechaRecepcionHasta()));
        }

        if (param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }

        if (param.getHashProcesamientoArchivo() != null && !param.getHashProcesamientoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(procesamientoArchivo.get("hash"), param.getHashProcesamientoArchivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public Long maxId() {
        
        String sql = "select max(id) from facturacion.orden_compra";

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public byte[] getPdfOrdenCompra(OrdenCompraParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        OrdenCompraPojo cip = findFirstPojo(param);
        
        byte[] result = null;

        Closer closer = Closer.instance();

        try {

            py.com.sepsa.erp.reporte.pojos.ordencompra.OrdenCompraParam params
                    = facades.getReportUtils().getOrdenCompraParam(closer, cip);

            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_PREPARACION_ORDEN_COMPRA");
            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);

            if (isNull(is)) {
                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para la orden de compra"));
            } else {
                //Obtenemos una conexion del DataSource
                Context ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                try (Connection connection = ds.getConnection()) {
                    result = JasperReporteGenerator.exportReportToPdfBytes(params, connection, is);
                }
            }

        } finally {
            closer.close();
        }

        return result;
    }
}
