/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicio;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicioTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicioTarifaPK;
import py.com.sepsa.erp.ejb.entities.comercial.Tarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioTarifaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ContratoServicioTarifaFacade", mappedName = "ContratoServicioTarifaFacade")
@Local(ContratoServicioTarifaFacade.class)
public class ContratoServicioTarifaFacadeImpl extends FacadeImpl<ContratoServicioTarifa, ContratoServicioTarifaParam> implements ContratoServicioTarifaFacade {

    public ContratoServicioTarifaFacadeImpl() {
        super(ContratoServicioTarifa.class);
    }
    
    @Override
    public Map<String, String> getMapConditions(ContratoServicioTarifa item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id_contrato", item.getContratoServicioTarifaPK().getIdContrato() + "");
        map.put("id_servicio", item.getContratoServicioTarifaPK().getIdServicio() + "");
        map.put("id_tarifa", item.getContratoServicioTarifaPK().getIdTarifa() + "");
        map.put("activo", item.getActivo() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(ContratoServicioTarifa item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id_contrato", item.getContratoServicioTarifaPK().getIdContrato() + "");
        pk.put("id_servicio", item.getContratoServicioTarifaPK().getIdServicio() + "");
        pk.put("id_tarifa", item.getContratoServicioTarifaPK().getIdTarifa() + "");
        
        return pk;
    }
    
    /**
     * Realiza la búsqueda de un contrato servicio
     * @param idContrato Identificador de contrato
     * @param idServicio Identificador de servicio
     * @param idTarifa Identificador de tarifa
     * @return Contrato servicio tarifa
     */
    @Override
    public ContratoServicioTarifa find(Integer idContrato, Integer idServicio, Integer idTarifa) {
        return super.find(new ContratoServicioTarifaPK(idContrato, idServicio, idTarifa));
    }
    
    /**
     * Valida si el objeto es válido para crear
     * @param param Parámetros
     * @param validadContratoServicio Bandera para validar contratoservicio
     * @return bandera
     */
    @Override
    public Boolean validToCreate(ContratoServicioTarifaParam param,
            boolean validadContratoServicio) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ContratoServicioTarifa contratoServicioTarifa =
                find(param.getIdContrato(), param.getIdServicio(),
                        param.getIdTarifa());
        
        if(contratoServicioTarifa != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe el contrato servicio tarifa"));
        }
        
        ContratoServicio contratoServicio = facades
                .getContratoServicioFacade()
                .find(param.getIdContrato(), param.getIdServicio());
        
        if(validadContratoServicio && contratoServicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato servicio"));
        }
        
        Contrato contrato = facades
                .getContratoFacade()
                .find(param.getIdContrato());
        
        if(validadContratoServicio && contrato == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato"));
        }
        
        Tarifa tarifa = facades
                .getTarifaFacade()
                .find(param.getIdTarifa());
        
        if(tarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la tarifa"));
        }
        
        if(contrato != null && tarifa != null
                && !contrato.getIdMoneda().equals(tarifa.getIdMoneda())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La moneda de la tarifa no corresponde con la del contrato"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Valida si el objeto es válido para editar
     * @param param Parámetros
     * @return bandera
     */
    @Override
    public Boolean validToEdit(ContratoServicioTarifaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ContratoServicioTarifa contratoServicioTarifa =
                find(param.getIdContrato(), param.getIdServicio(),
                        param.getIdTarifa());
        
        if(contratoServicioTarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato servicio tarifa"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public ContratoServicioTarifa create(ContratoServicioTarifaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, Boolean.FALSE)) {
            return null;
        }
        
        ContratoServicioTarifa item = new ContratoServicioTarifa(
                param.getIdContrato(),
                param.getIdServicio(),
                param.getIdTarifa());
        item.setActivo(param.getActivo());
        
        create(item);
        
        return item;
    }
    
    @Override
    public ContratoServicioTarifa edit(ContratoServicioTarifaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ContratoServicioTarifa item = find(
                param.getIdContrato(),
                param.getIdServicio(),
                param.getIdTarifa());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }

    @Override
    public List<ContratoServicioTarifa> find(ContratoServicioTarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(ContratoServicioTarifa.class);
        Root<ContratoServicioTarifa> root = cq.from(ContratoServicioTarifa.class);
        Join<ContratoServicioTarifa, ContratoServicio> cs = root.join("contratoServicio");
        
        Path cstPk = root.get("contratoServicioTarifaPK");
        
        cq.select(root);
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getIdContrato() != null) {
            predList.add(qb.equal(cstPk.get("idContrato"), param.getIdContrato()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(cstPk.get("idServicio"), param.getIdServicio()));
        }
        
        if (param.getIdTarifa() != null) {
            predList.add(qb.equal(cstPk.get("idTarifa"), param.getIdTarifa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<ContratoServicioTarifa> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ContratoServicioTarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ContratoServicioTarifa> root = cq.from(ContratoServicioTarifa.class);
        
        Path cstPk = root.get("contratoServicioTarifaPK");
        
        cq.select(qb.count(root));
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getIdContrato() != null) {
            predList.add(qb.equal(cstPk.get("idContrato"), param.getIdContrato()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(cstPk.get("idServicio"), param.getIdServicio()));
        }
        
        if (param.getIdTarifa() != null) {
            predList.add(qb.equal(cstPk.get("idTarifa"), param.getIdTarifa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    /**
     * Renueva un contratoserviciotarifa
     * @param idContratoActual Identificador de contrato actual
     * @param idContratoNuevo Identificador de contrato nuevo
     * @param idServicio Identificador de servicio
     * @param idTarifa Identificador de tarifa
     * @return ContratoServicioTarifa
     */
    @Override
    public ContratoServicioTarifa renovar(Integer idContratoActual, Integer idContratoNuevo, Integer idServicio, Integer idTarifa) {

        ContratoServicioTarifa contratoServicioTarifa = find(new ContratoServicioTarifaPK(idContratoActual, idServicio, idTarifa));
        contratoServicioTarifa.setActivo('N');
        edit(contratoServicioTarifa);

        contratoServicioTarifa.getContratoServicioTarifaPK().setIdContrato(idContratoNuevo);
        contratoServicioTarifa.setActivo('S');
        create(contratoServicioTarifa);
        
        return contratoServicioTarifa;
    }
}
