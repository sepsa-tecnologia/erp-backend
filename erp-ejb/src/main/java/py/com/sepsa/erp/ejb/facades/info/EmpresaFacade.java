/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.filters.EmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EmpresaPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface EmpresaFacade extends Facade<Empresa, EmpresaParam, UserInfoImpl> {

    public Empresa find(Integer id, String codigo, String ruc);

    public List<EmpresaPojo> findCantidadEmpresas(EmpresaParam param);

    public List<EmpresaPojo> findCantidadUsuarios(EmpresaParam param);
    
    public List<UsuarioPojo> findUsuarios(Integer idClienteSepsa, String bandera);
    
    public List<Empresa> findEmpresasFacturasPendientes(EmpresaParam param);
    
    public List<Empresa> findEmpresasNotaCreditoPendientes(EmpresaParam param);
    
    public List<Empresa> findEmpresasNotaDebitoPendientes(EmpresaParam param);
    
    public List<Empresa> findEmpresasAutofacturasPendientes(EmpresaParam param);
    
    public List<Empresa> findEmpresasNotaRemisionPendientes(EmpresaParam param);
    
}
