/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.Transportista;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TransportistaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "TransportistaFacade", mappedName = "TransportistaFacade")
@Local(TransportistaFacade.class)
public class TransportistaFacadeImpl extends FacadeImpl<Transportista, TransportistaParam> implements TransportistaFacade {

    public TransportistaFacadeImpl() {
        super(Transportista.class);
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(TransportistaParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        return !param.tieneErrores();
    }
    
    
    /**
     * Verifica si el objeto es válido para editar
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(TransportistaParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
          Transportista t = find(param.getId());

        if (t == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el transportista"));
        } 

        return !param.tieneErrores();
    }

    /**
     * Crea una instancia de Trasnportista
     *
     * @param param parámetros
     * @param userInfo Usuario
     * @return Instancia
     */
    @Override
    public Transportista create(TransportistaParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        Transportista t = new Transportista();
        t.setIdNaturalezaTransportista(param.getIdNaturalezaTransportista());
        t.setRazonSocial(param.getRazonSocial());
        t.setRuc(param.getRuc());
        t.setVerificadorRuc(param.getVerificadorRuc());
        t.setIdTipoDocumento(param.getIdTipoDocumento());
        t.setNroDocumento(param.getNroDocumento());
        t.setNombreCompleto(param.getNombreCompleto());
        t.setDomicilioFiscal(param.getDomicilioFiscal());
        t.setDireccionChofer(param.getDireccionChofer());
        t.setNroDocumentoChofer(param.getNroDocumentoChofer());
        t.setIdEmpresa(param.getIdEmpresa());
        t.setActivo('S');
        create(t);
        
        return t;
    }
    
    /**
     * Crea una instancia de Transportista
     *
     * @param param parámetros
     * @param userInfo Usuario
     * @return Instancia
     */
    @Override
    public Transportista edit(TransportistaParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Transportista t = new Transportista();
        t.setId(param.getId());
        t.setIdNaturalezaTransportista(param.getIdNaturalezaTransportista());
        t.setRazonSocial(param.getRazonSocial());
        t.setRuc(param.getRuc());
        t.setVerificadorRuc(param.getVerificadorRuc());
        t.setIdTipoDocumento(param.getIdTipoDocumento());
        t.setNroDocumento(param.getNroDocumento());
        t.setNombreCompleto(param.getNombreCompleto());
        t.setDomicilioFiscal(param.getDomicilioFiscal());
        t.setDireccionChofer(param.getDireccionChofer());
        t.setNroDocumentoChofer(param.getNroDocumentoChofer());
        t.setIdEmpresa(param.getIdEmpresa());
        t.setActivo(param.getActivo());
        edit(t);
        
        return t;
    }

    @Override
    public List<Transportista> find(TransportistaParam param) {

        AbstractFind find = new AbstractFind(Transportista.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, TransportistaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Transportista> root = cq.from(Transportista.class);

        find.addPath("root", root);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getRazonSocial()!= null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toLowerCase())));
        }
        
        if (param.getRuc()!= null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like((root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim())));
        }
        
        if (param.getVerificadorRuc()!= null) {
            predList.add(qb.equal(root.get("verificadorRuc"), param.getVerificadorRuc()));
        }
        
        if (param.getNroDocumentoChofer()!= null && !param.getNroDocumentoChofer().trim().isEmpty()) {
            predList.add(qb.like((root.<String>get("nroDocumentoChofer")),
                    String.format("%%%s%%", param.getNroDocumentoChofer().trim())));
        }
        
        if (param.getNombreCompleto()!= null && !param.getNombreCompleto().trim().isEmpty()) {
            predList.add(qb.like((root.<String>get("nombreCompleto")),
                    String.format("%%%s%%", param.getNombreCompleto().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(
                qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(TransportistaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Transportista> root = cq.from(Transportista.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getRazonSocial()!= null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.lower(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toLowerCase())));
        }

        if (param.getRuc()!= null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like((root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim())));
        }
        
        if (param.getVerificadorRuc()!= null) {
            predList.add(qb.equal(root.get("verificadorRuc"), param.getVerificadorRuc()));
        }
        
        if (param.getNroDocumentoChofer()!= null && !param.getNroDocumentoChofer().trim().isEmpty()) {
            predList.add(qb.like((root.<String>get("nroDocumentoChofer")),
                    String.format("%%%s%%", param.getNroDocumentoChofer().trim())));
        }
        
        if (param.getNombreCompleto()!= null && !param.getNombreCompleto().trim().isEmpty()) {
            predList.add(qb.like((root.<String>get("nombreCompleto")),
                    String.format("%%%s%%", param.getNombreCompleto().trim())));
        }
        
        if (param.getIdTipoDocumento()!= null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
