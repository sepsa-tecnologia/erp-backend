/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.AutoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.Cobro;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemision;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.TalonarioLocal;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoDocumento;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.info.Direccion;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioLocal;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TalonarioFacade", mappedName = "TalonarioFacade")
@javax.ejb.Local(TalonarioFacade.class)
public class TalonarioFacadeImpl extends FacadeImpl<Talonario, TalonarioParam> implements TalonarioFacade {

    public TalonarioFacadeImpl() {
        super(Talonario.class);
    }
    
    public Boolean validToCountPages(TalonarioParam param) {
        
        if(!param.isValidToCountPages()) {
            return Boolean.FALSE;
        }
        
        Talonario talonario = find(param.getId());
        
        if(isNull(talonario)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(TalonarioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        if(param.getIdLocal() != null) {
            Local local = facades.getLocalFacade().find(param.getIdLocal());
            
            if(local == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el local"));
            }
        }
        
        TipoDocumento tipoDocumento = facades
                .getTipoDocumentoFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento());
        
        if(tipoDocumento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de documento"));
        } else {
            param.setIdTipoDocumento(tipoDocumento.getId());
            param.setCodigoTipoDocumento(tipoDocumento.getCodigo());
        }
        
        if(param.getIdEncargado() != null) {
            
            Persona persona = facades.getPersonaFacade().find(param.getIdEncargado());
            
            if(persona == null) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona encargada"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(TalonarioParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Talonario talonario = find(param.getId());
        
        if(isNull(talonario)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }
        
        if(param.getIdLocal() != null) {
            Local local = facades.getLocalFacade().find(param.getIdLocal());
            
            if(local == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el local"));
            }
        }
        
        TipoDocumento tipoDocumento = facades
                .getTipoDocumentoFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento());
        
        if(tipoDocumento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de documento"));
        } else {
            param.setIdTipoDocumento(tipoDocumento.getId());
            param.setCodigoTipoDocumento(tipoDocumento.getCodigo());
        }
        
        if(param.getIdEncargado() != null) {
            
            Persona persona = facades.getPersonaFacade().find(param.getIdEncargado());
            
            if(persona == null) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona encargada"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica que la fecha del documento este en el rango de vigencia
     * @param talonario Talonario
     * @param fechaDocumento Fecha del documento
     * @return bandera
     */
    @Override
    public Boolean fechaDocumentoEnRango(TalonarioPojo talonario, Date fechaDocumento) {
        return fechaDocumentoEnRango(talonario.getFechaInicio(), talonario.getFechaVencimiento(), fechaDocumento);
    }
    
    /**
     * Verifica que la fecha del documento este en el rango de vigencia
     * @param fechaInicio Fecha de inicio de vigencia de timbrado
     * @param fechaVencimiento Fecha de fin de vigencia de timbrado
     * @param fechaDocumento Fecha del documento
     * @return bandera
     */
    @Override
    public Boolean fechaDocumentoEnRango(Date fechaInicio, Date fechaVencimiento, Date fechaDocumento) {
            
        Boolean valid = Boolean.TRUE;
        
        if(fechaVencimiento != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaVencimiento);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);

            if(cal.getTime().compareTo(fechaDocumento) < 0) {
                valid = Boolean.FALSE;
            }
        }
        
        if(fechaInicio != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaInicio);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);

            if(cal.getTime().compareTo(fechaDocumento) > 0) {
                valid = Boolean.FALSE;
            }
        }
        
        return valid;
    }
    
    /**
     * Verifica si un número de documento está en rango
     * @param talonario talonario
     * @param nroDoc número de documento
     * @return bandera
     */
    @Override
    public Boolean nroDocEnRango(TalonarioPojo talonario, String nroDoc) {
        
        Boolean valid = Boolean.FALSE;
        
        StringTokenizer token = new StringTokenizer(nroDoc, "-");
        if (token.countTokens() == 3) {
            token.nextToken();
            token.nextToken();
            int nro = Integer.parseInt(token.nextToken());
            if (nro <= talonario.getFin()) {
                valid = Boolean.TRUE;
            }
        }
        
        return valid;
    }
    
    @Override
    public Talonario create(TalonarioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Talonario talonario = new Talonario();
        talonario.setIdEmpresa(userInfo.getIdEmpresa());
        talonario.setDigital(param.getDigital());
        talonario.setActivo(param.getActivo());
        talonario.setFechaInicio(param.getFechaInicio());
        talonario.setFechaResolucion(param.getFechaResolucion());
        talonario.setFechaVencimiento(param.getFechaVencimiento());
        talonario.setFin(param.getFin());
        talonario.setIdLocal(param.getIdLocal());
        talonario.setIdEncargado(param.getIdEncargado());
        talonario.setIdTipoDocumento(param.getIdTipoDocumento());
        talonario.setInicio(param.getInicio());
        talonario.setNroPuntoVenta(param.getNroPuntoVenta());
        talonario.setNroResolucion(param.getNroResolucion());
        talonario.setNroSucursal(param.getNroSucursal());
        talonario.setSerie(param.getSerie());
        talonario.setTimbrado(param.getTimbrado());
        talonario.setCodigoInterno(param.getCodigoInterno());
        
        create(talonario);
        
        return talonario;
    }
    
    @Override
    public Talonario edit(TalonarioParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Talonario talonario = find(param.getId());
        talonario.setDigital(param.getDigital());
        talonario.setActivo(param.getActivo());
        talonario.setFechaInicio(param.getFechaInicio());
        talonario.setFechaResolucion(param.getFechaResolucion());
        talonario.setFechaVencimiento(param.getFechaVencimiento());
        talonario.setFin(param.getFin());
        talonario.setIdLocal(param.getIdLocal());
        talonario.setIdEncargado(param.getIdEncargado());
        talonario.setIdTipoDocumento(param.getIdTipoDocumento());
        talonario.setInicio(param.getInicio());
        talonario.setNroPuntoVenta(param.getNroPuntoVenta());
        talonario.setNroResolucion(param.getNroResolucion());
        talonario.setNroSucursal(param.getNroSucursal());
        talonario.setSerie(param.getSerie());
        talonario.setTimbrado(param.getTimbrado());
        talonario.setCodigoInterno(param.getCodigoInterno());
        
        edit(talonario);
        
        return talonario;
    }
    
    @Override
    public List<TalonarioPojo> findPojo(TalonarioParam param) {

        AbstractFind find = new AbstractFind(TalonarioPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("timbrado"),
                        getPath("root").get("inicio"),
                        getPath("root").get("fin"),
                        getPath("root").get("fechaVencimiento"),
                        getPath("root").get("activo"),
                        getPath("root").get("nroSucursal"),
                        getPath("root").get("nroPuntoVenta"),
                        getPath("root").get("fechaInicio"),
                        getPath("root").get("nroResolucion"),
                        getPath("root").get("fechaResolucion"),
                        getPath("root").get("serie"),
                        getPath("root").get("digital"),
                        getPath("root").get("idTipoDocumento"),
                        getPath("tipoDocumento").get("descripcion"),
                        getPath("direccion").get("direccion"),
                        getPath("local").get("descripcion"),
                        getPath("local").get("idExterno")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<Talonario> find(TalonarioParam param) {

        AbstractFind find = new AbstractFind(Talonario.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, TalonarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Talonario> root = cq.from(Talonario.class);
        Join<Talonario, Local> local = root.join("local", JoinType.LEFT);
        Join<Local, Direccion> direccion = local.join("direccion", JoinType.LEFT);
        Join<Talonario, TalonarioLocal> talonarioLocales = root.join("talonarioLocales", JoinType.LEFT);
        Join<TalonarioLocal, Local> locales = talonarioLocales.join("local", JoinType.LEFT);
        Join<Local, UsuarioLocal> usuarioLocales = locales.join("usuarioLocales", JoinType.LEFT);
        Join<Talonario, TipoDocumento> tipoDocumento = root.join("tipoDocumento", JoinType.LEFT);
        Join<Talonario, Empresa> empresa = root.join("empresa", JoinType.LEFT);
        
        talonarioLocales.on(qb.equal(talonarioLocales.get("activo"), 'S'));
        usuarioLocales.on(qb.equal(usuarioLocales.get("activo"), 'S'));
        
        find.addPath("root", root);
        find.addPath("tipoDocumento", tipoDocumento);
        find.addPath("empresa", empresa);
        find.addPath("local", local);
        find.addPath("direccion", direccion);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getIdTipoDocumento() != null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        if (param.getCodigoTipoDocumento() != null) {
            predList.add(qb.equal(tipoDocumento.get("codigo"), param.getCodigoTipoDocumento()));
        }
        
        if (param.getIdEncargado() != null) {
            predList.add(qb.equal(root.get("idEncargado"), param.getIdEncargado()));
        }
        
        if (param.getIdEncargadoONulo() != null) {
            predList.add(qb.or(root.get("idEncargado").isNull(), qb.equal(root.get("idEncargado"), param.getIdEncargadoONulo())));
        }
        
        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(usuarioLocales.get("idUsuario"), param.getIdUsuario()));
        }
        
        if (param.getIdLocal() != null) {
            predList.add(qb.or(qb.equal(local.get("id"), param.getIdLocal()),
                    qb.equal(locales.get("id"), param.getIdLocal())));
        }
        
        if (param.getIdLocales() != null && !param.getIdLocales().isEmpty()) {
            predList.add(qb.or(local.get("id").in(param.getIdLocal()),
                    locales.get("id").in(param.getIdLocal())));
        }
        
        if (param.getCodigoInterno()!= null) {
            predList.add(qb.equal(root.get("codigoInterno"), param.getCodigoInterno()));
        }
        
        if (param.getGlnLocal() != null) {
            predList.add(qb.or(qb.equal(local.get("gln"), param.getGlnLocal()),
                    qb.equal(locales.get("gln"), param.getGlnLocal())));
        }
        
        if (param.getIdExternoLocal() != null && !param.getIdExternoLocal().trim().isEmpty()) {
            predList.add(qb.or(qb.equal(local.get("idExterno"), param.getIdExternoLocal().trim()),
                    qb.equal(locales.get("idExterno"), param.getIdExternoLocal().trim())));
        }
        
        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.<Date>greaterThanOrEqualTo(root.get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }
        
        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.<Date>lessThanOrEqualTo(root.get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }
        
        if (param.getNroSucursal() != null && !param.getNroSucursal().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroSucursal"), param.getNroSucursal().trim()));
        }
        
        if (param.getNroPuntoVenta() != null && !param.getNroPuntoVenta().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroPuntoVenta"), param.getNroPuntoVenta().trim()));
        }
        
        if (param.getTimbrado() != null && !param.getTimbrado().trim().isEmpty()) {
            predList.add(qb.equal(root.get("timbrado"), param.getTimbrado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("activo")));
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(TalonarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Talonario> root = cq.from(Talonario.class);
        Join<Talonario, Local> local = root.join("local", JoinType.LEFT);
        Join<Talonario, TalonarioLocal> talonarioLocales = root.join("talonarioLocales", JoinType.LEFT);
        Join<TalonarioLocal, Local> locales = talonarioLocales.join("local", JoinType.LEFT);
        Join<Local, UsuarioLocal> usuarioLocales = locales.join("usuarioLocales", JoinType.LEFT);
        Join<Talonario, TipoDocumento> tipoDocumento = root.join("tipoDocumento", JoinType.LEFT);
        
        talonarioLocales.on(qb.equal(talonarioLocales.get("activo"), 'S'));
        usuarioLocales.on(qb.equal(usuarioLocales.get("activo"), 'S'));
        
        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getIdTipoDocumento() != null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        if (param.getCodigoTipoDocumento() != null) {
            predList.add(qb.equal(tipoDocumento.get("codigo"), param.getCodigoTipoDocumento()));
        }
        
        if (param.getIdEncargado() != null) {
            predList.add(qb.equal(root.get("idEncargado"), param.getIdEncargado()));
        }
        
        if (param.getIdEncargadoONulo() != null) {
            predList.add(qb.or(root.get("idEncargado").isNull(), qb.equal(root.get("idEncargado"), param.getIdEncargadoONulo())));
        }
        
        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(usuarioLocales.get("idUsuario"), param.getIdUsuario()));
        }
        
        if (param.getIdLocal() != null) {
            predList.add(qb.or(qb.equal(local.get("id"), param.getIdLocal()),
                    qb.equal(locales.get("id"), param.getIdLocal())));
        }
        
        if (param.getIdLocales() != null && !param.getIdLocales().isEmpty()) {
            predList.add(qb.or(local.get("id").in(param.getIdLocal()),
                    locales.get("id").in(param.getIdLocal())));
        }
        
        if (param.getCodigoInterno()!= null) {
            predList.add(qb.equal(root.get("codigoInterno"), param.getCodigoInterno()));
        }
        
        if (param.getGlnLocal() != null) {
            predList.add(qb.or(qb.equal(local.get("gln"), param.getGlnLocal()),
                    qb.equal(locales.get("gln"), param.getGlnLocal())));
        }
        
        if (param.getIdExternoLocal() != null && !param.getIdExternoLocal().trim().isEmpty()) {
            predList.add(qb.or(qb.equal(local.get("idExterno"), param.getIdExternoLocal().trim()),
                    qb.equal(locales.get("idExterno"), param.getIdExternoLocal().trim())));
        }
        
        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.<Date>greaterThanOrEqualTo(root.get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }
        
        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.<Date>lessThanOrEqualTo(root.get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }
        
        if (param.getNroSucursal() != null && !param.getNroSucursal().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroSucursal"), param.getNroSucursal().trim()));
        }
        
        if (param.getNroPuntoVenta() != null && !param.getNroPuntoVenta().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroPuntoVenta"), param.getNroPuntoVenta().trim()));
        }
        
        if (param.getTimbrado() != null && !param.getTimbrado().trim().isEmpty()) {
            predList.add(qb.equal(root.get("timbrado"), param.getTimbrado().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    /**
     * Inactiva la lista de talonarios
     * @param idEmpresa Identificador de empresa
     * @param idTipoDocumento Identificador de tipo de documento
     * @param idEncargado Identificador de encargado
     * @param noIdTalonario Identificador de talonario a descargar
     */
    @Override
    public void inactivarTalonarios(Integer idEmpresa, Integer idTipoDocumento,
            Integer idEncargado, Integer noIdTalonario) {

        if(idTipoDocumento == null
                || idEncargado == null
                || noIdTalonario == null) {
            return;
        }
        
        String sql = String.format("UPDATE facturacion.talonario t\n"
                + "SET activo='N'\n"
                + "WHERE t.id_tipo_documento = %d "
                + "and t.id_encargado = %d "
                + "and t.id != %d "
                + "and t.id_empresa = %d",
                idTipoDocumento, idEncargado, noIdTalonario, idEmpresa);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        q.executeUpdate();
    }
    
    /**
     * Obtiene la fecha del ultimo documento según su tipo
     *
     * @param talonario Talonario
     * @param idTipoDocumento Identificador del tipo de documento del talonario
     * @return Fecha de la ultima factura, o Fecha de la ultima Nota de Credito
     */
    @Override
    public Date ultimaFechaDoc(TalonarioPojo talonario, Integer idTipoDocumento) {
        Date lastDate = null;
        if (talonario != null && idTipoDocumento != null) {
            if (idTipoDocumento == 1) {
                CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
                CriteriaQuery cq = qb.createQuery();
                Root<Factura> root = cq.from(Factura.class);
                cq.select(root.get("fecha"));
                Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
                pred = qb.and(pred, qb.equal(root.get("anulado"), 'N'));
                cq.where(pred);
                cq.orderBy((qb.desc(root.get("fecha"))), qb.desc(root.get("nroFactura")));
                Query q = getEntityManager()
                        .createQuery(cq)
                        .setMaxResults(1);
                List<Date> list = q.getResultList();
                if (list.isEmpty()) {
                    Calendar today = Calendar.getInstance();
                    today.setTime(talonario.getFechaInicio());
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MILLISECOND, 0);
                    lastDate = today.getTime();
                } else {
                    lastDate = list.get(0);
                }
            } else if (idTipoDocumento == 3) {
                CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
                CriteriaQuery cq = qb.createQuery();
                Root<NotaCredito> root = cq.from(NotaCredito.class);
                cq.select(root.get("fecha"));
                Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
                pred = qb.and(pred, qb.equal(root.get("anulado"), 'N'));
                cq.where(pred);
                cq.orderBy((qb.desc(root.get("fecha"))), qb.desc(root.get("nroNotaCredito")));
                Query q = getEntityManager()
                        .createQuery(cq)
                        .setMaxResults(1);
                List<Date> list = q.getResultList();
                if (list.isEmpty()) {
                    Calendar today = Calendar.getInstance();
                    today.setTime(talonario.getFechaInicio());
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MILLISECOND, 0);
                    lastDate = today.getTime();
                } else {
                    lastDate = list.get(0);
                }
            } else if (idTipoDocumento == 2){
                CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
                CriteriaQuery cq = qb.createQuery();
                Root<Cobro> root = cq.from(Cobro.class);
                cq.select(root.get("fecha"));
                Predicate pred = qb.equal(root.get("idTalonario"),talonario.getId());
               // pred = qb.and(pred,qb.equal(root.get("anulado"), 'N'));
                cq.where(pred);
                cq.orderBy((qb.desc(root.get("fecha"))), qb.desc(root.get("nroRecibo")));
                Query q = getEntityManager()
                        .createQuery(cq)
                        .setMaxResults(1);
                List<Date> list = q.getResultList();
                if (list.isEmpty()){
                    Calendar today = Calendar.getInstance();
                    today.setTime(talonario.getFechaInicio());
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MILLISECOND, 0);
                    lastDate = today.getTime();
                } else {
                    lastDate = list.get(0);
                }
            } else if (idTipoDocumento == 4){
                CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
                CriteriaQuery cq = qb.createQuery();
                Root<NotaRemision> root = cq.from(NotaRemision.class);
                cq.select(root.get("fecha"));
                Predicate pred = qb.equal(root.get("idTalonario"),talonario.getId());
               // pred = qb.and(pred,qb.equal(root.get("anulado"), 'N'));
                cq.where(pred);
                cq.orderBy((qb.desc(root.get("fecha"))), qb.desc(root.get("nroNotaRemision")));
                Query q = getEntityManager()
                        .createQuery(cq)
                        .setMaxResults(1);
                List<Date> list = q.getResultList();
                if (list.isEmpty()){
                    Calendar today = Calendar.getInstance();
                    today.setTime(talonario.getFechaInicio());
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MILLISECOND, 0);
                    lastDate = today.getTime();
                } else {
                    lastDate = list.get(0);
                }
            } else if (idTipoDocumento == 5){
                CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
                CriteriaQuery cq = qb.createQuery();
                Root<AutoFactura> root = cq.from(AutoFactura.class);
                cq.select(root.get("fecha"));
                Predicate pred = qb.equal(root.get("idTalonario"),talonario.getId());
               //pred = qb.and(pred,qb.equal(root.get("anulado"), 'N'));
                cq.where(pred);
                cq.orderBy((qb.desc(root.get("fecha"))), qb.desc(root.get("nroAutofactura")));
                Query q = getEntityManager()
                        .createQuery(cq)
                        .setMaxResults(1);
                List<Date> list = q.getResultList();
                if (list.isEmpty()){
                    Calendar today = Calendar.getInstance();
                    today.setTime(talonario.getFechaInicio());
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MILLISECOND, 0);
                    lastDate = today.getTime();
                } else {
                    lastDate = list.get(0);
                }
            }
        }
        return lastDate;
    }
    
    /**
     * Obtiene el siguiente número para un recibo según su talonario
     *
     * @param talonario Talonario
     * @return Siguiente número de recibo
     */
    @Override
    public String sigNumRecibo(TalonarioPojo talonario) {
        String receiptNro = "";
        if (talonario != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Cobro> root = cq.from(Cobro.class);
            cq.select(root.get("nroRecibo"));
            cq.where(qb.equal(root.get("idTalonario"), talonario.getId()));
            cq.orderBy(qb.desc(qb.function("split_part", String.class, root.get("nroRecibo"), qb.literal("-"), qb.literal(3))));
            Query q = getEntityManager().createQuery(cq);
            List<String> list = q.getResultList();

            if (list.isEmpty()) {
                int nextNum = talonario.getInicio();
                String nextNumS = "" + nextNum;
                int l = nextNumS.length();
                for (int i = 0; i < 7 - l; i++) {
                    nextNumS = "0" + nextNumS;
                }

                receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
            } else {
                StringTokenizer token = new StringTokenizer(list.get(0), "-");
                if (token.countTokens() == 3) {
                    token.nextToken();
                    token.nextToken();
                    int nextNum = Integer.parseInt(token.nextToken()) + 1;
                    String nextNumS = "" + nextNum;
                    int l = nextNumS.length();
                    for (int i = 0; i < 7 - l; i++) {
                        nextNumS = "0" + nextNumS;
                    }

                    receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
                }
            }
        }
        return receiptNro;
    }
    
    @Override
    public Integer cantidadHojas(TalonarioParam param) {
        
        if(!validToCountPages(param)) {
            return null;
        }
        
        Talonario talonario = find(param.getId());
        
        Integer cantidadHojas = null;
        
        switch(talonario.getTipoDocumento().getCodigo()) {
            
            case "FACTURA":
                cantidadHojas = cantidadHojasFactura(param);
                break;
                
            case "NOTA_CREDITO":
                cantidadHojas = cantidadHojasNotaCredito(param);
                break;
                
            case "RECIBO":
                cantidadHojas = cantidadHojasRecibo(param);
                break;
                
            default:
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe soporte para el conteo de la cantidad de hojas del tipo de documento"));
        }
        
        return cantidadHojas;
    }
    
    private Integer cantidadHojas(Talonario talonario, String nroDocumento) {
        
        if(isNullOrEmpty(nroDocumento)) {
            return talonario.getFin() - talonario.getInicio();
        }
        
        StringTokenizer token = new StringTokenizer(nroDocumento, "-");
        if (token.countTokens() == 3) {
            token.nextToken();
            token.nextToken();
            int nroActual = Integer.parseInt(token.nextToken());
            return talonario.getFin() - nroActual;
        }
        
        return 0;
    }
    
    private Integer cantidadHojasFactura(TalonarioParam param) {
        
        Talonario talonario = find(param.getId());
        
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Factura> root = cq.from(Factura.class);
        cq.select(root.get("nroFactura"));

        Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
        cq.where(pred);
        cq.orderBy(qb.desc(root.get("nroFactura")));
        Query q = getEntityManager()
                .createQuery(cq)
                .setMaxResults(1)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<String> list = q.getResultList();
        
        return cantidadHojas(talonario, Units.execute(() -> list.get(0)));
    }
    
    private Integer cantidadHojasNotaCredito(TalonarioParam param) {
        
        Talonario talonario = find(param.getId());
        
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaCredito> root = cq.from(NotaCredito.class);
        cq.select(root.get("nroNotaCredito"));

        Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
        cq.where(pred);
        cq.orderBy(qb.desc(root.get("nroNotaCredito")));
        Query q = getEntityManager()
                .createQuery(cq)
                .setMaxResults(1)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<String> list = q.getResultList();
        
        return cantidadHojas(talonario, Units.execute(() -> list.get(0)));
    }
    
    private Integer cantidadHojasRecibo(TalonarioParam param) {
        
        Talonario talonario = find(param.getId());
        
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Cobro> root = cq.from(Cobro.class);
        cq.select(root.get("nroRecibo"));

        Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
        cq.where(pred);
        cq.orderBy(qb.desc(root.get("nroRecibo")));
        Query q = getEntityManager()
                .createQuery(cq)
                .setMaxResults(1)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<String> list = q.getResultList();
        
        return cantidadHojas(talonario, Units.execute(() -> list.get(0)));
    }
    
    /**
     * Obtiene el siguiente número para una factura según su talonario
     *
     * @param talonario Talonario
     * @return Siguiente número de una factura
     */
    @Override
    public String sigNumFactura(TalonarioPojo talonario) {
        String factNro = "";
        if (talonario != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Factura> root = cq.from(Factura.class);
            cq.select(root.get("nroFactura"));
            
            Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
            cq.where(pred);
            cq.orderBy(qb.desc(qb.function("split_part", String.class, root.get("nroFactura"), qb.literal("-"), qb.literal(3))));
            Query q = getEntityManager()
                    .createQuery(cq)
                    .setMaxResults(1)
                    .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            List<String> list = q.getResultList();
            if (list.isEmpty()) {
                int nextNum;
                nextNum = talonario.getInicio();
                String nextNumS = "" + nextNum;
                int l = nextNumS.length();
                for (int i = 0; i < 7 - l; i++) {
                    nextNumS = "0" + nextNumS;
                }
                factNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
            } else {
                StringTokenizer token = new StringTokenizer(list.get(0), "-");
                if (token.countTokens() == 3) {
                    token.nextToken();
                    token.nextToken();
                    int nextNum = Integer.parseInt(token.nextToken()) + 1;
                    String nextNumS = "" + nextNum;
                    int l = nextNumS.length();
                    for (int i = 0; i < 7 - l; i++) {
                        nextNumS = "0" + nextNumS;
                    }
                    factNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
                }
            }
        }
        return factNro;
    }
    
    @Override
    public String sigNumAutoFactura(TalonarioPojo talonario) {
        String factNro = "";
        if (talonario != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<AutoFactura> root = cq.from(AutoFactura.class);
            cq.select(root.get("nroAutofactura"));
            
            Predicate pred = qb.equal(root.get("idTalonario"), talonario.getId());
            cq.where(pred);
            cq.orderBy(qb.desc(qb.function("split_part", String.class, root.get("nroAutofactura"), qb.literal("-"), qb.literal(3))));
            Query q = getEntityManager()
                    .createQuery(cq)
                    .setMaxResults(1)
                    .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            List<String> list = q.getResultList();
            if (list.isEmpty()) {
                int nextNum;
                nextNum = talonario.getInicio();
                String nextNumS = "" + nextNum;
                int l = nextNumS.length();
                for (int i = 0; i < 7 - l; i++) {
                    nextNumS = "0" + nextNumS;
                }
                factNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
            } else {
                StringTokenizer token = new StringTokenizer(list.get(0), "-");
                if (token.countTokens() == 3) {
                    token.nextToken();
                    token.nextToken();
                    int nextNum = Integer.parseInt(token.nextToken()) + 1;
                    String nextNumS = "" + nextNum;
                    int l = nextNumS.length();
                    for (int i = 0; i < 7 - l; i++) {
                        nextNumS = "0" + nextNumS;
                    }
                    factNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
                }
            }
        }
        return factNro;
    }

    @Override
    public String sigNumNotaRemision(TalonarioPojo talonario) {
        String receiptNro = "";
        if (talonario != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<NotaRemision> root = cq.from(NotaRemision.class);
            cq.select(root.get("nroNotaRemision"));
            cq.where(qb.equal(root.get("idTalonario"), talonario.getId()));
            cq.orderBy(qb.desc(qb.function("split_part", String.class, root.get("nroNotaRemision"), qb.literal("-"), qb.literal(3))));
            Query q = getEntityManager().createQuery(cq);
            List<String> list = q.getResultList();
            if (list.isEmpty()) {
                int nextNum;
                nextNum = talonario.getInicio();
                String nextNumS = "" + nextNum;
                int l = nextNumS.length();
                for (int i = 0; i < 7 - l; i++) {
                    nextNumS = "0" + nextNumS;
                }
                receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
            } else {
                StringTokenizer token = new StringTokenizer(list.get(0), "-");
                if (token.countTokens() == 3) {
                    token.nextToken();
                    token.nextToken();
                    int nextNum = Integer.parseInt(token.nextToken()) + 1;
                    String nextNumS = "" + nextNum;
                    int l = nextNumS.length();
                    for (int i = 0; i < 7 - l; i++) {
                        nextNumS = "0" + nextNumS;
                    }
                    receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
                }
            }
        }
            
        return receiptNro;
    }
    
    
    /**
     * Obtiene el siguiente número para una nota de crédito según su talonario
     *
     * @param talonario Talonario
     * @return Siguiente número de una nota de crédito
     */
    @Override
    public String sigNumNotaCredito(TalonarioPojo talonario) {
        String receiptNro = "";
        if (talonario != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<NotaCredito> root = cq.from(NotaCredito.class);
            cq.select(root.get("nroNotaCredito"));
            cq.where(qb.equal(root.get("idTalonario"), talonario.getId()));
            cq.orderBy(qb.desc(qb.function("split_part", String.class, root.get("nroNotaCredito"), qb.literal("-"), qb.literal(3))));
            Query q = getEntityManager().createQuery(cq);
            List<String> list = q.getResultList();
            if (list.isEmpty()) {
                int nextNum;
                nextNum = talonario.getInicio();
                String nextNumS = "" + nextNum;
                int l = nextNumS.length();
                for (int i = 0; i < 7 - l; i++) {
                    nextNumS = "0" + nextNumS;
                }
                receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
            } else {
                StringTokenizer token = new StringTokenizer(list.get(0), "-");
                if (token.countTokens() == 3) {
                    token.nextToken();
                    token.nextToken();
                    int nextNum = Integer.parseInt(token.nextToken()) + 1;
                    String nextNumS = "" + nextNum;
                    int l = nextNumS.length();
                    for (int i = 0; i < 7 - l; i++) {
                        nextNumS = "0" + nextNumS;
                    }
                    receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
                }
            }
        }
        return receiptNro;
    }
    
     /**
     * Obtiene el siguiente número para una nota de débito según su talonario
     *
     * @param talonario Talonario
     * @return Siguiente número de una nota de débito
     */
    @Override
    public String sigNumNotaDebito(TalonarioPojo talonario) {
        String receiptNro = "";
        if (talonario != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<NotaDebito> root = cq.from(NotaDebito.class);
            cq.select(root.get("nroNotaDebito"));
            cq.where(qb.equal(root.get("idTalonario"), talonario.getId()));
            cq.orderBy(qb.desc(qb.function("split_part", String.class, root.get("nroNotaDebito"), qb.literal("-"), qb.literal(3))));
            Query q = getEntityManager().createQuery(cq);
            List<String> list = q.getResultList();
            if (list.isEmpty()) {
                int nextNum;
                nextNum = talonario.getInicio();
                String nextNumS = "" + nextNum;
                int l = nextNumS.length();
                for (int i = 0; i < 7 - l; i++) {
                    nextNumS = "0" + nextNumS;
                }
                receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
            } else {
                StringTokenizer token = new StringTokenizer(list.get(0), "-");
                if (token.countTokens() == 3) {
                    token.nextToken();
                    token.nextToken();
                    int nextNum = Integer.parseInt(token.nextToken()) + 1;
                    String nextNumS = "" + nextNum;
                    int l = nextNumS.length();
                    for (int i = 0; i < 7 - l; i++) {
                        nextNumS = "0" + nextNumS;
                    }
                    receiptNro = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-" + nextNumS;
                }
            }
        }
        return receiptNro;
    }
    
    
    @Override
    public TalonarioPojo getTalonario(TalonarioParam param) {
        List<TalonarioPojo> list = getTalonarios(param);
        return list == null || list.isEmpty() ? null : list.get(0);
    }
    
    @Override
    public List<TalonarioPojo> getTalonarios(TalonarioParam param) {
        
        List<TalonarioPojo> result = new ArrayList<>();

        if (param.getIdEmpresa() != null
                && param.getIdTipoDocumento() != null
                && param.getDigital() != null) {

            TalonarioParam param1 = new TalonarioParam();
            param1.setActivo('S');
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setDigital(param.getDigital());
            param1.setIdTipoDocumento(param.getIdTipoDocumento());
            param1.setId(param.getId());
            param1.setIdEncargadoONulo(param.getIdEncargado());
            param1.setIdUsuario(param.getIdUsuario());
            param1.setIdExternoLocal(param.getIdExternoLocal());
            param1.setIdLocal(param.getIdLocal());
            param1.setIdLocales(param.getIdLocales());
            
            if(param.getDigital().equals('N') && param.getFecha() != null) {
                if(param.getIdTipoDocumento() != 2) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(param.getFecha());
                    cal.set(Calendar.HOUR_OF_DAY, 23);
                    cal.set(Calendar.MINUTE, 59);
                    cal.set(Calendar.SECOND, 59);
                    cal.set(Calendar.MILLISECOND, 59);
                    param1.setFechaVencimientoDesde(cal.getTime());
                } 
            }
            param1.isValidToList();
            result = findPojo(param1);
        }
        
        return result;
    }
}
