/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.TipoMotivo;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoMotivoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoMotivoFacade", mappedName = "TipoMotivoFacade")
@Local(TipoMotivoFacade.class)
public class TipoMotivoFacadeImpl extends FacadeImpl<TipoMotivo, TipoMotivoParam> implements TipoMotivoFacade {

    public TipoMotivoFacadeImpl() {
        super(TipoMotivo.class);
    }

    public Boolean validToCreate(TipoMotivoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoMotivoParam param1 = new TipoMotivoParam();
        param1.setCodigo(param.getCodigo().trim());
        
        TipoMotivo tipoMotivo = findFirst(param1);
        
        if(tipoMotivo != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe otro registro con el mismo código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(TipoMotivoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoMotivo tipoMotivo = find(param.getId());
        
        if(tipoMotivo == null) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el tipo motivo"));
            
        } else if (!tipoMotivo.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoMotivoParam param1 = new TipoMotivoParam();
            param1.setCodigo(param.getCodigo().trim());

            TipoMotivo tm = findFirst(param1);

            if(tm != null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe otro registro con el mismo código"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public TipoMotivo create(TipoMotivoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoMotivo result = new TipoMotivo();
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        
        create(result);
        
        return result;
    }

    @Override
    public TipoMotivo edit(TipoMotivoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoMotivo result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public TipoMotivo find(Integer id, String codigoEstado) {
        TipoMotivoParam param = new TipoMotivoParam();
        param.setId(id);
        param.setCodigo(codigoEstado);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoMotivo> find(TipoMotivoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoMotivo.class);
        Root<TipoMotivo> root = cq.from(TipoMotivo.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<TipoMotivo> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoMotivoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoMotivo> root = cq.from(TipoMotivo.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
