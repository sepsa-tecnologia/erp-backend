/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.PersonaEmailNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailNotificacionParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.PersonaEmailNotificacionPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface PersonaEmailNotificacionFacade extends Facade<PersonaEmailNotificacion, PersonaEmailNotificacionParam, UserInfoImpl> {

    public List<PersonaEmailNotificacionPojo> asociado(Integer idContacto, Integer idEmail);
    
}
