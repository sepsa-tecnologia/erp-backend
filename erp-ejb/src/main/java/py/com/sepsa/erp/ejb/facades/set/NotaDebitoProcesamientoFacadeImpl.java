/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.set.NotaDebitoProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.NotaDebitoProcesamientoParam;
import py.com.sepsa.erp.ejb.entities.set.filters.ProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NotaDebitoProcesamientoFacade", mappedName = "NotaDebitoProcesamientoFacade")
@Local(NotaDebitoProcesamientoFacade.class)
public class NotaDebitoProcesamientoFacadeImpl extends FacadeImpl<NotaDebitoProcesamiento, NotaDebitoProcesamientoParam> implements NotaDebitoProcesamientoFacade {

    public NotaDebitoProcesamientoFacadeImpl() {
        super(NotaDebitoProcesamiento.class);
    }

    public Boolean validToCreate(NotaDebitoProcesamientoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        NotaDebitoParam param1 = new NotaDebitoParam();
        param1.setId(param.getIdNotaDebito());
        
        Long size = facades.getNotaDebitoFacade().findSize(param1);
        
        if(size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de débito"));
        }
        
        if(!isNull(param.getIdProcesamiento())) {
            ProcesamientoParam param2 = new ProcesamientoParam();
            param2.setId(param.getIdProcesamiento());

            size = facades.getProcesamientoFacade().findSize(param2);

            if(size <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el procesamiento"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "DTE_NOTA_DEBITO");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(!isNull(param.getProcesamiento())) {
            facades.getProcesamientoFacade().validToCreate(param.getProcesamiento());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(NotaDebitoProcesamientoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        NotaDebitoProcesamiento dp = find(param.getId());
        
        if(isNull(dp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de nota de débito procesamiento"));
        }
        
        NotaDebitoParam param1 = new NotaDebitoParam();
        param1.setId(param.getIdNotaDebito());
        
        Long size = facades.getNotaDebitoFacade().findSize(param1);
        
        if(size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de débito"));
        }
        
        if(!isNull(param.getIdProcesamiento())) {
            
            ProcesamientoParam param2 = new ProcesamientoParam();
            param2.setId(param.getIdProcesamiento());

            size = facades.getProcesamientoFacade().findSize(param2);

            if(size <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el procesamiento"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "DTE_NOTA_DEBITO");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public NotaDebitoProcesamiento create(NotaDebitoProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Integer idProcesamiento = null;
        
        if(!isNull(param.getProcesamiento())) {
            Procesamiento procesamiento = facades.getProcesamientoFacade()
                    .create(param.getProcesamiento(), userInfo);
            idProcesamiento = procesamiento.getId();
        } else {
            idProcesamiento = param.getIdProcesamiento();
        }
        
        NotaDebitoProcesamiento item = new NotaDebitoProcesamiento();
        item.setFechaInsercion(Calendar.getInstance().getTime());
        item.setIdEstado(param.getIdEstado());
        item.setIdNotaDebito(param.getIdNotaDebito());
        item.setIdProcesamiento(idProcesamiento);
        
        create(item);
        
        return item;
    }

    @Override
    public NotaDebitoProcesamiento edit(NotaDebitoProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        NotaDebitoProcesamiento item = find(param.getId());
        item.setIdEstado(param.getIdEstado());
        item.setIdNotaDebito(param.getIdNotaDebito());
        item.setIdProcesamiento(param.getIdProcesamiento());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<NotaDebitoProcesamiento> find(NotaDebitoProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(NotaDebitoProcesamiento.class);
        Root<NotaDebitoProcesamiento> root = cq.from(NotaDebitoProcesamiento.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if(param.getIdNotaDebito() != null) {
            predList.add(qb.equal(root.get("idNotaDebito"), param.getIdNotaDebito()));
        }
        
        if(param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<NotaDebitoProcesamiento> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(NotaDebitoProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<NotaDebitoProcesamiento> root = cq.from(NotaDebitoProcesamiento.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if(param.getIdNotaDebito() != null) {
            predList.add(qb.equal(root.get("idNotaDebito"), param.getIdNotaDebito()));
        }
        
        if(param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
