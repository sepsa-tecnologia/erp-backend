/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de configuracion aprobacion
 * @author Jonathan D. Bernal Fernández
 */
public class ConfigAprobacionParam extends CommonParam {
    
    public ConfigAprobacionParam() {
    }

    public ConfigAprobacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo etiqueta
     */
    @QueryParam("idTipoEtiqueta")
    private Integer idTipoEtiqueta;

    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoEtiqueta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo etiqueta"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la descripción"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de configuración de aprobación"));
        }
        
        if(isNull(idTipoEtiqueta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo etiqueta"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la descripción"));
        }
        
        return !tieneErrores();
    }
    
    public Integer getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(Integer idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
