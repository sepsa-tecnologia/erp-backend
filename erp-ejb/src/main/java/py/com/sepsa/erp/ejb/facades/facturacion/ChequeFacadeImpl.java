/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.Cheque;
import py.com.sepsa.erp.ejb.entities.facturacion.EntidadFinanciera;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ChequeParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ChequeFacade", mappedName = "ChequeFacade")
@Local(ChequeFacade.class)
public class ChequeFacadeImpl extends FacadeImpl<Cheque, ChequeParam> implements ChequeFacade {

    public ChequeFacadeImpl() {
        super(Cheque.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return bandera
     */
    @Override
    public Boolean validToCreate(ChequeParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        if ( param.getOmitirValidacionCheque() == null || param.getOmitirValidacionCheque().equalsIgnoreCase("N")){
            EntidadFinanciera entidadFinanciera = facades
                    .getEntidadFinancieraFacade()
                    .find(param.getIdEntidadFinanciera());

            if(entidadFinanciera == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la entidad financiera del cheque"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public Cheque create(ChequeParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Cheque cheque = new Cheque();
        cheque.setIdEmpresa(userInfo.getIdEmpresa());
        cheque.setFechaEmision(param.getFechaEmision());
        cheque.setFechaPago(param.getFechaPago());
        cheque.setIdEntidadFinanciera(param.getIdEntidadFinanciera());
        cheque.setMontoCheque(param.getMontoCheque());
        cheque.setNroCheque(param.getNroCheque());
        
        create(cheque);
        
        return cheque;
    }
}
