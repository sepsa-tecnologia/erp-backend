/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Descuento;
import py.com.sepsa.erp.ejb.entities.comercial.MontoTarifa;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleCadenaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.LiquidacionDetalleCadenaPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LiquidacionDetalleFacade", mappedName = "LiquidacionDetalleFacade")
@Local(LiquidacionDetalleFacade.class)
public class LiquidacionDetalleFacadeImpl extends FacadeImpl<LiquidacionDetalle, LiquidacionDetalleParam> implements LiquidacionDetalleFacade {

    public LiquidacionDetalleFacadeImpl() {
        super(LiquidacionDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarHistoricoLiquidacion Bandera para validar historico liquidación
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(LiquidacionDetalleParam param, boolean validarHistoricoLiquidacion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        HistoricoLiquidacion historicoLiquidacion = facades
                .getHistoricoLiquidacionFacade()
                .find(param.getIdHistoricoLiquidacion());
        
        if(validarHistoricoLiquidacion && historicoLiquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el historico de liquidación"));
        }
        
        if(param.getIdClienteRel() != null) {
            
            Cliente cliente = facades
                    .getClienteFacade()
                    .find(param.getIdClienteRel());
            
            if(cliente == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el cliente relacionado"));
            }
        }
        
        if(param.getIdMontoTarifa() != null) {
            
            MontoTarifa montoTarifa = facades
                    .getMontoTarifaFacade()
                    .find(param.getIdMontoTarifa());
            
            if(montoTarifa == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el monto tarifa"));
            }
        }
        
        if(param.getIdDescuento() != null) {
            
            Descuento descuento = facades
                    .getDescuentoFacade()
                    .find(param.getIdDescuento());
            
            if(descuento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el descuento"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(LiquidacionDetalleParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        LiquidacionDetalle liquidacionDetalle = find(param.getId());
        
        if(liquidacionDetalle == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el detalle de liquidación"));
        }
        
        if(param.getIdClienteRel() != null) {
            
            Cliente cliente = facades
                    .getClienteFacade()
                    .find(param.getIdClienteRel());
            
            if(cliente == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el cliente relacionado"));
            }
        }
        
        if(param.getIdMontoTarifa() != null) {
            
            MontoTarifa montoTarifa = facades
                    .getMontoTarifaFacade()
                    .find(param.getIdMontoTarifa());
            
            if(montoTarifa == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el monto tarifa"));
            }
        }
        
        if(param.getIdDescuento() != null) {
            
            Descuento descuento = facades
                    .getDescuentoFacade()
                    .find(param.getIdDescuento());
            
            if(descuento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el descuento"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public LiquidacionDetalle create(LiquidacionDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, Boolean.TRUE)) {
            return null;
        }
        
        LiquidacionDetalle ld = new LiquidacionDetalle();
        ld.setIdHistoricoLiquidacion(param.getIdHistoricoLiquidacion());
        ld.setNroLinea(param.getNroLinea());
        ld.setIdClienteRel(param.getIdClienteRel());
        ld.setIdDescuento(param.getIdDescuento());
        ld.setIdMontoTarifa(param.getIdMontoTarifa());
        ld.setMontoTarifa(param.getMontoTarifa());
        ld.setMontoDescuento(param.getMontoDescuento());
        ld.setMontoTotal(param.getMontoTotal());
        
        create(ld);
        
        return ld;
    }
    
    @Override
    public LiquidacionDetalle edit(LiquidacionDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        LiquidacionDetalle item = find(param.getId());
        item.setIdHistoricoLiquidacion(param.getIdHistoricoLiquidacion());
        item.setNroLinea(param.getNroLinea());
        item.setIdClienteRel(param.getIdClienteRel());
        item.setIdDescuento(param.getIdDescuento());
        item.setIdMontoTarifa(param.getIdMontoTarifa());
        item.setMontoTarifa(param.getMontoTarifa());
        item.setMontoDescuento(param.getMontoDescuento());
        item.setMontoTotal(param.getMontoTotal());
        
        edit(item);
        
        return item;
    }

    @Override
    public List<LiquidacionDetalle> find(LiquidacionDetalleParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format(" %s and ld.id = %d", where, param.getId());
        }
        
        if(param.getNroLinea() != null) {
            where = String.format(" %s and ld.nro_linea = %d", where, param.getNroLinea());
        }
        
        if(param.getIdHistoricoLiquidacion() != null) {
            where = String.format(" %s and ld.id_historico_liquidacion = %d", where, param.getIdHistoricoLiquidacion());
        }
        
        if(param.getIdContrato() != null) {
            where = String.format(" %s and co.id = %d", where, param.getIdContrato());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format(" %s and l.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdClienteRel() != null) {
            where = String.format(" %s and ld.id_cliente_rel = %d", where, param.getIdClienteRel());
        }
        
        if(param.getIdMontoTarifa() != null) {
            where = String.format(" %s and ld.id_monto_tarifa = %d", where, param.getIdMontoTarifa());
        }
        
        if(param.getIdDescuento() != null) {
            where = String.format(" %s and ld.id_descuento = %d", where, param.getIdDescuento());
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and to_char(l.fecha_desde, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and to_char(l.fecha_hasta, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select ld.*\n"
                + "from facturacion.liquidacion_detalle ld\n"
                + "left join facturacion.historico_liquidacion hl on hl.id = ld.id_historico_liquidacion\n"
                + "left join facturacion.liquidacion l on l.id = hl.id_liquidacion\n"
                + "left join comercial.contrato co on co.id = l.id_contrato\n"
                + "left join comercial.cliente cli on cli.id_cliente = co.id_cliente\n"
                + "left join comercial.cliente c on c.id_cliente = ld.id_cliente_rel\n"
                + "where %s order by ld.id_historico_liquidacion desc, ld.nro_linea asc offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, LiquidacionDetalle.class);

        List<LiquidacionDetalle> result = q.getResultList();
        
        return result;
    }

    @Override
    public Long findSize(LiquidacionDetalleParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format(" %s and ld.id = %d", where, param.getId());
        }
        
        if(param.getNroLinea() != null) {
            where = String.format(" %s and ld.nro_linea = %d", where, param.getNroLinea());
        }
        
        if(param.getIdHistoricoLiquidacion() != null) {
            where = String.format(" %s and ld.id_historico_liquidacion = %d", where, param.getIdHistoricoLiquidacion());
        }
        
        if(param.getIdContrato() != null) {
            where = String.format(" %s and co.id = %d", where, param.getIdContrato());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format(" %s and l.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdClienteRel() != null) {
            where = String.format(" %s and ld.id_cliente_rel = %d", where, param.getIdClienteRel());
        }
        
        if(param.getIdMontoTarifa() != null) {
            where = String.format(" %s and ld.id_monto_tarifa = %d", where, param.getIdMontoTarifa());
        }
        
        if(param.getIdDescuento() != null) {
            where = String.format(" %s and ld.id_descuento = %d", where, param.getIdDescuento());
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and to_char(l.fecha_desde, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and to_char(l.fecha_hasta, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select count(ld.id_historico_liquidacion)\n"
                + "from facturacion.liquidacion_detalle ld\n"
                + "left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = ld.id_historico_liquidacion\n"
                + "left join facturacion.liquidacion l on l.id = hl.id_liquidacion\n"
                + "left join comercial.contrato co on co.id = l.id_contrato\n"
                + "left join comercial.cliente cli on cli.id_cliente = co.id_cliente\n"
                + "left join comercial.cliente c on c.id_cliente = ld.id_cliente_rel\n"
                + "where %s",
                where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    /**
     * Obtiene la lista de liquidacion detalle por cadena
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public List<LiquidacionDetalleCadenaPojo> findCadena(LiquidacionDetalleCadenaParam param) {

        String where = "true";
        
        if(param.getIdCliente() != null) {
            where = String.format(" %s and cli.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdGrupo() != null) {
            where = String.format(" %s and cli.id_grupo = %d", where, param.getIdGrupo());
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and to_char(l.fecha_desde, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and to_char(l.fecha_hasta, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select distinct c.id_cliente, c.razon_social,\n"
                + "l.id_contrato, p.id as id_producto, p.descripcion\n"
                + "from facturacion.liquidacion l\n"
                + "left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "left join comercial.contrato co on co.id = l.id_contrato\n"
                + "left join comercial.cliente c on c.id_cliente = co.id_cliente\n"
                + "left join comercial.cliente cli on (ld.id_cliente_rel = cli.id_cliente or ld.id_grupo_cliente_rel = cli.id_grupo)\n"
                + "left join comercial.producto p on p.id = co.id_producto\n"
                + "where %s order by 2 asc offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<LiquidacionDetalleCadenaPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            LiquidacionDetalleCadenaPojo item = new LiquidacionDetalleCadenaPojo();
            item.setIdCliente((Integer)objects[0]);
            item.setCliente((String)objects[1]);
            item.setIdContrato((Integer)objects[2]);
            item.setIdProducto((Integer)objects[3]);
            item.setProducto((String)objects[4]);
            result.add(item);
        }
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de liquidacion detalle por cadena
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findCadenaSize(LiquidacionDetalleCadenaParam param) {

        String where = "true";
        
        if(param.getIdCliente() != null) {
            where = String.format(" %s and cli.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdGrupo() != null) {
            where = String.format(" %s and cli.id_grupo = %d", where, param.getIdGrupo());
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and to_char(l.fecha_desde, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and to_char(l.fecha_hasta, 'yyyy-MM-dd') = '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select count(distinct (c.id_cliente, p.id))\n"
                + "from facturacion.liquidacion l\n"
                + "left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "left join comercial.contrato co on co.id = l.id_contrato\n"
                + "left join comercial.cliente c on c.id_cliente = co.id_cliente\n"
                + "left join comercial.cliente cli on (ld.id_cliente_rel = cli.id_cliente or ld.id_grupo_cliente_rel = cli.id_grupo)\n"
                + "left join comercial.producto p on p.id = co.id_producto\n"
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
