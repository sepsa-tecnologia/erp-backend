/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.usuario.Perfil;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioPerfil;
import py.com.sepsa.erp.ejb.entities.usuario.filters.PerfilParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Sergio D. Riveros Vazquez
 */
@Stateless(name = "PerfilFacade", mappedName = "PerfilFacade")
@Local(PerfilFacade.class)
public class PerfilFacadeImpl extends FacadeImpl<Perfil, PerfilParam> implements PerfilFacade {

    public PerfilFacadeImpl() {
        super(Perfil.class);
    }

    public Boolean validToCreate(PerfilParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        PerfilParam param1 = new PerfilParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un perfil con el mismo código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(PerfilParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Perfil perfil = find(param.getId());
        
        if(perfil == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el perfil"));
        } else if (!perfil.getCodigo().equals(param.getCodigo().trim())) {
            PerfilParam param1 = new PerfilParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un perfil con el mismo código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Perfil create(PerfilParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Perfil perfil = new Perfil();
        perfil.setIdEmpresa(param.getIdEmpresa());
        perfil.setDescripcion(param.getDescripcion().trim());
        perfil.setCodigo(param.getCodigo().trim());
        perfil.setActivo(param.getActivo());
        
        create(perfil);
        
        return perfil;
    }

    @Override
    public Perfil edit(PerfilParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Perfil perfil = find(param.getId());
        perfil.setDescripcion(param.getDescripcion().trim());
        perfil.setCodigo(param.getCodigo().trim());
        perfil.setActivo(param.getActivo());
        perfil.setPermisos(new Gson().fromJson(param.getPermisos(), JsonElement.class));

        edit(perfil);
        
        return perfil;
    }
    
    @Override
    public Perfil find(Integer idEmpresa, Integer id, String codigo) {
        PerfilParam param = new PerfilParam();
        param.setIdEmpresa(idEmpresa);
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<Perfil> getListPerfil(Integer idEmpresa, String codigoEmpresa, Integer userId) {
        List<Perfil> list = null;
        if (userId != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery(Perfil.class);

            Root<UsuarioPerfil> userperfil = cq.from(UsuarioPerfil.class);
            Join<UsuarioPerfil, Perfil> perfil = userperfil.join("perfil");
            Join<Perfil, Empresa> empresa = perfil.join("empresa");
            Join<UsuarioPerfil, Estado> estado = userperfil.join("estado");
            
            cq.select(perfil);

            Predicate pred = qb.equal(userperfil.get("idUsuario"), userId);
            pred = qb.and(pred, qb.equal(estado.get("codigo"), "ACTIVO"));
            if(idEmpresa != null) {
                pred = qb.and(pred, qb.equal(empresa.get("id"), idEmpresa));
            }
            if(codigoEmpresa != null) {
                pred = qb.and(pred, qb.equal(empresa.get("codigo"), codigoEmpresa));
            }
            
            cq.where(pred);

            Query q = getEntityManager()
                    .createQuery(cq)
                    .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

            list = q.getResultList();
        }
        return list;
    }

    /**
     * Obtiene un perfil con un idPerfil como parametro
     *
     * @param perfilId Identificador del usuario buscado
     * @return Usuario
     */
    @Override
    public List<Perfil> findPerfil(Integer perfilId) {
        List<Perfil> perfil = null;
        if (perfilId != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery cq = qb.createQuery();
            Root<Perfil> root = cq.from(Perfil.class);
            cq.select(root);
            cq.where(qb.equal(root.get("id"), perfilId));
            Query q = getEntityManager().createQuery(cq);
            perfil = q.getResultList();
        }
        return perfil;
    }

    @Override
    public List<Perfil> find(PerfilParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Perfil.class);
        Root<Perfil> root = cq.from(Perfil.class);

        cq.select(root);
        
        cq.orderBy(qb.asc(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Perfil> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(PerfilParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Perfil> root = cq.from(Perfil.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Long result = ((Number) q.getSingleResult()).longValue();

        return result;
    }
    
}
