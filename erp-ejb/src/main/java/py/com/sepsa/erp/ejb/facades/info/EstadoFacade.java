/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.filters.EstadoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface EstadoFacade extends Facade<Estado, EstadoParam, UserInfoImpl> {

    public EstadoPojo find(Integer id, String codigoEstado, String codigoTipoEstado);

    public List<EstadoPojo> findPojoAlternative(EstadoParam param);
    
}
