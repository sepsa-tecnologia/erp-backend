/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils.jwe;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwk.JsonWebKey;
import org.jose4j.lang.JoseException;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioEmpresaPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPerfilPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.utils.misc.Unit;
import py.com.sepsa.utils.misc.Units;

/**
 *
 * @author Jonathan
 */
public class JWEUtils {
    
    /**
     * The shared secret or shared symmetric key represented as a octet sequence JSON Web Key (JWK)
     */
    private static final String JWK_JSON = "{\"kty\":\"oct\",\"k\":\"Fdh9u8rINbbnairbvifxVT1u232VQBZYKx1PGAGHt2I\"}";
    
    /**
     * Método que valida el JWT
     * @param jwt JWT
     * @return JWT content as JSON
     * @throws JoseException 
     */
    public static JsonObject validateAsJson(String jwt) throws JoseException {

        JsonWebKey jwk = JsonWebKey.Factory.newJwk(JWK_JSON);
        // That other party, the receiver, can then use JsonWebEncryption to decrypt the message.
        JsonWebEncryption receiverJwe = new JsonWebEncryption();
        // Set the algorithm constraints based on what is agreed upon or expected from the sender
        AlgorithmConstraints algConstraints = new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, KeyManagementAlgorithmIdentifiers.DIRECT);
        receiverJwe.setAlgorithmConstraints(algConstraints);
        AlgorithmConstraints encConstraints = new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        receiverJwe.setContentEncryptionAlgorithmConstraints(encConstraints);
        // Set the compact serialization on new Json Web Encryption object
        receiverJwe.setCompactSerialization(jwt);
        // Symmetric encryption, like we are doing here, requires that both parties have the same key.
        // The key will have had to have been securely exchanged out-of-band somehow.
        receiverJwe.setKey(jwk.getKey());
        // Get the message that was encrypted in the JWE. This step performs the actual decryption steps.
        String plaintext = receiverJwe.getPlaintextString();

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(plaintext);

        return element.getAsJsonObject();
    }
    
    public static String buildJWT(UsuarioPojo user, Integer idEmpresa) throws JoseException {

        HashMap<String, List<String>> profiles = new HashMap<>();
        
        for (UsuarioPerfilPojo up : user.getUsuarioPerfiles()) {
            
            if(idEmpresa != null && !idEmpresa.equals(up.getIdEmpresa())) {
                continue;
            }
            
            List<String> temp = profiles
                    .getOrDefault("SEPSA_ERP",
                            new ArrayList<>());

            if (!temp.contains(up.getCodigoPerfil())) {
                temp.add(up.getCodigoPerfil());
            }

            profiles.put("SEPSA_ERP", temp);
        }

        JsonArray perfiles = new JsonArray();

        for (String string : profiles.keySet()) {

            JsonArray temp2 = new JsonArray();
            for (String string1 : profiles.get(string)) {
                temp2.add(string1);
            }

            JsonObject object = new JsonObject();
            object.addProperty("software", string);
            object.add("roles", temp2);

            perfiles.add(object);
        }
        
        /*representa la hora actual + 1 hora de duracion del token*/
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, 24);
        String vencimiento = sdf.format(now.getTime());
        
        UsuarioEmpresaPojo ue = Units.<Unit<UsuarioEmpresaPojo>, UsuarioEmpresaPojo>execute(() -> ((List<UsuarioEmpresaPojo>)user.getUsuarioEmpresas()).get(0));
        
        JsonObject obj = new JsonObject();
        obj.addProperty("id", user.getId());
        obj.addProperty("idEmpresa", Units.<Unit<Integer>, Integer>execute(() -> ue.getIdEmpresa()));
        obj.addProperty("idClienteSepsa", Units.<Unit<Integer>, Integer>execute(() -> ue.getIdClienteSepsa()));
        obj.addProperty("idTipoEmpresa", Units.<Unit<Integer>, Integer>execute(() -> ue.getIdTipoEmpresa()));
        obj.addProperty("codigoTipoEmpresa", Units.<Unit<String>, String>execute(() -> ue.getCodigoTipoEmpresa()));
        obj.addProperty("moduloInventario", Units.<Unit<Character>, Character>execute(() -> ue.getModuloInventario()));
        obj.addProperty("usuario", user.getUsuario());
        obj.addProperty("idPersonaFisica", user.getIdPersonaFisica());
        obj.addProperty("nombre", user.getNombre());
        obj.addProperty("apellido", user.getApellido());
        obj.addProperty("vencimiento", vencimiento);
        obj.add("perfiles", perfiles);

        JsonWebKey jwk = JsonWebKey.Factory.newJwk(JWK_JSON);
        JsonWebEncryption senderJwe = new JsonWebEncryption();
        senderJwe.setPlaintext(obj.toString());
        senderJwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.DIRECT);
        senderJwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        senderJwe.setKey(jwk.getKey());
        String compactSerialization = senderJwe.getCompactSerialization();

        return compactSerialization;
    }
}
