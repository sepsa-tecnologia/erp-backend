/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.math.BigInteger;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de local
 * @author Jonathan
 */
public class LocalParam extends CommonParam {
    
    public LocalParam() {
    }

    public LocalParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Identificador de personas
     */
    @QueryParam("idPersonas")
    private List<Integer> idPersonas;
    
    /**
     * Observacion
     */
    @QueryParam("observacion")
    private String observacion;
    
    /**
     * Identificador externo
     */
    @QueryParam("idExterno")
    private String idExterno;
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * GLN
     */
    @QueryParam("gln")
    private BigInteger gln;
    
    /**
     * Local externo
     */
    @QueryParam("localExterno")
    private Character localExterno;
    
    /**
     * Identificador de dirección
     */
    @QueryParam("idDireccion")
    private Integer idDireccion;
    
    /**
     * Identificador de telefono
     */
    @QueryParam("idTelefono")
    private Integer idTelefono;
    
    private DireccionParam direccion;
    
    private TelefonoParam telefono;

    public boolean isValidToGetByIdExterno() {
        limpiarErrores();
        
        if(isNullOrEmpty(idExterno)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador externo del local"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToGetByGln() {
        limpiarErrores();
        
        if(isNull(gln)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el gln del local"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el local esta activo"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNull(localExterno)) {
            localExterno = 'S';
        }
        
        if(!isNull(direccion)) {
            direccion.isValidToCreate();
        }
        
        if(!isNull(telefono)) {
            telefono.isValidToCreate();
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el local esta activo"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNull(localExterno)) {
            localExterno = 'S';
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(direccion)) {
            list.addAll(direccion.getErrores());
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdPersonas(List<Integer> idPersonas) {
        this.idPersonas = idPersonas;
    }

    public List<Integer> getIdPersonas() {
        return idPersonas;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setIdExterno(String idExterno) {
        this.idExterno = idExterno;
    }

    public String getIdExterno() {
        return idExterno;
    }

    public BigInteger getGln() {
        return gln;
    }

    public void setGln(BigInteger gln) {
        this.gln = gln;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setDireccion(DireccionParam direccion) {
        this.direccion = direccion;
    }

    public DireccionParam getDireccion() {
        return direccion;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    public void setLocalExterno(Character localExterno) {
        this.localExterno = localExterno;
    }

    public Character getLocalExterno() {
        return localExterno;
    }

    public TelefonoParam getTelefono() {
        return telefono;
    }

    public void setTelefono(TelefonoParam telefono) {
        this.telefono = telefono;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }
    
}
