/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.filters.EmpresaParam;
import py.com.sepsa.erp.ejb.entities.usuario.Token;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.TokenParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.TokenPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "TokenFacade", mappedName = "TokenFacade")
@Local(TokenFacade.class)
public class TokenFacadeImpl extends FacadeImpl<Token, TokenParam> implements TokenFacade {

    public TokenFacadeImpl() {
        super(Token.class);
    }

    public Boolean validToCreate(TokenParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        UsuarioParam up = new UsuarioParam();
        up.setId(param.getIdUsuario());

        Long upSize = facades.getUsuarioFacade().findSize(up);

        if (upSize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }

        EmpresaParam e = new EmpresaParam();
        e.setId(param.getIdUsuario());

        Long eSize = facades.getEmpresaFacade().findSize(e);

        if (eSize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(TokenParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        Token token = find(param.getId());

        if (token == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el token"));
        }

        return !param.tieneErrores();
    }

    @Override
    public Token create(TokenParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        Token token = new Token();
        token.setIdEmpresa(param.getIdEmpresa());
        token.setIdUsuario(param.getIdUsuario());
        token.setClave(UUID.randomUUID().toString());
        token.setFechaInsercion(Calendar.getInstance().getTime());
        token.setActivo(param.getActivo());

        create(token);

        return token;
    }

    @Override
    public Token edit(TokenParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Token token = find(param.getId());
        token.setActivo(param.getActivo());

        edit(token);

        return token;
    }

    @Override
    public List<TokenPojo> findPojo(TokenParam param) {

        AbstractFind find = new AbstractFind(TokenPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idUsuario"),
                        getPath("usuario").get("usuario"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("activo"),
                        getPath("root").get("fechaInsercion"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<Token> find(TokenParam param) {

        AbstractFind find = new AbstractFind(Token.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, TokenParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Token> root = cq.from(Token.class);
        Join<Token, Usuario> usuario = root.join("usuario");
        Join<Token, Empresa> empresa = root.join("empresa");

        find.addPath("root", root);
        find.addPath("usuario", usuario);
        find.addPath("empresa", empresa);
        find.select(cq, qb);

        cq.orderBy(qb.asc(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(root.get("idUsuario"), param.getIdUsuario()));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(TokenParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Token> root = cq.from(Token.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdUsuario() != null) {
            predList.add(qb.equal(root.get("idUsuario"), param.getIdUsuario()));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Long result = ((Number) q.getSingleResult()).longValue();

        return result;
    }

    @Override
    public TokenPojo find(Integer idUsuario, Integer idEmpresa) {
        TokenParam param = new TokenParam();
        param.setIdUsuario(idUsuario);
        param.setIdEmpresa(idEmpresa);
        param.setActivo('A');
        return findFirstPojo(param);
    }

    @Override
    public TokenPojo find(String clave) {
        TokenParam param = new TokenParam();
        param.setClave(clave);
        return findFirstPojo(param);
    }

}
