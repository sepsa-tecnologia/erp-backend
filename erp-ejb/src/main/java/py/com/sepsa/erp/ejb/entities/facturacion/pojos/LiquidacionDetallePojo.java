/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;

/**
 * POJO para la entidad liquidacion detalle
 * @author Jonathan
 */
public class LiquidacionDetallePojo {

    public LiquidacionDetallePojo() {
    }

    public LiquidacionDetallePojo(Integer idHistoricoLiquidacion, Integer nroLinea, Integer idMontoTarifa, Integer idDescuento, Integer idClienteRel, BigDecimal montoTarifa, BigDecimal montoDescuento, BigDecimal montoTotal) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
        this.nroLinea = nroLinea;
        this.idMontoTarifa = idMontoTarifa;
        this.idDescuento = idDescuento;
        this.idClienteRel = idClienteRel;
        this.montoTarifa = montoTarifa;
        this.montoDescuento = montoDescuento;
        this.montoTotal = montoTotal;
    }
    
    /**
     * Identificador de historico liquidacion
     */
    private Integer idHistoricoLiquidacion;
    
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    
    /**
     * Identificador de monto tarifa
     */
    private Integer idMontoTarifa;
    
    /**
     * Identificador de descuento
     */
    private Integer idDescuento;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Cliente Relacionado
     */
    private String clienteRel;
    
    /**
     * Identificador de cliente relacionado
     */
    private Integer idClienteRel;
    
    /**
     * Monto tarifa
     */
    private BigDecimal montoTarifa;
    
    /**
     * Monto descuento
     */
    private BigDecimal montoDescuento;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;

    /**
     * Cantidad de documentos enviados
     */
    private Integer cantDocEnviados;
    
    /**
     * Cantidad de documentos recibidos
     */
    private Integer cantDocRecibidos;
    
    /**
     * Cantidad de locales del cliente relacionado
     */
    private Integer clienteRelCantLocales;
    
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(Integer idDescuento) {
        this.idDescuento = idDescuento;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCliente() {
        return cliente;
    }

    public Integer getIdClienteRel() {
        return idClienteRel;
    }

    public void setIdClienteRel(Integer idClienteRel) {
        this.idClienteRel = idClienteRel;
    }

    public BigDecimal getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(BigDecimal montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public BigDecimal getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(BigDecimal montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(Integer cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public Integer getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(Integer cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }

    public Integer getClienteRelCantLocales() {
        return clienteRelCantLocales;
    }

    public void setClienteRelCantLocales(Integer clienteRelCantLocales) {
        this.clienteRelCantLocales = clienteRelCantLocales;
    }

    public void setIdMontoTarifa(Integer idMontoTarifa) {
        this.idMontoTarifa = idMontoTarifa;
    }

    public Integer getIdMontoTarifa() {
        return idMontoTarifa;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getClienteRel() {
        return clienteRel;
    }

    public void setClienteRel(String clienteRel) {
        this.clienteRel = clienteRel;
    }
}
