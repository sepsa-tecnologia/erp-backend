/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;

/**
 * POJO para el reporte de proveedor operativo por comprador
 * @author Jonathan
 */
public class ProvOperativoPorCompPojo {
    
    /**
     * Identificador de proveedor
     */
    private Integer idProveedor;
    
    /**
     * Proveedor
     */
    private String proveedor;
    
    /**
     * Identificador de comprador
     */
    private Integer idComprador;
    
    /**
     * Identificador de grupo de comprador
     */
    private Integer idGrupoComprador;
    
    /**
     * Comprador
     */
    private String comprador;
    
    /**
     * Tipo de tarifa
     */
    private String tipoTarifa;
    
    /**
     * Tarifa
     */
    private String tarifa;
    
    /**
     * Monto tarifa
     */
    private BigDecimal montoTarifa;
    
    /**
     * Monto descuento
     */
    private BigDecimal montoDescuento;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Cantidad de documentos enviados
     */
    private Integer cantDocEnviados;
    
    /**
     * Cantidad de documentos recibidos
     */
    private Integer cantDocRecibidos;

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public Integer getIdGrupoComprador() {
        return idGrupoComprador;
    }

    public void setIdGrupoComprador(Integer idGrupoComprador) {
        this.idGrupoComprador = idGrupoComprador;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public BigDecimal getMontoTarifa() {
        return montoTarifa;
    }

    public void setMontoTarifa(BigDecimal montoTarifa) {
        this.montoTarifa = montoTarifa;
    }

    public BigDecimal getMontoDescuento() {
        return montoDescuento;
    }

    public void setMontoDescuento(BigDecimal montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getCantDocEnviados() {
        return cantDocEnviados;
    }

    public void setCantDocEnviados(Integer cantDocEnviados) {
        this.cantDocEnviados = cantDocEnviados;
    }

    public Integer getCantDocRecibidos() {
        return cantDocRecibidos;
    }

    public void setCantDocRecibidos(Integer cantDocRecibidos) {
        this.cantDocRecibidos = cantDocRecibidos;
    }
}
