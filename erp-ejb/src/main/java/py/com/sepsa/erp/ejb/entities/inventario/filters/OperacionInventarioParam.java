/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
public class OperacionInventarioParam extends CommonParam {

    public OperacionInventarioParam() {
    }

    public OperacionInventarioParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;

    /**
     * Identificador de tipo de operacion
     */
    @QueryParam("idTipoOperacion")
    private Integer idTipoOperacion;

    /**
     * Código de tipo de operacion
     */
    @QueryParam("codigoTipoOperacion")
    private String codigoTipoOperacion;

    /**
     * Identificador de motivo
     */
    @QueryParam("idMotivo")
    private Integer idMotivo;

    /**
     * Código motivo
     */
    @QueryParam("codigoMotivo")
    private String codigoMotivo;

    /**
     * Código tipo motivo
     */
    @QueryParam("codigoTipoMotivo")
    private String codigoTipoMotivo;

    /**
     * Identificador de orden de compra
     */
    @QueryParam("idOrdenCompra")
    private Integer idOrdenCompra;

    /**
     * Identificador de control de inventario
     */
    @QueryParam("idControlInventario")
    private Integer idControlInventario;

    /**
     * Código de control de inventario
     */
    @QueryParam("codigoControlInventario")
    private String codigoControlInventario;

    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Cambio de estado
     */
    private Boolean changeState;
    
    /**
     * Lista de detalle
     */
    private List<OperacionInventarioDetalleParam> operacionInventarioDetalles;
    
    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idTipoOperacion) && isNullOrEmpty(codigoTipoOperacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de operación"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        if (isNullOrEmpty(codigoTipoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de tipo de motivo"));
        }

        if (isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }
        
        if(!isNullOrEmpty(operacionInventarioDetalles)) {
            for (OperacionInventarioDetalleParam detalle : operacionInventarioDetalles) {
                detalle.setIdOperacionInventario(0);
                detalle.setIdEstadoOperacion(idEstado);
                detalle.setCodigoEstadoOperacion(codigoEstado);
                detalle.isValidToCreate();
            }
        }

        return !tieneErrores();
    }
    
    public boolean isValidToChangeState() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de operación de inventario"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNullOrEmpty(operacionInventarioDetalles)) {
            for (OperacionInventarioDetalleParam detalle : operacionInventarioDetalles) {
                list.addAll(detalle.getErrores());
            }
        }
        
        return list;
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNull(idTipoOperacion) && isNullOrEmpty(codigoTipoOperacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de operación"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        if (isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }
        
        if (isNullOrEmpty(codigoTipoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de tipo de motivo"));
        }

        return !tieneErrores();
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public String getCodigoTipoOperacion() {
        return codigoTipoOperacion;
    }

    public void setCodigoTipoOperacion(String codigoTipoOperacion) {
        this.codigoTipoOperacion = codigoTipoOperacion;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public void setOperacionInventarioDetalles(List<OperacionInventarioDetalleParam> operacionInventarioDetalles) {
        this.operacionInventarioDetalles = operacionInventarioDetalles;
    }

    public List<OperacionInventarioDetalleParam> getOperacionInventarioDetalles() {
        return operacionInventarioDetalles;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setChangeState(Boolean changeState) {
        this.changeState = changeState;
    }

    public Boolean getChangeState() {
        return changeState;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setCodigoControlInventario(String codigoControlInventario) {
        this.codigoControlInventario = codigoControlInventario;
    }

    public String getCodigoControlInventario() {
        return codigoControlInventario;
    }

    public void setCodigoTipoMotivo(String codigoTipoMotivo) {
        this.codigoTipoMotivo = codigoTipoMotivo;
    }

    public String getCodigoTipoMotivo() {
        return codigoTipoMotivo;
    }
}
