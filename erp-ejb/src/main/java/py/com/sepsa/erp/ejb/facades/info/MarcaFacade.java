/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Marca;
import py.com.sepsa.erp.ejb.entities.info.filters.MarcaParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface MarcaFacade extends Facade<Marca, MarcaParam, UserInfoImpl> {
    
    public Boolean validToCreate(MarcaParam param);
    
    public Marca findOrCreate(MarcaParam param, UserInfoImpl userInfo);
    
}
