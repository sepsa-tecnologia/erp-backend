package py.com.sepsa.erp.ejb.externalservice.http;

/**
 * Códigos de respuesta
 * @author Daniel F. Escauriza Arza
 */
public enum ResponseCode {
    
    OK(200, "OK"),
    BAD_REQUEST(400, "Error en la solicitud realizada al servicio"),
    UNAUTHORIZED(401, "Error en credenciales de autenticación"),
    FORBIDDEN(403, "No posee la autorización necesaria"),
    NOT_FOUND(404, "No se encuentra el la página solicitada"),
    ERROR(500, "Se ha producido un error en el sistema. Por favor, "
            + "comuniquese con el soporte tecnico de SEPSA.");//Cualquier error de forma genérica, sin tratar otros tipos de respuestaa HTTP
    
    /**
     * Código de respuesta
     */
    private int code;
    
    /**
     * Descripción de la respuesta
     */
    private String description;

    /**
     * Obtiene el código de la respuesta
     * @return Código de la respuesta
     */
    public int getCode() {
        return code;
    }

    /**
     * Setea el código de la respuesta
     * @param code Código de la respuesta
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * Obtiene la descripción de la respuesta
     * @return Descripción de la respuesta
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción de la respuesta
     * @param description Descripción de la respuesta
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Constructor de ResponseCode
     * @param code Código de la respuesta
     * @param description Descripción de la respuesta
     */
    private ResponseCode(int code, String description) {
        this.code = code;
        this.description = description;
    }
    
}
