/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de procesamiento archivo detalle
 *
 * @author Jonathan
 */
public class ProcesamientoArchivoDetalleParam extends CommonParam {

    public ProcesamientoArchivoDetalleParam() {
    }

    public ProcesamientoArchivoDetalleParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de procesamiento de archivo
     */
    @QueryParam("idProcesamientoArchivo")
    private Integer idProcesamientoArchivo;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idProcesamientoArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento de archivo"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }
}
