/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.Descuento;
import py.com.sepsa.erp.ejb.entities.comercial.MotivoDescuento;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.TipoDescuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.DescuentoParam;
import py.com.sepsa.erp.ejb.entities.info.Aprobacion;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "DescuentoFacade", mappedName = "DescuentoFacade")
@Local(DescuentoFacade.class)
public class DescuentoFacadeImpl extends FacadeImpl<Descuento, DescuentoParam> implements DescuentoFacade {

    public DescuentoFacadeImpl() {
        super(Descuento.class);
    }

    @Override
    public Map<String, String> getMapConditions(Descuento item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.

        map.put("id", item.getId() + "");
        map.put("id_cliente", item.getIdCliente() + "");
        map.put("id_contrato", item.getIdContrato() + "");
        map.put("id_servicio", item.getIdServicio() + "");
        map.put("id_cadena", item.getIdCadena() + "");
        map.put("codigo", item.getCodigo() + "");
        map.put("descripcion", item.getDescripcion() + "");
        map.put("fecha_insercion", item.getFechaInsercion() + "");
        map.put("fecha_desde", item.getFechaDesde() + "");
        map.put("fecha_hasta", item.getFechaHasta() + "");
        map.put("valor_descuento", item.getValorDescuento() == null ? "" : item.getValorDescuento().toPlainString());
        map.put("id_estado", item.getIdEstado() + "");
        map.put("excedente", item.getExcedente() + "");
        map.put("porcentual", item.getPorcentual() + "");
        map.put("id_motivo_descuento", item.getIdMotivoDescuento() + "");
        map.put("descripcion", item.getDescripcion() + "");
        map.put("id_aprobacion", item.getIdAprobacion() + "");
        map.put("id_tipo_descuento", item.getIdTipoDescuento() + "");

        return map;
    }

    @Override
    public Map<String, String> getMapPk(Descuento item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.

        pk.put("id", item.getId() + "");

        return pk;
    }

    @Override
    public Boolean validToCreate(DescuentoParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        if(!isNull(param.getIdCliente())) {
            
            ClienteParam cparam = new ClienteParam();
            cparam.setIdCliente(param.getIdCliente());
            Long clienteSize = facades.getClienteFacade().findSize(cparam);
            
            if(clienteSize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente"));
            }
        }

        if(!isNull(param.getIdMotivoDescuento())) {
            MotivoDescuento motivoDescuento = facades.getMotivoDescuentoFacade().find(param.getIdMotivoDescuento());

            if (motivoDescuento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el motivo de descuento"));
            }
        }

        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "DESCUENTO");
        if (estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado de descuento"));
        } else {
            param.setIdEstado(estado.getId());
        }

        TipoDescuento tipoDescuento = facades.getTipoDescuentoFacade().find(param.getIdTipoDescuento(), param.getCodigoTipoDescuento());

        if (tipoDescuento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de descuento"));
        } else {
            param.setIdTipoDescuento(tipoDescuento.getId());
        }

        Contrato contrato = null;
        Servicio servicio = null;
        
        if(!isNull(param.getIdContrato())) {
            contrato = facades.getContratoFacade().find(param.getIdContrato());

            if (contrato == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el contrato"));
            }
        }

        if(!isNull(param.getIdCadena())) {
            Cliente cadena = facades.getClienteFacade().find(param.getIdCadena());

            if (cadena == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la cadena"));
            }
        }

        if(!isNull(param.getIdServicio())) {
            servicio = facades.getServicioFacade().find(param.getIdServicio());

            if (servicio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el servicio"));
            }
        }

        if(!isNull(contrato) && !isNull(servicio)) {
            ContratoServicioParam param1 = new ContratoServicioParam();
            param1.setIdContrato(param.getIdContrato());
            param1.setIdServicio(param.getIdServicio());

            Long size = facades.getContratoServicioFacade().findSize(param1);

            if (size == 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El contrato no posee el servicio indicado"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Descuento create(DescuentoParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        Aprobacion aprobacion = facades.getAprobacionFacade().create();
        
        Descuento descuento = new Descuento();
        descuento.setIdAprobacion(aprobacion.getId());
        descuento.setIdTipoDescuento(param.getIdTipoDescuento());
        descuento.setDescripcion(param.getDescripcion());
        descuento.setCodigo(param.getCodigo());
        descuento.setIdEstado(param.getIdEstado());
        descuento.setPorcentual(param.getPorcentual());
        descuento.setExcedente(param.getExcedente());
        descuento.setFechaInsercion(Calendar.getInstance().getTime());
        descuento.setFechaDesde(param.getFechaDesde());
        descuento.setFechaHasta(param.getFechaHasta());
        descuento.setIdCadena(param.getIdCadena());
        descuento.setIdCliente(param.getIdCliente());
        descuento.setIdServicio(param.getIdServicio());
        descuento.setIdMotivoDescuento(param.getIdMotivoDescuento());
        descuento.setIdContrato(param.getIdContrato());
        descuento.setValorDescuento(param.getValorDescuento());
        descuento.setDescripcion(param.getDescripcion());

        create(descuento);

        return descuento;
    }

    /**
     * Obtiene la lista de descuento
     *
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public List<Descuento> find(DescuentoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Descuento.class);
        Root<Descuento> root = cq.from(Descuento.class);
        Join<Descuento, Cliente> cliente = root.join("cliente", JoinType.LEFT);
        Join<Descuento, Cliente> cadena = root.join("cadena", JoinType.LEFT);
        Join<Descuento, Contrato> contrato = root.join("contrato", JoinType.LEFT);
        Join<Descuento, Servicio> servicio = root.join("servicio", JoinType.LEFT);
        Join<Servicio, ProductoCom> producto = servicio.join("producto", JoinType.LEFT);
        Join<Descuento, MotivoDescuento> motivoDescuento = root.join("motivoDescuento", JoinType.LEFT);
        Join<Descuento, TipoDescuento> tipoDescuento = root.join("tipoDescuento", JoinType.LEFT);

        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdCadena() != null) {
            predList.add(qb.equal(cadena.get("idCliente"), param.getIdCadena()));
        }

        if (param.getIdContrato() != null) {
            predList.add(qb.equal(contrato.get("id"), param.getIdContrato()));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(cliente.get("idCliente"), param.getIdCliente()));
        }

        if (param.getIdProducto() != null) {
            predList.add(qb.equal(producto.get("id"), param.getIdProducto()));
        }

        if (param.getIdServicio() != null) {
            predList.add(qb.equal(servicio.get("id"), param.getIdServicio()));
        }

        if (param.getIdMotivoDescuento() != null) {
            predList.add(qb.equal(motivoDescuento.get("id"), param.getIdMotivoDescuento()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getIdTipoDescuento() != null) {
            predList.add(qb.equal(root.get("idTipoDescuento"), param.getIdTipoDescuento()));
        }

        if (param.getCodigoTipoDescuento() != null && !param.getCodigoTipoDescuento().trim().isEmpty()) {
            predList.add(qb.equal(tipoDescuento.get("codigo"), param.getCodigoTipoDescuento().trim()));
        }

        if (param.getPorcentual() != null) {
            predList.add(qb.equal(root.get("porcentual"), param.getPorcentual()));
        }

        if (param.getExcedente() != null) {
            predList.add(qb.equal(root.get("excedente"), param.getExcedente()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getFechaDesdeDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaDesde"), param.getFechaDesdeDesde()));
        }

        if (param.getFechaDesdeHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaDesde"), param.getFechaDesdeHasta()));
        }

        if (param.getFechaHastaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaHasta"), param.getFechaHastaDesde()));
        }

        if (param.getFechaHastaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaHasta"), param.getFechaHastaHasta()));
        }

        if (param.getTieneCadena() != null) {
            predList.add(param.getTieneCadena()
                    ? cadena.isNotNull()
                    : cadena.isNull());
        }

        if (param.getTieneContrato() != null) {
            predList.add(param.getTieneContrato()
                    ? contrato.isNotNull()
                    : contrato.isNull());
        }

        if (param.getTieneServicio() != null) {
            predList.add(param.getTieneServicio()
                    ? servicio.isNotNull()
                    : servicio.isNull());
        }

        if (param.getTieneCliente() != null) {
            predList.add(param.getTieneCliente()
                    ? cliente.isNotNull()
                    : cliente.isNull());
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.asc(root.get("estado")),
                qb.asc(root.get("fechaHasta")),
                qb.desc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<Descuento> result = q.getResultList();

        return result;
    }

    /**
     * Obtiene el tamaño de la lista de descuento
     *
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(DescuentoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Descuento> root = cq.from(Descuento.class);
        Join<Descuento, Cliente> cliente = root.join("cliente", JoinType.LEFT);
        Join<Descuento, Cliente> cadena = root.join("cadena", JoinType.LEFT);
        Join<Descuento, Contrato> contrato = root.join("contrato", JoinType.LEFT);
        Join<Descuento, Servicio> servicio = root.join("servicio", JoinType.LEFT);
        Join<Servicio, ProductoCom> producto = servicio.join("producto", JoinType.LEFT);
        Join<Descuento, MotivoDescuento> motivoDescuento = root.join("motivoDescuento", JoinType.LEFT);
        Join<Descuento, TipoDescuento> tipoDescuento = root.join("tipoDescuento", JoinType.LEFT);

        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdCadena() != null) {
            predList.add(qb.equal(cadena.get("idCliente"), param.getIdCadena()));
        }

        if (param.getIdContrato() != null) {
            predList.add(qb.equal(contrato.get("id"), param.getIdContrato()));
        }

        if (param.getIdCliente() != null) {
            predList.add(qb.equal(cliente.get("idCliente"), param.getIdCliente()));
        }

        if (param.getIdProducto() != null) {
            predList.add(qb.equal(producto.get("id"), param.getIdProducto()));
        }

        if (param.getIdServicio() != null) {
            predList.add(qb.equal(servicio.get("id"), param.getIdServicio()));
        }

        if (param.getIdMotivoDescuento() != null) {
            predList.add(qb.equal(motivoDescuento.get("id"), param.getIdMotivoDescuento()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getIdTipoDescuento() != null) {
            predList.add(qb.equal(root.get("idTipoDescuento"), param.getIdTipoDescuento()));
        }

        if (param.getCodigoTipoDescuento() != null && !param.getCodigoTipoDescuento().trim().isEmpty()) {
            predList.add(qb.equal(tipoDescuento.get("codigo"), param.getCodigoTipoDescuento().trim()));
        }
        
        if (param.getPorcentual() != null) {
            predList.add(qb.equal(root.get("porcentual"), param.getPorcentual()));
        }

        if (param.getExcedente() != null) {
            predList.add(qb.equal(root.get("excedente"), param.getExcedente()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getFechaDesdeDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaDesde"), param.getFechaDesdeDesde()));
        }

        if (param.getFechaDesdeHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaDesde"), param.getFechaDesdeHasta()));
        }

        if (param.getFechaHastaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaHasta"), param.getFechaHastaDesde()));
        }

        if (param.getFechaHastaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaHasta"), param.getFechaHastaHasta()));
        }

        if (param.getTieneCadena() != null) {
            predList.add(param.getTieneCadena()
                    ? cadena.isNotNull()
                    : cadena.isNull());
        }

        if (param.getTieneContrato() != null) {
            predList.add(param.getTieneContrato()
                    ? contrato.isNotNull()
                    : contrato.isNull());
        }

        if (param.getTieneServicio() != null) {
            predList.add(param.getTieneServicio()
                    ? servicio.isNotNull()
                    : servicio.isNull());
        }

        if (param.getTieneCliente() != null) {
            predList.add(param.getTieneCliente()
                    ? cliente.isNotNull()
                    : cliente.isNull());
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;

        return result;
    }
    
    public Boolean validToInactivate(DescuentoParam param) {
        
        if(!param.isValidToInactivate()) {
            return Boolean.FALSE;
        }
        
        Descuento descuento = find(param.getId());
        
        if(descuento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el descuento"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToActivate(DescuentoParam param) {
        
        if(!param.isValidToActivate()) {
            return Boolean.FALSE;
        }
        
        Descuento descuento = find(param.getId());
        
        if(descuento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el descuento"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Descuento inactivar(DescuentoParam param) {

        if(validToInactivate(param)) {
            return null;
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(null, "INACTIVO", "CONTRATO");

        Descuento descuento = find(param.getId());
        descuento.setIdEstado(estado.getId());
        edit(descuento);

        return descuento;
    }
    
    @Override
    public Descuento activar(DescuentoParam param) {

        if(validToActivate(param)) {
            return null;
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(null, "ACTIVO", "CONTRATO");

        Descuento descuento = find(param.getId());
        descuento.setIdEstado(estado.getId());
        edit(descuento);

        return descuento;
    }
}
