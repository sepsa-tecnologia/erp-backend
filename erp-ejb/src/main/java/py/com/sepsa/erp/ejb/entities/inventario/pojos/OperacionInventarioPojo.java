/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class OperacionInventarioPojo {

    public OperacionInventarioPojo(Integer id, Integer idEmpresa,
            String empresa, Integer idUsuario,
            String usuario, Integer idTipoOperacion, String tipoOperacion,
            String codigoTipoOperacion, Integer idMotivo, String motivo,
            String codigoMotivo, Integer idEstado, String estado,
            String codigoEstado, Date fechaInsercion, Integer idOrdenCompra) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idTipoOperacion = idTipoOperacion;
        this.tipoOperacion = tipoOperacion;
        this.codigoTipoOperacion = codigoTipoOperacion;
        this.idMotivo = idMotivo;
        this.motivo = motivo;
        this.codigoMotivo = codigoMotivo;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.fechaInsercion = fechaInsercion;
        this.idOrdenCompra = idOrdenCompra;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    private String usuario;
    
    /**
     * Identidicador de tipo de operación
     */
    private Integer idTipoOperacion;

    /**
     * Tipo de operación
     */
    private String tipoOperacion;

    /**
     * Código de tipo operación
     */
    private String codigoTipoOperacion;

    /**
     * Identificador de motivo
     */
    private Integer idMotivo;

    /**
     * Motivo
     */
    private String motivo;

    /**
     * Código de motivo
     */
    private String codigoMotivo;

    /**
     * Identificador de estado
     */
    private Integer idEstado;

    /**
     * Estado
     */
    private String estado;

    /**
     * Código de estado
     */
    private String codigoEstado;

    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;

    /**
     * Identificador de orden de compra
     */
    private Integer idOrdenCompra;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getCodigoTipoOperacion() {
        return codigoTipoOperacion;
    }

    public void setCodigoTipoOperacion(String codigoTipoOperacion) {
        this.codigoTipoOperacion = codigoTipoOperacion;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEmpresa() {
        return empresa;
    }
}
