/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class OrdenCompraDetalleInventarioDetalleParam extends CommonParam {

    public OrdenCompraDetalleInventarioDetalleParam() {
    }

    public OrdenCompraDetalleInventarioDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de inventario detalle
     */
    @QueryParam("idInventarioDetalle")
    private Integer idInventarioDetalle;
    
    /**
     * Identificador de orden de compra detalle
     */
    @QueryParam("idOrdenCompraDetalle")
    private Integer idOrdenCompraDetalle;
    
    /**
     * Identificador de orden de compra
     */
    @QueryParam("idOrdenCompra")
    private Integer idOrdenCompra;
    
    /**
     * Identificador de estado inventario
     */
    @QueryParam("idEstadoInventario")
    private Integer idEstadoInventario;
    
    /**
     * Código de estado inventario
     */
    @QueryParam("codigoEstadoInventario")
    private String codigoEstadoInventario;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de deposito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;
    
    /**
     * Fecha de vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;
    
    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private Integer cantidad;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idInventarioDetalle) && (isNull(idProducto)
                || isNull(idDepositoLogistico)
                || (isNull(idEstadoInventario) && isNullOrEmpty(codigoEstadoInventario))
                )) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de inventario detalle"));
        }
        
        if(isNull(idOrdenCompraDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra detalle"));
        }
        
        if(isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idInventarioDetalle) && (isNull(idProducto)
                || isNull(idDepositoLogistico)
                || (isNull(idEstadoInventario) && isNullOrEmpty(codigoEstadoInventario))
                )) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de inventario detalle"));
        }
        
        if(isNull(idOrdenCompraDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra detalle"));
        }
        
        if(isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToGetAvailable() {

        limpiarErrores();

        if (isNull(idOrdenCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de orden de compra"));
        }

        return !tieneErrores();
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdInventarioDetalle() {
        return idInventarioDetalle;
    }

    public void setIdInventarioDetalle(Integer idInventarioDetalle) {
        this.idInventarioDetalle = idInventarioDetalle;
    }

    public Integer getIdOrdenCompraDetalle() {
        return idOrdenCompraDetalle;
    }

    public void setIdOrdenCompraDetalle(Integer idOrdenCompraDetalle) {
        this.idOrdenCompraDetalle = idOrdenCompraDetalle;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }
}
