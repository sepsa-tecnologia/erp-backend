/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contacto email
 *
 * @author Jonathan
 */
public class PersonaEmailParam extends CommonParam {

    public PersonaEmailParam() {
    }

    public PersonaEmailParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Identificador de persona contacto
     */
    @QueryParam("idPersonaContacto")
    private Integer idPersonaContacto;

    /**
     * Identificador de cargo
     */
    @QueryParam("idCargo")
    private Integer idCargo;

    /**
     * Identificador de tipo de contacto
     */
    @QueryParam("idTipoContacto")
    private Integer idTipoContacto;

    /**
     * Identificador de contacto
     */
    @QueryParam("idContacto")
    private Integer idContacto;

    /**
     * Identificador de email
     */
    @QueryParam("idEmail")
    private Integer idEmail;

    /**
     * Contacto
     */
    @QueryParam("contacto")
    private String contacto;

    /**
     * Estado contacto
     */
    @QueryParam("contactoActivo")
    private Character contactoActivo;
    
    private EmailParam email;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona"));
        }

        if (isNull(idEmail) && isNull(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si esta activo"));
        }

        if(!isNull(email)) {
            email.isValidToCreate();
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona"));
        }

        if (isNull(idEmail)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de email"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere si esta activo"));
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(email)) {
            list.addAll(email.getErrores());
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public void setEmail(EmailParam email) {
        this.email = email;
    }

    public EmailParam getEmail() {
        return email;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public void setContactoActivo(Character contactoActivo) {
        this.contactoActivo = contactoActivo;
    }

    public Character getContactoActivo() {
        return contactoActivo;
    }

    public void setIdPersonaContacto(Integer idPersonaContacto) {
        this.idPersonaContacto = idPersonaContacto;
    }

    public Integer getIdPersonaContacto() {
        return idPersonaContacto;
    }
}
