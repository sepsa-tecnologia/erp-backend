/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class TipoCambioPojo {

    public TipoCambioPojo(Integer id, Integer idMoneda, String codigoMoneda,
            String moneda, Date fechaInsercion, BigDecimal compra,
            BigDecimal venta, Integer idEmpresa, Character activo) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.codigoMoneda = codigoMoneda;
        this.moneda = moneda;
        this.fechaInsercion = fechaInsercion;
        this.compra = compra;
        this.venta = venta;
        this.idEmpresa = idEmpresa;
        this.activo = activo;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Código de moneda
     */
    private String codigoMoneda;
    
    /**
     * Moneda
     */
    private String moneda;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;
    
    /**
     * Compra
     */
    private BigDecimal compra;
    
    /**
     * Venta
     */
    private BigDecimal venta;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Activo
     */
    private Character activo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public BigDecimal getCompra() {
        return compra;
    }

    public void setCompra(BigDecimal compra) {
        this.compra = compra;
    }

    public BigDecimal getVenta() {
        return venta;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }
}
