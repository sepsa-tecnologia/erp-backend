/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.MotivoDescuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MotivoDescuentoParam;
import py.com.sepsa.erp.ejb.entities.info.TipoEtiqueta;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "MotivoDescuentoFacade", mappedName = "MotivoDescuentoFacade")
@Local(MotivoDescuentoFacade.class)
public class MotivoDescuentoFacadeImpl extends FacadeImpl<MotivoDescuento, MotivoDescuentoParam> implements MotivoDescuentoFacade {

    /**
     * Facades
     */
    @Inject
    private Facades facades;
    
    public MotivoDescuentoFacadeImpl() {
        super(MotivoDescuento.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(MotivoDescuentoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoEtiqueta tipoEtiqueta = param.getIdTipoEtiqueta() == null
                ? null
                : facades.getTipoEtiquetaFacade()
                        .find(param.getIdTipoEtiqueta());
        
        if(tipoEtiqueta == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de etiqueta"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de MotivoDocumento
     * @param param parámetros
     * @return MotivoDescuento
     */
    public MotivoDescuento create(MotivoDescuentoParam param) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MotivoDescuento motivoDescuento = new MotivoDescuento();
        motivoDescuento.setDescripcion(param.getDescripcion().trim());
        motivoDescuento.setActivo(param.getActivo());
        motivoDescuento.setPorcentual(param.getPorcentual());
        motivoDescuento.setRangoDesde(param.getRangoDesde());
        motivoDescuento.setRangoHasta(param.getRangoHasta());
        motivoDescuento.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        motivoDescuento.setFechaDesde(param.getFechaDesde());
        motivoDescuento.setFechaHasta(param.getFechaHasta());
        
        create(motivoDescuento);
        
        return motivoDescuento;
    }
    
    @Override
    public List<MotivoDescuento> find(MotivoDescuentoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MotivoDescuento.class);
        Root<MotivoDescuento> root = cq.from(MotivoDescuento.class);
        Join<MotivoDescuento, TipoEtiqueta> tipoEtiqueta = root.join("tipoEtiqueta", JoinType.LEFT);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getIdTipoEtiqueta() != null) {
            predList.add(qb.equal(tipoEtiqueta.get("id"), param.getIdTipoEtiqueta()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<MotivoDescuento> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(MotivoDescuentoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MotivoDescuento.class);
        Root<MotivoDescuento> root = cq.from(MotivoDescuento.class);
        Join<MotivoDescuento, TipoEtiqueta> tipoEtiqueta = root.join("tipoEtiqueta", JoinType.LEFT);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoEtiqueta() != null) {
            predList.add(qb.equal(tipoEtiqueta.get("id"), param.getIdTipoEtiqueta()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
