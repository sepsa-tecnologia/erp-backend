/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de nota de crédito compra detalle
 * @author Jonathan D. Bernal Fernández
 */
public class NotaCreditoCompraDetalleParam extends CommonParam {
    
    public NotaCreditoCompraDetalleParam() {
    }

    public NotaCreditoCompraDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de nota de crédito de compra
     */
    @QueryParam("idNotaCreditoCompra")
    private Integer idNotaCreditoCompra;
    
    /**
     * Identificador de factura de compra
     */
    @QueryParam("idFacturaCompra")
    private Integer idFacturaCompra;
    
    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;
    
    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;
    
    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private Integer cantidad;

    /**
     * Precio unitario con IVA
     */
    @QueryParam("precioUnitarioConIva")
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin IVA
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;
    
    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Año y mes
     */
    @QueryParam("anhoMes")
    private String anhoMes;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idNotaCreditoCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito de compra"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador nro de linea"));
        }
        
        if(isNull(idFacturaCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura de compra"));
        }
        
        if(isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }
        
        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }
        
        if(isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de IVA"));
        }
        
        if(isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdNotaCreditoCompra(Integer idNotaCreditoCompra) {
        this.idNotaCreditoCompra = idNotaCreditoCompra;
    }

    public Integer getIdNotaCreditoCompra() {
        return idNotaCreditoCompra;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setAnhoMes(String anhoMes) {
        this.anhoMes = anhoMes;
    }

    public String getAnhoMes() {
        return anhoMes;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }
}
