/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Cargo;
import py.com.sepsa.erp.ejb.entities.info.Contacto;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.PersonaContacto;
import py.com.sepsa.erp.ejb.entities.info.TipoContacto;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaContactoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaContactoFacade", mappedName = "PersonaContactoFacade")
@Local(PersonaContactoFacade.class)
public class PersonaContactoFacadeImpl extends FacadeImpl<PersonaContacto, PersonaContactoParam> implements PersonaContactoFacade {

    public PersonaContactoFacadeImpl() {
        super(PersonaContacto.class);
    }
    
    public Boolean validToCreate(PersonaContactoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }
        
        if(param.getIdContacto() != null) {
            
            Contacto contacto = facades.getContactoFacade().find(param.getIdContacto());
            
            if(contacto == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el contacto"));
            }
        }
        
        if(param.getContacto() != null) {
            facades.getContactoFacade().validToCreate(param.getContacto(), true);
        }
        
        return !param.tieneErrores();
    }

    @Override
    public PersonaContacto create(PersonaContactoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Integer idContacto = param.getIdContacto();
        
        if(param.getContacto() != null) {
            Contacto contacto = facades.getContactoFacade().create(param.getContacto(), userInfo);
            idContacto = contacto == null ? idContacto : contacto.getIdContacto();
        }
        
        PersonaContacto result = new PersonaContacto();
        result.setIdPersona(param.getIdPersona());
        result.setIdContacto(idContacto);
        result.setActivo(param.getActivo());
        
        create(result);
        
        return result;
    }
    
    /**
     * Obtiene la lista de persona contacto
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public List<PersonaContacto> find(PersonaContactoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(PersonaContacto.class);
        Root<PersonaContacto> root = cq.from(PersonaContacto.class);
        Join<PersonaContacto, Persona> persona = root.join("persona");
        Join<PersonaContacto, Contacto> contacto = root.join("contacto");
        Join<Contacto, TipoContacto> tipoContacto = contacto.join("tipoContacto");
        Join<Contacto, Cargo> cargo = contacto.join("cargo");
        Join<Contacto, Cargo> pc = contacto.join("persona");
        
        cq.select(root);
        
        cq.orderBy(qb.asc(contacto.get("activo")),
                qb.asc(root.get("idContacto")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }

        if(param.getPersona() != null && !param.getPersona().trim().isEmpty()) {
            predList.add(qb.like(qb.coalesce(persona.<String>get("razonSocial"), qb.concat(qb.concat(persona.<String>get("nombre"), " "), persona.<String>get("apellido"))), String.format("%%%s%%", param.getPersona().trim())));
        }
        
        if (param.getIdContacto() != null) {
            predList.add(qb.equal(root.get("idContacto"), param.getIdContacto()));
        }
        
        if(param.getNombreContacto()!= null && !param.getNombreContacto().trim().isEmpty()) {
            predList.add(qb.like(qb.coalesce(pc.<String>get("razonSocial"), qb.concat(qb.concat(pc.<String>get("nombre"), " "), pc.<String>get("apellido"))), String.format("%%%s%%", param.getNombreContacto().trim())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(contacto.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoContacto() != null) {
            predList.add(qb.equal(tipoContacto.get("id"), param.getIdTipoContacto()));
        }

        if (param.getIdCargo() != null) {
            predList.add(qb.equal(cargo.get("id"), param.getIdCargo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<PersonaContacto> list = q.getResultList();

        return list;
    }

    /**
     * Obtiene el tamaño de la lista de persona contacto
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(PersonaContactoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        
        Root<PersonaContacto> root = cq.from(PersonaContacto.class);
        Join<PersonaContacto, Persona> persona = root.join("persona");
        Join<PersonaContacto, Contacto> contacto = root.join("contacto");
        Join<Contacto, TipoContacto> tipoContacto = contacto.join("tipoContacto");
        Join<Contacto, Cargo> cargo = contacto.join("cargo");
        Join<Contacto, Cargo> pc = contacto.join("persona");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }

        if(param.getPersona() != null && !param.getPersona().trim().isEmpty()) {
            predList.add(qb.like(qb.coalesce(persona.<String>get("razonSocial"), qb.concat(qb.concat(persona.<String>get("nombre"), " "), persona.<String>get("apellido"))), String.format("%%%s%%", param.getPersona().trim())));
        }
        
        if (param.getIdContacto() != null) {
            predList.add(qb.equal(root.get("idContacto"), param.getIdContacto()));
        }
        
        if(param.getNombreContacto() != null && !param.getNombreContacto().trim().isEmpty()) {
            predList.add(qb.like(qb.coalesce(pc.<String>get("razonSocial"), qb.concat(qb.concat(pc.<String>get("nombre"), " "), pc.<String>get("apellido"))), String.format("%%%s%%", param.getNombreContacto().trim())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(contacto.get("activo"), param.getActivo()));
        }

        if (param.getIdTipoContacto() != null) {
            predList.add(qb.equal(tipoContacto.get("id"), param.getIdTipoContacto()));
        }

        if (param.getIdCargo() != null) {
            predList.add(qb.equal(cargo.get("id"), param.getIdCargo()));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Long result = ((Long) q.getSingleResult()).longValue();

        return result;
    }
}
