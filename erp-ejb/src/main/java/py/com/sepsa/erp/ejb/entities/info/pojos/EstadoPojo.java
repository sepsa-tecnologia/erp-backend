/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class EstadoPojo {

    public EstadoPojo(Integer id, String descripcion, String codigo, Character activo) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.activo = activo;
    }

    public EstadoPojo(Integer id, String descripcion, String codigo, Character activo, Integer idTipoEstado, String codigoTipoEstado, String tipoEstado) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.activo = activo;
        this.idTipoEstado = idTipoEstado;
        this.codigoTipoEstado = codigoTipoEstado;
        this.tipoEstado = tipoEstado;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código
     */
    private String codigo;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Identificador de tipo de estado
     */
    private Integer idTipoEstado;
    
    /**
     * Código de tipo de estado
     */
    private String codigoTipoEstado;
    
    /**
     * Tipo estado
     */
    private String tipoEstado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getIdTipoEstado() {
        return idTipoEstado;
    }

    public void setIdTipoEstado(Integer idTipoEstado) {
        this.idTipoEstado = idTipoEstado;
    }

    public String getCodigoTipoEstado() {
        return codigoTipoEstado;
    }

    public void setCodigoTipoEstado(String codigoTipoEstado) {
        this.codigoTipoEstado = codigoTipoEstado;
    }

    public String getTipoEstado() {
        return tipoEstado;
    }

    public void setTipoEstado(String tipoEstado) {
        this.tipoEstado = tipoEstado;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }
}
