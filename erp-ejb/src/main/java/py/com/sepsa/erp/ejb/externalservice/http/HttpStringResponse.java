package py.com.sepsa.erp.ejb.externalservice.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;

/**
 * Respuesta HTTP en formato String
 * @author Daniel F. Escauriza Arza
 */
@Log4j2
public class HttpStringResponse extends HttpResponse<String> {
    
    /**
     * Crea una instancia de StringResponse
     * @param httpConn Conexión HTTP
     * @return Respuesta HTTP
     */
    public static HttpStringResponse createInstance(HttpURLConnection httpConn) {
        
        //Respuesta genérica en caso de falla
        HttpStringResponse response = new HttpStringResponse();
        
        try {
            
            if(httpConn != null) {
                
                ResponseCode code = ResponseCode.OK;
                
                log.info(String.format("Código: %s ", httpConn.getResponseCode()));
                
                if(httpConn.getResponseCode() == 401) {
                    code = ResponseCode.UNAUTHORIZED;
                } else if(httpConn.getResponseCode() == 403) {
                    code = ResponseCode.FORBIDDEN;
                } else if(httpConn.getResponseCode() != 200) {
                    code = ResponseCode.ERROR;
                }
                
                if(httpConn.getInputStream() != null) {
                    
                    response = new HttpStringResponse(httpConn.getInputStream(), 
                            code, httpConn.getContentType(),
                            httpConn.getHeaderFields());

                    String respData = response.getPayload().length() > 1500 
                            ? "Respuesta muy larga para log" 
                            : response.getPayload();
                    
                    log.info(String.format("Respuesta: %s ", respData));
                }

                httpConn.disconnect();
            }
            
        } catch(IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }
        
        return response;
    }
    
    /**
     * Constructor de StringResponse
     * @param input Entrada de la respuesta
     * @param code Código de respuesta
     * @param contentType Tipo de contenido de respuesta
     */
    public HttpStringResponse(InputStream input, ResponseCode code,
            String contentType) {
        
        super(code, contentType);
        
        try {
            BufferedReader responseBuffer = new BufferedReader(
                    new InputStreamReader(input, "UTF-8"));
            
            String aux;
            String resp = "";
            
            while((aux = responseBuffer.readLine()) != null) {
                resp += aux;
            }
            
            this.payload = resp;
            
        } catch (IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }
        
    }
    
    /**
     * Constructor de StringResponse
     * @param input Entrada de la respuesta
     * @param code Código de respuesta
     * @param contentType Tipo de contenido de respuesta
     * @param headers Lista de cabeceras
     */
    public HttpStringResponse(InputStream input, ResponseCode code,
            String contentType, Map<String, List<String>> headers) {
        
        super(code, contentType, headers);
        
        try {
            BufferedReader responseBuffer = new BufferedReader(
                    new InputStreamReader(input, "UTF-8"));
            
            String aux;
            String resp = "";
            
            while((aux = responseBuffer.readLine()) != null) {
                resp += aux;
            }
            
            this.payload = resp;
            
        } catch (IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }
        
    }

    /**
     * Constructor de HttpStringResponse
     * @param payload Datos de entrada
     * @param contentType Tipo de contenido
     */
    public HttpStringResponse(String payload, String contentType) {
        super(ResponseCode.OK, payload, contentType);
    }

    /**
     * Constructor de HttpStringResponse
     * @param respCode Código de Respuesta
     * @param contentType Tipo de contenido
     */
    public HttpStringResponse(ResponseCode respCode, String contentType) {
        super(respCode, contentType);
    }
    
    /**
     * Constructor de HttpStringResponse
     */
    public HttpStringResponse() {
        super();
    }
    
}
