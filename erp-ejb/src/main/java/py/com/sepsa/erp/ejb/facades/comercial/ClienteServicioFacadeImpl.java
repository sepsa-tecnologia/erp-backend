/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.ClienteServicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteServicioParam;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ClienteServicioPojo;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ReporteClienteServicioPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ClienteServicioFacade", mappedName = "ClienteServicioFacade")
@Local(ClienteServicioFacade.class)
public class ClienteServicioFacadeImpl extends FacadeImpl<ClienteServicio, ClienteServicioParam> implements ClienteServicioFacade {

    public ClienteServicioFacadeImpl() {
        super(ClienteServicio.class);
    }

    @Override
    public Map<String, String> getMapConditions(ClienteServicio item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id_cliente", item.getClienteServicioPK().getIdCliente() + "");
        map.put("id_servicio", item.getClienteServicioPK().getIdServicio() + "");
        map.put("id_estado", item.getIdEstado() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(ClienteServicio item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id_cliente", item.getClienteServicioPK().getIdCliente() + "");
        pk.put("id_servicio", item.getClienteServicioPK().getIdServicio() + "");
        
        return pk;
    }
    
    /**
     * Obtiene la lista de servicios relacionados al cliente
     * @param idCliente Identificador de cliente
     * @param idProducto Identificador de producto
     * @param idServicio Identificador de servicio
     * @return Lista
     */
    @Override
    public List<ClienteServicioPojo> asociado(Integer idCliente,
            Integer idProducto, Integer idServicio) {
        
        String where = "true";
        String where2 = "true";
        
        if(idCliente != null) {
            where = String.format(" %s and cs.id_cliente = %d", where, idCliente);
        }
        
        if(idProducto != null) {
            where2 = String.format(" %s and p.id = %d", where2, idProducto);
        }
        
        if(idServicio != null) {
            where2 = String.format(" %s and s.id = %d", where2, idServicio);
        }
        
        String sql = String.format("with cs as (select *"
                + " from comercial.cliente_servicio cs"
                + " where %s)"
                + " select p.id as id_producto, p.descripcion as producto,"
                + " s.id as id_servicio, s.descripcion as servicio,"
                + " cs is not null and cs.estado = 'A' as tiene,"
                + " cs.id_cliente,"
                + " coalesce(cs.estado, 'I') as estado"
                + " from cs right join comercial.servicio s on s.id = cs.id_servicio"
                + " join comercial.producto p on p.id = s.id_producto"
                + " where %s",
                where, where2);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<ClienteServicioPojo> result = new ArrayList<>();
        
        for (Object[] object : list) {
            
            ClienteServicioPojo pojo = new ClienteServicioPojo();
            pojo.setIdProducto((Integer)object[0]);
            pojo.setProducto((String)object[1]);
            pojo.setIdServicio((Integer)object[2]);
            pojo.setServicio((String)object[3]);
            pojo.setTiene((Boolean)object[4]);
            pojo.setIdCliente((Integer)object[5]);
            pojo.setEstado((Character)object[6]);
            
            result.add(pojo);
        }
        
        return result;
    }
    
    @Override
    public List<ReporteClienteServicioPojo> findReporte(ClienteServicioParam param) {

        String where = "c.activo = 'A'";
        
        if(param.getIdProducto() != null) {
            where = String.format("%s and p.id = %d", where, param.getIdProducto());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and s.id = %d", where, param.getIdServicio());
        }
        
        if(param.getIdTipoNegocio() != null) {
            where = String.format("%s and s.id = %d", where, param.getIdTipoNegocio());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format("%s and cli.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and cli.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdComercial() != null) {
            where = String.format("%s and cli.id_comercial = %d", where, param.getIdComercial());
        }
        
        String sql = String.format("select distinct cli.id_cliente, cli.razon_social, cli.nro_documento, cli.id_estado,\n"
                + "tt.id as id_tipo_tarifa, tt.descripcion as tipo_tarifa,\n"
                + "p.id as id_producto, p.descripcion as producto,\n"
                + "s.id as id_servicio, s.descripcion as servicio,\n"
                + "com.id as id_comercial, coalesce(com.razon_social, com.nombre || ' ' || com.apellido) as comercial\n"
                + "from comercial.contrato c\n"
                + "left join comercial.contrato_servicio cs on cs.id_contrato = c.id\n"
                + "left join comercial.tipo_tarifa tt on tt.id = c.id_tipo_tarifa\n"
                + "left join comercial.producto p on p.id = c.id_producto\n"
                + "left join comercial.servicio s on s.id = cs.id_servicio\n"
                + "left join comercial.cliente_tipo_negocio ctn on ctn.id_cliente = c.id_cliente\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join info.persona com on com.id = cli.id_comercial\n"
                + "where %s order by cli.razon_social offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<ReporteClienteServicioPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            ReporteClienteServicioPojo item = new ReporteClienteServicioPojo();
            item.setIdCliente((Integer)objects[0]);
            item.setCliente((String)objects[1]);
            item.setNroDocumento((String)objects[2]);
            item.setIdEstado((Integer)objects[3]);
            item.setIdTipoTarifa((Integer)objects[4]);
            item.setTipoTarifa((String)objects[5]);
            item.setIdProducto((Integer)objects[6]);
            item.setProducto((String)objects[7]);
            item.setIdServicio((Integer)objects[8]);
            item.setServicio((String)objects[9]);
            item.setIdComercial((Integer)objects[10]);
            item.setComercial((String)objects[11]);
            result.add(item);
        }
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de cliente servicio
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findReporteSize(ClienteServicioParam param) {

        String where = "c.activo = 'A'";
        
        if(param.getIdProducto() != null) {
            where = String.format("%s and p.id = %d", where, param.getIdProducto());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and s.id = %d", where, param.getIdServicio());
        }
        
        if(param.getIdTipoNegocio() != null) {
            where = String.format("%s and s.id = %d", where, param.getIdTipoNegocio());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format("%s and cli.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and cli.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdComercial() != null) {
            where = String.format("%s and cli.id_comercial = %d", where, param.getIdComercial());
        }
        
        String sql = String.format("select count(distinct (cli.id_cliente, tt.id, p.id, s.id, com.id))\n"
                + "from comercial.contrato c\n"
                + "left join comercial.contrato_servicio cs on cs.id_contrato = c.id\n"
                + "left join comercial.tipo_tarifa tt on tt.id = c.id_tipo_tarifa\n"
                + "left join comercial.producto p on p.id = c.id_producto\n"
                + "left join comercial.servicio s on s.id = cs.id_servicio\n"
                + "left join comercial.cliente_tipo_negocio ctn on ctn.id_cliente = c.id_cliente\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join info.persona com on com.id = cli.id_comercial\n"
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
