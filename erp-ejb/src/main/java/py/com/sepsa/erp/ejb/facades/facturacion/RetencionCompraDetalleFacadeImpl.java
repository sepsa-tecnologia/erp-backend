/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "RetencionCompraDetalleFacade", mappedName = "RetencionCompraDetalleFacade")
@Local(RetencionCompraDetalleFacade.class)
public class RetencionCompraDetalleFacadeImpl extends FacadeImpl<RetencionCompraDetalle, RetencionCompraDetalleParam> implements RetencionCompraDetalleFacade {

    public RetencionCompraDetalleFacadeImpl() {
        super(RetencionCompraDetalle.class);
    }

    @Override
    public Boolean validToCreate(RetencionCompraDetalleParam param, Boolean validarIdRetencion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        RetencionCompra retencion = facades.getRetencionCompraFacade().find(param.getIdRetencionCompra());
        
        if(validarIdRetencion && retencion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la retención de compra"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "RETENCION_DETALLE");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        FacturaCompra factura = facades.getFacturaCompraFacade().find(param.getIdFacturaCompra());
        
        if(factura == null) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura de compra"));
            
        } else {
            if(factura.getFecha().after(param.getFecha())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la retención no puede ser anterior a la factura"));
            }

            if(!factura.getMontoTotalFactura().equals(param.getMontoTotal())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto total de la factura no corresponde con lo informado"));
            }

            if(!factura.getMontoIvaTotal().equals(param.getMontoIva())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto IVA total de la factura no corresponde con lo informado"));
            }

            if(!factura.getMontoImponibleTotal().equals(param.getMontoImponible())) {
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto imponible total de la factura no corresponde con lo informado"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public RetencionCompraDetalle create(RetencionCompraDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, Boolean.TRUE)) {
            return null;
        }
        
        RetencionCompraDetalle retencionDetalle = new RetencionCompraDetalle();
        retencionDetalle.setIdRetencionCompra(param.getIdRetencionCompra());
        retencionDetalle.setIdFacturaCompra(param.getIdFacturaCompra());
        retencionDetalle.setMontoImponible(param.getMontoImponible());
        retencionDetalle.setMontoIva(param.getMontoIva());
        retencionDetalle.setMontoTotal(param.getMontoTotal());
        retencionDetalle.setMontoRetenido(param.getMontoRetenido());
        retencionDetalle.setIdEstado(param.getIdEstado());
        create(retencionDetalle);
        
        return retencionDetalle;
    }

    @Override
    public List<RetencionCompraDetalle> find(RetencionCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(RetencionCompraDetalle.class);
        Root<RetencionCompraDetalle> root = cq.from(RetencionCompraDetalle.class);
                        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdRetencionCompra() != null) {
            predList.add(qb.equal(root.get("idRetencionCompra"), param.getIdRetencionCompra()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(root.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<RetencionCompraDetalle> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(RetencionCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<RetencionCompraDetalle> root = cq.from(RetencionCompraDetalle.class);
                        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdRetencionCompra() != null) {
            predList.add(qb.equal(root.get("idRetencionCompra"), param.getIdRetencionCompra()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(root.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
