/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaDetallePojo;
import py.com.sepsa.erp.ejb.entities.info.Metrica;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FacturaDetalleFacade", mappedName = "FacturaDetalleFacade")
@Local(FacturaDetalleFacade.class)
public class FacturaDetalleFacadeImpl extends FacadeImpl<FacturaDetalle, FacturaDetalleParam> implements FacturaDetalleFacade {

    public FacturaDetalleFacadeImpl() {
        super(FacturaDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdFactura validar identificador de factura
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(FacturaDetalleParam param, boolean validarIdFactura) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        FacturaParam param1 = new FacturaParam();
        param1.setId(param.getIdFactura());
        Long size = facades.getFacturaFacade().findSize(param1);
        
        if(validarIdFactura && size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }
        
        if(param.getIdServicio() != null) {
            
            Servicio servicio = facades.getServicioFacade().find(param.getIdServicio());
            
            if(servicio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el servicio"));
            }
        }
        
        if(param.getIdProducto() != null) {
            
            ProductoParam param2 = new ProductoParam();
            param2.setId(param.getIdProducto());
            ProductoPojo producto = facades.getProductoFacade().findFirstPojo(param2);
            
            if(producto == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el producto"));
            } else {
                validarCantidad(param, producto, Integer.SIZE);
            }
        }
        
        if(param.getIdLiquidacion() != null) {
            
            Liquidacion liquidacion = facades.getLiquidacionFacade().find(param.getIdLiquidacion());
            
            if(liquidacion == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No exista la liquidación"));
            }
        }
        
        return !param.tieneErrores();
    }

    private void validarCantidad(CommonParam param, ProductoPojo producto, Integer cantidad) {
        
        BigDecimal umv = producto.getUmv() == null
                ? BigDecimal.ZERO
                : producto.getUmv().stripTrailingZeros();

        BigDecimal multiploMmv = producto.getMultiploUmv() == null
                ? BigDecimal.ZERO
                : producto.getMultiploUmv().stripTrailingZeros();
        
        BigDecimal verificar = new BigDecimal(cantidad);
        
        BigDecimal temp = verificar.subtract(umv).stripTrailingZeros();
        
        if(temp.compareTo(BigDecimal.ZERO) < 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion(String.format("La unidad minima de venta del producto %s es %d",
                            producto.getDescripcion(), umv)));
        } else {
            
            temp = multiploMmv.compareTo(BigDecimal.ZERO) == 0
                    ? BigDecimal.ZERO
                    : temp.remainder(multiploMmv).stripTrailingZeros();
            
            if(temp.compareTo(BigDecimal.ZERO) != 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion(String.format("La cantidad solicitada del producto %s debe ser múltiplo de %d",
                                producto.getDescripcion(), multiploMmv)));
            }
        }
    }
    
    @Override
    public FacturaDetalle create(FacturaDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        FacturaDetalle item = new FacturaDetalle();
        item.setIdFactura(param.getIdFactura());
        item.setNroLinea(param.getNroLinea());
        item.setDescripcion(param.getDescripcion());
        item.setDatoAdicional(param.getDatoAdicional());
        item.setPorcentajeIva(param.getPorcentajeIva());
        item.setPorcentajeGravada(param.getPorcentajeGravada());
        item.setCantidad(param.getCantidad());
        item.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva().stripTrailingZeros());
        item.setPrecioUnitarioSinIva(param.getPrecioUnitarioSinIva().stripTrailingZeros());
        item.setDescuentoParticularUnitario(param.getDescuentoParticularUnitario().stripTrailingZeros());
        item.setDescuentoGlobalUnitario(param.getDescuentoGlobalUnitario().stripTrailingZeros());
        item.setMontoDescuentoParticular(param.getMontoDescuentoParticular().stripTrailingZeros());
        item.setMontoDescuentoGlobal(param.getMontoDescuentoGlobal().stripTrailingZeros());
        item.setMontoIva(param.getMontoIva().stripTrailingZeros());
        item.setMontoImponible(param.getMontoImponible().stripTrailingZeros());
        item.setMontoExentoGravado(param.getMontoExentoGravado().stripTrailingZeros());
        item.setMontoTotal(param.getMontoTotal().stripTrailingZeros());
        item.setIdLiquidacion(param.getIdLiquidacion());
        item.setIdServicio(param.getIdServicio());
        item.setIdProducto(param.getIdProducto());
        item.setCodDncpNivelGeneral(param.getCodDncpNivelGeneral());
        item.setCodDncpNivelEspecifico(param.getCodDncpNivelEspecifico());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<FacturaDetallePojo> findPojo(FacturaDetalleParam param) {

        AbstractFind find = new AbstractFind(FacturaDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idFactura"),
                        getPath("root").get("nroLinea"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codDncpNivelGeneral"),
                        getPath("root").get("codDncpNivelEspecifico"),
                        getPath("root").get("porcentajeIva"),
                        getPath("root").get("montoIva"),
                        getPath("root").get("montoImponible"),
                        getPath("root").get("montoTotal"),
                        getPath("root").get("idLiquidacion"),
                        getPath("root").get("idServicio"),
                        getPath("root").get("idProducto"),
                        getPath("producto").get("descripcion"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("codigoInterno"),
                        getPath("producto").get("fechaVencimientoLote"),
                        getPath("producto").get("nroLote"),
                        getPath("metrica").get("codigo"),
                        getPath("metrica").get("descripcion"),
                        getPath("root").get("cantidad"),
                        getPath("root").get("precioUnitarioConIva"),
                        getPath("root").get("precioUnitarioSinIva"),
                        getPath("root").get("montoDescuentoParticular"),
                        getPath("root").get("montoDescuentoGlobal"),
                        getPath("root").get("descuentoParticularUnitario"),
                        getPath("root").get("descuentoGlobalUnitario"),
                        getPath("root").get("porcentajeGravada"),
                        getPath("root").get("montoExentoGravado"),
                        getPath("root").get("datoAdicional"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<FacturaDetalle> find(FacturaDetalleParam param) {

        AbstractFind find = new AbstractFind(FacturaDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, FacturaDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<FacturaDetalle> root = cq.from(FacturaDetalle.class);
        Join<FacturaDetalle, Producto> producto = root.join("producto", JoinType.LEFT);
        Join<Producto, Metrica> metrica = producto.join("metrica",JoinType.LEFT);
        find.addPath("root", root);
        find.addPath("producto", producto);
        find.addPath("metrica", metrica);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        
        if (param.getIdLiquidacion() != null) {
            predList.add(qb.equal(root.get("idLiquidacion"), param.getIdLiquidacion()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaDetalle> root = cq.from(FacturaDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }

        if (param.getIdLiquidacion() != null) {
            predList.add(qb.equal(root.get("idLiquidacion"), param.getIdLiquidacion()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
