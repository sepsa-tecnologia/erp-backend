/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.DatoPersona;
import py.com.sepsa.erp.ejb.entities.info.DatoPersonaPK;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.TipoDatoPersona;
import py.com.sepsa.erp.ejb.entities.info.filters.DatoPersonaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "DatoPersonaFacade", mappedName = "DatoPersonaFacade")
@Local(DatoPersonaFacade.class)
public class DatoPersonaFacadeImpl extends FacadeImpl<DatoPersona, DatoPersonaParam> implements DatoPersonaFacade {

    public DatoPersonaFacadeImpl() {
        super(DatoPersona.class);
    }

    public Boolean validToCreate(DatoPersonaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        DatoPersona datoPersona = find(new DatoPersonaPK(
                param.getIdPersona(),
                param.getIdTipoDatoPersona()));
        
        if(datoPersona != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe el registro de dato persona"));
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
        
        TipoDatoPersona tipoDatoPersona = facades.getTipoDatoPersonaFacade()
                .find(param.getIdTipoDatoPersona());
        
        if(tipoDatoPersona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de dato persona"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(DatoPersonaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        DatoPersona datoPersona = find(new DatoPersonaPK(
                param.getIdPersona(),
                param.getIdTipoDatoPersona()));
        
        if(datoPersona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de dato persona"));
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
        
        TipoDatoPersona tipoDatoPersona = facades.getTipoDatoPersonaFacade()
                .find(param.getIdTipoDatoPersona());
        
        if(tipoDatoPersona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de dato persona"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public DatoPersona create(DatoPersonaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        DatoPersona item = new DatoPersona(
                param.getIdPersona(),
                param.getIdTipoDatoPersona());
        item.setValor(param.getValor().trim());
        
        create(item);
        
        return item;
    }
    
    @Override
    public DatoPersona edit(DatoPersonaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        DatoPersona item = find(new DatoPersona(
                param.getIdPersona(),
                param.getIdTipoDatoPersona()));
        item.setValor(param.getValor().trim());
        
        edit(item);
        
        return item;
    }
    
    public List<DatoPersona> getDatoPersona(Integer persId) {
        List<DatoPersona> provCanalList = new ArrayList();
        if (persId != null) {
            CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
            javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
            Root<DatoPersona> root = cq.from(DatoPersona.class);
            cq.select(root);
            //cq.orderBy(qb.asc(root.get("id")));

            Predicate pred = qb.equal(root.get("datoPersonaPK").get("idPersona"), persId);
            cq.where(pred);
            javax.persistence.Query q = getEntityManager()
                    .createQuery(cq)
                    .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            
            provCanalList = q.getResultList();
        }
        return provCanalList;
    }

    /**
     * Obtiene la lista de Dato persona
     *
     * @param idPersona
     * @param idTipoDatoPersona
     * @param valor
     * @param first
     * @param max
     * @return Usuario
     */
    public List<DatoPersona> findDatoPersona(Integer idPersona, Integer idTipoDatoPersona, String valor, Integer first, Integer max) {
        List<DatoPersona> lista = new ArrayList<>();

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<DatoPersona> root = cq.from(DatoPersona.class);
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (idPersona != null) {
            predList.add(qb.equal(root.get("datoPersonaPK").get("idPersona"), idPersona));
        }

        if (idTipoDatoPersona != null) {
            predList.add(qb.equal(root.get("datoPersonaPK").get("idTipoDatoPersona"), idTipoDatoPersona));
        }

        if (valor != null && !valor.trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("valor")), String.format("%%%s%%", valor.trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(first)
                .setMaxResults(max)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        lista = q.getResultList();

        return lista;
    }

    /**
     * Obtiene el tamaño de la consulta con la busqueda de los parametros dados
     *
     * @param idPersona
     * @param idTipoDatoPersona
     * @param valor
     * @param first
     * @param max
     * @return contactos
     */
    public Integer findSize(Integer idPersona, Integer idTipoDatoPersona, String valor, Integer first, Integer max) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<DatoPersona> root = cq.from(DatoPersona.class);
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;

        if (idPersona != null) {
            predList.add(qb.equal(root.get("datoPersonaPK").get("idPersona"), idPersona));
        }

        if (idTipoDatoPersona != null) {
            predList.add(qb.equal(root.get("datoPersonaPK").get("idTipoDatoPersona"), idTipoDatoPersona));
        }

        if (valor != null && !valor.trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("valor")), String.format("%%%s%%", valor.trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        Integer result = ((Long) q.getSingleResult()).intValue();

        return result;
    }
}
