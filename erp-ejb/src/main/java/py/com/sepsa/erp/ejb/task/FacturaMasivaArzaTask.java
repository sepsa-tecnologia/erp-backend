/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.utils.FacturaUtils;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
@Log4j2
public class FacturaMasivaArzaTask extends AbstracProcesamientoLote {

    @Resource
    private UserTransaction userTransaction;

    @Override
    public void procesar(Lote lote) {

        LoteArchivoParam laparam = new LoteArchivoParam();
        laparam.setIdLote(lote.getId());
        laparam.isValidToList();
        LoteArchivo archivo = facades.getLoteArchivoFacade().findFirst(laparam);

        Result result = convert(lote, archivo);
        if (!result.isSuccess()) {
            String resultadoBase64 = Base64.getEncoder().encodeToString(result.getResult().getBytes(StandardCharsets.UTF_8));

            LoteParam loteparam = new LoteParam();
            loteparam.setId(lote.getId());
            loteparam.setIdEmpresa(lote.getIdEmpresa());
            loteparam.setCodigoEstado("PROCESADO");
            loteparam.setNotificado(lote.getNotificado());
            loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
            loteparam.setResultado(resultadoBase64);
            
            facades.getLoteFacade().edit(loteparam, null);
            return;
        }

        FacturaParam param = new FacturaParam();
        param.setIdEmpresa(lote.getIdEmpresa());
        param.setIdUsuario(lote.getIdUsuario());
        param.setUploadedFileBytes(result.getResult().getBytes(StandardCharsets.UTF_8));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<FacturaParam> facturas = FacturaUtils.convertToList(facades, param);
        String inicio = String.format("TIMBRADO;NRO_FACTURA;FECHA;ID_FACTURA;ESTADO_RESULTADO");
        StringBuilder buffer = new StringBuilder();
        buffer.append(inicio).append("\r\n");
        for (FacturaParam fact : facturas) {
            if (fact.tieneErrores()) {
                String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                error = error.concat("%s| ");
                for (MensajePojo mp : fact.getErrores()) {
                    error = String.format(error, mp.getDescripcion());
                    error = error.concat("%s| ");
                }
                error = error.substring(0, error.length() - 5);
                error = error.concat(";;");
                buffer.append(error).append("\r\n");
            } else {

                try {
                    userTransaction.begin();

                    Factura factura = facades
                            .getFacturaFacade()
                            .create(fact, null);

                    if (factura == null) {
                        String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                        error = error.concat("| ");
                        for (MensajePojo mp : fact.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {
                        String ok = String.format("%s;%s;%s;%s;OK", fact.getTimbrado(), fact.getNroFactura(), sdf.format(fact.getFecha()), factura.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }

                    userTransaction.commit();
                } catch (Exception exception) {
                    try {
                        userTransaction.rollback();
                    } catch (Exception e) {
                        log.fatal("Error", e);
                    }
                    String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                    error = error.concat("| ");
                    error = error.concat(exception.getMessage());
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                }
            }
        }

        String resultadoBase64 = Base64.getEncoder().encodeToString(buffer.toString().getBytes(StandardCharsets.UTF_8));

        LoteParam loteparam = new LoteParam();
        loteparam.setId(lote.getId());
        loteparam.setIdEmpresa(lote.getIdEmpresa());
        loteparam.setCodigoEstado("PROCESADO");
        loteparam.setNotificado(lote.getNotificado());
        loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
        loteparam.setResultado(resultadoBase64);

        facades.getLoteFacade().edit(loteparam, null);
    }

    public Result convert(Lote lote, LoteArchivo file) {
        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(file.getContenido())));  StringWriter writer = new StringWriter()) {
            int numSheets = workbook.getNumberOfSheets();
            for (int sheetNumber = 0; sheetNumber < numSheets; sheetNumber++) {

                Sheet sheet = workbook.getSheetAt(sheetNumber);

                int rowIndex = 0;
                int linea = 1; // Contador de línea
                for (Row row : sheet) {
                    if (rowIndex < 1) { // Omitir las 2 primeras filas
                        rowIndex++;
                        continue;
                    }

                    String timbrado = getCellValueAsString(row.getCell(0));
                    String nroF = getCellValueAsString(row.getCell(1));
                    String nroFactura = "";
                    while (nroF.length() < 7) {
                        nroF = "0" + nroF;
                    }
                    String prefijoTimbrado = getSucursales(lote.getIdEmpresa(), timbrado);
                    if (prefijoTimbrado != null && !prefijoTimbrado.trim().isEmpty()) {
                        nroFactura = prefijoTimbrado + nroF;
                    } else {
                        return Result.builder()
                                .success(false)
                                .result("No se encontró un talonario activo de facturas")
                                .build();
                    }
                    String fecha = getCellValueAsString(row.getCell(2));
                    String codigoIsoMoneda = "PYG";
                    String rucCi = getCellValueAsString(row.getCell(4)).replace(",", "").replace(".", "").trim();
                    String razonSocial = Normalizer.normalize(getCellValueAsString(row.getCell(5)), Normalizer.Form.NFD)
                            .replaceAll("[^\\p{ASCII}]", "").replace(",", "").replace(";", "").trim()
                            .toUpperCase(); //Normalizamos a mayusculas y quitamos acentos
                    String codSeguridad = getCellValueAsString(row.getCell(6));
                    String tipoCambio = "";
                    String tipoFactura = getCellValueAsString(row.getCell(8));
                    String tipoCredito = getCellValueAsString(row.getCell(9));
                    String diasCredito = getCellValueAsString(row.getCell(10));
                    String cantidadCuotas = getCellValueAsString(row.getCell(11));
                    String codigoProducto1 = "001";
                    String codigoProducto2 = "LEY881";
                    String porcentajeImpuesto1 = getCellValueAsString(row.getCell(13));
                    String porcentajeImpuesto2 = "0";
                    String porcentajeGravada = "100";
                    String precioUnitarioConIva1 = calculoResto(row.getCell(17), row.getCell(16));;
                    String precioUnitarioConIva2 = getCellValueAsString(row.getCell(16));
                    String descuentoUnitario = getCellValueAsString(row.getCell(18));
                    String cantidad = getCellValueAsString(row.getCell(19));
                    String descripcion1 = getCellValueAsString(row.getCell(20)) + " " + getCellValueAsString(row.getCell(12)).replace(",", "").replace(";", "").trim();
                    String descripcion2 = "Ley 881";
                    String observacion = String.format("{\"EDICION\":\"%s\", \"ZONA\":\"%s\",\"VENCIMIENTO_PAGO_ARZA\":\"%s\"}",getCellValueAsString(row.getCell(21)), getCellValueAsString(row.getCell(23)), getCellValueAsString(row.getCell(22)));
               
                    // Primera línea
                    String linea1 = 1 + "," + timbrado + "," + nroFactura + "," + fecha + "," + codigoIsoMoneda + "," + rucCi + "," + razonSocial + "," + codSeguridad + "," + tipoCambio + "," + tipoFactura + "," + tipoCredito + "," + diasCredito + "," + cantidadCuotas;
                    writer.write(linea1 + "\n");

                    // Detalle 1
                    String linea2 = 2 + "," + timbrado + "," + nroFactura + "," + codigoProducto1 + "," + porcentajeImpuesto1 + "," + porcentajeGravada + "," + precioUnitarioConIva1 + "," + descuentoUnitario + "," + cantidad + "," + descripcion1;
                    writer.write(linea2 + "\n");
                    // Detalle 2 (Ley 811)
                    String linea3 = 2 + "," + timbrado + "," + nroFactura + "," + codigoProducto2 + "," + porcentajeImpuesto2 + "," + porcentajeGravada + "," + precioUnitarioConIva2 + "," + descuentoUnitario + "," + cantidad + "," + descripcion2;
                    writer.write(linea3 + "\n");
                    
                    // Detalle 3 Observacion/Dato Adicional
                    String linea4 = 3 + "," + timbrado + "," + nroFactura + "," + observacion;
                    writer.write(linea4 + "\n");
                    linea++;
                    rowIndex++;

                    linea++;
                    rowIndex++;
                }
            }

            return Result.builder()
                    .success(true)
                    .result(writer.toString())
                    .build();
        } catch (IOException e) {
            return Result.builder()
                    .success(true)
                    .result(e.getMessage())
                    .build();
        }
    }

    private String getSucursales(Integer idEmpresa, String timbrado) {
        String sucursales = "";

        TalonarioParam t = new TalonarioParam();
        t.setIdEmpresa(idEmpresa);
        t.setActivo('S');
        t.setTimbrado(timbrado);
        t.setIdTipoDocumento(1);
        TalonarioPojo talonario = facades.getTalonarioFacade().findFirstPojo(t);
        if (talonario != null) {
            sucursales = talonario.getNroSucursal() + "-" + talonario.getNroPuntoVenta() + "-";
        }

        return sucursales;
    }

    private static String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    return sdf.format(cell.getDateCellValue());
                } else {
                    return String.valueOf((long) cell.getNumericCellValue());
                }
            default:
                return "";
        }
    }

    private static String calculoResto(Cell total, Cell aux) {
        if (total == null || aux == null) {
            return "0";
        }
        double precio1 = total.getNumericCellValue();
        double precio2 = aux.getNumericCellValue();
        return String.valueOf((long) (precio1 - precio2));
    }

    @Data
    @Builder
    private static class Result {

        private boolean success;
        private String result;
    }

    @Data
    private class Cliente {

        private Integer idCliente;
        private String nroDocumento;
    }
}
