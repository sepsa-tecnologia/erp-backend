/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.pojos;

import java.util.List;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class UsuarioPojo {

    public UsuarioPojo() {
    }

    public UsuarioPojo(Integer id, String usuario, Integer idEstado,
            String codigoEstado, String estado, Integer idEmail,
            Integer idTelefono, String hash, Integer idPersonaFisica,
            String personaFisica, String empresa) {
        this.id = id;
        this.usuario = usuario;
        this.idEstado = idEstado;
        this.codigoEstado = codigoEstado;
        this.estado = estado;
        this.idEmail = idEmail;
        this.idTelefono = idTelefono;
        this.hash = hash;
        this.idPersonaFisica = idPersonaFisica;
        this.personaFisica = personaFisica;
        this.empresa = empresa;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Usuario
     */
    private String usuario;

    /**
     * Contraseña
     */
    private String contrasena;

    /**
     * Identificador de estado
     */
    private Integer idEstado;

    /**
     * Código de estado
     */
    private String codigoEstado;

    /**
     * Estado
     */
    private String estado;

    /**
     * Identificador de email
     */
    private Integer idEmail;

    /**
     * Identificador de telefono
     */
    private Integer idTelefono;

    /**
     * Hash
     */
    private String hash;

    /**
     * Identificador de persona fisica
     */
    private Integer idPersonaFisica;

    /**
     * Persona fisica
     */
    private String personaFisica;

    /**
     * Nombre
     */
    private String nombre;

    /**
     * Apellido
     */
    private String apellido;
    /**
     * Empresa
     */
    private String empresa;

    /**
     * Lista de usuario perfil
     */
    private List<UsuarioPerfilPojo> usuarioPerfiles;

    /**
     * Lista de usuario local
     */
    private List<UsuarioLocalPojo> usuarioLocales;

    /**
     * Lista de usuario empresa
     */
    private List<UsuarioEmpresaPojo> usuarioEmpresas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    public String getPersonaFisica() {
        return personaFisica;
    }

    public void setPersonaFisica(String personaFisica) {
        this.personaFisica = personaFisica;
    }

    public void setUsuarioPerfiles(List<UsuarioPerfilPojo> usuarioPerfiles) {
        this.usuarioPerfiles = usuarioPerfiles;
    }

    public List<UsuarioPerfilPojo> getUsuarioPerfiles() {
        return usuarioPerfiles;
    }

    public void setUsuarioLocales(List<UsuarioLocalPojo> usuarioLocales) {
        this.usuarioLocales = usuarioLocales;
    }

    public List<UsuarioLocalPojo> getUsuarioLocales() {
        return usuarioLocales;
    }

    public void setUsuarioEmpresas(List<UsuarioEmpresaPojo> usuarioEmpresas) {
        this.usuarioEmpresas = usuarioEmpresas;
    }

    public List<UsuarioEmpresaPojo> getUsuarioEmpresas() {
        return usuarioEmpresas;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido() {
        return apellido;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
