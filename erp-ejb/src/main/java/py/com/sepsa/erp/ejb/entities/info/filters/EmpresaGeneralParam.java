/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de empresa
 *
 * @author Jonathan
 */
public class EmpresaGeneralParam extends CommonParam {

    public EmpresaGeneralParam() {
    }

    public EmpresaGeneralParam(String bruto) {
        super(bruto);
    }

    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;

    /**
     * Identificador de tipo de empresa
     */
    @QueryParam("idTipoEmpresa")
    private Integer idTipoEmpresa;

    /**
     * Código de tipo de empresa
     */
    @QueryParam("codigoTipoEmpresa")
    private String codigoTipoEmpresa;

    /**
     * Identificador de cliente sepsa
     */
    @QueryParam("idClienteSepsa")
    private Integer idClienteSepsa;

    /**
     * Parámetro de Empresa
     */
    @QueryParam("empresa")
    private EmpresaParam empresa;
    /**
     * Parámetro de Persona
     */
    @QueryParam("persona")
    private PersonaParam persona;
    /**
     * Parámetro de Usuario
     */
    @QueryParam("usuario")
    private UsuarioParam usuario;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idTipoEmpresa) && isNullOrEmpty(codigoTipoEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de empresa"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción de la empresa"));
        }

        if (isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de la empresa"));
        }

        if (isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC de la empresa"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        if (isNull(idClienteSepsa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente sepsa"));
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }

        if (isNull(idTipoEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de empresa"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción de la empresa"));
        }

        if (isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de la empresa"));
        }

        if (isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC de la empresa"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        if (isNull(idClienteSepsa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente sepsa"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    public void setIdTipoEmpresa(Integer idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }

    public EmpresaParam getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaParam empresa) {
        this.empresa = empresa;
    }

    public PersonaParam getPersona() {
        return persona;
    }

    public void setPersona(PersonaParam persona) {
        this.persona = persona;
    }

    public UsuarioParam getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioParam usuario) {
        this.usuario = usuario;
    }
    
    
    
    
}
