/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.TipoTelefono;
import py.com.sepsa.erp.ejb.entities.info.filters.TelefonoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TelefonoFacade", mappedName = "TelefonoFacade")
@Local(TelefonoFacade.class)
public class TelefonoFacadeImpl extends FacadeImpl<Telefono, TelefonoParam> implements TelefonoFacade {

    public TelefonoFacadeImpl() {
        super(Telefono.class);
    }

    @Override
    public List<Telefono> find(TelefonoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Telefono> root = cq.from(Telefono.class);
        cq.select(root);
        cq.orderBy(qb.asc(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdTipoTelefono() != null) {
            predList.add(qb.equal(root.get("tipoTelefono").get("id"), param.getIdTipoTelefono()));
        }

        if (param.getPrincipal() != null) {
            predList.add(qb.equal(root.get("principal"), param.getPrincipal()));
        }

        if (param.getNumero() != null && !param.getNumero().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("numero")), String.format("%%%s%%", param.getNumero().trim().toUpperCase())));
        }

        if (param.getPrefijo() != null && !param.getPrefijo().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("prefijo")), String.format("%%%s%%", param.getPrefijo().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Telefono> telefonos = q.getResultList();

        return telefonos;
    }

    @Override
    public Long findSize(TelefonoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Telefono> root = cq.from(Telefono.class);
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdTipoTelefono() != null) {
            predList.add(qb.equal(root.get("tipoTelefono").get("id"), param.getIdTipoTelefono()));
        }

        if (param.getPrincipal() != null) {
            predList.add(qb.equal(root.get("principal"), param.getPrincipal()));
        }

        if (param.getNumero() != null && !param.getNumero().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("numero")), String.format("%%%s%%", param.getNumero().trim().toUpperCase())));
        }

        if (param.getPrefijo() != null && !param.getPrefijo().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("prefijo")), String.format("%%%s%%", param.getPrefijo().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        Long result = (Long) q.getSingleResult();

        return result;
    }
    
    @Override
    public Boolean validToCreate(TelefonoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoTelefono tipoTelefono = facades.getTipoTelefonoFacade()
                .find(param.getIdTipoTelefono(), param.getCodigoTipoTelefono());
        
        if(tipoTelefono == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de telefono"));
        } else {
            param.setIdTipoTelefono(tipoTelefono.getId());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(TelefonoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Telefono telefono = find(param.getId());
        
        if(telefono == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el telefono"));
        }
        
        TipoTelefono tipoTelefono = facades.getTipoTelefonoFacade()
                .find(param.getIdTipoTelefono(), param.getCodigoTipoTelefono());
        
        if(tipoTelefono == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de telefono"));
        } else {
            param.setIdTipoTelefono(tipoTelefono.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Telefono create(TelefonoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Telefono telefono = new Telefono();
        telefono.setIdTipoTelefono(param.getIdTipoTelefono());
        telefono.setNumero(param.getNumero());
        telefono.setPrefijo(param.getPrefijo());
        telefono.setPrincipal(param.getPrincipal());
        
        create(telefono);
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id", telefono.getId() + " ");
        atributos.put("id_tipo_telefono", param.getIdTipoTelefono() + " ");
        atributos.put("numero", param.getNumero() + " ");
        atributos.put("prefijo", param.getPrefijo() + " ");
        atributos.put("principal", param.getPrincipal() + " ");

        facades.getRegistroFacade().create("info", "telefono", atributos, userInfo);
        
        return telefono;
    }
    
    @Override
    public Telefono edit(TelefonoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Telefono telefono = find(param.getId());
        telefono.setIdTipoTelefono(param.getIdTipoTelefono());
        telefono.setNumero(param.getNumero());
        telefono.setPrefijo(param.getPrefijo());
        telefono.setPrincipal(param.getPrincipal());
        
        edit(telefono);
        
        Map<String, String> pk = new HashMap();
        pk.put("id", telefono.getId() + " ");
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id_tipo_telefono", param.getIdTipoTelefono() + " ");
        atributos.put("numero", param.getNumero() + " ");
        atributos.put("prefijo", param.getPrefijo() + " ");
        atributos.put("principal", param.getPrincipal() + " ");

        facades.getRegistroFacade().edit("info", "telefono", atributos, userInfo, pk);
        
        return telefono;
    }
}
