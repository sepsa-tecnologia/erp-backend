/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ProvOpPorCompParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.LiquidacionPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.ProvOperativoPorCompPojo;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LiquidacionFacade", mappedName = "LiquidacionFacade")
@Local(LiquidacionFacade.class)
public class LiquidacionFacadeImpl extends FacadeImpl<Liquidacion, LiquidacionParam> implements LiquidacionFacade {

    public LiquidacionFacadeImpl() {
        super(Liquidacion.class);
    }
    
    @Override
    public Liquidacion findOrCreate(LiquidacionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        LiquidacionParam param1 = new LiquidacionParam();
        param1.setIdEmpresa(userInfo.getIdEmpresa());
        param1.setIdPeriodoLiquidacion(param.getIdPeriodoLiquidacion());
        param1.setIdCliente(param.getIdCliente());
        param1.setIdMoneda(param.getIdMoneda());
        param1.setIdServicio(param.getIdServicio());
        param1.setIdContrato(param.getIdContrato());
        param1.isValidToList();
        
        List<Liquidacion> list = find(param1);
        
        Liquidacion liquidacion;
        
        if(Assertions.isNullOrEmpty(list)) {
            liquidacion = create(param, userInfo);
        } else {
            liquidacion = list.get(0);
            
            param.getHistoricoLiquidacion().setIdLiquidacion(liquidacion.getId());
        
            HistoricoLiquidacion hl = facades
                    .getHistoricoLiquidacionFacade()
                    .create(param.getHistoricoLiquidacion(), userInfo);

            liquidacion.setIdHistoricoLiquidacion(hl.getId());
            edit(liquidacion);
            refresh(liquidacion);
        }
        
        return liquidacion;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param param
     * @return Bandera
     */
    public Boolean validToCreate(LiquidacionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());
        
        if(Assertions.isNull(cliente)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "LIQUIDACION");
        
        if(Assertions.isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        if(param.getIdContrato() != null) {
            
            Contrato contrato = facades.getContratoFacade().find(param.getIdContrato());

            if(contrato == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el contrato"));
            }
        }
        
        PeriodoLiquidacion periodoLiquidacion = null;
        
        if(!Assertions.isNull(param.getIdPeriodoLiquidacion())) {
            periodoLiquidacion = facades
                    .getPeriodoLiquidacionFacade()
                    .find(param.getIdPeriodoLiquidacion());
        }
        
        if(Assertions.isNull(periodoLiquidacion) && !Assertions.isNull(param.getFechaLiquidacion())) {
            periodoLiquidacion = facades
                    .getPeriodoLiquidacionFacade()
                    .find(param.getFechaLiquidacion());
        }
        
        if(periodoLiquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el periodo liquidación"));
        } else {
            param.setIdPeriodoLiquidacion(periodoLiquidacion.getId());
        }
        
        Moneda moneda = facades.getMonedaFacade().find(
                param.getIdMoneda(),
                param.getCodigoMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
        }
        
        Servicio servicio = facades.getServicioFacade().find(
                param.getIdServicio(),
                param.getCodigoServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el servicio"));
        } else {
            param.setIdServicio(servicio.getId());
        }
        
        if(param.getHistoricoLiquidacion() != null) {
            facades.getHistoricoLiquidacionFacade()
                    .validToCreate(param.getHistoricoLiquidacion(), Boolean.FALSE);
        }
        
        return !param.tieneErrores();
        
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param param
     * @return Bandera
     */
    public Boolean validToEdit(LiquidacionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());
        
        if(Assertions.isNull(cliente)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        Estado estado = facades.getEstadoFacade().find(param.getIdEstado());
        
        if(Assertions.isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        }
        
        Liquidacion liquidacion = facades
                .getLiquidacionFacade()
                .find(param.getId());
        
        if(liquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la liquidación"));
        }
        
        if(param.getIdContrato() != null) {
            
            Contrato contrato = facades.getContratoFacade().find(param.getIdContrato());

            if(contrato == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el contrato"));
            }
        }
        
        PeriodoLiquidacion periodoLiquidacion = facades
                .getPeriodoLiquidacionFacade()
                .find(param.getIdPeriodoLiquidacion());
        
        if(periodoLiquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el periodo liquidación"));
        }
        
        Moneda moneda = facades
                .getMonedaFacade()
                .find(param.getIdMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la moneda"));
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el servicio"));
        }
        
        if(param.getIdHistoricoLiquidacion() != null) {
            
            HistoricoLiquidacion historicoLiquidacion = facades
                    .getHistoricoLiquidacionFacade()
                    .find(param.getIdHistoricoLiquidacion());
            
            if(historicoLiquidacion == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el historico liquidación"));
            }
        }
        
        return !param.tieneErrores();
        
    }
    
    /**
     * Crea una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public Liquidacion create(LiquidacionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Liquidacion liquidacion = new Liquidacion();
        liquidacion.setIdEmpresa(userInfo.getIdEmpresa());
        liquidacion.setIdCliente(param.getIdCliente());
        liquidacion.setIdContrato(param.getIdContrato());
        liquidacion.setIdMoneda(param.getIdMoneda());
        liquidacion.setIdPeriodoLiquidacion(param.getIdPeriodoLiquidacion());
        liquidacion.setIdServicio(param.getIdServicio());
        liquidacion.setIdEstado(param.getIdEstado());
        
        create(liquidacion);
        
        param.getHistoricoLiquidacion().setIdLiquidacion(liquidacion.getId());
        
        HistoricoLiquidacion hl = facades
                .getHistoricoLiquidacionFacade()
                .create(param.getHistoricoLiquidacion(), userInfo);
        
        liquidacion.setIdHistoricoLiquidacion(hl.getId());
        edit(liquidacion);
        refresh(liquidacion);
        
        return liquidacion;
    }
    
    /**
     * Edita una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public Liquidacion edit(LiquidacionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Liquidacion liquidacion = find(param.getId());
        liquidacion.setIdContrato(param.getIdContrato());
        liquidacion.setIdMoneda(param.getIdMoneda());
        liquidacion.setIdPeriodoLiquidacion(param.getIdPeriodoLiquidacion());
        liquidacion.setIdServicio(param.getIdServicio());
        liquidacion.setIdEstado(param.getIdEstado());
        liquidacion.setIdHistoricoLiquidacion(param.getIdHistoricoLiquidacion());
        
        edit(liquidacion);
        
        return liquidacion;
    }
    
    @Override
    public List<Liquidacion> find(LiquidacionParam param) {

        String where = "true";
        
        if (param.getId() != null) {
            where = String.format("%s and l.id = %d", where, param.getId());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format("%s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdHistoricoLiquidacion() != null) {
            where = String.format("%s and l.id_historico_liquidacion = %d", where, param.getIdHistoricoLiquidacion());
        }
        
        if (param.getIdMoneda() != null) {
            where = String.format("%s and l.id_moneda = %d", where, param.getIdMoneda());
        }
        
        if (param.getIdContrato() != null) {
            where = String.format("%s and l.id_contrato = %d", where, param.getIdContrato());
        }
        
        if (param.getIdCliente() != null) {
            where = String.format("%s and l.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdPeriodoLiquidacion() != null) {
            where = String.format("%s and l.id_periodo_liquidacion = %d", where, param.getIdPeriodoLiquidacion());
        }
        
        if (param.getIdProducto() != null) {
            where = String.format("%s and s.id_producto = %d", where, param.getIdProducto());
        }
        
        if (param.getIdServicio() != null) {
            where = String.format("%s and l.id_servicio = %d", where, param.getIdServicio());
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        
        if (param.getFechaDesde() != null) {
            where = String.format("%s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            where = String.format("%s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        if (param.getIdEstado() != null) {
            where = String.format("%s and l.id_estado = %d", where, param.getIdEstado());
        }
        
        String sql = String.format("select l.*"
                + " from facturacion.liquidacion l"
                + " left join comercial.moneda m on m.id = l.id_moneda"
                + " left join comercial.contrato c on c.id = l.id_contrato"
                + " left join comercial.servicio s on s.id = l.id_servicio"
                + " left join comercial.producto p on p.id = s.id_producto"
                + " where %s order by l.id desc offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Liquidacion.class);
        
        List<Liquidacion> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(LiquidacionParam param) {

        String where = "true";
        
        if (param.getId() != null) {
            where = String.format("%s and l.id = %d", where, param.getId());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format("%s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdHistoricoLiquidacion() != null) {
            where = String.format("%s and l.id_historico_liquidacion = %d", where, param.getIdHistoricoLiquidacion());
        }
        
        if (param.getIdMoneda() != null) {
            where = String.format("%s and l.id_moneda = %d", where, param.getIdMoneda());
        }
        
        if (param.getIdContrato() != null) {
            where = String.format("%s and l.id_contrato = %d", where, param.getIdContrato());
        }
        
        if (param.getIdCliente() != null) {
            where = String.format("%s and l.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdPeriodoLiquidacion() != null) {
            where = String.format("%s and l.id_periodo_liquidacion = %d", where, param.getIdPeriodoLiquidacion());
        }
        
        if (param.getIdProducto() != null) {
            where = String.format("%s and s.id_producto = %d", where, param.getIdProducto());
        }
        
        if (param.getIdServicio() != null) {
            where = String.format("%s and l.id_servicio = %d", where, param.getIdServicio());
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        
        if (param.getFechaDesde() != null) {
            where = String.format("%s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            where = String.format("%s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        if (param.getIdEstado() != null) {
            where = String.format("%s and l.id_estado = %d", where, param.getIdEstado());
        }
        
        String sql = String.format("select count(l.*)"
                + " from facturacion.liquidacion l"
                + " left join comercial.moneda m on m.id = l.id_moneda"
                + " left join comercial.contrato c on c.id = l.id_contrato"
                + " left join comercial.servicio s on s.id = l.id_servicio"
                + " left join comercial.producto p on p.id = s.id_producto"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
    
    /**
     * Obtiene la lista de proveedores operativos por comprador
     * @param param parámetros
     * @return Lista
     */
    public List<ProvOperativoPorCompPojo> findProvOpPorComp(ProvOpPorCompParam param) {

        String where = "true";
        String where2 = "false";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdProducto() != null) {
            where = String.format(" %s and c.id_producto = %d", where, param.getIdProducto());
        }
        
        if(param.getIdComprador() != null) {
            where2 = String.format(" %s or ld.id_cliente_rel = %d", where2, param.getIdComprador());
        }
        
        if(param.getIdGrupoComprador() != null) {
            where2 = String.format(" %s or ld.id_grupo_cliente_rel = %d", where2, param.getIdGrupoComprador());
        }
        
        String sql = String.format("select prov.id_cliente as id_proveedor, prov.razon_social as proveedor,\n"
                + "comp.id_cliente as id_comprador,\n"
                + "gc.id as id_grupo_comprador,\n"
                + "coalesce(gc.descripcion, sc.descripcion, comp.razon_social) as comprador,\n"
                + "tt.descripcion as tipo_tarifa, t.descripcion as tarifa, ld.monto_tarifa, ld.monto_descuento,\n"
                + "ld.monto_total, lde.cant_doc_enviados, lde.cant_doc_recibidos\n"
                + "from facturacion.liquidacion l\n"
                + "left join facturacion.historico_liquidacion hl on hl.id_liquidacion = l.id\n"
                + "left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "left join facturacion.liquidacion_detalle_edi lde on lde.id_historico_liquidacion = ld.id_historico_liquidacion and lde.nro_linea = ld.nro_linea\n"
                + "left join comercial.tarifa t on t.id = ld.id_tarifa\n"
                + "left join comercial.tipo_tarifa tt on tt.id = t.id_tipo_tarifa\n"
                + "left join comercial.contrato c on c.id = l.id_contrato\n"
                + "left join comercial.cliente prov on prov.id_cliente = c.id_cliente\n"
                + "left join comercial.cliente comp on comp.id_cliente = ld.id_cliente_rel\n"
                + "left join comercial.grupo_cliente gc on gc.id = ld.id_grupo_cliente_rel\n"
                + "left join comercial.sociedad_cliente sc on sc.id = ld.id_sociedad_cliente_rel\n"
                + "where hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "and l.estado in ('P','F','X','N')\n"
                + "and %s and (%s)"
                + "order by proveedor offset %d limit %d",
                where, where2, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<ProvOperativoPorCompPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            
            ProvOperativoPorCompPojo item = new ProvOperativoPorCompPojo();
            item.setIdProveedor((Integer)objects[0]);
            item.setProveedor((String)objects[1]);
            item.setIdComprador((Integer)objects[2]);
            item.setIdGrupoComprador((Integer)objects[3]);
            item.setComprador((String)objects[4]);
            item.setTipoTarifa((String)objects[5]);
            item.setTarifa((String)objects[6]);
            item.setMontoTarifa((BigDecimal)objects[7]);
            item.setMontoDescuento((BigDecimal)objects[8]);
            item.setMontoTotal((BigDecimal)objects[9]);
            item.setCantDocEnviados((Integer)objects[10]);
            item.setCantDocRecibidos((Integer)objects[11]);
            result.add(item);
        }
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de proveedores operativos por comprador
     * @param param parámetros
     * @return Tamaño de la lista
     */
    public Long findProvOpPorCompSize(ProvOpPorCompParam param) {

        String where = "true";
        String where2 = "false";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdProducto() != null) {
            where = String.format(" %s and c.id_producto = %d", where, param.getIdProducto());
        }
        
        if(param.getIdComprador() != null) {
            where2 = String.format(" %s or ld.id_cliente_rel = %d", where2, param.getIdComprador());
        }
        
        if(param.getIdGrupoComprador() != null) {
            where2 = String.format(" %s or ld.id_grupo_cliente_rel = %d", where2, param.getIdGrupoComprador());
        }
        
        String sql = String.format("select count(distinct(prov.id_cliente, comp.id_cliente, gc.id,\n"
                + "tt.id, t.id, ld.id_historico_liquidacion, ld.nro_linea,\n"
                + "lde.id_historico_liquidacion, lde.nro_linea))\n"
                + "from facturacion.liquidacion l\n"
                + "left join facturacion.historico_liquidacion hl on hl.id_liquidacion = l.id\n"
                + "left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "left join facturacion.liquidacion_detalle_edi lde on lde.id_historico_liquidacion = ld.id_historico_liquidacion and lde.nro_linea = ld.nro_linea\n"
                + "left join comercial.tarifa t on t.id = ld.id_tarifa\n"
                + "left join comercial.tipo_tarifa tt on tt.id = t.id_tipo_tarifa\n"
                + "left join comercial.contrato c on c.id = l.id_contrato\n"
                + "left join comercial.cliente prov on prov.id_cliente = c.id_cliente\n"
                + "left join comercial.cliente comp on comp.id_cliente = ld.id_cliente_rel\n"
                + "left join comercial.grupo_cliente gc on gc.id = ld.id_grupo_cliente_rel\n"
                + "left join comercial.sociedad_cliente sc on sc.id = ld.id_sociedad_cliente_rel\n"
                + "where hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "and l.estado in ('P','F','X','N')\n"
                + "and %s and (%s)", where, where2);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<LiquidacionPojo> findPorClienteProducto(LiquidacionParam param) {
        
        String where = "true";
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and l.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdPeriodoLiquidacion() != null) {
            where = String.format("%s and l.id_periodo_liquidacion = %d", where, param.getIdPeriodoLiquidacion());
        }
        
        if(param.getIdContrato() != null) {
            where = String.format("%s and l.id_contrato = %d", where, param.getIdContrato());
        }
        
        if(param.getIdProducto() != null) {
            where = String.format("%s and p.id = %d", where, param.getIdProducto());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and s.id = %d", where, param.getIdServicio());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format("%s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdMoneda() != null) {
            where = String.format("%s and l.id_moneda = %d", where, param.getIdMoneda());
        }
        
        String sql = String.format("select cli.id_cliente, cli.razon_social, cli.requiere_oc,\n"
                + "cli.fact_electronica, p.id as id_producto, p.descripcion as producto,\n"
                + "m.id as id_moneda, m.descripcion as moneda,count(*)\n"
                + "from facturacion.liquidacion l\n"
                + "left join comercial.contrato c on l.id_contrato = c.id\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join comercial.moneda m on l.id_moneda = m.id\n"
                + "left join comercial.servicio s on s.id = l.id_servicio\n"
                + "left join comercial.producto p on p.id = s.id_producto\n"
                + "where %s group by cli.id_cliente, p.id, m.id\n"
                + "order by p.id, cli.razon_social offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<LiquidacionPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            LiquidacionPojo item = new LiquidacionPojo();
            item.setIdCliente((Integer)objects[i++]);
            item.setRazonSocial((String)objects[i++]);
            item.setRequiereOc((Character)objects[i++]);
            item.setFactElectronica((Character)objects[i++]);
            item.setIdProducto((Integer)objects[i++]);
            item.setProducto((String)objects[i++]);
            item.setIdMoneda((Integer)objects[i++]);
            item.setMoneda((String)objects[i++]);
            item.setCantidad((Number)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public Long findPorClienteProductoSize(LiquidacionParam param) {
        
        String where = "true";
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and l.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdPeriodoLiquidacion() != null) {
            where = String.format("%s and l.id_periodo_liquidacion = %d", where, param.getIdPeriodoLiquidacion());
        }
        
        if(param.getIdContrato() != null) {
            where = String.format("%s and l.id_contrato = %d", where, param.getIdContrato());
        }
        
        if(param.getIdProducto() != null) {
            where = String.format("%s and p.id = %d", where, param.getIdProducto());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and s.id = %d", where, param.getIdServicio());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format("%s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getIdMoneda() != null) {
            where = String.format("%s and l.id_moneda = %d", where, param.getIdMoneda());
        }
        
        String sql = String.format("select count( distinct(cli.id_cliente, p.id, m.id))\n"
                + "from facturacion.liquidacion l\n"
                + "left join comercial.contrato c on l.id_contrato = c.id\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join comercial.moneda m on l.id_moneda = m.id\n"
                + "left join comercial.servicio s on s.id = l.id_servicio\n"
                + "left join comercial.producto p on p.id = s.id_producto\n"
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
