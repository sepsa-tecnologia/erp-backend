/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de liquidación de producto
 *
 * @author Jonathan D. Bernal Fernández
 */
public class LiquidacionProductoParam extends CommonParam {

    public LiquidacionProductoParam() {
    }

    public LiquidacionProductoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;

    /**
     * Plazo
     */
    @QueryParam("plazo")
    private Character plazo;

    /**
     * Extensión
     */
    @QueryParam("extension")
    private Character extension;

    /**
     * Facturas pendientes
     */
    @QueryParam("facturasPendientes")
    private Integer facturasPendientes;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar el producto"));
        }

        if (isNull(plazo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar el plazo"));
        }

        if (isNull(extension)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar la extensión"));
        }

        if (isNull(facturasPendientes)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar la cantidad de facturas pendientes"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar si esta activo"));
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar el producto"));
        }

        if (isNull(plazo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar el plazo"));
        }

        if (isNull(extension)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar la extensión"));
        }

        if (isNull(facturasPendientes)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar la cantidad de facturas pendientes"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar si esta activo"));
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Character getPlazo() {
        return plazo;
    }

    public void setPlazo(Character plazo) {
        this.plazo = plazo;
    }

    public Character getExtension() {
        return extension;
    }

    public void setExtension(Character extension) {
        this.extension = extension;
    }

    public Integer getFacturasPendientes() {
        return facturasPendientes;
    }

    public void setFacturasPendientes(Integer facturasPendientes) {
        this.facturasPendientes = facturasPendientes;
    }
}
