/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.externalservice.adapters;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionGeneralParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.FirmaDigitalPojo;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;
import py.com.sepsa.erp.ejb.externalservice.service.APISepsaCore;

/**
 * Adaptador para obtener el FirmaDigital asociado a un Cliente
 *
 * @author Gustavo Benitez
 */
@Log4j2
public class FirmaDigitalAdapter extends AbstractAdapterImpl<List<FirmaDigitalPojo>, HttpStringResponse> {

    /**
     * ConfiguracionGeneral
     */
    private ConfiguracionGeneralParam param;

    /**
     * Host SepsaCore
     */
    private String hostSepsaCore;

    /**
     * Token SiediApi
     */
    private String apiToken;

    @Override
    public void request() {
        APISepsaCore api = new APISepsaCore(hostSepsaCore, apiToken);

        this.resp = api.obtenerFirmaDigital(param);
        JsonObject payload;

        List<FirmaDigitalPojo> list = new ArrayList<>();

        switch (resp.getRespCode()) {
            case OK:

                try {
                payload = new JsonParser().parse(resp.getPayload()).getAsJsonObject();
                setData(payload);

                JsonArray array = payload
                        .getAsJsonObject("payload")
                        .getAsJsonArray("data");

                Gson gson = new Gson();

                for (Object object : array) {
                    JsonObject o = (JsonObject) object;
                    list.add(gson.fromJson(o.toString(), FirmaDigitalPojo.class));
                }

                setPayload(list);
            } catch (JsonSyntaxException e) {
            }

            break;


            default:
                setPayload(list);
                break;
        }

    }

    public static List<FirmaDigitalPojo> obtener(String hostSepsaCore, String apiToken, ConfiguracionGeneralParam param) {
        List<FirmaDigitalPojo> list = new ArrayList<>();

        try {
            FirmaDigitalAdapter adapter = new FirmaDigitalAdapter();
            adapter.setParam(param);
            adapter.setHostSepsaCore(hostSepsaCore);
            adapter.setApiToken(apiToken);
            adapter.request();
            list = adapter.getPayload();

        } catch (Exception e) {
            log.fatal("Error", e);
        }

        return list;
    }

    public FirmaDigitalAdapter() {
        super(new HttpStringResponse());
    }

    public void setParam(ConfiguracionGeneralParam param) {
        this.param = param;
    }

    public ConfiguracionGeneralParam getParam() {
        return param;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public void setHostSepsaCore(String hostSepsaCore) {
        this.hostSepsaCore = hostSepsaCore;
    }

    public String getHostSepsaCore() {
        return hostSepsaCore;
    }
}
