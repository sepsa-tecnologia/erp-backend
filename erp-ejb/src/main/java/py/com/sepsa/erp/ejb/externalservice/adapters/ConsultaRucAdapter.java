/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.externalservice.adapters;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.info.pojos.RucPojo;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;
import py.com.sepsa.erp.ejb.externalservice.service.APITuRuc;

/**
 * Adaptador para obtener el FirmaDigital asociado a un Cliente
 *
 * @author Gustavo Benitez
 */
@Log4j2
public class ConsultaRucAdapter extends AbstractAdapterImpl<RucPojo, HttpStringResponse> {

    private static final String HOST = "0#https://turuc.com.py:443";

    @Getter
    @Setter
    private String ruc;

    @Override
    public void request() {
        APITuRuc api = new APITuRuc(HOST);

        this.resp = api.consultarRuc(ruc);
        JsonObject payload;

        RucPojo result = null;

        switch (resp.getRespCode()) {
            case OK:

                try {
                payload = new JsonParser().parse(resp.getPayload()).getAsJsonObject();
                setData(payload);

                JsonObject data = payload.getAsJsonObject("data");
                result = new RucPojo();
                result.setRazonSocialSugerida(data.get("razonSocial").getAsString());
                result.setEsPersonaJuridica(data.get("esPersonaJuridica").getAsBoolean());
                result.setEsEntidadPublica(data.get("esEntidadPublica").getAsBoolean());

                setPayload(result);
            } catch (JsonSyntaxException e) {
            }

            break;

            default:
                setPayload(null);
                break;
        }

    }

    public static RucPojo consultarRuc(String ruc) {

        try {
            ConsultaRucAdapter adapter = new ConsultaRucAdapter();
            adapter.setRuc(ruc);
            adapter.request();
            return adapter.getPayload();

        } catch (Exception e) {
            log.fatal("Error", e);
        }

        return null;
    }

    public ConsultaRucAdapter() {
        super(new HttpStringResponse());
    }
}
