/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de configuración
 * @author Jonathan
 */
public class ConfiguracionParam extends CommonParam {

    public ConfiguracionParam() {
    }

    public ConfiguracionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Lista de valores de configuración
     */
    private List<ConfiguracionValorParam> configuracionValores;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(!isNullOrEmpty(configuracionValores)) {
            for (ConfiguracionValorParam item : configuracionValores) {
                item.setIdConfiguracion(0);
                item.setIdEmpresa(idEmpresa);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNullOrEmpty(configuracionValores)) {
            for (ConfiguracionValorParam item : configuracionValores) {
                list.addAll(item.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setConfiguracionValores(List<ConfiguracionValorParam> configuracionValores) {
        this.configuracionValores = configuracionValores;
    }

    public List<ConfiguracionValorParam> getConfiguracionValores() {
        return configuracionValores;
    }
}
