/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de usuario empresa
 * @author Jonathan
 */
public class UsuarioEmpresaParam extends CommonParam {
    
    public UsuarioEmpresaParam() {
    }

    public UsuarioEmpresaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    @QueryParam("usuario")
    private String usuario;
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Facturable
     */
    @QueryParam("facturable")
    private Character facturable;

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de local"));
        }
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(facturable)) {
            facturable = 'S';
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToListRelacionado() {
        
        super.isValidToList();
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCreateMasivo() {
        
        limpiarErrores();
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere indicar si esta activo"));
        }
        
        if(isNull(facturable)) {
            facturable = 'S';
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEmpresa) && isNullOrEmpty(codigoEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de local"));
        }
        
        if(isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de usuario"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(facturable)) {
            facturable = 'S';
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setFacturable(Character facturable) {
        this.facturable = facturable;
    }

    public Character getFacturable() {
        return facturable;
    }
}
