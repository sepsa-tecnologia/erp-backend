/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils.jwe;

import lombok.extern.log4j.Log4j2;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.JoseException;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
@Log4j2
public class RsaKeyProducer {

    private RsaKeyProducer() {
    }

    private static RsaJsonWebKey theOne;

    /**
     * Produce un RsaJsonWebKey
     * @return RsaJsonWebKey
     */
    public static RsaJsonWebKey produce() {
        if (theOne == null) {
            try {
                /*Map<String, Object> key = new HashMap<>();
                key.put("Empresa", "Sepsa");
                key.put("Contraseña", "sepsa123");
                theOne = new RsaJsonWebKey(key);//*/
                theOne = RsaJwkGenerator.generateJwk(2048);// mientras se realizan otros servicio utilizar asi.
            } catch (JoseException ex) {
                log.fatal("No se pudo generar la clave RsaJsonWebKey", ex);
            }
        }

        return theOne;
    }
}
