/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan Bernal
 */
public class FacturaDncpPojo {

    public FacturaDncpPojo() {
    }

    public FacturaDncpPojo(Integer idFactura, String modalidad, String secuencia, String anho, String entidad, Date fechaEmision) {
        this.idFactura = idFactura;
        this.modalidad = modalidad;
        this.secuencia = secuencia;
        this.anho = anho;
        this.entidad = entidad;
        this.fechaEmision = fechaEmision;
    }

    /**
     * Identificador de factura
     */
    private Integer idFactura;

    /**
     * Modalidad
     */
    private String modalidad;

    /**
     * Secuencia
     */
    private String secuencia;

    /**
     * Año
     */
    private String anho;

    /**
     * Entidad
     */
    private String entidad;

    /**
     * Fecha de emisión
     */
    private Date fechaEmision;

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getAnho() {
        return anho;
    }

    public void setAnho(String anho) {
        this.anho = anho;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
}
