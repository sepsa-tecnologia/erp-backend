
package py.com.sepsa.erp.ejb.externalservice.notificacion;

/**
 * POJO de la entidad producto
 * @author Daniel F. Escauriza Arza
 */
public class Product {
    
    /**
     * Identificador de la entidad producto
     */
    private Integer id;
    
    /**
     * Descripción del producto
     */
    private String description;
            
    /**
     * Código del producto
     */
    private String code;
    
    /**
     * Bandera que indica si el producto es bonificable
     */
    private String redeem;
    
    /**
     * Estado de la entidad producto
     */
    private String state;
    
    /**
     * URL del servicio asociado al producto
     */
    private String url;

    /**
     * Obtiene el identificador de la entidad producto
     * @return Identificador de la entidad producto
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setea el identificador de la entidad producto
     * @param id Identificador de la entidad producto
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Obtiene la descripción del producto
     * @return Descripción del producto
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setea la descripción del producto
     * @param description Descripción del producto
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Obtiene el código del producto
     * @return Código del producto
     */
    public String getCode() {
        return code;
    }

    /**
     * Setea el código del producto
     * @param code Código del producto
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtiene la bandera que indica si el producto es bonificable
     * @return Bandera que indica si el producto es bonificable
     */
    public String getRedeem() {
        return redeem;
    }

    /**
     * Setea la bandera que indica si el producto es bonificable
     * @param redeem Bandera que indica si el producto es bonificable
     */
    public void setRedeem(String redeem) {
        this.redeem = redeem;
    }

    /**
     * Obtiene el estado de la entidad producto
     * @return Estado de la entidad producto
     */
    public String getState() {
        return state;
    }

    /**
     * Setea el estado de la entidad producto
     * @param state Estado de la entidad producto
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Obtiene la URL del servicio asociado al producto
     * @return URL del servicio asociado al producto
     */
    public String getUrl() {
        return url;
    }

    /**
     * Setea la URL del servicio asociado al producto
     * @param url URL del servicio asociado al producto
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    /**
     * Constructor de Product
     * @param id Identificador de la entidad producto
     * @param description Descripción del producto
     * @param code Código del producto
     * @param redeem Bandera que indica si el producto es bonificable
     * @param state Estado de la entidad producto
     * @param url URL del servicio asociado al producto
     */
    public Product(Integer id, String description, String code, 
            Character redeem, Character state, String url) {
        this.id = id;
        this.description = description;
        this.code = code;
        this.redeem = redeem == null ? "" : redeem.toString();
        this.state = state == null ? "" : state.toString();
        this.url = url;
    }

    /**
     * Constructor de ContentType
     * @param id Identificador de la entidad producto
     */
    public Product(Integer id) {
        this.id = id;
    }
    
    /**
     * Constructor de Product
     */
    public Product() {}
       
}
