/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de configuración valor
 * @author Jonathan
 */
public class ConfiguracionValorParam extends CommonParam {

    public ConfiguracionValorParam() {
    }

    public ConfiguracionValorParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de configuración
     */
    @QueryParam("idConfiguracion")
    private Integer idConfiguracion;

    /**
     * Código de configuración
     */
    @QueryParam("codigoConfiguracion")
    private String codigoConfiguracion;
    
    /**
     * Valor
     */
    @QueryParam("valor")
    private String valor;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idConfiguracion) && isNullOrEmpty(codigoConfiguracion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de configuración"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNullOrEmpty(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idConfiguracion) && isNullOrEmpty(codigoConfiguracion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de configuración"));
        }
        
        if(isNullOrEmpty(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdConfiguracion() {
        return idConfiguracion;
    }

    public void setIdConfiguracion(Integer idConfiguracion) {
        this.idConfiguracion = idConfiguracion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public void setCodigoConfiguracion(String codigoConfiguracion) {
        this.codigoConfiguracion = codigoConfiguracion;
    }

    public String getCodigoConfiguracion() {
        return codigoConfiguracion;
    }
}
