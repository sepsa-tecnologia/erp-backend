/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 *
 * @author Jonathan
 */
public class EmpresaPojo {
    
    /**
     * Identificador de cliente_sepsa
     */
    private Integer idClienteSepsa;
    
    /**
     * Cantidad
     */
    private Number cantidad;

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public Number getCantidad() {
        return cantidad;
    }

    public void setCantidad(Number cantidad) {
        this.cantidad = cantidad;
    }
}
