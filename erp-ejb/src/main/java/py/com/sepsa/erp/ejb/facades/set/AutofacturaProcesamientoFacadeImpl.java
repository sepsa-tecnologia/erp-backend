/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.set.AutofacturaProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.FacturaProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.AutofacturaProcesamientoParam;
import py.com.sepsa.erp.ejb.entities.set.filters.FacturaProcesamientoParam;
import py.com.sepsa.erp.ejb.entities.set.filters.ProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "AutofacturaProcesamientoFacade", mappedName = "AutofacturaProcesamientoFacade")
@Local(AutofacturaProcesamientoFacade.class)
public class AutofacturaProcesamientoFacadeImpl extends FacadeImpl<AutofacturaProcesamiento, AutofacturaProcesamientoParam> implements AutofacturaProcesamientoFacade {

    public AutofacturaProcesamientoFacadeImpl() {
        super(AutofacturaProcesamiento.class);
    }

    public Boolean validToCreate(AutofacturaProcesamientoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        AutoFacturaParam param1 = new AutoFacturaParam();
        param1.setId(param.getIdAutofactura());
        
        Long size = facades.getAutoFacturaFacade().findSize(param1);
        
        if(size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la autofactura"));
        }
        
        if(!isNull(param.getIdProcesamiento())) {
            ProcesamientoParam param2 = new ProcesamientoParam();
            param2.setId(param.getIdProcesamiento());

            size = facades.getProcesamientoFacade().findSize(param2);

            if(size <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el procesamiento"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "DTE_AUTOFACTURA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(!isNull(param.getProcesamiento())) {
            facades.getProcesamientoFacade().validToCreate(param.getProcesamiento());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(AutofacturaProcesamientoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        AutofacturaProcesamiento dp = find(param.getId());
        
        if(isNull(dp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de autofactura procesamiento"));
        }
        
        AutoFacturaParam param1 = new AutoFacturaParam();
        param1.setId(param.getIdAutofactura());
        
        Long size = facades.getAutoFacturaFacade().findSize(param1);
        
        if(size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la autofactura"));
        }
        
        if(!isNull(param.getIdProcesamiento())) {
            
            ProcesamientoParam param2 = new ProcesamientoParam();
            param2.setId(param.getIdProcesamiento());

            size = facades.getProcesamientoFacade().findSize(param2);

            if(size <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el procesamiento"));
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "DTE_AUTOFACTURA");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public AutofacturaProcesamiento create(AutofacturaProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Integer idProcesamiento = null;
        
        if(!isNull(param.getProcesamiento())) {
            Procesamiento procesamiento = facades.getProcesamientoFacade()
                    .create(param.getProcesamiento(), userInfo);
            idProcesamiento = procesamiento.getId();
        } else {
            idProcesamiento = param.getIdProcesamiento();
        }
        
        AutofacturaProcesamiento item = new AutofacturaProcesamiento();
        item.setFechaInsercion(Calendar.getInstance().getTime());
        item.setIdEstado(param.getIdEstado());
        item.setIdAutofactura(param.getIdAutofactura());
        item.setIdProcesamiento(idProcesamiento);
        
        create(item);
        
        return item;
    }

    @Override
    public AutofacturaProcesamiento edit(AutofacturaProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        AutofacturaProcesamiento item = find(param.getId());
        item.setIdEstado(param.getIdEstado());
        item.setIdAutofactura(param.getIdAutofactura());
        item.setIdProcesamiento(param.getIdProcesamiento());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<AutofacturaProcesamiento> find(AutofacturaProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(AutofacturaProcesamiento.class);
        Root<AutofacturaProcesamiento> root = cq.from(AutofacturaProcesamiento.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if(param.getIdAutofactura() != null) {
            predList.add(qb.equal(root.get("idAutofactura"), param.getIdAutofactura()));
        }
        
        if(param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<AutofacturaProcesamiento> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(AutofacturaProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<AutofacturaProcesamiento> root = cq.from(AutofacturaProcesamiento.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if(param.getIdAutofactura() != null) {
            predList.add(qb.equal(root.get("idAutofactura"), param.getIdAutofactura()));
        }
        
        if(param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
