/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Metrica;
import py.com.sepsa.erp.ejb.entities.info.filters.MetricaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MetricaFacade", mappedName = "MetricaFacade")
@Local(MetricaFacade.class)
public class MetricaFacadeImpl extends FacadeImpl<Metrica, MetricaParam> implements MetricaFacade {

    public MetricaFacadeImpl() {
        super(Metrica.class);
    }
    
    public Boolean validToCreate(MetricaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MetricaParam param1 = new MetricaParam();
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(MetricaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Metrica metrica = find(param.getId());
        
        if(metrica == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la métrica"));
        } else if(!metrica.getCodigo().equals(param.getCodigo().trim())) {
            
            MetricaParam param1 = new MetricaParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Metrica create(MetricaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Metrica result = new Metrica();
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        create(result);
        
        return result;
    }
    
    @Override
    public Metrica edit(MetricaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Metrica result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        edit(result);
        
        return result;
    }

    @Override
    public Metrica find(Integer id, String codigo) {
        MetricaParam param = new MetricaParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.setActivo('S');
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<Metrica> find(MetricaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Metrica.class);
        Root<Metrica> root = cq.from(Metrica.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<Metrica> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MetricaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Metrica> root = cq.from(Metrica.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
