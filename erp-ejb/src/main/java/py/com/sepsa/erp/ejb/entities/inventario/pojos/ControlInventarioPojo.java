/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class ControlInventarioPojo {

    public ControlInventarioPojo(Integer id, String codigo,
            Integer idEmpresa, String empresa, Integer idUsuarioEncargado,
            String usuarioEncargado, Integer idUsuarioSupervisor,
            String usuarioSupervisor, Integer idMotivo, String motivo,
            String codigoMotivo, Integer idEstado, String estado,
            String codigoEstado, Date fechaInsercion, Date fechaInicio,
            Date fechaFin) {
        this.id = id;
        this.codigo = codigo;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idUsuarioEncargado = idUsuarioEncargado;
        this.usuarioEncargado = usuarioEncargado;
        this.idUsuarioSupervisor = idUsuarioSupervisor;
        this.usuarioSupervisor = usuarioSupervisor;
        this.idMotivo = idMotivo;
        this.motivo = motivo;
        this.codigoMotivo = codigoMotivo;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.fechaInsercion = fechaInsercion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Código
     */
    private String codigo;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de usuario encargado
     */
    private Integer idUsuarioEncargado;
    
    /**
     * Usuario encargado
     */
    private String usuarioEncargado;
    
    /**
     * Identificador de usuario supervisor
     */
    private Integer idUsuarioSupervisor;
    
    /**
     * Usuario supervisor
     */
    private String usuarioSupervisor;
    
    /**
     * Identificador de motivo
     */
    private Integer idMotivo;
    
    /**
     * Motivo
     */
    private String motivo;
    
    /**
     * Código de motivo
     */
    private String codigoMotivo;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;
    
    /**
     * Fecha inicio
     */
    private Date fechaInicio;
    
    /**
     * Fecha fin
     */
    private Date fechaFin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuarioEncargado() {
        return idUsuarioEncargado;
    }

    public void setIdUsuarioEncargado(Integer idUsuarioEncargado) {
        this.idUsuarioEncargado = idUsuarioEncargado;
    }

    public String getUsuarioEncargado() {
        return usuarioEncargado;
    }

    public void setUsuarioEncargado(String usuarioEncargado) {
        this.usuarioEncargado = usuarioEncargado;
    }

    public Integer getIdUsuarioSupervisor() {
        return idUsuarioSupervisor;
    }

    public void setIdUsuarioSupervisor(Integer idUsuarioSupervisor) {
        this.idUsuarioSupervisor = idUsuarioSupervisor;
    }

    public String getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(String usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEmpresa() {
        return empresa;
    }
}
