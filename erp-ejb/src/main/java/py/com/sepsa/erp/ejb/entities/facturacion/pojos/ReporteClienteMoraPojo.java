/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;

/**
 * POJO para el reporte de clientes en mora
 * @author Jonathan
 */
public class ReporteClienteMoraPojo {
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Identificador de comercial
     */
    private Integer idComercial;
    
    /**
     * Comercial
     */
    private String comercial;
    
    /**
     * Monto total facturado
     */
    private BigDecimal montoTotalFacturado = BigDecimal.ZERO;
    
    /**
     * Monto total pendiente
     */
    private BigDecimal montoTotalPendiente = BigDecimal.ZERO;
    
    /**
     * Porcentaje de mora
     */
    private BigDecimal porcentajeMora = BigDecimal.ZERO;
    
    /**
     * Monto 0
     */
    private BigDecimal monto0 = BigDecimal.ZERO;
    
    /**
     * Monto 30
     */
    private BigDecimal monto30 = BigDecimal.ZERO;
    
    /**
     * Monto 60
     */
    private BigDecimal monto60 = BigDecimal.ZERO;
    
    /**
     * Monto 90
     */
    private BigDecimal monto90 = BigDecimal.ZERO;
    
    /**
     * Monto 120
     */
    private BigDecimal monto120 = BigDecimal.ZERO;
    
    /**
     * Monto 150
     */
    private BigDecimal monto150 = BigDecimal.ZERO;
    
    /**
     * Monto 180
     */
    private BigDecimal monto180 = BigDecimal.ZERO;

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public BigDecimal getMontoTotalFacturado() {
        return montoTotalFacturado;
    }

    public void setMontoTotalFacturado(BigDecimal montoTotalFacturado) {
        this.montoTotalFacturado = montoTotalFacturado;
    }

    public BigDecimal getMontoTotalPendiente() {
        return montoTotalPendiente;
    }

    public void setMontoTotalPendiente(BigDecimal montoTotalPendiente) {
        this.montoTotalPendiente = montoTotalPendiente;
    }

    public BigDecimal getPorcentajeMora() {
        return porcentajeMora;
    }

    public void setPorcentajeMora(BigDecimal porcentajeMora) {
        this.porcentajeMora = porcentajeMora;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public String getComercial() {
        return comercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public BigDecimal getMonto0() {
        return monto0;
    }

    public void setMonto0(BigDecimal monto0) {
        this.monto0 = monto0;
    }

    public BigDecimal getMonto30() {
        return monto30;
    }

    public void setMonto30(BigDecimal monto30) {
        this.monto30 = monto30;
    }

    public BigDecimal getMonto60() {
        return monto60;
    }

    public void setMonto60(BigDecimal monto60) {
        this.monto60 = monto60;
    }

    public BigDecimal getMonto90() {
        return monto90;
    }

    public void setMonto90(BigDecimal monto90) {
        this.monto90 = monto90;
    }

    public BigDecimal getMonto120() {
        return monto120;
    }

    public void setMonto120(BigDecimal monto120) {
        this.monto120 = monto120;
    }

    public BigDecimal getMonto150() {
        return monto150;
    }

    public void setMonto150(BigDecimal monto150) {
        this.monto150 = monto150;
    }

    public BigDecimal getMonto180() {
        return monto180;
    }

    public void setMonto180(BigDecimal monto180) {
        this.monto180 = monto180;
    }
}
