/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.pojos;

import java.io.Serializable;

/**
 * POJO para el reporte de cliente y servicios
 * @author Jonathan
 */
public class ReporteClienteServicioPojo implements Serializable {
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Nro de documento
     */
    private String nroDocumento;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Identificador de tipo de tarifa
     */
    private Integer idTipoTarifa;
    
    /**
     * Tipo tarifa
     */
    private String tipoTarifa;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Servicio
     */
    private String servicio;
    
    /**
     * Identificador de comercial
     */
    private Integer idComercial;
    
    /**
     * Comercial
     */
    private String comercial;

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }


    public Integer getIdTipoTarifa() {
        return idTipoTarifa;
    }

    public void setIdTipoTarifa(Integer idTipoTarifa) {
        this.idTipoTarifa = idTipoTarifa;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public String getComercial() {
        return comercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }
}
