/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.info.CanalVenta;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.TipoDatoPersona;
import py.com.sepsa.erp.ejb.entities.comercial.pojos.ClientePojo;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.CanalVentaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.DireccionParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.TelefonoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.utils.misc.Strings;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ClienteFacade", mappedName = "ClienteFacade")
@Local(ClienteFacade.class)
public class ClienteFacadeImpl extends FacadeImpl<Cliente, ClienteParam> implements ClienteFacade{

    public ClienteFacadeImpl() {
        super(Cliente.class);
    }

    @Override
    public Map<String, String> getMapConditions(Cliente item) {
        Map<String, String> atributos = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.

        atributos.put("id_cliente", item.getIdCliente() + " ");
        atributos.put("id_empresa", item.getIdEmpresa() + " ");
        atributos.put("id_comercial", item.getIdComercial() + " ");
        atributos.put("id_estado", item.getIdEstado() + " ");
        atributos.put("porcentaje_retencion", item.getPorcentajeRetencion() + " ");
        atributos.put("nro_documento", item.getNroDocumento() + " ");
        atributos.put("id_tipo_documento", item.getIdTipoDocumento() + " ");
        atributos.put("razon_social", item.getRazonSocial() + " ");
        atributos.put("clave_pago", item.getClavePago() + " ");
        atributos.put("fact_electronica", item.getFactElectronica() + " ");
        atributos.put("id_naturaleza_cliente", item.getIdNaturalezaCliente() + " ");
        
        return atributos;
    }
    
    @Override
    public Map<String, String> getMapPk(Cliente item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id_cliente", item.getIdCliente()+ "");
        
        return pk;
    }

    @Override
    public String getTabla() {
        return "cliente";
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @param validarPersona Validar persona
     * @return Bandera
     */
    public Boolean validToCreate(ClienteParam param, Boolean validarPersona) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ClienteParam param1 = new ClienteParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setNroDocumentoEq(param.getNroDocumento().trim());
        Long size = findSize(param1);
        
        if (!param.isRucDuplicado()){
            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un cliente registrado con el nro. de documento"));
            }
        }
            
        NaturalezaCliente naturaleza = facades.getNaturalezaClienteFacade()
                .find(param.getIdNaturalezaCliente(), param.getCodigoNaturalezaCliente());
        
        if(naturaleza == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la naturaleza del cliente"));
        } else {
            param.setIdNaturalezaCliente(naturaleza.getId());
            param.setCodigoNaturalezaCliente(naturaleza.getCodigo());
            
            Boolean validarFormatoRuc = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "VALIDAR_FORMATO_RUC", Boolean.TRUE);
            if(validarFormatoRuc) {
                if (param.getCodigoNaturalezaCliente().equalsIgnoreCase("CONTRIBUYENTE") && (param.getNroDocumento().length() < 7 || param.getNroDocumento().length() > 10)) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se requiere un nro de RUC válido"));
                } else if ((!param.getCodigoNaturalezaCliente().equalsIgnoreCase("CONTRIBUYENTE")) && (param.getNroDocumento().length() < 1 || param.getNroDocumento().length() > 20)) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se requiere un nro de documento válido"));
                }
            }
        }
        
        TipoDatoPersona tipoDatoPersona = facades.getTipoDatoPersonaFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento());
        
        if(tipoDatoPersona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de dato persona"));
        } else {
            param.setIdTipoDocumento(tipoDatoPersona.getId());
        }
        
        if(param.getIdComercial() != null) {
            Persona comercial = facades.getPersonaFacade().find(param.getIdComercial());

            if(comercial == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el comercial"));
            }
        }
        
        if(param.getPersona() != null) {
            facades.getPersonaFacade().validToCreate(param.getPersona());
        } else if(param.getIdCliente() != null) {
            
            Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());

            if(validarPersona && cliente != null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un cliente registrado con el identificador"));
            }
            
            PersonaParam personaParam = new PersonaParam();
            personaParam.setId(param.getIdCliente());
            personaParam.setIdEmpresa(param.getIdEmpresa());
            Persona persona = facades.getPersonaFacade().findFirst(personaParam);
            
            if(validarPersona && persona == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la persona asociada al cliente"));
            } else {
                
                String razonSocial = "";
                
                if(persona.getTipoPersona() != null && persona.getTipoPersona().getCodigo().equalsIgnoreCase("FISICA")) {
                    
                    razonSocial = String.format("%s %s",
                            persona.getNombre() == null ? "" : persona.getNombre(),
                            persona.getApellido() == null ? "" : persona.getApellido());
                    
                } else if(persona.getTipoPersona() != null && persona.getTipoPersona().getCodigo().equalsIgnoreCase("JURIDICA")) {
                    
                    razonSocial = persona.getRazonSocial();
                }
                
                param.setRazonSocial(razonSocial);
            }
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "CLIENTE");
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        if(param.getPersona() != null) {
            facades.getPersonaFacade().validToCreate(param.getPersona());
            
            String razonSocial = "";
                
            if(param.getPersona().getCodigoTipoPersona().equalsIgnoreCase("FISICA")) {

                razonSocial = String.format("%s %s",
                        param.getPersona().getNombre() == null ? "" : param.getPersona().getNombre(),
                        param.getPersona().getApellido() == null ? "" : param.getPersona().getApellido());

            } else if(param.getPersona().getCodigoTipoPersona().equalsIgnoreCase("JURIDICA")) {

                razonSocial = param.getPersona().getRazonSocial();
            }

            param.setRazonSocial(razonSocial);
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToEdit(ClienteParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Cliente cliente = find(param.getIdCliente());
        
        if(cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        } else if(!cliente.getNroDocumento().equals(param.getNroDocumento().trim())){
            
            ClienteParam param1 = new ClienteParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setNroDocumentoEq(param.getNroDocumento());
            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un cliente registrado con el nro. de documento"));
            }
        }
        
        if(!Assertions.isNull(param.getIdCanalVenta())
                || !Assertions.isNullOrEmpty(param.getCodigoCanalVenta())) {
            
            CanalVenta canalVenta = facades.getCanalVentaFacade()
                    .find(param.getIdEmpresa(), param.getIdCanalVenta(),
                            param.getCodigoCanalVenta());
            
            if(canalVenta == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el canal de venta"));
            } else {
                param.setIdCanalVenta(canalVenta.getId());
            }
        }
        
        NaturalezaCliente naturaleza = facades.getNaturalezaClienteFacade()
                .find(param.getIdNaturalezaCliente(), param.getCodigoNaturalezaCliente());
        
        if(naturaleza == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la naturaleza del cliente"));
        } else {
            param.setIdNaturalezaCliente(naturaleza.getId());
            param.setCodigoNaturalezaCliente(naturaleza.getCodigo());
            
            Boolean validarFormatoRuc = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "VALIDAR_FORMATO_RUC", Boolean.TRUE);
            if(validarFormatoRuc) {
                if (param.getCodigoNaturalezaCliente().equalsIgnoreCase("CONTRIBUYENTE") && (param.getNroDocumento().length() < 7 || param.getNroDocumento().length() > 10)) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se requiere un nro de RUC válido"));
                } else if ((!param.getCodigoNaturalezaCliente().equalsIgnoreCase("CONTRIBUYENTE")) && (param.getNroDocumento().length() < 1 || param.getNroDocumento().length() > 20)) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se requiere un nro de documento válido"));
                }
            }
        }
        
        TipoDatoPersona tipoDatoPersona = facades.getTipoDatoPersonaFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento());
        
        if(tipoDatoPersona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de dato persona"));
        } else {
            param.setIdTipoDocumento(tipoDatoPersona.getId());
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "CLIENTE");
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        if(param.getIdComercial() != null) {
            Persona comercial = facades.getPersonaFacade().find(param.getIdComercial());

            if(comercial == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el comercial"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Cliente create(ClienteParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        Integer idCliente;
        
        if(param.getPersona() != null) {
            Persona p = facades.getPersonaFacade().create(param.getPersona(), userInfo);
            idCliente = p.getId();
        } else {
            idCliente = param.getIdCliente();
        }
        
        Cliente item = new Cliente();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setIdCliente(idCliente);
        item.setRazonSocial(param.getRazonSocial().trim());
        item.setIdComercial(param.getIdComercial());
        item.setPorcentajeRetencion(param.getPorcentajeRetencion());
        item.setIdEstado(param.getIdEstado());
        item.setNroDocumento(param.getNroDocumento().trim());
        item.setIdTipoDocumento(param.getIdTipoDocumento());
        item.setIdNaturalezaCliente(param.getIdNaturalezaCliente());
        item.setFactElectronica(param.getFactElectronica());
        item.setIdCanalVenta(param.getIdCanalVenta());
        item.setProveedor(param.getProveedor() == null ? 'N' : param.getProveedor());
        item.setComprador(param.getComprador() == null ? 'S' : param.getComprador());
        item.setClavePago(Strings.randomAlphanumeric(4).toUpperCase());
        item.setCodigoPais(param.getCodigoPais());
        
        create(item);
        
        Map<String, String> atributos = getMapConditions(item);
        facades.getRegistroFacade().create("comercial", "cliente", atributos, userInfo);
        
        return item;
    }
    
    @Override
    public Cliente edit(ClienteParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Cliente item = find(param.getIdCliente());
        item.setIdEmpresa(param.getIdEmpresa());
        item.setRazonSocial(param.getRazonSocial().trim());
        item.setIdComercial(param.getIdComercial());
        item.setPorcentajeRetencion(param.getPorcentajeRetencion());
        item.setIdCanalVenta(param.getIdCanalVenta());
        item.setIdEstado(param.getIdEstado());
        item.setNroDocumento(param.getNroDocumento().trim());
        item.setIdTipoDocumento(param.getIdTipoDocumento());
        item.setIdNaturalezaCliente(param.getIdNaturalezaCliente());
        item.setFactElectronica(param.getFactElectronica());
        item.setClavePago(param.getClavePago().trim());
        item.setCodigoPais(param.getCodigoPais());
        
        edit(item);
        
        Map<String, String> pk = getMapPk(item);
        Map<String, String> atributos = getMapConditions(item);
        facades.getRegistroFacade().edit("comercial", "cliente", atributos, userInfo, pk);
        
        return item;
    }
    
    public Cliente cambiarEstadoCliente(Integer idCliente, String codigoEstado, UserInfoImpl userInfo) {
        
        EstadoPojo estado = facades.getEstadoFacade().find(null, codigoEstado, "CLIENTE");
        
        Cliente item = find(idCliente);
        item.setIdEstado(estado.getId());
        
        Map<String, String> pk = getMapPk(item);
        Map<String, String> atributos = getMapConditions(item);
        facades.getRegistroFacade().edit("comercial", "cliente", atributos, userInfo, pk);
        
        return item;
    }
    
    @Override
    public List<String> updateClienteMasivo(ClienteParam param, UserInfoImpl userInfo) {
        try {
            List<String> result = new ArrayList();
            InputStreamReader input = new InputStreamReader(new ByteArrayInputStream(param.getUploadedFileBytes()));
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;
            
            Integer idEmpresa = userInfo.getIdEmpresa();
            ReferenciaGeograficaParam refGeoParam = new ReferenciaGeograficaParam();
            refGeoParam.setIdTipoReferenciaGeografica(1);
            List<ReferenciaGeografica> departamentos = facades.getReferenciaGeograficaFacade().find(refGeoParam);
            
            refGeoParam.setIdTipoReferenciaGeografica(2);
            List<ReferenciaGeografica> distritos = facades.getReferenciaGeograficaFacade().find(refGeoParam);
            
            refGeoParam.setIdTipoReferenciaGeografica(3);
            List<ReferenciaGeografica> ciudades = facades.getReferenciaGeograficaFacade().find(refGeoParam);

            while (true) {

                line = reader.readLine();

                if (line == null) {
                    break;
                }
                lineas.add(line);
            }

            for (int i = 1; i < lineas.size(); i++) {
                lineas.set(i, lineas.get(i) + ";");
            }

            for (int i = 1; i < lineas.size(); i++) {

                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    String[] stringArr = lineas.get(i).split(";");

                    //Datos de Cliente
                    String nombre = null;
                    String ruc = null;
                    String rubro = null;
                    String direccion = null;
                    String telefono = null;
                    String correo = null;
                    String departamento = null;
                    String ciudad = null;

                    for (int k = 0; k < stringArr.length; k++) {
                        switch (k) {
                            case 0:
                                nombre = (String) stringArr[k];
                                break;
                            case 1:
                                rubro = (String) stringArr[k];
                                break;
                            case 2:
                                ciudad = (String) stringArr[k];
                                break;
                            case 3:
                                departamento = (String) stringArr[k];
                                break;
                            case 4:
                                ruc = (String) stringArr[k];
                                break;
                            case 6:
                                direccion = (String) stringArr[k];
                                break;
                            case 7:
                                telefono = (String) stringArr[k];
                                break;
                            case 8:
                                correo = (String) stringArr[k];
                                break;
                        }

                    }
                    Cliente client = null;
                    CanalVenta canalVenta = null;
                    ClienteParam clienteFilter = new ClienteParam();
                    if (ruc != null && !ruc.trim().equalsIgnoreCase("")){
                        
                        clienteFilter.setNroDocumentoEq(ruc);
                        clienteFilter.setBruto(ruc);
                        clienteFilter.setIdEmpresa(idEmpresa);
                        List<Cliente> clientes = find(clienteFilter);
                        
                        if (clientes != null && !clientes.isEmpty()){
                            client = findFirst(clienteFilter);
                        } else {
                           clienteFilter.addError(new MensajePojo().descripcion("No existe un cliente registrado con éste nro. de documento."));
                        }
                        
                    }
                    
                    if (rubro != null && !rubro.trim().equalsIgnoreCase("")) {
                        CanalVentaParam canalVentaParam = new CanalVentaParam();
                        canalVentaParam.setIdEmpresa(idEmpresa);
                        canalVentaParam.setCodigo(rubro);
                        
                        canalVenta = facades.getCanalVentaFacade().findFirst(canalVentaParam);
                        
                    }
                    
                    
                    //Obtener datos de direccion
                    ReferenciaGeografica dep = null;
                    ReferenciaGeografica dis = null;
                    ReferenciaGeografica ciu = null;
                    
                    if(ciudad != null && departamento != null) {
                        //Se obtiene el departamento
                        final String filtroDep = departamento;
                        Optional<ReferenciaGeografica> resultado = departamentos.stream()
                                                        .filter(ref -> filtroDep.equals(ref.getDescripcion()))
                                                        .findFirst();

                        if (resultado.isPresent()) {
                            dep = resultado.get();
                            // Se obtiene el distrito
                            final String filtroDist = ciudad;
                            Optional<ReferenciaGeografica> resultado1 = distritos.stream()
                                                        .filter(ref -> filtroDist.equals(ref.getDescripcion()))
                                                        .findFirst();

                            if (resultado1.isPresent()) {
                                //Se obtiene la ciudad
                                dis = resultado1.get();
                                
                                Optional<ReferenciaGeografica> resultadoCiudad = null;
                                
                                resultadoCiudad = ciudades.stream()
                                                        .filter(ref -> filtroDist.equals(ref.getDescripcion()))
                                                        .findFirst();
                                if (resultadoCiudad.isPresent()) {
                                    ciu = resultadoCiudad.get();
                                } else {
                                    final Integer filtroCiudad = dis.getId();
                                    resultadoCiudad = distritos.stream()
                                                        .filter(ref -> filtroCiudad.equals(ref.getIdPadre()))
                                                        .findFirst();
                                    ciu = resultadoCiudad.get();
                                    
                                }

                            } else {
                                final String filtroCiu = ciudad;
                                Optional<ReferenciaGeografica> resultado2 = ciudades.stream()
                                                            .filter(ref -> filtroCiu.equals(ref.getDescripcion()))
                                                            .findFirst();
                                if (resultado2.isPresent()) {
                                    ciu = resultado.get();
                                } else {
                                    //Se obtiene cualquier distrito del departamento para asignar por defecto.
                                    final Integer idPadreDis = dep.getId();
                                    Optional<ReferenciaGeografica> defaultDis = distritos.stream()
                                                            .filter(ref -> idPadreDis.equals(ref.getIdPadre()))
                                                            .findFirst();
                                    dis = defaultDis.get();

                                    //Se obtiene cualquier ciudad del distrito para asignar por defecto.
                                    final Integer idPadreCiu = dis.getId();
                                    Optional<ReferenciaGeografica> defaulCiu = ciudades.stream()
                                                            .filter(ref -> idPadreCiu.equals(ref.getIdPadre()))
                                                            .findFirst();
                                    ciu = defaulCiu.get();
                                }

                            }
                            
                            if (client != null && ciu != null && dis != null && dep != null) {
                                client.getPersona().getDireccion().setIdCiudad(ciu.getId());
                                client.getPersona().getDireccion().setIdDistrito(dis.getId());
                                client.getPersona().getDireccion().setIdDepartamento(dep.getId());
                            }
                            
                        } else {
                            clienteFilter.addError(new MensajePojo().descripcion("Departamento, Distrito o Ciudad incorrecto."));
                        }
                        
                        
                    }
                    
                    Cliente editado = null;
                    if (canalVenta != null && client != null && dep != null && dis != null && ciu != null && !clienteFilter.tieneErrores()){
                        client.setIdCanalVenta(canalVenta.getId());
                        editado = edit(client);
                    }
                            
                    if (editado == null) {
                        if (clienteFilter.getRazonSocial() == null && nombre != null){
                            clienteFilter.setRazonSocial(nombre);
                        }
                        String error = String.format("%s;ERROR;", clienteFilter.getRazonSocial());
                        error = error.concat("%s| ");
                        for(MensajePojo mp : clienteFilter.getErrores()){
                            error = String.format(error,mp.getDescripcion());
                            error = error.concat("%s| ");
                        }
                        //Al final se eliminan los ultimos 3 caracteres sobrantes %s;
                        error = error.substring(0,error.length()-5);
                        error=error.concat(";;");
                        result.add(error);
                    } else {
                        String ok = String.format("%s;OK;;%s", client.getRazonSocial(), client.getIdCliente().toString());
                        result.add(ok);
                    }
                }
            }
            
            return result;
        } catch (Exception ex){
            Logger.getLogger(ClienteFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        
    }
    
    @Override
    public List<String> altaMasivaClientes(ClienteParam param, UserInfoImpl userInfo) {
        try {
            List<String> result = new ArrayList();
            List<ClienteParam> cpl = new ArrayList<>();
            InputStreamReader input = new InputStreamReader(new ByteArrayInputStream(param.getUploadedFileBytes()));
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;
            
            Integer idEmpresa = userInfo.getIdEmpresa();

            while (true) {

                line = reader.readLine();

                if (line == null) {
                    break;
                }
                lineas.add(line);
            }

            for (int i = 1; i < lineas.size(); i++) {
                lineas.set(i, lineas.get(i) + ";");
            }

            for (int i = 1; i < lineas.size(); i++) {

                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    String[] stringArr = lineas.get(i).split(";");

                    //Datos de Cliente
                    String nombre = null;
                    String ruc = null;
                    String direccion = null;
                    String telefono = null;
                    String correo = null;

                    ClienteParam cliente = new ClienteParam();
                    cliente.setCodigoEstado("ACTIVO");
                    //TO DO: Obtener naturalezaCliente del archivo csv y idEmpresa del token de la petición.

                    cliente.setIdEmpresa(idEmpresa);
                    cliente.setIdNaturalezaCliente(1);
                    for (int k = 0; k < stringArr.length; k++) {
                        switch (k) {
                            case 0:
                                nombre = (String) stringArr[k];
                                break;
                            case 4:
                                ruc = (String) stringArr[k];
                                break;
                            case 6:
                                direccion = (String) stringArr[k];
                                break;
                            case 7:
                                telefono = (String) stringArr[k];
                                break;
                            case 8:
                                correo = (String) stringArr[k];
                                break;
                        }

                    }
                    DireccionParam direccionParam = new DireccionParam();
                    EmailParam emailParam = new EmailParam();
                    TelefonoParam telParam = new TelefonoParam();

                    direccionParam.setIdDepartamento(1);
                    direccionParam.setIdDistrito(19);
                    direccionParam.setIdCiudad(297);
                    direccionParam.setLatitud("0.0");
                    direccionParam.setLongitud("0.0");
                    if (direccion != null && !direccion.trim().equalsIgnoreCase("")){
                        direccionParam.setDireccion(direccion);
                    } else {
                        direccionParam.setDireccion("Establecer dirección");
                    }
                    
                    direccionParam.setNumero(0);

                    PersonaParam personaParam = new PersonaParam();
                    personaParam.setActivo('S');
                    personaParam.setDireccion(direccionParam);

                    if (telefono != null && !telefono.equalsIgnoreCase("")) {
                        telParam.setPrincipal('S');
                        telParam.setNumero(telefono);
                        telParam.setPrefijo("595");
                        telParam.setCodigoTipoTelefono("TRABAJO");
                        personaParam.setTelefono(telParam);
                    }

                    if (correo != null && !correo.equalsIgnoreCase("")) {
                        emailParam.setEmail(correo);
                        emailParam.setPrincipal('S');
                        emailParam.setCodigoTipoEmail("TRABAJO");
                        personaParam.setEmail(emailParam);
                    }

                    if (ruc != null && !ruc.equalsIgnoreCase(" ")) {
                        if (ruc.contains("-")) {
                            cliente.setIdNaturalezaCliente(1);
                            personaParam.setIdTipoPersona(2);
                        } else {
                            cliente.setIdNaturalezaCliente(2);
                            personaParam.setIdTipoPersona(1);
                        }
                        cliente.setIdTipoDocumento(1);
                        cliente.setCodigoPais("PRY");
                        cliente.setNroDocumento(ruc);
                    }

                    if (nombre != null && !nombre.equalsIgnoreCase("")) {
                        if (cliente.getIdNaturalezaCliente() == 1) {
                            cliente.setRazonSocial(nombre);
                            cliente.setRazonSocial(nombre);
                            personaParam.setNombreFantasia(nombre);
                            personaParam.setRazonSocial(nombre);
                        } else {
                            personaParam.setNombre(nombre);
                            personaParam.setApellido(nombre);
                        }
                    }

                    cliente.setFactElectronica('S');
                    cliente.setPersona(personaParam);
                    cliente.setActivo('S');

                    Cliente nuevoCliente = create(cliente, userInfo);
                    
                    if (nuevoCliente == null) {
                        String error = String.format("%s;ERROR;", cliente.getRazonSocial());
                        error = error.concat("%s| ");
                        for(MensajePojo mp : cliente.getErrores()){
                            error = String.format(error,mp.getDescripcion());
                            error = error.concat("%s| ");
                        }
                        //Al final se eliminan los ultimos 3 caracteres sobrantes %s;
                        error = error.substring(0,error.length()-5);
                        error=error.concat(";;");
                        result.add(error);
                    } else {
                        String ok = String.format("%s;OK;;%s", nuevoCliente.getRazonSocial(), nuevoCliente.getIdCliente().toString());
                        result.add(ok);
                    }

                }
            }
            return result;
        } catch (IOException ex) {
            Logger.getLogger(ClienteFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    @Override
    public List<ClientePojo> findPojo(ClienteParam param) {

        String where = "true";
        
        if (param.getIdCliente() != null) {
            where = String.format(" %s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and c.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdComercial() != null) {
            where = String.format(" %s and c.id_comercial = %d", where, param.getIdComercial());
        }

        if (param.getIdCanalVenta() != null) {
            where = String.format(" %s and c.id_canal_venta = %d", where, param.getIdCanalVenta());
        }

        if (param.getIdEstado() != null) {
            where = String.format(" %s and c.id_estado = %d", where, param.getIdEstado());
        }
        
        if (param.getFactElectronica() != null) {
            where = String.format(" %s and c.fact_electronica = '%s'", where, param.getFactElectronica());
        }
        
        if (param.getComprador() != null) {
            where = String.format(" %s and c.comprador = '%s'", where, param.getComprador());
        }
        
        if (param.getProveedor() != null) {
            where = String.format(" %s and c.proveedor = '%s'", where, param.getProveedor());
        }

        if (param.getPorcentajeRetencion() != null) {
            where = String.format(" %s and c.porcentaje_retencion = %d", where, param.getPorcentajeRetencion());
        }

        if (param.getNroDocumento() != null && !param.getNroDocumento().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento ilike '%%%s%%'", where, param.getNroDocumento().trim());
        }

        if (param.getNroDocumentoEq() != null && !param.getNroDocumentoEq().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento = '%s'", where, param.getNroDocumentoEq().trim());
        }

        if (param.getCodigoCanalVenta() != null && !param.getCodigoCanalVenta().trim().isEmpty()) {
            where = String.format(" %s and cv.codigo = '%s'", where, param.getCodigoCanalVenta().trim());
        }

        if (param.getIdTipoDocumento()  != null) {
            where = String.format(" %s and c.id_tipo_documento = %d", where, param.getIdTipoDocumento());
        }

        if (param.getIdNaturalezaCliente() != null) {
            where = String.format(" %s and c.id_naturaleza_cliente = %d", where, param.getIdNaturalezaCliente());
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format(" %s and c.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }
        
        String sql = String.format("select c.id_cliente, c.id_empresa, em.descripcion as empresa,\n"
                + "c.id_estado, e.descripcion as estado, e.codigo as codigo_estado,\n"
                + "c.proveedor, c.comprador, c.razon_social, c.nro_documento,\n"
                + "c.id_tipo_documento, tdp.descripcion as tipo_documento,\n"
                + "tdp.codigo as codigo_tipo_documento, p.id_tipo_persona,\n"
                + "tp.descripcion as tipo_persona, tp.codigo as codigo_tipo_persona,\n"
                + "c.id_naturaleza_cliente, nc.descripcion as naturaleza_cliente,\n"
                + "nc.codigo as codigo_naturaleza_cliente, c.id_canal_venta,\n"
                + "cv.descripcion as canal_venta, p.id_direccion, d.direccion,\n"
                + "d.id_departamento, rfde.descripcion as departamento, d.id_distrito,\n"
                + "rfdi.descripcion as distrito, d.id_ciudad,\n"
                + "rfci.descripcion as ciudad, d.numero, c.codigo_pais\n"
                + "from comercial.cliente c\n"
                + "join info.persona p on p.id = c.id_cliente\n"
                + "left join info.direccion d on d.id = p.id_direccion\n"
                + "left join info.referencia_geografica rfde on rfde.id = d.id_departamento\n"
                + "left join info.referencia_geografica rfdi on rfdi.id = d.id_distrito\n"
                + "left join info.referencia_geografica rfci on rfci.id = d.id_ciudad\n"
                + "join info.tipo_persona tp on tp.id = p.id_tipo_persona\n"
                + "join info.tipo_dato_persona tdp on tdp.id = c.id_tipo_documento\n"
                + "left join comercial.naturaleza_cliente nc on nc.id = c.id_naturaleza_cliente\n"
                + "left join info.persona com on com.id = c.id_comercial\n"
                + "join info.estado e on e.id = c.id_estado\n"
                + "join info.empresa em on em.id = c.id_empresa\n"
                + "left join info.canal_venta cv on cv.id = c.id_canal_venta\n"
                + "where %s order by e.descripcion, c.id_cliente offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<ClientePojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            ClientePojo item = new ClientePojo();
            item.setIdCliente((Integer)objects[i++]);
            item.setIdEmpresa((Integer)objects[i++]);
            item.setEmpresa((String)objects[i++]);
            item.setIdEstado((Integer)objects[i++]);
            item.setEstado((String)objects[i++]);
            item.setCodigoEstado((String)objects[i++]);
            item.setProveedor((Character)objects[i++]);
            item.setComprador((Character)objects[i++]);
            item.setRazonSocial((String)objects[i++]);
            item.setNroDocumento((String)objects[i++]);
            item.setIdTipoDocumento((Integer)objects[i++]);
            item.setTipoDocumento((String)objects[i++]);
            item.setCodigoTipoDocumento((String)objects[i++]);
            item.setIdTipoPersona((Integer)objects[i++]);
            item.setTipoPersona((String)objects[i++]);
            item.setCodigoTipoPersona((String)objects[i++]);
            item.setIdNaturalezaCliente((Integer)objects[i++]);
            item.setNaturalezaCliente((String)objects[i++]);
            item.setCodigoNaturalezaCliente((String)objects[i++]);
            item.setIdCanalVenta((Integer)objects[i++]);
            item.setCanalVenta((String)objects[i++]);
            item.setIdDireccion((Integer)objects[i++]);
            item.setDireccion((String)objects[i++]);
            item.setIdDepartamento((Integer)objects[i++]);
            item.setDepartamento((String)objects[i++]);
            item.setIdDistrito((Integer)objects[i++]);
            item.setDistrito((String)objects[i++]);
            item.setIdCiudad((Integer)objects[i++]);
            item.setCiudad((String)objects[i++]);
            item.setNumeroDireccion((Integer)objects[i++]);
            item.setCodigoPais((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public List<Cliente> find(ClienteParam param) {

        String where = "true";
        
        if (param.getIdCliente() != null) {
            where = String.format(" %s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and c.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdComercial() != null) {
            where = String.format(" %s and c.id_comercial = %d", where, param.getIdComercial());
        }

        if (param.getIdCanalVenta() != null) {
            where = String.format(" %s and c.id_canal_venta = %d", where, param.getIdCanalVenta());
        }

        if (param.getIdEstado() != null) {
            where = String.format(" %s and c.id_estado = %d", where, param.getIdEstado());
        }
        
        if (param.getFactElectronica() != null) {
            where = String.format(" %s and c.fact_electronica = '%s'", where, param.getFactElectronica());
        }
        
        if (param.getComprador() != null) {
            where = String.format(" %s and c.comprador = '%s'", where, param.getComprador());
        }
        
        if (param.getProveedor() != null) {
            where = String.format(" %s and c.proveedor = '%s'", where, param.getProveedor());
        }

        if (param.getPorcentajeRetencion() != null) {
            where = String.format(" %s and c.porcentaje_retencion = %d", where, param.getPorcentajeRetencion());
        }

        if (param.getNroDocumento() != null && !param.getNroDocumento().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento ilike '%%%s%%'", where, param.getNroDocumento().trim());
        }

        if (param.getNroDocumentoEq() != null && !param.getNroDocumentoEq().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento = '%s'", where, param.getNroDocumentoEq().trim());
        }

        if (param.getCodigoCanalVenta() != null && !param.getCodigoCanalVenta().trim().isEmpty()) {
            where = String.format(" %s and cv.codigo = '%s'", where, param.getCodigoCanalVenta().trim());
        }

        if (param.getIdTipoDocumento()  != null) {
            where = String.format(" %s and c.id_tipo_documento = %d", where, param.getIdTipoDocumento());
        }
        
        if (param.getIdNaturalezaCliente() != null) {
            where = String.format(" %s and c.id_naturaleza_cliente = %d", where, param.getIdNaturalezaCliente());
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format(" %s and c.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }
        
        String sql = String.format("select c.*\n"
                + "from comercial.cliente c\n"
                + "left join info.tipo_dato_persona tdp on tdp.id = c.id_tipo_documento\n"
                + "left join info.persona com on com.id = c.id_comercial\n"
                + "left join info.estado e on e.id = c.id_estado\n"
                + "left join info.canal_venta cv on cv.id = c.id_canal_venta\n"
                + "where %s order by e.descripcion, c.id_cliente offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Cliente.class);
        
        List<Cliente> result = q.getResultList();
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista
     * @param param parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(ClienteParam param) {

        String where = "true";
        
        if (param.getIdCliente() != null) {
            where = String.format(" %s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and c.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdComercial() != null) {
            where = String.format(" %s and c.id_comercial = %d", where, param.getIdComercial());
        }

        if (param.getIdCanalVenta() != null) {
            where = String.format(" %s and c.id_canal_venta = %d", where, param.getIdCanalVenta());
        }

        if (param.getIdEstado() != null) {
            where = String.format(" %s and c.id_estado = %d", where, param.getIdEstado());
        }
        
        if (param.getFactElectronica() != null) {
            where = String.format(" %s and c.fact_electronica = '%s'", where, param.getFactElectronica());
        }
        
        if (param.getComprador() != null) {
            where = String.format(" %s and c.comprador = '%s'", where, param.getComprador());
        }
        
        if (param.getProveedor() != null) {
            where = String.format(" %s and c.proveedor = '%s'", where, param.getProveedor());
        }

        if (param.getPorcentajeRetencion() != null) {
            where = String.format(" %s and c.porcentaje_retencion = %d", where, param.getPorcentajeRetencion());
        }

        if (param.getNroDocumento() != null && !param.getNroDocumento().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento ilike '%%%s%%'", where, param.getNroDocumento().trim());
        }

        if (param.getNroDocumentoEq() != null && !param.getNroDocumentoEq().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento = '%s'", where, param.getNroDocumentoEq().trim());
        }

        if (param.getCodigoCanalVenta() != null && !param.getCodigoCanalVenta().trim().isEmpty()) {
            where = String.format(" %s and cv.codigo = '%s'", where, param.getCodigoCanalVenta().trim());
        }

        if (param.getIdTipoDocumento()  != null) {
            where = String.format(" %s and c.id_tipo_documento = %d", where, param.getIdTipoDocumento());
        }

        if (param.getIdNaturalezaCliente() != null) {
            where = String.format(" %s and c.id_naturaleza_cliente = %d", where, param.getIdNaturalezaCliente());
        }
        
        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format(" %s and c.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }
        
        String sql = String.format("select count(distinct c.id_cliente)\n"
                + "from comercial.cliente c\n"
                + "left join info.canal_venta cv on cv.id = c.id_canal_venta\n"
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
