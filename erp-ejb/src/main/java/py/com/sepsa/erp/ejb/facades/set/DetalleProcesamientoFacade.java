/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.set.DetalleProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.DetalleProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface DetalleProcesamientoFacade extends Facade<DetalleProcesamiento, DetalleProcesamientoParam, UserInfoImpl> {

    public Boolean validToCreate(DetalleProcesamientoParam param, boolean validarProcesamiento);
    
}
