/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de liquidacion
 * @author Jonathan D. Bernal Fernández
 */
public class LiquidacionParam extends CommonParam {
    
    public LiquidacionParam() {
    }

    public LiquidacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de historico liquidacion
     */
    @QueryParam("idHistoricoLiquidacion")
    private Integer idHistoricoLiquidacion;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;
    
    /**
     * Identificador de periodo de liquidacion
     */
    @QueryParam("idPeriodoLiquidacion")
    private Integer idPeriodoLiquidacion;
    
    /**
     * Fecha liquidación
     */
    @QueryParam("fechaLiquidacion")
    private Date fechaLiquidacion;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Identificador de contrato
     */
    @QueryParam("idContrato")
    private Integer idContrato;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Código de servicio
     */
    @QueryParam("codigoServicio")
    private String codigoServicio;
    
    /**
     * Historico liquidación
     */
    private HistoricoLiquidacionParam historicoLiquidacion;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idServicio) && isNullOrEmpty(codigoServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de servicio"));
        }
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente"));
        }
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }
        
        if(isNull(idPeriodoLiquidacion) && isNull(fechaLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de periodo liquidación o fecha de liquidación"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNull(historicoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el historico de liquidación"));
        } else {
            historicoLiquidacion.setIdLiquidacion(0);
            historicoLiquidacion.setIdEmpresa(idEmpresa);
            historicoLiquidacion.isValidToCreate();
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de liquidación"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente"));
        }
        
        if(isNull(idMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de moneda"));
        }
        
        if(isNull(idPeriodoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de periodo liquidación"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        return !tieneErrores();
    }
    
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getCodigoServicio() {
        return codigoServicio;
    }

    public void setCodigoServicio(String codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    public void setFechaLiquidacion(Date fechaLiquidacion) {
        this.fechaLiquidacion = fechaLiquidacion;
    }

    public Date getFechaLiquidacion() {
        return fechaLiquidacion;
    }

    public void setHistoricoLiquidacion(HistoricoLiquidacionParam historicoLiquidacion) {
        this.historicoLiquidacion = historicoLiquidacion;
    }

    public HistoricoLiquidacionParam getHistoricoLiquidacion() {
        return historicoLiquidacion;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdPeriodoLiquidacion(Integer idPeriodoLiquidacion) {
        this.idPeriodoLiquidacion = idPeriodoLiquidacion;
    }

    public Integer getIdPeriodoLiquidacion() {
        return idPeriodoLiquidacion;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = new ArrayList<>();
        
        if(!isNull(historicoLiquidacion)) {
            list.addAll(historicoLiquidacion.getErrores());
        }
        
        return list;
    }
}
