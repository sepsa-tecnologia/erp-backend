/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.Excedente;
import py.com.sepsa.erp.ejb.entities.facturacion.ExcedenteDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ExcedenteDetalleParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ExcedenteDetalleFacade", mappedName = "ExcedenteDetalleFacade")
@Local(ExcedenteDetalleFacade.class)
public class ExcedenteDetalleFacadeImpl extends FacadeImpl<ExcedenteDetalle, ExcedenteDetalleParam> implements ExcedenteDetalleFacade {

    public ExcedenteDetalleFacadeImpl() {
        super(ExcedenteDetalle.class);
    }
    
    /**
     * Valida si el objeto es válido para crear
     * @param param parámetros
     * @param validarHistoricoLiquidacion Validar histórico liquidación
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(ExcedenteDetalleParam param, boolean validarHistoricoLiquidacion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        HistoricoLiquidacion hl = facades
                .getHistoricoLiquidacionFacade()
                .find(param.getIdHistoricoLiquidacion());
        
        if(validarHistoricoLiquidacion && hl == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el histórico liquidación"));
        }
        
        Excedente excedente = facades
                .getExcedenteFacade()
                .find(param.getIdExcedente());
        
        if(excedente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el excedente"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Valida si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(ExcedenteDetalleParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ExcedenteDetalle excedenteDetalle = find(param.getId());
        
        if(excedenteDetalle == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el excedente detalle"));
        }
        
        Excedente excedente = facades
                .getExcedenteFacade()
                .find(param.getIdExcedente());
        
        if(excedente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el excedente"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public ExcedenteDetalle create(ExcedenteDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, Boolean.TRUE)) {
            return null;
        }
        
        ExcedenteDetalle item = new ExcedenteDetalle();
        item.setIdHistoricoLiquidacion(param.getIdHistoricoLiquidacion());
        item.setCantidadExcedida(param.getCantidadExcedida());
        item.setMontoExcedente(param.getMontoExcedente());
        item.setIdExcedente(param.getIdExcedente());
        
        create(item);
        
        return item;
    }
    
    @Override
    public ExcedenteDetalle edit(ExcedenteDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ExcedenteDetalle item = find(param.getId());
        item.setIdHistoricoLiquidacion(param.getIdHistoricoLiquidacion());
        item.setCantidadExcedida(param.getCantidadExcedida());
        item.setMontoExcedente(param.getMontoExcedente());
        item.setIdExcedente(param.getIdExcedente());
        
        edit(item);
        
        return item;
    }
}
