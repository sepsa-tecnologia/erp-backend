/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.google.gson.JsonObject;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.Cobro;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface CobroFacade extends Facade<Cobro, CobroParam, UserInfoImpl> {

    public Cobro anular(CobroParam param, UserInfoImpl userInfo);

    public byte[] getPdfCobro(CobroParam param, UserInfoImpl userInfo) throws Exception;
    
    public byte[] getPdfCobroMasivo(CobroParam param, UserInfoImpl userInfo) throws Exception;

    public Boolean upload(CobroParam param, UserInfoImpl userInfo) throws Exception;
    
    public Long cantidadCobrosEmitidas (CobroParam param);
    
    public Long montoTotalVentas(CobroParam param);
}