/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.auditoria.filters;

import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de auditoria
 * @author Jonathan
 */
public class AuditoriaParam extends CommonParam {

    public AuditoriaParam() {
    }

    public AuditoriaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de auditoria
     */
    @QueryParam("idTipoAuditoria")
    private Integer idTipoAuditoria;
    
    /**
     * Código de tipo de auditoria
     */
    @QueryParam("codigoTipoAuditoria")
    private String codigoTipoAuditoria;
    
    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Auditoria detalle
     */
    private AuditoriaDetalleParam auditoriaDetalle;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoAuditoria) && isNullOrEmpty(codigoTipoAuditoria)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo auditoria"));
        }
        
        if(!isNull(auditoriaDetalle)) {
            auditoriaDetalle.setIdEmpresa(idEmpresa);
            auditoriaDetalle.setIdAuditoria(0);
            auditoriaDetalle.isValidToCreate();
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(auditoriaDetalle)) {
            if(auditoriaDetalle.tieneErrores()) {
                list.addAll(auditoriaDetalle.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoAuditoria() {
        return idTipoAuditoria;
    }

    public void setIdTipoAuditoria(Integer idTipoAuditoria) {
        this.idTipoAuditoria = idTipoAuditoria;
    }

    public String getCodigoTipoAuditoria() {
        return codigoTipoAuditoria;
    }

    public void setCodigoTipoAuditoria(String codigoTipoAuditoria) {
        this.codigoTipoAuditoria = codigoTipoAuditoria;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public void setAuditoriaDetalle(AuditoriaDetalleParam auditoriaDetalle) {
        this.auditoriaDetalle = auditoriaDetalle;
    }

    public AuditoriaDetalleParam getAuditoriaDetalle() {
        return auditoriaDetalle;
    }
}
