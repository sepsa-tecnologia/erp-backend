/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.Cuenta;
import py.com.sepsa.erp.ejb.entities.facturacion.EntidadFinanciera;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CuentaParam;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "CuentaFacade", mappedName = "CuentaFacade")
@Local(CuentaFacade.class)
public class CuentaFacadeImpl extends FacadeImpl<Cuenta, CuentaParam> implements CuentaFacade {

    public CuentaFacadeImpl() {
        super(Cuenta.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(CuentaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        EntidadFinanciera entidadFinanciera = facades
                .getEntidadFinancieraFacade()
                .find(param.getIdEntidadFinanciera());
        
        if(entidadFinanciera == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la entidad financiera"));
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(CuentaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Cuenta cuentaEntidadFinanciera = find(param.getId());
        
        if(cuentaEntidadFinanciera == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la cuenta entidad financiera"));
        }
        
        EntidadFinanciera entidadFinanciera = facades
                .getEntidadFinancieraFacade()
                .find(param.getIdEntidadFinanciera());
        
        if(entidadFinanciera == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la entidad financiera"));
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Cuenta create(CuentaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Cuenta item = new Cuenta();
        item.setFechaAlta(Calendar.getInstance().getTime());
        item.setIdPersona(param.getIdPersona());
        item.setIdEntidadFinanciera(param.getIdEntidadFinanciera());
        item.setNroCuenta(param.getNroCuenta().trim());
        item.setActivo('S');
        
        create(item);
        
        return item;
    }
    
    @Override
    public Cuenta edit(CuentaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Cuenta item = find(param.getId());
        item.setIdPersona(param.getIdPersona());
        item.setIdEntidadFinanciera(param.getIdEntidadFinanciera());
        item.setNroCuenta(param.getNroCuenta().trim());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<Cuenta> find(CuentaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Cuenta.class);
        Root<Cuenta> root = cq.from(Cuenta.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEntidadFinanciera() != null) {
            predList.add(qb.equal(root.get("idEntidadFinanciera"), param.getIdEntidadFinanciera()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("estado")),
                qb.desc(root.get("fechaAlta")));
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<Cuenta> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(CuentaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Cuenta> root = cq.from(Cuenta.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEntidadFinanciera() != null) {
            predList.add(qb.equal(root.get("idEntidadFinanciera"), param.getIdEntidadFinanciera()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
