/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contrato servicio
 * @author Jonathan D. Bernal Fernández
 */
public class ContratoServicioParam extends CommonParam {
    
    public ContratoServicioParam() {
    }

    public ContratoServicioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de contrato
     */
    @QueryParam("idContrato")
    private Integer idContrato;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de Producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Producto
     */
    @QueryParam("producto")
    private String producto;
    
    /**
     * Identificador de Servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Servicio
     */
    @QueryParam("servicio")
    private String servicio;
    
    /**
     * Monto acuerdo
     */
    @QueryParam("montoAcuerdo")
    private BigDecimal montoAcuerdo;
    
    /**
     * Monto acuerdo desde
     */
    @QueryParam("montoAcuerdoDesde")
    private BigDecimal montoAcuerdoDesde;
    
    /**
     * Monto acuerdo hasta
     */
    @QueryParam("montoAcuerdoHasta")
    private BigDecimal montoAcuerdoHasta;
    
    /**
     * Fecha vencimiento acuerdo
     */
    @QueryParam("fechaVencimientoAcuerdo")
    private Date fechaVencimientoAcuerdo;
    
    /**
     * Fecha vencimiento acuerdo desde
     */
    @QueryParam("fechaVencimientoAcuerdoDesde")
    private Date fechaVencimientoAcuerdoDesde;
    
    /**
     * Fecha vencimiento acuerdo hasta
     */
    @QueryParam("fechaVencimientoAcuerdoHasta")
    private Date fechaVencimientoAcuerdoHasta;
    
    /**
     * Identificador de Monto minimo
     */
    @QueryParam("idMontoMinimo")
    private Integer idMontoMinimo;
    
    /**
     * Estado contrato
     */
    @QueryParam("estadoContrato")
    private Character estadoContrato;

    /**
     * Contrato servicio tarifa
     */
    private List<ContratoServicioTarifaParam> tarifas;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idContrato)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de contrato"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNullOrEmpty(tarifas)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar las tarifas asociadas al servicio"));
        } else {
            for (ContratoServicioTarifaParam tarifa : tarifas) {
                tarifa.setIdContrato(0);
                tarifa.setIdServicio(idServicio);
                tarifa.setIdEmpresa(idEmpresa);
                tarifa.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = new ArrayList<>();
        
        if(tarifas != null) {
            for (ContratoServicioTarifaParam tarifa : tarifas) {
                result.addAll(tarifa.getErrores());
            }
        }
        
        return result;
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idContrato)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de contrato"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public BigDecimal getMontoAcuerdo() {
        return montoAcuerdo;
    }

    public void setMontoAcuerdo(BigDecimal montoAcuerdo) {
        this.montoAcuerdo = montoAcuerdo;
    }

    public BigDecimal getMontoAcuerdoDesde() {
        return montoAcuerdoDesde;
    }

    public void setMontoAcuerdoDesde(BigDecimal montoAcuerdoDesde) {
        this.montoAcuerdoDesde = montoAcuerdoDesde;
    }

    public BigDecimal getMontoAcuerdoHasta() {
        return montoAcuerdoHasta;
    }

    public void setMontoAcuerdoHasta(BigDecimal montoAcuerdoHasta) {
        this.montoAcuerdoHasta = montoAcuerdoHasta;
    }

    public Date getFechaVencimientoAcuerdo() {
        return fechaVencimientoAcuerdo;
    }

    public void setFechaVencimientoAcuerdo(Date fechaVencimientoAcuerdo) {
        this.fechaVencimientoAcuerdo = fechaVencimientoAcuerdo;
    }

    public Date getFechaVencimientoAcuerdoDesde() {
        return fechaVencimientoAcuerdoDesde;
    }

    public void setFechaVencimientoAcuerdoDesde(Date fechaVencimientoAcuerdoDesde) {
        this.fechaVencimientoAcuerdoDesde = fechaVencimientoAcuerdoDesde;
    }

    public Date getFechaVencimientoAcuerdoHasta() {
        return fechaVencimientoAcuerdoHasta;
    }

    public void setFechaVencimientoAcuerdoHasta(Date fechaVencimientoAcuerdoHasta) {
        this.fechaVencimientoAcuerdoHasta = fechaVencimientoAcuerdoHasta;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setEstadoContrato(Character estadoContrato) {
        this.estadoContrato = estadoContrato;
    }

    public Character getEstadoContrato() {
        return estadoContrato;
    }

    public void setTarifas(List<ContratoServicioTarifaParam> tarifas) {
        this.tarifas = tarifas;
    }

    public List<ContratoServicioTarifaParam> getTarifas() {
        return tarifas;
    }
}
