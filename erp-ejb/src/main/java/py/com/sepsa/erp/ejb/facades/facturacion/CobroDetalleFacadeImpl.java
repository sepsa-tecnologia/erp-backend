/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.Cheque;
import py.com.sepsa.erp.ejb.entities.facturacion.Cobro;
import py.com.sepsa.erp.ejb.entities.facturacion.CobroDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.ConceptoCobro;
import py.com.sepsa.erp.ejb.entities.facturacion.EntidadFinanciera;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoCobro;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.CobroDetallePojo;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "CobroDetalleFacade", mappedName = "CobroDetalleFacade")
@Local(CobroDetalleFacade.class)
public class CobroDetalleFacadeImpl extends FacadeImpl<CobroDetalle, CobroDetalleParam> implements CobroDetalleFacade {

    public CobroDetalleFacadeImpl() {
        super(CobroDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdCobro validar id cobro
     * @return bandera
     */
    @Override
    public Boolean validToCreate(CobroDetalleParam param, boolean validarIdCobro) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Cobro cobro = facades
                .getCobroFacade()
                .find(param.getIdCobro());
        
        if(validarIdCobro && cobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cobro"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "COBRO_DETALLE");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado de cobro"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        EntidadFinanciera entidadFinanciera = param.getIdEntidadFinanciera() == null
                ? null
                : facades.getEntidadFinancieraFacade().find(param.getIdEntidadFinanciera());
        
        if(param.getIdEntidadFinanciera() != null && entidadFinanciera == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la entidad financiera"));
        }
        
        ConceptoCobro conceptoCobro = facades.getConceptoCobroFacade()
                .find(param.getIdEmpresa(), param.getIdConceptoCobro(),
                        param.getCodigoConceptoCobro());
        
        if(conceptoCobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el concepto de cobro"));
        } else {
            param.setIdConceptoCobro(conceptoCobro.getId());
        }
        
        TipoCobro tipoCobro = facades.getTipoCobroFacade()
                .find(param.getIdTipoCobro(), param.getCodigoTipoCobro());
        
        if(tipoCobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de cobro"));
        } else {
            param.setIdTipoCobro(tipoCobro.getId());
        }
        if(param.getIdFactura()!=null){
            Factura factura = facades
                    .getFacturaFacade()
                    .find(param.getIdFactura());

            if (factura == null) {

                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura de cobro"));
            } else {

                if (factura.getAnulado().equals('S')) {

                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se puede cobrar una factura anulada"));
                }
                
                BigDecimal result = param.getMontoCobro().subtract(factura.getSaldo());
                
                if (result.compareTo(BigDecimal.ONE) > -1) {

                    param.addError(MensajePojo.createInstance()
                            .descripcion("El monto de cobro no puede ser mayor al saldo de la factura"));
                }
            }
        }
        
        
        Cheque cheque = param.getIdCheque() == null
                ? null
                : facades.getChequeFacade().find(param.getIdCheque());
        
        if(param.getIdCheque() != null && cheque == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cheque de cobro"));
        }
        
        if(param.getCheque() != null) {
            facades
                    .getChequeFacade()
                    .validToCreate(param.getCheque());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public CobroDetalle create(CobroDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        Integer idCheque;
        
        if(param.getCheque() != null) {
            Cheque cheque = facades
                    .getChequeFacade()
                    .create(param.getCheque(), userInfo);
            
            idCheque = cheque == null ? null : cheque.getId();
        } else {
            idCheque = param.getIdCheque();
        }
        
        CobroDetalle cobroDetalle = new CobroDetalle();
        cobroDetalle.setIdCobro(param.getIdCobro());
        cobroDetalle.setNroLinea(param.getNroLinea());
        cobroDetalle.setIdFactura(param.getIdFactura());
        cobroDetalle.setIdEstado(param.getIdEstado());
        cobroDetalle.setIdCheque(idCheque);
        cobroDetalle.setIdEntidadFinanciera(param.getIdEntidadFinanciera());
        cobroDetalle.setIdConceptoCobro(param.getIdConceptoCobro());
        cobroDetalle.setIdTipoCobro(param.getIdTipoCobro());
        cobroDetalle.setMontoCobro(param.getMontoCobro());
        cobroDetalle.setDescripcion(param.getDescripcion() == null
                ? null
                : param.getDescripcion().trim());
        cobroDetalle.setMontoCobroGuaranies(param.getMontoCobroGuaranies());
        create(cobroDetalle);
        
        if (cobroDetalle.getMontoCobroGuaranies() != null){
            if (cobroDetalle.getIdFactura() != null) {
                facades.getFacturaFacade().actualizarSaldoFactura(
                        param.getIdFactura(),
                        param.getMontoCobroGuaranies());
            }
        } else {
            if (cobroDetalle.getIdFactura() != null) {
                facades.getFacturaFacade().actualizarSaldoFactura(
                        param.getIdFactura(),
                        param.getMontoCobro());
            }
        }
       
        
        return cobroDetalle;
    }
    
    @Override
    public List<CobroDetallePojo> findPojo(CobroDetalleParam param) {

        AbstractFind find = new AbstractFind(CobroDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idCobro"),
                        getPath("root").get("nroLinea"),
                        getPath("root").get("idFactura"),
                        getPath("factura").get("nroFactura"),
                        getPath("factura").get("fecha"),
                        getPath("root").get("idTipoCobro"),
                        getPath("tipoCobro").get("descripcion"),
                        getPath("tipoCobro").get("codigo"),
                        getPath("root").get("idCheque"),
                        getPath("cheque").get("montoCheque"),
                        getPath("cheque").get("nroCheque"),
                        getPath("cheque").get("fechaEmision"),
                        getPath("root").get("montoCobro"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("idConceptoCobro"),
                        getPath("conceptoCobro").get("descripcion"),
                        getPath("root").get("idEntidadFinanciera"),
                        getPath("entidadFinanciera").get("razonSocial"),
                        getPath("root").get("descripcion"),
                        getPath("cobro").get("nroRecibo")
                );
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<CobroDetalle> find(CobroDetalleParam param) {

        AbstractFind find = new AbstractFind(CobroDetalle.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, CobroDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<CobroDetalle> root = cq.from(CobroDetalle.class);
        Join<CobroDetalle, Estado> estado = root.join("estado");
        Join<CobroDetalle, Cobro> cobro = root.join("cobro");
        Join<CobroDetalle, Factura> factura = root.join("factura", JoinType.LEFT);
        Join<CobroDetalle, TipoCobro> tipoCobro = root.join("tipoCobro");
        Join<CobroDetalle, Persona> entidadFinanciera = root.join("entidadFinanciera", JoinType.LEFT);
        Join<CobroDetalle, Cheque> cheque = root.join("cheque", JoinType.LEFT);
        Join<CobroDetalle, ConceptoCobro> conceptoCobro = root.join("conceptoCobro", JoinType.LEFT);
                
        find.addPath("root", root);
        find.addPath("cobro", cobro);
        find.addPath("estado", estado);
        find.addPath("factura", factura);
        find.addPath("tipoCobro", tipoCobro);
        find.addPath("entidadFinanciera", entidadFinanciera);
        find.addPath("cheque", cheque);
        find.addPath("conceptoCobro", conceptoCobro);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdCobro() != null) {
            predList.add(qb.equal(root.get("idCobro"), param.getIdCobro()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getIdTipoCobro() != null) {
            predList.add(qb.equal(root.get("idTipoCobro"), param.getIdTipoCobro()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdCheque() != null) {
            predList.add(qb.equal(root.get("idCheque"), param.getIdCheque()));
        }
        
        if (param.getIdConceptoCobro() != null) {
            predList.add(qb.equal(root.get("idConceptoCobro"), param.getIdConceptoCobro()));
        }
        
        if (param.getIdEntidadFinanciera() != null) {
            predList.add(qb.equal(root.get("idEntidadFinanciera"), param.getIdEntidadFinanciera()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(CobroDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<CobroDetalle> root = cq.from(CobroDetalle.class);
        Join<CobroDetalle, Estado> estado = root.join("estado");
                
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdCobro() != null) {
            predList.add(qb.equal(root.get("idCobro"), param.getIdCobro()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getIdTipoCobro() != null) {
            predList.add(qb.equal(root.get("idTipoCobro"), param.getIdTipoCobro()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdCheque() != null) {
            predList.add(qb.equal(root.get("idCheque"), param.getIdCheque()));
        }
        
        if (param.getIdConceptoCobro() != null) {
            predList.add(qb.equal(root.get("idConceptoCobro"), param.getIdConceptoCobro()));
        }
        
        if (param.getIdEntidadFinanciera() != null) {
            predList.add(qb.equal(root.get("idEntidadFinanciera"), param.getIdEntidadFinanciera()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
