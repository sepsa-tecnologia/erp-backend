/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad factura detalle
 * @author Jonathan
 */
public class NotaCreditoDetallePojo {

    public NotaCreditoDetallePojo(Integer id, Integer idNotaCredito, Integer nroLinea,
            BigDecimal cantidad, Integer idFactura, String descripcion, Integer porcentajeIva,
            BigDecimal precioUnitarioConIva, BigDecimal precioUnitarioSinIva,
            BigDecimal montoIva, BigDecimal montoImponible, BigDecimal montoTotal,
            String nroNotaCredito, Date fechaNotaCredito, String razonSocial,
            String ruc, String timbradoFactura, String nroFactura, Date fechaFactura,
            String cdcFactura, Character facturaDigital, Character anulado, String codigoMetrica, String metrica,
            BigDecimal montoDescuentoParticular,BigDecimal descuentoParticularUnitario, Date fechaVencimientoLote, String nroLote, String datoAdicional, String codigoInterno,
            BigDecimal porcentajeGravada, BigDecimal montoExentoGravado) {
        this.id = id;
        this.idNotaCredito = idNotaCredito;
        this.nroLinea = nroLinea;
        this.cantidad = cantidad;
        this.idFactura = idFactura;
        this.descripcion = descripcion;
        this.porcentajeIva = porcentajeIva;
        this.precioUnitarioConIva = precioUnitarioConIva;
        this.precioUnitarioSinIva = precioUnitarioSinIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
        this.nroNotaCredito = nroNotaCredito;
        this.fechaNotaCredito = fechaNotaCredito;
        this.razonSocial = razonSocial;
        this.ruc = ruc;
        this.timbradoFactura = timbradoFactura;
        this.nroFactura = nroFactura;
        this.fechaFactura = fechaFactura;
        this.cdcFactura = cdcFactura;
        this.facturaDigital = facturaDigital;
        this.anulado = anulado;
        this.codigoMetrica = codigoMetrica;
        this.metrica = metrica;
        this.montoDescuentoParticular = montoDescuentoParticular;
        this.descuentoParticularUnitario = descuentoParticularUnitario;
        this.fechaVencimientoLote = fechaVencimientoLote;
        this.nroLote = nroLote;
        this.datoAdicional = datoAdicional;
        this.codigoInterno = codigoInterno;
        this.porcentajeGravada = porcentajeGravada;
        this.montoExentoGravado = montoExentoGravado;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de nota de créditp
     */
    private Integer idNotaCredito;
    
    /**
     * Nro nota de crédito
     */
    private String nroNotaCredito;
    
    /**
     * Fecha nota de crédito
     */
    private Date fechaNotaCredito;
    
    /**
     * Razón social
     */
    private String razonSocial;
    
    /**
     * RUC
     */
    private String ruc;
    
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    
    /**
     * Cantidad
     */
    private BigDecimal cantidad;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Timbrado de factura
     */
    private String timbradoFactura;
    
    /**
     * Nro de factura
     */
    private String nroFactura;
    
    /**
     * Fecha factura
     */
    private Date fechaFactura;
    
    /**
     * CDC de factura
     */
    private String cdcFactura;
    
    /**
     * Factura digital
     */
    private Character facturaDigital;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Porcentaje de iva
     */
    private Integer porcentajeIva;
    
    /**
     * Precio unitario con IVA
     */
    private BigDecimal precioUnitarioConIva;
    
    /**
     * Precio unitario sin IVA
     */
    private BigDecimal precioUnitarioSinIva;
    
    /**
     * Monto iva
     */
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Anulado
     */
    private Character anulado;
    
    /**
     * Codigo de metrica
     */
    private String codigoMetrica;
    
    /**
     * Descripcion de metrica
     */
    private String metrica;
    
    /**
     * Porcentaje de descuento particular unitario
     */
    private BigDecimal descuentoParticularUnitario;
    
    /**
     * Monto del descuento particular
     */
    private BigDecimal montoDescuentoParticular;
    
    /**
     * Fecha de vencimiento de lote de producto
     */
    private Date fechaVencimientoLote;
    /**
     * Número de lote de producto
     */
    private String nroLote;
    
    /**
     * Dato adicional
     */
    private String datoAdicional;
    
    private String codigoInterno;
    /**
     * Porcentaje Gravada
     */
    private BigDecimal porcentajeGravada;
    
    /**
     * Monto Exento Gravada
     */
    private BigDecimal montoExentoGravado;
    
    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public void setIdNotaCredito(Integer idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public Integer getIdNotaCredito() {
        return idNotaCredito;
    }
    
    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public Date getFechaNotaCredito() {
        return fechaNotaCredito;
    }

    public void setFechaNotaCredito(Date fechaNotaCredito) {
        this.fechaNotaCredito = fechaNotaCredito;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setFacturaDigital(Character facturaDigital) {
        this.facturaDigital = facturaDigital;
    }

    public Character getFacturaDigital() {
        return facturaDigital;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }  

    public String getMetrica() {
        return metrica;
    }

    public void setMetrica(String metrica) {
        this.metrica = metrica;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public BigDecimal getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(BigDecimal porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }
    
}
