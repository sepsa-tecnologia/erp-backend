/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad retencion detalle
 * @author Jonathan
 */
public class RetencionDetallePojo {

    public RetencionDetallePojo(Integer idRetencion, Date fecha, String nroRetencion, Integer porcentajeRetencion, Integer idFactura, BigDecimal montoImponible, BigDecimal montoIva, BigDecimal montoTotal, BigDecimal montoRetenido) {
        this.idRetencion = idRetencion;
        this.fecha = fecha;
        this.nroRetencion = nroRetencion;
        this.porcentajeRetencion = porcentajeRetencion;
        this.idFactura = idFactura;
        this.montoImponible = montoImponible;
        this.montoIva = montoIva;
        this.montoTotal = montoTotal;
        this.montoRetenido = montoRetenido;
    }
    
    /**
     * Identificador de retencion
     */
    private Integer idRetencion;
    
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * Nro de retención
     */
    private String nroRetencion;
    
    /**
     * Porcentaje de retención
     */
    private Integer porcentajeRetencion;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    
    /**
     * Monto iva
     */
    private BigDecimal montoIva;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Monto retenido
     */
    private BigDecimal montoRetenido;

    public Integer getIdRetencion() {
        return idRetencion;
    }

    public void setIdRetencion(Integer idRetencion) {
        this.idRetencion = idRetencion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getMontoRetenido() {
        return montoRetenido;
    }

    public void setMontoRetenido(BigDecimal montoRetenido) {
        this.montoRetenido = montoRetenido;
    }
}
