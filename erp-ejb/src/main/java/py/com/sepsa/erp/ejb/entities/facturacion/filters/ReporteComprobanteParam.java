/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.Calendar;
import java.util.Date;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de reporte de comprobante
 * @author Jonathan D. Bernal Fernández
 */
public class ReporteComprobanteParam extends CommonParam {
    
    public ReporteComprobanteParam() {
    }

    public ReporteComprobanteParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Fecha
     */
    @PathParam("fecha")
    private Date fecha;
    
    /**
     * Anual
     */
    @PathParam("anual")
    private Boolean anual;
    
        
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Fecha desde
     */
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    private Date fechaHasta;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToList() {
        
        limpiarErrores();
        
        super.isValidToList();
        
        if(isNull(anual)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el reporte es anual"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha del reporte"));
        }
        
        if(!isNull(anual) && !isNull(fecha)) {
            
            Calendar from = Calendar.getInstance();
            from.setTime(fecha);
            from.set(Calendar.HOUR_OF_DAY, 0);
            from.set(Calendar.MINUTE, 0);
            from.set(Calendar.SECOND, 0);
            from.set(Calendar.MILLISECOND, 0);
            
            Calendar to = Calendar.getInstance();
            to.setTime(fecha);
            to.set(Calendar.HOUR_OF_DAY, 23);
            to.set(Calendar.MINUTE, 59);
            to.set(Calendar.SECOND, 59);
            to.set(Calendar.MILLISECOND, 999);
            
            if(anual) {
                from.set(Calendar.MONTH, from.getActualMinimum(Calendar.MONTH));
                to.set(Calendar.MONTH, to.getActualMaximum(Calendar.MONTH));
            }
            
            from.set(Calendar.DAY_OF_MONTH, from.getActualMinimum(Calendar.DAY_OF_MONTH));
            to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
            
            fechaDesde = from.getTime();
            fechaHasta = to.getTime();
        }
        
        return !tieneErrores();
    }
    
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getAnual() {
        return anual;
    }

    public void setAnual(Boolean anual) {
        this.anual = anual;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

}
