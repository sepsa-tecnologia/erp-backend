/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionCompraParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface RetencionCompraFacade extends Facade<RetencionCompra, RetencionCompraParam, UserInfoImpl> {

    public RetencionCompra anular(RetencionCompraParam param, UserInfoImpl userInfo);

    public Boolean delete(RetencionCompraParam param, UserInfoImpl userInfo);
    
}