/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de solicitud
 * @author Williams 
 */
public class SolicitudParam extends CommonParam {
    
    public SolicitudParam() {
    }

    public SolicitudParam(String bruto) {
        super(bruto);
    }
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Razón Social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * Identificador de cliente sepsa
     */
    @QueryParam("direccion")
    private String direccion;
    
    /**
     * Número de teléfono
     */
    @QueryParam("nroTelefono")
    private String nroTelefono;
    
    /**
     * Email
     */
    @QueryParam("email")
    private String email;
    
    /**
     * Nombre del contacto
     */
    @QueryParam("nombreContacto")
    private String nombreContacto;
    
    /**
     * Apellido del contacto
     */
    @QueryParam("apellidoContacto")
    private String apellidoContacto;
    
    /**
     * Email del contacto
     */
    @QueryParam("emailContacto")
    private String emailContacto;
    
    /**
     * Telefono del contacto
     */
    @QueryParam("telefonoContacto")
    private String telefonoContacto;
    
    /**
     * Telefono del contacto
     */
    @QueryParam("varificado")
    private String verificado;
    
    /**
     * Hash
     */
    @QueryParam("hash")
    private String hash;
    
    /**
     * API Url
     */
    @QueryParam("urlDestino")
    private String urlDestino;
    

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        

        
        if(isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social de la empresa"));
        }
        
        if(isNullOrEmpty(direccion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la dirección de la empresa"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC de la empresa"));
        }
        
        if(isNullOrEmpty(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email de la empresa"));
        }
        
        if(isNullOrEmpty(nroTelefono)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el teléfono de la empresa"));
        }
        
        if(isNullOrEmpty(nombreContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre del contacto"));
        }
        
        if(isNullOrEmpty(apellidoContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el apellido del contacto"));
        }
        
        if(isNullOrEmpty(telefonoContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el teléfono del contacto"));
        }
        
        if(isNullOrEmpty(emailContacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email del contacto"));
        
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC de la empresa"));
        }
        
        if(isNullOrEmpty(verificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la solicitud ya fue verificada"));
        }
       
        return !tieneErrores();
    }
    
    public boolean isValidToSendVerification() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email"));
        }
        
        if(isNullOrEmpty(urlDestino)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la URL destino"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToVerificate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(hash)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el hash"));
        }
        
        return !tieneErrores();
    }
    

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getApellidoContacto() {
        return apellidoContacto;
    }

    public void setApellidoContacto(String apellidoContacto) {
        this.apellidoContacto = apellidoContacto;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getVerificado() {
        return verificado;
    }

    public void setVerificado(String verificado) {
        this.verificado = verificado;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getUrlDestino() {
        return urlDestino;
    }

    public void setUrlDestino(String urlDestino) {
        this.urlDestino = urlDestino;
    }
    
}
