/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils.jwe;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.Map;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * Clase para el manejo de la información del usuario
 * @author Jonathan D. Bernal Fernández
 */
public class UserInfoImpl implements CustomPrincipal.UserInfo {
    
    /**
     * Genera una instancia desde un JsonObject
     * @param json JsonObject
     * @return UserInfoImpl
     */
    public static UserInfoImpl generate(JsonObject json) {
        return generate(null, json);
    }
    
    /**
     * Genera una instancia desde un JsonObject
     * @param token Token
     * @param json JsonObject
     * @return UserInfoImpl
     */
    public static UserInfoImpl generate(String token, JsonObject json) {
        UserInfoImpl user = new UserInfoImpl();
        user.setToken(token);
        user.setModuloInventario('N');
        user.setTieneModuloInventario(false);
        for (Map.Entry<String, JsonElement> entry : json.entrySet()) {
            
            JsonElement value = entry.getValue();
            
            if(value == null || value.isJsonNull()) {
                continue;
            }
            
            switch(entry.getKey()) {
                
                case "id":
                    user.setId(value.getAsInt());
                    break;
                    
                case "idEmpresa":
                    user.setIdEmpresa(value.getAsInt());
                    break;
                    
                case "idClienteSepsa":
                    user.setIdClienteSepsa(value.getAsInt());
                    break;
                    
                case "idTipoEmpresa":
                    user.setIdTipoEmpresa(value.getAsInt());
                    break;
                    
                case "codigoTipoEmpresa":
                    user.setCodigoTipoEmpresa(value.getAsString());
                    break;
                    
                case "moduloInventario":
                    user.setModuloInventario(value.getAsCharacter());
                    user.setTieneModuloInventario(value.getAsCharacter() == 'S');
                    break;
                    
                case "usuario":
                    user.setUsuario(value.getAsString());
                    break;
                    
                case "idPersonaFisica":
                    user.setIdPersonaFisica(value.getAsInt());
                    break;
                    
                case "nombre":
                    user.setNombre(value.getAsString());
                    break;
                    
                case "apellido":
                    user.setApellido(value.getAsString());
                    break;
            }
        }
        
        return user;
    }
    
    /**
     * Token de usuario
     */
    private String token;
    
    /**
     * Identificador del usuario
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Identificador de cliente sepsa
     */
    private Integer idClienteSepsa;
    
    /**
     * Identificador de tipo de empresa
     */
    private Integer idTipoEmpresa;
    
    /**
     * Código tipo de empresa
     */
    private String codigoTipoEmpresa;
    
    /**
     * Módulo de inventario
     */
    private Character moduloInventario;
    
    /**
     * Tiene módulo de inventario
     */
    private Boolean tieneModuloInventario;
    
    /**
     * Usuario
     */
    private String usuario;
    
    /**
     * Identificador de persona física
     */
    private Integer idPersonaFisica;
    
    /**
     * Nombre 
     */
    private String nombre;
    
    /**
     * Apellido
     */
    private String apellido;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setIdPersonaFisica(Integer idPersonaFisica) {
        this.idPersonaFisica = idPersonaFisica;
    }

    public Integer getIdPersonaFisica() {
        return idPersonaFisica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdClienteSepsa(Integer idClienteSepsa) {
        this.idClienteSepsa = idClienteSepsa;
    }

    public Integer getIdClienteSepsa() {
        return idClienteSepsa;
    }

    public void setIdTipoEmpresa(Integer idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    public Integer getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    public void setCodigoTipoEmpresa(String codigoTipoEmpresa) {
        this.codigoTipoEmpresa = codigoTipoEmpresa;
    }

    public String getCodigoTipoEmpresa() {
        return codigoTipoEmpresa;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setModuloInventario(Character moduloInventario) {
        this.moduloInventario = moduloInventario;
    }

    public Character getModuloInventario() {
        return moduloInventario;
    }

    public void setTieneModuloInventario(Boolean tieneModuloInventario) {
        this.tieneModuloInventario = tieneModuloInventario;
    }

    public Boolean getTieneModuloInventario() {
        return tieneModuloInventario;
    }
}
