/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Motivo;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.ControlInventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.ControlInventarioDetallePojo;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.ControlInventarioPojo;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioPojo;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.misc.Strings;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ControlInventarioFacade", mappedName = "ControlInventarioFacade")
@javax.ejb.Local(ControlInventarioFacade.class)
public class ControlInventarioFacadeImp extends FacadeImpl<ControlInventario, ControlInventarioParam> implements ControlInventarioFacade {

    public ControlInventarioFacadeImp() {
        super(ControlInventario.class);
    }

    public Boolean validToCreate(ControlInventarioParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        UsuarioParam ueparam = new UsuarioParam();
        ueparam.setId(param.getIdUsuarioEncargado());
        Long uesize = facades.getUsuarioFacade().findSize(ueparam);
        if (uesize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el usuario encargado"));
        }

        UsuarioParam usparam = new UsuarioParam();
        usparam.setId(param.getIdUsuarioSupervisor());
        Long ussize = facades.getUsuarioFacade().findSize(usparam);
        if (ussize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el usuario supervisor"));
        }

        MotivoPojo motivo = facades.getMotivoFacade().find(param.getIdMotivo(), param.getCodigoMotivo(), "CONTROL_INVENTARIO");
        if (motivo == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de control de inventario"));
        } else {
            param.setIdMotivo(motivo.getId());
            param.setCodigoMotivo(motivo.getCodigo());
        }

        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "CONTROL_INVENTARIO");
        if (estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }

        if (param.getFechaInicio() != null && param.getFechaFin() != null) {
            if (param.getFechaInicio().compareTo(param.getFechaFin()) > 0) {
                param.addError(MensajePojo.createInstance().descripcion("Se debe indicar un rango de fecha válido"));
            }
        }

        if (!isNullOrEmpty(param.getControlInventarioDetalles())) {
            for (ControlInventarioDetalleParam detalle : param.getControlInventarioDetalles()) {
                facades.getControlInventarioDetalleFacade().validToCreate(detalle, false);
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(ControlInventarioParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "CONTROL_INVENTARIO");
        if (estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        ControlInventarioParam param1 = new ControlInventarioParam();
        param1.setId(param.getId());
        ControlInventarioPojo controlInventario = findFirstPojo(param1);
        if (controlInventario == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el control de inventario"));
        } else if (param.getCodigoEstado() != null
                && controlInventario.getCodigoEstado().equals("CONFIRMADO")
                && !controlInventario.getCodigoEstado().equals(param.getCodigoEstado())) {
            param.addError(MensajePojo.createInstance().descripcion("No se puede editar una operación confirmada"));
        }

        UsuarioParam ueparam = new UsuarioParam();
        ueparam.setId(param.getIdUsuarioEncargado());
        Long uesize = facades.getUsuarioFacade().findSize(ueparam);
        if (uesize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el usuario encargado"));
        }

        UsuarioParam usparam = new UsuarioParam();
        usparam.setId(param.getIdUsuarioSupervisor());
        Long ussize = facades.getUsuarioFacade().findSize(usparam);
        if (ussize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el usuario supervisor"));
        }

        MotivoPojo motivo = facades.getMotivoFacade().find(param.getIdMotivo(), param.getCodigoMotivo(), "CONTROL_INVENTARIO");
        if (motivo == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de operación"));
        } else {
            param.setIdMotivo(motivo.getId());
            param.setCodigoMotivo(motivo.getCodigo());
        }

        if (param.getFechaInicio() != null && param.getFechaFin() != null) {
            if (param.getFechaInicio().compareTo(param.getFechaFin()) > 0) {
                param.addError(MensajePojo.createInstance().descripcion("Se debe indicar un rango de fecha válido"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToComplete(ControlInventarioParam param) {
        
        if(!param.isValidToComplete()) {
            return Boolean.FALSE;
        }
        
        ControlInventarioParam param1 = new ControlInventarioParam();
        param1.setId(param.getId());
        ControlInventarioPojo controlInventario = findFirstPojo(param1);
        if (controlInventario == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el control de inventario"));
        }
        
        ControlInventarioDetalleParam param2 = new ControlInventarioDetalleParam();
        param2.setIdControlInventario(param.getId());
        param2.isValidToList();
        List<ControlInventarioDetallePojo> list = facades.getControlInventarioDetalleFacade().findPojo(param2);
        if(list.size() != param.getControlInventarioDetalles().size()) {
            param.addError(MensajePojo.createInstance().descripcion("El detalle del control de inventario debe coincidir"));
        } else {
            a:
            for (ControlInventarioDetallePojo item : list) {
                for (ControlInventarioDetalleParam detalle : param.getControlInventarioDetalles()) {
                    if(Objects.equals(detalle.getIdDepositoLogistico(), item.getIdDepositoLogistico())
                            && Objects.equals(detalle.getIdProducto(), item.getIdProducto())
                            && Objects.equals(detalle.getFechaVencimiento(), item.getFechaVencimiento()))
                        continue a;
                }
                param.addError(MensajePojo.createInstance().descripcion("El detalle del control de inventario debe coincidir"));
                break;
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToCreateTemplate(ControlInventarioParam param) {
        
        if(!param.isValidToCreateTemplate()) {
            return Boolean.FALSE;
        }
        
        param.setControlInventarioDetalles(new ArrayList<>());
        
        List<String> ids = new ArrayList<>();
        
        InventarioParam param1 = new InventarioParam();
        param1.setIdDepositoLogisticos(param.getIdDepositoLogisticos());
        param1.isValidToList();
        List<InventarioPojo> list = facades.getInventarioFacade().findResumen(param1);
        for (InventarioPojo item : list) {
            ControlInventarioDetalleParam param2 = new ControlInventarioDetalleParam();
            param2.setIdDepositoLogistico(item.getIdDepositoLogistico());
            param2.setIdProducto(item.getIdProducto());
            param2.setCantidadInicial(item.getCantidad());
            if(!ids.contains(item.getKey())) {
                ids.add(item.getKey());
                param.getControlInventarioDetalles().add(param2);
            }
        }
        
        param1 = new InventarioParam();
        param1.setIdProductos(param.getIdProductos());
        param1.isValidToList();
        list = facades.getInventarioFacade().findResumen(param1);
        for (InventarioPojo item : list) {
            ControlInventarioDetalleParam param2 = new ControlInventarioDetalleParam();
            param2.setIdDepositoLogistico(item.getIdDepositoLogistico());
            param2.setIdProducto(item.getIdProducto());
            param2.setCantidadInicial(item.getCantidad());
            if(!ids.contains(item.getKey())) {
                ids.add(item.getKey());
                param.getControlInventarioDetalles().add(param2);
            }
        }
        
        validToCreate(param);
        
        return !param.tieneErrores();
    }
    
    public Boolean validToChangeState(ControlInventarioParam param) {
        
        if(!param.isValidToChangeState()) {
            return Boolean.FALSE;
        }
        
        ControlInventarioParam param1 = new ControlInventarioParam();
        param1.setId(param.getId());
        ControlInventarioPojo controlInventario = findFirstPojo(param1);
        if(controlInventario == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el control de inventario"));
        } else {
            param.setIdUsuarioEncargado(controlInventario.getIdUsuarioEncargado());
            param.setIdUsuarioSupervisor(controlInventario.getIdUsuarioSupervisor());
            param.setIdMotivo(controlInventario.getIdMotivo());
            param.setCodigoMotivo(controlInventario.getCodigoMotivo());
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "CONTROL_INVENTARIO");
        if(estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(controlInventario != null && estado != null) {
            
            if(controlInventario.getCodigoEstado().equals("CONFIRMADO")
                    && !estado.getCodigo().equals("CONFIRMADO")) {
                param.addError(MensajePojo.createInstance().descripcion("No se puede cambiar de estado un control de inventario confirmado"));
            }
            
            if(param.getChangeState() == null) {
                param.setChangeState(!controlInventario.getIdEstado().equals(estado.getId()));
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToGetPdf(ControlInventarioParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        Long fsize = findSize(param);

        if (fsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el control de inventario"));
        }

        return !param.tieneErrores();
    }
    
    @Override
    public ControlInventario create(ControlInventarioParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        String codigo = Strings.randomAlphanumeric(10).toUpperCase();
        while (true) {            
            ControlInventarioParam ciparam = new ControlInventarioParam();
            ciparam.setCodigo(codigo);
            ciparam.setIdEmpresa(param.getIdEmpresa());
            Long size = findSize(ciparam);
            if(size <= 0) {
                break;
            } else {
                codigo = Strings.randomAlphanumeric(10).toUpperCase();
            }
        }
        
        ControlInventario result = new ControlInventario();
        result.setCodigo(codigo);
        result.setIdEmpresa(param.getIdEmpresa());
        result.setIdUsuarioEncargado(param.getIdUsuarioEncargado());
        result.setIdUsuarioSupervisor(param.getIdUsuarioSupervisor());
        result.setIdMotivo(param.getIdMotivo());
        result.setIdEstado(param.getIdEstado());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());

        create(result);

        if (!isNullOrEmpty(param.getControlInventarioDetalles())) {
            for (ControlInventarioDetalleParam detalle : param.getControlInventarioDetalles()) {
                detalle.setIdControlInventario(result.getId());
                facades.getControlInventarioDetalleFacade().create(detalle, userInfo);
            }
        }

        return result;
    }

    @Override
    public ControlInventario edit(ControlInventarioParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        ControlInventario result = find(param.getId());
        result.setIdUsuarioEncargado(param.getIdUsuarioEncargado());
        result.setIdUsuarioSupervisor(param.getIdUsuarioSupervisor());
        result.setIdMotivo(param.getIdMotivo());
        result.setIdEstado(param.getIdEstado());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());

        edit(result);

        return result;
    }

    @Override
    public ControlInventario complete(ControlInventarioParam param, UserInfoImpl userInfo) {

        if (!validToComplete(param)) {
            return null;
        }

        ControlInventario result = find(param.getId());
        result.setIdEstado(param.getIdEstado());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());

        edit(result);
        
        for (ControlInventarioDetalleParam detalle : param.getControlInventarioDetalles()) {
            facades.getControlInventarioDetalleFacade().edit(detalle, userInfo);
        }

        return result;
    }
    
    @Override
    public ControlInventario changeState(ControlInventarioParam param, UserInfoImpl userInfo) {
        
        if(!validToChangeState(param)) {
            return null;
        }
        
        ControlInventario result = null;
        
        if(param.getChangeState() && param.getCodigoEstado().equals("CONFIRMADO")) {

            List<ControlInventarioDetallePojo> detalles = facades
                    .getControlInventarioDetalleFacade()
                    .findByIdControlInventarioPojo(param.getId());

            for (ControlInventarioDetallePojo detalle : detalles) {
                facades.getInventarioDetalleFacade().find(
                        detalle.getIdDepositoLogistico(),
                        detalle.getIdProducto(), null, "DISPONIBLE",
                        detalle.getFechaVencimiento());
            }
        }

        result = find(param.getId());
        result.setIdEstado(param.getIdEstado());
        edit(result);
        
        return result;
    }

    @Override
    public ControlInventario createTemplate(ControlInventarioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreateTemplate(param)) {
            return null;
        }
        
        return create(param, userInfo);
    }
    
    @Override
    public List<ControlInventarioPojo> findPojo(ControlInventarioParam param) {

        AbstractFind find = new AbstractFind(ControlInventarioPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("codigo"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idUsuarioEncargado"),
                        getPath("usuarioEncargado").get("usuario"),
                        getPath("root").get("idUsuarioSupervisor"),
                        getPath("usuarioSupervisor").get("usuario"),
                        getPath("root").get("idMotivo"),
                        getPath("motivo").get("descripcion"),
                        getPath("motivo").get("codigo"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("fechaInicio"),
                        getPath("root").get("fechaFin"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<ControlInventario> find(ControlInventarioParam param) {

        AbstractFind find = new AbstractFind(ControlInventario.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, ControlInventarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<ControlInventario> root = cq.from(ControlInventario.class);
        Join<ControlInventario, Motivo> motivo = root.join("motivo");
        Join<ControlInventario, Empresa> empresa = root.join("empresa");
        Join<ControlInventario, Estado> estado = root.join("estado");
        Join<ControlInventario, Usuario> usuarioEncargado = root.join("usuarioEncargado");
        Join<ControlInventario, Usuario> usuarioSupervisor = root.join("usuarioSupervisor");

        find.addPath("root", root);
        find.addPath("estado", estado);
        find.addPath("empresa", empresa);
        find.addPath("motivo", motivo);
        find.addPath("usuarioEncargado", usuarioEncargado);
        find.addPath("usuarioSupervisor", usuarioSupervisor);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getIdUsuarioEncargado() != null) {
            predList.add(qb.equal(root.get("idUsuarioEncargado"), param.getIdUsuarioEncargado()));
        }

        if (param.getIdUsuarioSupervisor() != null) {
            predList.add(qb.equal(root.get("idUsuarioSupervisor"), param.getIdUsuarioSupervisor()));
        }

        if (param.getIdMotivo() != null) {
            predList.add(qb.equal(root.get("idMotivo"), param.getIdMotivo()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);
        
        cq.orderBy(qb.desc(root.get("fechaInsercion")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ControlInventarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ControlInventario> root = cq.from(ControlInventario.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getIdUsuarioEncargado() != null) {
            predList.add(qb.equal(root.get("idUsuarioEncargado"), param.getIdUsuarioEncargado()));
        }

        if (param.getIdUsuarioSupervisor() != null) {
            predList.add(qb.equal(root.get("idUsuarioSupervisor"), param.getIdUsuarioSupervisor()));
        }

        if (param.getIdMotivo() != null) {
            predList.add(qb.equal(root.get("idMotivo"), param.getIdMotivo()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;

        return result;
    }
    
    @Override
    public byte[] getPdfControlInventario(ControlInventarioParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        ControlInventarioPojo cip = findFirstPojo(param);
        
        byte[] result = null;

        Closer closer = Closer.instance();

        try {

            py.com.sepsa.erp.reporte.pojos.inventario.ControlInventarioParam params
                    = facades.getReportUtils().getControlInventarioParam(closer, cip);

            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_CONTROL_INVENTARIO");
            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);

            if (isNull(is)) {
                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el control de inventario"));
            } else {
                //Obtenemos una conexion del DataSource
                Context ctx = new InitialContext();
                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                try (Connection connection = ds.getConnection()) {
                    result = JasperReporteGenerator.exportReportToPdfBytes(params, connection, is);
                }
            }

        } finally {
            closer.close();
        }

        return result;
    }
}
