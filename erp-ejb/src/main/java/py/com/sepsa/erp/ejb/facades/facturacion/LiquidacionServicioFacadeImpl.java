/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionServicio;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionServicioParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LiquidacionServicioFacade", mappedName = "LiquidacionServicioFacade")
@Local(LiquidacionServicioFacade.class)
public class LiquidacionServicioFacadeImpl extends FacadeImpl<LiquidacionServicio, LiquidacionServicioParam> implements LiquidacionServicioFacade {

    public LiquidacionServicioFacadeImpl() {
        super(LiquidacionServicio.class);
    }
    
    public Boolean validToCreate(LiquidacionServicioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        LiquidacionServicioParam param1 = new LiquidacionServicioParam();
        param1.setIdServicio(param.getIdServicio());
        param1.setIdCliente(param.getIdCliente());
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        }
        
        if(param.getIdCliente() != null) {
            
            Cliente cliente = facades
                    .getClienteFacade()
                    .find(param.getIdCliente());
            
            if(cliente == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(LiquidacionServicioParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        LiquidacionServicioParam param1 = new LiquidacionServicioParam();
        param1.setIdServicio(param.getIdServicio());
        param1.setIdCliente(param.getIdCliente());
        List<LiquidacionServicio> list = find(param1);
        
        if(list == null || list.isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un registro"));
        } else {
            param.setId(list.get(0).getId());
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        }
        
        if(param.getIdCliente() != null) {
            
            Cliente cliente = facades
                    .getClienteFacade()
                    .find(param.getIdCliente());
            
            if(cliente == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el cliente"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    public LiquidacionServicio create(LiquidacionServicioParam param) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        LiquidacionServicio item = new LiquidacionServicio();
        item.setIdServicio(param.getIdServicio());
        item.setIdCliente(param.getIdCliente());
        item.setDescripcion(param.getDescripcion());
        item.setBloqueable(param.getBloqueable());
        item.setActivo(param.getActivo());
        create(item);
        
        return item;
    }
    
    public LiquidacionServicio edit(LiquidacionServicioParam param) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        LiquidacionServicio item = find(param.getId());
        item.setIdServicio(param.getIdServicio());
        item.setIdCliente(param.getIdCliente());
        item.setDescripcion(param.getDescripcion());
        item.setBloqueable(param.getBloqueable());
        item.setActivo(param.getActivo());
        edit(item);
        
        return item;
    }
    
    public LiquidacionServicio editOrCreate(LiquidacionServicioParam param) {
        
        LiquidacionServicio item = edit(param);
        
        if(item == null) {
            item = create(param);
        }
        
        return item;
    }
    
    @Override
    public List<LiquidacionServicio> find(LiquidacionServicioParam param) {
        
        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ls.id = %d", where, param.getId());
        }
        
        if(param.getIdProducto() != null) {
            where = String.format("%s and s.id_producto = %d", where, param.getIdProducto());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and ls.id_servicio = %d", where, param.getIdServicio());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format("%s and ls.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format("%s and ls.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }
        
        if(param.getBloqueable() != null) {
            where = String.format("%s and ls.bloqueable = '%s'", where, param.getBloqueable());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ls.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select ls.*"
                + " from facturacion.liquidacion_servicio ls"
                + " left join comercial.servicio s on s.id = ls.id_servicio"
                + " left join comercial.producto p on p.id = s.id_producto"
                + " left join comercial.cliente c on c.id_cliente = ls.id_cliente"
                + " where %s order by ls.id_servicio offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<LiquidacionServicio> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(LiquidacionServicioParam param) {
        
        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ls.id = %d", where, param.getId());
        }
        
        if(param.getIdProducto() != null) {
            where = String.format("%s and s.id_producto = %d", where, param.getIdProducto());
        }
        
        if(param.getIdServicio() != null) {
            where = String.format("%s and ls.id_servicio = %d", where, param.getIdServicio());
        }
        
        if(param.getIdCliente() != null) {
            where = String.format("%s and ls.id_cliente = %d", where, param.getIdCliente());
        }
        
        if(param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format("%s and ls.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }
        
        if(param.getBloqueable() != null) {
            where = String.format("%s and ls.bloqueable = '%s'", where, param.getBloqueable());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ls.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(ls.id)"
                + " from facturacion.liquidacion_servicio ls"
                + " left join comercial.servicio s on s.id = ls.id_servicio"
                + " left join comercial.producto p on p.id = s.id_producto"
                + " left join comercial.cliente c on c.id_cliente = ls.id_cliente"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
