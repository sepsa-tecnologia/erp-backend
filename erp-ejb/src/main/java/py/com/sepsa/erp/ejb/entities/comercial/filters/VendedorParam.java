/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Gustavo Benítez
 */
public class VendedorParam extends CommonParam {

    public VendedorParam() {
    }

    public VendedorParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de tipo persona
     */
    @QueryParam("idTipoPersona")
    private Integer idTipoPersona;
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;

    /**
     * Nombre del Vendedor
     */
    @QueryParam("nombre")
    private String nombre;

    /**
     * Apellido del Vendedor
     */
    @QueryParam("apellido")
    private String apellido;

    private PersonaParam persona;

    @Override
    public boolean isValidToCreate() {
        limpiarErrores();

        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Empresa"));
        }

        if (isNull(nombre)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre del Vendedor"));
        }

        if (isNull(apellido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el apellido del Vendedor"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de actividad del vendedor"));
        }

        if (isNull(idPersona) && isNull(persona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona o información de la persona"));
        }
        
        if (isNull(idPersona)) {
            this.idPersona = 0;
        }

        if (!isNull(persona)) {
            persona.setIdEmpresa(idEmpresa);
            persona.isValidToCreate();
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {

        List<MensajePojo> list = super.getCustomErrores();

        if (!isNull(persona)) {
            if (persona.tieneErrores()) {
                list.addAll(persona.getErrores());
            }
        }

        return list;
    }

//<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">

    public Integer getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(Integer idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public PersonaParam getPersona() {
        return persona;
    }

    public void setPersona(PersonaParam persona) {
        this.persona = persona;
    }
    

//</editor-fold>
    @Override
    public boolean isValidToEdit() {
       limpiarErrores();

        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo el vendedor"));
        }

        if (isNull(idPersona) && isNull(persona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de persona o información de la persona"));
        }
        
        if (isNull(idPersona)) {
            this.idPersona = 0;
        }

        if (!isNull(persona)) {
            persona.setIdEmpresa(idEmpresa);
            persona.isValidToCreate();
        }

        return !tieneErrores();
    }

}
