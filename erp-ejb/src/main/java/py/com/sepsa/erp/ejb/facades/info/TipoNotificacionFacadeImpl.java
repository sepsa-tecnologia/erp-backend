/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.TipoNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoNotificacionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoNotificacionFacade", mappedName = "TipoNotificacionFacade")
@Local(TipoNotificacionFacade.class)
public class TipoNotificacionFacadeImpl extends FacadeImpl<TipoNotificacion, TipoNotificacionParam> implements TipoNotificacionFacade {

    public TipoNotificacionFacadeImpl() {
        super(TipoNotificacion.class);
    }
    
    @Override
    public TipoNotificacion find(Integer id, String codigo) {
        TipoNotificacionParam param = new TipoNotificacionParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        return findFirst(param);
    }
    
    @Override
    public List<TipoNotificacion> find(TipoNotificacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoNotificacion.class);
        Root<TipoNotificacion> root = cq.from(TipoNotificacion.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getEmail() != null) {
            predList.add(qb.equal(root.get("email"), param.getEmail()));
        }

        if (param.getTelefono() != null) {
            predList.add(qb.equal(root.get("telefono"), param.getTelefono()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<TipoNotificacion> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoNotificacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoNotificacion> root = cq.from(TipoNotificacion.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getEmail() != null) {
            predList.add(qb.equal(root.get("email"), param.getEmail()));
        }

        if (param.getTelefono() != null) {
            predList.add(qb.equal(root.get("telefono"), param.getTelefono()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
