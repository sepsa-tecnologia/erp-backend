/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class ProductoPrecioPojo {

    public ProductoPrecioPojo() {
    }

    public ProductoPrecioPojo(Integer id, Integer idMoneda,
            String descripcionMoneda, String codigoMoneda, BigDecimal precio,
            Character activo, Date fechaInsercion, Date fechaInicio,
            Date fechaFin, Integer idProducto, String descripcionProducto,
            String codigoGtin, Integer porcentajeImpuesto, Integer idCanalVenta,
            String descripcionCanalVenta, String codigoCanalVenta,
            Integer idCliente, String razonSocial) {
        this.id = id;
        this.idMoneda = idMoneda;
        this.descripcionMoneda = descripcionMoneda;
        this.codigoMoneda = codigoMoneda;
        this.precio = precio;
        this.activo = activo;
        this.fechaInsercion = fechaInsercion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.idProducto = idProducto;
        this.descripcionProducto = descripcionProducto;
        this.codigoGtin = codigoGtin;
        this.porcentajeImpuesto = porcentajeImpuesto;
        this.idCanalVenta = idCanalVenta;
        this.descripcionCanalVenta = descripcionCanalVenta;
        this.codigoCanalVenta = codigoCanalVenta;
        this.idCliente = idCliente;
        this.razonSocial = razonSocial;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Descripción moneda
     */
    private String descripcionMoneda;
    
    /**
     * Código de moneda
     */
    private String codigoMoneda;
    
    /**
     * Precio
     */
    private BigDecimal precio;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;
    
    /**
     * Fecha de inicio
     */
    private Date fechaInicio;
    
    /**
     * Fecha de fin
     */
    private Date fechaFin;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Descripción del producto
     */
    private String descripcionProducto;
    
    /**
     * Código GTIN
     */
    private String codigoGtin;
    
    /**
     * Porcentaje de impuesto
     */
    private Integer porcentajeImpuesto;
    
    /**
     * Identificador de canal de venta
     */
    private Integer idCanalVenta;
    
    /**
     * Descripción del canal de venta
     */
    private String descripcionCanalVenta;
    
    /**
     * Código del canal de venta
     */
    private String codigoCanalVenta;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Razón social
     */
    private String razonSocial;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public Integer getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(Integer porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public String getDescripcionCanalVenta() {
        return descripcionCanalVenta;
    }

    public void setDescripcionCanalVenta(String descripcionCanalVenta) {
        this.descripcionCanalVenta = descripcionCanalVenta;
    }

    public String getCodigoCanalVenta() {
        return codigoCanalVenta;
    }

    public void setCodigoCanalVenta(String codigoCanalVenta) {
        this.codigoCanalVenta = codigoCanalVenta;
    }

    public void setDescripcionMoneda(String descripcionMoneda) {
        this.descripcionMoneda = descripcionMoneda;
    }

    public String getDescripcionMoneda() {
        return descripcionMoneda;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
}
