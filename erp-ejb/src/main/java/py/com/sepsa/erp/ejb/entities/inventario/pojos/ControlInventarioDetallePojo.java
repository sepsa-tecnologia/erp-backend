/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan
 */
public class ControlInventarioDetallePojo {

    public ControlInventarioDetallePojo(Integer id, Integer idControlInventario,
            Integer idDepositoLogistico, String codigoDeposito, Integer idLocal,
            String local, Integer idProducto, String producto, String codigoGtin,
            String codigoInterno, Integer cantidadInicial, Integer cantidadFinal,
            String observacion, Date fechaVencimiento, Date fechaInicio, Date fechaFin) {
        this.id = id;
        this.idControlInventario = idControlInventario;
        this.idDepositoLogistico = idDepositoLogistico;
        this.codigoDeposito = codigoDeposito;
        this.idLocal = idLocal;
        this.local = local;
        this.idProducto = idProducto;
        this.producto = producto;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.cantidadInicial = cantidadInicial;
        this.cantidadFinal = cantidadFinal;
        this.observacion = observacion;
        this.fechaVencimiento = fechaVencimiento;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de control de inventario
     */
    private Integer idControlInventario;

    /**
     * Identificador de depósito logístico
     */
    private Integer idDepositoLogistico;

    /**
     * Código de depósito
     */
    private String codigoDeposito;

    /**
     * Identificador de local
     */
    private Integer idLocal;

    /**
     * Local
     */
    private String local;

    /**
     * Identificador de producto
     */
    private Integer idProducto;

    /**
     * Producto
     */
    private String producto;

    /**
     * Código GTIN
     */
    private String codigoGtin;

    /**
     * Código Interno
     */
    private String codigoInterno;

    /**
     * Cantidad inicial
     */
    private Integer cantidadInicial;

    /**
     * Cantidad final
     */
    private Integer cantidadFinal;

    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;

    /**
     * Fecha de inicio
     */
    private Date fechaInicio;

    /**
     * Fecha fin
     */
    private Date fechaFin;

    /**
     * Observación
     */
    private String observacion;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public String getCodigoDeposito() {
        return codigoDeposito;
    }

    public void setCodigoDeposito(String codigoDeposito) {
        this.codigoDeposito = codigoDeposito;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }
}
