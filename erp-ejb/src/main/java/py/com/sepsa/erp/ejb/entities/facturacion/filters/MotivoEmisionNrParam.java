/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
public class MotivoEmisionNrParam extends CommonParam {
    
    public MotivoEmisionNrParam() {
    }

    public MotivoEmisionNrParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    /**
     * Identificador de motivo de emisión
     */
    @QueryParam("idMotivoEmisionNr")
    private Integer idMotivoEmisionNr;

    /**
     * Código de motivo de emisión
     */
    @QueryParam("codigoMotivoEmisionNr")
    private String codigoMotivoEmision;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idMotivoEmisionNr) && isNullOrEmpty(codigoMotivoEmision)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo de emisión"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idMotivoEmisionNr) && isNullOrEmpty(codigoMotivoEmision)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo de emisión"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        
        return !tieneErrores();
    }

    public Integer getIdMotivoEmisionNr() {
        return idMotivoEmisionNr;
    }

    public void setIdMotivoEmisionNr(Integer idMotivoEmisionNr) {
        this.idMotivoEmisionNr = idMotivoEmisionNr;
    }

    public String getCodigoMotivoEmision() {
        return codigoMotivoEmision;
    }

    public void setCodigoMotivoEmision(String codigoMotivoEmision) {
        this.codigoMotivoEmision = codigoMotivoEmision;
    }
}
