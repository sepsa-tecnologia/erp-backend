/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.DepositoLogistico;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventario;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.TipoOperacion;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioPojo;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.OperacionInventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "OperacionInventarioDetalleFacade", mappedName = "OperacionInventarioDetalleFacade")
@javax.ejb.Local(OperacionInventarioDetalleFacade.class)
public class OperacionInventarioDetalleFacadeImp extends FacadeImpl<OperacionInventarioDetalle, OperacionInventarioDetalleParam> implements OperacionInventarioDetalleFacade {

    public OperacionInventarioDetalleFacadeImp() {
        super(OperacionInventarioDetalle.class);
    }
    
    @Override
    public Boolean validToCreate(OperacionInventarioDetalleParam param, boolean validarOperacionInventario) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        OperacionInventarioParam oiparam = new OperacionInventarioParam();
        oiparam.setId(param.getIdOperacionInventario());
        Long oisize = validarOperacionInventario
                ? facades.getOperacionInventarioFacade().findSize(oiparam)
                : 0;
        if(validarOperacionInventario && oisize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la operación de inventario"));
        }
        
        ProductoParam pparam = new ProductoParam();
        pparam.setId(param.getIdProducto());
        Long psize = facades.getProductoFacade().findSize(pparam);
        if(psize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el producto"));
        }
        
        if(param.getIdEstadoInventarioOrigen() != null || param.getCodigoEstadoInventarioOrigen() != null) {
            EstadoPojo estadoInventarioOrigen = facades.getEstadoFacade().find(param.getIdEstadoInventarioOrigen(), param.getCodigoEstadoInventarioOrigen(), "INVENTARIO");
            if(estadoInventarioOrigen == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado de inventario origen"));
            } else {
                param.setIdEstadoInventarioOrigen(estadoInventarioOrigen.getId());
                param.setCodigoEstadoInventarioOrigen(estadoInventarioOrigen.getCodigo());
            }
        }
        
        if(param.getIdEstadoInventarioDestino() != null || param.getCodigoEstadoInventarioDestino() != null) {
            EstadoPojo estadoInventarioDestino = facades.getEstadoFacade().find(param.getIdEstadoInventarioDestino(), param.getCodigoEstadoInventarioDestino(), "INVENTARIO");
            if(estadoInventarioDestino == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado de inventario destino"));
            } else {
                param.setIdEstadoInventarioDestino(estadoInventarioDestino.getId());
                param.setCodigoEstadoInventarioDestino(estadoInventarioDestino.getCodigo());
            }
        }
        
        if(param.getIdDepositoOrigen() != null) {
            DepositoLogisticoParam dlparam = new DepositoLogisticoParam();
            dlparam.setId(param.getIdDepositoOrigen());
            Long dlosize = facades.getDepositoLogisticoFacade().findSize(dlparam);
            if(dlosize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el depósito origen"));
            }
        }
        
        if(param.getIdDepositoDestino() != null) {
            DepositoLogisticoParam dlparam = new DepositoLogisticoParam();
            dlparam.setId(param.getIdDepositoDestino());
            Long dldsize = facades.getDepositoLogisticoFacade().findSize(dlparam);
            if(dldsize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el depósito destino"));
            }
        }
        
        if(!param.tieneErrores() && param.getCodigoTipoOperacion() != null) {
            
            if(Arrays.asList("ENTRADA", "MOVIMIENTO_INTERNO", "CAMBIO_ESTADO",
                    "AJUSTE_INVENTARIO", "AJUSTE_CONTROL_INVENTARIO")
                    .contains(param.getCodigoTipoOperacion())) {
                if(param.getIdDepositoDestino() == null) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el deposito destino"));
                }
                if(param.getIdEstadoInventarioDestino() == null) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el estado de inventario destino"));
                }
            }
            
            if(Arrays.asList("SALIDA", "MOVIMIENTO_INTERNO", "CAMBIO_ESTADO")
                    .contains(param.getCodigoTipoOperacion())) {
                if(param.getIdDepositoOrigen() == null) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el deposito origen"));
                }
                if(param.getIdEstadoInventarioOrigen() == null) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el estado de inventario origen"));
                }
            }
            
            if(!param.tieneErrores() && param.getCodigoTipoOperacion().equals("CAMBIO_ESTADO")) {
                if(!param.getIdDepositoOrigen().equals(param.getIdDepositoDestino())) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("El depósito origen y destino debe ser igual para un cambio de estado"));
                }
                if(param.getIdEstadoInventarioOrigen().equals(param.getIdEstadoInventarioDestino())) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("El estado origen y destino no debe ser igual para un cambio de estado"));
                }
                if(Arrays.asList("BLOQUEADO").contains(param.getCodigoEstadoInventarioOrigen())) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se puede realizar el cambio de estado de productos bloqueados"));
                }
            }
            
            if(!param.tieneErrores() && param.getCodigoTipoOperacion().equals("AJUSTE_INVENTARIO")
                    && Arrays.asList("BLOQUEADO", "CUARENTENA").contains(param.getCodigoEstadoInventarioDestino())) {

                if(param.getCodigoEstadoInventarioDestino().equals("BLOQUEADO")) {
                    InventarioParam iparam = new InventarioParam();
                    iparam.setIdProducto(param.getIdProducto());
                    iparam.setIdDepositoLogistico(param.getIdDepositoDestino());
                    iparam.setIdEstado(param.getIdEstadoInventarioDestino());
                    iparam.setIdEmpresa(param.getIdEmpresa());
                    InventarioPojo inventario = facades.getInventarioFacade().findFirstPojo(iparam);
                    
                    if(inventario != null && inventario.getCantidad() < param.getCantidad()) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("Para aumentar la cantidad bloqueada debe utilizar el cambio de estado."));
                    }
                }

                if(param.getCodigoEstadoInventarioDestino().equals("CUARENTENA")) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se puede ajustar el inventario de productos en cuarentena."));
                }
            }
            
            if(!param.tieneErrores() && Arrays.asList("ENTRADA", "AJUSTE_INVENTARIO",
                    "AJUSTE_CONTROL_INVENTARIO", "MOVIMIENTO_INTERNO", "CAMBIO_ESTADO")
                    .contains(param.getCodigoTipoOperacion())) {
                if(Arrays.asList("COMPROMETIDO", "FACTURADO").contains(param.getCodigoEstadoInventarioOrigen())
                        || Arrays.asList("COMPROMETIDO", "FACTURADO").contains(param.getCodigoEstadoInventarioDestino())) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se puede ajustar el inventario de productos comprometidos o facturados"));
                }
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public OperacionInventarioDetalle create(OperacionInventarioDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        OperacionInventarioDetalle result = new OperacionInventarioDetalle();
        result.setIdOperacionInventario(param.getIdOperacionInventario());
        result.setIdProducto(param.getIdProducto());
        result.setIdEstadoInventarioOrigen(param.getIdEstadoInventarioOrigen());
        result.setIdEstadoInventarioDestino(param.getIdEstadoInventarioDestino());
        result.setIdDepositoOrigen(param.getIdDepositoOrigen());
        result.setIdDepositoDestino(param.getIdDepositoDestino());
        result.setCantidad(param.getCantidad());
        result.setFechaVencimiento(param.getFechaVencimiento());
        
        create(result);
        
        if(userInfo.getTieneModuloInventario()) {
            facades.getInventarioUtils().operacionInventarioDetalle(userInfo, param);
        }
        
        return result;
    }

    @Override
    public OperacionInventarioDetalle create(UserInfoImpl userInfo, Integer idOperacionInventario, String codigoTipoOperacion, Integer idProducto, Integer cantidad, Date fechaVencimiento, Integer idDepositoOrigen, Integer idEstadoInventarioOrigen, String codigoEstadoInventarioOrigen, Integer idDepositoDestino, Integer idEstadoInventarioDestino, String codigoEstadoInventarioDestino) {
        OperacionInventarioDetalleParam param = new OperacionInventarioDetalleParam();
        param.setIdOperacionInventario(idOperacionInventario);
        param.setIdProducto(idProducto);
        param.setCantidad(cantidad);
        param.setFechaVencimiento(fechaVencimiento);
        param.setIdDepositoOrigen(idDepositoOrigen);
        param.setIdEstadoInventarioOrigen(idEstadoInventarioOrigen);
        param.setCodigoEstadoInventarioOrigen(codigoEstadoInventarioOrigen);
        param.setIdDepositoDestino(idDepositoDestino);
        param.setIdEstadoInventarioDestino(idEstadoInventarioDestino);
        param.setCodigoEstadoInventarioDestino(codigoEstadoInventarioDestino);
        param.setCodigoTipoOperacion(codigoTipoOperacion);
        param.setCodigoEstadoOperacion("CONFIRMADO");
        return create(param, userInfo);
    }
    
    @Override
    public List<OperacionInventarioDetallePojo> findPojo(OperacionInventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(OperacionInventarioDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idOperacionInventario"),
                        getPath("root").get("idProducto"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("codigoInterno"),
                        getPath("producto").get("descripcion"),
                        getPath("tipoOperacion").get("id"),
                        getPath("tipoOperacion").get("codigo"),
                        getPath("tipoOperacion").get("descripcion"),
                        getPath("root").get("idEstadoInventarioOrigen"),
                        getPath("estadoInventarioOrigen").get("descripcion"),
                        getPath("estadoInventarioOrigen").get("codigo"),
                        getPath("root").get("idEstadoInventarioDestino"),
                        getPath("estadoInventarioDestino").get("descripcion"),
                        getPath("estadoInventarioDestino").get("codigo"),
                        getPath("root").get("idDepositoOrigen"),
                        getPath("depositoOrigen").get("descripcion"),
                        getPath("depositoOrigen").get("idLocal"),
                        getPath("localOrigen").get("descripcion"),
                        getPath("root").get("idDepositoDestino"),
                        getPath("depositoDestino").get("descripcion"),
                        getPath("depositoDestino").get("idLocal"),
                        getPath("localDestino").get("descripcion"),
                        getPath("root").get("cantidad"),
                        getPath("root").get("fechaVencimiento"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<OperacionInventarioDetalle> findByIdOperacionInventario(Integer idOperacionInventario) {
        OperacionInventarioDetalleParam param = new OperacionInventarioDetalleParam();
        param.setIdOperacionInventario(idOperacionInventario);
        param.setPageSize(1000);
        param.setFirstResult(0);
        return find(param);
    }
    
    @Override
    public List<OperacionInventarioDetallePojo> findByIdOperacionInventarioPojo(Integer idOperacionInventario) {
        OperacionInventarioDetalleParam param = new OperacionInventarioDetalleParam();
        param.setIdOperacionInventario(idOperacionInventario);
        param.setPageSize(1000);
        param.setFirstResult(0);
        return findPojo(param);
    }
    
    @Override
    public List<OperacionInventarioDetalle> find(OperacionInventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(OperacionInventarioDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, OperacionInventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<OperacionInventarioDetalle> root = cq.from(OperacionInventarioDetalle.class);
        Join<OperacionInventarioDetalle, Producto> producto = root.join("producto");
        Join<OperacionInventarioDetalle, OperacionInventario> operacionInventario = root.join("operacionInventario");
        Join<OperacionInventario, TipoOperacion> tipoOperacion = operacionInventario.join("tipoOperacion");
        Join<OperacionInventarioDetalle, Estado> estadoInventarioOrigen = root.join("estadoInventarioOrigen", JoinType.LEFT);
        Join<OperacionInventarioDetalle, Estado> estadoInventarioDestino = root.join("estadoInventarioDestino", JoinType.LEFT);
        Join<OperacionInventario, DepositoLogistico> depositoOrigen = root.join("depositoOrigen", JoinType.LEFT);
        Join<DepositoLogistico, Local> localOrigen = depositoOrigen.join("local", JoinType.LEFT);
        Join<OperacionInventario, DepositoLogistico> depositoDestino = root.join("depositoDestino", JoinType.LEFT);
        Join<DepositoLogistico, Local> localDestino = depositoDestino.join("local", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("producto", producto);
        find.addPath("tipoOperacion", tipoOperacion);
        find.addPath("estadoInventarioOrigen", estadoInventarioOrigen);
        find.addPath("estadoInventarioDestino", estadoInventarioDestino);
        find.addPath("depositoOrigen", depositoOrigen);
        find.addPath("depositoOrigen", depositoOrigen);
        find.addPath("localOrigen", localOrigen);
        find.addPath("depositoDestino", depositoDestino);
        find.addPath("localDestino", localDestino);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdOperacionInventario() != null) {
            predList.add(qb.equal(root.get("idOperacionInventario"), param.getIdOperacionInventario()));
        }
        
        if (param.getIdProducto()!= null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdDepositoOrigen() != null) {
            predList.add(qb.equal(root.get("idDepositoOrigen"), param.getIdDepositoOrigen()));
        }
        
        if (param.getIdDepositoDestino() != null) {
            predList.add(qb.equal(root.get("idDepositoDestino"), param.getIdDepositoDestino()));
        }
        
        if (param.getIdEstadoInventarioOrigen() != null) {
            predList.add(qb.equal(root.get("idEstadoInventarioOrigen"), param.getIdEstadoInventarioOrigen()));
        }
        
        if (param.getIdEstadoInventarioDestino() != null) {
            predList.add(qb.equal(root.get("idEstadoInventarioDestino"), param.getIdEstadoInventarioDestino()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);
        
        cq.orderBy(qb.asc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(OperacionInventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<OperacionInventarioDetalle> root = cq.from(OperacionInventarioDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdOperacionInventario() != null) {
            predList.add(qb.equal(root.get("idOperacionInventario"), param.getIdOperacionInventario()));
        }
        
        if (param.getIdProducto()!= null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdDepositoOrigen() != null) {
            predList.add(qb.equal(root.get("idDepositoOrigen"), param.getIdDepositoOrigen()));
        }
        
        if (param.getIdDepositoDestino() != null) {
            predList.add(qb.equal(root.get("idDepositoDestino"), param.getIdDepositoDestino()));
        }
        
        if (param.getIdEstadoInventarioOrigen() != null) {
            predList.add(qb.equal(root.get("idEstadoInventarioOrigen"), param.getIdEstadoInventarioOrigen()));
        }
        
        if (param.getIdEstadoInventarioDestino() != null) {
            predList.add(qb.equal(root.get("idEstadoInventarioDestino"), param.getIdEstadoInventarioDestino()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
