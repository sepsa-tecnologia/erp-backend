/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.Calendar;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Aprobacion;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "AprobacionFacade", mappedName = "AprobacionFacade")
@Local(AprobacionFacade.class)
public class AprobacionFacadeImpl extends FacadeImpl<Aprobacion, CommonParam> implements AprobacionFacade {

    public AprobacionFacadeImpl() {
        super(Aprobacion.class);
    }
    
    @Override
    public Aprobacion create() {
        
        EstadoPojo estado = facades.getEstadoFacade().find(null, "PENDIENTE", "APROBACION");
        
        Aprobacion item = new Aprobacion();
        item.setFechaInsercion(Calendar.getInstance().getTime());
        item.setIdEstado(estado.getId());
        
        create(item);
        
        return item;
    }
}
