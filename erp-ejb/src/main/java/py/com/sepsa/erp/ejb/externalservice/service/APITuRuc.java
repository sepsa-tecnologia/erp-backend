package py.com.sepsa.erp.ejb.externalservice.service;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;

/**
 * Cliente API Service para los servicios comunes
 *
 * @author Daniel F. Escauriza Arza
 */
@Log4j2
public class APITuRuc extends API {

    //Parámetros de conexión
    private final static String BASE_API = "api";
    private final static int CONN_TIMEOUT = 3 * 60 * 1000;
    private final static int READ_TIMEOUT = 20 * 1000;
    private final static Scheme PROTOCOL = Scheme.HTTP;

    //Datos para enviar formulario HTTP
    private static final String BOUNDARY = "--------------------------" + System.currentTimeMillis();
    private static final String LINE_FEED = "\r\n";

    public static String token;
    public static String apiToken;

    /**
     * Consulta el RUC de un contribuyente
     *
     * @param ruc RUC
     * @return
     */
    public HttpStringResponse consultarRuc(String ruc) {
        HttpStringResponse response = new HttpStringResponse();

        Map<String, Object> map = new HashMap<>();
        map.put("ruc", ruc);

        HttpURLConnection httpConn = GET(Resource.CONTRIBUYENTE.url, ContentType.JSON, map);
        try {
            if (httpConn != null) {
                response = HttpStringResponse.createInstance(httpConn);
                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }
        return response;
    }

    public APITuRuc(String host) {
        super(Service.EDI_SERVER, PROTOCOL, BASE_API, token, CONN_TIMEOUT,
                READ_TIMEOUT, BOUNDARY, LINE_FEED, null, apiToken);

        List<Host> hosts = Host.createInstaces(host);

        this.host = hosts == null || hosts.isEmpty() ? null : hosts.get(0);
    }

    /**
     * Recursos de conexión o servicios del API
     */
    public enum Resource {

        //Servicios
        CONTRIBUYENTE("Obtiene el RUC de un contribuyente", "contribuyente");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }
    }
}
