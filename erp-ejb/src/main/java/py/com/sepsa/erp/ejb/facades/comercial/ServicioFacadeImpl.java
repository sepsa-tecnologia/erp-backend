/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ServicioParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "ServicioFacade", mappedName = "ServicioFacade")
@Local(ServicioFacade.class)
public class ServicioFacadeImpl extends FacadeImpl<Servicio, ServicioParam> implements ServicioFacade {

    public ServicioFacadeImpl() {
        super(Servicio.class);
    }

    @Override
    public Map<String, String> getMapConditions(Servicio item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("descripcion", item.getDescripcion() + "");
        map.put("codigo", item.getCodigo()+ "");
        map.put("id_producto", item.getIdProducto() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(Servicio item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToCreate(ServicioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ProductoCom producto = facades
                .getProductoComFacade()
                .find(param.getIdProducto());
        
        if(producto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto"));
        }
        
        ServicioParam param1 = new ServicioParam();
        param1.setCodigo(param.getCodigo());
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un servicio con el código indicado"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de servicio
     * @param param Parámetros
     * @return Servicio
     */
    public Servicio create(ServicioParam param) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Servicio servicio = new Servicio();
        servicio.setCodigo(param.getCodigo());
        servicio.setDescripcion(param.getDescripcion());
        servicio.setIdProducto(param.getIdProducto());
        
        create(servicio);
        
        return servicio;
    }
    
    @Override
    public Servicio find(Integer id, String codigo) {
        ServicioParam param = new ServicioParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<Servicio> find(ServicioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Servicio.class);
        Root<Servicio> root = cq.from(Servicio.class);
        Join<Servicio, ProductoCom> p = root.join("producto");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getIdProducto()!= null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getCodigoProducto() != null && !param.getCodigoProducto().trim().isEmpty()) {
            predList.add(qb.equal(p.get("codigo"), param.getCodigoProducto().trim()));
        }

        if (param.getProducto()!= null && !param.getProducto().trim().isEmpty()) {
            predList.add(qb.like(p.<String>get("descripcion"),
                    String.format("%%%s%%", param.getProducto().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<Servicio> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(ServicioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Servicio> root = cq.from(Servicio.class);
        Join<Servicio, ProductoCom> p = root.join("producto");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getIdProducto()!= null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getCodigoProducto() != null && !param.getCodigoProducto().trim().isEmpty()) {
            predList.add(qb.equal(p.get("codigo"), param.getCodigoProducto().trim()));
        }

        if (param.getProducto()!= null && !param.getProducto().trim().isEmpty()) {
            predList.add(qb.like(p.<String>get("descripcion"),
                    String.format("%%%s%%", param.getProducto().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : (Long) object;

        return result;
    }
}
