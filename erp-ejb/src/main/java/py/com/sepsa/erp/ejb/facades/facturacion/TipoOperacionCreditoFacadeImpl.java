/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoOperacionCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoOperacionCreditoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoOperacionCreditoFacade", mappedName = "TipoOperacionCreditoFacade")
@Local(TipoOperacionCreditoFacade.class)
public class TipoOperacionCreditoFacadeImpl extends FacadeImpl<TipoOperacionCredito, TipoOperacionCreditoParam> implements TipoOperacionCreditoFacade {

    public TipoOperacionCreditoFacadeImpl() {
        super(TipoOperacionCredito.class);
    }

    public Boolean validToCreate(TipoOperacionCreditoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoOperacionCreditoParam param1 = new TipoOperacionCreditoParam();
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(TipoOperacionCreditoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoOperacionCredito tipoOperacionCredito = find(param.getId());
        
        if(tipoOperacionCredito == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de operación a crédito"));
        } else if(!tipoOperacionCredito.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoOperacionCreditoParam param1 = new TipoOperacionCreditoParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public TipoOperacionCredito create(TipoOperacionCreditoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoOperacionCredito tipoCobro = new TipoOperacionCredito();
        tipoCobro.setCodigo(param.getCodigo().trim());
        tipoCobro.setDescripcion(param.getDescripcion().trim());
        create(tipoCobro);
        
        return tipoCobro;
    }
    
    @Override
    public TipoOperacionCredito edit(TipoOperacionCreditoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoOperacionCredito tipoOperacionCredito = find(param.getId());
        tipoOperacionCredito.setCodigo(param.getCodigo().trim());
        tipoOperacionCredito.setDescripcion(param.getDescripcion().trim());
        edit(tipoOperacionCredito);
        
        return tipoOperacionCredito;
    }
    
    @Override
    public TipoOperacionCredito find(Integer id, String codigo) {
        TipoOperacionCreditoParam param = new TipoOperacionCreditoParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoOperacionCredito> find(TipoOperacionCreditoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoOperacionCredito.class);
        Root<TipoOperacionCredito> root = cq.from(TipoOperacionCredito.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<TipoOperacionCredito> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoOperacionCreditoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoOperacionCredito> root = cq.from(TipoOperacionCredito.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
