/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaNotificacionPojo {

    public FacturaNotificacionPojo(Integer id, Integer idFactura,
            Integer idTipoNotificacion, String tipoNotificacion,
            String codigoTipoNotificacion, String email) {
        this.id = id;
        this.idFactura = idFactura;
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.codigoTipoNotificacion = codigoTipoNotificacion;
        this.email = email;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Identificador de tipo de notificación
     */
    private Integer idTipoNotificacion;
    
    /**
     * Tipo de notificación
     */
    private String tipoNotificacion;
    
    /**
     * Código de tipo de notificación
     */
    private String codigoTipoNotificacion;
    
    /**
     * Email
     */
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
