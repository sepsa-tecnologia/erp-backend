/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class NotaRemisionDetalleParam extends CommonParam{
    
    public NotaRemisionDetalleParam(){
        
    }
    
    public NotaRemisionDetalleParam(String bruto){
        super(bruto);
    }
    
    @QueryParam("idNotaRemision")
    private Integer idNotaRemision;
    
    @QueryParam("nroLinea")
    private Integer nroLinea;
    
    @QueryParam("idProducto")
    private Integer idProducto;
    
    @QueryParam("cantidad")
    private BigDecimal cantidad;
    
    @QueryParam("listadoPojo")
    private Boolean litadoPojo;
    
    @QueryParam("datoAdicional")
    private String datoAdicional;

    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }
    
       @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idNotaRemision)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de remision"));
        }
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador del producto"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador nro de linea"));
        }
        
       
        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        return !tieneErrores();
    }
    
    public Integer getIdNotaRemision() {
        return idNotaRemision;
    }

    public void setIdNotaRemision(Integer idNotaRemision) {
        this.idNotaRemision = idNotaRemision;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public Boolean getLitadoPojo() {
        return litadoPojo;
    }

    public void setLitadoPojo(Boolean litadoPojo) {
        this.litadoPojo = litadoPojo;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }
    
}
