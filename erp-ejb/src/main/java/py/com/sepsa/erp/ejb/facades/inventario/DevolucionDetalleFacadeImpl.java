/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Motivo;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.DepositoLogistico;
import py.com.sepsa.erp.ejb.entities.inventario.DevolucionDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DevolucionDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DevolucionParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.DevolucionDetallePojo;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.DevolucionPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "DevolucionDetalleFacade", mappedName = "DevolucionDetalleFacade")
@Local(DevolucionDetalleFacade.class)
public class DevolucionDetalleFacadeImpl extends FacadeImpl<DevolucionDetalle, DevolucionDetalleParam> implements DevolucionDetalleFacade {

    public DevolucionDetalleFacadeImpl() {
        super(DevolucionDetalle.class);
    }
    
    @Override
    public Boolean validToCreate(DevolucionDetalleParam param, boolean validarDevolucion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        DevolucionParam dparam = new DevolucionParam();
        dparam.setId(param.getIdDevolucion());
        Long dsize = validarDevolucion
                ? facades.getDevolucionFacade().findSize(dparam)
                : 0;
        if(validarDevolucion && dsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de devolución"));
        }
        
        MotivoPojo motivo = facades.getMotivoFacade().find(param.getIdMotivo(), param.getCodigoMotivo(), "DEVOLUCION");

        if(motivo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el motivo"));
        } else {
            param.setIdMotivo(motivo.getId());
            param.setCodigoMotivo(motivo.getCodigo());
        }
        
        ProductoParam pparam = new ProductoParam();
        pparam.setId(param.getIdProducto());
        Long psize = facades.getProductoFacade().findSize(pparam);
        
        if(psize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto"));
        }
        
        DepositoLogisticoParam dlparam = new DepositoLogisticoParam();
        dlparam.setId(param.getIdDepositoLogistico());
        Long dlsize = facades.getDepositoLogisticoFacade().findSize(dlparam);
        
        if(dlsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el depósito logistico"));
        }
        
        EstadoPojo estadoInventario = facades.getEstadoFacade().find(param.getIdEstadoInventario(), param.getCodigoEstadoInventario(), "INVENTARIO");
        
        if(estadoInventario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado de inventario"));
        } else {
            param.setIdEstadoInventario(estadoInventario.getId());
            param.setCodigoEstadoInventario(estadoInventario.getCodigo());
        }
        
        if(param.getIdFacturaDetalle() != null) {
            FacturaDetalleParam fdparam = new FacturaDetalleParam();
            fdparam.setId(param.getIdFacturaDetalle());
            Long fdsize = facades.getFacturaDetalleFacade().findSize(fdparam);

            if(fdsize <= 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la factura detalle"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public DevolucionDetalle create(DevolucionDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        DevolucionDetalle result = new DevolucionDetalle();
        result.setIdDevolucion(param.getIdDevolucion());
        result.setIdMotivo(param.getIdMotivo());
        result.setIdProducto(param.getIdProducto());
        result.setIdDepositoLogistico(param.getIdDepositoLogistico());
        result.setIdEstadoInventario(param.getIdEstadoInventario());
        result.setIdFacturaDetalle(param.getIdFacturaDetalle());
        result.setCantidadFacturada(param.getCantidadFacturada());
        result.setCantidadDevolucion(param.getCantidadDevolucion());
        result.setFechaVencimiento(param.getFechaVencimiento());
        
        create(result);
        
        return result;
    }
    
    @Override
    public List<DevolucionPojo> findPojo(DevolucionDetalleParam param) {

        AbstractFind find = new AbstractFind(DevolucionDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idDevolucion"),
                        getPath("root").get("idMotivo"),
                        getPath("motivo").get("codigo"),
                        getPath("motivo").get("descripcion"),
                        getPath("root").get("idFacturaDetalle"),
                        getPath("root").get("idEstadoInventario"),
                        getPath("estadoInventario").get("codigo"),
                        getPath("estadoInventario").get("descripcion"),
                        getPath("root").get("idDepositoLogistico"),
                        getPath("depositoLogistico").get("descripcion"),
                        getPath("root").get("idProducto"),
                        getPath("producto").get("descripcion"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("codigoInterno"),
                        getPath("root").get("cantidadFacturada"),
                        getPath("root").get("cantidadDevolucion"),
                        getPath("root").get("fechaVencimiento"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<DevolucionDetalle> find(DevolucionDetalleParam param) {

        AbstractFind find = new AbstractFind(DevolucionDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, DevolucionDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<DevolucionDetalle> root = cq.from(DevolucionDetalle.class);
        Join<DevolucionDetalle, Motivo> motivo = root.join("motivo");
        Join<DevolucionDetalle, Producto> producto = root.join("producto");
        Join<DevolucionDetalle, Estado> estadoInventario = root.join("estadoInventario");
        Join<DevolucionDetalle, DepositoLogistico> depositoLogistico = root.join("depositoLogistico");
        
        find.addPath("root", root);
        find.addPath("motivo", motivo);
        find.addPath("producto", producto);
        find.addPath("estadoInventario", estadoInventario);
        find.addPath("depositoLogistico", depositoLogistico);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdDevolucion() != null) {
            predList.add(qb.equal(root.get("idDevolucion"), param.getIdDevolucion()));
        }
        
        if (param.getIdMotivo() != null) {
            predList.add(qb.equal(root.get("idMotivo"), param.getIdMotivo()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdEstadoInventario() != null) {
            predList.add(qb.equal(root.get("idEstadoInventario"), param.getIdEstadoInventario()));
        }
        
        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(root.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }
        
        if (param.getIdFacturaDetalle() != null) {
            predList.add(qb.equal(root.get("idFacturaDetalle"), param.getIdFacturaDetalle()));
        }
        
        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }
        
        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(DevolucionDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<DevolucionDetalle> root = cq.from(DevolucionDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdDevolucion() != null) {
            predList.add(qb.equal(root.get("idDevolucion"), param.getIdDevolucion()));
        }
        
        if (param.getIdMotivo() != null) {
            predList.add(qb.equal(root.get("idMotivo"), param.getIdMotivo()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdEstadoInventario() != null) {
            predList.add(qb.equal(root.get("idEstadoInventario"), param.getIdEstadoInventario()));
        }
        
        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(root.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }
        
        if (param.getIdFacturaDetalle() != null) {
            predList.add(qb.equal(root.get("idFacturaDetalle"), param.getIdFacturaDetalle()));
        }
        
        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }
        
        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
