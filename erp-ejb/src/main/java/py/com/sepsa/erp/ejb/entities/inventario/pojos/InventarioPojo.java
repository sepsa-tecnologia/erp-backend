/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

/**
 *
 * @author Jonathan
 */
public class InventarioPojo {

    public InventarioPojo() {
    }

    public InventarioPojo(Integer id, Integer idEmpresa, String empresa,
            Integer idDepositoLogistico, String codigoDepositoLogistico,
            String depositoLogistico, Integer idLocal, String local,
            Integer idProducto, String producto, String codigoGtin,
            String codigoInterno, Integer idEstado, String estado,
            String codigoEstado, Integer cantidad) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idDepositoLogistico = idDepositoLogistico;
        this.codigoDepositoLogistico = codigoDepositoLogistico;
        this.depositoLogistico = depositoLogistico;
        this.idLocal = idLocal;
        this.local = local;
        this.idProducto = idProducto;
        this.producto = producto;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.cantidad = cantidad;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
    
    /**
     * Código de deposito
     */
    private String codigoDepositoLogistico;
    
    /**
     * Descripción de deposito logistico
     */
    private String depositoLogistico;
    
    /**
     * Identificador de local
     */
    private Integer idLocal;
    
    /**
     * Local
     */
    private String local;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Descripción del producto
     */
    private String producto;
    
    /**
     * Código GTIN
     */
    private String codigoGtin;
    
    /**
     * Código interno
     */
    private String codigoInterno;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Descripción del estado
     */
    private String estado;
    
    /**
     * Código del producto
     */
    private String codigoEstado;
    
    /**
     * Cantidad
     */
    private Integer cantidad;

    public String getKey() {
        return String.format("%d-%d-%d", idDepositoLogistico, idProducto, idEstado);
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public String getCodigoDepositoLogistico() {
        return codigoDepositoLogistico;
    }

    public void setCodigoDepositoLogistico(String codigoDepositoLogistico) {
        this.codigoDepositoLogistico = codigoDepositoLogistico;
    }

    public String getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(String depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getLocal() {
        return local;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
