/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;

/**
 *
 * @author Jonathan
 */
public class FacturaDescuentoPojo {

    public FacturaDescuentoPojo(Integer id, Integer idFactura,
            Integer idDescuento, String descripcion, String codigo,
            String tipoDescuento, String codigoTipoDescuento,
            Character activo, BigDecimal valorDescuento) {
        this.id = id;
        this.idFactura = idFactura;
        this.idDescuento = idDescuento;
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.tipoDescuento = tipoDescuento;
        this.codigoTipoDescuento = codigoTipoDescuento;
        this.activo = activo;
        this.valorDescuento = valorDescuento;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Identificador de descuento
     */
    private Integer idDescuento;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código
     */
    private String codigo;
    
    /**
     * Tipo descuento
     */
    private String tipoDescuento;
    
    /**
     * Código tipo descuento
     */
    private String codigoTipoDescuento;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Valor descuento
     */
    private BigDecimal valorDescuento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(Integer idDescuento) {
        this.idDescuento = idDescuento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(String tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public String getCodigoTipoDescuento() {
        return codigoTipoDescuento;
    }

    public void setCodigoTipoDescuento(String codigoTipoDescuento) {
        this.codigoTipoDescuento = codigoTipoDescuento;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public BigDecimal getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(BigDecimal valorDescuento) {
        this.valorDescuento = valorDescuento;
    }
}
