/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan Bernal
 */
public class TokenPojo {

    public TokenPojo(Integer id, Integer idUsuario, String usuario, Integer idEmpresa, String empresa, Character activo, Date fechaInsercion) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.activo = activo;
        this.fechaInsercion = fechaInsercion;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de usuario
     */
    private Integer idUsuario;
    
    /**
     * Usuario
     */
    private String usuario;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Fecha inserción
     */
    private Date fechaInsercion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
