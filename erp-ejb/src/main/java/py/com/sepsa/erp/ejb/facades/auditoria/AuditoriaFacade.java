/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.auditoria.Auditoria;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.AuditoriaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface AuditoriaFacade extends Facade<Auditoria, AuditoriaParam, UserInfoImpl> {

    public Auditoria nuevaInstancia(UserInfoImpl userInfo);
    
}
