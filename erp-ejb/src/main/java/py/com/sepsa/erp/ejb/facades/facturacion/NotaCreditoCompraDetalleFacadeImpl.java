/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoCompraDetalleParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NotaCreditoCompraDetalleFacade", mappedName = "NotaCreditoCompraDetalleFacade")
@Local(NotaCreditoCompraDetalleFacade.class)
public class NotaCreditoCompraDetalleFacadeImpl extends FacadeImpl<NotaCreditoCompraDetalle, NotaCreditoCompraDetalleParam> implements NotaCreditoCompraDetalleFacade {

    public NotaCreditoCompraDetalleFacadeImpl() {
        super(NotaCreditoCompraDetalle.class);
    }

    @Override
    public Boolean validToCreate(NotaCreditoCompraDetalleParam param, boolean validarIdNc) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        NotaCreditoCompra ncc = facades.getNotaCreditoCompraFacade().find(param.getIdNotaCreditoCompra());
        
        if(validarIdNc && ncc == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de crédito compra"));
        }
        
        FacturaCompra facturaCompra = facades.getFacturaCompraFacade().find(param.getIdFacturaCompra());
        
        if(facturaCompra == null) {
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura de compra"));
        } else {
            
            if(facturaCompra.getAnulado().equals('S')) {
                
                param.addError(MensajePojo.createInstance()
                    .descripcion("No se puede crear una NC asociada a una factura anulada"));
            }
            
            if(facturaCompra.getSaldo().compareTo(param.getMontoTotal()) < 0) {
                
                param.addError(MensajePojo.createInstance()
                    .descripcion("El monto total no puede ser mayor al saldo de la factura"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public NotaCreditoCompraDetalle create(NotaCreditoCompraDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        NotaCreditoCompraDetalle ncd = new NotaCreditoCompraDetalle();
        ncd.setIdNotaCreditoCompra(param.getIdNotaCreditoCompra());
        ncd.setNroLinea(param.getNroLinea());
        ncd.setIdFacturaCompra(param.getIdFacturaCompra());
        ncd.setDescripcion(param.getDescripcion());
        ncd.setPorcentajeIva(param.getPorcentajeIva());
        ncd.setCantidad(param.getCantidad());
        ncd.setPrecioUnitarioConIva(param.getPrecioUnitarioConIva().stripTrailingZeros());
        ncd.setPrecioUnitarioSinIva(param.getPrecioUnitarioSinIva().stripTrailingZeros());
        ncd.setMontoIva(param.getMontoIva().stripTrailingZeros());
        ncd.setMontoImponible(param.getMontoImponible().stripTrailingZeros());
        ncd.setMontoTotal(param.getMontoTotal().stripTrailingZeros());
        
        create(ncd);
        
        facades.getFacturaCompraFacade().actualizarSaldoFactura(
                param.getIdFacturaCompra(),
                param.getMontoTotal());
        
        return ncd;
    }
    
    @Override
    public List<NotaCreditoCompraDetalle> find(NotaCreditoCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(NotaCreditoCompraDetalle.class);
        Root<NotaCreditoCompraDetalle> root = cq.from(NotaCreditoCompraDetalle.class);
        Join<NotaCreditoCompraDetalle, NotaCreditoCompra> nc = root.join("notaCreditoCompra", JoinType.LEFT);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdNotaCreditoCompra() != null) {
            predList.add(qb.equal(root.get("idNotaCreditoCompra"), param.getIdNotaCreditoCompra()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(root.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        
        if(param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {
         
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            
            Date date = null;
            
            try {
                date = sdf.parse(param.getAnhoMes());
            } catch (Exception e) {}
            
            if(date != null) {
                Calendar from = Calendar.getInstance();
                from.setTime(date);
                from.set(Calendar.DAY_OF_MONTH, 1);
                from.set(Calendar.HOUR, 0);
                from.set(Calendar.MINUTE, 0);
                from.set(Calendar.SECOND, 0);
                
                Calendar to = Calendar.getInstance();
                to.setTime(date);
                to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
                to.set(Calendar.HOUR, 23);
                to.set(Calendar.MINUTE, 59);
                to.set(Calendar.SECOND, 59);
                
                predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), from.getTime()));
                predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), to.getTime()));
            }
        }

        if(param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaDesde()));
        }

        if(param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(nc.get("fecha")),
                qb.asc(root.get("nroLinea")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<NotaCreditoCompraDetalle> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaCreditoCompraDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaCreditoCompraDetalle> root = cq.from(NotaCreditoCompraDetalle.class);
        Join<NotaCreditoCompraDetalle, NotaCreditoCompra> nc = root.join("notaCreditoCompra", JoinType.LEFT);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdNotaCreditoCompra() != null) {
            predList.add(qb.equal(root.get("idNotaCreditoCompra"), param.getIdNotaCreditoCompra()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(root.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        
        if(param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {
         
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            
            Date date = null;
            
            try {
                date = sdf.parse(param.getAnhoMes());
            } catch (Exception e) {}
            
            if(date != null) {
                Calendar from = Calendar.getInstance();
                from.setTime(date);
                from.set(Calendar.DAY_OF_MONTH, 1);
                from.set(Calendar.HOUR, 0);
                from.set(Calendar.MINUTE, 0);
                from.set(Calendar.SECOND, 0);
                
                Calendar to = Calendar.getInstance();
                to.setTime(date);
                to.set(Calendar.DAY_OF_MONTH, to.getActualMaximum(Calendar.DAY_OF_MONTH));
                to.set(Calendar.HOUR, 23);
                to.set(Calendar.MINUTE, 59);
                to.set(Calendar.SECOND, 59);
                
                predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), from.getTime()));
                predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), to.getTime()));
            }
        }
        
        if(param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaDesde()));
        }

        if(param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(nc.<Date>get("fecha"), param.getFechaHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();

        return result;
    }
}
