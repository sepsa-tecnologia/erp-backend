/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
public class ControlInventarioParam extends CommonParam {

    public ControlInventarioParam() {
    }

    public ControlInventarioParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de usuario encargado
     */
    @QueryParam("idUsuarioEncargado")
    private Integer idUsuarioEncargado;

    /**
     * Identificador de usuario supervisor
     */
    @QueryParam("idUsuarioSupervisor")
    private Integer idUsuarioSupervisor;

    /**
     * Identificador de deposito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;

    /**
     * Identificador de deposito logisticos
     */
    @QueryParam("idDepositoLogisticos")
    private List<Integer> idDepositoLogisticos;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Identificador de productos
     */
    @QueryParam("idProductos")
    private List<Integer> idProductos;

    /**
     * Identificador de motivo
     */
    @QueryParam("idMotivo")
    private Integer idMotivo;

    /**
     * Código motivo
     */
    @QueryParam("codigoMotivo")
    private String codigoMotivo;

    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Fecha inicio
     */
    @QueryParam("fechaInicio")
    private Date fechaInicio;

    /**
     * Fecha fin
     */
    @QueryParam("fechaFin")
    private Date fechaFin;

    /**
     * Cambio de estado
     */
    @QueryParam("changeState")
    private Boolean changeState;

    /**
     * Lista de detalle
     */
    private List<ControlInventarioDetalleParam> controlInventarioDetalles;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idUsuarioEncargado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario encargado"));
        }

        if (isNull(idUsuarioSupervisor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario supervisor"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        if (isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }

        if (isNullOrEmpty(controlInventarioDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de control de inventario"));
        } else {
            for (ControlInventarioDetalleParam detalle : controlInventarioDetalles) {
                detalle.setIdControlInventario(0);
                detalle.isValidToCreate();
            }
        }

        return !tieneErrores();
    }
    
    public boolean isValidToCreateTemplate() {

        limpiarErrores();

        if (isNull(idUsuarioEncargado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario encargado"));
        }

        if (isNull(idUsuarioSupervisor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario supervisor"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        if (isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }

        if(isNull(idDepositoLogisticos)) {
            idDepositoLogisticos = new ArrayList<>();
        }

        if(isNull(idProductos)) {
            idProductos = new ArrayList<>();
        }
        
        if(idDepositoLogistico != null && !idDepositoLogisticos.contains(idDepositoLogistico)) {
            idDepositoLogisticos.add(idDepositoLogistico);
        }
        
        if(idProducto != null && !idProductos.contains(idProducto)) {
            idProductos.add(idProducto);
        }
        
        if (isNullOrEmpty(idDepositoLogisticos) && isNullOrEmpty(idProductos)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el depósito logistico y/o producto"));
        }

        return !tieneErrores();
    }
    
    public boolean isValidToGetPdf() {
        
        limpiarErrores();
        
        if(isNull(id) && isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de control de inventario"));
        }
        
        return !tieneErrores();
    }

    public boolean isValidToChangeState() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de control de inventario"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        return !tieneErrores();
    }
    
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();

        if (!isNullOrEmpty(controlInventarioDetalles)) {
            for (ControlInventarioDetalleParam detalle : controlInventarioDetalles) {
                list.addAll(detalle.getErrores());
            }
        }

        return list;
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNull(idUsuarioEncargado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario encargado"));
        }

        if (isNull(idUsuarioSupervisor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario supervisor"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        if (isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }

        return !tieneErrores();
    }
    
    public boolean isValidToComplete() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNullOrEmpty(controlInventarioDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de control de inventario"));
        } else {
            for (ControlInventarioDetalleParam detalle : controlInventarioDetalles) {
                detalle.setIdControlInventario(0);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Integer getIdUsuarioEncargado() {
        return idUsuarioEncargado;
    }

    public void setIdUsuarioEncargado(Integer idUsuarioEncargado) {
        this.idUsuarioEncargado = idUsuarioEncargado;
    }

    public Integer getIdUsuarioSupervisor() {
        return idUsuarioSupervisor;
    }

    public void setIdUsuarioSupervisor(Integer idUsuarioSupervisor) {
        this.idUsuarioSupervisor = idUsuarioSupervisor;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<ControlInventarioDetalleParam> getControlInventarioDetalles() {
        return controlInventarioDetalles;
    }

    public void setControlInventarioDetalles(List<ControlInventarioDetalleParam> controlInventarioDetalles) {
        this.controlInventarioDetalles = controlInventarioDetalles;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setChangeState(Boolean changeState) {
        this.changeState = changeState;
    }

    public Boolean getChangeState() {
        return changeState;
    }

    public void setIdProductos(List<Integer> idProductos) {
        this.idProductos = idProductos;
    }

    public List<Integer> getIdProductos() {
        return idProductos;
    }

    public void setIdDepositoLogisticos(List<Integer> idDepositoLogisticos) {
        this.idDepositoLogisticos = idDepositoLogisticos;
    }

    public List<Integer> getIdDepositoLogisticos() {
        return idDepositoLogisticos;
    }
}
