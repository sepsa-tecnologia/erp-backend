/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioEmpresa;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioEmpresaParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioEmpresaPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "UsuarioEmpresaFacade", mappedName = "UsuarioEmpresaFacade")
@javax.ejb.Local(UsuarioEmpresaFacade.class)
public class UsuarioEmpresaFacadeImpl extends FacadeImpl<UsuarioEmpresa, UsuarioEmpresaParam> implements UsuarioEmpresaFacade {

    public UsuarioEmpresaFacadeImpl() {
        super(UsuarioEmpresa.class);
    }

    @Override
    public Boolean validToCreate(UsuarioEmpresaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        UsuarioEmpresaParam param1 = new UsuarioEmpresaParam();
        param1.setIdUsuario(param.getIdUsuario());
        param1.setIdEmpresa(param.getIdEmpresa());
        UsuarioEmpresa item = findFirst(param1);
        
        if(item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe el registro"));
        }
        
        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());
        
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(UsuarioEmpresaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
   
        UsuarioEmpresaParam param1 = new UsuarioEmpresaParam();
        param1.setIdUsuario(param.getIdUsuario());
        param1.setIdEmpresa(param.getIdEmpresa());
        UsuarioEmpresa item = findFirst(param1);

        if(item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro"));
        } else {
            param.setId(item.getId());
        }
        
        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());
        
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public UsuarioEmpresa create(UsuarioEmpresaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        UsuarioEmpresa item = new UsuarioEmpresa();
        item.setIdUsuario(param.getIdUsuario());
        item.setIdEmpresa(param.getIdEmpresa());
        item.setActivo(param.getActivo());
        item.setFacturable(param.getFacturable());
        
        create(item);
        
        return item;
    }
    
    @Override
    public UsuarioEmpresa edit(UsuarioEmpresaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        UsuarioEmpresa item = find(param.getId());
        item.setIdUsuario(param.getIdUsuario());
        item.setIdEmpresa(param.getIdEmpresa());
        item.setActivo(param.getActivo());
        item.setFacturable(param.getFacturable());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<UsuarioEmpresaPojo> findPojo(UsuarioEmpresaParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ue.id = %d", where, param.getId());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and ue.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and ue.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ue.activo = '%s'", where, param.getActivo());
        }
        
        if(param.getFacturable() != null) {
            where = String.format("%s and ue.facturable = '%s'", where, param.getFacturable());
        }
        
        String sql = String.format("select ue.id, ue.id_usuario, u.usuario,"
                + " ue.id_empresa, e.codigo as codigo_empresa, e.descripcion as empresa,"
                + " e.modulo_inventario, e.id_tipo_empresa, te.codigo as codigo_tipo_empresa,"
                + " ue.activo, ue.facturable, e.id_cliente_sepsa "
                + " from usuario.usuario_empresa ue"
                + " left join usuario.usuario u on u.id = ue.id_usuario"
                + " left join info.empresa e on e.id = ue.id_empresa"
                + " left join info.tipo_empresa te on te.id = e.id_tipo_empresa"
                + " where %s order by ue.activo, ue.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();

        List<UsuarioEmpresaPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            UsuarioEmpresaPojo item = new UsuarioEmpresaPojo();
            item.setId((Integer)objects[i++]);
            item.setIdUsuario((Integer)objects[i++]);
            item.setUsuario((String)objects[i++]);
            item.setIdEmpresa((Integer)objects[i++]);
            item.setCodigoEmpresa((String)objects[i++]);
            item.setEmpresa((String)objects[i++]);
            item.setModuloInventario((Character)objects[i++]);
            item.setIdTipoEmpresa((Integer)objects[i++]);
            item.setCodigoTipoEmpresa((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setFacturable((Character)objects[i++]);
            item.setIdClienteSepsa((Integer)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public List<UsuarioEmpresa> find(UsuarioEmpresaParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ue.id = %d", where, param.getId());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and ue.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and ue.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ue.activo = '%s'", where, param.getActivo());
        }
        
        if(param.getFacturable() != null) {
            where = String.format("%s and ue.facturable = '%s'", where, param.getFacturable());
        }
        
        String sql = String.format("select ue.*"
                + " from usuario.usuario_empresa ue"
                + " left join usuario.usuario u on u.id = ue.id_usuario"
                + " left join info.empresa e on e.id = ue.id_empresa"
                + " where %s order by ue.activo, ue.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, UsuarioEmpresa.class);
        
        List<UsuarioEmpresa> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(UsuarioEmpresaParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and ue.id = %d", where, param.getId());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and ue.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and ue.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and ue.activo = '%s'", where, param.getActivo());
        }
        
        if(param.getFacturable() != null) {
            where = String.format("%s and ue.facturable = '%s'", where, param.getFacturable());
        }
        
        String sql = String.format("select count(ue.id)"
                + " from usuario.usuario_empresa ue"
                + " left join usuario.usuario u on u.id = ue.id_usuario"
                + " left join info.empresa e on e.id = ue.id_empresa"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public void asociarMasivo(UsuarioEmpresaParam param, UserInfoImpl userInfo) {
        
        param.setFirstResult(0);
        param.setPageSize(500);
        List<UsuarioEmpresa> lista = findRelacionados(param);

        for (UsuarioEmpresa item : lista) {

            UsuarioEmpresaParam param1 = new UsuarioEmpresaParam();
            param1.setIdUsuario(item.getIdUsuario());
            param1.setIdEmpresa(item.getIdEmpresa());
            param1.setActivo(param.getActivo());
            param1.setFacturable(param.getFacturable());

            editarOCrear(param1, userInfo);
        }
        
    }
    
    @Override
    public List<UsuarioEmpresa> findRelacionados(UsuarioEmpresaParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and e.id = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select coalesce(ue.id, 0) as id, coalesce(ue.id_usuario, %d) as id_usuario, e.id as id_empresa, coalesce(ue.activo, 'N') as activo\n"
                + "from info.empresa e\n"
                + "left join usuario.usuario_empresa ue on ue.id_empresa = e.id and ue.id_usuario = %d\n"
                + "where %s limit %d offset %d", param.getIdUsuario(), param.getIdUsuario(), where, param.getPageSize(), param.getFirstResult());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<UsuarioEmpresa> result = Lists.empty();
        
        for (Object[] objects : list) {
            int i = 0;
            UsuarioEmpresa ue = new UsuarioEmpresa();
            ue.setId((Integer)objects[i++]);
            ue.setIdUsuario((Integer)objects[i++]);
            ue.setIdEmpresa((Integer)objects[i++]);
            ue.setActivo((Character)objects[i++]);
            ue.setEmpresa(facades.getEmpresaFacade().find(ue.getIdEmpresa()));
            ue.setUsuario(facades.getUsuarioFacade().find(ue.getIdUsuario()));
            result.add(ue);
        }
        
        return result;
    }

    @Override
    public Long findRelacionadosSize(UsuarioEmpresaParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and e.id = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select count(e)\n"
                + "from info.empresa e\n"
                + "left join usuario.usuario_empresa ue on ue.id_empresa = e.id and ue.id_usuario = %d\n"
                + "where %s", param.getIdUsuario(), where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
