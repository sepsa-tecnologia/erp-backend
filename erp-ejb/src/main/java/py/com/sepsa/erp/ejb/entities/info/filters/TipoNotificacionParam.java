/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Clase para el manejo de los parámetros de tipo notificacion
 * @author Jonathan
 */
public class TipoNotificacionParam extends CommonParam {
    
    public TipoNotificacionParam() {
    }

    public TipoNotificacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Email
     */
    @QueryParam("email")
    private Character email;
    
    /**
     * Telefono
     */
    @QueryParam("telefono")
    private Character telefono;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Character getEmail() {
        return email;
    }

    public void setEmail(Character email) {
        this.email = email;
    }

    public Character getTelefono() {
        return telefono;
    }

    public void setTelefono(Character telefono) {
        this.telefono = telefono;
    }
}
