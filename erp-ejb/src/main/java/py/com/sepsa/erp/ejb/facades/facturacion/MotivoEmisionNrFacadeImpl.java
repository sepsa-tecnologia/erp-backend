/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionNr;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.MotivoEmisionNrParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "MotivoEmisionNrFacade", mappedName = "MotivoEmisionNrFacade")
@Local(MotivoEmisionNrFacade.class)
public class MotivoEmisionNrFacadeImpl extends FacadeImpl<MotivoEmisionNr, MotivoEmisionNrParam> implements MotivoEmisionNrFacade {

    public MotivoEmisionNrFacadeImpl() {
        super(MotivoEmisionNr.class);
    }
    
    public Boolean validToCreate(MotivoEmisionNrParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MotivoEmisionNrParam param1 = new MotivoEmisionNrParam();
        param1.setCodigo(param.getCodigo().trim());
        
        MotivoEmisionNr mei = findFirst(param1);
        
        if(!isNull(mei)) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(MotivoEmisionNrParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        MotivoEmisionNr item = find(param.getId());
        
        if(isNull(item)) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
            
        } else if (!item.getCodigo().equals(param.getCodigo().trim())) {
            
            MotivoEmisionNrParam param1 = new MotivoEmisionNrParam();
            param1.setCodigo(param.getCodigo().trim());

            MotivoEmisionNr mei = findFirst(param1);

            if(!isNull(mei)) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public MotivoEmisionNr create(MotivoEmisionNrParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MotivoEmisionNr result = new MotivoEmisionNr();
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        
        create(result);
        
        return result;
    }

    @Override
    public MotivoEmisionNr edit(MotivoEmisionNrParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MotivoEmisionNr result = find(param.getId());
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigo(param.getCodigo().trim());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public MotivoEmisionNr find(Integer id, String codigo) {
        
        MotivoEmisionNrParam param = new MotivoEmisionNrParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<MotivoEmisionNr> find(MotivoEmisionNrParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MotivoEmisionNr.class);
        Root<MotivoEmisionNr> root = cq.from(MotivoEmisionNr.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<MotivoEmisionNr> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MotivoEmisionNrParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<MotivoEmisionNr> root = cq.from(MotivoEmisionNr.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
