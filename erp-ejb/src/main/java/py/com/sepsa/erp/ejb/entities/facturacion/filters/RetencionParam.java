/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de retención
 * @author Jonathan D. Bernal Fernández
 */
public class RetencionParam extends CommonParam {
    
    public RetencionParam() {
    }

    public RetencionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Cliente
     */
    @QueryParam("cliente")
    private String cliente;
    
    /**
     * Nro de retención
     */
    @QueryParam("nroRetencion")
    private String nroRetencion;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;
    
    /**
     * Porcentaje retención
     */
    @QueryParam("porcentajeRetencion")
    private Integer porcentajeRetencion;
    
    /**
     * Monto retenido total
     */
    @QueryParam("montoRetenidoTotal")
    private BigDecimal montoRetenidoTotal;
    
    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;
    
    /**
     * Observación anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;
    
    /**
     * Detalle
     */
    private List<RetencionDetalleParam> retencionDetalles;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de retención"));
        } else {
            
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            
            if(cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la retención no puede ser posterior a la fecha actual"));
            }
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA total"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if(isNull(montoRetenidoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto retenido total"));
        }
        
        if(isNull(porcentajeRetencion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de retención"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNullOrEmpty(nroRetencion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de retención"));
        }
        
        if(isNullOrEmpty(retencionDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de retención"));
        } else {
            for (RetencionDetalleParam detalle : retencionDetalles) {
                detalle.setFecha(fecha);
                detalle.setIdRetencion(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCancel() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de retención"));
        }
        
        if(isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identiifcador de motivo de anulación"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = new ArrayList<>();
        
        if(retencionDetalles != null) {
            for (RetencionDetalleParam detalle : retencionDetalles) {
                result.addAll(detalle.getErrores());
            }
        }
        
        return result;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public void setRetencionDetalles(List<RetencionDetalleParam> retencionDetalles) {
        this.retencionDetalles = retencionDetalles;
    }

    public List<RetencionDetalleParam> getRetencionDetalles() {
        return retencionDetalles;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }
}
