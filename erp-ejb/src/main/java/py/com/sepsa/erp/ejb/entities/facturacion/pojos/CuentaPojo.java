/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.util.Date;

/**
 * POJO para la entidad cuenta entidad financiera
 * @author Jonathan
 */
public class CuentaPojo {

    public CuentaPojo(Integer id, Integer idEntidadFinanciera,
            String entidadFinanciera, Integer idPersona, String persona,
            String nroCuenta, Date fechaAlta, Character estado) {
        this.id = id;
        this.idEntidadFinanciera = idEntidadFinanciera;
        this.entidadFinanciera = entidadFinanciera;
        this.idPersona = idPersona;
        this.persona = persona;
        this.nroCuenta = nroCuenta;
        this.fechaAlta = fechaAlta;
        this.estado = estado;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de entidad financiera
     */
    private Integer idEntidadFinanciera;
    
    /**
     * Entidad financiera
     */
    private String entidadFinanciera;
    
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    
    /**
     * Persona
     */
    private String persona;
    
    /**
     * Nro de cuenta
     */
    private String nroCuenta;
    
    /**
     * Fecha de alta
     */
    private Date fechaAlta;

    /**
     * Estado
     */
    private Character estado;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public String getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(String entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getEstado() {
        return estado;
    }
}
