/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.MontoMinimo;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ExcedenteDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.HistoricoLiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.HistoricoLiquidacionPojo;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "HistoricoLiquidacionFacade", mappedName = "HistoricoLiquidacionFacade")
@Local(HistoricoLiquidacionFacade.class)
public class HistoricoLiquidacionFacadeImpl extends FacadeImpl<HistoricoLiquidacion, HistoricoLiquidacionParam> implements HistoricoLiquidacionFacade {

    public HistoricoLiquidacionFacadeImpl() {
        super(HistoricoLiquidacion.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarLiquidacion Bandera para validar liquidación
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(HistoricoLiquidacionParam param, Boolean validarLiquidacion) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        if(param.getIdMontoMinimo() != null) {
            
            MontoMinimo montoMinimo = facades
                    .getMontoMinimoFacade()
                    .find(param.getIdMontoMinimo());
            
            if(montoMinimo == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el monto minimo"));
            }
        }
        
        Liquidacion liquidacion = facades
                .getLiquidacionFacade()
                .find(param.getIdLiquidacion());
        
        if(validarLiquidacion && liquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la liquidación"));
        }
        
        if(param.getLiquidacionDetalles() != null) {
            for (LiquidacionDetalleParam item : param.getLiquidacionDetalles()) {
                facades.getLiquidacionDetalleFacade()
                        .validToCreate(item, Boolean.FALSE);
            }
        }
        
        if(param.getExcedenteDetalles() != null) {
            for (ExcedenteDetalleParam item : param.getExcedenteDetalles()) {
                facades.getExcedenteDetalleFacade()
                        .validToCreate(item, Boolean.FALSE);
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(HistoricoLiquidacionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        HistoricoLiquidacion historicoLiquidacion = facades
                .getHistoricoLiquidacionFacade()
                .find(param.getIdHistoricoLiquidacion());
        
        if(historicoLiquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el historico de liquidación"));
        }
        
        if(param.getIdMontoMinimo() != null) {
            
            MontoMinimo montoMinimo = facades
                    .getMontoMinimoFacade()
                    .find(param.getIdMontoMinimo());
            
            if(montoMinimo == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el monto minimo"));
            }
        }
        
        Liquidacion liquidacion = facades
                .getLiquidacionFacade()
                .find(param.getIdLiquidacion());
        
        if(liquidacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra la liquidación"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public HistoricoLiquidacion create(HistoricoLiquidacionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, Boolean.TRUE)) {
            return null;
        }
        
        HistoricoLiquidacion hl = new HistoricoLiquidacion();
        hl.setIdEmpresa(userInfo.getIdEmpresa());
        hl.setFechaProceso(Calendar.getInstance().getTime());
        hl.setMontoTarifaDetalle(param.getMontoTarifaDetalle());
        hl.setMontoDescuentoDetalle(param.getMontoDescuentoDetalle());
        hl.setMontoTotalDetalle(param.getMontoTotalDetalle());
        hl.setMontoTotalExcedente(param.getMontoTotalExcedente());
        hl.setMontoDescuentoExcedente(param.getMontoDescuentoExcedente());
        hl.setMontoDescuentoGeneral(param.getMontoDescuentoGeneral());
        hl.setMontoTotalParcial(param.getMontoTotalParcial());
        hl.setMontoTotalLiquidado(param.getMontoTotalLiquidado());
        hl.setMontoAcuerdo(param.getMontoAcuerdo());
        hl.setMontoMinimo(param.getMontoMinimo());
        hl.setIdMontoMinimo(param.getIdMontoMinimo());
        hl.setBonificado(param.getBonificado());
        hl.setCobroAdelantado(param.getCobroAdelantado());
        hl.setIdLiquidacion(param.getIdLiquidacion());
        
        create(hl);
        
        if(param.getExcedenteDetalles() != null) {
            for (ExcedenteDetalleParam item : param.getExcedenteDetalles()) {
                item.setIdHistoricoLiquidacion(hl.getId());
                facades.getExcedenteDetalleFacade().create(item, userInfo);
            }
        }
        
        if(param.getLiquidacionDetalles() != null) {
            for (LiquidacionDetalleParam item : param.getLiquidacionDetalles()) {
                item.setIdHistoricoLiquidacion(hl.getId());
                facades.getLiquidacionDetalleFacade().create(item, userInfo);
            }
        }
        
        return hl;
    }
    
    @Override
    public HistoricoLiquidacion edit(HistoricoLiquidacionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        HistoricoLiquidacion historicoLiquidacion = find(param.getIdHistoricoLiquidacion());
        historicoLiquidacion.setFechaProceso(param.getFechaProceso());
        historicoLiquidacion.setMontoTotalDetalle(param.getMontoTotalDetalle());
        historicoLiquidacion.setMontoTotalExcedente(param.getMontoTotalExcedente());
        historicoLiquidacion.setMontoTotalLiquidado(param.getMontoTotalLiquidado());
        historicoLiquidacion.setMontoDescuentoDetalle(param.getMontoDescuentoDetalle());
        historicoLiquidacion.setMontoDescuentoExcedente(param.getMontoDescuentoExcedente());
        historicoLiquidacion.setMontoDescuentoGeneral(param.getMontoDescuentoGeneral());
        historicoLiquidacion.setMontoMinimo(param.getMontoMinimo());
        historicoLiquidacion.setIdMontoMinimo(param.getIdMontoMinimo());
        historicoLiquidacion.setBonificado(param.getBonificado());
        historicoLiquidacion.setCobroAdelantado(param.getCobroAdelantado());
        historicoLiquidacion.setIdLiquidacion(param.getIdLiquidacion());
        
        edit(historicoLiquidacion);
        
        return historicoLiquidacion;
    }
    
    @Override
    public List<HistoricoLiquidacion> find(HistoricoLiquidacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(HistoricoLiquidacion.class);
        Root<HistoricoLiquidacion> root = cq.from(HistoricoLiquidacion.class);
        Join<HistoricoLiquidacion, Liquidacion> liquidacion = root.join("liquidacion", JoinType.LEFT);
        Join<Liquidacion, Contrato> contrato = liquidacion.join("contrato", JoinType.LEFT);
        Join<Contrato, Cliente> cliente = contrato.join("cliente", JoinType.LEFT);
        Join<Contrato, Producto> producto = contrato.join("producto", JoinType.LEFT);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getActual() != null) {
            if(param.getActual()) {
                predList.add(qb.equal(root.get("idHistoricoLiquidacion"), liquidacion.get("idHistoricoLiquidacion")));
            } else {
                predList.add(qb.notEqual(root.get("idHistoricoLiquidacion"), liquidacion.get("idHistoricoLiquidacion")));
            }
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(producto.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(producto.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(liquidacion.get("idServicio"), param.getIdServicio()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(contrato.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdHistoricoLiquidacion() != null) {
            predList.add(qb.equal(root.get("idHistoricoLiquidacion"), param.getIdHistoricoLiquidacion()));
        }
        
        if (param.getBonificado() != null) {
            predList.add(qb.equal(root.get("bonificado"), param.getBonificado()));
        }
        
        if (param.getMontoMinimo() != null) {
            predList.add(qb.equal(root.get("montoMinimo"), param.getMontoMinimo()));
        }
        
        if (param.getIdMontoMinimo() != null) {
            predList.add(qb.equal(root.get("idMontoMinimo"), param.getIdMontoMinimo()));
        }
        
        if (param.getCobroAdelantado() != null) {
            predList.add(qb.equal(root.get("cobroAdelantado"), param.getCobroAdelantado()));
        }
        
        if (param.getIdLiquidacion() != null) {
            predList.add(qb.equal(root.get("idLiquidacion"), param.getIdLiquidacion()));
        }
        
        if (param.getFechaProceso() != null) {
            predList.add(qb.equal(root.get("fechaProceso"), param.getFechaProceso()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(cliente.get("idCliente")),
                qb.desc(producto.get("id")));
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<HistoricoLiquidacion> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(HistoricoLiquidacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<HistoricoLiquidacion> root = cq.from(HistoricoLiquidacion.class);
        Join<HistoricoLiquidacion, Liquidacion> liquidacion = root.join("liquidacion", JoinType.LEFT);
        Join<Liquidacion, Contrato> contrato = liquidacion.join("contrato", JoinType.LEFT);
        Join<Contrato, Producto> producto = contrato.join("producto", JoinType.LEFT);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getActual() != null) {
            if(param.getActual()) {
                predList.add(qb.equal(root.get("idHistoricoLiquidacion"), liquidacion.get("idHistoricoLiquidacion")));
            } else {
                predList.add(qb.notEqual(root.get("idHistoricoLiquidacion"), liquidacion.get("idHistoricoLiquidacion")));
            }
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(producto.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(producto.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(liquidacion.get("idServicio"), param.getIdServicio()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(contrato.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdHistoricoLiquidacion() != null) {
            predList.add(qb.equal(root.get("idHistoricoLiquidacion"), param.getIdHistoricoLiquidacion()));
        }
        
        if (param.getBonificado() != null) {
            predList.add(qb.equal(root.get("bonificado"), param.getBonificado()));
        }
        
        if (param.getMontoMinimo() != null) {
            predList.add(qb.equal(root.get("montoMinimo"), param.getMontoMinimo()));
        }
        
        if (param.getIdMontoMinimo() != null) {
            predList.add(qb.equal(root.get("idMontoMinimo"), param.getIdMontoMinimo()));
        }
        
        if (param.getCobroAdelantado() != null) {
            predList.add(qb.equal(root.get("cobroAdelantado"), param.getCobroAdelantado()));
        }
        
        if (param.getIdLiquidacion() != null) {
            predList.add(qb.equal(root.get("idLiquidacion"), param.getIdLiquidacion()));
        }
        
        if (param.getFechaProceso() != null) {
            predList.add(qb.equal(root.get("fechaProceso"), param.getFechaProceso()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    /**
     * Obtiene la lista de liquidación periodo
     * @param param parámetros
     * @return Lista
     */
    @Override
    public List<HistoricoLiquidacionPojo> findPeriodo(HistoricoLiquidacionParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdPeriodoLiquidacion() != null) {
            where = String.format(" %s and l.id_periodo_liquidacion = %d", where, param.getIdPeriodoLiquidacion());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        if(param.getIdProducto() != null) {
            where = String.format(" %s and c.id_producto = %d", where, param.getIdProducto());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format(" %s and l.id_estado = %d", where, param.getIdEstado());
        }
        
        String sql = String.format("select l.id as id_liquidacion, l.id_historico_liquidacion, cli.id_cliente, cli.razon_social,\n"
                + "l.id_periodo_liquidacion, l.fecha_desde, l.fecha_hasta, l.id_estado, e.descripcion as estado, hl.fecha_proceso,\n"
                + "hl.monto_total_excedente, hl.monto_total_liquidado,\n"
                + "hl.bonificado, hl.monto_minimo, p.id as id_producto, p.descripcion as producto,\n"
                + "s.id as id_servicio, s.descripcion as servicio,\n"
                + "coalesce(per.razon_social, per.nombre || ' ' || per.apellido) as comercial, \n"
                + "count(ld.nro_linea) as cant_lineas\n"
                + "from facturacion.liquidacion l\n"
                + "left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "left join facturacion.excedente_detalle ed on ed.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "left join comercial.contrato c on c.id = l.id_contrato\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join info.persona per on per.id = cli.id_comercial\n"
                + "left join comercial.producto p on p.id = c.id_producto\n"
                + "left join comercial.servicio s on s.id = l.id_servicio\n"
                + "left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "where %s group by l.id, hl.id_historico_liquidacion, c.id, cli.id_cliente,\n"
                + "per.id, p.id, s.id, ed.cant_doc_excedidos\n"
                + "order by cli.id_cliente, p.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<HistoricoLiquidacionPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            HistoricoLiquidacionPojo item = new HistoricoLiquidacionPojo();
            item.setIdLiquidacion((Integer)objects[i++]);
            item.setIdHistoricoLiquidacion((Integer)objects[i++]);
            item.setIdCliente((Integer)objects[i++]);
            item.setCliente((String)objects[i++]);
            item.setIdPeriodoLiquidacion((Integer)objects[i++]);
            item.setFechaDesde((Date)objects[i++]);
            item.setFechaHasta((Date)objects[i++]);
            item.setIdEstado((Integer)objects[i++]);
            item.setEstado((String)objects[i++]);
            item.setFechaProceso((Date)objects[i++]);
            item.setMontoTotalExcedente((BigDecimal)objects[i++]);
            item.setMontoTotalLiquidado((BigDecimal)objects[i++]);
            item.setBonificado((Character)objects[i++]);
            item.setMontoMinimo((Character)objects[i++]);
            item.setIdProducto((Integer)objects[i++]);
            item.setProducto((String)objects[i++]);
            item.setIdServicio((Integer)objects[i++]);
            item.setServicio((String)objects[i++]);
            item.setComercial((String)objects[i++]);
            item.setCantLineas((Number)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de liquidación periodo
     * @param param parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findPeriodoSize(HistoricoLiquidacionParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdPeriodoLiquidacion() != null) {
            where = String.format(" %s and l.id_periodo_liquidacion = %d", where, param.getIdPeriodoLiquidacion());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        if(param.getIdProducto() != null) {
            where = String.format(" %s and c.id_producto = %d", where, param.getIdProducto());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format(" %s and l.id_estado = %d", where, param.getIdEstado());
        }
        
        String sql = String.format("select count(l.id)\n"
                + "from facturacion.liquidacion l\n"
                + "left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "left join facturacion.excedente_detalle ed on ed.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "left join comercial.contrato c on c.id = l.id_contrato\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join info.persona per on per.id = cli.id_comercial\n"
                + "left join comercial.producto p on p.id = c.id_producto\n"
                + "left join comercial.servicio s on s.id = l.id_servicio\n"
                + "where %s\n", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }

    @Override
    public List<HistoricoLiquidacionPojo> findCadena(HistoricoLiquidacionParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("(select ld.id_cliente_rel as id_comprador, c.razon_social as Concepto,\n"
                + "  sum(ROUND(ld.monto_tarifa+(ld.monto_tarifa*10/100), 0)) as LINEA,\n"
                + "  sum(ROUND(ld.monto_descuento+(ld.monto_descuento*10/100), 0)) as DESCUENTO,\n"
                + "  sum(ROUND((ld.monto_tarifa-ld.monto_descuento)+((ld.monto_tarifa-ld.monto_descuento)*10/100), 0)) as TOTAL\n"
                + "  from facturacion.liquidacion l\n"
                + "  left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "  left join facturacion.liquidacion_detalle ld on hl.id_historico_liquidacion = ld.id_historico_liquidacion\n"
                + "  join comercial.cliente c on c.id_cliente = ld.id_cliente_rel\n"
                + "  where %s\n"
                + "  and ld.id_sociedad_cliente_rel is null\n"
                + "  and l.estado in ('P','F','X')\n"
                + "  group by id_comprador, c.razon_social) \n"
                + "UNION\n"
                + " (select 8 as id_comprador, gc.descripcion as Concepto,\n"
                + "  sum(ROUND(ld.monto_tarifa+(ld.monto_tarifa*10/100), 0)) as LINEA,\n"
                + "  sum(ROUND(ld.monto_descuento+(ld.monto_descuento*10/100), 0)) as DESCUENTO,\n"
                + "  sum(ROUND((ld.monto_tarifa-ld.monto_descuento)+((ld.monto_tarifa-ld.monto_descuento)*10/100), 0)) as TOTAL\n"
                + "  from facturacion.liquidacion l\n"
                + "  left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "  left join facturacion.liquidacion_detalle ld on hl.id_historico_liquidacion = ld.id_historico_liquidacion\n"
                + "  join comercial.grupo_cliente gc on gc.id = ld.id_grupo_cliente_rel\n"
                + "  where %s\n"
                + "  and l.estado in ('P','F','X')\n"
                + "  group by id_comprador, gc.id)\n"
                + "UNION\n"
                + " (select ld.id_cliente_rel as id_comprador, s.descripcion as Concepto,\n"
                + "  sum(ROUND(ld.monto_tarifa+(ld.monto_tarifa*10/100), 0)) as LINEA,\n"
                + "  sum(ROUND(ld.monto_descuento+(ld.monto_descuento*10/100), 0)) as DESCUENTO,\n"
                + "  sum(ROUND((ld.monto_tarifa-ld.monto_descuento)+((ld.monto_tarifa-ld.monto_descuento)*10/100), 0)) as TOTAL\n"
                + "  from facturacion.liquidacion l\n"
                + "  left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "  left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "  join comercial.sociedad_cliente s on s.id = ld.id_sociedad_cliente_rel\n"
                + "  where %s\n"
                + "  and l.estado in ('P','F','X')\n"
                + "  group by id_comprador, s.descripcion )\n"
                + "  \n"
                + "order by Concepto\n"
                + "offset %d limit %d",
                where, where, where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<HistoricoLiquidacionPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            
            HistoricoLiquidacionPojo item = new HistoricoLiquidacionPojo();
            item.setIdCliente((Integer)objects[0]);
            item.setCliente((String)objects[1]);
            item.setLinea((BigDecimal)objects[2]);
            item.setDescuento((BigDecimal)objects[3]);
            item.setTotal((BigDecimal)objects[4]);
            result.add(item);
        }
        
        return result;
    }

    @Override
    public Long findCadenaSize(HistoricoLiquidacionParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and l.fecha_desde >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and l.fecha_hasta <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select count(*)\n"
                + "FROM ((select ld.id_cliente_rel as id_comprador, c.razon_social as Concepto,\n"
                + "  sum(ROUND(ld.monto_tarifa+(ld.monto_tarifa*10/100), 0)) as LINEA,\n"
                + "  sum(ROUND(ld.monto_descuento+(ld.monto_descuento*10/100), 0)) as DESCUENTO,\n"
                + "  sum(ROUND((ld.monto_tarifa-ld.monto_descuento)+((ld.monto_tarifa-ld.monto_descuento)*10/100), 0)) as TOTAL\n"
                + "  from facturacion.liquidacion l\n"
                + "  left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "  left join facturacion.liquidacion_detalle ld on hl.id_historico_liquidacion = ld.id_historico_liquidacion\n"
                + "  join comercial.cliente c on c.id_cliente = ld.id_cliente_rel\n"
                + "  where %s\n"
                + "  and ld.id_sociedad_cliente_rel is null\n"
                + "  and l.estado in ('P','F','X')\n"
                + "  group by id_comprador, c.razon_social) \n"
                + "UNION\n"
                + " (select 8 as id_comprador, gc.descripcion as Concepto,\n"
                + "  sum(ROUND(ld.monto_tarifa+(ld.monto_tarifa*10/100), 0)) as LINEA,\n"
                + "  sum(ROUND(ld.monto_descuento+(ld.monto_descuento*10/100), 0)) as DESCUENTO,\n"
                + "  sum(ROUND((ld.monto_tarifa-ld.monto_descuento)+((ld.monto_tarifa-ld.monto_descuento)*10/100), 0)) as TOTAL\n"
                + "  from facturacion.liquidacion l\n"
                + "  left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "  left join facturacion.liquidacion_detalle ld on hl.id_historico_liquidacion = ld.id_historico_liquidacion\n"
                + "  join comercial.grupo_cliente gc on gc.id = ld.id_grupo_cliente_rel\n"
                + "  where %s\n"
                + "  and l.estado in ('P','F','X')\n"
                + "  group by id_comprador, gc.id)\n"
                + "UNION\n"
                + " (select ld.id_cliente_rel as id_comprador, s.descripcion as Concepto,\n"
                + "  sum(ROUND(ld.monto_tarifa+(ld.monto_tarifa*10/100), 0)) as LINEA,\n"
                + "  sum(ROUND(ld.monto_descuento+(ld.monto_descuento*10/100), 0)) as DESCUENTO,\n"
                + "  sum(ROUND((ld.monto_tarifa-ld.monto_descuento)+((ld.monto_tarifa-ld.monto_descuento)*10/100), 0)) as TOTAL\n"
                + "  from facturacion.liquidacion l\n"
                + "  left join facturacion.historico_liquidacion hl on hl.id_historico_liquidacion = l.id_historico_liquidacion\n"
                + "  left join facturacion.liquidacion_detalle ld on ld.id_historico_liquidacion = hl.id_historico_liquidacion\n"
                + "  join comercial.sociedad_cliente s on s.id = ld.id_sociedad_cliente_rel\n"
                + "  where %s\n"
                + "  and l.estado in ('P','F','X')\n"
                + "  group by id_comprador, s.descripcion )) as a\n",
                where, where, where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
