/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.erp.ejb.entities.info.Local;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de solicitud nota crédito
 * @author Jonathan D. Bernal Fernández
 */
public class SolicitudNotaCreditoParam extends CommonParam {
    
    public SolicitudNotaCreditoParam() {
    }

    public SolicitudNotaCreditoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Nro de documento
     */
    @QueryParam("nroDocumento")
    private String nroDocumento;
    
    /**
     * Recibido
     */
    @QueryParam("recibido")
    private Character recibido;
    
    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;
    
    /**
     * Archivo EDI
     */
    @QueryParam("archivoEdi")
    private Character archivoEdi;
    
    /**
     * Generado EDI
     */
    @QueryParam("generadoEdi")
    private Character generadoEdi;
    
    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Monto iva 5
     */
    @QueryParam("montoIva5")
    private BigDecimal montoIva5;
    
    /**
     * Monto imponible 5
     */
    @QueryParam("montoImponible5")
    private BigDecimal montoImponible5;
    
    /**
     * Monto total 5
     */
    @QueryParam("montoTotal5")
    private BigDecimal montoTotal5;
    
    /**
     * Monto iva 10
     */
    @QueryParam("montoIva10")
    private BigDecimal montoIva10;
    
    /**
     * Monto imponible 10
     */
    @QueryParam("montoImponible10")
    private BigDecimal montoImponible10;
    
    /**
     * Monto total 10
     */
    @QueryParam("montoTotal10")
    private BigDecimal montoTotal10;
    
    /**
     * Monto total exento
     */
    @QueryParam("montoTotalExento")
    private BigDecimal montoTotalExento;
    
    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto total de solicitud nota de crédito
     */
    @QueryParam("montoTotalSolicitudNotaCredito")
    private BigDecimal montoTotalSolicitudNotaCredito;
    
    /**
     * Identificador de procesamiento de archivo
     */
    @QueryParam("idProcesamientoArchivo")
    private Integer idProcesamientoArchivo;
    
    /**
     * Hash de procesamiento de archivo
     */
    @QueryParam("hashProcesamientoArchivo")
    private String hashProcesamientoArchivo;
    
    /**
     * Identificador de local origen
     */
    private Integer idLocalOrigen;
    
    /**
     * Local origen
     */
    private Local localOrigen;
    
    /**
     * Identificador de local destino
     */
    private Integer idLocalDestino;
    
    /**
     * Local destino
     */
    private Local localDestino;
    
    /**
     * Detalles
     */
    private List<SolicitudNotaCreditoDetalleParam> solicitudNotaCreditoDetalles;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente"));
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de anulado"));
        }
        
        if(isNull(archivoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo EDI"));
        }
        
        if(isNull(generadoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si ya fue generado el archivo EDI"));
        }
        
        if(isNull(montoIva5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 5%"));
        }
        
        if(isNull(montoImponible5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 5%"));
        }
        
        if(isNull(montoTotal5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 5%"));
        }
        
        if(isNull(montoIva10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 10%"));
        }
        
        if(isNull(montoImponible10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 10%"));
        }
        
        if(isNull(montoTotal10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 10%"));
        }
        
        if(isNull(montoTotalExento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excento"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA"));
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoTotalSolicitudNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total de la solicitud nota de crédito"));
        }
        
        if(isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del cliente"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el ruc/ci"));
        }
        
        if(!isNull(recibido) && recibido.equals('S') && isNullOrEmpty(nroDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de documento"));
        }
        
        if(isNull(recibido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento es recibido o no"));
        }
        
        if(isNull(idLocalOrigen)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de local origen"));
        }
        
        if(isNull(idLocalDestino)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de local destino"));
        }
        
        if(isNullOrEmpty(solicitudNotaCreditoDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la SNC"));
        } else {
            for (SolicitudNotaCreditoDetalleParam detalle : solicitudNotaCreditoDetalles) {
                detalle.setIdSolicitudNotaCredito(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de SNC"));
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de anulado"));
        }
        
        if(isNull(archivoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo EDI"));
        }
        
        if(isNull(generadoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si ya fue generado el archivo EDI"));
        }
        
        if(isNull(recibido)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el documento es recibido o no"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> result = new ArrayList<>();
        
        if(solicitudNotaCreditoDetalles != null) {
            for (SolicitudNotaCreditoDetalleParam detalle : solicitudNotaCreditoDetalles) {
                result.addAll(detalle.getErrores());
            }
        }
        
        return result;
    }
    
    public boolean isValidToUpdateFileProcess() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de solicitud de nota de crédito"));
        }
        
        if(isNull(idProcesamientoArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento de archivo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public void setMontoTotalSolicitudNotaCredito(BigDecimal montoTotalSolicitudNotaCredito) {
        this.montoTotalSolicitudNotaCredito = montoTotalSolicitudNotaCredito;
    }

    public BigDecimal getMontoTotalSolicitudNotaCredito() {
        return montoTotalSolicitudNotaCredito;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public String getHashProcesamientoArchivo() {
        return hashProcesamientoArchivo;
    }

    public void setHashProcesamientoArchivo(String hashProcesamientoArchivo) {
        this.hashProcesamientoArchivo = hashProcesamientoArchivo;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public void setSolicitudNotaCreditoDetalles(List<SolicitudNotaCreditoDetalleParam> solicitudNotaCreditoDetalles) {
        this.solicitudNotaCreditoDetalles = solicitudNotaCreditoDetalles;
    }

    public List<SolicitudNotaCreditoDetalleParam> getSolicitudNotaCreditoDetalles() {
        return solicitudNotaCreditoDetalles;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Character getRecibido() {
        return recibido;
    }

    public void setRecibido(Character recibido) {
        this.recibido = recibido;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Local getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(Local localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Local getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(Local localDestino) {
        this.localDestino = localDestino;
    }
}
