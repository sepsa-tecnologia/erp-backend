/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionValorParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.TipoArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.TipoDocumentoProceso;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesamientoArchivoDetalleParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesamientoArchivoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Files;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ProcesamientoArchivoFacade", mappedName = "ProcesamientoArchivoFacade")
@javax.ejb.Local(ProcesamientoArchivoFacade.class)
public class ProcesamientoArchivoFacadeImpl extends FacadeImpl<ProcesamientoArchivo, ProcesamientoArchivoParam> implements ProcesamientoArchivoFacade {

    public ProcesamientoArchivoFacadeImpl() {
        super(ProcesamientoArchivo.class);
    }

    @Override
    public Boolean validToCreate(ProcesamientoArchivoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoArchivo tipoArchivo = facades.getTipoArchivoFacade()
                .find(param.getIdTipoArchivo(), param.getCodigoTipoArchivo());
        
        if(isNull(tipoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de archivo"));
        } else {
            param.setIdTipoArchivo(tipoArchivo.getId());
        }
        
        TipoDocumentoProceso tipoDocumentoProceso = facades.getTipoDocumentoProcesoFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento(),
                        param.getIdTipoArchivo(), param.getCodigoTipoArchivo());
        
        if(isNull(tipoDocumentoProceso)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de documento"));
        } else {
            param.setIdTipoDocumento(tipoDocumentoProceso.getId());
        }
        
        EstadoPojo estado = facades.getEstadoFacade()
                .find(param.getIdEstado(), param.getCodigoEstado(), "PROCESAMIENTO_ARCHIVO");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        if(!isNull(param.getIdLocalOrigen())) {
            Local local = facades.getLocalFacade().find(param.getIdLocalOrigen());
            
            if(isNull(local)) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local origen"));
            }
        }
        
        if(!isNull(param.getIdLocalDestino())) {
            Local local = facades.getLocalFacade().find(param.getIdLocalDestino());
            
            if(isNull(local)) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local destino"));
            }
        }
        
        if(!isNull(param.getProcesamientoArchivoDetalles())) {
            for (ProcesamientoArchivoDetalleParam procesamientoArchivoDetalle : param.getProcesamientoArchivoDetalles()) {
                facades.getProcesamientoArchivoDetalleFacade().validToCreate(procesamientoArchivoDetalle, false);
            }
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(ProcesamientoArchivoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ProcesamientoArchivo procesamientoArchivo = find(param.getId());
        
        if(isNull(procesamientoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de procesamiento de archivo"));
        }
        
        TipoArchivo tipoArchivo = facades.getTipoArchivoFacade()
                .find(param.getIdTipoArchivo(), param.getCodigoTipoArchivo());
        
        if(isNull(tipoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de archivo"));
        } else {
            param.setIdTipoArchivo(tipoArchivo.getId());
        }
        
        TipoDocumentoProceso tipoDocumentoProceso = facades.getTipoDocumentoProcesoFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento(),
                        param.getIdTipoArchivo(), param.getCodigoTipoArchivo());
        
        if(isNull(tipoDocumentoProceso)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de documento"));
        } else {
            param.setIdTipoDocumento(tipoDocumentoProceso.getId());
        }
        
        EstadoPojo estado = facades.getEstadoFacade()
                .find(param.getIdEstado(), param.getCodigoEstado(), "PROCESAMIENTO_ARCHIVO");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        if(!isNull(param.getIdLocalOrigen())) {
            Local local = facades.getLocalFacade().find(param.getIdLocalOrigen());
            
            if(isNull(local)) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local origen"));
            }
        }
        
        if(!isNull(param.getIdLocalDestino())) {
            Local local = facades.getLocalFacade().find(param.getIdLocalDestino());
            
            if(isNull(local)) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local destino"));
            }
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToResend(ProcesamientoArchivoParam param) {
        
        if(!param.isValidToResend()) {
            return Boolean.FALSE;
        }
        
        ProcesamientoArchivo procesamientoArchivo = find(param.getId());
        
        if(procesamientoArchivo == null) {
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el archivo de procesamiento de archivo"));
            
        } else if (procesamientoArchivo.getReenviado().equals('S')) {
            
            param.addError(MensajePojo.createInstance()
                    .descripcion("El archivo ya fue reenviado"));
            
        } else {
            
            File file = procesamientoArchivo.getRutaArchivoErroneo() == null
                    ? null
                    : new File(procesamientoArchivo.getRutaArchivoErroneo());
            
            if(file == null || !file.exists()) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra el archivo erroneo para el reenvío"));
            }
        }
        
        
        return !param.tieneErrores();
    }
    
    @Override
    public ProcesamientoArchivo create(ProcesamientoArchivoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        ProcesamientoArchivo item = new ProcesamientoArchivo();
        item.setTamano(param.getTamano());
        item.setNombreArchivo(param.getNombreArchivo());
        item.setHash(param.getHash().trim());
        item.setIdTipoArchivo(param.getIdTipoArchivo());
        item.setIdEstado(param.getIdEstado());
        item.setConfirmado(param.getConfirmado());
        item.setFechaInsercion(Calendar.getInstance().getTime());
        item.setRutaArchivoErroneo(param.getRutaArchivoErroneo());
        item.setNroDocumento(param.getNroDocumento());
        item.setReenviado(param.getReenviado());
        item.setIdTipoDocumento(param.getIdTipoDocumento());
        item.setIdLocalOrigen(param.getIdLocalOrigen());
        item.setIdLocalDestino(param.getIdLocalDestino());
        item.setIdEmpresa(param.getIdEmpresa());
        create(item);
        
        if(!isNull(param.getProcesamientoArchivoDetalles())) {
            for (ProcesamientoArchivoDetalleParam procesamientoArchivoDetalle : param.getProcesamientoArchivoDetalles()) {
                procesamientoArchivoDetalle.setIdProcesamientoArchivo(item.getId());
                facades.getProcesamientoArchivoDetalleFacade().create(procesamientoArchivoDetalle, userInfo);
            }
        }
        
        return item;
    }

    @Override
    public ProcesamientoArchivo edit(ProcesamientoArchivoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ProcesamientoArchivo item = find(param.getId());
        item.setTamano(param.getTamano());
        item.setNombreArchivo(param.getNombreArchivo());
        item.setHash(param.getHash().trim());
        item.setIdTipoArchivo(param.getIdTipoArchivo());
        item.setIdEstado(param.getIdEstado());
        item.setConfirmado(param.getConfirmado());
        item.setRutaArchivoErroneo(param.getRutaArchivoErroneo());
        item.setNroDocumento(param.getNroDocumento());
        item.setReenviado(param.getReenviado());
        item.setIdTipoDocumento(param.getIdTipoDocumento());
        item.setIdLocalOrigen(param.getIdLocalOrigen());
        item.setIdLocalDestino(param.getIdLocalDestino());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public ProcesamientoArchivo reenviar(ProcesamientoArchivoParam param) {
        
        if(!validToResend(param)) {
            return null;
        }
        
        try {
            ProcesamientoArchivo item = find(param.getId());

            File file = new File(item.getRutaArchivoErroneo());

            if(item.getTipoArchivo().getCodigo().equals("EDI")) {

                ConfiguracionValorParam param1 = new ConfiguracionValorParam();
                param1.setCodigoConfiguracion("RUTA_ARCHIVOS_EDI_ENTRADA");
                ConfiguracionValor cv = facades.getConfiguracionValorFacade().findFirst(param1);

                String rutaArchivo = String.format("%s/%s", cv.getValor(), file.getName());
                Files.copy(file, rutaArchivo);
                file.delete();
            }

            item.setReenviado('S');
            edit(item);

            return item;
        } catch (Exception e) {
            param.addError(MensajePojo.createInstance().descripcion("Error al realizar el reenvío del documento"));
            return null;
        }
    }
    
    @Override
    public List<ProcesamientoArchivo> find(ProcesamientoArchivoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(ProcesamientoArchivo.class);
        Root<ProcesamientoArchivo> root = cq.from(ProcesamientoArchivo.class);
        Join<ProcesamientoArchivo, Estado> estado = root.join("estado");
        Join<ProcesamientoArchivo, TipoArchivo> tipoArchivo = root.join("tipoArchivo");
        Join<ProcesamientoArchivo, Empresa> empresa = root.join("empresa");
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(param.getConfirmado() != null) {
            predList.add(qb.equal(root.get("confirmado"), param.getConfirmado()));
        }
        
        if(param.getReenviado() != null) {
            predList.add(qb.equal(root.get("reenviado"), param.getReenviado()));
        }
        
        if(param.getIdTipoDocumento() != null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        if(param.getIdLocalOrigen() != null) {
            predList.add(qb.equal(root.get("idLocalOrigen"), param.getIdLocalOrigen()));
        }
        
        if(param.getIdLocalDestino() != null) {
            predList.add(qb.equal(root.get("idLocalDestino"), param.getIdLocalDestino()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if(param.getHash() != null && !param.getHash().trim().isEmpty()) {
            predList.add(qb.equal(root.get("hash"), param.getHash().trim()));
        }
        
        if (param.getNombreArchivo() != null && !param.getNombreArchivo().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombreArchivo")),
                    String.format("%%%s%%", param.getNombreArchivo().trim().toUpperCase())));
        }
        
        if (param.getNroDocumento() != null && !param.getNroDocumento().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroDocumento")),
                    String.format("%%%s%%", param.getNroDocumento().trim().toUpperCase())));
        }
        
        if(param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if(param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if(param.getIdTipoArchivo() != null) {
            predList.add(qb.equal(root.get("idTipoArchivo"), param.getIdTipoArchivo()));
        }
        
        if(param.getCodigoTipoArchivo() != null && !param.getCodigoTipoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(tipoArchivo.get("codigo"), param.getCodigoTipoArchivo().trim()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("fechaInsercion")),
                qb.desc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<ProcesamientoArchivo> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ProcesamientoArchivoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<ProcesamientoArchivo> root = cq.from(ProcesamientoArchivo.class);
        Join<ProcesamientoArchivo, Estado> estado = root.join("estado");
        Join<ProcesamientoArchivo, TipoArchivo> tipoArchivo = root.join("tipoArchivo");
        Join<ProcesamientoArchivo, Empresa> empresa = root.join("empresa");
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(param.getConfirmado() != null) {
            predList.add(qb.equal(root.get("confirmado"), param.getConfirmado()));
        }
        
        if(param.getReenviado() != null) {
            predList.add(qb.equal(root.get("reenviado"), param.getReenviado()));
        }
        
        if(param.getIdTipoDocumento() != null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        if(param.getIdLocalOrigen() != null) {
            predList.add(qb.equal(root.get("idLocalOrigen"), param.getIdLocalOrigen()));
        }
        
        if(param.getIdLocalDestino() != null) {
            predList.add(qb.equal(root.get("idLocalDestino"), param.getIdLocalDestino()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if(param.getHash() != null && !param.getHash().trim().isEmpty()) {
            predList.add(qb.equal(root.get("hash"), param.getHash().trim()));
        }
        
        if (param.getNombreArchivo() != null && !param.getNombreArchivo().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombreArchivo")),
                    String.format("%%%s%%", param.getNombreArchivo().trim().toUpperCase())));
        }
        
        if (param.getNroDocumento() != null && !param.getNroDocumento().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroDocumento")),
                    String.format("%%%s%%", param.getNroDocumento().trim().toUpperCase())));
        }
        
        if(param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if(param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if(param.getIdTipoArchivo() != null) {
            predList.add(qb.equal(root.get("idTipoArchivo"), param.getIdTipoArchivo()));
        }
        
        if(param.getCodigoTipoArchivo() != null && !param.getCodigoTipoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(tipoArchivo.get("codigo"), param.getCodigoTipoArchivo().trim()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
