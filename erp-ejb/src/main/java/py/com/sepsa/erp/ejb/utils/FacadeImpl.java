/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFacade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan Bernal
 * @param <T> Parámetro de tipo
 * @param <U> Parámetro de tipo
 */
public abstract class FacadeImpl<T, U extends CommonParam> extends AbstractFacade<T, U, UserInfoImpl> {

    protected final static String REFRESH_HINT = "org.hibernate.cacheMode";
    protected final static String REFRESH_HINT_VAL = "REFRESH";
    
    @EJB
    protected Facades facades;
    
    @PersistenceContext(unitName = "ERP-PU")
    private EntityManager em;

    public FacadeImpl(Class<T> entityClass) {
        super(entityClass);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public Map<String, String> getMapConditions(T item) {

        Map<String, String> atributos = new HashMap<>();

        return atributos;
    }

    public Map<String, String> getMapPk(T item) {
        Map<String, String> atributos = new HashMap<>();

        return atributos;
    }

    public String getTabla() {
        return "";
    }
}
