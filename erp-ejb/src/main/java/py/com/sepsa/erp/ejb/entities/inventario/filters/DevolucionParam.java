/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class DevolucionParam extends CommonParam {

    public DevolucionParam() {
    }

    public DevolucionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;
    
    /**
     * Lista de detalle de devolución
     */
    private List<DevolucionDetalleParam> devolucionDetalles;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        anulado = 'N';
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNullOrEmpty(devolucionDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de devolución"));
        } else {
            for (DevolucionDetalleParam item : devolucionDetalles) {
                item.setIdDevolucion(0);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNullOrEmpty(devolucionDetalles)) {
            for (DevolucionDetalleParam detalle : devolucionDetalles) {
                list.addAll(detalle.getErrores());
            }
        }
        
        return list;
    }
    
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de devolución"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public List<DevolucionDetalleParam> getDevolucionDetalles() {
        return devolucionDetalles;
    }

    public void setDevolucionDetalles(List<DevolucionDetalleParam> devolucionDetalles) {
        this.devolucionDetalles = devolucionDetalles;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getAnulado() {
        return anulado;
    }
}
