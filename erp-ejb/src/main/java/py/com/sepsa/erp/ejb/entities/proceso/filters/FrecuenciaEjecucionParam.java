/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de tipo etiqueta
 * @author Jonathan
 */
public class FrecuenciaEjecucionParam extends CommonParam {

    public FrecuenciaEjecucionParam() {
    }

    public FrecuenciaEjecucionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Frecuencia
     */
    @QueryParam("frecuencia")
    private Integer frecuencia;
    
    /**
     * Hora
     */
    @QueryParam("hora")
    private Integer hora;
    
    /**
     * Minuto
     */
    @QueryParam("minuto")
    private Integer minuto;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(frecuencia)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la frecuencia en segundos"));
        }
        
        if(isNull(hora)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la hora"));
        } else if ((hora < 0) || (23 < hora)) {
            addError(MensajePojo.createInstance()
                    .descripcion("La hora debe ser entre 0 y 23"));
        }
        
        if(isNull(minuto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el minuto"));
        } else if ((minuto < 0) || (60 < minuto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("El minuto debe ser entre 0 y 60"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de frecuencia de ejecución"));
        }
        
        if(isNull(frecuencia)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la frecuencia"));
        }
        
        if(isNull(hora)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la hora"));
        } else if (hora < 0 || 60 < hora) {
            addError(MensajePojo.createInstance()
                    .descripcion("La hora debe ser entre 0 y 23"));
        }
        
        if(isNull(minuto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el minuto"));
        } else if (minuto < 0 || 60 < minuto) {
            addError(MensajePojo.createInstance()
                    .descripcion("El minuto debe ser entre 0 y 60"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(Integer frecuencia) {
        this.frecuencia = frecuencia;
    }

    public Integer getHora() {
        return hora;
    }

    public void setHora(Integer hora) {
        this.hora = hora;
    }

    public Integer getMinuto() {
        return minuto;
    }

    public void setMinuto(Integer minuto) {
        this.minuto = minuto;
    }
}
