/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Cargo;
import py.com.sepsa.erp.ejb.entities.info.Contacto;
import py.com.sepsa.erp.ejb.entities.info.Email;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.TipoContacto;
import py.com.sepsa.erp.ejb.entities.info.filters.ContactoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaTelefonoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ContactoFacade", mappedName = "ContactoFacade")
@Local(ContactoFacade.class)
public class ContactoFacadeImpl extends FacadeImpl<Contacto, ContactoParam> implements ContactoFacade {

    public ContactoFacadeImpl() {
        super(Contacto.class);
    }

    @Override
    public Boolean validToCreate(ContactoParam param, boolean validarPersona) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Persona persona = param.getIdContacto() == null
                ? null
                : facades.getPersonaFacade().find(param.getIdContacto());
        
        if(param.getPersona() != null) {
            facades.getPersonaFacade().validToCreate(param.getPersona());
        } else if(validarPersona && persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona asociada al contacto"));
        }
        
        Cargo cargo = facades.getCargoFacade().find(param.getIdCargo());
        
        if(cargo == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cargo"));
        }
        
        TipoContacto tipoContacto = facades.getTipoContactoFacade().find(param.getIdTipoContacto());
        
        if(tipoContacto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de contacto"));
        }
        
        if(param.getEmail() != null) {
            facades.getEmailFacade().validToCreate(param.getEmail());
        }
        
        if(param.getTelefono() != null) {
            facades.getTelefonoFacade().validToCreate(param.getTelefono());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(ContactoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Contacto contacto = facades.getContactoFacade().find(param.getIdContacto());
        
        if(contacto == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el contacto"));
        }
        
        Cargo cargo = facades.getCargoFacade().find(param.getIdCargo());
        
        if(cargo == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el cargo"));
        }
        
        TipoContacto tipoContacto = facades.getTipoContactoFacade().find(param.getIdTipoContacto());
        
        if(tipoContacto == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el tipo de contacto"));
        }
        
        if(param.getEmail() != null) {
            facades.getEmailFacade().validToCreate(param.getEmail());
        }
        
        if(param.getTelefono() != null) {
            facades.getTelefonoFacade().validToCreate(param.getTelefono());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Contacto create(ContactoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        Integer idContacto;
        
        if(param.getPersona() != null) {
            Persona p = facades.getPersonaFacade().create(param.getPersona(), userInfo);
            idContacto = p.getId();
        } else {
            idContacto = param.getIdContacto();
        }
        
        Contacto item = new Contacto();
        item.setIdEmpresa(userInfo.getIdEmpresa());
        item.setIdContacto(idContacto);
        item.setIdCargo(param.getIdCargo());
        item.setIdTipoContacto(param.getIdTipoContacto());
        item.setActivo(param.getActivo());
        create(item);
        
        if(param.getEmail() != null) {
            
            Email email = facades.getEmailFacade().create(param.getEmail(), userInfo);
            
            if(email != null) {
                PersonaEmailParam param1 = new PersonaEmailParam();
                param1.setIdEmail(email.getId());
                param1.setIdContacto(item.getIdContacto());
                param1.setActivo('S');
                facades.getPersonaEmailFacade().create(param1, userInfo);
            }
        }
        
        if(param.getTelefono() != null) {
            
            Telefono telefono = facades.getTelefonoFacade().create(param.getTelefono(), userInfo);
            
            if(telefono != null) {
                PersonaTelefonoParam param1 = new PersonaTelefonoParam();
                param1.setIdTelefono(telefono.getId());
                param1.setIdContacto(item.getIdContacto());
                param1.setActivo('S');
                facades.getPersonaTelefonoFacade().create(param1, userInfo);
            }
        }
        
        return item;
    }

    @Override
    public Contacto edit(ContactoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Contacto item = find(param.getIdContacto());
        item.setIdCargo(param.getIdCargo());
        item.setIdTipoContacto(param.getIdTipoContacto());
        item.setActivo(param.getActivo());
        edit(item);
        
        return item;
    }
    
    @Override
    public List<Contacto> find(ContactoParam param) {
        
        String where = "true";
        
        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and c.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }
        
        if(param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select c.*"
                + " from info.contacto c"
                + " left join info.tipo_contacto tc on tc.id = c.id_tipo_contacto"
                + " left join info.cargo ca on ca.id = c.id_cargo"
                + " left join info.persona p on p.id = c.id_contacto"
                + " where %s order by c.activo offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Contacto.class);
        
        List<Contacto> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ContactoParam param) {
        
        String where = "true";
        
        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and c.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }
        
        if(param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(c.id_contacto)"
                + " from info.contacto c"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public Contacto findByPhonePrefix(ContactoParam param) {
        
        String where = "true";
        
        if(param.getPrefijoNumero() != null && !param.getPrefijoNumero().trim().isEmpty()) {
            where = String.format("%s and t.prefijo || t.numero = '%s'", where, param.getPrefijoNumero().trim());
        }
        
        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and c.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }
        
        if(param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select distinct c.*"
                + " from info.telefono t"
                + " join info.persona_telefono pt on pt.id_telefono = t.id"
                + " join info.contacto c on pt.id_contacto = c.id_contacto"
                + " where %s limit 1", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Contacto result = object == null ? null : (Contacto)object;
        
        return result;
    }
}
