/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.utils.FacturaUtils;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
@TransactionManagement(value=TransactionManagementType.BEAN)
@Log4j2
public class FacturaMasivaTask extends AbstracProcesamientoLote {

    @Resource
    private UserTransaction userTransaction;

    @Override
    public void procesar(Lote lote) {

        LoteArchivoParam laparam = new LoteArchivoParam();
        laparam.setIdLote(lote.getId());
        laparam.isValidToList();
        LoteArchivo archivo = facades.getLoteArchivoFacade().findFirst(laparam);
        
        FacturaParam param = new FacturaParam();
        param.setIdEmpresa(lote.getIdEmpresa());
        param.setIdUsuario(lote.getIdUsuario());
        param.setUploadedFileBytes(Base64.getDecoder().decode(archivo.getContenido()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<FacturaParam> facturas = FacturaUtils.convertToList(facades, param);
        String inicio = String.format("TIMBRADO;NRO_FACTURA;FECHA;ID_FACTURA;ESTADO_RESULTADO");
        StringBuilder buffer = new StringBuilder();
        buffer.append(inicio).append("\r\n");
        for (FacturaParam fact : facturas) {
            if (fact.tieneErrores()) {
                String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                error = error.concat("%s| ");
                for (MensajePojo mp : fact.getErrores()) {
                    error = String.format(error, mp.getDescripcion());
                    error = error.concat("%s| ");
                }
                error = error.substring(0, error.length() - 5);
                error = error.concat(";;");
                buffer.append(error).append("\r\n");
            } else {

                try {
                    userTransaction.begin();

                    Factura factura = facades
                            .getFacturaFacade()
                            .create(fact, null);

                    if (factura == null) {
                        String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                        error = error.concat("| ");
                        for (MensajePojo mp : fact.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {
                        String ok = String.format("%s;%s;%s;%s;OK", fact.getTimbrado(), fact.getNroFactura(), sdf.format(fact.getFecha()), factura.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }

                    userTransaction.commit();
                } catch (Exception exception) {
                    try {
                        userTransaction.rollback();
                    } catch (Exception e) {
                        log.fatal("Error", e);
                    }
                    String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                    error = error.concat("| ");
                    error = error.concat(exception.getMessage());
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                }
            }
        }

        String resultadoBase64 = Base64.getEncoder().encodeToString(buffer.toString().getBytes(StandardCharsets.UTF_8));

        LoteParam loteparam = new LoteParam();
        loteparam.setId(lote.getId());
        loteparam.setIdEmpresa(lote.getIdEmpresa());
        loteparam.setCodigoEstado("PROCESADO");
        loteparam.setNotificado(lote.getNotificado());
        loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
        loteparam.setResultado(resultadoBase64);

        facades.getLoteFacade().edit(loteparam, null);
    }
}
