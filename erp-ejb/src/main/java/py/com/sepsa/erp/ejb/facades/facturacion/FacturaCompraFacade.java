/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface FacturaCompraFacade extends Facade<FacturaCompra, FacturaCompraParam, UserInfoImpl> {

    public void actualizarSaldoFactura(Integer idFacturaCompra, BigDecimal saldo);

    public FacturaCompra anular(FacturaCompraParam param, UserInfoImpl userInfo);

    public List<FacturaPojo> findMonto(FacturaCompraParam param);

    public Long findMontoSize(FacturaCompraParam param);

    public byte[] getPdfReporteCompra(FacturaCompraParam param, UserInfoImpl userInfo);

    public FacturaCompra updateFileProcess(FacturaCompraParam param, UserInfoImpl userInfo);

    public Boolean delete(FacturaCompraParam param, UserInfoImpl userInfo);

    public Integer generarComprobanteCompraSize(ReporteComprobanteParam param);

    public List<RegistroComprobantePojo> generarComprobanteCompra(ReporteComprobanteParam param);
    
}