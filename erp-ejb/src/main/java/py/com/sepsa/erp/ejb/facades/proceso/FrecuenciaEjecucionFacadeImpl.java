/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.FrecuenciaEjecucion;
import py.com.sepsa.erp.ejb.entities.proceso.filters.FrecuenciaEjecucionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.facades.AbstractFacade;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "FrecuenciaEjecucionFacade", mappedName = "FrecuenciaEjecucionFacade")
@Local(FrecuenciaEjecucionFacade.class)
public class FrecuenciaEjecucionFacadeImpl extends FacadeImpl<FrecuenciaEjecucion, FrecuenciaEjecucionParam> implements FrecuenciaEjecucionFacade {

    public FrecuenciaEjecucionFacadeImpl() {
        super(FrecuenciaEjecucion.class);
    }
    
    @Override
    public Boolean validToCreate(FrecuenciaEjecucionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(FrecuenciaEjecucionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        FrecuenciaEjecucion fe = find(param.getId());
        
        if(isNull(fe)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la entidad"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public FrecuenciaEjecucion create(FrecuenciaEjecucionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        FrecuenciaEjecucion item = new FrecuenciaEjecucion();
        item.setFrecuencia(param.getFrecuencia());
        item.setHora(param.getHora());
        item.setMinuto(param.getMinuto());
        
        create(item);
        
        return item;
    }

    @Override
    public FrecuenciaEjecucion edit(FrecuenciaEjecucionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        FrecuenciaEjecucion item = find(param.getId());
        item.setFrecuencia(param.getFrecuencia());
        item.setHora(param.getHora());
        item.setMinuto(param.getMinuto());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<FrecuenciaEjecucion> find(FrecuenciaEjecucionParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(FrecuenciaEjecucion.class);
        Root<FrecuenciaEjecucion> root = cq.from(FrecuenciaEjecucion.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }
        
        cq.orderBy(qb.asc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<FrecuenciaEjecucion> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(FrecuenciaEjecucionParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<FrecuenciaEjecucion> root = cq.from(FrecuenciaEjecucion.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
