/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Motivo;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventario;
import py.com.sepsa.erp.ejb.entities.inventario.TipoOperacion;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.OperacionInventarioPojo;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "OperacionInventarioFacade", mappedName = "OperacionInventarioFacade")
@javax.ejb.Local(OperacionInventarioFacade.class)
public class OperacionInventarioFacadeImp extends FacadeImpl<OperacionInventario, OperacionInventarioParam> implements OperacionInventarioFacade {

    public OperacionInventarioFacadeImp() {
        super(OperacionInventario.class);
    }
    
    public Boolean validToCreate(OperacionInventarioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoOperacion tipoOperacion = facades.getTipoOperacionFacade().find(param.getIdTipoOperacion(), param.getCodigoTipoOperacion());
        if(tipoOperacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el tipo de operación"));
        } else {
            param.setIdTipoOperacion(tipoOperacion.getId());
            param.setCodigoTipoOperacion(tipoOperacion.getCodigo());
        }
        
        MotivoPojo motivo = facades.getMotivoFacade().find(param.getIdMotivo(), param.getCodigoMotivo(), param.getCodigoTipoMotivo());
        if(motivo == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de operación"));
        } else {
            param.setIdMotivo(motivo.getId());
            param.setCodigoMotivo(motivo.getCodigo());
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "OPERACION_INVENTARIO");
        if(estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(param.getCodigoTipoOperacion() != null) {
            if(param.getCodigoTipoOperacion().equals("AJUSTE_CONTROL_INVENTARIO")) {
                if(param.getIdControlInventario() == null && param.getCodigoControlInventario() == null) {
                    param.addError(MensajePojo.createInstance().descripcion("Se debe indicar el control de inventario"));
                }
            }
        }
        
        if(param.getIdOrdenCompra() != null) {
            OrdenCompraParam ocparam = new OrdenCompraParam();
            ocparam.setId(param.getIdOrdenCompra());
            Long ocsize = facades.getOrdenCompraFacade().findSize(ocparam);
            if(ocsize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe la orden de compra"));
            }
        }
        
        if(param.getIdControlInventario() != null) {
            ControlInventarioParam ciparam = new ControlInventarioParam();
            ciparam.setId(param.getIdControlInventario());
            ciparam.setCodigo(param.getCodigoControlInventario());
            ciparam.setIdEmpresa(param.getIdEmpresa());
            Long cisize = facades.getControlInventarioFacade().findSize(ciparam);
            if(cisize <= 0) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el control de inventario"));
            }
        }
        
        if(!isNullOrEmpty(param.getOperacionInventarioDetalles())) {
            for (OperacionInventarioDetalleParam detalle : param.getOperacionInventarioDetalles()) {
                detalle.setCodigoTipoOperacion(param.getCodigoTipoOperacion());
                facades.getOperacionInventarioDetalleFacade().validToCreate(detalle, false);
            }
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToChangeState(OperacionInventarioParam param) {
        
        if(!param.isValidToChangeState()) {
            return Boolean.FALSE;
        }
        
        OperacionInventarioParam param1 = new OperacionInventarioParam();
        param1.setId(param.getId());
        OperacionInventarioPojo operacionInventario = findFirstPojo(param1);
        if(operacionInventario == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la operación de inventario"));
        } else {
            param.setIdTipoOperacion(operacionInventario.getIdTipoOperacion());
            param.setCodigoTipoOperacion(operacionInventario.getCodigoTipoOperacion());
            param.setIdMotivo(operacionInventario.getIdMotivo());
            param.setCodigoMotivo(operacionInventario.getCodigoMotivo());
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "OPERACION_INVENTARIO");
        if(estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        if(operacionInventario != null && estado != null) {
            
            if(operacionInventario.getCodigoEstado().equals("CONFIRMADO")
                    && !estado.getCodigo().equals("CONFIRMADO")) {
                param.addError(MensajePojo.createInstance().descripcion("No se puede cambiar de estado una operación confirmada"));
            }
            
            if(param.getChangeState() == null) {
                param.setChangeState(!operacionInventario.getIdEstado().equals(estado.getId()));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public OperacionInventario create(OperacionInventarioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        OperacionInventario result = new OperacionInventario();
        result.setIdEmpresa(param.getIdEmpresa()); 
        result.setIdUsuario(userInfo.getId()); 
        result.setIdTipoOperacion(param.getIdTipoOperacion());
        result.setIdMotivo(param.getIdMotivo());
        result.setIdEstado(param.getIdEstado());
        result.setIdOrdenCompra(param.getIdOrdenCompra());
        result.setIdControlInventario(param.getIdControlInventario());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        
        create(result);
        
        if(!isNullOrEmpty(param.getOperacionInventarioDetalles())) {
            for (OperacionInventarioDetalleParam detalle : param.getOperacionInventarioDetalles()) {
                detalle.setIdOperacionInventario(result.getId());
                detalle.setCodigoEstadoOperacion(param.getCodigoEstado());
                detalle.setCodigoTipoOperacion(param.getCodigoTipoOperacion());
                facades.getOperacionInventarioDetalleFacade().create(detalle, userInfo);
            }
        }
        
        return result;
    }

    @Override
    public OperacionInventario changeState(OperacionInventarioParam param, UserInfoImpl userInfo) {
        
        if(!validToChangeState(param)) {
            return null;
        }
        
        OperacionInventario result = find(param.getId());
        result.setIdEstado(param.getIdEstado());
        edit(result);
        
        return result;
    }
    
    @Override
    public OperacionInventario create(UserInfoImpl userInfo, String codigoTipoOperacion, String codigoMotivo, String codigoEstado, Integer idOrdenCompra, Integer idEmpresa) {
        OperacionInventarioParam param = new OperacionInventarioParam();
        param.setCodigoTipoOperacion(codigoTipoOperacion);
        param.setCodigoMotivo(codigoMotivo);
        param.setCodigoEstado(codigoEstado);
        param.setIdOrdenCompra(idOrdenCompra);
        param.setIdEmpresa(idEmpresa);
        param.setCodigoTipoMotivo("OPERACION_INVENTARIO");
        return create(param, userInfo);
    }
    
    @Override
    public List<OperacionInventarioPojo> findPojo(OperacionInventarioParam param) {

        AbstractFind find = new AbstractFind(OperacionInventarioPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idUsuario"),
                        getPath("usuario").get("usuario"),
                        getPath("root").get("idTipoOperacion"),
                        getPath("tipoOperacion").get("descripcion"),
                        getPath("tipoOperacion").get("codigo"),
                        getPath("root").get("idMotivo"),
                        getPath("motivo").get("descripcion"),
                        getPath("motivo").get("codigo"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("idOrdenCompra"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<OperacionInventario> find(OperacionInventarioParam param) {

        AbstractFind find = new AbstractFind(OperacionInventario.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, OperacionInventarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<OperacionInventario> root = cq.from(OperacionInventario.class);
        Join<OperacionInventario, Empresa> empresa = root.join("empresa");
        Join<OperacionInventario, TipoOperacion> tipoOperacion = root.join("tipoOperacion");
        Join<OperacionInventario, Motivo> motivo = root.join("motivo");
        Join<OperacionInventario, Estado> estado = root.join("estado");
        Join<OperacionInventario, Usuario> usuario = root.join("usuario", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("empresa", empresa);
        find.addPath("tipoOperacion", tipoOperacion);
        find.addPath("usuario", usuario);
        find.addPath("estado", estado);
        find.addPath("motivo", motivo);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdTipoOperacion() != null) {
            predList.add(qb.equal(root.get("idTipoOperacion"), param.getIdTipoOperacion()));
        }
        
        if (param.getIdMotivo() != null) {
            predList.add(qb.equal(root.get("idMotivo"), param.getIdMotivo()));
        }
        
        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(OperacionInventarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<OperacionInventario> root = cq.from(OperacionInventario.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdTipoOperacion() != null) {
            predList.add(qb.equal(root.get("idTipoOperacion"), param.getIdTipoOperacion()));
        }
        
        if (param.getIdMotivo() != null) {
            predList.add(qb.equal(root.get("idMotivo"), param.getIdMotivo()));
        }
        
        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(root.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
