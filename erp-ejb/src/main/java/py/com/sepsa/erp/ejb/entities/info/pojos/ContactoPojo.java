/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.util.List;

/**
 * POJO para la entidad contacto
 * @author Jonathan
 */
public class ContactoPojo {
    
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    
    /**
     * Contacto
     */
    private String contacto;
    
    /**
     * Identificador de cargo
     */
    private Integer idCargo;
    
    /**
     * Cargo
     */
    private String cargo;
    
    /**
     * Identificador de tipo de contacto
     */
    private Integer idTipoContacto;
    
    /**
     * Tipo contacto
     */
    private String tipoContacto;
    
    /**
     * Estado
     */
    private Character estado;
    
    /**
     * Emails
     */
    private List<PersonaEmailPojo> emails;
    
    /**
     * Telefonos
     */
    private List<PersonaTelefonoPojo> telefonos;

    /*public void updateData(Facades facades) {
        
        PersonaEmailParam ceparam = new PersonaEmailParam();
        ceparam.setIdPersona(idContacto);
        ceparam.setFirstResult(0);
        ceparam.setPageSize(100);
        emails = facades.getContactoEmailFacade().find(ceparam);
        
        PersonaTelefonoParam ctparam = new PersonaTelefonoParam();
        ctparam.setIdPersona(idContacto);
        ctparam.setFirstResult(0);
        ctparam.setPageSize(100);
        telefonos = facades.getContactoTelefonoFacade().find(ctparam);
    }*/
    
    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getContacto() {
        return contacto;
    }

    public List<PersonaEmailPojo> getEmails() {
        return emails;
    }

    public void setEmails(List<PersonaEmailPojo> emails) {
        this.emails = emails;
    }

    public List<PersonaTelefonoPojo> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<PersonaTelefonoPojo> telefonos) {
        this.telefonos = telefonos;
    }
}
