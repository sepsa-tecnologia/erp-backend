/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.TipoReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "TipoReferenciaGeograficaFacade", mappedName = "TipoReferenciaGeograficaFacade")
@Local(TipoReferenciaGeograficaFacade.class)
public class TipoReferenciaGeograficaFacadeImpl extends FacadeImpl<TipoReferenciaGeografica, TipoReferenciaGeograficaParam> implements TipoReferenciaGeograficaFacade {

    public TipoReferenciaGeograficaFacadeImpl() {
        super(TipoReferenciaGeografica.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(TipoReferenciaGeograficaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoReferenciaGeografica trg = find(null, param.getCodigo().trim());
        
        if(!isNull(trg)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(TipoReferenciaGeograficaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoReferenciaGeografica trg = find(param.getId());
        
        if(isNull(trg)) {
            
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el tipo de referencia geográfica"));
            
        } else if(!Objects.equals(trg.getCodigo(), param.getCodigo().trim())) {
            
            TipoReferenciaGeografica trg1 = find(null, param.getCodigo().trim());

            if(!isNull(trg1)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public TipoReferenciaGeografica create(TipoReferenciaGeograficaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoReferenciaGeografica item = new TipoReferenciaGeografica();
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        create(item);
        
        return item;
    }
    
    @Override
    public TipoReferenciaGeografica edit(TipoReferenciaGeograficaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoReferenciaGeografica item = find(param.getId());
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        edit(item);
        
        return item;
    }

    @Override
    public TipoReferenciaGeografica find(Integer id, String codigo) {
        TipoReferenciaGeograficaParam param = new TipoReferenciaGeograficaParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoReferenciaGeografica> find(TipoReferenciaGeograficaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoReferenciaGeografica.class);
        Root<TipoReferenciaGeografica> root = cq.from(TipoReferenciaGeografica.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<TipoReferenciaGeografica> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(TipoReferenciaGeograficaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoReferenciaGeografica> root = cq.from(TipoReferenciaGeografica.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
