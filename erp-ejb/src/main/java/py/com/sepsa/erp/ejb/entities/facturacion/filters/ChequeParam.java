/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de cheque
 * @author Jonathan D. Bernal Fernández
 */
public class ChequeParam extends CommonParam {
    
    public ChequeParam() {
    }

    public ChequeParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de entidad financiera
     */
    @QueryParam("idEntidadFinanciera")
    private Integer idEntidadFinanciera;
    
    /**
     * Identificador de deposito
     */
    @QueryParam("idDeposito")
    private Integer idDeposito;
    
    /**
     * Nro cheque
     */
    @QueryParam("nroCheque")
    private String nroCheque;
    
    /**
     * Fecha de emisión
     */
    @QueryParam("fechaEmision")
    private Date fechaEmision;
    
    /**
     * Fecha de pago
     */
    @QueryParam("fechaPago")
    private Date fechaPago;
    
    /**
     * Monto cheque
     */
    @QueryParam("montoCheque")
    private BigDecimal montoCheque;
    
    @QueryParam("omitirValidacionCheque")
    private String omitirValidacionCheque;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if (isNull(omitirValidacionCheque) || omitirValidacionCheque.equalsIgnoreCase("N")) {

            if (isNull(fechaEmision)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar la fecha de emisión del cheque"));
            }

            if (isNull(montoCheque)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el monto del cheque"));
            }

            if (isNull(idEntidadFinanciera)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el identificador de entidad financiera"));
            }

            if (isNullOrEmpty(nroCheque)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el nro de cheque"));
            }
        }
        return !tieneErrores();
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdDeposito() {
        return idDeposito;
    }

    public void setIdDeposito(Integer idDeposito) {
        this.idDeposito = idDeposito;
    }

    public String getNroCheque() {
        return nroCheque;
    }

    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public BigDecimal getMontoCheque() {
        return montoCheque;
    }

    public void setMontoCheque(BigDecimal montoCheque) {
        this.montoCheque = montoCheque;
    }

    public String getOmitirValidacionCheque() {
        return omitirValidacionCheque;
    }

    public void setOmitirValidacionCheque(String omitirValidacionCheque) {
        this.omitirValidacionCheque = omitirValidacionCheque;
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
