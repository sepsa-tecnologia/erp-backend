package py.com.sepsa.erp.ejb.externalservice.adapters;

import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStreamResponse;
import static py.com.sepsa.erp.ejb.externalservice.http.ResponseCode.OK;
import py.com.sepsa.erp.ejb.externalservice.service.APISiedi;

/**
 * Adaptador para obtener el kude asociado a un dte
 *
 * @author Jonathan D. Bernal Fernandez
 */
public class KudeDteAdapter extends AbstractAdapterImpl<byte[],
        HttpStreamResponse> {
    
    /**
     * Host
     */
    private String host;
    
    /**
     * CDC
     */
    private String cdc;
    
    /**
     * Nombre del archivo
     */
    private String nombreArchivo;
    
    /**
     * Datos adicionales
     */
    private JsonObject datos;
    
    /**
     * Bandera para ticket 
     */
    private Boolean ticket = Boolean.FALSE;
    
    /**
     * Bandera para siediApi 
     */
    private Boolean siediApi = Boolean.FALSE;
    
    /**
     * Token SiediApi
     */
    private String tokenSiedi;

    /**
     * Realiza una petición al servicio
     *
     */
    @Override
    public void request() {
        APISiedi api = null;
        if (siediApi) {
              api = new APISiedi(host,siediApi);
        } else {
              api = new APISiedi(host);
        }

        Map param = new HashMap();
        param.put("ticket", this.getTicket().toString());
        if (siediApi) {
             this.resp = api.obtenerDteKudeSiediApi(tokenSiedi,cdc, nombreArchivo, datos, param);
        } else {
             this.resp = api.obtenerDteKude(cdc, nombreArchivo, datos, param);
        }
            
        switch(resp.getRespCode()) {
            case OK:
                setPayload(resp.getPayload());
                break;

            default:
                byte[] bytes = {};
                setPayload(bytes);
        }
    }

    public static byte[] obtener(String host, String cdc, String nombreArchivo) {
        KudeDteAdapter adapter = new KudeDteAdapter(host, cdc, nombreArchivo);
        adapter.request();
        return adapter.getPayload();
    }

    
    public static byte[] obtener(String host, String cdc, String nombreArchivo, Boolean ticket) {
        KudeDteAdapter adapter = new KudeDteAdapter(host, cdc, nombreArchivo);
        adapter.setTicket(ticket);
        adapter.request();
        return adapter.getPayload();
    }
    
    public static byte[] obtener(String tokenSiedi,String host, String cdc, String nombreArchivo, JsonObject datos, Boolean ticket, Boolean siediApi) {
        KudeDteAdapter adapter = new KudeDteAdapter(host, cdc, nombreArchivo, datos);
        adapter.setTicket(ticket);
        adapter.setSiediApi(siediApi);
        adapter.setTokenSiedi(tokenSiedi);
        adapter.request();
        return adapter.getPayload();
    }

    public KudeDteAdapter(String host, String cdc, String nombreArchivo, JsonObject datos) {
        super(new HttpStreamResponse());
        this.host = host;
        this.cdc = cdc;
        this.nombreArchivo = nombreArchivo;
        this.datos = datos;
    }

    public KudeDteAdapter(String host, String cdc, String nombreArchivo) {
        super(new HttpStreamResponse());
        this.host = host;
        this.cdc = cdc;
        this.nombreArchivo = nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public void setDatos(JsonObject datos) {
        this.datos = datos;
    }

    public JsonObject getDatos() {
        return datos;
    }

    public Boolean getTicket() {
        return ticket;
    }

    public void setTicket(Boolean ticket) {
        this.ticket = ticket;
    }

    public Boolean getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(Boolean siediApi) {
        this.siediApi = siediApi;
    }

    public String getTokenSiedi() {
        return tokenSiedi;
    }

    public void setTokenSiedi(String tokenSiedi) {
        this.tokenSiedi = tokenSiedi;
    }
    
    
    
}
