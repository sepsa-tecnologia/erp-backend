/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Clase para el manejo de los parámetros de aprobacion contrato
 * @author Jonathan D. Bernal Fernández
 */
public class AprobacionContratoParam extends CommonParam {

    public AprobacionContratoParam() {
    }

    public AprobacionContratoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de aprobacion
     */
    @QueryParam("idAprobacion")
    private Integer idAprobacion;
    
    /**
     * Identificador de contrato
     */
    @QueryParam("idContrato")
    private Integer idContrato;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdAprobacion() {
        return idAprobacion;
    }

    public void setIdAprobacion(Integer idAprobacion) {
        this.idAprobacion = idAprobacion;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }
}
