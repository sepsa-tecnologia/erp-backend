/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.Perfil;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioPerfil;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioPerfilParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPerfilPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Sergio D. Riveros Vazquez
 */
@Stateless(name = "UsuarioPerfilFacade", mappedName = "UsuarioPerfilFacade")
@Local(UsuarioPerfilFacade.class)
public class UsuarioPerfilFacadeImpl extends FacadeImpl<UsuarioPerfil, UsuarioPerfilParam> implements UsuarioPerfilFacade {

    public UsuarioPerfilFacadeImpl() {
        super(UsuarioPerfil.class);
    }

    @Override
    public Boolean validToCreate(UsuarioPerfilParam param, boolean validarIdUsuario) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        UsuarioPerfilParam param1 = new UsuarioPerfilParam();
        param1.setIdPerfil(param.getIdPerfil());
        param1.setIdUsuario(param.getIdUsuario());
        
        UsuarioPerfil item = findFirst(param1);
        
        if(item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }
        
        Perfil perfil = facades.getPerfilFacade().find(param.getIdPerfil());
        
        if(perfil == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el perfil"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(validarIdUsuario && usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "USUARIO_PERFIL");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(UsuarioPerfilParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        UsuarioPerfilParam param1 = new UsuarioPerfilParam();
        param1.setIdPerfil(param.getIdPerfil());
        param1.setIdUsuario(param.getIdUsuario());
        
        UsuarioPerfil item = findFirst(param1);
        
        if(item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un registro"));
        } else {
            param.setId(item.getId());
        }
        
        Perfil perfil = facades.getPerfilFacade().find(param.getIdPerfil());
        
        if(perfil == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el perfil"));
        }
        
        Usuario usuario = facades.getUsuarioFacade().find(param.getIdUsuario());
        
        if(usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el usuario"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "USUARIO_PERFIL");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public UsuarioPerfil create(UsuarioPerfilParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        UsuarioPerfil item = new UsuarioPerfil();
        item.setIdPerfil(param.getIdPerfil());
        item.setIdUsuario(param.getIdUsuario());
        item.setIdEstado(param.getIdEstado());
        
        create(item);
        
        return item;
    }
    
    @Override
    public UsuarioPerfil edit(UsuarioPerfilParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        UsuarioPerfil item = find(param.getId());
        item.setIdPerfil(param.getIdPerfil());
        item.setIdUsuario(param.getIdUsuario());
        item.setIdEstado(param.getIdEstado());
        
        edit(item);
        
        return item;
    }
    
    public Boolean validToFindPermisos(UsuarioPerfilParam param){
    
        if(!param.isValidToGetById()){
            return Boolean.FALSE;
        }
        
        Long size = findSize(param);
        if(size == 0){
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se puede encontrar el Usuario-Perfil"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public JsonElement findPermisosById(UsuarioPerfilParam param){
        if(!validToFindPermisos(param)){
            return null;
        }
        return findFirst(param).getPerfil().getPermisos();
    }
    
    @Override
    public List<UsuarioPerfilPojo> findPojo(UsuarioPerfilParam param) {

        String where = "true";
        
        if(param.getUsuario() != null && !param.getUsuario().trim().isEmpty()) {
            where = String.format("%s and u.usuario = '%s'", where, param.getUsuario().trim());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and up.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdPerfil() != null) {
            where = String.format("%s and up.id_perfil = %d", where, param.getIdPerfil());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and up.id_estado = %d", where, param.getIdEstado());
        }
        
        String sql = String.format("select up.id, up.id_usuario, u.usuario,"
                + " up.id_perfil, p.codigo as codigo_perfil, p.descripcion as perfil,"
                + " up.id_estado, e.descripcion as estado, e.codigo as codigo_estado,"
                + " p.id_empresa"
                + " from usuario.usuario_perfil up"
                + " join info.estado e on e.id = up.id_estado"
                + " join usuario.usuario u on u.id = up.id_usuario"
                + " join usuario.perfil p on p.id = up.id_perfil"
                + " where %s order by up.id_usuario, up.id_perfil offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.setHint(REFRESH_HINT, REFRESH_HINT_VAL).getResultList();

        List<UsuarioPerfilPojo> result = new ArrayList<>();
        for (Object[] objects : list) {
            int i = 0;
            UsuarioPerfilPojo item = new UsuarioPerfilPojo();
            item.setId((Integer)objects[i++]);
            item.setIdUsuario((Integer)objects[i++]);
            item.setUsuario((String)objects[i++]);
            item.setIdPerfil((Integer)objects[i++]);
            item.setCodigoPerfil((String)objects[i++]);
            item.setPerfil((String)objects[i++]);
            item.setIdEstado((Integer)objects[i++]);
            item.setEstado((String)objects[i++]);
            item.setCodigoEstado((String)objects[i++]);
            item.setIdEmpresa((Integer)objects[i++]);
            result.add(item);
        }
        return result;
    }
    
    @Override
    public List<UsuarioPerfil> find(UsuarioPerfilParam param) {

        String where = "true";
        
        if(param.getId() != null){
            where = String.format("%s and up.id = %d", where, param.getId());
        }
        
        if(param.getUsuario() != null && !param.getUsuario().trim().isEmpty()) {
            where = String.format("%s and u.usuario = '%s'", where, param.getUsuario().trim());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and up.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdPerfil() != null) {
            where = String.format("%s and up.id_perfil = %d", where, param.getIdPerfil());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and up.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEstado());
        }
        
        String sql = String.format("select up.*"
                + " from usuario.usuario_perfil up"
                + " left join usuario.usuario u on u.id = up.id_usuario"
                + " left join usuario.perfil p on p.id = up.id_perfil"
                + " left join info.estado e on e.id = up.id_estado"
                + " where %s order by up.id_usuario, up.id_perfil offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, UsuarioPerfil.class);
        
        List<UsuarioPerfil> result = q.setHint(REFRESH_HINT, REFRESH_HINT_VAL).getResultList();

        return result;
    }
    
    @Override
    public Long findSize(UsuarioPerfilParam param) {

        String where = "true";
        
        if(param.getId() != null){
            where = String.format("%s and up.id = %d", where, param.getId());
        }
        
        if(param.getUsuario() != null && !param.getUsuario().trim().isEmpty()) {
            where = String.format("%s and u.usuario = '%s'", where, param.getUsuario().trim());
        }
        
        if(param.getIdUsuario() != null) {
            where = String.format("%s and up.id_usuario = %d", where, param.getIdUsuario());
        }
        
        if(param.getIdPerfil() != null) {
            where = String.format("%s and up.id_perfil = %d", where, param.getIdPerfil());
        }
        
        if(param.getIdEstado() != null) {
            where = String.format("%s and up.id_estado = %d", where, param.getIdEstado());
        }
        
        if(param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEstado());
        }
        
        String sql = String.format("select count(up.*)"
                + " from usuario.usuario_perfil up"
                + " left join usuario.usuario u on u.id = up.id_usuario"
                + " left join usuario.perfil p on p.id = up.id_perfil"
                + " left join info.estado e on e.id = up.id_estado"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<UsuarioPerfil> findRelacionados(UsuarioPerfilParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select coalesce(up.id, 0) as id, coalesce(up.id_usuario, %d) as id_usuario, p.id as id_perfil, up.id_estado\n"
                + "from usuario.perfil p\n"
                + "left join usuario.usuario_perfil up on up.id_perfil = p.id and up.id_usuario = %d\n"
                + "left join info.estado e on e.id = up.id_estado and e.codigo = 'ACTIVO'\n"
                + "left join usuario.usuario u on u.id = up.id_usuario\n"
                + "where %s limit %d offset %d", param.getIdUsuario(), param.getIdUsuario(), where, param.getPageSize(), param.getFirstResult());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.setHint(REFRESH_HINT, REFRESH_HINT_VAL).getResultList();
        
        List<UsuarioPerfil> result = Lists.empty();
        
        for (Object[] objects : list) {
            int i = 0;
            UsuarioPerfil ul = new UsuarioPerfil();
            ul.setId((Integer)objects[i++]);
            ul.setIdUsuario((Integer)objects[i++]);
            ul.setIdPerfil((Integer)objects[i++]);
            ul.setIdEstado((Integer)objects[i++]);
            ul.setEstado(ul.getIdEstado() == null ? null : facades.getEstadoFacade().find(ul.getIdEstado()));
            ul.setPerfil(facades.getPerfilFacade().find(ul.getIdPerfil()));
            ul.setUsuario(facades.getUsuarioFacade().find(ul.getIdUsuario()));
            result.add(ul);
        }
        
        return result;
    }

    @Override
    public Long findRelacionadosSize(UsuarioPerfilParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select count(p)\n"
                + "from usuario.perfil p\n"
                + "left join usuario.usuario_perfil up on up.id_perfil = p.id and up.id_usuario = %d\n"
                + "left join info.estado e on e.id = up.id_estado and e.codigo = 'ACTIVO'\n"
                + "left join usuario.usuario u on u.id = up.id_usuario\n"
                + "where %s", param.getIdUsuario(), where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
