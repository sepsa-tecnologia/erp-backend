/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de trafico detalle
 *
 * @author Jonathan D. Bernal Fernández
 */
public class LiquidacionServicioParam extends CommonParam {

    public LiquidacionServicioParam() {
    }

    public LiquidacionServicioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;

    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;

    /**
     * Bloqueable
     */
    @QueryParam("bloqueable")
    private Character bloqueable;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }

        if (isNull(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

        if (isNull(bloqueable)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar si es bloqueable"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        limpiarErrores();

        if (isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }

        if (isNull(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

        if (isNull(bloqueable)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debde indicar si es bloqueable"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        return !tieneErrores();
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public void setBloqueable(Character bloqueable) {
        this.bloqueable = bloqueable;
    }

    public Character getBloqueable() {
        return bloqueable;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }
}
