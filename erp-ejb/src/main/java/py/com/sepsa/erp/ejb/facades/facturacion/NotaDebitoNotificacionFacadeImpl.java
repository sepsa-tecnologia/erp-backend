/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebitoNotificacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaDebitoNotificacionPojo;
import py.com.sepsa.erp.ejb.entities.info.TipoNotificacion;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "NotaDebitoNotificacionFacade", mappedName = "NotaDebitoNotificacionFacade")
@Local(NotaDebitoNotificacionFacade.class)
public class NotaDebitoNotificacionFacadeImpl extends FacadeImpl<NotaDebitoNotificacion, NotaDebitoNotificacionParam> implements NotaDebitoNotificacionFacade {

    public NotaDebitoNotificacionFacadeImpl() {
        super(NotaDebitoNotificacion.class);
    }
    
    @Override
    public Boolean validToCreate(NotaDebitoNotificacionParam param, boolean validarIdNotaDebito) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        NotaDebitoParam param1 = new NotaDebitoParam();
        param1.setId(param.getIdNotaDebito());
        Long size = facades.getNotaDebitoFacade().findSize(param1);
        
        if(validarIdNotaDebito && size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la Nota de crédito"));
        }
            
        TipoNotificacion tipoNotificacion = facades.getTipoNotificacionFacade()
                .find(param.getIdTipoNotificacion(), param.getCodigoTipoNotificacion());

        if(tipoNotificacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de notificación"));
        } else {
            param.setIdTipoNotificacion(tipoNotificacion.getId());
            param.setCodigoTipoNotificacion(tipoNotificacion.getCodigo());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public NotaDebitoNotificacion create(NotaDebitoNotificacionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        NotaDebitoNotificacion item = new NotaDebitoNotificacion();
        item.setIdNotaDebito(param.getIdNotaDebito());
        item.setIdTipoNotificacion(param.getIdTipoNotificacion());
        item.setEmail(param.getEmail());
        
        create(item);
        
        return item;
    }
    
    @Override
    public List<NotaDebitoNotificacionPojo> findPojo(NotaDebitoNotificacionParam param) {

        AbstractFind find = new AbstractFind(NotaDebitoNotificacionPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idNotaDebito"),
                        getPath("root").get("idTipoNotificacion"),
                        getPath("tipoNotificacion").get("descripcion"),
                        getPath("tipoNotificacion").get("codigo"),
                        getPath("root").get("email"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<NotaDebitoNotificacion> find(NotaDebitoNotificacionParam param) {

        AbstractFind find = new AbstractFind(NotaDebitoNotificacion.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, NotaDebitoNotificacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<NotaDebitoNotificacion> root = cq.from(NotaDebitoNotificacion.class);
        Join<NotaDebitoNotificacion, TipoNotificacion> tipoNotificacion = root.join("tipoNotificacion");
        
        find.addPath("root", root);
        find.addPath("tipoNotificacion", tipoNotificacion);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdNotaDebito()!= null) {
            predList.add(qb.equal(root.get("idNotaDebito"), param.getIdNotaDebito()));
        }
        
        if (param.getIdTipoNotificacion() != null) {
            predList.add(qb.equal(root.get("idTipoNotificacion"), param.getIdTipoNotificacion()));
        }
        
        if (param.getCodigoTipoNotificacion() != null && !param.getCodigoTipoNotificacion().trim().isEmpty()) {
            predList.add(qb.equal(tipoNotificacion.get("codigo"), param.getCodigoTipoNotificacion().trim()));
        }
        
        if (param.getEmail() != null && !param.getEmail().trim().isEmpty()) {
            predList.add(qb.equal(root.get("email"), param.getEmail().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaDebitoNotificacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaDebitoNotificacion> root = cq.from(NotaDebitoNotificacion.class);
        Join<NotaDebitoNotificacion, TipoNotificacion> tipoNotificacion = root.join("tipoNotificacion");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdNotaDebito()!= null) {
            predList.add(qb.equal(root.get("idNotaDebito"), param.getIdNotaDebito()));
        }
        
        if (param.getIdTipoNotificacion() != null) {
            predList.add(qb.equal(root.get("idTipoNotificacion"), param.getIdTipoNotificacion()));
        }
        
        if (param.getCodigoTipoNotificacion() != null && !param.getCodigoTipoNotificacion().trim().isEmpty()) {
            predList.add(qb.equal(tipoNotificacion.get("codigo"), param.getCodigoTipoNotificacion().trim()));
        }
        
        if (param.getEmail() != null && !param.getEmail().trim().isEmpty()) {
            predList.add(qb.equal(root.get("email"), param.getEmail().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
