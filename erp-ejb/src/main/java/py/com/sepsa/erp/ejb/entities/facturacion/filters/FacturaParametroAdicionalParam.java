/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class FacturaParametroAdicionalParam extends CommonParam  {
    
      public FacturaParametroAdicionalParam() {
    }

    public FacturaParametroAdicionalParam(String bruto) {
        super(bruto);
    }

    @QueryParam("idFactura")
    private Integer idFactura;

    @QueryParam("idParametroAdicional")
    private Integer idParametroAdicional;

    @QueryParam("valor")
    private String valor;
    
    @QueryParam("codigoTipoParametroAdicional")
    private String codigoTipoParametroAdicional;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe el identificador de empresa"));
        }
        
        if(isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Factura"));
        }
        
        if(isNull(idParametroAdicional)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Parametro"));
        }

        if(isNullOrEmpty(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor del parametro"));
        }   
        
        return !tieneErrores();
    }  
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe el identificador de empresa"));
        }
        
        if(isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Factura"));
        }
        
        if(isNull(idParametroAdicional)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de Parametro"));
        }

        if(isNullOrEmpty(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor del parametro"));
        }   
        
        return !tieneErrores();
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdParametroAdicional() {
        return idParametroAdicional;
    }

    public void setIdParametroAdicional(Integer idParametroAdicional) {
        this.idParametroAdicional = idParametroAdicional;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCodigoTipoParametroAdicional() {
        return codigoTipoParametroAdicional;
    }

    public void setCodigoTipoParametroAdicional(String codigoTipoParametroAdicional) {
        this.codigoTipoParametroAdicional = codigoTipoParametroAdicional;
    }
    
}
