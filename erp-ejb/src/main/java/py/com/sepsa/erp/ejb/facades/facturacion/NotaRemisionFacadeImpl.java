/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.google.gson.JsonObject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.dtecore.v150.handler.Document;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.facturacion.LocalEntrega;
import py.com.sepsa.erp.ejb.entities.facturacion.LocalSalida;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionNr;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemision;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaRemisionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.Transportista;
import py.com.sepsa.erp.ejb.entities.facturacion.VehiculoTraslado;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaRemisionPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.KudeDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.adapters.XmlDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.rest.client.ConnectionPojo;
import py.com.sepsa.utils.rest.client.body.BodyResponse;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import py.com.sepsa.utils.siediApi.pojos.ResultadoOperacionSiediApi;
import py.com.sepsa.utils.siediApi.remote.LoginServiceSiediApi;
import py.com.sepsa.utils.siediApi.remote.SiediApiServiceClient;
import py.com.sepsa.utils.siedimonitor.pojos.DetalleResultado;
import py.com.sepsa.utils.siedimonitor.pojos.ResultadoOperacion;
import py.com.sepsa.utils.siedimonitor.remote.EventoService;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NotaRemisionFacade", mappedName = "NotaRemisionFacade")
@javax.ejb.Local(NotaRemisionFacade.class)
public class NotaRemisionFacadeImpl extends FacadeImpl<NotaRemision, NotaRemisionParam> implements NotaRemisionFacade {

    public NotaRemisionFacadeImpl() {
        super(NotaRemision.class);
    }
    
    /**
     * Obtiene los datos como para crear
     * @param talonario talonario
     * @param idCliente Identificador de cliente
     * @return Datos
     */
    public NotaRemisionPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente) {
        
        Date fecha = facades.getTalonarioFacade().ultimaFechaDoc(talonario, 4);
        String nroNotaRemision = facades.getTalonarioFacade().sigNumNotaRemision(talonario);

        String razonSocial = null;
        String ruc = null;
        String direccion = null;
        String nroCasa = null;
        Integer idDepartamento = null;
        Integer idDistrito = null;
        Integer idCiudad = null;

        Cliente cliente = idCliente == null
                ? null
                : facades.getClienteFacade().find(idCliente);

        if (cliente != null) {
            razonSocial = cliente.getRazonSocial();
            ruc = cliente.getNroDocumento();
            direccion = Units.execute(()-> cliente.getPersona().getDireccion().getDireccion());
            nroCasa = Units.execute(()-> cliente.getPersona().getDireccion().getNumero().toString());
            idDepartamento = Units.execute(()-> cliente.getPersona().getDireccion().getDepartamento().getId());
            idDistrito = Units.execute(()-> cliente.getPersona().getDireccion().getDistrito().getId());
            idCiudad = Units.execute(()-> cliente.getPersona().getDireccion().getCiudad().getId());
        }
        
        NotaRemisionPojo item = new NotaRemisionPojo();
        item.setDigital(talonario.getDigital());
        item.setIdTalonario(talonario.getId());
        item.setFechaVencimientoTimbrado(talonario.getFechaVencimiento());
        item.setTimbrado(talonario.getTimbrado());
        item.setFecha(fecha);
        item.setNroNotaRemision(nroNotaRemision);
        item.setRazonSocial(razonSocial);
        item.setRuc(ruc);
        item.setDireccion(direccion);
        item.setNroCasa(nroCasa);
        item.setIdDepartamento(idDepartamento);
        item.setIdDistrito(idDistrito);
        item.setIdCiudad(idCiudad);
        
        return item;
    }
    

    @Override
    public byte[] getXmlNotaRemision(NotaRemisionParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        NotaRemisionPojo nr = findFirstPojo(param);
        
        
        Closer closer = Closer.instance();

        try {

            String hostSiedi = null;
            Boolean siediApi = param.getSiediApi();
            if(siediApi){
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");
            } else {
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI");
            }
            
            if (hostSiedi == null || hostSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No se encuentra valor para el parámetro HOST_SIEDI/SIEDI_API_HOST"));
                return null;
            }
        
            Boolean ticket = param.getTicket();               
            String cdc = nr.getCdc();
            
            String nombreArchivo = String.format("NR_%s.txt", cdc);
            JsonObject object = null;
            if (siediApi){

                ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                String userSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                String passSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);

              if (tokenSiedi == null){

                    param.addError(MensajePojo.createInstance()
                            .descripcion("Credenciales de siediApi incorrectas."));
                    return null;
                }
                
                return XmlDteAdapter.obtener(tokenSiedi,hostSiedi, cdc, nombreArchivo, object,ticket, true);
            } else {
                return XmlDteAdapter.obtener(null,hostSiedi, cdc, nombreArchivo, object,ticket,false);
            }
   
        } finally {
            closer.close();
        }
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToEdit(NotaRemisionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        NotaRemision nr = facades
                .getNotaRemisionFacade()
                .find(param.getId());
        
        if(nr == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de remisión"));
        }
        
        if(param.getIdEstado() != null || param.getCodigoEstado() != null) {
            EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                    param.getCodigoEstado(), "NOTA_REMISION");

            if(estado == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
            } else {
                param.setIdEstado(estado.getId());
                param.setCodigoEstado(estado.getCodigo());
            }
        }
        
        
        if(param.getIdProcesamiento() != null) {
            
            Procesamiento procesamiento = facades.getProcesamientoFacade()
                    .find(param.getIdProcesamiento());

            if(procesamiento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el registro de procesamiento"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToGetPdf(NotaRemisionParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        NotaRemision notaRemision = find(param.getId());

        if (notaRemision == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de remisión"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToCancel(NotaRemisionParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "NOTA_REMISION");

        if (motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        } else {
            param.setIdMotivoAnulacion(motivoAnulacion.getId());
            param.setCodigoMotivoAnulacion(motivoAnulacion.getCodigo());
            param.setMotivoAnulacion(motivoAnulacion.getDescripcion());
        }
        
        NotaRemision notaRemision = facades.getNotaRemisionFacade().find(param.getId());

        if (notaRemision == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la nota de remisión"));
        } else {

            if (Objects.equals(notaRemision.getAnulado(), 'S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La nota de remisión ya se encuentra anulada"));
            }
            
            String diaDeclaracionMes = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "DIA_DECLARACION_MES");

            Integer diaDeclaracion = Units.execute(() -> Integer.valueOf(diaDeclaracionMes));

            if ((param.getIgnorarPeriodoAnulacion() == null
                    || !param.getIgnorarPeriodoAnulacion())
                    && !isNull(diaDeclaracion)) {

                Calendar inv = Calendar.getInstance();
                inv.setTime(notaRemision.getFecha());
                inv.add(Calendar.DAY_OF_MONTH, diaDeclaracion);
                inv.set(Calendar.HOUR_OF_DAY, 0);
                inv.set(Calendar.MINUTE, 0);
                inv.set(Calendar.SECOND, 0);
                
                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);

                if (inv.compareTo(now) < 0) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Sólo se pueden anular NRs que esten dentro del periodo de anulación"));
                }
            }
        }

        return !param.tieneErrores();
    }
    
    public Boolean validToCreate(NotaRemisionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        UsuarioParam uparam = new UsuarioParam();
        uparam.setId(param.getIdUsuario());
        UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);

        if (usuario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo usuario"));
        }
        
        if (param.getTransportista().getId() != null){
            Transportista trans = facades.getTransportistaFacade().find(param.getTransportista().getId());
            if (trans == null) {
                 param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el transportista"));
            }
        }
        
        if (param.getVehiculoTraslado().getId() != null){
            VehiculoTraslado vehiculo = facades.getVehiculoTrasladoFacade().find(param.getVehiculoTraslado().getId());
            if (vehiculo == null) {
                 param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el vehiculo de traslado"));
            }
        }
          
        TalonarioParam param2 = new TalonarioParam();
        param2.setId(param.getIdTalonario());
        TalonarioPojo talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        
        if(talonario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }
        
        NaturalezaCliente naturaleza = facades.getNaturalezaClienteFacade()
                .find(param.getIdNaturalezaCliente(), param.getCodigoNaturalezaCliente());

        if (naturaleza == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la naturaleza del cliente"));
        } else {
            param.setIdNaturalezaCliente(naturaleza.getId());
            param.setCodigoNaturalezaCliente(naturaleza.getCodigo());
        }
        
        Cliente cliente = param.getIdCliente() == null
                ? null
                : facades.getClienteFacade().find(param.getIdCliente());
        
        ReferenciaGeografica departamento = param.getIdDepartamento() == null
                ? null
                : facades.getReferenciaGeograficaFacade().find(param.getIdDepartamento());
        
        ReferenciaGeografica distrito = param.getIdDistrito()== null
                ? null
                : facades.getReferenciaGeograficaFacade().find(param.getIdDistrito());
        
        ReferenciaGeografica ciudad = param.getIdCiudad()== null
                ? null
                : facades.getReferenciaGeograficaFacade().find(param.getIdCiudad());
        
        if(param.getIdCliente() != null && cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        if(param.getIdDepartamento() != null && departamento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el departamento"));
        }
        
        if(param.getIdDistrito()!= null && distrito == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el distrito"));
        }
        
        if(param.getIdCiudad()!= null && ciudad == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la ciudad"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
            param.getCodigoEstado(), "NOTA_REMISION");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        
        String ruc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");
        
        if(ruc == null || ruc.trim().isEmpty()) {
            ruc = "";
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el valor de configuración de RUC"));
        }
        
        if(param.getDigital().equals('S')) {
            
            String codSeguridad =  "000000000" + String.valueOf(param.getFecha().getTime());
            codSeguridad = codSeguridad.substring(codSeguridad.length() - 9, codSeguridad.length());
            param.setCodSeguridad(codSeguridad);
                
            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(param.getNroNotaRemision(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch(j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            int i = 0;

            String dRucEm = "";
            String dDVEmi = "";
            
            for (StringTokenizer stringTokenizer = new StringTokenizer(ruc, "-"); stringTokenizer.hasMoreTokens(); i++) {

                String token = stringTokenizer.nextToken();

                switch(i) {
                    case 0:
                        dRucEm = token;
                        break;

                    case 1:
                        dDVEmi = token;
                        break;
                }
            }
            
            String tipoContribuyente = null;
            if (dRucEm.length()<8){
                tipoContribuyente = "1"; //Persona Física
            } else {
                tipoContribuyente = "2"; //Persona Jurídica
            }
            
            String cdc = Document.getCDCWithoutDv("07", dRucEm, dDVEmi, dEst, dPunt, nroDoc,
                    tipoContribuyente, param.getFecha(), "1", param.getCodSeguridad());
            cdc = Document.getCDC(cdc);
            param.setCdc(cdc);
        }
        
        Date date = Calendar.getInstance().getTime();
        TalonarioParam param1 = new TalonarioParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setId(param.getIdTalonario());
        param1.setIdTipoDocumento(4);
        param1.setDigital(param.getDigital());
        param1.setFecha(date);
        
        TalonarioPojo talonarioC = facades.getTalonarioFacade().getTalonario(param1);
        
        if(talonarioC == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un talonario activo para el tipo de documento nota de remisión"));
        } else {
            if(!talonarioC.getId().equals(param.getIdTalonario())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El identificador de talonario no corresponde con el activo"));
            }
            
            if (!facades.getTalonarioFacade().fechaDocumentoEnRango(talonario, param.getFecha())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la nota de remisión debe estar en el rango de vigencia del timbrado"));
            }
        }
        
        if(!facades.getTalonarioFacade().nroDocEnRango(talonario, param.getNroNotaRemision())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de nota de remisión no se encuentra en el rango del talonario"));
        }
        
        NotaRemisionPojo item = obtenerDatosCrear(talonario, null);
        
        Boolean correlativoNroNotaRemision = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "TALONARIO_CORRELATIVO_NOTA_REMISION", Boolean.TRUE);
        Boolean asignarNumeracionNr = facades.getConfiguracionValorFacade().getCVBoolean(param.getIdEmpresa(), "ASIGNAR_NUMERACION_NOTA_REMISION", Boolean.FALSE);
        
        if(asignarNumeracionNr) {
            param.setNroNotaRemision(item.getNroNotaRemision());
        } else if (correlativoNroNotaRemision) {
            if (!item.getNroNotaRemision().equals(param.getNroNotaRemision())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El nro de la nota de remisión debe ser correlativo"));
            }
        }
        
        NotaRemisionParam nRemisionRepetido = new NotaRemisionParam();
        nRemisionRepetido.setIdTalonario(talonario.getId());
        nRemisionRepetido.setIdEmpresa(param.getIdEmpresa());
        nRemisionRepetido.setNroNotaRemision(param.getNroNotaRemision());
        NotaRemisionPojo NROc = facades.getNotaRemisionFacade().findFirstPojo(nRemisionRepetido);
        if(NROc!=null){
            param.addError(MensajePojo.createInstance()
                        .descripcion("El nro de la nota de remisión ya existe en el talonario"));
        }
        
        if (param.getDigital().equals('N') && item.getFecha().after(param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La nota de remisión no puede tener fecha anterior a una creada previamente"));
        }
                
        for (NotaRemisionDetalleParam detalle : param.getNotaRemisionDetalles()) {
            facades.getNotaRemisionDetalleFacade().validToCreate(detalle, false);
        }
        
        return !param.tieneErrores();
    }
    
    
    /**
     * Edita una instancia
     * @param param parámetros
     * @return instancia
     */
    @Override
    public NotaRemision edit(NotaRemisionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        NotaRemision nr = facades
                .getNotaRemisionFacade()
                .find(param.getId());
        
        nr.setObservacion(param.getObservacion());
        nr.setArchivoSet(param.getArchivoSet());
        nr.setEstadoSincronizado(nr.getDigital().equals('S')
                && param.getArchivoSet().equals('S')
                && !nr.getGeneradoSet().equals(param.getGeneradoSet())
                ? 'N'
                : param.getEstadoSincronizado() != null
                        ? param.getEstadoSincronizado()
                        : nr.getEstadoSincronizado());
        nr.setGeneradoSet(param.getGeneradoSet());
        nr.setIdEstado(param.getIdEstado() != null ? param.getIdEstado() : nr.getIdEstado());
        nr.setIdProcesamiento(param.getIdProcesamiento());
        
        edit(nr);
        
        return nr;
    }
    
    @Override
    public NotaRemision anular(NotaRemisionParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        Boolean anular = Boolean.FALSE;
        String codigoEstado = "CANCELADO";

        NotaRemision notaRemision = find(param.getId());

        if (notaRemision.getDigital().equals('N')) {

            anular = Boolean.TRUE;

        } else {

            String urlSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI_JSON");

            if (urlSiedi == null || urlSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance().descripcion("No existe la configuración HOST_SIEDI_JSON"));
                return null;
            }

            EventoService service = new EventoService(userInfo.getToken(), urlSiedi);

            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(notaRemision.getNroNotaRemision(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch (j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            BodyResponse<ResultadoOperacion> result = null;
            BodyResponse<List<ResultadoOperacionSiediApi>> result1 = null;

            String siediApi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API");     
            String userSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
            String passSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
            String siediApiHost =  facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");

            if (notaRemision.getEstado().getCodigo().startsWith("APROBADO")) {
                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.cancelarDteSiediApi(notaRemision.getCdc(),
                           param.getMotivoAnulacion());
                   codigoEstado = "CANCELADO";
                } else {
                result = service.cancelarDte(notaRemision.getCdc(),
                        param.getMotivoAnulacion());
                codigoEstado = "CANCELADO";
                }
            } else {
                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.inutilizarSiediApi(notaRemision.getTalonario().getTimbrado(),
                        dEst, dPunt, nroDoc, nroDoc, 5, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                } else {
                    result = service.inutilizar(notaRemision.getTalonario().getTimbrado(),
                        dEst, dPunt, nroDoc, nroDoc, 5, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                }
            }

            if (result != null) {
                
                if (!result.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result.getPayload().getOperaciones() != null
                            && !result.getPayload().getOperaciones().isEmpty()
                            && result.getPayload().getOperaciones().get(0).getEstado().equalsIgnoreCase("Aprobado")) {
                        anular = Boolean.TRUE;
                    }
                }
            } else if (result1 != null){
                 if (!result1.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Rechazado")))) {

                        for (DetalleResultado dr : result1.getPayload().get(0).getResultado().getDetalles()) {
                           if ("4003".equals(dr.getCodigo())) {
                               anular = Boolean.TRUE;
                           } else {
                                param.addError(MensajePojo.createInstance()
                            .descripcion(dr.getMensaje()));
                           }     
                       } 
                    } else if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Aprobado"))
                            || (result1.getPayload().get(0).getResultado().getDetalles() != null
                            && result1.getPayload().get(0).getResultado().getDetalles().stream().filter(item -> item.getCodigo().equals("4003")).count() > 0))) {
                        anular = Boolean.TRUE;
                    } 
                }
            } else {
                param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
            }
        }

        if (anular) {

            EstadoPojo estado = facades.getEstadoFacade().find(null, codigoEstado, "NOTA_REMISION");

            notaRemision.setAnulado('S');
            notaRemision.setIdEstado(estado.getId());
            notaRemision.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
            notaRemision.setObservacionAnulacion(param.getObservacionAnulacion());

            edit(notaRemision);

            return notaRemision;
        } else {
            return null;
        }
    }
    
    @Override
    public NotaRemision create(NotaRemisionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        LocalEntrega localEntrega = new LocalEntrega();
        localEntrega.setDireccion(param.getLocalEntrega().getDireccion());
        localEntrega.setNroCasa(param.getLocalEntrega().getNroCasa());
        localEntrega.setIdDepartamento(param.getLocalEntrega().getIdDepartamento());
        localEntrega.setIdDistrito(param.getLocalEntrega().getIdDistrito());
        localEntrega.setIdCiudad(param.getLocalEntrega().getIdCiudad());
        facades.getLocalEntregaFacade().create(localEntrega);
        
        LocalSalida localSalida = new LocalSalida();
        localSalida.setDireccion(param.getLocalSalida().getDireccion());
        localSalida.setNroCasa(param.getLocalSalida().getNroCasa());
        localSalida.setIdDepartamento(param.getLocalSalida().getIdDepartamento());
        localSalida.setIdDistrito(param.getLocalSalida().getIdDistrito());
        localSalida.setIdCiudad(param.getLocalSalida().getIdCiudad());
        facades.getLocalSalidaFacade().create(localSalida);
        
        
        VehiculoTraslado vt = new VehiculoTraslado();
        if (param.getVehiculoTraslado().getId() != null){
            vt.setId(param.getVehiculoTraslado().getId());
        } else {
            vt.setMarca(param.getVehiculoTraslado().getMarca());
            vt.setTipoIdentificacion(param.getVehiculoTraslado().getTipoIdentificacion());
            vt.setNroIdentificacion(param.getVehiculoTraslado().getNroIdentificacion());
            vt.setTipoVehiculo(param.getVehiculoTraslado().getTipoVehiculo());
            vt.setNroMatricula(param.getVehiculoTraslado().getNroMatricula());
            vt.setNroVuelo(param.getVehiculoTraslado().getNroVuelo());
            vt.setIdEmpresa(param.getIdEmpresa());
            facades.getVehiculoTrasladoFacade().create(vt);
        }
         
        Transportista t = new Transportista();
        if (param.getTransportista().getId() != null) {
            t.setId(param.getTransportista().getId());
        } else {
            t.setIdNaturalezaTransportista(param.getTransportista().getIdNaturalezaTransportista());
            t.setRazonSocial(param.getTransportista().getRazonSocial());
            t.setRuc(param.getTransportista().getRuc());
            t.setVerificadorRuc(param.getTransportista().getVerificadorRuc());
            t.setIdTipoDocumento(param.getTransportista().getIdTipoDocumento());
            t.setNroDocumento(param.getTransportista().getNroDocumento());
            t.setNombreCompleto(param.getTransportista().getNombreCompleto());
            t.setDomicilioFiscal(param.getTransportista().getDomicilioFiscal());
            t.setDireccionChofer(param.getTransportista().getDireccionChofer());
            t.setNroDocumentoChofer(param.getTransportista().getNroDocumentoChofer());
            t.setIdEmpresa(param.getIdEmpresa());
            facades.getTransportistaFacade().create(t);
        }
        
        NotaRemision nr = new NotaRemision();
        nr.setIdLocalEntrega(localEntrega.getId());
        nr.setIdLocalSalida(localSalida.getId());
        nr.setIdVehiculoTraslado(vt.getId());
        nr.setIdTransportista(t.getId());
       
        nr.setIdEmpresa(param.getIdEmpresa());
        nr.setIdUsuario(param.getIdUsuario());
        nr.setAnulado(param.getAnulado());
        nr.setCdc(param.getCdc());
        nr.setCodSeguridad(param.getCodSeguridad());
        nr.setIdNaturalezaCliente(param.getIdNaturalezaCliente());
        nr.setDireccion(param.getDireccion());
        nr.setNroCasa(param.getNroCasa());
        nr.setNroNotaRemision(param.getNroNotaRemision());
        nr.setFecha(param.getFecha());
        nr.setArchivoSet(param.getArchivoSet());
        nr.setGeneradoSet('N');
        nr.setDigital(param.getDigital());
        nr.setEstadoSincronizado(param.getEstadoSincronizado());
        nr.setIdCiudad(param.getIdCiudad());
        nr.setIdCliente(param.getIdCliente());
        nr.setIdDepartamento(param.getIdDepartamento());
        nr.setIdDistrito(param.getIdDistrito());
        nr.setIdMotivoEmisionNr(param.getIdMotivoEmisionNr());
        nr.setIdTalonario(param.getIdTalonario());
        nr.setObservacion(param.getObservacion());
        nr.setRazonSocial(param.getRazonSocial());
        nr.setRuc(param.getRuc());
        nr.setKm(param.getKm());
        nr.setFechaInsercion(Calendar.getInstance().getTime());
        nr.setResponsableEmision(param.getResponsableEmision());     
        nr.setTipoTransporte(param.getTipoTransporte());
        nr.setModalidadTransporte(param.getModalidadTransporte());
        nr.setResponsableCostoFlete(param.getResponsableCostoFlete());
        nr.setNroDespachoImportacion(param.getNroDespachoImportacion());
        nr.setDescripcionMotivoEmisionNr(param.getDescripcionMotivoEmisionNr());
        nr.setFechaInicioTraslado(param.getFechaInicioTraslado());
        nr.setFechaFinTraslado(param.getFechaFinTraslado());
        nr.setFechaFuturaEmision(param.getFechaFuturaEmision());
        nr.setCdcFactura(param.getCdcFactura());
        nr.setTimbradoFactura(param.getTimbradoFactura());
        nr.setNroFactura(param.getNroFactura());
        nr.setFechaFactura(param.getFechaFactura());
        nr.setTieneFactura(param.getTieneFactura());
        nr.setIdEstado(param.getIdEstado());
        create(nr);
        
        for (NotaRemisionDetalleParam detalle : param.getNotaRemisionDetalles()) {
            detalle.setIdNotaRemision(nr.getId());
            facades.getNotaRemisionDetalleFacade().create(detalle, userInfo);
        }
        
        return nr;
    }
    
//    @Override
//    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param) {
//        
//        String where = "true";
//        
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        
//        if(param.getIdEmpresa() != null) {
//            where = String.format(" %s and nc.id_empresa = %d", where, param.getIdEmpresa());
//        }
//        
//        if(param.getFechaDesde() != null) {
//            where = String.format(" %s and nc.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//        
//        if(param.getFechaHasta() != null) {
//            where = String.format(" %s and nc.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//        
//        String sql = String.format("select distinct nc.ruc, 110 as tipo_comprobante, nc.fecha, t.timbrado,\n"
//                + "nc.nro_nota_credito, nc.monto_total_10, nc.monto_total_5,\n"
//                + "nc.monto_total_exento, nc.monto_total_nota_credito,\n"
//                + "case when m.codigo in ('PYG','Gs.') then 'N' else 'S' end,\n"
//                + "f.nro_factura, tf.timbrado as timbrado_factura\n"
//                + "from facturacion.nota_credito nc\n"
//                + "left join facturacion.nota_credito_detalle ncd on ncd.id_nota_credito = nc.id\n"
//                + "left join facturacion.factura f on f.id = ncd.id_factura\n"
//                + "left join facturacion.talonario tf on tf.id = f.id_talonario\n"
//                + "left join comercial.moneda m on m.id = f.id_moneda\n"
//                + "left join facturacion.talonario t on t.id = f.id_talonario\n"
//                + "where %s offset %d limit %d",
//                where, param.getFirstResult(), param.getPageSize());
//        
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        List<Object[]> list = q.getResultList();
//
//        List<RegistroComprobantePojo> result = new ArrayList<>();
//        
//        for (Object[] objects : list) {
//            int i = 0;
//            RegistroComprobantePojo pojo = new RegistroComprobantePojo();
//            pojo.setIdentificacionComprador((String) objects[i++]);
//            pojo.setTipoComprobante((Integer) objects[i++]);
//            pojo.setFechaEmisionComprobante((Date) objects[i++]);
//            pojo.setTimbradoComprobanteAsociado(new Integer((String) objects[i++]));
//            pojo.setNumeroComprobante((String) objects[i++]);
//            pojo.setMontoIva10(new BigDecimal(objects[i++] + ""));
//            pojo.setMontoIva5(new BigDecimal(objects[i++] + ""));
//            pojo.setMontoExento(new BigDecimal(objects[i++] + ""));
//            pojo.setMontoTotal(new BigDecimal(objects[i++] + ""));
//            pojo.setMonedaExtrangera(((String)objects[i++]).charAt(0));
//            pojo.setNumeroComprobanteAsociado((String) objects[i++]);
//            pojo.setTimbradoComprobanteAsociado(new Integer((String) objects[i++]));
//            result.add(pojo);
//        }
//
//        return result;
//    }

//    @Override
//    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param) {
//        
//        String where = "true";
//        
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        
//        if(param.getIdEmpresa() != null) {
//            where = String.format(" %s and nc.id_empresa = %d", where, param.getIdEmpresa());
//        }
//        
//        if(param.getFechaDesde() != null) {
//            where = String.format(" %s and nc.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//        
//        if(param.getFechaHasta() != null) {
//            where = String.format(" %s and nc.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//        
//        String sql = String.format("select count(distinct(nc.id, f.id)) "
//                + "from facturacion.nota_credito nc "
//                + "left join facturacion.nota_credito_detalle ncd on ncd.id_nota_credito = nc.id "
//                + "left join facturacion.factura f on f.id = ncd.id_factura "
//                + "where %s ", where);
//        
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        Object object = q.getSingleResult();
//
//        Integer result = object == null
//                ? 0
//                : ((Number)object).intValue();
//
//        return result;
//    }
    
    @Override
    public List<NotaRemisionPojo> findPojo(NotaRemisionParam param) {

        AbstractFind find = new AbstractFind(NotaRemisionPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idCliente"),
                        getPath("root").get("nroNotaRemision"),
                        getPath("root").get("idTalonario"),
                        getPath("talonario").get("fechaVencimiento"),
                        getPath("talonario").get("fechaInicio"),
                        getPath("talonario").get("timbrado"),
                        getPath("talonario").get("serie"),
                        getPath("localTalonario").get("descripcion"),
                        getPath("root").get("fecha"),
                        getPath("root").get("razonSocial"),
                        getPath("root").get("direccion"),
                        getPath("root").get("nroCasa"),
                        getPath("root").get("idDepartamento"),
                        getPath("root").get("idDistrito"),
                        getPath("root").get("idCiudad"),
                        getPath("root").get("ruc"),
                        getPath("root").get("observacion"),
                        getPath("root").get("anulado"),
                        getPath("root").get("digital"),
                        getPath("root").get("archivoSet"),
                        getPath("root").get("generadoSet"),
                        getPath("root").get("estadoSincronizado"),
                        getPath("root").get("idProcesamiento"),
                        getPath("root").get("cdc"),
                        getPath("root").get("codSeguridad"),
                        getPath("root").get("idNaturalezaCliente"),
                        getPath("root").get("idMotivoEmisionNr"),
                        getPath("motivoEmisionNr").get("descripcion"),
                        getPath("motivoEmisionNr").get("codigo"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("codigo")
              );
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<NotaRemision> find(NotaRemisionParam param) {

        AbstractFind find = new AbstractFind(NotaRemision.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, NotaRemisionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<NotaRemision> root = cq.from(NotaRemision.class);
        Join<NotaRemision, NotaRemisionDetalle> notaRemisionDetalles = root.join("notaRemisionDetalles", JoinType.LEFT);
        Join<NotaRemision, MotivoEmisionNr> motivoEmisionNr = root.join("motivoEmisionNr", JoinType.LEFT);
        Join<NotaRemision, LocalEntrega> localEntrega = root.join("localEntrega", JoinType.LEFT);
        Join<NotaRemision, LocalSalida> localSalida = root.join("localSalida", JoinType.LEFT);
        Join<NotaRemision, VehiculoTraslado> vehiculoTraslado = root.join("vehiculoTraslado", JoinType.LEFT);
        Join<NotaRemision, Transportista> transportista = root.join("transportista", JoinType.LEFT);
        Join<NotaRemision, Talonario> talonario = root.join("talonario", JoinType.LEFT);
        Join<Talonario, Local> localTalonario = talonario.join("local", JoinType.LEFT);
        Join<NotaRemision, Estado> estado = root.join("estado", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("notaRemisionDetalles", notaRemisionDetalles);
        find.addPath("motivoEmisionNr", motivoEmisionNr);
        find.addPath("localEntrega", localEntrega);
        find.addPath("localSalida", localSalida);
        find.addPath("vehiculoTraslado", vehiculoTraslado);
        find.addPath("transportista", transportista);
        find.addPath("talonario",talonario);
        find.addPath("estado",estado);
        find.addPath("localTalonario",localTalonario);


        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
    
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
           
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
       
        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }

        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }
        
        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }
        
        if (param.getIdMotivoEmisionNr() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionNr"), param.getIdMotivoEmisionNr()));
        }
        

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroNotaRemision() != null && !param.getNroNotaRemision().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroNotaRemision")),
                    String.format("%%%s%%", param.getNroNotaRemision().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(param.getNroNotaCreditoDescendente() ? qb.desc(root.get("fecha")) : qb.asc(root.get("fecha")),
                param.getNroNotaCreditoDescendente() ? qb.desc(root.get("nroNotaRemision")) : qb.asc(root.get("nroNotaRemision")),
                param.getIdDescendente() ? qb.desc(root.get("id")) : qb.asc(root.get("id")));
        
        cq.where(pred);

        cq.groupBy(root.get("id"),
                    estado.get("id"),
                    talonario.get("id"),
                    motivoEmisionNr.get("id"),
                    talonario.get("local").get("descripcion"));
//                    moneda.get("id"));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaRemisionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaRemision> root = cq.from(NotaRemision.class);
        Join<NotaRemision, Estado> estado = root.join("estado", JoinType.LEFT);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }
        
        if (param.getIdNaturalezaCliente()!= null) {
            predList.add(qb.equal(root.get("idNaturalezaCliente"), param.getIdNaturalezaCliente()));
        }
       
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
            
        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
  
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }
        
        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }
        
        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getIdMotivoEmisionNr() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionNr"), param.getIdMotivoEmisionNr()));
        }
        
        
        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroNotaRemision() != null && !param.getNroNotaRemision().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroNotaRemision")),
                    String.format("%%%s%%", param.getNroNotaRemision().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }     

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }

    @Override
    public byte[] getPdfNotaRemision(NotaRemisionParam param) throws Exception {
        
        if (!validToGetPdf(param)) {
            return null;
        }
        
        NotaRemision notaRemision = find(param.getId());

        byte[] result = null;
        
        Closer closer = Closer.instance();
        
        try {
            if(notaRemision.getDigital().equals('N')) {

                String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_NOTA_CREDITO_AUTOIMPRESOR");
                InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
//                
//                if (isNull(is)) {
//                    param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para nota de crédito autoimpresor"));
//                } else {
//                    
//                    byte[] template = IOUtils.toByteArray(is);
//                    
//                    List<byte[]> bytesList = Lists.empty();
//                    
//                    if(param.getImprimirOriginal()) {
//                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//                            NotaCreditoAutoimpresorParam params = facades.getReportUtils().getParam(closer, notaRemision);
//                            params.setTipoNotaCredito(NotaCreditoAutoimpresorParam.TipoNotaCredito.ORIGINAL);
//                            byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//                            if(bytes != null && bytes.length > 0) {
//                                bytesList.add(bytes);
//                            }
//                        }
//                    }
//                    
//                    if(param.getImprimirDuplicado()) {
//                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//                            NotaCreditoAutoimpresorParam params = facades.getReportUtils().getParam(closer, notaRemision);
//                            params.setTipoNotaCredito(NotaCreditoAutoimpresorParam.TipoNotaCredito.DUPLICADO);
//                            byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//                            if(bytes != null && bytes.length > 0) {
//                                bytesList.add(bytes);
//                            }
//                        }
//                    }
//                    
//                    if(param.getImprimirTriplicado()) {
//                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//                            NotaCreditoAutoimpresorParam params = facades.getReportUtils().getParam(closer, notaRemision);
//                            params.setTipoNotaCredito(NotaCreditoAutoimpresorParam.TipoNotaCredito.TRIPLICADO);
//                            byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//                            if(bytes != null && bytes.length > 0) {
//                                bytesList.add(bytes);
//                            }
//                        }
//                    }
//                    
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                    facades.getReportUtils().merge(bytesList, baos, PageSize.LEGAL);
//                    result = baos.toByteArray();
//                }

            } else {
                String hostSiedi;
                boolean siediApi = Boolean.TRUE.equals(param.getSiediApi())
                        || "S".equalsIgnoreCase(facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API"));

                String claveHost = siediApi ? "SIEDI_API_HOST" : "HOST_SIEDI";
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), claveHost);
                
                if (hostSiedi == null || hostSiedi.trim().isEmpty()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se encuentra valor para el parámetro HOST_SIEDI/SIEDI_API_HOST"));
                    return null;
                }
                
                if(hostSiedi == null || hostSiedi.trim().isEmpty()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("No se encuentra valor para el parámetro HOST_SIEDI"));
                    return null;
                }
                
                String cdc = notaRemision.getCdc();
                String nombreArchivo = String.format("NR_%s.txt", cdc);
                JsonObject object = null;
                
                if (siediApi){
                    
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                    String userSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                    String passSiedi  = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi,passSiedi, connectionPojo);

                    if (tokenSiedi == null){
                        param.addError(MensajePojo.createInstance()
                              .descripcion("Credenciales de siediApi ncorrectas."));
                        return null;
                    }
                    return KudeDteAdapter.obtener(tokenSiedi,hostSiedi, cdc, nombreArchivo, object,false, true);
                } else {
                    return KudeDteAdapter.obtener(null,hostSiedi, cdc, nombreArchivo, object,false,false);
                }
            }
        } finally {
            closer.close();
        }
        
        return result;
    }
//    
//    @Override
//    public byte[] getXlsReporteVenta(NotaCreditoParam param, UserInfoImpl userInfo) {
//
//        if (!param.isValidToGetReporteVenta()) {
//            return null;
//        }
//
//        byte[] result = null;
//
//        Closer closer = Closer.instance();
//
//        try {
//
//            param.setFirstResult(0);
//            param.setPageSize(100000);
//
//            List<NotaCreditoPojo> notaCreditos = facades.getNotaCreditoFacade().findPojo(param);
//
//            ReporteVentaNotaCreditoParam params = facades.getReportUtils().getReporteVentaNotaCreditoParam(closer, param.getIdEmpresa(), param.getFechaDesde(), param.getFechaHasta(), notaCreditos);
//
//            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_REPORTE_VENTA_NC");
//            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
//
//            if (isNull(is)) {
//                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el reporte de venta"));
//            } else {
//                
//                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//                configuration.setOnePagePerSheet(false);
//                configuration.setDetectCellType(true);
//                //borrar espacio entre filas
//                configuration.setRemoveEmptySpaceBetweenRows(true);
//                //borrar espacio entre columnas
//                configuration.setRemoveEmptySpaceBetweenColumns(true);
//                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
//                configuration.setIgnoreCellBorder(false);
//                configuration.setWhitePageBackground(false);
//
//                result = JasperReporteGenerator.exportReportToXlsxBytes(params, is, configuration);
//            }
//
//        } finally {
//            closer.close();
//        }
//
//        return result;
//    }
}
