/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Contrato;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicio;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioParam;
import py.com.sepsa.erp.ejb.entities.info.Aprobacion;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "ContratoFacade", mappedName = "ContratoFacade")
@Local(ContratoFacade.class)
public class ContratoFacadeImpl extends FacadeImpl<Contrato, ContratoParam> implements ContratoFacade {

    public ContratoFacadeImpl() {
        super(Contrato.class);
    }

    @Override
    public Map<String, String> getMapConditions(Contrato item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("id_producto", item.getIdProducto() + "");
        map.put("id_moneda", item.getIdMoneda() + "");
        map.put("id_cliente", item.getIdCliente() + "");
        map.put("nro_contrato", item.getNroContrato() + "");
        map.put("fecha_inicio", item.getFechaInicio() + "");
        map.put("fecha_fin", item.getFechaFin()+ "");
        map.put("firmado", item.getFirmado()+ "");
        map.put("documento_aval", item.getDocumentoAval()+ "");
        map.put("renovacion_automatica", item.getRenovacionAutomatica()+ "");
        map.put("debito_automatico", item.getDebitoAutomatico()+ "");
        map.put("plazo_rescision", item.getPlazoRescision()+ "");
        map.put("observacion", item.getObservacion()+ "");
        map.put("id_estado", item.getIdEstado()+ "");
        map.put("id_aprobacion", item.getIdAprobacion() + "");
        map.put("cobro_adelantado", item.getCobroAdelantado()+ "");
        map.put("id_periodo_cobro", item.getIdPeriodoCobro()+ "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(Contrato item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    /**
     * Verifica si el objeto es valido para crear
     * @param param Parametros
     * @return Bandera
     */
    public Boolean validToCreate(ContratoParam param) {
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Cliente cliente = facades
                .getClienteFacade()
                .find(param.getIdCliente());
        
        if(cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        Moneda moneda = facades
                .getMonedaFacade()
                .find(param.getIdMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        }
        
        ProductoCom producto = facades
                .getProductoComFacade()
                .find(param.getIdProducto());
        
        if(producto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el concepto de producto"));
        }
        
        for (ContratoServicioParam servicio : param.getServicios()) {
            servicio.setIdProducto(param.getIdProducto());
            facades
                    .getContratoServicioFacade()
                    .validToCreate(servicio, Boolean.FALSE);
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de contrato
     * @param param Parámetros
     * @return Contrato
     */
    @Override
    public Contrato create(ContratoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Aprobacion aprobacion = facades.getAprobacionFacade().create();

        EstadoPojo estado = facades.getEstadoFacade().find(null, "PENDIENTE", "CONTRATO");
        
        Contrato contrato = new Contrato();
        contrato.setIdAprobacion(aprobacion.getId());
        contrato.setCobroAdelantado(param.getCobroAdelantado());
        contrato.setDebitoAutomatico(param.getDebitoAutomatico());
        contrato.setDocumentoAval(param.getDocumentoAval());
        contrato.setIdEstado(estado.getId());
        contrato.setFechaFin(param.getFechaFin());
        contrato.setFechaInicio(param.getFechaInicio());
        contrato.setFirmado(param.getFirmado());
        contrato.setIdCliente(param.getIdCliente());
        contrato.setIdMoneda(param.getIdMoneda());
        contrato.setIdProducto(param.getIdProducto());
        contrato.setNroContrato(param.getNroContrato());
        contrato.setObservacion(param.getObservacion());
        contrato.setPlazoRescision(param.getPlazoRescision());
        contrato.setRenovacionAutomatica(param.getRenovacionAutomatica());
        
        create(contrato);
        
        if(param.getServicios()!= null) {
            
            for (ContratoServicioParam servicio : param.getServicios()) {
                servicio.setIdContrato(contrato.getId());
                facades.getContratoServicioFacade().editarOCrear(servicio, userInfo);
            }
        }
        
        return contrato;
    }
    
    @Override
    public List<Contrato> find(ContratoParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        
        if (param.getId() != null) {
            where = String.format(" %s and c.id = %d", where, param.getId());
        }
        
        if (param.getFechaInicioDesde() != null) {
            where = String.format(" %s and c.fecha_inicio >= '%s'", where, sdf.format(param.getFechaInicioDesde()));
        }
        
        if (param.getFechaInicioHasta() != null) {
            where = String.format(" %s and c.fecha_inicio <= '%s'", where, sdf.format(param.getFechaInicioHasta()));
        }
        
        if (param.getFechaInicio() != null) {
            where = String.format(" %s and c.fecha_inicio = '%s'", where, sdf2.format(param.getFechaInicio()));
        }
        
        if (param.getFechaFinDesde() != null) {
            where = String.format(" %s and c.fecha_fin >= '%s'", where, sdf.format(param.getFechaFinDesde()));
        }
        
        if (param.getFechaFinHasta() != null) {
            where = String.format(" %s and c.fecha_fin <= '%s'", where, sdf.format(param.getFechaFinHasta()));
        }
        
        if (param.getFechaFin() != null) {
            where = String.format(" %s and c.fecha_fin = '%s'", where, sdf2.format(param.getFechaFin()));
        }
        
        if (param.getId() != null) {
            where = String.format(" %s and c.id = '%s'", where, param.getId());
        }
        
        if (param.getIdEstado() != null) {
            where = String.format(" %s and c.id_estado = %d", where, param.getIdEstado());
        }
        
        if (param.getRenovacionAutomatica() != null) {
            where = String.format(" %s and c.renovacion_automatica = '%s'", where, param.getRenovacionAutomatica());
        }
        
        if (param.getIdProducto() != null) {
            where = String.format(" %s and c.id_producto = %d", where, param.getIdProducto());
        }
        
        if (param.getIdCliente() != null) {
            where = String.format(" %s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdComercial() != null) {
            where = String.format(" %s and cli.id_comercial = %d", where, param.getIdComercial());
        }
        
        if (param.getEstadoCliente() != null) {
            where = String.format(" %s and cli.estado = '%s'", where, param.getEstadoCliente());
        }
        
        if (param.getIdMoneda() != null) {
            where = String.format(" %s and c.id_moneda = %d", where, param.getIdMoneda());
        }
        
        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format(" %s and c.observacion ilike '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getNroContrato() != null && !param.getNroContrato().trim().isEmpty()) {
            where = String.format(" %s and c.nro_contrato ilike '%%%s%%'", where, param.getNroContrato().trim());
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format(" %s and cli.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }
        
        String sql = String.format("select c.*\n"
                + "from comercial.contrato c\n"
                + "left join comercial.producto p on p.id = c.id_producto\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "left join info.persona com on com.id = cli.id_comercial\n"
                + "left join comercial.moneda m on m.id = c.id_moneda\n"
                + "where %s order by cli.razon_social offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Contrato.class);

        List<Contrato> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(ContratoParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        
        if (param.getId() != null) {
            where = String.format(" %s and c.id = %d", where, param.getId());
        }
        
        if (param.getFechaInicioDesde() != null) {
            where = String.format(" %s and c.fecha_inicio >= '%s'", where, sdf.format(param.getFechaInicioDesde()));
        }
        
        if (param.getFechaInicioHasta() != null) {
            where = String.format(" %s and c.fecha_inicio <= '%s'", where, sdf.format(param.getFechaInicioHasta()));
        }
        
        if (param.getFechaInicio() != null) {
            where = String.format(" %s and c.fecha_inicio = '%s'", where, sdf2.format(param.getFechaInicio()));
        }
        
        if (param.getFechaFinDesde() != null) {
            where = String.format(" %s and c.fecha_fin >= '%s'", where, sdf.format(param.getFechaFinDesde()));
        }
        
        if (param.getFechaFinHasta() != null) {
            where = String.format(" %s and c.fecha_fin <= '%s'", where, sdf.format(param.getFechaFinHasta()));
        }
        
        if (param.getFechaFin() != null) {
            where = String.format(" %s and c.fecha_fin = '%s'", where, sdf2.format(param.getFechaFin()));
        }
        
        if (param.getId() != null) {
            where = String.format(" %s and c.id = '%s'", where, param.getId());
        }
        
        if (param.getIdEstado() != null) {
            where = String.format(" %s and c.id_estado = %d", where, param.getIdEstado());
        }
        
        if (param.getRenovacionAutomatica() != null) {
            where = String.format(" %s and c.renovacion_automatica = '%s'", where, param.getRenovacionAutomatica());
        }
        
        if (param.getIdProducto() != null) {
            where = String.format(" %s and c.id_producto = %d", where, param.getIdProducto());
        }
        
        if (param.getIdCliente() != null) {
            where = String.format(" %s and c.id_cliente = %d", where, param.getIdCliente());
        }
        
        if (param.getIdComercial() != null) {
            where = String.format(" %s and cli.id_comercial = %d", where, param.getIdComercial());
        }
        
        if (param.getEstadoCliente() != null) {
            where = String.format(" %s and cli.estado = '%s'", where, param.getEstadoCliente());
        }

        if (param.getIdMoneda() != null) {
            where = String.format(" %s and c.id_moneda = %d", where, param.getIdMoneda());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format(" %s and c.observacion ilike '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getNroContrato() != null && !param.getNroContrato().trim().isEmpty()) {
            where = String.format(" %s and c.nro_contrato ilike '%%%s%%'", where, param.getNroContrato().trim());
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            where = String.format(" %s and cli.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
        }
        
        String sql = String.format("select count( distinct(c.id))\n"
                + "from comercial.contrato c\n"
                + "left join comercial.producto p on p.id = c.id_producto\n"
                + "left join comercial.cliente cli on cli.id_cliente = c.id_cliente\n"
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number) object).longValue();
        
        return result;
    }
    
    /**
     * Inactiva un contrato
     * @param param Parámetros
     * @return Contrato
     */
    @Override
    public Contrato inactivar(ContratoParam param) {

        if(!param.isValidToInactivate()) {
            return null;
        }

        Contrato contrato = find(param.getId());

        if(contrato == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato"));
            return null;
        }

        EstadoPojo estado = facades.getEstadoFacade().find(null, "INACTIVO", "CONTRATO");
        contrato.setIdEstado(estado.getId());
        edit(contrato);
        
        return contrato;
    }
    
    /**
     * Renueva un contrato
     * @param param parámetros
     * @return Contrato
     */
    @Override
    public Contrato renovar(ContratoParam param) {

        if(!param.isValidToRenew()) {
            return null;
        }

        Contrato contrato = find(param.getId());

        if(contrato == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el contrato"));
            return null;
        }

        EstadoPojo estado = facades.getEstadoFacade().find(null, "INACTIVO", "CONTRATO");
        contrato.setIdEstado(estado.getId());
        edit(contrato);

        estado = facades.getEstadoFacade().find(null, "ACTIVO", "CONTRATO");
        contrato.setId(null);
        contrato.setIdEstado(estado.getId());
        create(contrato);

        ContratoServicioParam param1 = new ContratoServicioParam();
        param1.setIdContrato(param.getId());
        param1.setActivo('S');
        param1.isValidToList();
        List<ContratoServicio> list = facades
                .getContratoServicioFacade()
                .find(param1);

        for (ContratoServicio item : list) {
            facades.getContratoServicioFacade()
                    .renovar(param.getId(), contrato.getId(),
                            item.getContratoServicioPK().getIdServicio());
        }

        return contrato;
    }
}
