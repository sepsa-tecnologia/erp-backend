/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.LiquidacionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleCadenaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.LiquidacionDetalleCadenaPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface LiquidacionDetalleFacade extends Facade<LiquidacionDetalle, LiquidacionDetalleParam, UserInfoImpl> {

    public Boolean validToCreate(LiquidacionDetalleParam param, boolean validarHistoricoLiquidacion);

    public List<LiquidacionDetalleCadenaPojo> findCadena(LiquidacionDetalleCadenaParam param);

    public Long findCadenaSize(LiquidacionDetalleCadenaParam param);
    
}