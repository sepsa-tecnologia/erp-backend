/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Retencion;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "RetencionFacade", mappedName = "RetencionFacade")
@Local(RetencionFacade.class)
public class RetencionFacadeImpl extends FacadeImpl<Retencion, RetencionParam> implements RetencionFacade {

    public RetencionFacadeImpl() {
        super(Retencion.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToCreate(RetencionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Cliente cliente = facades
                .getClienteFacade()
                .find(param.getIdCliente());
        
        if(isNull(cliente)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el cliente"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "RETENCION");
        
        if(isNull(estado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
        }
        
        for (RetencionDetalleParam detalle : param.getRetencionDetalles()) {
            facades.getRetencionDetalleFacade().validToCreate(detalle, false);
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToCancel(RetencionParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "RETENCION");
        
        if(motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        }
        
        Retencion retencion = facades.getRetencionFacade().find(param.getId());

        if (retencion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la retención"));
        }

        return !param.tieneErrores();
    }
    
    @Override
    public Retencion create(RetencionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Retencion retencion = new Retencion();
        retencion.setIdEmpresa(userInfo.getIdEmpresa());
        retencion.setFechaInsercion(Calendar.getInstance().getTime());
        retencion.setFecha(param.getFecha());
        retencion.setIdCliente(param.getIdCliente());
        retencion.setMontoImponibleTotal(param.getMontoImponibleTotal());
        retencion.setMontoIvaTotal(param.getMontoIvaTotal());
        retencion.setMontoTotal(param.getMontoTotal());
        retencion.setMontoRetenidoTotal(param.getMontoRetenidoTotal());
        retencion.setNroRetencion(param.getNroRetencion());
        retencion.setPorcentajeRetencion(param.getPorcentajeRetencion());
        retencion.setIdEstado(param.getIdEstado());
        create(retencion);
        
        for (RetencionDetalleParam detalle : param.getRetencionDetalles()) {
            detalle.setIdRetencion(retencion.getId());
            facades.getRetencionDetalleFacade().create(detalle, userInfo);
        }
        
        return retencion;
    }

    @Override
    public Retencion anular(RetencionParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        RetencionDetalleParam param1 = new RetencionDetalleParam();
        param1.setIdEmpresa(userInfo.getIdEmpresa());
        param1.setIdRetencion(param.getId());
        param1.setFirstResult(0);
        param1.setPageSize(100);

        List<RetencionDetalle> list = facades
                .getRetencionDetalleFacade()
                .find(param1);

        for (RetencionDetalle retencionDetalle : list) {

            EstadoPojo estado = facades.getEstadoFacade().find(null, "ANULADO", "RETENCION_DETALLE");
            retencionDetalle.setIdEstado(estado.getId());
            facades.getRetencionDetalleFacade().edit(retencionDetalle);

            facades.getFacturaFacade().actualizarSaldoFactura(
                    retencionDetalle.getIdFactura(),
                    retencionDetalle.getMontoRetenido().negate());

        }

        EstadoPojo estado = facades.getEstadoFacade().find(null, "ANULADO", "RETENCION");
        
        Retencion retencion = find(param.getId());
        retencion.setIdEstado(estado.getId());
        retencion.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
        retencion.setObservacionAnulacion(param.getObservacionAnulacion());

        edit(retencion);

        return retencion;
    }
    
    @Override
    public List<Retencion> find(RetencionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Retencion.class);
        Root<Retencion> root = cq.from(Retencion.class);
        Join<Retencion, RetencionDetalle> retencionDetalles = root.join("retencionDetalles", JoinType.LEFT);
        Join<Retencion, Cliente> cliente = root.join("cliente", JoinType.LEFT);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(retencionDetalles.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(cliente.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getCliente() != null && !param.getCliente().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(cliente.<String>get("razonSocial")), String.format("%%%s%%", param.getCliente().trim().toUpperCase())));
        }
        
        if (param.getNroRetencion() != null && !param.getNroRetencion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroRetencion")), String.format("%%%s%%", param.getNroRetencion().trim().toUpperCase())));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.groupBy(root.get("id"));
        
        cq.orderBy(qb.desc(root.get("fecha")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<Retencion> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(RetencionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Retencion> root = cq.from(Retencion.class);
        Join<Retencion, RetencionDetalle> retencionDetalles = root.join("retencionDetalles", JoinType.LEFT);
        Join<Retencion, Cliente> cliente = root.join("cliente", JoinType.LEFT);
        
        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFactura() != null) {
            predList.add(qb.equal(retencionDetalles.get("idFactura"), param.getIdFactura()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(cliente.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getCliente() != null && !param.getCliente().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(cliente.<String>get("razonSocial")), String.format("%%%s%%", param.getCliente().trim().toUpperCase())));
        }
        
        if (param.getNroRetencion() != null && !param.getNroRetencion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroRetencion")), String.format("%%%s%%", param.getNroRetencion().trim().toUpperCase())));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
