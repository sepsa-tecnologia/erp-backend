/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.proceso.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de lote
 *
 * @author Jonathan
 */
public class LoteParam extends CommonParam {

    public LoteParam() {
    }

    public LoteParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de tipo de lote
     */
    @QueryParam("idTipoLote")
    private Integer idTipoLote;

    /**
     * Código de tipo de lote
     */
    @QueryParam("codigoTipoLote")
    private String codigoTipoLote;

    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;

    /**
     * Notificado
     */
    @QueryParam("notificado")
    private Character notificado;

    /**
     * Fecha insercion
     */
    private Date fechaInsercion;

    /**
     * Fecha procesamiento
     */
    private Date fechaProcesamiento;

    /**
     * Fecha insercion desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha insercion hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Nombre de archivo
     */
    private String nombreArchivo;

    /**
     * Resultado del lote
     */
    private String resultado;

    /**
     * Email
     */
    private String email;

    /**
     * Email
     */
    private String observacion;

    /**
     * Archivos
     */
    private List<LoteArchivoParam> archivos;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        codigoEstado = "PENDIENTE";

        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }

        if (isNull(idUsuario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de usuario"));
        }

        if (isNull(idTipoLote) && isNullOrEmpty(codigoTipoLote)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo lote"));
        }

        if (isNullOrEmpty(archivos)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los archivos"));
        } else {
            for (LoteArchivoParam archivo : archivos) {
                archivo.setIdLote(0);
                archivo.setIdEmpresa(idEmpresa);
                archivo.isValidToCreate();
            }
        }

        if (isNullOrEmpty(nombreArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre del archivo"));
        }

        if (isNullOrEmpty(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email para la notificación de procesamiento"));
        }

        return !tieneErrores();
    }
    
    public boolean isValidToGetResult() {

        limpiarErrores();

        super.isValidToList();
        
        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de proceso"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de proceso"));
        }

        if (isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }

        if (isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }

        if (isNull(notificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el lote fue notificado"));
        }

        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {

        List<MensajePojo> result = new ArrayList<>();

        if (!isNullOrEmpty(archivos)) {
            for (LoteArchivoParam archivo : archivos) {
                result.addAll(archivo.getErrores());
            }
        }
        
        return result;
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoLote() {
        return idTipoLote;
    }

    public void setIdTipoLote(Integer idTipoLote) {
        this.idTipoLote = idTipoLote;
    }

    public String getCodigoTipoLote() {
        return codigoTipoLote;
    }

    public void setCodigoTipoLote(String codigoTipoLote) {
        this.codigoTipoLote = codigoTipoLote;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Character getNotificado() {
        return notificado;
    }

    public void setNotificado(Character notificado) {
        this.notificado = notificado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getResultado() {
        return resultado;
    }

    public void setArchivos(List<LoteArchivoParam> archivos) {
        this.archivos = archivos;
    }

    public List<LoteArchivoParam> getArchivos() {
        return archivos;
    }

}
