/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Email;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.PersonaEmail;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaEmailFacade", mappedName = "PersonaEmailFacade")
@Local(PersonaEmailFacade.class)
public class PersonaEmailFacadeImpl extends FacadeImpl<PersonaEmail, PersonaEmailParam> implements PersonaEmailFacade {

    public PersonaEmailFacadeImpl() {
        super(PersonaEmail.class);
    }

    @Override
    public boolean validToCreate(PersonaEmailParam param, boolean validarIdPersona) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        if(param.getIdPersona() != null && param.getIdEmail()!= null) {
            PersonaEmailParam param1 = new PersonaEmailParam();
            param1.setIdPersona(param.getIdPersona());
            param1.setIdEmail(param.getIdEmail());
            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro"));
            }
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(validarIdPersona && persona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }
        
        if(param.getIdEmail() != null) {
            Email email = facades.getEmailFacade().find(param.getIdEmail());

            if(email == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el email"));
            }
        }
        
        if(param.getEmail() != null) {
            facades.getEmailFacade().validToCreate(param.getEmail());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public boolean validToEdit(PersonaEmailParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        PersonaEmailParam param1 = new PersonaEmailParam();
        param1.setIdPersona(param.getIdPersona());
        param1.setIdEmail(param.getIdEmail());
        PersonaEmail pe = findFirst(param1);
        
        if(pe == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe un registro"));
        } else {
            param.setId(pe.getId());
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la persona"));
        }
        
        Email email = facades.getEmailFacade().find(param.getIdEmail());
        
        if(email == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el email"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public PersonaEmail create(PersonaEmailParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        Integer idEmail = param.getIdEmail();
        
        if(param.getEmail() != null) {
            Email email = facades.getEmailFacade().create(param.getEmail(), userInfo);
            idEmail = email == null ? idEmail : email.getId();
        }
        
        PersonaEmail item = new PersonaEmail();
        item.setIdPersona(param.getIdPersona());
        item.setIdEmail(idEmail);
        item.setActivo(param.getActivo());
        
        create(item);
        
        return item;
    }
    
    @Override
    public PersonaEmail edit(PersonaEmailParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        PersonaEmail item = find(param.getId());
        item.setIdPersona(param.getIdPersona());
        item.setIdEmail(param.getIdEmail());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public PersonaEmail obtenerEmailCliente(Integer idCliente) {
        PersonaEmailParam param = new PersonaEmailParam();
        param.setActivo('S');
        param.setIdPersona(idCliente);
        return findFirst(param);
    }
    
    @Override
    public List<PersonaEmail> find(PersonaEmailParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and con.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getContacto() != null && !param.getContacto().trim().isEmpty()) {
            where = String.format("%s and coalesce(con.razon_social, con.nombre || ' ' || con.apellido) ilike '%%%s%%'", where, param.getContacto().trim());
        }
        
        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }
        
        if(param.getIdEmail() != null) {
            where = String.format("%s and pe.id_email = %d", where, param.getIdEmail());
        }
        
        if(param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }
        
        if(param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and pe.activo = '%s'", where, param.getActivo());
        }
        
        if (param.getContactoActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getContactoActivo());
        }
        
        if(param.getIdPersona() != null) {
            where = String.format("%s and pe.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getIdPersonaContacto() != null) {
            where = String.format("%s and pc.id_persona = %d ", where, param.getIdPersonaContacto());
        }
        
        String sql = String.format("select pe.*"
                + " from info.persona_email pe"
                + " left join info.contacto c on c.id_contacto = pe.id_persona"
                + " left join info.persona con on con.id = pe.id_persona"
                + " left join info.persona_contacto pc on pc.id_contacto = pe.id_persona"
                + " left join info.cargo cargo on cargo.id = c.id_cargo"
                + " left join info.tipo_contacto tc on tc.id = c.id_tipo_contacto"
                + " left join info.email e on e.id = pe.id_email"
                + " where %s order by c.activo asc, pc.id_persona asc offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, PersonaEmail.class);
        
        List<PersonaEmail> result = q.getResultList();

        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de contacto email
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(PersonaEmailParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and con.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getContacto() != null && !param.getContacto().trim().isEmpty()) {
            where = String.format("%s and coalesce(con.razon_social, con.nombre || ' ' || con.apellido) ilike '%%%s%%'", where, param.getContacto().trim());
        }
        
        if(param.getIdContacto() != null) {
            where = String.format("%s and c.id_contacto = %d", where, param.getIdContacto());
        }
        
        if(param.getIdEmail() != null) {
            where = String.format("%s and pe.id_email = %d", where, param.getIdEmail());
        }
        
        if(param.getIdCargo() != null) {
            where = String.format("%s and c.id_cargo = %d", where, param.getIdCargo());
        }
        
        if(param.getIdTipoContacto() != null) {
            where = String.format("%s and c.id_tipo_contacto = %d", where, param.getIdTipoContacto());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and pe.activo = '%s'", where, param.getActivo());
        }
        
        if (param.getContactoActivo() != null) {
            where = String.format("%s and c.activo = '%s'", where, param.getContactoActivo());
        }
        
        if(param.getIdPersona() != null) {
            where = String.format("%s and pe.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getIdPersonaContacto() != null) {
            where = String.format("%s and pc.id_persona = %d ", where, param.getIdPersonaContacto());
        }
        
        String sql = String.format("select count(distinct (pe.*))"
                + " from info.persona_email pe"
                + " left join info.contacto c on c.id_contacto = pe.id_persona"
                + " left join info.persona con on con.id = pe.id_persona"
                + " left join info.persona_contacto pc on pc.id_contacto = pe.id_persona"
                + " left join info.email e on e.id = pe.id_email"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
