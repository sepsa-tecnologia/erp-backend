/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class DevolucionDetalleParam extends CommonParam {

    public DevolucionDetalleParam() {
    }

    public DevolucionDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de devolución
     */
    @QueryParam("idDevolucion")
    private Integer idDevolucion;
    
    /**
     * Identificador de motivo
     */
    @QueryParam("idMotivo")
    private Integer idMotivo;
    
    /**
     * Código de motivo
     */
    @QueryParam("codigoMotivo")
    private String codigoMotivo;
    
    /**
     * Identificador de estado de inventario
     */
    @QueryParam("idEstadoInventario")
    private Integer idEstadoInventario;
    
    /**
     * Código de estado de inventario
     */
    @QueryParam("codigoEstadoInventario")
    private String codigoEstadoInventario;
    
    /**
     * Identificador de depósito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Fecha de vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;
    
    /**
     * Cantidad facturada
     */
    @QueryParam("cantidadFacturada")
    private Integer cantidadFacturada;
    
    /**
     * Cantidad devolucion
     */
    @QueryParam("cantidadDevolucion")
    private Integer cantidadDevolucion;
    
    /**
     * Identificador de fatura detalle
     */
    @QueryParam("idFacturaDetalle")
    private Integer idFacturaDetalle;
    
    /**
     * Fecha vencimiento desde
     */
    @QueryParam("fechaVencimientoDesde")
    private Date fechaVencimientoDesde;
    
    /**
     * Fecha vencimiento hasta
     */
    @QueryParam("fechaVencimientoHasta")
    private Date fechaVencimientoHasta;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idDevolucion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de devolución"));
        }
        
        if(isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(cantidadFacturada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad facturada"));
        }
        
        if(isNull(cantidadDevolucion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad de devolución"));
        }
        
        if(isNull(idDepositoLogistico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de depósito logistico"));
        }
        
        if(isNull(idEstadoInventario) && isNullOrEmpty(codigoEstadoInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado de inventario"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de detalle de devolución"));
        }
        
        if(isNull(idDevolucion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de devolución"));
        }
        
        if(isNull(idMotivo) && isNullOrEmpty(codigoMotivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo"));
        }
        
        if(isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de producto"));
        }
        
        if(isNull(cantidadFacturada)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad facturada"));
        }
        
        if(isNull(cantidadDevolucion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad de devolución"));
        }
        
        if(isNull(idDepositoLogistico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de depósito logistico"));
        }
        
        if(isNull(idEstadoInventario) && isNullOrEmpty(codigoEstadoInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado de inventario"));
        }
        
        return !tieneErrores();
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdDevolucion() {
        return idDevolucion;
    }

    public void setIdDevolucion(Integer idDevolucion) {
        this.idDevolucion = idDevolucion;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Integer getCantidadFacturada() {
        return cantidadFacturada;
    }

    public void setCantidadFacturada(Integer cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }

    public Integer getCantidadDevolucion() {
        return cantidadDevolucion;
    }

    public void setCantidadDevolucion(Integer cantidadDevolucion) {
        this.cantidadDevolucion = cantidadDevolucion;
    }

    public Integer getIdFacturaDetalle() {
        return idFacturaDetalle;
    }

    public void setIdFacturaDetalle(Integer idFacturaDetalle) {
        this.idFacturaDetalle = idFacturaDetalle;
    }

    public void setFechaVencimientoHasta(Date fechaVencimientoHasta) {
        this.fechaVencimientoHasta = fechaVencimientoHasta;
    }

    public Date getFechaVencimientoHasta() {
        return fechaVencimientoHasta;
    }

    public void setFechaVencimientoDesde(Date fechaVencimientoDesde) {
        this.fechaVencimientoDesde = fechaVencimientoDesde;
    }

    public Date getFechaVencimientoDesde() {
        return fechaVencimientoDesde;
    }
}
