/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de cobro
 * @author Jonathan D. Bernal Fernández
 */
public class CobroParam extends CommonParam {
    
    public CobroParam() {
    }

    public CobroParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de Factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Identificador de lugar de cobro
     */
    @QueryParam("idLugarCobro")
    private Integer idLugarCobro;
    
    /**
     * Código de lugar de cobro
     */
    @QueryParam("codigoLugarCobro")
    private String codigoLugarCobro;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;
    
    /**
     * Identificador de tipo de cambio
     */
    @QueryParam("idTipoCambio")
    private Integer idTipoCambio;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha Hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Monto cobro
     */
    @QueryParam("montoCobro")
    private BigDecimal montoCobro;
    
    /**
     * Nro de recibo
     */
    @QueryParam("nroRecibo")
    private String nroRecibo;
    
    /**
     * Identificador de talonario
     */
    @QueryParam("idTalonario")
    private Integer idTalonario;
    
    /**
     * Identificador de tipo de transaccion red
     */
    @QueryParam("idTransaccionRed")
    private Integer idTransaccionRed;
    
    /**
     * Fecha de envío
     */
    @QueryParam("fechaEnvio")
    private Date fechaEnvio;
    
    /**
     * Enviado
     */
    @QueryParam("enviado")
    private Character enviado;
    
    /**
     * Digital
     */
    @QueryParam("digital")
    private Character digital;
    
    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;
    
    /**
     * Observación anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;
    
    /**
     * Monto de cobro ficticio
     */
    @QueryParam("montoCobroFicticio")
    private BigDecimal montoCobroFicticio;
    
    /**
     * Monto de cobro ficticio
     */
    @QueryParam("montoCobroGuaranies")
    private BigDecimal montoCobroGuaranies;
    
    /**
     * Transaccion red
     */
    private TransaccionRedParam transaccionRed;
    
    /**
     * Detalle de cobro
     */
    private List<CobroDetalleParam> cobroDetalles;
    
    private List<Integer> idCobros;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idLugarCobro) && isNullOrEmpty(codigoLugarCobro)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de lugar de cobro"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de cobro"));
        }
        
        if(isNull(montoCobro)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de cobro"));
        }
        
       
        if(isNull(idTalonario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de talonario"));
        }
       
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cliente"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado del cobro"));
        }
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            codigoMoneda = "PYG";
        }
        
        if(isNull(enviado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de envío"));
        }
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el cobro es digital o no"));
        }
        
        if(isNullOrEmpty(nroRecibo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de recibo"));
        }
        
        if(isNullOrEmpty(cobroDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar un detalle de cobro"));
        } else {
            for (CobroDetalleParam detalle : cobroDetalles) {
                detalle.setIdCobro(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        if(transaccionRed != null) {
            transaccionRed.setIdEmpresa(idEmpresa);
            transaccionRed.isValidToCreate();
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = new ArrayList<>();
        
        if(transaccionRed != null) {
            result.addAll(transaccionRed.getErrores());
        }
        
        if(cobroDetalles != null) {
            for (CobroDetalleParam detalle : cobroDetalles) {
                result.addAll(detalle.getErrores());
            }
        }
        
        return result;
    }
    
    public boolean isValidToGetPdf() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cobro"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToGetPdfMasivo() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(idCobros)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los identificador de cobro"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cobro"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de cobro"));
        }
        
        if(isNullOrEmpty(nroRecibo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de recibo del cobro"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCancel() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cobro"));
        }
        
        if(isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identiifcador de motivo de anulación"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToUpload() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cobro"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    
    public Integer getIdLugarCobro() {
        return idLugarCobro;
    }

    public void setIdLugarCobro(Integer idLugarCobro) {
        this.idLugarCobro = idLugarCobro;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNroRecibo() {
        return nroRecibo;
    }

    public void setNroRecibo(String nroRecibo) {
        this.nroRecibo = nroRecibo;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdTransaccionRed() {
        return idTransaccionRed;
    }

    public void setIdTransaccionRed(Integer idTransaccionRed) {
        this.idTransaccionRed = idTransaccionRed;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Character getEnviado() {
        return enviado;
    }

    public void setEnviado(Character enviado) {
        this.enviado = enviado;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setCobroDetalles(List<CobroDetalleParam> cobroDetalles) {
        this.cobroDetalles = cobroDetalles;
    }

    public List<CobroDetalleParam> getCobroDetalles() {
        return cobroDetalles;
    }

    public void setTransaccionRed(TransaccionRedParam transaccionRed) {
        this.transaccionRed = transaccionRed;
    }

    public TransaccionRedParam getTransaccionRed() {
        return transaccionRed;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getDigital() {
        return digital;
    }

    public void setCodigoLugarCobro(String codigoLugarCobro) {
        this.codigoLugarCobro = codigoLugarCobro;
    }

    public String getCodigoLugarCobro() {
        return codigoLugarCobro;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public BigDecimal getMontoCobroFicticio() {
        return montoCobroFicticio;
    }

    public void setMontoCobroFicticio(BigDecimal montoCobroFicticio) {
        this.montoCobroFicticio = montoCobroFicticio;
    }

    public BigDecimal getMontoCobroGuaranies() {
        return montoCobroGuaranies;
    }

    public void setMontoCobroGuaranies(BigDecimal montoCobroGuaranies) {
        this.montoCobroGuaranies = montoCobroGuaranies;
    }

    public List<Integer> getIdCobros() {
        return idCobros;
    }

    public void setIdCobros(List<Integer> idCobros) {
        this.idCobros = idCobros;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }
    
    
}
