/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.inventario.ControlInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.DepositoLogistico;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.ControlInventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ControlInventarioDetalleFacade", mappedName = "ControlInventarioDetalleFacade")
@javax.ejb.Local(ControlInventarioDetalleFacade.class)
public class ControlInventarioDetalleFacadeImp extends FacadeImpl<ControlInventarioDetalle, ControlInventarioDetalleParam> implements ControlInventarioDetalleFacade {

    public ControlInventarioDetalleFacadeImp() {
        super(ControlInventarioDetalle.class);
    }

    @Override
    public Boolean validToCreate(ControlInventarioDetalleParam param, boolean validarControlInventario) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        ControlInventarioParam ciparam = new ControlInventarioParam();
        ciparam.setId(param.getIdControlInventario());
        Long cisize = validarControlInventario
                ? facades.getControlInventarioFacade().findSize(ciparam)
                : 0;
        if (validarControlInventario && cisize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el control de inventario"));
        }

        DepositoLogisticoParam dlparam = new DepositoLogisticoParam();
        dlparam.setId(param.getIdDepositoLogistico());
        Long dlsize = facades.getDepositoLogisticoFacade().findSize(dlparam);
        if (dlsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el depósito logístico"));
        }

        ProductoParam pparam = new ProductoParam();
        pparam.setId(param.getIdProducto());
        Long psize = facades.getProductoFacade().findSize(pparam);
        if (psize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el producto"));
        }

        if (param.getFechaInicio() != null && param.getFechaFin() != null) {
            if (param.getFechaInicio().compareTo(param.getFechaFin()) > 0) {
                param.addError(MensajePojo.createInstance().descripcion("Se debe indicar un rango de fecha válido"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(ControlInventarioDetalleParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        ControlInventarioDetalleParam cidparam = new ControlInventarioDetalleParam();
        cidparam.setId(param.getId());
        Long cidsize = findSize(cidparam);
        if (cidsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el control de inventario detalle"));
        }

        if (param.getFechaInicio() != null && param.getFechaFin() != null) {
            if (param.getFechaInicio().compareTo(param.getFechaFin()) > 0) {
                param.addError(MensajePojo.createInstance().descripcion("Se debe indicar un rango de fecha válido"));
            }
        }

        return !param.tieneErrores();
    }

    @Override
    public ControlInventarioDetalle create(ControlInventarioDetalleParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        ControlInventarioDetalle result = new ControlInventarioDetalle();
        result.setIdControlInventario(param.getIdControlInventario());
        result.setIdDepositoLogistico(param.getIdDepositoLogistico());
        result.setIdProducto(param.getIdProducto());
        result.setCantidadInicial(param.getCantidadInicial());
        result.setCantidadFinal(param.getCantidadFinal());
        result.setFechaVencimiento(param.getFechaVencimiento());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());

        create(result);

        return result;
    }

    @Override
    public ControlInventarioDetalle edit(ControlInventarioDetalleParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        ControlInventarioDetalle result = find(param.getId());
        result.setCantidadFinal(param.getCantidadFinal());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());
        result.setObservacion(param.getObservacion());

        edit(result);

        return result;
    }
    
    @Override
    public List<ControlInventarioDetalle> findByIdControlInventario(Integer idControlInventario) {
        ControlInventarioDetalleParam param = new ControlInventarioDetalleParam();
        param.setIdControlInventario(idControlInventario);
        param.setPageSize(1000);
        param.setFirstResult(0);
        return find(param);
    }
    
    @Override
    public List<ControlInventarioDetallePojo> findByIdControlInventarioPojo(Integer idControlInventario) {
        ControlInventarioDetalleParam param = new ControlInventarioDetalleParam();
        param.setIdControlInventario(idControlInventario);
        param.setPageSize(1000);
        param.setFirstResult(0);
        return findPojo(param);
    }

    @Override
    public List<ControlInventarioDetallePojo> findPojo(ControlInventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(ControlInventarioDetallePojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idControlInventario"),
                        getPath("root").get("idDepositoLogistico"),
                        getPath("depositoLogistico").get("codigo"),
                        getPath("depositoLogistico").get("idLocal"),
                        getPath("local").get("descripcion"),
                        getPath("root").get("idProducto"),
                        getPath("producto").get("descripcion"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("codigoInterno"),
                        getPath("root").get("cantidadInicial"),
                        getPath("root").get("cantidadFinal"),
                        getPath("root").get("observacion"),
                        getPath("root").get("fechaVencimiento"),
                        getPath("root").get("fechaInicio"),
                        getPath("root").get("fechaFin"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<ControlInventarioDetalle> find(ControlInventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(ControlInventarioDetalle.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, ControlInventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<ControlInventarioDetalle> root = cq.from(ControlInventarioDetalle.class);
        Join<ControlInventarioDetalle, DepositoLogistico> depositoLogistico = root.join("depositoLogistico");
        Join<DepositoLogistico, Local> local = depositoLogistico.join("local");
        Join<ControlInventarioDetalle, Producto> producto = root.join("producto");

        find.addPath("root", root);
        find.addPath("depositoLogistico", depositoLogistico);
        find.addPath("local", local);
        find.addPath("producto", producto);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdControlInventario() != null) {
            predList.add(qb.equal(root.get("idControlInventario"), param.getIdControlInventario()));
        }

        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(root.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }

        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ControlInventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ControlInventarioDetalle> root = cq.from(ControlInventarioDetalle.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdControlInventario() != null) {
            predList.add(qb.equal(root.get("idControlInventario"), param.getIdControlInventario()));
        }

        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(root.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }

        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;

        return result;
    }
}
