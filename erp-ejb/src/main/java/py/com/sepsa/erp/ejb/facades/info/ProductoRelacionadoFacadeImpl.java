/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Menu;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.ProductoRelacionado;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoRelacionadoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "ProductoRelacionadoFacade", mappedName = "ProductoRelacionadoFacade")
@Local(ProductoRelacionadoFacade.class)
public class ProductoRelacionadoFacadeImpl extends FacadeImpl<ProductoRelacionado, ProductoRelacionadoParam> implements ProductoRelacionadoFacade {

    public ProductoRelacionadoFacadeImpl() {
        super(ProductoRelacionado.class);
    }

    @Override
    public Boolean validToCreate(ProductoRelacionadoParam param, boolean validarIdMenu) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ProductoRelacionadoParam param1 = new ProductoRelacionadoParam();
        param1.setIdProducto(param.getIdProducto());
        param1.setIdProductoRelacionado(param.getIdProductoRelacionado());
        param1.setActivo(param.getActivo());
        
        
        ProductoRelacionado item = findFirst(param1);
        
        if(item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }
        
        
        Producto producto = facades.getProductoFacade().find(param.getIdEmpresa(),
                param.getIdProducto(),
                null);
        
        if(validarIdMenu && producto == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el producto"));
        } else if(producto != null) {
            param.setIdProducto(producto.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(ProductoRelacionadoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ProductoRelacionadoParam param1 = new ProductoRelacionadoParam();
        param1.setIdProducto(param.getIdProducto());
        param1.setIdProductoRelacionado(param.getIdProductoRelacionado());
       
        
        ProductoRelacionado item = findFirst(param1);
        
        if(item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un registro"));
        } else {
            param.setId(item.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public ProductoRelacionado create(ProductoRelacionadoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        ProductoRelacionado item = new ProductoRelacionado();
        item.setIdProducto(param.getIdProducto());
        item.setIdProductoRelacionado(param.getIdProductoRelacionado());
        item.setActivo(param.getActivo());
        item.setPorcentajeRelacionado(param.getPorcentajeRelacionado());
        item.setFechaInsercion(Calendar.getInstance().getTime());
        create(item);
        
        return item;
    }
    
    @Override
    public ProductoRelacionado edit(ProductoRelacionadoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ProductoRelacionado item = find(param.getId());
        item.setIdProducto(param.getIdProducto());
        item.setIdProductoRelacionado(param.getIdProductoRelacionado());
        item.setPorcentajeRelacionado(param.getPorcentajeRelacionado());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<ProductoRelacionado> find(ProductoRelacionadoParam param) {

        String where = "true";

        
        if(!isNull(param.getIdProducto())) {
            where = String.format("%s and pr.id_producto = %d", where, param.getIdProducto());
        }
        
        if(!isNull(param.getIdProductoRelacionado())) {
            where = String.format("%s and pr.id_producto_relacionado = %d", where, param.getIdProductoRelacionado());
        }
        
        if(!isNull(param.getActivo())) {
            where = String.format("%s and pr.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select pr.*"
                + " from info.producto_relacionado pr"
                + " left join info.producto p on p.id = pr.id_producto"
                + " left join info.producto p2 on p2.id = pr.id_producto_relacionado"
                + " where %s order by pr.activo, pr.id_producto, pr.id_producto_relacionado offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, ProductoRelacionado.class);
        
        List<ProductoRelacionado> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(ProductoRelacionadoParam param) {

        String where = "true";
        
         if(!isNull(param.getIdProducto())) {
            where = String.format("%s and pr.id_producto = %d", where, param.getIdProducto());
        }
        
        if(!isNull(param.getIdProductoRelacionado())) {
            where = String.format("%s and pr.id_producto_relacionado = %d", where, param.getIdProductoRelacionado());
        }
         
        if(!isNull(param.getActivo())) {
            where = String.format("%s and pr.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(pr.*)"
                + " from info.producto_relacionado pr"
                + " left join info.producto p on p.id = pr.id_producto"
                + " left join info.producto p2 on p2.id = pr.id_producto_relacionado"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<ProductoRelacionado> findRelacionados(ProductoRelacionadoParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select coalesce(pr.id, 0) as id, coalesce(pr.id_producto, %d) as id_producto, p.id as id_producto_relacionado, coalesce(pr.activo, 'N') as activo, pr.porcentaje_relacionado\n"
                + "from info.producto p\n"
                + "join info.producto_relacionado pr on pr.id_producto_relacionado = p.id and pr.activo = 'S' and pr.id_producto = %d\n"
                + "where %s limit %d offset %d", param.getIdProducto(), param.getIdProducto(), where, param.getPageSize(), param.getFirstResult());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<ProductoRelacionado> result = Lists.empty();
        
        for (Object[] objects : list) {
            int i = 0;
            ProductoRelacionado pr = new ProductoRelacionado();
            pr.setId((Integer)objects[i++]);
            pr.setIdProducto((Integer)objects[i++]);
            pr.setIdProductoRelacionado((Integer)objects[i++]);
            pr.setActivo((Character)objects[i++]);
            pr.setPorcentajeRelacionado((new BigDecimal(objects[i++].toString())));
            pr.setProducto(facades.getProductoFacade().find(pr.getIdProducto()));
            pr.setProductoRelacionado(facades.getProductoFacade().find(pr.getIdProductoRelacionado()));
            result.add(pr);
        }
        
        return result;
    }

    @Override
    public Long findRelacionadosSize(ProductoRelacionadoParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select count(p)\n"
                + "from info.producto p\n"
                + "join info.producto_relacionado pr on pr.id_producto = p.id and pr.activo = 'S' and pr.id_producto = %d\n"
                + "where %s", param.getIdProducto(), where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
