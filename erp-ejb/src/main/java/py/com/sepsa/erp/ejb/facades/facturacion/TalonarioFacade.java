/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface TalonarioFacade extends Facade<Talonario, TalonarioParam, UserInfoImpl> {

    public Boolean nroDocEnRango(TalonarioPojo talonario, String nroDoc);

    public void inactivarTalonarios(Integer idEmpresa, Integer idTipoDocumento, Integer idEncargado, Integer noIdTalonario);

    public Date ultimaFechaDoc(TalonarioPojo talonario, Integer idTipoDocumento);

    public String sigNumRecibo(TalonarioPojo talonario);

    public String sigNumFactura(TalonarioPojo talonario);
    
    public String sigNumAutoFactura(TalonarioPojo talonario);

    public String sigNumNotaCredito(TalonarioPojo talonario);
    
    public String sigNumNotaDebito(TalonarioPojo talonario);
    
    public String sigNumNotaRemision(TalonarioPojo talonario);
    
    public TalonarioPojo getTalonario(TalonarioParam param);

    public Integer cantidadHojas(TalonarioParam param);

    public Boolean fechaDocumentoEnRango(TalonarioPojo talonario, Date fecha);

    public Boolean fechaDocumentoEnRango(Date fechaInicio, Date fechaVencimiento, Date fechaDocumento);

    public List<TalonarioPojo> getTalonarios(TalonarioParam param);
    
}