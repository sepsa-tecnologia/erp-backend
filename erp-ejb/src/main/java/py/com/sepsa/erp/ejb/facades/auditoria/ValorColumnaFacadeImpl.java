/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.auditoria.AuditoriaDetalle;
import py.com.sepsa.erp.ejb.entities.auditoria.ValorColumna;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.AuditoriaDetalleParam;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.ValorColumnaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "ValorColumnaFacade", mappedName = "ValorColumnaFacade")
@Local(ValorColumnaFacade.class)
public class ValorColumnaFacadeImpl extends FacadeImpl<ValorColumna, ValorColumnaParam> implements ValorColumnaFacade {

    public ValorColumnaFacadeImpl() {
        super(ValorColumna.class);
    }

    public Boolean validToCreate(ValorColumnaParam param, Boolean validarAuditoriaDetalle) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        AuditoriaDetalle auditoriaDetalle = facades.getAuditoriaDetalleFacade()
                .find(param.getIdAuditoriaDetalle());
        
        if(auditoriaDetalle == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la auditoria detalle"));
        } else {
            param.setIdAuditoria(auditoriaDetalle.getIdAuditoria());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public ValorColumna create(ValorColumnaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        ValorColumna result = new ValorColumna();
        result.setIdAuditoria(param.getIdAuditoria());
        result.setIdAuditoriaDetalle(param.getIdAuditoriaDetalle());
        result.setNombreColumna(param.getNombreColumna());
        result.setValorNuevo(param.getValorNuevo());
        result.setValorAnterior(param.getValorAnterior());
        create(result);
        
        return result;
    }

    @Override
    public AuditoriaDetalle nuevaInstancia(String codigo, Map<String, String> map, UserInfoImpl userInfo) {
        
        AuditoriaDetalle auditoriaDetalle = facades.getAuditoriaDetalleFacade().nuevaInstancia(codigo, userInfo);
        
        for (Map.Entry<String, String> entry : map.entrySet()) {
            ValorColumna item = new ValorColumna();
            item.setIdAuditoria(auditoriaDetalle.getIdAuditoria());
            item.setIdAuditoriaDetalle(auditoriaDetalle.getId());
            item.setNombreColumna(entry.getKey());
            item.setValorAnterior(null);
            item.setValorNuevo(entry.getValue() != null && entry.getValue().trim().equals("null") ? null : entry.getValue());
            create(item);
        }
        
        return auditoriaDetalle;
    }

    @Override
    public AuditoriaDetalle editarInstancia(Integer idAuditoria, String codigo, Map<String, String> map, UserInfoImpl userInfo) {
        
        Integer idAuditoriaDetalleAnterior = 0;
        
        if(idAuditoria != null) {
            AuditoriaDetalleParam adparam = new AuditoriaDetalleParam();
            adparam.setIdAuditoria(idAuditoria);
            AuditoriaDetalle auditoriaDetalle = facades.getAuditoriaDetalleFacade().findFirst(adparam);
            idAuditoriaDetalleAnterior = auditoriaDetalle.getId();
        }
        
        AuditoriaDetalle auditoriaDetalle = facades.getAuditoriaDetalleFacade().editarInstancia(idAuditoria, codigo, userInfo);
        
        for (Map.Entry<String, String> entry : map.entrySet()) {
            
            ValorColumnaParam vcparam = new ValorColumnaParam();
            vcparam.setIdAuditoriaDetalle(idAuditoriaDetalleAnterior);
            vcparam.setNombreColumna(entry.getKey());
            ValorColumna vc = findFirst(vcparam);
            
            ValorColumna vcparam1 = new ValorColumna();
            vcparam1.setIdAuditoria(auditoriaDetalle.getIdAuditoria());
            vcparam1.setIdAuditoriaDetalle(auditoriaDetalle.getId());
            vcparam1.setNombreColumna(entry.getKey());
            vcparam1.setValorAnterior(vc == null ? null : vc.getValorNuevo());
            vcparam1.setValorNuevo(entry.getValue() != null && entry.getValue().trim().equals("null") ? null : entry.getValue());
            create(vcparam1);
        }
        
        return auditoriaDetalle;
    }
    
    @Override
    public List<ValorColumna> find(ValorColumnaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(ValorColumna.class);
        Root<ValorColumna> root = cq.from(ValorColumna.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdAuditoriaDetalle() != null) {
            predList.add(qb.equal(root.get("idAuditoriaDetalle"), param.getIdAuditoriaDetalle()));
        }
        
        if (param.getNombreColumna() != null && !param.getNombreColumna().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nombreColumna"), param.getNombreColumna()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<ValorColumna> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(ValorColumnaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ValorColumna> root = cq.from(ValorColumna.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdAuditoriaDetalle() != null) {
            predList.add(qb.equal(root.get("idAuditoriaDetalle"), param.getIdAuditoriaDetalle()));
        }
        
        if (param.getNombreColumna() != null && !param.getNombreColumna().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nombreColumna"), param.getNombreColumna()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
