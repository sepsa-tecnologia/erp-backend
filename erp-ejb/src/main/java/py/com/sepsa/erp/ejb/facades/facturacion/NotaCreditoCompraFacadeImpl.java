/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoCompraParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NotaCreditoCompraFacade", mappedName = "NotaCreditoCompraFacade")
@Local(NotaCreditoCompraFacade.class)
public class NotaCreditoCompraFacadeImpl extends FacadeImpl<NotaCreditoCompra, NotaCreditoCompraParam> implements NotaCreditoCompraFacade {

    public NotaCreditoCompraFacadeImpl() {
        super(NotaCreditoCompra.class);
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return bandera
     */
    public Boolean validToEdit(NotaCreditoCompraParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        NotaCreditoCompra ncc = facades
                .getNotaCreditoCompraFacade()
                .find(param.getId());
        
        if(ncc == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la nota de crédito de compra"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToCreate(NotaCreditoCompraParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda(), param.getCodigoMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
        }
        
        Persona persona = param.getIdPersona() == null
                ? null
                : facades.getPersonaFacade().find(param.getIdPersona());
        
        if(param.getIdPersona() != null && persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
        
        if(param.getIdMotivoEmisionInterno() != null) {
            
            MotivoEmisionInterno motivoEmisionInterno = facades.getMotivoEmisionInternoFacade().find(param.getIdMotivoEmisionInterno());
            
            if(isNull(motivoEmisionInterno)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el motivo de emisión interno"));
            } else {
                param.setIdMotivoEmision(motivoEmisionInterno.getIdMotivoEmision());
            }
        }
        
        if (!facades.getTalonarioFacade().fechaDocumentoEnRango(null, param.getFechaVencimientoTimbrado(), param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la nota de crédito debe estar en el rango de vigencia del timbrado"));
        }
        
        for (NotaCreditoCompraDetalleParam detalle : param.getNotaCreditoCompraDetalles()) {
            facades.getNotaCreditoCompraDetalleFacade().validToCreate(detalle, false);
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToCancel(NotaCreditoCompraParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), null, "NOTA_CREDITO_COMPRA");
        
        if(motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        }
        
        NotaCreditoCompra notaCredito = facades.getNotaCreditoCompraFacade().find(param.getId());

        if (notaCredito == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la nota de crédito de compra"));
        } else {

            if (Objects.equals(notaCredito.getAnulado(), 'S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La nota de crédito de compra ya se encuentra anulada"));
            }
        }

        return !param.tieneErrores();
    }
    
    public Boolean validToDelete(NotaCreditoCompraParam param) {

        if (!param.isValidToDelete()) {
            return Boolean.FALSE;
        }

        Long size = facades.getNotaCreditoCompraFacade().findSize(param);

        if (size <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existen notas de crédito de compra asociadas"));
        }

        return !param.tieneErrores();
    }
    
    @Override
    public NotaCreditoCompra create(NotaCreditoCompraParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        NotaCreditoCompra nc = new NotaCreditoCompra();
        nc.setIdEmpresa(userInfo.getIdEmpresa());
        nc.setIdPersona(param.getIdPersona());
        nc.setAnulado(param.getAnulado());
        nc.setCdc(param.getCdc());
        nc.setDigital(param.getDigital());
        nc.setFechaInsercion(Calendar.getInstance().getTime());
        nc.setFecha(param.getFecha());
        nc.setIdMoneda(param.getIdMoneda());
        nc.setNroTimbrado(param.getNroTimbrado());
        nc.setIdMotivoEmision(param.getIdMotivoEmision());
        nc.setIdMotivoEmisionInterno(param.getIdMotivoEmisionInterno());
        nc.setFechaVencimientoTimbrado(param.getFechaVencimientoTimbrado());
        nc.setMontoImponible10(param.getMontoImponible10().stripTrailingZeros());
        nc.setMontoImponible5(param.getMontoImponible5().stripTrailingZeros());
        nc.setMontoImponibleTotal(param.getMontoImponibleTotal().stripTrailingZeros());
        nc.setMontoIva10(param.getMontoIva10().stripTrailingZeros());
        nc.setMontoIva5(param.getMontoIva5().stripTrailingZeros());
        nc.setMontoIvaTotal(param.getMontoIvaTotal().stripTrailingZeros());
        nc.setMontoTotal10(param.getMontoTotal10().stripTrailingZeros());
        nc.setMontoTotal5(param.getMontoTotal5().stripTrailingZeros());
        nc.setMontoTotalExento(param.getMontoTotalExento().stripTrailingZeros());
        nc.setMontoTotalNotaCredito(param.getMontoTotalNotaCredito().stripTrailingZeros());
        nc.setNroNotaCredito(param.getNroNotaCredito());
        nc.setRazonSocial(param.getRazonSocial());
        nc.setRuc(param.getRuc());
        
        create(nc);
        
        for (NotaCreditoCompraDetalleParam detalle : param.getNotaCreditoCompraDetalles()) {
            detalle.setIdNotaCreditoCompra(nc.getId());
            facades.getNotaCreditoCompraDetalleFacade().create(detalle, userInfo);
        }
        
        return nc;
    }
    
    @Override
    public NotaCreditoCompra anular(NotaCreditoCompraParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        NotaCreditoCompra notaCredito = find(param.getId());
        notaCredito.setAnulado('S');
        notaCredito.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
        notaCredito.setObservacionAnulacion(param.getObservacionAnulacion());

        edit(notaCredito);

        NotaCreditoCompraDetalleParam param1 = new NotaCreditoCompraDetalleParam();
        param1.setIdNotaCreditoCompra(param.getId());
        param1.isValidToList();

        List<NotaCreditoCompraDetalle> list = facades.getNotaCreditoCompraDetalleFacade().find(param1);

        for (NotaCreditoCompraDetalle item : list) {

            if(item.getIdFacturaCompra() != null) {
                facades.getFacturaCompraFacade().actualizarSaldoFactura(
                        item.getIdFacturaCompra(),
                        item.getMontoTotal().negate());
            }
        }
        
        return notaCredito;
    }
    
    @Override
    public Boolean delete(NotaCreditoCompraParam param, UserInfoImpl userInfo) {

        if (!validToDelete(param)) {
            return Boolean.FALSE;
        }
        
        param.setIdEmpresa(userInfo.getIdEmpresa());
        param.setFirstResult(0);
        param.setPageSize(100);
        List<NotaCreditoCompra> list = find(param);
        
        for (NotaCreditoCompra item : list) {
            NotaCreditoCompraDetalleParam param1 = new NotaCreditoCompraDetalleParam();
            param1.setIdNotaCreditoCompra(item.getId());
            param1.setFirstResult(0);
            param1.setPageSize(100);

            List<NotaCreditoCompraDetalle> list1 = facades.getNotaCreditoCompraDetalleFacade().find(param1);

            for (NotaCreditoCompraDetalle item1 : list1) {

                if(item1.getIdFacturaCompra() != null) {

                    facades.getNotaCreditoCompraDetalleFacade().remove(item1);

                    facades.getFacturaCompraFacade().actualizarSaldoFactura(
                            item1.getIdFacturaCompra(),
                            item1.getMontoTotal().negate());
                }
            }

            NotaCreditoCompra notaCredito = find(item.getId());

            remove(notaCredito);
        }
        
        return Boolean.TRUE;
    }
    
    @Override
    public List<NotaCreditoCompra> find(NotaCreditoCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(NotaCredito.class);
        Root<NotaCreditoCompra> root = cq.from(NotaCreditoCompra.class);
        Join<NotaCreditoCompra, Empresa> empresa = root.join("empresa");
        Join<NotaCreditoCompra, NotaCreditoCompraDetalle> detalles = root.join("notaCreditoCompraDetalles", JoinType.LEFT);
        
        cq.select(root).distinct(true);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(detalles.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }
        
        if (param.getIdMotivoEmision() != null) {
            predList.add(qb.equal(root.get("idMotivoEmision"), param.getIdMotivoEmision()));
        }
        
        if (param.getIdMotivoEmisionInterno() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionInterno"), param.getIdMotivoEmisionInterno()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroNotaCredito() != null && !param.getNroNotaCredito().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroNotaCredito")),
                    String.format("%%%s%%", param.getNroNotaCredito().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<NotaCreditoCompra> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(NotaCreditoCompraParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NotaCreditoCompra> root = cq.from(NotaCreditoCompra.class);
        Join<NotaCreditoCompra, Empresa> empresa = root.join("empresa");
        Join<NotaCreditoCompra, NotaCreditoCompraDetalle> detalles = root.join("notaCreditoCompraDetalles", JoinType.LEFT);
        
        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdFacturaCompra() != null) {
            predList.add(qb.equal(detalles.get("idFacturaCompra"), param.getIdFacturaCompra()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getIdPersona() != null) {
            predList.add(qb.equal(root.get("idPersona"), param.getIdPersona()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }
        
        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }
        
        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }
        
        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getIdMotivoEmision() != null) {
            predList.add(qb.equal(root.get("idMotivoEmision"), param.getIdMotivoEmision()));
        }
        
        if (param.getIdMotivoEmisionInterno() != null) {
            predList.add(qb.equal(root.get("idMotivoEmisionInterno"), param.getIdMotivoEmisionInterno()));
        }
        
        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroNotaCredito() != null && !param.getNroNotaCredito().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroNotaCredito")),
                    String.format("%%%s%%", param.getNroNotaCredito().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<RegistroComprobantePojo> generarComprobanteCompra(ReporteComprobanteParam param) {
        
        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and ncc.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and ncc.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and ncc.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select distinct ncc.ruc, ncc.razon_social, "
                + "110 as tipo_comprobante, ncc.fecha, ncc.nro_timbrado, "
                + "ncc.nro_nota_credito, ncc.monto_total_10, ncc.monto_total_5, "
                + "ncc.monto_total_exento, ncc.monto_total_nota_credito, "
                + "case when m.codigo in ('PYG','Gs.') then 'N' else 'S' end, "
                + "fc.nro_factura, fc.nro_timbrado as timbrado_factura "
                + "from facturacion.nota_credito_compra ncc "
                + "left join facturacion.nota_credito_compra_detalle nccd on nccd.id_nota_credito_compra = ncc.id "
                + "left join facturacion.factura_compra fc on fc.id = nccd.id_factura_compra "
                + "left join comercial.moneda m on m.id = fc.id_moneda "
                + "where %s offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        Query q = getEntityManager().createNativeQuery(sql);

        List<Object[]> list = q.getResultList();

        List<RegistroComprobantePojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            RegistroComprobantePojo pojo = new RegistroComprobantePojo();
            pojo.setIdentificacionVendedor((String) objects[i++]);
            pojo.setRazonSocialVendedor((String) objects[i++]);
            pojo.setTipoComprobante((Integer) objects[i++]);
            pojo.setFechaEmisionComprobante((Date) objects[i++]);
            pojo.setTimbradoComprobanteAsociado(new Integer((String) objects[i++]));
            pojo.setNumeroComprobante((String) objects[i++]);
            pojo.setMontoIva10(new BigDecimal(objects[i++] + ""));
            pojo.setMontoIva5(new BigDecimal(objects[i++] + ""));
            pojo.setMontoExento(new BigDecimal(objects[i++] + ""));
            pojo.setMontoTotal(new BigDecimal(objects[i++] + ""));
            pojo.setMonedaExtrangera(((String)objects[i++]).charAt(0));
            pojo.setNumeroComprobanteAsociado((String) objects[i++]);
            pojo.setTimbradoComprobanteAsociado(new Integer((String) objects[i++]));
            result.add(pojo);
        }

        return result;
    }

    @Override
    public Integer generarComprobanteCompraSize(ReporteComprobanteParam param) {
        
        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and ncc.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getFechaDesde() != null) {
            where = String.format(" %s and ncc.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
        }
        
        if(param.getFechaHasta() != null) {
            where = String.format(" %s and ncc.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
        }
        
        String sql = String.format("select count(distinct(ncc.id, fc.id)) "
                + "from facturacion.nota_credito_compra ncc "
                + "left join facturacion.nota_credito_compra_detalle nccd on nccd.id_nota_credito_compra = ncc.id "
                + "left join facturacion.factura_compra fc on fc.id = nccd.id_factura_compra "
                + "where %s ", where);
        
        Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Integer result = object == null
                ? 0
                : ((Number)object).intValue();

        return result;
    }
}
