/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoTransaccion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoTransaccionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoTransaccionFacade", mappedName = "TipoTransaccionFacade")
@Local(TipoTransaccionFacade.class)
public class TipoTransaccionFacadeImpl extends FacadeImpl<TipoTransaccion, TipoTransaccionParam> implements TipoTransaccionFacade {

    public TipoTransaccionFacadeImpl() {
        super(TipoTransaccion.class);
    }

    public Boolean validToCreate(TipoTransaccionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoTransaccionParam param1 = new TipoTransaccionParam();
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(TipoTransaccionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoTransaccion tipoTransaccion = find(param.getId());
        
        if(tipoTransaccion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de transacción"));
        } else if(!tipoTransaccion.getCodigo().equals(param.getCodigo().trim())) {
            
            TipoTransaccionParam param1 = new TipoTransaccionParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public TipoTransaccion create(TipoTransaccionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoTransaccion tipoTransaccion = new TipoTransaccion();
        tipoTransaccion.setCodigo(param.getCodigo().trim());
        tipoTransaccion.setDescripcion(param.getDescripcion().trim());
        create(tipoTransaccion);
        
        return tipoTransaccion;
    }
    
    @Override
    public TipoTransaccion edit(TipoTransaccionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoTransaccion tipoTransaccion = find(param.getId());
        tipoTransaccion.setCodigo(param.getCodigo().trim());
        tipoTransaccion.setDescripcion(param.getDescripcion().trim());
        edit(tipoTransaccion);
        
        return tipoTransaccion;
    }
    
    @Override
    public TipoTransaccion find(Integer id, String codigo) {
        TipoTransaccionParam param = new TipoTransaccionParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoTransaccion> find(TipoTransaccionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(TipoTransaccion.class);
        Root<TipoTransaccion> root = cq.from(TipoTransaccion.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<TipoTransaccion> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoTransaccionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoTransaccion> root = cq.from(TipoTransaccion.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
