/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class DetalleProcesamientoParam extends CommonParam {

    public DetalleProcesamientoParam() {
    }

    public DetalleProcesamientoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Código de resultado
     */
    @QueryParam("codigoResultado")
    private Integer codigoResultado;
    
    /**
     * Mensaje de resultado
     */
    @QueryParam("mensajeResultado")
    private String mensajeResultado;
    
    /**
     * Identificador de procesamiento
     */
    @QueryParam("idProcesamiento")
    private Integer idProcesamiento;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(codigoResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de resultado"));
        }
        
        if(isNull(mensajeResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el mensaje de resultado"));
        }
        
        if(isNull(idProcesamiento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de detalle de procesamiento"));
        }
        
        if(isNull(codigoResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de resultado"));
        }
        
        if(isNull(mensajeResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el mensaje de resultado"));
        }
        
        if(isNull(idProcesamiento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getCodigoResultado() {
        return codigoResultado;
    }

    public void setCodigoResultado(Integer codigoResultado) {
        this.codigoResultado = codigoResultado;
    }

    public String getMensajeResultado() {
        return mensajeResultado;
    }

    public void setMensajeResultado(String mensajeResultado) {
        this.mensajeResultado = mensajeResultado;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }
}
