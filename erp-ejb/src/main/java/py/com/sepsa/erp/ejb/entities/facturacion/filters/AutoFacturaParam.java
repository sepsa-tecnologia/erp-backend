/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import org.jboss.resteasy.annotations.providers.multipart.PartType;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de factura
 *
 * @author Jonathan D. Bernal Fernández
 */
public class AutoFacturaParam extends CommonParam {

    public AutoFacturaParam() {
    }

    public AutoFacturaParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de talonario
     */
    @QueryParam("idTalonario")
    private Integer idTalonario;

    /**
     * Timbrado
     */
    @QueryParam("timbrado")
    private String timbrado;

    /**
     * Establecimiento
     */
    @QueryParam("establecimiento")
    private String establecimiento;

    /**
     * Punto de expedición
     */
    @QueryParam("puntoExpedicion")
    private String puntoExpedicion;

    /**
     * Identificador de local de talonario
     */
    @QueryParam("idLocalTalonario")
    private Integer idLocalTalonario;

    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;

    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;

    /**
     * Identificador de naturaleza del cliente
     */
    @QueryParam("idNaturalezaVendedor")
    private Integer idNaturalezaVendedor;

    /**
     * Código de naturaleza del cliente
     */
    @QueryParam("codigoNaturalezaVendedor")
    private String codigoNaturalezaVendedor;

    /**
     * Identificador de tipo de cambio
     */
    @QueryParam("idTipoCambio")
    private Integer idTipoCambio;

    /**
     * Identificador de tipo de factura
     */
    @QueryParam("idTipoFactura")
    private Integer idTipoFactura;

    /**
     * Código de tipo de factura
     */
    @QueryParam("codigoTipoFactura")
    private String codigoTipoFactura;

    /**
     * Nro de factura
     */
    @QueryParam("nroAutofactura")
    private String nroAutofactura;

    /**
     * Nro de factura eq
     */
    @QueryParam("nroFacturaEq")
    private String nroFacturaEq;

    /**
     * Año y Mes
     */
    @QueryParam("anhoMes")
    private String anhoMes;

    /**
     * Fecha factura
     */
    @QueryParam("fecha")
    private Date fecha;

    /**
     * Fecha vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;

        /**
     * Razon social
     */
    @QueryParam("nroDocumentoVendedor")
    private String nroDocumentoVendedor;
    
    /**
     * Razon social
     */
    @QueryParam("nombreVendedor")
    private String nombreVendedor;

    /**
     * Dirección
     */
    @QueryParam("direccion")
    private String direccion;

    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Razon Social
     */
    @QueryParam("razonSocial")
    private String razonSocial;

    /**
     * Email
     */
    @QueryParam("email")
    private String email;

    /**
     * Telefono
     */
    @QueryParam("telefono")
    private String telefono;

    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;

    /**
     * Fecha Hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;

    /**
     * Fecha inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha inserción Hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Código de resultado
     */
    @QueryParam("codigoResultado")
    private String codigoResultado;

    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;

    /**
     * Cobrado
     */
    @QueryParam("cobrado")
    private Character cobrado;

    /**
     * Archivo EDI
     */
    @QueryParam("archivoEdi")
    private Character archivoEdi;

    /**
     * Archivo EDI
     */
    @QueryParam("archivoSet")
    private Character archivoSet;

    /**
     * Generado EDI
     */
    @QueryParam("generadoEdi")
    private Character generadoEdi;

    /**
     * Generado SET
     */
    @QueryParam("generadoSet")
    private Character generadoSet;

    /**
     * Estado sincronizado
     */
    @QueryParam("estadoSincronizado")
    private Character estadoSincronizado;

    /**
     * Impreso
     */
    @QueryParam("impreso")
    private Character impreso;

    /**
     * Entregado
     */
    @QueryParam("entregado")
    private Character entregado;

    /**
     * Fecha entrega
     */
    @QueryParam("fechaEntrega")
    private Date fechaEntrega;

    /**
     * Factura Dncp
     */
    @QueryParam("compraPublica")
    private Character compraPublica;

    /**
     * Observacion
     */
    @QueryParam("observacion")
    private String observacion;

    /**
     * Monto iva 5
     */
    @QueryParam("montoIva5")
    private BigDecimal montoIva5;

    /**
     * Monto imponible 5
     */
    @QueryParam("montoImponible5")
    private BigDecimal montoImponible5;

    /**
     * Monto total 5
     */
    @QueryParam("montoTotal5")
    private BigDecimal montoTotal5;

    /**
     * Monto iva 10
     */
    @QueryParam("montoIva10")
    private BigDecimal montoIva10;

    /**
     * Monto imponible 10
     */
    @QueryParam("montoImponible10")
    private BigDecimal montoImponible10;

    /**
     * Monto total 10
     */
    @QueryParam("montoTotal10")
    private BigDecimal montoTotal10;

    /**
     * Monto total exento
     */
    @QueryParam("montoTotalExento")
    private BigDecimal montoTotalExento;

    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;

    /**
     * Monto total descuento particular
     */
    @QueryParam("montoTotalDescuentoParticular")
    private BigDecimal montoTotalDescuentoParticular;

    /**
     * Monto total descuento global
     */
    @QueryParam("montoTotalDescuentoGlobal")
    private BigDecimal montoTotalDescuentoGlobal;

    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;

    /**
     * Monto total factura
     */
    @QueryParam("montoTotalFactura")
    private BigDecimal montoTotalFactura;

    /**
     * Monto total en guaranies
     */
    @QueryParam("montoTotalGuaranies")
    private BigDecimal montoTotalGuaranies;

    /**
     * Digital
     */
    @QueryParam("digital")
    private Character digital;

    /**
     * CDC
     */
    @QueryParam("cdc")
    private String cdc;

    /**
     * Código de seguridad
     */
    @QueryParam("codSeguridad")
    private String codSeguridad;

    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;

    /**
     * Código de motivo anulación
     */
    @QueryParam("codigoMotivoAnulacion")
    private String codigoMotivoAnulacion;

    /**
     * Motivo anulación
     */
    @QueryParam("motivoAnulacion")
    private String motivoAnulacion;

    /**
     * Observación anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;

    /**
     * Nro casa
     */
    @QueryParam("nroCasa")
    private Integer nroCasa;

    /**
     * Identificador de departamento
     */
    @QueryParam("idDepartamento")
    private Integer idDepartamento;

    /**
     * Identificador de distrito
     */
    @QueryParam("idDistrito")
    private Integer idDistrito;

    /**
     * Identificador de ciudad
     */
    @QueryParam("idCiudad")
    private Integer idCiudad;
    
        /**
     * Identificador de departamento
     */
    @QueryParam("idDepartamentoTransaccion")
    private Integer idDepartamentoTransaccion;

    /**
     * Identificador de distrito
     */
    @QueryParam("idDistritoTransaccion")
    private Integer idDistritoTransaccion;

    /**
     * Identificador de ciudad
     */
    @QueryParam("idCiudadTransaccion")
    private Integer idCiudadTransaccion;

    /**
     * Identificador de encargado
     */
    @QueryParam("idEncargado")
    private Integer idEncargado;


    /**
     * Tiene saldo
     */
    @QueryParam("tieneSaldo")
    private Boolean tieneSaldo;

    /**
     * Tiene retencion
     */
    @QueryParam("tieneRetencion")
    private Boolean tieneRetencion;

    /**
     * Fecha vencimiento desde
     */
    @QueryParam("fechaVencimientoDesde")
    private Date fechaVencimientoDesde;

    /**
     * Fecha vencimiento hasta
     */
    @QueryParam("fechaVencimientoHasta")
    private Date fechaVencimientoHasta;

    /**
     * Imprimir original
     */
    @QueryParam("imprimirOriginal")
    private Boolean imprimirOriginal;

    /**
     * Imprimir duplicado
     */
    @QueryParam("imprimirDuplicado")
    private Boolean imprimirDuplicado;

    /**
     * Imprimir triplicado
     */
    @QueryParam("imprimirTriplicado")
    private Boolean imprimirTriplicado;

    /**
     * Monto anuladas cero
     */
    @QueryParam("montoAnuladasCero")
    private Boolean montoAnuladasCero;

    /**
     * Ignorar periodo de anulación
     */
    @QueryParam("ignorarPeriodoAnulacion")
    private Boolean ignorarPeriodoAnulacion;

    /**
     * Asignar nro factura
     */
    @QueryParam("asignarNroFactura")
    private Boolean asignarNroFactura;

    /**
     * Identificador de procesamiento
     */
    @QueryParam("idProcesamiento")
    private Integer idProcesamiento;

    /**
     * Identificador de Local Origen
     */
    @QueryParam("idLocalOrigen")
    private Integer idLocalOrigen;

    /**
     * Identificador de Local Destino
     */
    @QueryParam("idLocalDestino")
    private Integer idLocalDestino;
    
    @FormParam("uploadedFile")
    @PartType("application/octet-stream")
    private byte[] uploadedFileBytes;
    
    @QueryParam("ticket")
    private Boolean ticket = Boolean.FALSE;
    
    @QueryParam("nroConstancia")
    private String nroConstancia;
    
    @QueryParam("nroControlConstancia")
    private String nroControlConstancia;
    
    /**
     * Bandera para obtener kude del siediApi
     */
    @QueryParam("siediApi")
    private Boolean siediApi = Boolean.FALSE;
    
    /**
     * Bandera nro factura descendente
     */
    private Boolean nroFacturaDescendente = Boolean.TRUE;

    /**
     * Bandera identificador descendente
     */
    private Boolean idDescendente = Boolean.TRUE;

    /**
     * Factura Detalles
     */
    private List<AutoFacturaDetalleParam> facturaDetalles;

    /**
     * Orden de compra
     */
    private FacturaDncpParam facturaDncp;

    /**
     * Factura Descuentos
     */
    private List<FacturaDescuentoParam> facturaDescuentos;

    /**
     * Factura Notificación
     */
    private List<FacturaNotificacionParam> facturaNotificaciones;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idTalonario) && isNullOrEmpty(timbrado) ) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el timbrado o identificador de talonario"));
        }

        if (isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }

        if (isNull(idTipoFactura) && isNullOrEmpty(codigoTipoFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de factura"));
        }

        if (isNull(idNaturalezaVendedor) && isNullOrEmpty(codigoNaturalezaVendedor)) {
            codigoNaturalezaVendedor = "NO_CONTRIBUYENTE";
        }

        if (isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de la factura"));
        } else {

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);

            if (cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la factura no puede ser posterior a la fecha actual"));
            }
        }

        if (isNull(fechaVencimiento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de vencimiento de la factura"));
        }

        if (isNull(anulado)) {
            anulado = 'N';
        }

        if (isNull(cobrado)) {
            cobrado = 'N';
        }

        if (isNull(impreso)) {
            impreso = 'N';
        }

        if (isNull(entregado)) {
            entregado = 'N';
        }

        if (isNull(compraPublica)) {
            compraPublica = 'N';
        }

        if (isNull(archivoEdi)) {
            archivoEdi = 'N';
        }

        if (isNull(archivoSet)) {
            archivoSet = 'N';
        }

        if (isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la factura es digital o no"));
        } else {
            estadoSincronizado = digital.equals('S') && archivoSet.equals('S') ? 'N' : 'S';
        }

        if (!isNull(digital) && isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            if (digital.equals('S')) {
                codigoEstado = "PENDIENTE";
            } else {
                codigoEstado = "APROBADO";
            }
        }

        if (isNull(montoTotalFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total de la factura"));
        }

        if (isNull(idEncargado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el encargado"));
        }

        if (isNullOrEmpty(nroAutofactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de la autofactura"));
        } else {
            if(nroAutofactura.length() >= 7) {
                establecimiento = nroAutofactura.substring(0, 3);
                puntoExpedicion = nroAutofactura.substring(4, 7);
            } else {
                establecimiento = "0";
                puntoExpedicion = "0";
            }
        }

//        if (isNullOrEmpty(ruc)) {
//            addError(MensajePojo.createInstance()
//                    .descripcion("Se debe indicar el RUC/CI del cliente"));
//        }

        if (isNullOrEmpty(nroDocumentoVendedor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el Nro. de documento del vendedor"));
        }
        
            if (isNullOrEmpty(nombreVendedor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre completo del vendedor"));
        }
            
        if (isNullOrEmpty(nroControlConstancia)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el Nro. de control de constancia del vendedor"));
        }
        
        if (isNullOrEmpty(nroConstancia)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el Nro. de constancia de no contribuyente del vendedor"));
        }

        if (!isNull(digital) && digital.equals('S')) {

            if (!isNullOrEmpty(direccion)) {

                if (isNull(nroCasa)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el nro de casa del cliente"));
                }

                if (isNull(idDepartamento)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el departamento del cliente"));
                }

                if (isNull(idDistrito)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el distrito del cliente"));
                }

                if (isNull(idCiudad)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar la ciudad del cliente"));
                }
            }
        }

      

        if(!isNull(compraPublica) && compraPublica.equals('S')
                && isNull(facturaDncp)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar los datos de compra pública (DNCP)"));
        } else if (!isNull(facturaDncp)) {
            facturaDncp.setIdFactura(0);
            facturaDncp.setIdEmpresa(idEmpresa);
            facturaDncp.isValidToCreate();
        }

        if (isNullOrEmpty(facturaDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la factura"));
        } else {
            for (AutoFacturaDetalleParam detalle : facturaDetalles) {
                detalle.setIdAutofactura(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.setCompraPublica(compraPublica);
                detalle.isValidToCreate();
            }
        }

        if (!isNull(facturaDescuentos)) {
            for (FacturaDescuentoParam detalle : facturaDescuentos) {
                detalle.setIdFactura(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }


        if (!isNull(facturaNotificaciones)) {
            for (FacturaNotificacionParam detalle : facturaNotificaciones) {
                detalle.setIdFactura(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }

        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {

        List<MensajePojo> result = new ArrayList<>();

        if (!isNullOrEmpty(facturaDetalles)) {
            for (AutoFacturaDetalleParam detalle : facturaDetalles) {
                result.addAll(detalle.getErrores());
            }
        }

        if (!isNullOrEmpty(facturaDescuentos)) {
            for (FacturaDescuentoParam detalle : facturaDescuentos) {
                result.addAll(detalle.getErrores());
            }
        }


        if (!isNullOrEmpty(facturaNotificaciones)) {
            for (FacturaNotificacionParam detalle : facturaNotificaciones) {
                result.addAll(detalle.getErrores());
            }
        }

       
        if (!isNull(facturaDncp)) {
            result.addAll(facturaDncp.getErrores());
        }

        return result;
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (isNull(entregado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de entrega de la factura"));
        }

        if (isNull(impreso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de impresión de la factura"));
        }

        if (isNull(archivoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo EDI"));
        }

        if (isNull(archivoSet)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo SET"));
        }

        if (isNull(generadoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el archivo EDI esta generado"));
        }

        if (isNull(generadoSet)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el archivo SET esta generado"));
        }

        return !tieneErrores();
    }

    public boolean isValidToCancel() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (isNull(idMotivoAnulacion) && isNullOrEmpty(codigoMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de motivo de anulación"));
        }

        return !tieneErrores();
    }
    
    public boolean isValidToResendRejected() {

        limpiarErrores();

        super.isValidToList();
        
        if (isNullOrEmpty(codigoResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de resultado"));
        }

        if (isNull(fechaInsercionDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de inserción desde"));
        }

        return !tieneErrores();
    }

    public boolean isValidToGetPdf() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (ticket == null) {
            ticket = Boolean.FALSE;
        }

        if (imprimirOriginal == null) {
            imprimirOriginal = Boolean.FALSE;
        }

        if (imprimirDuplicado == null) {
            imprimirDuplicado = Boolean.FALSE;
        }

        if (imprimirTriplicado == null) {
            imprimirTriplicado = Boolean.FALSE;
        }

        if (!imprimirOriginal
                && !imprimirDuplicado
                && !imprimirTriplicado) {
            imprimirOriginal = Boolean.TRUE;
        }

        return !tieneErrores();
    }


    public boolean isValidToGetReporteVenta() {

        limpiarErrores();

        if (isNull(fechaDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha desde"));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaDesde);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            fechaDesde = cal.getTime();
        }

        if (isNull(fechaHasta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha hasta"));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaHasta);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            fechaHasta = cal.getTime();
        }

        nroFacturaDescendente = Boolean.FALSE;
        idDescendente = Boolean.FALSE;

        return !tieneErrores();
    }

    public boolean isValidToGetEstadoCuenta() {

        limpiarErrores();

        if (isNull(fechaDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha desde"));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaDesde);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            fechaDesde = cal.getTime();
        }

        if (isNull(fechaHasta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha hasta"));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaHasta);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            fechaHasta = cal.getTime();
        }

        return !tieneErrores();
    }


    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public void setTieneSaldo(Boolean tieneSaldo) {
        this.tieneSaldo = tieneSaldo;
    }

    public Boolean getTieneSaldo() {
        return tieneSaldo;
    }

    public void setFechaVencimientoHasta(Date fechaVencimientoHasta) {
        this.fechaVencimientoHasta = fechaVencimientoHasta;
    }

    public Date getFechaVencimientoHasta() {
        return fechaVencimientoHasta;
    }

    public void setFechaVencimientoDesde(Date fechaVencimientoDesde) {
        this.fechaVencimientoDesde = fechaVencimientoDesde;
    }

    public Date getFechaVencimientoDesde() {
        return fechaVencimientoDesde;
    }

    public String getNroAutofactura() {
        return nroAutofactura;
    }

    public void setNroAutofactura(String nroAutofactura) {
        this.nroAutofactura = nroAutofactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getCobrado() {
        return cobrado;
    }

    public void setCobrado(Character cobrado) {
        this.cobrado = cobrado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public void setTieneRetencion(Boolean tieneRetencion) {
        this.tieneRetencion = tieneRetencion;
    }

    public Boolean getTieneRetencion() {
        return tieneRetencion;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getDigital() {
        return digital;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public List<AutoFacturaDetalleParam> getFacturaDetalles() {
        return facturaDetalles;
    }

    public void setFacturaDetalles(List<AutoFacturaDetalleParam> facturaDetalles) {
        this.facturaDetalles = facturaDetalles;
    }

  
    public void setAnhoMes(String anhoMes) {
        this.anhoMes = anhoMes;
    }

    public String getAnhoMes() {
        return anhoMes;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoTipoFactura() {
        return codigoTipoFactura;
    }

    public void setCodigoTipoFactura(String codigoTipoFactura) {
        this.codigoTipoFactura = codigoTipoFactura;
    }

    public BigDecimal getMontoTotalDescuentoParticular() {
        return montoTotalDescuentoParticular;
    }

    public void setMontoTotalDescuentoParticular(BigDecimal montoTotalDescuentoParticular) {
        this.montoTotalDescuentoParticular = montoTotalDescuentoParticular;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setCodigoMotivoAnulacion(String codigoMotivoAnulacion) {
        this.codigoMotivoAnulacion = codigoMotivoAnulacion;
    }

    public String getCodigoMotivoAnulacion() {
        return codigoMotivoAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setFacturaDescuentos(List<FacturaDescuentoParam> facturaDescuentos) {
        this.facturaDescuentos = facturaDescuentos;
    }

    public List<FacturaDescuentoParam> getFacturaDescuentos() {
        return facturaDescuentos;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public Boolean getImprimirOriginal() {
        return imprimirOriginal;
    }

    public void setImprimirOriginal(Boolean imprimirOriginal) {
        this.imprimirOriginal = imprimirOriginal;
    }

    public Boolean getImprimirDuplicado() {
        return imprimirDuplicado;
    }

    public void setImprimirDuplicado(Boolean imprimirDuplicado) {
        this.imprimirDuplicado = imprimirDuplicado;
    }

    public Boolean getImprimirTriplicado() {
        return imprimirTriplicado;
    }

    public void setImprimirTriplicado(Boolean imprimirTriplicado) {
        this.imprimirTriplicado = imprimirTriplicado;
    }

    public void setNroFacturaEq(String nroFacturaEq) {
        this.nroFacturaEq = nroFacturaEq;
    }

    public String getNroFacturaEq() {
        return nroFacturaEq;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public void setIdDescendente(Boolean idDescendente) {
        this.idDescendente = idDescendente;
    }

    public Boolean getIdDescendente() {
        return idDescendente;
    }

    public void setNroFacturaDescendente(Boolean nroFacturaDescendente) {
        this.nroFacturaDescendente = nroFacturaDescendente;
    }

    public Boolean getNroFacturaDescendente() {
        return nroFacturaDescendente;
    }

    public void setIdLocalTalonario(Integer idLocalTalonario) {
        this.idLocalTalonario = idLocalTalonario;
    }

    public Integer getIdLocalTalonario() {
        return idLocalTalonario;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setMontoAnuladasCero(Boolean montoAnuladasCero) {
        this.montoAnuladasCero = montoAnuladasCero;
    }

    public Boolean getMontoAnuladasCero() {
        return montoAnuladasCero;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public String getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setAsignarNroFactura(Boolean asignarNroFactura) {
        this.asignarNroFactura = asignarNroFactura;
    }

    public Boolean getAsignarNroFactura() {
        return asignarNroFactura;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }


    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setCodigoResultado(String codigoResultado) {
        this.codigoResultado = codigoResultado;
    }

    public String getCodigoResultado() {
        return codigoResultado;
    }

    public void setFacturaNotificaciones(List<FacturaNotificacionParam> facturaNotificaciones) {
        this.facturaNotificaciones = facturaNotificaciones;
    }

    public List<FacturaNotificacionParam> getFacturaNotificaciones() {
        return facturaNotificaciones;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public Integer getIdNaturalezaVendedor() {
        return idNaturalezaVendedor;
    }

    public void setIdNaturalezaVendedor(Integer idNaturalezaVendedor) {
        this.idNaturalezaVendedor = idNaturalezaVendedor;
    }

    public String getCodigoNaturalezaVendedor() {
        return codigoNaturalezaVendedor;
    }

    public void setCodigoNaturalezaVendedor(String codigoNaturalezaVendedor) {
        this.codigoNaturalezaVendedor = codigoNaturalezaVendedor;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public byte[] getUploadedFileBytes() {
        return uploadedFileBytes;
    }

    public void setUploadedFileBytes(byte[] uploadedFileBytes) {
        this.uploadedFileBytes = uploadedFileBytes;
    }   

    public void setCompraPublica(Character compraPublica) {
        this.compraPublica = compraPublica;
    }

    public Character getCompraPublica() {
        return compraPublica;
    }

    public void setFacturaDncp(FacturaDncpParam facturaDncp) {
        this.facturaDncp = facturaDncp;
    }

    public FacturaDncpParam getFacturaDncp() {
        return facturaDncp;
    }

    public Boolean getTicket() {
        return ticket;
    }

    public void setTicket(Boolean ticket) {
        this.ticket = ticket;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public Integer getIdDepartamentoTransaccion() {
        return idDepartamentoTransaccion;
    }

    public void setIdDepartamentoTransaccion(Integer idDepartamentoTransaccion) {
        this.idDepartamentoTransaccion = idDepartamentoTransaccion;
    }

    public Integer getIdDistritoTransaccion() {
        return idDistritoTransaccion;
    }

    public void setIdDistritoTransaccion(Integer idDistritoTransaccion) {
        this.idDistritoTransaccion = idDistritoTransaccion;
    }

    public Integer getIdCiudadTransaccion() {
        return idCiudadTransaccion;
    }

    public void setIdCiudadTransaccion(Integer idCiudadTransaccion) {
        this.idCiudadTransaccion = idCiudadTransaccion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNroDocumentoVendedor() {
        return nroDocumentoVendedor;
    }

    public void setNroDocumentoVendedor(String nroDocumentoVendedor) {
        this.nroDocumentoVendedor = nroDocumentoVendedor;
    }

    public String getNroControlConstancia() {
        return nroControlConstancia;
    }

    public void setNroControlConstancia(String nroControlConstancia) {
        this.nroControlConstancia = nroControlConstancia;
    }

    public String getNroConstancia() {
        return nroConstancia;
    }

    public void setNroConstancia(String nroConstancia) {
        this.nroConstancia = nroConstancia;
    }

    public Boolean getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(Boolean siediApi) {
        this.siediApi = siediApi;
    }
  
}
