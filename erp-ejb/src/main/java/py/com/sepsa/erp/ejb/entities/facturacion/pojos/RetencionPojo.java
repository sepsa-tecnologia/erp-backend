/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad retención
 * @author Jonathan
 */
public class RetencionPojo {

    public RetencionPojo(Integer id, Integer idCliente, String cliente, String nroRetencion, Date fecha, BigDecimal montoImponibleTotal, BigDecimal montoIvaTotal, BigDecimal montoTotal, BigDecimal montoRetenidoTotal, Integer porcentajeRetencion, Character estado) {
        this.id = id;
        this.idCliente = idCliente;
        this.cliente = cliente;
        this.nroRetencion = nroRetencion;
        this.fecha = fecha;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoIvaTotal = montoIvaTotal;
        this.montoTotal = montoTotal;
        this.montoRetenidoTotal = montoRetenidoTotal;
        this.porcentajeRetencion = porcentajeRetencion;
        this.estado = estado;
    }
    
    /**
     * Identificador 
     */
    private Integer id;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Nro de retención
     */
    private String nroRetencion;
    
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto iva total
     */
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Monto retenido total
     */
    private BigDecimal montoRetenidoTotal;
    
    /**
     * Porcentaje de retención
     */
    private Integer porcentajeRetencion;
    
    /**
     * Estado
     */
    private Character estado;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }
}
