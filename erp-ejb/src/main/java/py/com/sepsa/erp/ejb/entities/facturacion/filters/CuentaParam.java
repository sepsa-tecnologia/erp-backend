/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de cheque
 * @author Jonathan D. Bernal Fernández
 */
public class CuentaParam extends CommonParam {
    
    public CuentaParam() {
    }

    public CuentaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de entidad financiera
     */
    @QueryParam("idEntidadFinanciera")
    private Integer idEntidadFinanciera;
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Nro cuenta
     */
    @QueryParam("nroCuenta")
    private String nroCuenta;
    
    /**
     * Fecha de alta
     */
    @QueryParam("fechaAlta")
    private Date fechaAlta;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEntidadFinanciera)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de entidad financiera"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNullOrEmpty(nroCuenta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de cuenta"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idEntidadFinanciera)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de entidad financiera"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la cuenta esta activa"));
        }
        
        if(isNullOrEmpty(nroCuenta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de cuenta"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
