/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de email
 * @author Jonathan
 */
public class EmailParam extends CommonParam {
    
    public EmailParam() {
    }

    public EmailParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Email
     */
    @QueryParam("email")
    private String email;
    
    /**
     * Principal
     */
    @QueryParam("principal")
    private Character principal;
    
    /**
     * Identificador de tipo de email
     */
    @QueryParam("idTipoEmail")
    private Integer idTipoEmail;
    
    /**
     * Código de tipo de email
     */
    @QueryParam("codigoTipoEmail")
    private String codigoTipoEmail;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoEmail) && isNullOrEmpty(codigoTipoEmail)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de email"));
        }
        
        if(isNull(principal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el mail es principal o no"));
        }
        
        if(isNullOrEmpty(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idTipoEmail)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de email"));
        }
        
        if(isNull(principal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el mail es principal o no"));
        }
        
        if(isNullOrEmpty(email)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el email"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCodigoTipoEmail(String codigoTipoEmail) {
        this.codigoTipoEmail = codigoTipoEmail;
    }

    public String getCodigoTipoEmail() {
        return codigoTipoEmail;
    }

    public Character getPrincipal() {
        return principal;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public Integer getIdTipoEmail() {
        return idTipoEmail;
    }

    public void setIdTipoEmail(Integer idTipoEmail) {
        this.idTipoEmail = idTipoEmail;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
