/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.TalonarioLocal;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioLocalParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface TalonarioLocalFacade extends Facade<TalonarioLocal, TalonarioLocalParam, UserInfoImpl> {

    public Boolean validToCreate(TalonarioLocalParam param);

    public Boolean validToEdit(TalonarioLocalParam param);

    public List<TalonarioLocal> findRelacionados(TalonarioLocalParam param);

    public Long findRelacionadosSize(TalonarioLocalParam param);

    public void asociarMasivo(TalonarioLocalParam param, UserInfoImpl userInfo);
    
}
