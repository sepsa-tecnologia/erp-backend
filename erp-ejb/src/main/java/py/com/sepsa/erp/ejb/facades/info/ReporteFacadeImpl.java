/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Reporte;
import py.com.sepsa.erp.ejb.entities.info.filters.ReporteParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ReportePojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ReporteFacade", mappedName = "ReporteFacade")
@Local(ReporteFacade.class)
public class ReporteFacadeImpl extends FacadeImpl<Reporte, ReporteParam> implements ReporteFacade {

    public ReporteFacadeImpl() {
        super(Reporte.class);
    }

    public Boolean validToCreate(ReporteParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        ReporteParam param1 = new ReporteParam();
        param1.setCodigo(param.getCodigo().trim());

        Reporte reporte = findFirst(param1);

        if (reporte != null) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro con el mismo código"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(ReporteParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        Reporte reporte = find(param.getId());

        if (reporte == null) {

            param.addError(MensajePojo.createInstance().descripcion("No existe el reporte"));

        } else if (!reporte.getCodigo().equals(param.getCodigo().trim())) {

            ReporteParam param1 = new ReporteParam();
            param1.setCodigo(param.getCodigo().trim());

            Reporte r = findFirst(param1);

            if (r != null) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro con el mismo código"));
            }
        }

        return !param.tieneErrores();
    }

    @Override
    public Reporte create(ReporteParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        Reporte result = new Reporte();
        result.setCodigo(param.getCodigo().trim());
        result.setCodigoTipoReporte(param.getCodigoTipoReporte().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setParametros(param.getParametros());

        create(result);

        return result;
    }

    @Override
    public Reporte edit(ReporteParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        Reporte result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setParametros(param.getParametros());

        edit(result);

        return result;
    }

    @Override
    public ReportePojo find(Integer id, String codigoReporte) {
        ReporteParam param = new ReporteParam();
        param.setId(id);
        param.setCodigo(codigoReporte);
        param.setActivo('S');
        param.setFirstResult(0);
        param.setPageSize(1);
        List<ReportePojo> list = findPojo(param);
        return list == null || list.isEmpty() ? null : list.get(0);
    }

    @Override
    public List<ReportePojo> findPojo(ReporteParam param) {

        AbstractFind find = new AbstractFind(ReportePojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigo"),
                        getPath("root").get("activo"),
                        getPath("root").get("parametros"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<Reporte> find(ReporteParam param) {

        AbstractFind find = new AbstractFind(Reporte.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, ReporteParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Reporte> root = cq.from(Reporte.class);

        find.addPath("root", root);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ReporteParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Reporte> root = cq.from(Reporte.class);

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;

        return result;
    }
}
