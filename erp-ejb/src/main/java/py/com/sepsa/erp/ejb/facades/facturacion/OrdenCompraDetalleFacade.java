/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraDetallePojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface OrdenCompraDetalleFacade extends Facade<OrdenCompraDetalle, OrdenCompraDetalleParam, UserInfoImpl> {

    public Boolean validToCreate(UserInfoImpl userInfo, OrdenCompraDetalleParam param, boolean validarIdOrdenCompra);

    public Boolean validToEdit(UserInfoImpl userInfo, OrdenCompraDetalleParam param);

    public List<OrdenCompraDetallePojo> getByOrdenCompra(OrdenCompraDetalleParam param);
    
    public List<OrdenCompraDetallePojo> getDisponibleDetalle(OrdenCompraDetalleParam param);

    public List<OrdenCompraDetallePojo> getDisponible(Integer idOrdenCompra, Integer idProducto, Integer idEmpresa);
}