/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.Menu;
import py.com.sepsa.erp.ejb.entities.usuario.Perfil;
import py.com.sepsa.erp.ejb.entities.info.MenuPerfil;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuPerfilParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Sergio D. Riveros Vazquez
 */
@Stateless(name = "MenuPerfilFacade", mappedName = "MenuPerfilFacade")
@Local(MenuPerfilFacade.class)
public class MenuPerfilFacadeImpl extends FacadeImpl<MenuPerfil, MenuPerfilParam> implements MenuPerfilFacade {

    public MenuPerfilFacadeImpl() {
        super(MenuPerfil.class);
    }

    @Override
    public Boolean validToCreate(MenuPerfilParam param, boolean validarIdMenu) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MenuPerfilParam param1 = new MenuPerfilParam();
        param1.setIdPerfil(param.getIdPerfil());
        param1.setCodigoPerfil(param.getCodigoPerfil());
        param1.setIdMenu(param.getIdMenu());
        param1.setCodigoMenu(param.getCodigoMenu());
        
        MenuPerfil item = findFirst(param1);
        
        if(item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }
        
        Perfil perfil = facades.getPerfilFacade().find(param.getIdEmpresa(),
                param.getIdPerfil(), param.getCodigoPerfil());
        
        if(perfil == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el perfil"));
        } else {
            param.setIdPerfil(perfil.getId());
        }
        
        Menu menu = facades.getMenuFacade().find(param.getIdEmpresa(),
                param.getIdMenu(),
                param.getCodigoMenu());
        
        if(validarIdMenu && menu == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el menú"));
        } else if(menu != null) {
            param.setIdMenu(menu.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(MenuPerfilParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        MenuPerfilParam param1 = new MenuPerfilParam();
        param1.setIdPerfil(param.getIdPerfil());
        param1.setCodigoPerfil(param.getCodigoPerfil());
        param1.setIdMenu(param.getIdMenu());
        param1.setCodigoMenu(param.getCodigoMenu());
        
        MenuPerfil item = findFirst(param1);
        
        if(item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un registro"));
        } else {
            param.setId(item.getId());
        }
        
        Perfil perfil = facades.getPerfilFacade().find(param.getIdEmpresa(),
                param.getIdPerfil(), param.getCodigoPerfil());
        
        if(perfil == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el perfil"));
        } else {
            param.setIdPerfil(perfil.getId());
        }
        
        Menu menu = facades.getMenuFacade().find(param.getIdEmpresa(),
                param.getIdMenu(), param.getCodigoMenu());
        
        if(menu == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el menú"));
        } else {
            param.setIdMenu(menu.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public MenuPerfil create(MenuPerfilParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        MenuPerfil item = new MenuPerfil();
        item.setIdPerfil(param.getIdPerfil());
        item.setIdMenu(param.getIdMenu());
        item.setActivo(param.getActivo());
        
        create(item);
        
        return item;
    }
    
    @Override
    public MenuPerfil edit(MenuPerfilParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MenuPerfil item = find(param.getId());
        item.setIdPerfil(param.getIdPerfil());
        item.setIdMenu(param.getIdMenu());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<MenuPerfil> find(MenuPerfilParam param) {

        String where = "true";
        
        if(!isNullOrEmpty(param.getCodigoMenu())) {
            where = String.format("%s and m.codigo = '%s'", where, param.getCodigoMenu().trim());
        }
        
        if(!isNull(param.getIdMenu())) {
            where = String.format("%s and mp.id_menu = %d", where, param.getIdMenu());
        }
        
        if(!isNullOrEmpty(param.getCodigoPerfil())) {
            where = String.format("%s and p.codigo = '%s'", where, param.getCodigoPerfil().trim());
        }
        
        if(!isNull(param.getIdPerfil())) {
            where = String.format("%s and mp.id_perfil = %d", where, param.getIdPerfil());
        }
        
        if(!isNull(param.getActivo())) {
            where = String.format("%s and mp.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select mp.*"
                + " from info.menu_perfil mp"
                + " left join info.menu m on m.id = mp.id_menu"
                + " left join usuario.perfil p on p.id = mp.id_perfil"
                + " where %s order by mp.activo, mp.id_menu, mp.id_perfil offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, MenuPerfil.class);
        
        List<MenuPerfil> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MenuPerfilParam param) {

        String where = "true";
        
        if(!isNullOrEmpty(param.getCodigoMenu())) {
            where = String.format("%s and m.codigo = '%s'", where, param.getCodigoMenu().trim());
        }
        
        if(!isNull(param.getIdMenu())) {
            where = String.format("%s and mp.id_menu = %d", where, param.getIdMenu());
        }
        
        if(!isNullOrEmpty(param.getCodigoPerfil())) {
            where = String.format("%s and p.codigo = '%s'", where, param.getCodigoPerfil().trim());
        }
        
        if(!isNull(param.getIdPerfil())) {
            where = String.format("%s and mp.id_perfil = %d", where, param.getIdPerfil());
        }
        
        if(!isNull(param.getActivo())) {
            where = String.format("%s and mp.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(mp.*)"
                + " from info.menu_perfil mp"
                + " left join info.menu m on m.id = mp.id_menu"
                + " left join usuario.perfil p on p.id = mp.id_perfil"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<MenuPerfil> findRelacionados(MenuPerfilParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select coalesce(mp.id, 0) as id, coalesce(mp.id_menu, %d) as id_menu, p.id as id_perfil, coalesce(mp.activo, 'N') as activo\n"
                + "from usuario.perfil p\n"
                + "left join info.menu_perfil mp on mp.id_perfil = p.id and mp.activo = 'S' and mp.id_menu = %d\n"
                + "left join info.menu m on m.id = mp.id_menu\n"
                + "where %s limit %d offset %d", param.getIdMenu(), param.getIdMenu(), where, param.getPageSize(), param.getFirstResult());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<MenuPerfil> result = Lists.empty();
        
        for (Object[] objects : list) {
            int i = 0;
            MenuPerfil mp = new MenuPerfil();
            mp.setId((Integer)objects[i++]);
            mp.setIdMenu((Integer)objects[i++]);
            mp.setIdPerfil((Integer)objects[i++]);
            mp.setActivo((Character)objects[i++]);
            mp.setPerfil(facades.getPerfilFacade().find(mp.getIdPerfil()));
            mp.setMenu(facades.getMenuFacade().find(mp.getIdMenu()));
            result.add(mp);
        }
        
        return result;
    }

    @Override
    public Long findRelacionadosSize(MenuPerfilParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and p.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        String sql = String.format("select count(p)\n"
                + "from usuario.perfil p\n"
                + "left join info.menu_perfil mp on mp.id_perfil = p.id and mp.activo = 'S' and mp.id_menu = %d\n"
                + "left join info.menu m on m.id = mp.id_menu\n"
                + "where %s", param.getIdMenu(), where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
