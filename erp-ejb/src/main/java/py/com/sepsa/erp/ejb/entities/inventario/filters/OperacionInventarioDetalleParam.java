/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
public class OperacionInventarioDetalleParam extends CommonParam {

    public OperacionInventarioDetalleParam() {
    }

    public OperacionInventarioDetalleParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de operacion inventario
     */
    @QueryParam("idOperacionInventario")
    private Integer idOperacionInventario;

    /**
     * Identificador de estado de operación
     */
    @QueryParam("idEstadoOperacion")
    private Integer idEstadoOperacion;

    /**
     * Código de estado de operación
     */
    @QueryParam("codigoEstadoOperacion")
    private String codigoEstadoOperacion;

    /**
     * Identificador de estado de inventario origen
     */
    @QueryParam("idEstadoInventarioOrigen")
    private Integer idEstadoInventarioOrigen;

    /**
     * Identificador de estado de inventario destino
     */
    @QueryParam("idEstadoInventarioDestino")
    private Integer idEstadoInventarioDestino;

    /**
     * Código de estado de inventario origen
     */
    @QueryParam("codigoEstadoInventarioOrigen")
    private String codigoEstadoInventarioOrigen;

    /**
     * Código de estado de inventario destino
     */
    @QueryParam("codigoEstadoInventarioDestino")
    private String codigoEstadoInventarioDestino;

    /**
     * Código de tipo de operación
     */
    @QueryParam("codigoTipoOperacion")
    private String codigoTipoOperacion;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de deposito origen
     */
    @QueryParam("idDepositoOrigen")
    private Integer idDepositoOrigen;

    /**
     * Identificador de deposito destino
     */
    @QueryParam("idDepositoDestino")
    private Integer idDepositoDestino;

    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private Integer cantidad;

    /**
     * Fecha de vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idOperacionInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de operación de inventario"));
        }

        if (isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de producto"));
        }

        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNull(idOperacionInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de operación de inventario"));
        }

        if (isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de producto"));
        }

        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

        return !tieneErrores();
    }

    public Integer getIdOperacionInventario() {
        return idOperacionInventario;
    }

    public void setIdOperacionInventario(Integer idOperacionInventario) {
        this.idOperacionInventario = idOperacionInventario;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public Integer getIdEstadoOperacion() {
        return idEstadoOperacion;
    }

    public void setIdEstadoOperacion(Integer idEstadoOperacion) {
        this.idEstadoOperacion = idEstadoOperacion;
    }

    public String getCodigoEstadoOperacion() {
        return codigoEstadoOperacion;
    }

    public void setCodigoEstadoOperacion(String codigoEstadoOperacion) {
        this.codigoEstadoOperacion = codigoEstadoOperacion;
    }

    public Integer getIdEstadoInventarioOrigen() {
        return idEstadoInventarioOrigen;
    }

    public void setIdEstadoInventarioOrigen(Integer idEstadoInventarioOrigen) {
        this.idEstadoInventarioOrigen = idEstadoInventarioOrigen;
    }

    public Integer getIdEstadoInventarioDestino() {
        return idEstadoInventarioDestino;
    }

    public void setIdEstadoInventarioDestino(Integer idEstadoInventarioDestino) {
        this.idEstadoInventarioDestino = idEstadoInventarioDestino;
    }

    public String getCodigoEstadoInventarioOrigen() {
        return codigoEstadoInventarioOrigen;
    }

    public void setCodigoEstadoInventarioOrigen(String codigoEstadoInventarioOrigen) {
        this.codigoEstadoInventarioOrigen = codigoEstadoInventarioOrigen;
    }

    public String getCodigoEstadoInventarioDestino() {
        return codigoEstadoInventarioDestino;
    }

    public void setCodigoEstadoInventarioDestino(String codigoEstadoInventarioDestino) {
        this.codigoEstadoInventarioDestino = codigoEstadoInventarioDestino;
    }

    public void setIdDepositoDestino(Integer idDepositoDestino) {
        this.idDepositoDestino = idDepositoDestino;
    }

    public Integer getIdDepositoDestino() {
        return idDepositoDestino;
    }

    public void setIdDepositoOrigen(Integer idDepositoOrigen) {
        this.idDepositoOrigen = idDepositoOrigen;
    }

    public Integer getIdDepositoOrigen() {
        return idDepositoOrigen;
    }

    public void setCodigoTipoOperacion(String codigoTipoOperacion) {
        this.codigoTipoOperacion = codigoTipoOperacion;
    }

    public String getCodigoTipoOperacion() {
        return codigoTipoOperacion;
    }
}
