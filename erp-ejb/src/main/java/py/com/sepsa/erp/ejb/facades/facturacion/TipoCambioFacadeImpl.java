/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoCambio;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoCambioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TipoCambioPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoCambioFacade", mappedName = "TipoCambioFacade")
@Local(TipoCambioFacade.class)
public class TipoCambioFacadeImpl extends FacadeImpl<TipoCambio, TipoCambioParam> implements TipoCambioFacade {

    public TipoCambioFacadeImpl() {
        super(TipoCambio.class);
    }

    public Boolean validToCreate(TipoCambioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda(), param.getCodigoMoneda());
        
        if(isNull(moneda)) {
           param.addError(MensajePojo.createInstance()
                   .descripcion("No existe la moneda"));
        } else {

            Calendar from = Calendar.getInstance();
            from.set(Calendar.HOUR_OF_DAY, 0);
            from.set(Calendar.MINUTE, 0);
            from.set(Calendar.SECOND, 0);

            Calendar to = Calendar.getInstance();
            to.set(Calendar.HOUR_OF_DAY, 23);
            to.set(Calendar.MINUTE, 59);
            to.set(Calendar.SECOND, 59);
            
            TipoCambioParam param1 = new TipoCambioParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setIdMoneda(param.getIdMoneda());
            param1.setFechaInsercionDesde(from.getTime());
            param1.setFechaInsercionHasta(to.getTime());
            param1.setActivo('S');

            TipoCambio tc = findFirst(param1);

            if(!isNull(tc)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Existe otro tipo de cambio activo para la moneda"));
            }
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(TipoCambioParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        TipoCambio tipoCobro = find(param.getId());
        
        if(tipoCobro == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de cambio"));
        }
        
        return !param.tieneErrores();
    }
    
    
    
    @Override
    public TipoCambio actualizarTipoCambio(TipoCambioParam param, UserInfoImpl userInfo) {
       
//        if (!validToEdit(param)){
//            return null;
//        }
        
        Calendar from = Calendar.getInstance();
        from.set(Calendar.HOUR_OF_DAY, 0);
        from.set(Calendar.MINUTE, 0);
        from.set(Calendar.SECOND, 0);

        Calendar to = Calendar.getInstance();
        to.set(Calendar.HOUR_OF_DAY, 23);
        to.set(Calendar.MINUTE, 59);
        to.set(Calendar.SECOND, 59);

        TipoCambioParam param1 = new TipoCambioParam();
        param1.setIdEmpresa(userInfo.getIdEmpresa());
        param1.setId(param.getId());
        param1.setActivo('S');

        TipoCambio tc = findFirst(param1);

        if(!isNull(tc)) {
            tc.setActivo('N');

            edit(tc);
        }
        
        TipoCambio result = new TipoCambio();
        result.setIdEmpresa(userInfo.getIdEmpresa());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        result.setIdMoneda(param.getIdMoneda());
        result.setCompra(param.getCompra());
        result.setVenta(param.getVenta());
        result.setActivo(param.getActivo());
        create(result);
        
        return result;
        
        
    }
            
    
    
    @Override
    public TipoCambio create(TipoCambioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TipoCambio result = new TipoCambio();
        result.setIdEmpresa(userInfo.getIdEmpresa());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        result.setIdMoneda(param.getIdMoneda());
        result.setCompra(param.getCompra());
        result.setVenta(param.getVenta());
        result.setActivo(param.getActivo());
        create(result);
        
        return result;
    }
    
    @Override
    public TipoCambio edit(TipoCambioParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TipoCambio result = find(param.getId());
        result.setActivo(param.getActivo());
        edit(result);
        
        return result;
    }
    
    @Override
    public TipoCambioPojo findFirst(Integer id) {
        TipoCambioParam param = new TipoCambioParam();
        param.setId(id);
        param.setListadoPojo(Boolean.TRUE);
        param.isValidToList();
        return findFirstPojo(param);
    }
    
    @Override
    public List<TipoCambioPojo> findPojo(TipoCambioParam param) {

        AbstractFind find = new AbstractFind(TipoCambioPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("codigo"),
                        getPath("moneda").get("descripcion"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("compra"),
                        getPath("root").get("venta"),
                        getPath("root").get("idEmpresa"),
                        getPath("root").get("activo"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<TipoCambio> find(TipoCambioParam param) {

        AbstractFind find = new AbstractFind(TipoCambioPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, TipoCambioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<TipoCambio> root = cq.from(TipoCambio.class);
        Join<TipoCambio, Moneda> moneda = root.join("moneda");
        
        find.addPath("root", root);
        find.addPath("moneda", moneda);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        cq.orderBy(qb.asc(root.get("activo")), qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TipoCambioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoCambio> root = cq.from(TipoCambio.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
