/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.ReporteEmpresa;
import py.com.sepsa.erp.ejb.entities.info.filters.ReporteEmpresaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ReporteEmpresaFacade extends Facade<ReporteEmpresa, ReporteEmpresaParam, UserInfoImpl> {

    public byte[] generateReport(ReporteEmpresaParam param, UserInfoImpl userInfo) throws Exception;

}
