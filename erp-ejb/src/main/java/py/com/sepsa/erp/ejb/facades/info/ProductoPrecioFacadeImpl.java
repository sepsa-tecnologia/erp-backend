/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.info.CanalVenta;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.ProductoPrecio;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoPrecioParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPrecioPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import py.com.sepsa.utils.facades.AbstractFind;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ProductoPrecioFacade", mappedName = "ProductoPrecioFacade")
@Local(ProductoPrecioFacade.class)
public class ProductoPrecioFacadeImpl extends FacadeImpl<ProductoPrecio, ProductoPrecioParam> implements ProductoPrecioFacade {

    public ProductoPrecioFacadeImpl() {
        super(ProductoPrecio.class);
    }
    
    public Boolean validToCreate(ProductoPrecioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Producto producto = facades.getProductoFacade().find(param.getIdProducto());
        
        if(producto == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el producto"));
        }
        
        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la moneda"));
        }
        
        if(param.getIdCanalVenta() != null) {
            CanalVenta canalVenta = facades.getCanalVentaFacade().find(param.getIdCanalVenta());

            if(canalVenta == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el canal de venta"));
            }
        }
        
        if(param.getIdCliente() != null) {
            Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());

            if(cliente == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el cliente"));
            }
        }
        
        validarUnicicidad(param);
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(ProductoPrecioParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ProductoPrecio productoPrecio = find(param.getId());
        
        if(productoPrecio == null) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
            
        } else {
            
            param.setIdExcluidos(Arrays.asList(param.getId()));
            
            Date fechaInicio = productoPrecio.getFechaInicio();
            Date fechaInicioParam = param.getFechaInicio();
            
            LocalDateTime localDateTime = fechaInicio.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime localDateTime2 = fechaInicioParam.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            
            if(!localDateTime.equals(localDateTime2)
                || (param.getFechaFin() != null && !productoPrecio.getFechaFin().equals(param.getFechaFin()))) {
                validarUnicicidad(param);
            }
            
        }
        
        Producto producto = facades.getProductoFacade().find(param.getIdProducto());
        
        if(producto == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el producto"));
        }
        
        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la moneda"));
        }
        
        if(param.getIdCanalVenta() != null) {
            CanalVenta canalVenta = facades.getCanalVentaFacade().find(param.getIdCanalVenta());

            if(canalVenta == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el canal de venta"));
            }
        }
        
        if(param.getIdCliente() != null) {
            Cliente cliente = facades.getClienteFacade().find(param.getIdCliente());

            if(cliente == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el cliente"));
            }
        }
        
        return !param.tieneErrores();
    }

    private void validarUnicicidad(ProductoPrecioParam param) {
        
        ProductoPrecioParam param2 = new ProductoPrecioParam();
        param2.setIdEmpresa(param.getIdEmpresa());
        param2.setIdExcluidos(param.getIdExcluidos());
        param2.setIdProducto(param.getIdProducto());
        param2.setIdCanalVenta(param.getIdCanalVenta());
        param2.setIdCliente(param.getIdCliente());
        param2.setIdMoneda(param.getIdMoneda());
        param2.setActivo('S');
        Long size;
        
        param2.setFechaInicioDesde(null);
        param2.setFechaInicioHasta(param.getFechaInicio());
        param2.setFechaFinDesde(param.getFechaInicio());
        param2.setFechaFinHasta(null);
        param2.setFechaFinNull(null);
        size = findSize(param2);
        
        param2.setFechaInicioDesde(null);
        param2.setFechaInicioHasta(param.getFechaInicio());
        param2.setFechaFinDesde(null);
        param2.setFechaFinHasta(null);
        param2.setFechaFinNull(Boolean.TRUE);
        size = findSize(param2) + size;
        
        if(param.getFechaFin() != null) {
            param2.setFechaInicioDesde(null);
            param2.setFechaInicioHasta(param.getFechaFin());
            param2.setFechaFinDesde(param.getFechaFin());
            param2.setFechaFinHasta(null);
            param2.setFechaFinNull(null);
            size = findSize(param2) + size;
        } else {
            param2.setFechaInicioDesde(null);
            param2.setFechaInicioHasta(null);
            param2.setFechaFinDesde(null);
            param2.setFechaFinHasta(null);
            param2.setFechaFinNull(Boolean.TRUE);
            size = findSize(param2) + size;
        }
        
        param2.setFechaInicioDesde(param.getFechaInicio());
        param2.setFechaInicioHasta(null);
        param2.setFechaFinDesde(null);
        if(param.getFechaFin() != null) {
            param2.setFechaFinHasta(param.getFechaFin());
            param2.setFechaFinNull(null);
        } else {
            param2.setFechaFinHasta(null);
            param2.setFechaFinNull(Boolean.TRUE);
        }
        size = findSize(param2) + size;
        
        param2.setFechaInicioDesde(null);
        param2.setFechaInicioHasta(param.getFechaInicio());
        if(param.getFechaFin() != null) {
            param2.setFechaFinDesde(param.getFechaFin());
            param2.setFechaFinNull(null);
        } else {
            param2.setFechaFinDesde(null);
            param2.setFechaFinNull(Boolean.TRUE);
        }
        param2.setFechaFinHasta(null);
        size = findSize(param2) + size;
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro activo para el producto, moneda, canal de venta y cliente en el rango de fecha"));
        }
        
    }
    
    @Override
    public ProductoPrecio create(ProductoPrecioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        ProductoPrecio result = new ProductoPrecio();
        result.setIdEmpresa(param.getIdEmpresa());
        result.setIdProducto(param.getIdProducto());
        result.setIdMoneda(param.getIdMoneda());
        result.setIdCanalVenta(param.getIdCanalVenta());
        result.setIdCliente(param.getIdCliente());
        result.setPrecio(param.getPrecio());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());
        result.setActivo('S');
        result.setFechaInsercion(Calendar.getInstance().getTime());
        
        create(result);
        
        return result;
    }
    
    @Override
    public ProductoPrecio edit(ProductoPrecioParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ProductoPrecio result = find(param.getId());
        //result.setIdProducto(param.getIdProducto());
        //result.setIdMoneda(param.getIdMoneda());
        //result.setIdCanalVenta(param.getIdCanalVenta());
        if(param.getPrecio() != null) {
            result.setPrecio(param.getPrecio());
        }
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());
        result.setActivo(param.getActivo());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public List<ProductoPrecioPojo> findPojo(ProductoPrecioParam param) {

        AbstractFind find = new AbstractFind(ProductoPrecioPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("descripcion"),
                        getPath("moneda").get("codigo"),
                        getPath("root").get("precio"),
                        getPath("root").get("activo"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("fechaInicio"),
                        getPath("root").get("fechaFin"),
                        getPath("root").get("idProducto"),
                        getPath("producto").get("descripcion"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("porcentajeImpuesto"),
                        getPath("root").get("idCanalVenta"),
                        getPath("canalVenta").get("descripcion"),
                        getPath("canalVenta").get("codigo"),
                        getPath("root").get("idCliente"),
                        getPath("cliente").get("razonSocial"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<ProductoPrecio> find(ProductoPrecioParam param) {

        AbstractFind find = new AbstractFind(ProductoPrecio.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, ProductoPrecioParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<ProductoPrecio> root = cq.from(ProductoPrecio.class);
        Join<ProductoPrecio, Producto> producto = root.join("producto");
        Join<ProductoPrecio, Moneda> moneda = root.join("moneda");
        Join<ProductoPrecio, CanalVenta> canalVenta = root.join("canalVenta", JoinType.LEFT);
        Join<ProductoPrecio, Cliente> cliente = root.join("cliente", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("producto", producto);
        find.addPath("cliente", cliente);
        find.addPath("canalVenta", canalVenta);
        find.addPath("moneda", moneda);
        
        find.select(cq, qb);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getIdExcluidos() != null && !param.getIdExcluidos().isEmpty()) {
            predList.add(qb.not(root.get("id").in(param.getIdExcluidos())));
        }
        
        if(param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if(param.getProductoDescripcion() != null && !param.getProductoDescripcion().trim().isEmpty()) {
            predList.add(qb.like(producto.<String>get("descripcion"),
                    String.format("%%%s%%", param.getProductoDescripcion().trim())));
        }
        
        if(param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if(param.getIdCanalVenta() != null) {
            predList.add(qb.equal(root.get("idCanalVenta"), param.getIdCanalVenta()));
        }
        
        if(param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if(param.getCanalVentaDescripcion() != null && !param.getCanalVentaDescripcion().trim().isEmpty()) {
            predList.add(qb.like(canalVenta.<String>get("descripcion"),
                    String.format("%%%s%%", param.getCanalVentaDescripcion().trim())));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(param.getFecha() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInicio"), param.getFecha()));
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaFin"), param.getFecha()));
        }
        
        if(param.getFechaInicioDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInicio"), param.getFechaInicioDesde()));
        }
        
        if(param.getFechaInicioHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInicio"), param.getFechaInicioHasta()));
        }
        
        if(param.getFechaFinDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaFin"), param.getFechaFinDesde()));
        }
        
        if(param.getFechaFinHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaFin"), param.getFechaFinHasta()));
        }
        
        if(param.getFechaFinNull() != null) {
            predList.add(param.getFechaFinNull()
                    ? root.get("fechaFin").isNull()
                    : root.get("fechaFin").isNotNull());
        }
        
        if(param.getIdCanalVentaNull() != null) {
            predList.add(param.getIdCanalVentaNull()
                    ? root.get("idCanalVenta").isNull()
                    : root.get("idCanalVenta").isNotNull());
        }
        
        if(param.getIdClienteNull() != null) {
            predList.add(param.getIdClienteNull()
                    ? root.get("idCliente").isNull()
                    : root.get("idCliente").isNotNull());
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        cq.orderBy(qb.desc(root.get("id")));
        
        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = find.query(cq, getEntityManager());
        
        List<T> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ProductoPrecioParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<ProductoPrecio> root = cq.from(ProductoPrecio.class);
        Join<ProductoPrecio, Producto> producto = root.join("producto");
        Join<ProductoPrecio, CanalVenta> canalVenta = root.join("canalVenta",JoinType.LEFT);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getIdExcluidos() != null && !param.getIdExcluidos().isEmpty()) {
            predList.add(qb.not(root.get("id").in(param.getIdExcluidos())));
        }
        
        if(param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if(param.getProductoDescripcion() != null && !param.getProductoDescripcion().trim().isEmpty()) {
            predList.add(qb.like(producto.<String>get("descripcion"),
                    String.format("%%%s%%", param.getProductoDescripcion().trim())));
        }
        
        if(param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if(param.getIdCanalVenta() != null) {
            predList.add(qb.equal(root.get("idCanalVenta"), param.getIdCanalVenta()));
        }
        
        if(param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if(param.getCanalVentaDescripcion() != null && !param.getCanalVentaDescripcion().trim().isEmpty()) {
            predList.add(qb.like(canalVenta.<String>get("descripcion"),
                    String.format("%%%s%%", param.getCanalVentaDescripcion().trim())));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(param.getFecha() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInicio"), param.getFecha()));
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaFin"), param.getFecha()));
        }
        
        if(param.getFechaInicioDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInicio"), param.getFechaInicioDesde()));
        }
        
        if(param.getFechaInicioHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInicio"), param.getFechaInicioHasta()));
        }
        
        if(param.getFechaFinDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaFin"), param.getFechaFinDesde()));
        }
        
        if(param.getFechaFinHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaFin"), param.getFechaFinHasta()));
        }
        
        if(param.getFechaFinNull() != null) {
            predList.add(param.getFechaFinNull()
                    ? root.get("fechaFin").isNull()
                    : root.get("fechaFin").isNotNull());
        }
        
        if(param.getIdCanalVentaNull() != null) {
            predList.add(param.getIdCanalVentaNull()
                    ? root.get("idCanalVenta").isNull()
                    : root.get("idCanalVenta").isNotNull());
        }
        
        if(param.getIdClienteNull() != null) {
            predList.add(param.getIdClienteNull()
                    ? root.get("idCliente").isNull()
                    : root.get("idCliente").isNotNull());
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
