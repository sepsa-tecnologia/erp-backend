/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
public class TransportistaParam extends CommonParam {
    
    public TransportistaParam(){
    }
    
    public TransportistaParam(String bruto){
        super(bruto);
    }
    
    @QueryParam("idNaturalezaTransportista")
    private Integer idNaturalezaTransportista;
    
    @QueryParam("razonSocial")
    private String razonSocial;
    
    @QueryParam("ruc")
    private String ruc;
    
    @QueryParam("verificadorRuc")
    private Integer verificadorRuc;
    
    @QueryParam("idTipoDocumento")
    private Integer idTipoDocumento;
    
    @QueryParam("nroDocumento")
    private String nroDocumento;
    
    @QueryParam("nombreCompleto")
    private String nombreCompleto;
    
    @QueryParam("domicilioFiscal")
    private String domicilioFiscal;
    
    @QueryParam("direccionChofer")
    private String direccionChofer;
    
    @QueryParam("nroDocumentoChofer")
    private String nroDocumentoChofer;
    
    
    @Override
    public boolean isValidToCreate(){
        limpiarErrores();
        
        if(isNull(idNaturalezaTransportista)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de naturaleza del transportista"));
        } else {
            if(idNaturalezaTransportista == 1) {
                if (isNullOrEmpty(ruc)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC del transportista"));
                } else if (isNull(verificadorRuc)) {
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar dígito verificador del RUC del transportista"));
                }
                this.idTipoDocumento = null;
                this.nroDocumento = null;
            } else {
                if (isNull(idTipoDocumento)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de documento del transportista"));
                }
                
                if(isNullOrEmpty(nroDocumento)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de documento del transportista"));
                }
                this.ruc = null;
                this.verificadorRuc = null;
            } 
            
        }
        
        if(isNullOrEmpty(razonSocial)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del transportista"));
        }
        
        if(isNullOrEmpty(direccionChofer)) {
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la dirección del chofer"));
        }
        
        if(isNullOrEmpty(domicilioFiscal)) {
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el domicilio fiscal del transportista"));
        }
        
        if(isNullOrEmpty(nroDocumentoChofer)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de documento del chofer"));
        }
        
        if(isNull(idEmpresa)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit(){
        limpiarErrores();
        
        if(isNull(id)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador del transportista"));
        }
        
        if(isNull(idNaturalezaTransportista)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de naturaleza del transportista"));
        } else {
            if(idNaturalezaTransportista == 1) {
                if (isNullOrEmpty(ruc)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC del transportista"));
                } else if (isNull(verificadorRuc)) {
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar dígito verificador del RUC del transportista"));
                }
                this.idTipoDocumento = null;
                this.nroDocumento = null;
            } else {
                if (isNull(idTipoDocumento)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de documento del transportista"));
                }
                
                if(isNullOrEmpty(nroDocumento)){
                     addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de documento del transportista"));
                }
                this.ruc = null;
                this.verificadorRuc = null;
            } 
            
        }
        
        if(isNullOrEmpty(razonSocial)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del transportista"));
        }
        
        if(isNullOrEmpty(direccionChofer)) {
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la dirección del chofer"));
        }
        
        if(isNullOrEmpty(domicilioFiscal)) {
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el domicilio fiscal del transportista"));
        }
        
        if(isNullOrEmpty(nroDocumentoChofer)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de documento del chofer"));
        }
        
        if(isNull(idEmpresa)){
             addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdNaturalezaTransportista() {
        return idNaturalezaTransportista;
    }

    public void setIdNaturalezaTransportista(Integer idNaturalezaTransportista) {
        this.idNaturalezaTransportista = idNaturalezaTransportista;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Integer getVerificadorRuc() {
        return verificadorRuc;
    }

    public void setVerificadorRuc(Integer verificadorRuc) {
        this.verificadorRuc = verificadorRuc;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDireccionChofer() {
        return direccionChofer;
    }

    public void setDireccionChofer(String direccionChofer) {
        this.direccionChofer = direccionChofer;
    }

    public String getNroDocumentoChofer() {
        return nroDocumentoChofer;
    }

    public void setNroDocumentoChofer(String nroDocumentoChofer) {
        this.nroDocumentoChofer = nroDocumentoChofer;
    }
    
    
}
