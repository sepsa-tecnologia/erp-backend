/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.AutoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.AutoFacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Williams Vera
 */
@Local
public interface AutoFacturaFacade extends Facade<AutoFactura, AutoFacturaParam, UserInfoImpl> {

    public AutoFacturaPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente);
    
    public AutoFacturaPojo editPojo(AutoFacturaParam param, UserInfoImpl userInfo);
//
//    public List<FacturaPojo> findMonto(FacturaParam param);
//
//    public Long findMontoSize(FacturaParam param);
//
//    public FacturaPojo findMontoSumatoria(FacturaParam param);
//
    public AutoFactura anular(AutoFacturaParam param, UserInfoImpl userInfo);
//
    public byte[] getPdfFactura(AutoFacturaParam param, UserInfoImpl userInfo) throws Exception;
//
//    public void actualizarSaldoFactura(Integer idFactura, BigDecimal saldo);
//
//    public byte[] getXlsReporteVenta(FacturaParam param, UserInfoImpl userInfo) throws Exception;
//
//    public byte[] getXlsEstadoCuenta(FacturaParam param, UserInfoImpl userInfo);
//
//    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param);
//
//    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param);
//
//    public Boolean reenviarFacturasRechazadas(FacturaParam param);
//    
//    public List<String> createFacturaMasiva(List<FacturaParam> fpl, List<FacturaDetalleParam> fdpl, UserInfoImpl userInfo);
    
}
