/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de referencia geografica
 * @author Jonathan
 */
public class ReferenciaGeograficaParam extends CommonParam {
    
    public ReferenciaGeograficaParam() {
    }

    public ReferenciaGeograficaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo referencia geografica
     */
    @QueryParam("idTipoReferenciaGeografica")
    private Integer idTipoReferenciaGeografica;
    
    /**
     * Código de tipo referencia geografica
     */
    @QueryParam("codigoTipoReferenciaGeografica")
    private String codigoTipoReferenciaGeografica;
    
    /**
     * Identificador de padre
     */
    @QueryParam("idPadre")
    private Integer idPadre;
    
    /**
     * Código padre
     */
    @QueryParam("codigoPadre")
    private String codigoPadre;
    
    /**
     * Tiene padre
     */
    @QueryParam("tienePadre")
    private Boolean tienePadre;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(idTipoReferenciaGeografica) && isNullOrEmpty(codigoTipoReferenciaGeografica)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de referencia geográfica"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(idTipoReferenciaGeografica) && isNullOrEmpty(codigoTipoReferenciaGeografica)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de referencia geográfica"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoReferenciaGeografica() {
        return idTipoReferenciaGeografica;
    }

    public void setIdTipoReferenciaGeografica(Integer idTipoReferenciaGeografica) {
        this.idTipoReferenciaGeografica = idTipoReferenciaGeografica;
    }

    public String getCodigoTipoReferenciaGeografica() {
        return codigoTipoReferenciaGeografica;
    }

    public void setCodigoTipoReferenciaGeografica(String codigoTipoReferenciaGeografica) {
        this.codigoTipoReferenciaGeografica = codigoTipoReferenciaGeografica;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public void setCodigoPadre(String codigoPadre) {
        this.codigoPadre = codigoPadre;
    }

    public String getCodigoPadre() {
        return codigoPadre;
    }

    public void setTienePadre(Boolean tienePadre) {
        this.tienePadre = tienePadre;
    }

    public Boolean getTienePadre() {
        return tienePadre;
    }
}
