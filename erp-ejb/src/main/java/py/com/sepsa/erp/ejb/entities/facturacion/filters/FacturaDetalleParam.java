/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de factura detalle
 *
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaDetalleParam extends CommonParam {

    public FacturaDetalleParam() {
    }

    public FacturaDetalleParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;

    /**
     * Nro de factura
     */
    @QueryParam("nroFactura")
    private String nroFactura;

    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;

    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;

    /**
     * Porcentaje de gravada
     */
    @QueryParam("porcentajeGravada")
    private BigDecimal porcentajeGravada;

    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private BigDecimal cantidad;

    /**
     * Precio unitario con iva
     */
    @QueryParam("precioUnitarioConIva")
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin iva
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;

    /**
     * Descuento particular unitario
     */
    @QueryParam("descuentoParticularUnitario")
    private BigDecimal descuentoParticularUnitario;

    /**
     * Descuento global unitario
     */
    @QueryParam("descuentoGlobalUnitario")
    private BigDecimal descuentoGlobalUnitario;

    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;

    /**
     * Monto descuento particular
     */
    @QueryParam("montoDescuentoParticular")
    private BigDecimal montoDescuentoParticular;

    /**
     * Monto descuento global
     */
    @QueryParam("montoDescuentoGlobal")
    private BigDecimal montoDescuentoGlobal;

    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;

    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;

    /**
     * Monto Exento Gravado
     */
    @QueryParam("montoExentoGravado")
    private BigDecimal montoExentoGravado;

    /**
     * Identificador de liquidación
     */
    @QueryParam("idLiquidacion")
    private Integer idLiquidacion;

    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Dato adicional
     */
    @QueryParam("datoAdicional")
    private String datoAdicional;

    /**
     * Código DNCP de nivel general
     */
    @QueryParam("codDncpNivelGeneral")
    private String codDncpNivelGeneral;

    /**
     * Código DNCP de nivel específico
     */
    @QueryParam("codDncpNivelEspecifico")
    private String codDncpNivelEspecifico;

    /**
     * Compra pública
     */
    @QueryParam("compraPublica")
    private Character compraPublica;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de línea"));
        }

        if (isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }

        if (isNull(porcentajeGravada)) {
            porcentajeGravada = new BigDecimal(100);
        } else if (porcentajeGravada.compareTo(BigDecimal.ZERO) < 0 || porcentajeGravada.compareTo(new BigDecimal(100)) > 0) {
            addError(MensajePojo.createInstance()
                    .descripcion("El porcentaje de gravada debe ser entre 0 y 100"));
        }

        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }

        if (isNull(descuentoParticularUnitario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el descuento particular unitario"));
        }

        if (isNull(descuentoGlobalUnitario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el descuento global unitario"));
        }

        if (isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de IVA"));
        }

        if (isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }

        if (isNull(montoDescuentoParticular)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de descuento particular"));
        }

        if (isNull(montoDescuentoGlobal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de descuento global"));
        }

        if (isNull(montoExentoGravado)) {
            montoExentoGravado = BigDecimal.ZERO;
        }

        if (isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción del detalle"));
        }
        
        if (isNull(compraPublica)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es una compra pública"));
        } else if (compraPublica.equals('S')) {
            if (isNullOrEmpty(codDncpNivelGeneral)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el código DNCP de nivel general"));
            }
            if (isNullOrEmpty(codDncpNivelEspecifico)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el código DNCP de nivel específico"));
            }
        }
        
        if (isNull(descuentoParticularUnitario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar el porcentaje de descuento unitario."));
        } else if (descuentoParticularUnitario.compareTo(BigDecimal.ZERO) == -1 || descuentoParticularUnitario.compareTo(new BigDecimal("100")) == 1) {
            addError(MensajePojo.createInstance()
                    .descripcion("El porcentaje de descuento unitario debe ser entre 0 y 100"));
        }
        
        if(!tieneErrores()) {
            BigDecimal _montoTotal = precioUnitarioConIva
                    .subtract(montoDescuentoParticular)
                    .subtract(montoDescuentoGlobal)
                    .multiply(cantidad);
            if(_montoTotal.subtract(montoTotal).abs().compareTo(BigDecimal.ONE) >= 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("El cálculo del monto total no corresponde"));
            }
            
            BigDecimal _montoImponible = _montoTotal
                    .multiply(porcentajeGravada)
                    .multiply(new BigDecimal(100))
                    .divide(new BigDecimal(10000).add(new BigDecimal(porcentajeIva).multiply(porcentajeGravada)), 8, RoundingMode.HALF_UP);
            if(_montoImponible.subtract(montoImponible).abs().compareTo(BigDecimal.ONE) >= 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("El cálculo del monto imponible no corresponde"));
            }
            
            BigDecimal _montoIva = _montoImponible
                    .multiply(new BigDecimal(porcentajeIva))
                    .divide(new BigDecimal(100), 8, RoundingMode.HALF_UP);
            if(_montoIva.subtract(montoIva).abs().compareTo(BigDecimal.ONE) >= 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("El cálculo del monto iva no corresponde"));
            }
        }

        return !tieneErrores();
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(BigDecimal porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCompraPublica(Character compraPublica) {
        this.compraPublica = compraPublica;
    }

    public Character getCompraPublica() {
        return compraPublica;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }
}
