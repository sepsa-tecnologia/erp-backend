/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 * POJO para la entidad contacto email
 * @author Jonathan
 */
public class PersonaEmailPojo {

    public PersonaEmailPojo() {
    }

    public PersonaEmailPojo(Integer idContacto, String contacto, Integer idEmail, String email) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idEmail = idEmail;
        this.email = email;
    }
    
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    
    /**
     * Persona
     */
    private String persona;
    
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    
    /**
     * Contacto
     */
    private String contacto;
    
    /**
     * Identificador de email
     */
    private Integer idEmail;
    
    /**
     * Email
     */
    private String email;
    
    /**
     * Identificador de tipo de email
     */
    private Integer idTipoEmail;
    
    /**
     * Tipo email
     */
    private String tipoEmail;
    
    /**
     * Principal
     */
    private Character principal;
    
    /**
     * Identificador de tipo de contacto
     */
    private Integer idTipoContacto;
    
    /**
     * Tipo contacto
     */
    private String tipoContacto;
    
    /**
     * Identificador de cargo
     */
    private Integer idCargo;
    
    /**
     * Cargo
     */
    private String cargo;
    
    /**
     * Estado
     */
    private Character estado;

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public String getTipoContacto() {
        return tipoContacto;
    }

    public void setTipoContacto(String tipoContacto) {
        this.tipoContacto = tipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public void setTipoEmail(String tipoEmail) {
        this.tipoEmail = tipoEmail;
    }

    public String getTipoEmail() {
        return tipoEmail;
    }

    public void setIdTipoEmail(Integer idTipoEmail) {
        this.idTipoEmail = idTipoEmail;
    }

    public Integer getIdTipoEmail() {
        return idTipoEmail;
    }

    public void setPrincipal(Character principal) {
        this.principal = principal;
    }

    public Character getPrincipal() {
        return principal;
    }
}
