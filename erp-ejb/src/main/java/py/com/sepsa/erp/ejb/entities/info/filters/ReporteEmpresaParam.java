/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de parámetros de reporte empresa
 *
 * @author Jonathan
 */
public class ReporteEmpresaParam extends CommonParam {

    public ReporteEmpresaParam() {
    }

    public ReporteEmpresaParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de reporte
     */
    @QueryParam("idReporte")
    private Integer idReporte;

    /**
     * Código de reporte
     */
    @QueryParam("codigoReporte")
    private String codigoReporte;

    /**
     * Descripción de reporte
     */
    @QueryParam("descripcionReporte")
    private String descripcionReporte;

    /**
     * Parámetros
     */
    @QueryParam("jasper")
    private String jasper;

    /**
     * Parámetros
     */
    private JsonElement parametros;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idReporte) && isNullOrEmpty(codigoReporte)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador o código de reporte"));
        }

        if (isNullOrEmpty(jasper)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el jasper"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        return !tieneErrores();
    }
    
    public boolean isValidToGenerateReport() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de reporte de empresa"));
        }
        
        if(isNull(parametros)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se deben indicar los parámetros"));
        }
        
        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNull(idReporte) && isNullOrEmpty(codigoReporte)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador o código de reporte"));
        }

        if (isNullOrEmpty(jasper)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el jasper"));
        }

        if (isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        return !tieneErrores();
    }

    public void setJasper(String jasper) {
        this.jasper = jasper;
    }

    public String getJasper() {
        return jasper;
    }

    public void setCodigoReporte(String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }

    public String getCodigoReporte() {
        return codigoReporte;
    }

    public void setIdReporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    public Integer getIdReporte() {
        return idReporte;
    }

    public void setParametros(JsonElement parametros) {
        this.parametros = parametros;
    }

    public JsonElement getParametros() {
        return parametros;
    }

    public void setDescripcionReporte(String descripcionReporte) {
        this.descripcionReporte = descripcionReporte;
    }

    public String getDescripcionReporte() {
        return descripcionReporte;
    }

}
