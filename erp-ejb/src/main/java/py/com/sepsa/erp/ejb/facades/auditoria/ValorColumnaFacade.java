/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.Map;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.auditoria.AuditoriaDetalle;
import py.com.sepsa.erp.ejb.entities.auditoria.ValorColumna;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.ValorColumnaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ValorColumnaFacade extends Facade<ValorColumna, ValorColumnaParam, UserInfoImpl> {

    public AuditoriaDetalle nuevaInstancia(String codigo, Map<String, String> map, UserInfoImpl userInfo);

    public AuditoriaDetalle editarInstancia(Integer idAuditoria, String codigo, Map<String, String> map, UserInfoImpl userInfo);
    
}
