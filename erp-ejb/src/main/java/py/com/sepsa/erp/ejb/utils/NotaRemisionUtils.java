/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.input.BOMInputStream;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.NaturalezaClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionNr;
import py.com.sepsa.erp.ejb.entities.facturacion.NaturalezaTransportista;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LocalEntregaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LocalSalidaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaRemisionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TransportistaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.VehiculoTrasladoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.info.ReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.TipoReferenciaGeografica;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.TipoReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Antonella Lucero
 */
public class NotaRemisionUtils {

    /**
     * Método para devolver la lista de notas de remisión
     */
    public static List<NotaRemisionParam> convertToList(Facades facades, @MultipartForm NotaRemisionParam param){
        List<NotaRemisionParam> lista = new ArrayList<>();
        try {
            List<NotaRemisionParam> nrl = new ArrayList<>();
            InputStreamReader input = new InputStreamReader(new BOMInputStream(new ByteArrayInputStream(param.getUploadedFileBytes()), false),StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                Pattern pattern = Pattern.compile(";|,");
                Matcher mather = pattern.matcher(line);

                if (mather.find() == true) {
                    try{
                        String strArray[] = line.split(";|,");
                        if (strArray.length>0){
                            lineas.add(line);
                        } 
                    } catch(Exception e) {
                        break;
                    }  
                }
            }
            for (int i = 0; i < lineas.size(); i++) {
                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    NotaRemisionParam nInicial = new NotaRemisionParam();
                    int errores = 0;
                    String[] stringArr = lineas.get(i).split(";|,");
                    String tipoLinea = stringArr.length <= 0 ? "" : stringArr[0];
                    String timb = stringArr.length <= 1 ? "" : stringArr[1];
                    String nroN = stringArr.length <= 2 ? "" : stringArr[2];
                    if (timb != null && !timb.trim().isEmpty()) {
                        nInicial.setTimbrado(timb);
                    } else {
                        lista.add(nInicial);
                        nInicial.setTimbrado("-");
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de timbrado de la nota de remisión"));
                        errores += 1;
                    }
                    if (nroN != null && !nroN.trim().isEmpty()) {
                        nInicial.setNroNotaRemision(nroN);
                    } else {
                        nInicial.setNroNotaRemision("-");
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de la nota de remisión"));
                        errores += 1;
                    }
                    if (!(tipoLinea != null && !tipoLinea.trim().isEmpty())) {
                        if (!lista.contains(nInicial)) {
                            lista.add(nInicial);
                        }
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar un Tipo de Línea"));
                        errores += 1;
                        continue;
                    } else {
                        if (errores > 0) {
                            continue;
                        }
                        //Datos de Cabecera
                        String timbrado = null;
                        String nroNRemision = null;
                        String fechaNr = null;
                        String nroDocumento = null;
                        String razonSocial = null;
                        String codSeguridad = null;
                        String direccion = null;
                        String codDepartamento = null;
                        String codDistrito = null;
                        String codCiudad = null;
                        String nroCasa = null;
                        String codMotivoEmision = null;
                        String respNotaRemision = null;
                        String kmRecorridos = null;
                        String fechaFactura = null;
                        String tipoTransporte = null;
                        String modalidadTransporte = null;
                        String respFlete = null;
                        String fechaInicioTranslado = null;
                        String fechaFinTranslado = null;
                        String direccionLocalOrigen = null;
                        String nroCasaLocalOrigen = null;
                        String codDepartamentoLocalOrigen = null;
                        String codDistritoLocalOrigen = null;
                        String codCiudadLocalOrigen = null;
                        String direccionLocalDestino = null;
                        String nroCasaLocalDestino = null;
                        String codDepartamentoLocalDestino = null;
                        String codDistritoLocalDestino = null;
                        String codCiudadLocalDestino = null;
                        String tipoVehiculo = null;
                        String marca = null;
                        String tipoIdentificacionVehiculo = null;
                        String identificacionVehiculo = null;
                        String naturalezaTransportista = null;
                        String razonSocialTransportista = null;
                        String nroDocumentoTransportista = null;
                        String ciChofer = null;
                        String nombreChofer = null;
                        String domicilioTransportista = null;
                        String domicilioChofer = null;
                        //Datos de Detalle
                        String timbradoDetalle = null;
                        String nroNRemisionDetalle = null;
                        String codProducto = null;
                        String cantidad = null;
                        String descripcion = null;
                        NotaRemisionParam nr = new NotaRemisionParam();
                        NotaRemisionDetalleParam nrd = new NotaRemisionDetalleParam();
                        switch (tipoLinea) {
                            case "1":
                                //TODO cabecera
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroNRemision = (String) stringArr[k];
                                            break;
                                        case 3:
                                            fechaNr = (String) stringArr[k];
                                            break;
                                        case 4:
                                            nroDocumento = (String) stringArr[k];
                                            break;
                                        case 5:
                                            razonSocial = (String) stringArr[k];
                                            break;
                                        case 6:
                                            codSeguridad = (String) stringArr[k];
                                            break;
                                        case 7:
                                            direccion = (String) stringArr[k];
                                            break;
                                        case 8:
                                            codDepartamento = (String) stringArr[k];
                                            break;
                                        case 9:
                                            codDistrito = (String) stringArr[k];
                                            break;
                                        case 10:
                                            codCiudad = (String) stringArr[k];
                                            break;
                                        case 11:
                                            nroCasa = (String) stringArr[k];
                                            break;
                                        case 12:
                                            codMotivoEmision = (String) stringArr[k];
                                            break;
                                        case 13:
                                            respNotaRemision = (String) stringArr[k];
                                            break;
                                        case 14:
                                            kmRecorridos = (String) stringArr[k];
                                            break;
                                        case 15:
                                            fechaFactura = (String) stringArr[k];
                                            break;
                                        case 16:
                                            tipoTransporte = (String) stringArr[k];
                                            break;
                                        case 17:
                                            modalidadTransporte = (String) stringArr[k];
                                            break;
                                        case 18:
                                            respFlete = (String) stringArr[k];
                                            break;
                                        case 19:
                                            fechaInicioTranslado = (String) stringArr[k];
                                            break;
                                        case 20:
                                            fechaFinTranslado = (String) stringArr[k];
                                            break;
                                        case 21:
                                            direccionLocalOrigen = (String) stringArr[k];
                                            break;
                                        case 22:
                                            nroCasaLocalOrigen = (String) stringArr[k];
                                            break;
                                        case 23:
                                            codDepartamentoLocalOrigen = (String) stringArr[k];
                                            break;
                                        case 24:
                                            codDistritoLocalOrigen = (String) stringArr[k];
                                            break;
                                        case 25:
                                            codCiudadLocalOrigen = (String) stringArr[k];
                                            break;
                                        case 26:
                                            direccionLocalDestino = (String) stringArr[k];
                                            break;
                                        case 27:
                                            nroCasaLocalDestino = (String) stringArr[k];
                                            break;
                                        case 28:
                                            codDepartamentoLocalDestino = (String) stringArr[k];
                                            break;
                                        case 29:
                                            codDistritoLocalDestino = (String) stringArr[k];
                                            break;
                                        case 30:
                                            codCiudadLocalDestino = (String) stringArr[k];
                                            break;
                                        case 31:
                                            tipoVehiculo = (String) stringArr[k];
                                            break;
                                        case 32:
                                            marca = (String) stringArr[k];
                                            break;
                                        case 33:
                                            tipoIdentificacionVehiculo = (String) stringArr[k];
                                            break;
                                        case 34:
                                            identificacionVehiculo = (String) stringArr[k];
                                            break;
                                        case 35:
                                            naturalezaTransportista = (String) stringArr[k];
                                            break;
                                        case 36:
                                            razonSocialTransportista = (String) stringArr[k];
                                            break;
                                        case 37:
                                            nroDocumentoTransportista = (String) stringArr[k];
                                            break;
                                        case 38:
                                            ciChofer = (String) stringArr[k];
                                            break;
                                        case 39:
                                            nombreChofer = (String) stringArr[k];
                                            break;
                                        case 40:
                                            domicilioTransportista = (String) stringArr[k];
                                            break;
                                        case 41:
                                            domicilioChofer = (String) stringArr[k];
                                            break;
                                    }
                                }
                                nr.setArchivoSet('S');
                                nr.setIdEmpresa(param.getIdEmpresa());
                                nr.setTimbrado(timbrado);
                                nr.setNroNotaRemision(nroNRemision);
                                if (nr.getNroNotaRemision() != null) {
                                    if (nr.getNroNotaRemision().length() != 15 || !(nr.getNroNotaRemision().matches("^[0-9]{3}[\\-][0-9]{3}[\\-][0-9]{7}$"))) {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("El formato del nro de nota de remisión de la cabecera es invalido"));
                                        continue;
                                    }
                                }
                                nr.setAnulado('N');
                                nr.setObservacion("//");
                                if (fechaNr != null && !fechaNr.trim().isEmpty()) {
                                    nr.setFecha(new SimpleDateFormat("dd/MM/yyyy").parse(fechaNr));
                                }
                                nr.setRuc(nroDocumento.replace(".", ""));
                                nr.setRazonSocial(razonSocial);
                                if (nr.getRazonSocial() != null && !nr.getRazonSocial().trim().isEmpty() && nr.getRuc() != null && !nr.getRuc().trim().isEmpty()) {
                                    if(nr.getRuc().contains("-")){
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if(n!=null){
                                            nr.setIdNaturalezaCliente(n.getId());
                                        }
                                    }else{
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("NO_CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if(n!=null){
                                            nr.setIdNaturalezaCliente(n.getId());
                                        }
                                    }
                                    ClienteParam paramc = new ClienteParam();
                                    paramc.setIdEmpresa(nr.getIdEmpresa());
                                    paramc.setNroDocumentoEq(nr.getRuc());
                                    paramc.setRazonSocial(nr.getRazonSocial());
                                    Cliente cliente = facades.getClienteFacade().findFirst(paramc);
                                    if (cliente != null) {
                                        nr.setIdCliente(cliente.getIdCliente());
                                        nr.setIdNaturalezaCliente(cliente.getIdNaturalezaCliente());
                                    }
                                }
                                nr.setCodSeguridad(codSeguridad);
                                nr.setDireccion(direccion);
                                nr.setNroCasa(nroCasa);
                                if(codDepartamento!= null && !codDepartamento.trim().isEmpty() && codDistrito!= null && !codDistrito.trim().isEmpty() && codCiudad!= null && !codCiudad.trim().isEmpty()){
                                    ArrayList<Integer> direcciones = obtenerDirecciones(facades, codDepartamento, codDistrito, codCiudad);
                                    if(direcciones!=null){
                                        nr.setIdDepartamento(direcciones.get(0));
                                        nr.setIdDistrito(direcciones.get(1));
                                        nr.setIdCiudad(direcciones.get(2));
                                    }else{
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Se deben de ingresar correctamente las referencias geográficas de la cabecera"));
                                        continue;
                                    }   
                                }
                                
                                if (codMotivoEmision != null && !codMotivoEmision.trim().isEmpty()) {
                                    MotivoEmisionNr motivoEmision = facades.getMotivoEmisionNrFacade().find(nr.getIdMotivoEmisionNr(), codMotivoEmision);
                                    if (motivoEmision != null) {
                                        nr.setIdMotivoEmisionNr(motivoEmision.getId());
                                    } else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Mótivo de Emisión de Nota de Remisión Inexistente"));
                                        continue;
                                    }

                                }
                                if (respNotaRemision != null && !respNotaRemision.trim().isEmpty()) {
                                    int resp = Integer.parseInt(respNotaRemision);
                                    if(resp>=1 && resp<=5){
                                        nr.setResponsableEmision(resp);
                                    }else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Responsable de Emisión Inexistente"));
                                        continue;
                                    }
                                }
                                if (kmRecorridos != null && !kmRecorridos.trim().isEmpty()) {
                                    try {
                                        nr.setKm(new BigDecimal(kmRecorridos.trim().replace(" ", "")));
                                    } catch (NumberFormatException e) {
                                        String kmU = kmRecorridos.replace("\"", "").trim().replace(" ", "");
                                        DecimalFormat decimalFormat = new DecimalFormat();
                                        decimalFormat.setParseBigDecimal(true);
                                        Number parsedKm = decimalFormat.parse(kmU);
                                        BigDecimal bdKm = (BigDecimal) parsedKm;
                                        nr.setKm(bdKm);
                                    }
                                }
                                if (fechaFactura != null && !fechaFactura.trim().isEmpty()) {
                                    nr.setFechaFuturaEmision(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFactura));
                                }
                                nr.setTieneFactura('N');
                                if (tipoTransporte != null && !tipoTransporte.trim().isEmpty()) {
                                    int tipoT = Integer.parseInt(tipoTransporte);
                                    if(tipoT>=1 && tipoT<=2){
                                        nr.setTipoTransporte(tipoT);
                                    }else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Tipo de Transporte Inexistente"));
                                        continue;
                                    }
                                }
                                if (modalidadTransporte != null && !modalidadTransporte.trim().isEmpty()) {
                                    int modT = Integer.parseInt(modalidadTransporte);
                                    if(modT>=1 && modT<=3){
                                        nr.setModalidadTransporte(modT);
                                    }else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Modalidad de Transporte Inexistente"));
                                        continue;
                                    }
                                }
                                if (respFlete != null && !respFlete.trim().isEmpty()) {
                                    int respF = Integer.parseInt(respFlete);
                                    if(respF>=1 && respF<=5){
                                        nr.setResponsableCostoFlete(respF);
                                    }else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Responsable de Costo Flete Inexistente"));
                                        continue;
                                    }
                                }
                                if (fechaInicioTranslado != null && !fechaInicioTranslado.trim().isEmpty()) {
                                    nr.setFechaInicioTraslado(new SimpleDateFormat("dd/MM/yyyy").parse(fechaInicioTranslado));
                                }
                                if (fechaFinTranslado != null && !fechaFinTranslado.trim().isEmpty()) {
                                    nr.setFechaFinTraslado(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFinTranslado));        
                                }
                                
                                if (direccionLocalOrigen != null && !direccionLocalOrigen.trim().isEmpty() && nroCasaLocalOrigen!=null && !nroCasaLocalOrigen.trim().isEmpty()) {
                                    LocalEntregaParam origen = new LocalEntregaParam();
                                    nr.setLocalEntrega(origen);
                                    nr.getLocalEntrega().setDireccion(direccionLocalOrigen);
                                    nr.getLocalEntrega().setNroCasa(Integer.parseInt(nroCasaLocalOrigen));
                                    if (codDepartamentoLocalOrigen != null && !codDepartamentoLocalOrigen.trim().isEmpty() && codDistritoLocalOrigen != null && !codDistritoLocalOrigen.trim().isEmpty() && codCiudadLocalOrigen != null && !codCiudadLocalOrigen.trim().isEmpty()) {
                                        ArrayList<Integer> direcciones = obtenerDirecciones(facades, codDepartamentoLocalOrigen, codDistritoLocalOrigen, codCiudadLocalOrigen);
                                        if (direcciones != null) {
                                            nr.getLocalEntrega().setIdDepartamento(direcciones.get(0));
                                            nr.getLocalEntrega().setIdDistrito(direcciones.get(1));
                                            nr.getLocalEntrega().setIdCiudad(direcciones.get(2));
                                        } else {
                                            lista.add(nr);
                                            nr.addError(MensajePojo.createInstance().descripcion("Se deben de ingresar correctamente las referencias geográficas del Local de Origen"));
                                            continue;
                                        }
                                    }
                                }
                                
                                if (direccionLocalDestino != null && !direccionLocalDestino.trim().isEmpty() && nroCasaLocalDestino!=null && !nroCasaLocalDestino.trim().isEmpty()) {
                                    LocalSalidaParam destino = new LocalSalidaParam();
                                    nr.setLocalSalida(destino);
                                    nr.getLocalSalida().setDireccion(direccionLocalDestino);
                                    nr.getLocalSalida().setNroCasa(Integer.parseInt(nroCasaLocalDestino));
                                    if (codDepartamentoLocalDestino != null && !codDepartamentoLocalDestino.trim().isEmpty() && codDistritoLocalDestino != null && !codDistritoLocalDestino.trim().isEmpty() && codCiudadLocalDestino != null && !codCiudadLocalDestino.trim().isEmpty()) {
                                        ArrayList<Integer> direcciones = obtenerDirecciones(facades, codDepartamentoLocalDestino, codDistritoLocalDestino, codCiudadLocalDestino);
                                        if (direcciones != null) {
                                            nr.getLocalSalida().setIdDepartamento(direcciones.get(0));
                                            nr.getLocalSalida().setIdDistrito(direcciones.get(1));
                                            nr.getLocalSalida().setIdCiudad(direcciones.get(2));
                                        } else {
                                            lista.add(nr);
                                            nr.addError(MensajePojo.createInstance().descripcion("Se deben de ingresar correctamente las referencias geográficas del Local de Destino"));
                                            continue;
                                        }
                                    }
                                }
                                
                                VehiculoTrasladoParam vehiculo = new VehiculoTrasladoParam();
                                nr.setVehiculoTraslado(vehiculo);
                                if (tipoVehiculo != null && !tipoVehiculo.trim().isEmpty()) {
                                    if(tipoVehiculo.equalsIgnoreCase("CARGA") || tipoVehiculo.equalsIgnoreCase("FLUVIAL") || tipoVehiculo.equalsIgnoreCase("AEREO") || tipoVehiculo.equalsIgnoreCase("MULTIMODAL")){
                                        nr.getVehiculoTraslado().setTipoVehiculo(tipoVehiculo);
                                    } else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Tipo de Vehículo Inexistente"));
                                        continue;
                                    }
                                }
                                if (marca != null && marca.length() <= 15) {
                                    nr.getVehiculoTraslado().setMarca(marca);
                                }
                                if (tipoIdentificacionVehiculo != null && !tipoIdentificacionVehiculo.trim().isEmpty()) {
                                    nr.getVehiculoTraslado().setTipoIdentificacion(Integer.parseInt(tipoIdentificacionVehiculo));
                                    if (nr.getVehiculoTraslado().getTipoIdentificacion() >= 1 && nr.getVehiculoTraslado().getTipoIdentificacion() <= 2) {
                                        switch (nr.getVehiculoTraslado().getTipoIdentificacion()) {
                                            case 1:
                                                nr.getVehiculoTraslado().setNroIdentificacion(identificacionVehiculo);
                                                break;
                                            case 2:
                                                nr.getVehiculoTraslado().setNroMatricula(identificacionVehiculo);
                                                break;
                                        }
                                    } else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Tipo de Identificación de Vehículo Inexistente"));
                                        continue;
                                    }
                                }
                                TransportistaParam transportista = new TransportistaParam();
                                nr.setTransportista(transportista);
                                if(naturalezaTransportista!=null && !naturalezaTransportista.trim().isEmpty()){
                                    NaturalezaTransportista natTrans = facades.getNaturalezaTransportistaFacade()
                                            .find(nr.getTransportista().getIdNaturalezaTransportista(), nr.getTransportista().getCodigo());
                                    if (natTrans != null) {
                                        nr.getTransportista().setIdNaturalezaTransportista(natTrans.getId());
                                    }else{
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Naturaleza de Transportista Inexistente"));
                                        continue;
                                    }
                                    if(naturalezaTransportista.equalsIgnoreCase("CONTRIBUYENTE")){    
                                        if(nroDocumentoTransportista != null && !nroDocumentoTransportista.trim().isEmpty()&&nroDocumentoTransportista.length()>=3 && nroDocumentoTransportista.length()<=10 && nroDocumentoTransportista.matches("^[0-9]+(-[0-9])?$")){
                                            String[] ruc = nroDocumentoTransportista.replace(".", "").split("-");
                                            nr.getTransportista().setRuc(ruc[0]);
                                            nr.getTransportista().setVerificadorRuc(Integer.parseInt(ruc[1]));
                                        }else{
                                            lista.add(nr);
                                            nr.addError(MensajePojo.createInstance().descripcion("RUC del Transportista Inválido"));
                                            continue;
                                        }
                                    }else if(naturalezaTransportista.equalsIgnoreCase("NO_CONTRIBUYENTE")){
                                        if(nroDocumentoTransportista != null && !nroDocumentoTransportista.trim().isEmpty() && nroDocumentoTransportista.length()>=1 && nroDocumentoTransportista.length()<=20){
                                            nr.getTransportista().setNroDocumento(nroDocumentoTransportista.replace(".", ""));
                                            nr.getTransportista().setVerificadorRuc(null);
                                        }else{
                                            lista.add(nr);
                                            nr.addError(MensajePojo.createInstance().descripcion("Nro de documento del Transportista Inválido"));
                                            continue;
                                        }
                                    }
                                }
                                if(razonSocialTransportista!=null && !razonSocialTransportista.trim().isEmpty()){
                                    nr.getTransportista().setRazonSocial(razonSocialTransportista);
                                }
                                if(ciChofer!=null && !ciChofer.trim().isEmpty() && ciChofer.length()>=1 && ciChofer.length()<=20){
                                    nr.getTransportista().setNroDocumentoChofer(ciChofer.replace(".", ""));
                                }
                                if(nombreChofer!=null && !nombreChofer.trim().isEmpty()){
                                    nr.getTransportista().setNombreCompleto(nombreChofer);
                                }
                                if(domicilioChofer!=null && !domicilioChofer.trim().isEmpty()){
                                    nr.getTransportista().setDireccionChofer(domicilioChofer);
                                }
                                if(domicilioTransportista!=null && !domicilioTransportista.trim().isEmpty()){
                                    nr.getTransportista().setDomicilioFiscal(domicilioTransportista);
                                }
                                UsuarioParam uparam = new UsuarioParam();
                                uparam.setId(param.getIdUsuario());
                                UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);
                                nr.setIdUsuario(usuario.getId());
                                TalonarioPojo talonario = null;
                                if (nr.getNroNotaRemision() != null && !nr.getNroNotaRemision().trim().isEmpty() && nr.getTimbrado() != null && !nr.getTimbrado().trim().isEmpty()) {
                                    TalonarioParam param2 = new TalonarioParam();
                                    param2.setIdEmpresa(nr.getIdEmpresa());
                                    param2.setTimbrado(nr.getTimbrado());
                                    param2.setActivo('S');
                                    param2.setNroSucursal(nr.getNroNotaRemision().substring(0, 3));
                                    param2.setNroPuntoVenta(nr.getNroNotaRemision().substring(4, 7));
                                    param2.setIdTipoDocumento(4);
                                    talonario = facades.getTalonarioFacade().findFirstPojo(param2);
                                    if (talonario != null) {
                                        nr.setIdTalonario(talonario.getId());
                                        nr.setDigital(talonario.getDigital());
                                    } else {
                                        lista.add(nr);
                                        nr.addError(MensajePojo.createInstance().descripcion("Nro de timbrado, nro de sucursal y nro de punto de venta inexistentes"));
                                        continue;
                                    }
                                }

                                nrl.add(nr);
                                break;

                            case "2":
                                //TODO detalle
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbradoDetalle = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroNRemisionDetalle = (String) stringArr[k];
                                            break;
                                        case 3:
                                            codProducto = (String) stringArr[k];
                                            break;
                                        case 4:
                                            cantidad = (String) stringArr[k];
                                            break;
                                        case 5:
                                            descripcion = (String) stringArr[k];
                                            break;
                                    }
                                }
                                //guardar el detalle a la nc correspondiente   
                                boolean coincide = false;
                                for (NotaRemisionParam n : nrl) {
                                    if (n.getTimbrado() != null && !n.getTimbrado().trim().isEmpty() && timbradoDetalle != null && !timbradoDetalle.trim().isEmpty() && n.getNroNotaRemision() != null && !n.getNroNotaRemision().trim().isEmpty() && nroNRemisionDetalle != null && !nroNRemisionDetalle.trim().isEmpty()) {
                                        if (n.getTimbrado().equalsIgnoreCase(timbradoDetalle) && n.getNroNotaRemision().equalsIgnoreCase(nroNRemisionDetalle)) {
                                            coincide = true;
                                            n.setDescripcion(descripcion);
                                            n.getNotaRemisionDetalles().add(nrd);
                                            if (!lista.contains(n)) {
                                                lista.add(n);
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (coincide == false) {
                                    NotaRemisionParam nError = new NotaRemisionParam();
                                    if (nroNRemisionDetalle != null && !nroNRemisionDetalle.trim().isEmpty()) {
                                        nError.setNroNotaRemision(nroNRemisionDetalle);
                                    } else {
                                        nError.setNroNotaRemision("-");
                                    }
                                    if (timbradoDetalle != null && !timbradoDetalle.trim().isEmpty()) {
                                        nError.setTimbrado(timbradoDetalle);
                                    } else {
                                        nError.setTimbrado("-");
                                    }
                                    lista.add(nError);
                                    nError.addError(MensajePojo.createInstance().descripcion("El detalle no coincide con ninguna cabecera"));
                                    continue;
                                }
                                nrd.setIdEmpresa(param.getIdEmpresa());
                               
                                if (codProducto != null && !codProducto.trim().isEmpty()) {
                                    ProductoParam prod = new ProductoParam();
                                    prod.setCodigoInternoEq(codProducto);
                                    prod.setIdEmpresa(param.getIdEmpresa());
                                    prod.setActivo('S');
                                    ProductoPojo producto = facades.getProductoFacade().findFirstPojo(prod);
                                    if (producto == null) {
                                        nrd.addError(MensajePojo.createInstance().descripcion("Producto no encontrado, verificar código interno"));
                                        continue;
                                    } else {
                                        nrd.setIdProducto(producto.getId());
                                    }
                                } else {
                                    nrd.addError(MensajePojo.createInstance().descripcion("Se debe indicar el código interno del producto"));
                                    continue;
                                }
                                nrd.setDescripcion(descripcion);
                                if (cantidad != null && !cantidad.trim().isEmpty()) {
                                    nrd.setCantidad(new BigDecimal(cantidad));
                                }
                                break;
                            default:
                                if (!lista.contains(nInicial)) {
                                    lista.add(nInicial);
                                }
                                nInicial.addError(MensajePojo.createInstance().descripcion("Tipo de línea únicamente puede ser 1 o 2"));
                                break;
                        }
                    }
                } 
            }
            
            for (NotaRemisionParam n : nrl) {
                Integer k = 1;
                for (NotaRemisionDetalleParam nD : n.getNotaRemisionDetalles()) {
                    nD.setNroLinea(k);
                    k++;
                }
            }
            return lista;
        } catch (Exception ex) {
            System.out.println(ex);
            return lista;

        } 
    }
    /**
     *  método para obtener los id de los departamentos, distritos, ciudades
     * */
    public static ArrayList<Integer> obtenerDirecciones (Facades facades, String departamento, String distrito, String ciudad){
        ArrayList<Integer> direcciones = new ArrayList<>();
        int errores = 0;
        //departamento
        ReferenciaGeograficaParam result1 = new ReferenciaGeograficaParam();
        result1.setCodigo(departamento);
        result1.setIdPadre(null);
        TipoReferenciaGeograficaParam dept = new TipoReferenciaGeograficaParam();
        dept.setCodigo("DEPARTAMENTO");
        dept.setDescripcion("Departamento");
        TipoReferenciaGeografica d = facades.getTipoReferenciaGeograficaFacade().findFirst(dept);
        result1.setIdTipoReferenciaGeografica(d.getId());
        result1.setCodigoTipoReferenciaGeografica(d.getCodigo());
        ReferenciaGeografica depto = facades.getReferenciaGeograficaFacade().findFirst(result1);
        if(depto!=null){
            direcciones.add(depto.getId());
            //distrito
            ReferenciaGeograficaParam result2 = new ReferenciaGeograficaParam();
            result2.setCodigo(distrito);
            result2.setIdPadre(depto.getId());
            TipoReferenciaGeograficaParam dist = new TipoReferenciaGeograficaParam();
            dist.setCodigo("DISTRITO");
            dist.setDescripcion("Distrito");
            TipoReferenciaGeografica di = facades.getTipoReferenciaGeograficaFacade().findFirst(dist);
            result2.setIdTipoReferenciaGeografica(di.getId());
            result2.setCodigoTipoReferenciaGeografica(di.getCodigo());
            ReferenciaGeografica distri = facades.getReferenciaGeograficaFacade().findFirst(result2);
            if (distri != null) {
                direcciones.add(distri.getId());
                //ciudad
                ReferenciaGeograficaParam result3 = new ReferenciaGeograficaParam();
                result3.setCodigo(ciudad);
                result3.setIdPadre(distri.getId());
                TipoReferenciaGeograficaParam ciu = new TipoReferenciaGeograficaParam();
                ciu.setCodigo("CIUDAD");
                ciu.setDescripcion("Ciudad");
                TipoReferenciaGeografica ci = facades.getTipoReferenciaGeograficaFacade().findFirst(ciu);
                result3.setIdTipoReferenciaGeografica(ci.getId());
                result3.setCodigoTipoReferenciaGeografica(ci.getCodigo());
                ReferenciaGeografica city = facades.getReferenciaGeograficaFacade().findFirst(result3);
                if (city != null) {
                    direcciones.add(city.getId());
                } else {
                    errores += 1;
                }
            } else {
                errores += 1;
            }
        } else {
            errores += 1;
        }
        if (errores > 0) {
            return null;
        }
        return direcciones;
    }
}
