/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.EncargadoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Local
public interface UsuarioFacade extends Facade<Usuario, UsuarioParam, UserInfoImpl> {

    public UsuarioPojo findPojoCompleto(UsuarioPojo usuario);

    public Usuario findUsuario(String userName);

    public UsuarioPojo findUsuarioPojo(String userName);

    public Usuario findHash(String hash);

    public Usuario findUsuario(Integer userId);

    public Boolean validToCreate(UsuarioParam param);

    public Boolean validToEdit(UsuarioParam param);

    public List<EncargadoPojo> findEncargados(CommonParam param);

    public Long findEncargadosSize(CommonParam param);

    public boolean validToSendRecover(UsuarioParam param);

    public boolean validToRecover(UsuarioParam param) throws Exception;

    public Boolean notificarRecuperacion(UsuarioParam param);

    public Boolean enviarCorreoRecuperacion(UsuarioParam param) throws Exception;

    public Boolean recuperarContrasena(UsuarioParam param) throws Exception;
    
    public Boolean validarHash(UsuarioParam param) throws Exception;
    
    public List<UsuarioPojo> bloquearUsuarios(Integer idClienteSepsa, UserInfoImpl userInfo);
    
    public List<UsuarioPojo> desbloquearUsuarios(Integer idClienteSepsa, UserInfoImpl userInfo);
    
}
