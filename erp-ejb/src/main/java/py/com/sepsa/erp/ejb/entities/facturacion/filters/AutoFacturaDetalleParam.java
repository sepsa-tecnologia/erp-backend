/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de factura detalle
 *
 * @author Jonathan D. Bernal Fernández
 */
public class AutoFacturaDetalleParam extends CommonParam {

    public AutoFacturaDetalleParam() {
    }

    public AutoFacturaDetalleParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de factura
     */
    @QueryParam("idAutofactura")
    private Integer idAutofactura;

    /**
     * Nro de factura
     */
    @QueryParam("nroFactura")
    private String nroFactura;

    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;

    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;

    /**
     * Porcentaje de gravada
     */
    @QueryParam("porcentajeGravada")
    private Integer porcentajeGravada;

    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private BigDecimal cantidad;

    /**
     * Precio unitario con iva
     */
    @QueryParam("precioUnitario")
    private BigDecimal precioUnitario;

    /**
     * Precio unitario sin iva
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;

    /**
     * Descuento particular unitario
     */
    @QueryParam("descuentoParticularUnitario")
    private BigDecimal descuentoParticularUnitario;

    /**
     * Descuento global unitario
     */
    @QueryParam("descuentoGlobalUnitario")
    private BigDecimal descuentoGlobalUnitario;

    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;

    /**
     * Monto descuento particular
     */
    @QueryParam("montoDescuentoParticular")
    private BigDecimal montoDescuentoParticular;

    /**
     * Monto descuento global
     */
    @QueryParam("montoDescuentoGlobal")
    private BigDecimal montoDescuentoGlobal;

    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;

    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;

    /**
     * Monto Exento Gravado
     */
    @QueryParam("montoExentoGravado")
    private BigDecimal montoExentoGravado;

    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;

    /**
     * Código DNCP de nivel general
     */
    @QueryParam("codDncpNivelGeneral")
    private String codDncpNivelGeneral;

    /**
     * Código DNCP de nivel específico
     */
    @QueryParam("codDncpNivelEspecifico")
    private String codDncpNivelEspecifico;

    /**
     * Compra pública
     */
    @QueryParam("compraPublica")
    private Character compraPublica;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idAutofactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de línea"));
        }


     

        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

    

      



        if (isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }

        if (isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }

     

        return !tieneErrores();
    }

    public Integer getIdAutofactura() {
        return idAutofactura;
    }

    public void setIdAutofactura(Integer idAutofactura) {
        this.idAutofactura = idAutofactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }


    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }


    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public void setPorcentajeGravada(Integer porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public Integer getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCompraPublica(Character compraPublica) {
        this.compraPublica = compraPublica;
    }

    public Character getCompraPublica() {
        return compraPublica;
    }
}
