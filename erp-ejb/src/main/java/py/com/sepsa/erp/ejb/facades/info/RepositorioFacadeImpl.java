/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;
import py.com.sepsa.erp.ejb.entities.info.TipoRepositorio;
import py.com.sepsa.erp.ejb.entities.info.filters.RepositorioParam;
import py.com.sepsa.erp.ejb.utils.AwsUtils;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "RepositorioFacade", mappedName = "RepositorioFacade")
@Local(RepositorioFacade.class)
public class RepositorioFacadeImpl extends FacadeImpl<Repositorio, RepositorioParam> implements RepositorioFacade {

    public RepositorioFacadeImpl() {
        super(Repositorio.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(RepositorioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoRepositorio tipoRepositorio = facades
                .getTipoRepositorioFacade()
                .find(param.getIdTipoRepositorio());
        
        if(tipoRepositorio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el tipo de repositorio"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(RepositorioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoRepositorio tipoRepositorio = facades
                .getTipoRepositorioFacade()
                .find(param.getIdTipoRepositorio());
        
        if(tipoRepositorio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el tipo de repositorio"));
        }
        
        Repositorio repositorio = find(param.getId());
        
        if(repositorio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el repositorio"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public Repositorio create(RepositorioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Repositorio repositorio = new Repositorio();
        repositorio.setIdEmpresa(userInfo.getIdEmpresa());
        repositorio.setBucket(param.getBucket());
        repositorio.setClaveAcceso(param.getClaveAcceso());
        repositorio.setClaveSecreta(param.getClaveSecreta());
        repositorio.setActivo(param.getActivo());
        repositorio.setIdTipoRepositorio(param.getIdTipoRepositorio());
        repositorio.setRegion(param.getRegion());
        create(repositorio);
        
        return repositorio;
    }
    
    /**
     * Edita una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public Repositorio edit(RepositorioParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Repositorio repositorio = find(param.getId());
        repositorio.setBucket(param.getBucket());
        repositorio.setClaveAcceso(param.getClaveAcceso());
        repositorio.setClaveSecreta(param.getClaveSecreta());
        repositorio.setActivo(param.getActivo());
        repositorio.setIdTipoRepositorio(param.getIdTipoRepositorio());
        repositorio.setRegion(param.getRegion());
        edit(repositorio);
        
        return repositorio;
    }
    
    /**
     * Obtiene el repositorio asociado a un código
     * @param idEmpresa Identificador de empresa
     * @param codigo Código
     * @return Repositorio
     */
    @Override
    public Repositorio getRepositorio(Integer idEmpresa, String codigo) {
        
        RepositorioParam param1 = new RepositorioParam();
        param1.setIdEmpresa(idEmpresa);
        param1.setCodigoTipoRepositorio(codigo);
        param1.setActivo('S');
        param1.isValidToList();
        
        List<Repositorio> list = find(param1);
        
        if(list == null || list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }
    
    /**
     * Guarda un xml
     * @param repositorio Repositorio
     * @param fileName Nombre de archivo
     * @param contentBase64 Contenido en base 64
     * @param contentType Tipo de contenido
     * @return Estado
     */
    public Boolean guardarBase64(Repositorio repositorio, String fileName, String contentBase64, String contentType) {
        
        if(contentBase64 == null || contentBase64.trim().isEmpty()) {
            return Boolean.FALSE;
        }
        
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] bytes = decoder.decode(contentBase64);
        
        return guardar(repositorio, fileName, bytes, contentType);
    }
    
    public byte[] getFile(Integer idRepositorio, String fileName) throws IOException {
        
        
        RepositorioParam param1 = new RepositorioParam();
        param1.setId(idRepositorio);
        param1.isValidToList();
        
        List<Repositorio> list = find(param1);
        
        if(list == null || list.isEmpty()) {
            return null;
        }
        
        Repositorio repositorio = list.get(0);
        
        return AwsUtils.getFile(repositorio, fileName);
    }
    
    /**
     * Guarda un archivo
     * @param repositorio Repositorio
     * @param fileName Nombre de archivo
     * @param contentType Tipo de contenido
     * @param bytes Bytes del XML
     * @return Estado
     */
    @Override
    public Boolean guardar(Repositorio repositorio, String fileName, byte[] bytes, String contentType) {

        InputStream is = new ByteArrayInputStream(bytes);

        return AwsUtils.upload(repositorio, fileName, is, bytes.length, contentType);
    }
    
    /**
     * Obtiene un archivo
     * @param repositorio Repositorio
     * @param fileName Nombre de archivo
     * @return Estado
     * @throws java.io.IOException
     */
    @Override
    public byte[] obtener(Repositorio repositorio, String fileName) throws IOException {
        return AwsUtils.getFile(repositorio, fileName);
    }
    
    @Override
    public List<Repositorio> find(RepositorioParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(Repositorio.class);
        Root<Repositorio> root = cq.from(Repositorio.class);
        Join<Repositorio, TipoRepositorio> tipoRepositorio = root.join("tipoRepositorio");
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getIdTipoRepositorio() != null) {
            predList.add(qb.equal(root.get("idTipoRepositorio"), param.getIdTipoRepositorio()));
        }
        
        if(param.getCodigoTipoRepositorio() != null && !param.getCodigoTipoRepositorio().trim().isEmpty()) {
            predList.add(qb.equal(tipoRepositorio.get("codigo"), param.getCodigoTipoRepositorio().trim()));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());
        
        List<Repositorio> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(RepositorioParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Repositorio> root = cq.from(Repositorio.class);
        Join<Repositorio, TipoRepositorio> tipoRepositorio = root.join("tipoRepositorio");
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getIdTipoRepositorio() != null) {
            predList.add(qb.equal(root.get("idTipoRepositorio"), param.getIdTipoRepositorio()));
        }
        
        if(param.getCodigoTipoRepositorio() != null && !param.getCodigoTipoRepositorio().trim().isEmpty()) {
            predList.add(qb.equal(tipoRepositorio.get("codigo"), param.getCodigoTipoRepositorio().trim()));
        }
        
        if(param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
