/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.Date;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.InventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface InventarioDetalleFacade extends Facade<InventarioDetalle, InventarioDetalleParam, UserInfoImpl> {

    public InventarioDetalle find(Integer idInventario, Date fechaVencimiento);

    public InventarioDetallePojo findPojo(Integer idInventario, Date fechaVencimiento);

    public void aumentarInventarioDetalle(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, Date fechaVencimiento, Integer cantidadAjuste, UserInfoImpl userInfo);

    public void disminuirInventarioDetalle(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, Date fechaVencimiento, Integer cantidadAjuste, UserInfoImpl userInfo);

    public InventarioDetalle find(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, String codigoEstado, Date fechaVencimiento);

    public InventarioDetalle registrarInventarioDetalle(Integer idDepositoLogistico, Integer idProducto, Integer idEstado, Date fechaVencimiento, Integer nuevaCantidadInventarioDetalle, UserInfoImpl userInfo);

    public Boolean validToCreate(InventarioDetalleParam param, boolean validarInventario);
    
}
