/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.auditoria.TipoAuditoria;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.TipoAuditoriaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface TipoAuditoriaFacade extends Facade<TipoAuditoria, TipoAuditoriaParam, UserInfoImpl> {

    public TipoAuditoria find(Integer id, String codigo);
    
}
