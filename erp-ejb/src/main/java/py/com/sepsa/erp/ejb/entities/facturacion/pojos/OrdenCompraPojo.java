/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class OrdenCompraPojo {

    public OrdenCompraPojo(Integer id, Integer idEmpresa, String empresa,
            Integer idEstado, String estado, String codigoEstado,
            Integer idClienteOrigen, String clienteOrigen,
            Integer idClienteDestino, String clienteDestino,
            Integer idLocalOrigen, String localOrigen, Integer idLocalDestino,
            String localDestino, Date fechaInsercion, Date fechaRecepcion,
            String nroOrdenCompra, String observacion, Character recibido,
            Character anulado, Character registroInventario,
            Character archivoEdi, Character generadoEdi,
            Integer idProcesamientoArchivo, BigDecimal montoIva5,
            BigDecimal montoImponible5, BigDecimal montoTotal5,
            BigDecimal montoIva10, BigDecimal montoImponible10,
            BigDecimal montoTotal10, BigDecimal montoTotalExento,
            BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal,
            BigDecimal montoTotalOrdenCompra) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.idClienteOrigen = idClienteOrigen;
        this.clienteOrigen = clienteOrigen;
        this.idClienteDestino = idClienteDestino;
        this.clienteDestino = clienteDestino;
        this.idLocalOrigen = idLocalOrigen;
        this.localOrigen = localOrigen;
        this.idLocalDestino = idLocalDestino;
        this.localDestino = localDestino;
        this.fechaInsercion = fechaInsercion;
        this.fechaRecepcion = fechaRecepcion;
        this.nroOrdenCompra = nroOrdenCompra;
        this.observacion = observacion;
        this.recibido = recibido;
        this.anulado = anulado;
        this.registroInventario = registroInventario;
        this.archivoEdi = archivoEdi;
        this.generadoEdi = generadoEdi;
        this.idProcesamientoArchivo = idProcesamientoArchivo;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalOrdenCompra = montoTotalOrdenCompra;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Identificador de cliente origen
     */
    private Integer idClienteOrigen;
    
    /**
     * Cliente origen
     */
    private String clienteOrigen;
    
    /**
     * Identificador de cliente destino
     */
    private Integer idClienteDestino;
    
    /**
     * Cliente destino
     */
    private String clienteDestino;
    
    /**
     * Identificador de local origen
     */
    private Integer idLocalOrigen;
    
    /**
     * Local origen
     */
    private String localOrigen;
    
    /**
     * Identificador de local destino
     */
    private Integer idLocalDestino;
    
    /**
     * Local destino
     */
    private String localDestino;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;
    
    /**
     * Fecha de recepción
     */
    private Date fechaRecepcion;
    
    /**
     * Nro de orden de compra
     */
    private String nroOrdenCompra;
    
    /**
     * Observación
     */
    private String observacion;
    
    /**
     * Recibido
     */
    private Character recibido;
    
    /**
     * Registro inventario
     */
    private Character registroInventario;
    
    /**
     * Anulado
     */
    private Character anulado;
    
    /**
     * Archivo EDI
     */
    private Character archivoEdi;
    
    /**
     * Generado EDI
     */
    private Character generadoEdi;
    
    /**
     * Identificador de procesamiento de archivo
     */
    private Integer idProcesamientoArchivo;
    
    /**
     * Monto IVA 5
     */
    private BigDecimal montoIva5;
    
    /**
     * Monto imponible 5
     */
    private BigDecimal montoImponible5;
    
    /**
     * Monto total 5
     */
    private BigDecimal montoTotal5;
    
    /**
     * Monto IVA 10
     */
    private BigDecimal montoIva10;
    
    /**
     * Monto imponible 10
     */
    private BigDecimal montoImponible10;
    
    /**
     * Monto total 10
     */
    private BigDecimal montoTotal10;
    
    /**
     * Monto total exento
     */
    private BigDecimal montoTotalExento;
    
    /**
     * Monto IVA total
     */
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto total orden de compra
     */
    private BigDecimal montoTotalOrdenCompra;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public String getNroOrdenCompra() {
        return nroOrdenCompra;
    }

    public void setNroOrdenCompra(String nroOrdenCompra) {
        this.nroOrdenCompra = nroOrdenCompra;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public String getLocalOrigen() {
        return localOrigen;
    }

    public void setLocalOrigen(String localOrigen) {
        this.localOrigen = localOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public String getLocalDestino() {
        return localDestino;
    }

    public void setLocalDestino(String localDestino) {
        this.localDestino = localDestino;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public Character getRecibido() {
        return recibido;
    }

    public void setRecibido(Character recibido) {
        this.recibido = recibido;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalOrdenCompra() {
        return montoTotalOrdenCompra;
    }

    public void setMontoTotalOrdenCompra(BigDecimal montoTotalOrdenCompra) {
        this.montoTotalOrdenCompra = montoTotalOrdenCompra;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public Integer getIdClienteOrigen() {
        return idClienteOrigen;
    }

    public void setIdClienteOrigen(Integer idClienteOrigen) {
        this.idClienteOrigen = idClienteOrigen;
    }

    public String getClienteOrigen() {
        return clienteOrigen;
    }

    public void setClienteOrigen(String clienteOrigen) {
        this.clienteOrigen = clienteOrigen;
    }

    public Integer getIdClienteDestino() {
        return idClienteDestino;
    }

    public void setIdClienteDestino(Integer idClienteDestino) {
        this.idClienteDestino = idClienteDestino;
    }

    public String getClienteDestino() {
        return clienteDestino;
    }

    public void setClienteDestino(String clienteDestino) {
        this.clienteDestino = clienteDestino;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public void setRegistroInventario(Character registroInventario) {
        this.registroInventario = registroInventario;
    }

    public Character getRegistroInventario() {
        return registroInventario;
    }
}
