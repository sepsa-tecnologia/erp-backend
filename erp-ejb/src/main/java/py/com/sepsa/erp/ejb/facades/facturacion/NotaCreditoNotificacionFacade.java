/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoNotificacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoNotificacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Williams Vera
 */
@Local
public interface NotaCreditoNotificacionFacade extends Facade<NotaCreditoNotificacion, NotaCreditoNotificacionParam, UserInfoImpl> {

    public Boolean validToCreate(NotaCreditoNotificacionParam param, boolean validarIdNotaCredito);
    
}