/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

/**
 * POJO para la entidad entidad financiera
 * @author Jonathan
 */
public class EntidadFinancieraPojo {

    public EntidadFinancieraPojo(Integer id, String razonSocial) {
        this.id = id;
        this.razonSocial = razonSocial;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Razón social
     */
    private String razonSocial;

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
