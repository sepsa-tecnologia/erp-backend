/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaCuotaPojo {

    public FacturaCuotaPojo(Integer id, Integer idFactura,
            Integer idMoneda, String moneda, String codigoMoneda,
            BigDecimal monto, Date fechaVencimiento) {
        this.id = id;
        this.idFactura = idFactura;
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.codigoMoneda = codigoMoneda;
        this.monto = monto;
        this.fechaVencimiento = fechaVencimiento;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Moneda
     */
    private String moneda;
    
    /**
     * Código de moneda
     */
    private String codigoMoneda;
    
    /**
     * Monto
     */
    private BigDecimal monto;
    
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }
}
