/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.TipoTelefono;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoTelefonoFacade", mappedName = "TipoTelefonoFacade")
@Local(TipoTelefonoFacade.class)
public class TipoTelefonoFacadeImpl extends FacadeImpl<TipoTelefono, CommonParam> implements TipoTelefonoFacade {

    public TipoTelefonoFacadeImpl() {
        super(TipoTelefono.class);
    }

    @Override
    public TipoTelefono find(Integer id, String codigo) {
        CommonParam param = new CommonParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<TipoTelefono> find(CommonParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(TipoTelefono.class);
        Root<TipoTelefono> root = cq.from(TipoTelefono.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<TipoTelefono> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(CommonParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<TipoTelefono> root = cq.from(TipoTelefono.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }
        
        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
