/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.OperacionRedPago;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "OperacionRedPagoFacade", mappedName = "OperacionRedPagoFacade")
@Local(OperacionRedPagoFacade.class)
public class OperacionRedPagoFacadeImpl extends FacadeImpl<OperacionRedPago, CommonParam> implements OperacionRedPagoFacade {

    public OperacionRedPagoFacadeImpl() {
        super(OperacionRedPago.class);
    }
    
}
