/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.HistoricoLiquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Liquidacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.LiquidacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.LiquidacionPojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface LiquidacionFacade extends Facade<Liquidacion, LiquidacionParam, UserInfoImpl> {

    public Liquidacion findOrCreate(LiquidacionParam param, UserInfoImpl userInfo);

    public List<LiquidacionPojo> findPorClienteProducto(LiquidacionParam param);

    public Long findPorClienteProductoSize(LiquidacionParam param);
    
}