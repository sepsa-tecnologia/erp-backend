/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de tipo etiqueta
 * @author Jonathan
 */
public class MotivoAnulacionParam extends CommonParam {

    public MotivoAnulacionParam() {
    }

    public MotivoAnulacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo motivo anulacion
     */
    @QueryParam("idTipoMotivoAnulacion")
    private Integer idTipoMotivoAnulacion;
    
    /**
     * Código de tipo motivo anulacion
     */
    @QueryParam("codigoTipoMotivoAnulacion")
    private String codigoTipoMotivoAnulacion;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idTipoMotivoAnulacion) && isNullOrEmpty(codigoTipoMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de motivo de anulación"));
        }
        
        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idTipoMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de motivo de anulación"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoMotivoAnulacion() {
        return idTipoMotivoAnulacion;
    }

    public void setIdTipoMotivoAnulacion(Integer idTipoMotivoAnulacion) {
        this.idTipoMotivoAnulacion = idTipoMotivoAnulacion;
    }

    public String getCodigoTipoMotivoAnulacion() {
        return codigoTipoMotivoAnulacion;
    }

    public void setCodigoTipoMotivoAnulacion(String codigoTipoMotivoAnulacion) {
        this.codigoTipoMotivoAnulacion = codigoTipoMotivoAnulacion;
    }
}
