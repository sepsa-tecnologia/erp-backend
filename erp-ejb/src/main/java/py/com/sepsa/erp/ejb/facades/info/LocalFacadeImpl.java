/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import static py.com.sepsa.utils.misc.Lists.join;
import py.com.sepsa.erp.ejb.entities.info.Direccion;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.Telefono;
import py.com.sepsa.erp.ejb.entities.info.filters.LocalParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.LocalPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LocalFacade", mappedName = "LocalFacade")
@javax.ejb.Local(LocalFacade.class)
public class LocalFacadeImpl extends FacadeImpl<Local, LocalParam> implements LocalFacade {

    public LocalFacadeImpl() {
        super(Local.class);
    }

    @Override
    public List<LocalPojo> findPojo(LocalParam param) {

        String where = "true";
        
        if (param.getId() != null) {
            where = String.format(" %s and l.id = %d", where, param.getId());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getIdPersonas() != null && !param.getIdPersonas().isEmpty()) {
            where = String.format(" %s and l.id_persona in (%s)", where, join(param.getIdPersonas()));
        }

        if (param.getIdDireccion() != null) {
            where = String.format(" %s and l.id_direccion = %d", where, param.getIdDireccion());
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format(" %s and l.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }

        if (param.getActivo() != null) {
            where = String.format(" %s and l.activo = '%s'", where, param.getActivo());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format(" %s and l.observacion ilike '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getGln() != null) {
            where = String.format(" %s and l.gln = '%s'", where, param.getGln());
        }

        if (param.getLocalExterno() != null) {
            where = String.format(" %s and l.local_externo = '%s'", where, param.getLocalExterno());
        }

        if (param.getIdExterno() != null && !param.getIdExterno().trim().isEmpty()) {
            where = String.format(" %s and l.id_externo = '%s'", where, param.getIdExterno().trim());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento ilike '%%%s%%'", where, param.getRuc().trim());
        }
        
        String sql = String.format("select l.id, l.id_empresa, e.descripcion as empresa, "
                + "l.id_persona, l.id_direccion, l.gln, l.id_externo, l.activo, "
                + "l.local_externo, l.descripcion, l.observacion, l.id_telefono "
                + "from info.local l "
                + "join info.empresa e on e.id = l.id_empresa "
                + "left join comercial.cliente c on c.id_cliente = l.id_persona "
                + "left join info.persona com on com.id = c.id_comercial "
                + "where %s "
                + "order by l.activo, l.descripcion "
                + "offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<LocalPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            LocalPojo item = new LocalPojo();
            item.setId((Integer)objects[i++]);
            item.setIdEmpresa((Integer)objects[i++]);
            item.setEmpresa((String)objects[i++]);
            item.setIdPersona((Integer)objects[i++]);
            item.setIdDireccion((Integer)objects[i++]);
            item.setGln(objects[i] == null ? null : new BigInteger(objects[i] + ""));
            i++;
            item.setIdExterno((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setLocalExterno((Character)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setObservacion((String)objects[i++]);
            item.setIdTelefono((Integer) objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    /**
     * Obtiene la lista 
     * @param param parámetros
     * @return Lista
     */
    @Override
    public List<Local> find(LocalParam param) {

        String where = "true";
        
        if (param.getId() != null) {
            where = String.format(" %s and l.id = %d", where, param.getId());
        }
        
        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }

        if (param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getIdPersonas() != null && !param.getIdPersonas().isEmpty()) {
            where = String.format(" %s and l.id_persona in (%s)", where, join(param.getIdPersonas()));
        }

        if (param.getIdDireccion() != null) {
            where = String.format(" %s and l.id_direccion = %d", where, param.getIdDireccion());
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format(" %s and l.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }

        if (param.getActivo() != null) {
            where = String.format(" %s and l.activo = '%s'", where, param.getActivo());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format(" %s and l.observacion ilike '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getGln() != null) {
            where = String.format(" %s and l.gln = '%s'", where, param.getGln());
        }

        if (param.getLocalExterno() != null) {
            where = String.format(" %s and l.local_externo = '%s'", where, param.getLocalExterno());
        }

        if (param.getIdExterno() != null && !param.getIdExterno().trim().isEmpty()) {
            where = String.format(" %s and l.id_externo = '%s'", where, param.getIdExterno().trim());
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento ilike '%%%s%%'", where, param.getRuc().trim());
        }
        
        String sql = String.format("select l.*"
                + "from info.local l "
                + "left join comercial.cliente c on c.id_cliente = l.id_persona "
                + "left join info.persona com on com.id = c.id_comercial "
                + "where %s "
                + "order by l.activo, l.descripcion "
                + "offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, Local.class);
        
        List<Local> result = q.getResultList();

        return result;
    }

    /**
     * Obtiene el tamaño de la lista 
     * @param param parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(LocalParam param) {

        String where = "true";
        
        if (param.getId() != null) {
            where = String.format(" %s and l.id = %d", where, param.getId());
        }

        if (param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }

        if (param.getIdPersonas() != null && !param.getIdPersonas().isEmpty()) {
            where = String.format(" %s and l.id_persona in (%s)", where, join(param.getIdPersonas()));
        }

        if (param.getIdDireccion() != null) {
            where = String.format(" %s and l.id_direccion = %d", where, param.getIdDireccion());
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            where = String.format(" %s and l.descripcion ilike '%%%s%%'", where, param.getDescripcion().trim());
        }

        if (param.getActivo() != null) {
            where = String.format(" %s and l.activo = '%s'", where, param.getActivo());
        }

        if (param.getObservacion() != null && !param.getObservacion().trim().isEmpty()) {
            where = String.format(" %s and l.observacion ilike '%%%s%%'", where, param.getObservacion().trim());
        }

        if (param.getGln() != null) {
            where = String.format(" %s and l.gln = '%s'", where, param.getGln());
        }
        
        if (param.getLocalExterno() != null) {
            where = String.format(" %s and l.local_externo = '%s'", where, param.getLocalExterno());
        }
        
        if (param.getIdExterno() != null && !param.getIdExterno().trim().isEmpty()) {
            where = String.format(" %s and l.id_externo = '%s'", where, param.getIdExterno().trim());
        }
        
        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            where = String.format(" %s and c.nro_documento ilike '%%%s%%'", where, param.getRuc().trim());
        }
        
        String sql = String.format("select count(distinct (l.*)) "
                + "from info.local l "
                + "left join comercial.cliente c on c.id_cliente = l.id_persona "
                + "left join info.persona com on com.id = c.id_comercial "
                + "where %s", where);
        
        javax.persistence.Query q = getEntityManager()
                .createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();

        return result;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToCreate(LocalParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
            
        if(!Assertions.isNull(param.getIdDireccion())) {
            Direccion direccion = facades.getDireccionFacade().find(param.getIdDireccion());

            if(direccion == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la dirección"));
            }
        }
        
        if(!Assertions.isNull(param.getIdTelefono())) {
            Telefono telefono = facades.getTelefonoFacade().find(param.getIdTelefono());
            
            if (telefono == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el telefono"));
            }
        }
        
        
        if(!Assertions.isNull(param.getDireccion())) {
            facades.getDireccionFacade().validToCreate(param.getDireccion());
        }
        
        if(!Assertions.isNull(param.getTelefono())) {
            facades.getTelefonoFacade().validToCreate(param.getTelefono());
        }
        
        LocalParam param1 = new LocalParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setGln(param1.getGln());
        Long size = findSize(param1);
        
        if(size > 0) {
            param1.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un GLN registrado"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de local
     * @param param Parámetros
     * @param userInfo Identificador de usuario
     * @return 
     */
    @Override
    public Local create(LocalParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        if(!Assertions.isNull(param.getDireccion())) {
            Direccion direccion = facades.getDireccionFacade().create(param.getDireccion(), userInfo);
            param.setIdDireccion(direccion == null ? null : direccion.getId());
        }
        
        Local local = new Local();
        local.setIdEmpresa(userInfo.getIdEmpresa());
        local.setIdPersona(param.getIdPersona());
        local.setIdExterno(param.getIdExterno());
        local.setDescripcion(param.getDescripcion().trim());
        local.setObservacion(param.getObservacion());
        local.setActivo(param.getActivo());
        local.setGln(param.getGln());
        local.setLocalExterno(param.getLocalExterno());
        local.setIdDireccion(param.getIdDireccion());
        local.setIdTelefono(param.getIdTelefono());
        
        create(local);
        
        Map<String, String> atributos = new HashMap();
        atributos.put("id", local.getId() + " ");
        atributos.put("id_empresa", param.getIdEmpresa() + " ");
        atributos.put("id_persona", param.getIdPersona() + " ");
        atributos.put("id_direccion", param.getIdDireccion() + " ");
        atributos.put("descripcion", param.getDescripcion() + " ");
        atributos.put("activo", param.getActivo() + " ");
        atributos.put("observacion", param.getObservacion() + " ");
        atributos.put("local_externo", param.getLocalExterno() + " ");
        atributos.put("gln", param.getGln() + " ");

        facades.getRegistroFacade().create("info", "local", atributos, userInfo);
        
        return local;
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToEdit(LocalParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Local local = facades.getLocalFacade().findFirst(param);
        
        if(local == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el local"));
        }
        
        Persona persona = facades.getPersonaFacade().find(param.getIdPersona());
        
        if(persona == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la persona"));
        }
            
        Direccion direccion = param.getIdDireccion() == null
                ? null
                : facades.getDireccionFacade().find(param.getIdDireccion());

        if(param.getIdDireccion() != null && direccion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la dirección"));
        }
        
        Telefono telefono = param.getIdTelefono() == null
                ? null
                : facades.getTelefonoFacade().find(param.getIdTelefono());
        
        if (param.getIdDireccion() != null && direccion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el telefono"));
        }
        
        
        return !param.tieneErrores();
    }
    
    /**
     * Edita una instancia de local
     * @param param Parámetros
     * @param userInfo Identificador de usuario
     * @return 
     */
    @Override
    public Local edit(LocalParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        Local local = find(param.getId());
 
        if (param.getTelefono() != null){
            Telefono telefonoEdit = new Telefono();

            telefonoEdit.setIdTipoTelefono(param.getTelefono().getIdTipoTelefono());
            telefonoEdit.setNumero(param.getTelefono().getNumero());
            telefonoEdit.setPrefijo(param.getTelefono().getPrefijo());
            telefonoEdit.setPrincipal('S');

            if (param.getIdTelefono() != null) {
                telefonoEdit.setId(param.getIdTelefono());
                facades.getTelefonoFacade().edit(telefonoEdit);

            } else {
                facades.getTelefonoFacade().create(telefonoEdit);  
                param.setIdTelefono(telefonoEdit.getId());
            }
            local.setIdTelefono(param.getIdTelefono());
            
        }
        
        
        local.setDescripcion(param.getDescripcion());
        local.setObservacion(param.getObservacion());
        local.setActivo(param.getActivo());
        local.setGln(param.getGln());
        local.setLocalExterno(param.getLocalExterno());
        local.setIdDireccion(param.getIdDireccion());
        
        
        edit(local);
        
        Map<String, String> pk = new HashMap();
        pk.put("id", param.getId() + " ");
        Map<String, String> atributos = new HashMap();
        atributos.put("id_empresa", param.getIdEmpresa() + " ");
        atributos.put("id_persona", param.getIdPersona() + " ");
        atributos.put("id_direccion", param.getIdDireccion() + " ");
        atributos.put("descripcion", param.getDescripcion() + " ");
        atributos.put("activo", param.getActivo() + " ");
        atributos.put("observacion", param.getObservacion() + " ");
        atributos.put("local_externo", param.getLocalExterno() + " ");
        atributos.put("gln", param.getGln() + " ");

        facades.getRegistroFacade().edit("info", "local", atributos, userInfo, pk);
        
        return local;
    }
}
