/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.TalonarioLocal;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioLocalParam;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "TalonarioLocalFacade", mappedName = "TalonarioLocalFacade")
@javax.ejb.Local(TalonarioLocalFacade.class)
public class TalonarioLocalFacadeImpl extends FacadeImpl<TalonarioLocal, TalonarioLocalParam> implements TalonarioLocalFacade {

    public TalonarioLocalFacadeImpl() {
        super(TalonarioLocal.class);
    }

    @Override
    public Boolean validToCreate(TalonarioLocalParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TalonarioLocalParam param1 = new TalonarioLocalParam();
        param1.setIdTalonario(param.getIdTalonario());
        param1.setIdLocal(param.getIdLocal());
        TalonarioLocal item = findFirst(param1);
        
        if(item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe el registro"));
        }
        
        Local local = facades.getLocalFacade().find(param.getIdLocal());
        
        if(local == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el local"));
        }
        
        Talonario talonario = facades.getTalonarioFacade().find(param.getIdTalonario());
        
        if(talonario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Boolean validToEdit(TalonarioLocalParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
   
        TalonarioLocalParam param1 = new TalonarioLocalParam();
        param1.setIdTalonario(param.getIdTalonario());
        param1.setIdLocal(param.getIdLocal());
        TalonarioLocal item = findFirst(param1);

        if(item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro"));
        } else {
            param.setId(item.getId());
        }
        
        Local local = facades.getLocalFacade().find(param.getIdLocal());
        
        if(local == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el local"));
        }
        
        Talonario talonario = facades.getTalonarioFacade().find(param.getIdTalonario());
        
        if(talonario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public TalonarioLocal create(TalonarioLocalParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        TalonarioLocal item = new TalonarioLocal();
        item.setIdTalonario(param.getIdTalonario());
        item.setIdLocal(param.getIdLocal());
        item.setActivo(param.getActivo());
        
        create(item);
        
        return item;
    }
    
    @Override
    public TalonarioLocal edit(TalonarioLocalParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        TalonarioLocal item = find(param.getId());
        item.setIdTalonario(param.getIdTalonario());
        item.setIdLocal(param.getIdLocal());
        item.setActivo(param.getActivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public void asociarMasivo(TalonarioLocalParam param, UserInfoImpl userInfo) {
        
        param.setFirstResult(0);
        param.setPageSize(500);
        List<TalonarioLocal> lista = findRelacionados(param);

        for (TalonarioLocal item : lista) {

            TalonarioLocalParam param1 = new TalonarioLocalParam();
            param1.setIdTalonario(item.getIdTalonario());
            param1.setIdLocal(item.getIdLocal());
            param1.setActivo(param.getActivo());

            editarOCrear(param1, userInfo);
        }
        
    }
    
    @Override
    public List<TalonarioLocal> find(TalonarioLocalParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and tl.id = %d", where, param.getId());
        }
        
        if(param.getIdTalonario()!= null) {
            where = String.format("%s and tl.id_talonario = %d", where, param.getIdTalonario());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format("%s and tl.id_local = %d", where, param.getIdLocal());
        }
        
        if(!Assertions.isNullOrEmpty(param.getIdExternoLocal())) {
            where = String.format("%s and l.id_externo = '%s'", where, param.getIdExternoLocal());
        }
        
        if(!Assertions.isNull(param.getGlnLocal())) {
            where = String.format("%s and l.gln = '%s'", where, param.getGlnLocal());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and tl.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select tl.*"
                + " from facturacion.talonario_local tl"
                + " left join info.local l on l.id = tl.id_local"
                + " where %s order by tl.activo, tl.id offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, TalonarioLocal.class);
        
        List<TalonarioLocal> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TalonarioLocalParam param) {

        String where = "true";
        
        if(param.getId() != null) {
            where = String.format("%s and tl.id = %d", where, param.getId());
        }
        
        if(param.getIdTalonario() != null) {
            where = String.format("%s and tl.id_talonario = %d", where, param.getIdTalonario());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format("%s and tl.id_local = %d", where, param.getIdLocal());
        }
        
        if(!Assertions.isNullOrEmpty(param.getIdExternoLocal())) {
            where = String.format("%s and l.id_externo = '%s'", where, param.getIdExternoLocal());
        }
        
        if(!Assertions.isNull(param.getGlnLocal())) {
            where = String.format("%s and l.gln = '%s'", where, param.getGlnLocal());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and tl.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(tl.id)"
                + " from facturacion.talonario_local tl"
                + " left join info.local l on l.id = tl.id_local"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }

    @Override
    public List<TalonarioLocal> findRelacionados(TalonarioLocalParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format(" %s and l.id = %d", where, param.getIdLocal());
        }
        
        if(param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getLocalExterno() != null) {
            where = String.format(" %s and l.externo = '%s'", where, param.getLocalExterno());
        }
        
        String sql = String.format("select coalesce(tl.id, 0) as id, coalesce(tl.id_talonario, %d) as id_talonario, l.id as id_local, coalesce(tl.activo, 'N') as activo\n"
                + "from info.local l\n"
                + "left join facturacion.talonario_local tl on tl.id_local = l.id and tl.id_talonario = %d\n"
                + "where %s limit %d offset %d", param.getIdTalonario(), param.getIdTalonario(), where, param.getPageSize(), param.getFirstResult());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<TalonarioLocal> result = Lists.empty();
        
        for (Object[] objects : list) {
            int i = 0;
            TalonarioLocal ul = new TalonarioLocal();
            ul.setId((Integer)objects[i++]);
            ul.setIdTalonario((Integer)objects[i++]);
            ul.setIdLocal((Integer)objects[i++]);
            ul.setActivo((Character)objects[i++]);
            ul.setLocal(facades.getLocalFacade().find(ul.getIdLocal()));
            ul.setTalonario(facades.getTalonarioFacade().find(ul.getIdTalonario()));
            result.add(ul);
        }
        
        return result;
    }

    @Override
    public Long findRelacionadosSize(TalonarioLocalParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format(" %s and l.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getIdLocal() != null) {
            where = String.format(" %s and l.id = %d", where, param.getIdLocal());
        }
        
        if(param.getIdPersona() != null) {
            where = String.format(" %s and l.id_persona = %d", where, param.getIdPersona());
        }
        
        if(param.getLocalExterno() != null) {
            where = String.format(" %s and l.externo = '%s'", where, param.getLocalExterno());
        }
        
        String sql = String.format("select count(l)\n"
                + "from info.local l\n"
                + "left join facturacion.talonario_local tl on tl.id_local = l.id and tl.id_talonario = %d\n"
                + "where %s", param.getIdTalonario(), where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
