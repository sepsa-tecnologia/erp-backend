/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.dtecore.v150.handler.Document;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.AutoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoAnulacion;
import py.com.sepsa.erp.ejb.entities.facturacion.Talonario;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoFactura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDescuentoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.AutoFacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TipoCambioPojo;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefono;
import py.com.sepsa.erp.ejb.entities.info.filters.ReferenciaGeograficaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.set.DetalleProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.externalservice.adapters.KudeDteAdapter;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.reporte.pojos.factura.FacturaAutoimpresorParam;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.misc.Strings;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.client.ConnectionPojo;
import py.com.sepsa.utils.rest.client.body.BodyResponse;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import py.com.sepsa.utils.siediApi.pojos.ResultadoOperacionSiediApi;
import py.com.sepsa.utils.siediApi.remote.LoginServiceSiediApi;
import py.com.sepsa.utils.siediApi.remote.SiediApiServiceClient;
import py.com.sepsa.utils.siedimonitor.pojos.DetalleResultado;
import py.com.sepsa.utils.siedimonitor.pojos.ResultadoOperacion;
import py.com.sepsa.utils.siedimonitor.remote.EventoService;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "AutoFacturaFacade", mappedName = "AutoFacturaFacade")
@Local(AutoFacturaFacade.class)
public class AutoFacturaFacadeImpl extends FacadeImpl<AutoFactura, AutoFacturaParam> implements AutoFacturaFacade {

    public AutoFacturaFacadeImpl() {
        super(AutoFactura.class);
    }

    /**
     * Obtiene los datos como para crear
     *
     * @param talonario talonario
     * @param idCliente Identificador de cliente
     * @return Datos
     */
    @Override
    public AutoFacturaPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente) {

        Date fecha = facades.getTalonarioFacade().ultimaFechaDoc(talonario, 5);
        String nroFactura = facades.getTalonarioFacade().sigNumAutoFactura(talonario);

        String razonSocial = null;
        String ruc = null;
        String direccion = null;
        Integer nroCasa = null;
        Integer idDepartamento = null;
        Integer idDistrito = null;
        Integer idCiudad = null;
        String email = null;
        String telefono = null;

        Cliente cliente = idCliente == null
                ? null
                : facades.getClienteFacade().find(idCliente);

        if (cliente != null) {
            email = Units.execute(() -> facades.getPersonaEmailFacade().obtenerEmailCliente(idCliente).getEmail().getEmail());
            telefono = Units.execute(() -> {
                PersonaTelefono item = facades.getPersonaTelefonoFacade().obtenerTelefonoCliente(idCliente);
                return String.format("%s%s", item.getTelefono().getPrefijo(), item.getTelefono().getNumero());
            });
            razonSocial = cliente.getRazonSocial();
            ruc = cliente.getNroDocumento();
            direccion = Units.execute(() -> cliente.getPersona().getDireccion().getDireccion());
            nroCasa = Units.execute(() -> cliente.getPersona().getDireccion().getNumero());
            idDepartamento = Units.execute(() -> cliente.getPersona().getDireccion().getDepartamento().getId());
            idDistrito = Units.execute(() -> cliente.getPersona().getDireccion().getDistrito().getId());
            idCiudad = Units.execute(() -> cliente.getPersona().getDireccion().getCiudad().getId());
        }

        StringTokenizer st = new StringTokenizer(nroFactura, "-");

        String establecimiento = st.nextToken();
        String puntoExpedicion = st.nextToken();
        String nroDocumento = "0000000" + st.nextToken();
        nroDocumento = nroDocumento.substring(nroDocumento.length() - 7, nroDocumento.length());

        AutoFacturaPojo item = new AutoFacturaPojo();
        item.setIdTalonario(talonario.getId());
        item.setFechaVencimientoTimbrado(talonario.getFechaVencimiento());
        item.setTimbrado(talonario.getTimbrado());
        item.setFecha(fecha);
        item.setEstablecimiento(establecimiento);
        item.setPuntoExpedicion(puntoExpedicion);
        item.setNroDocumentoVendedor(nroDocumento);
        item.setNroAutofactura(nroFactura);
        item.setDireccion(direccion);
        item.setNroCasa(nroCasa);
        item.setIdDepartamento(idDepartamento);
        item.setIdDistrito(idDistrito);
        item.setIdCiudad(idCiudad);
        item.setEmail(email);
        item.setTelefono(telefono);
        item.setRuc(ruc);

        return item;
    }

    public Boolean validToEdit(AutoFacturaParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        AutoFactura autoFactura = find(param.getId());

        if (autoFactura == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la autofactura"));
        }

        if (param.getIdEstado() != null || param.getCodigoEstado() != null) {
            EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                    param.getCodigoEstado(), "AUTOFACTURA");

            if (estado == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
            } else {
                param.setIdEstado(estado.getId());
                param.setCodigoEstado(estado.getCodigo());

                if ((estado.getCodigo().equals("APROBADO")
                        || estado.getCodigo().equals("CANCELADO")
                        || estado.getCodigo().equals("INUTILIZADO"))) {
                    if (autoFactura != null && !autoFactura.getRuc().equals(param.getRuc())) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion(String.format("No se puede editar "
                                        + "el RUC de una factura con estado %s",
                                        estado.getCodigo())));
                    }
                }
            }
        }

        if (param.getIdProcesamiento() != null) {

            Procesamiento procesamiento = facades.getProcesamientoFacade()
                    .find(param.getIdProcesamiento());

            if (procesamiento == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el registro de procesamiento"));
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToGetPdf(AutoFacturaParam param) {

        if (!param.isValidToGetPdf()) {
            return Boolean.FALSE;
        }

        Long fsize = findSize(param);

        if (fsize <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la factura"));
        }

        return !param.tieneErrores();
    }

    public Boolean validToCancel(AutoFacturaParam param) {

        if (!param.isValidToCancel()) {
            return Boolean.FALSE;
        }

        MotivoAnulacion motivoAnulacion = facades.getMotivoAnulacionFacade()
                .find(param.getIdMotivoAnulacion(), param.getCodigoMotivoAnulacion(), "AUTOFACTURA");

        if (motivoAnulacion == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el motivo de anulación"));
        } else {
            param.setIdMotivoAnulacion(motivoAnulacion.getId());
            param.setCodigoMotivoAnulacion(motivoAnulacion.getCodigo());
            param.setMotivoAnulacion(motivoAnulacion.getDescripcion());
        }

        AutoFacturaParam fparam = new AutoFacturaParam();
        fparam.setId(param.getId());
        AutoFacturaPojo factura = facades.getAutoFacturaFacade().findFirstPojo(fparam);

        if (factura == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la factura"));
        } else {

            param.setNroAutofactura(factura.getNroAutofactura());

            if (Objects.equals(factura.getAnulado(), 'S')) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La factura ya se encuentra anulada"));
            }

            String diaDeclaracionMes = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "DIA_DECLARACION_MES");

            Integer diaDeclaracion = Units.execute(() -> Integer.valueOf(diaDeclaracionMes));

            if ((param.getIgnorarPeriodoAnulacion() == null
                    || !param.getIgnorarPeriodoAnulacion())
                    && !isNull(diaDeclaracion)) {

                Calendar inv = Calendar.getInstance();
                inv.setTime(factura.getFecha());
                inv.add(Calendar.DAY_OF_MONTH, diaDeclaracion);
                inv.set(Calendar.HOUR_OF_DAY, 0);
                inv.set(Calendar.MINUTE, 0);
                inv.set(Calendar.SECOND, 0);

                Calendar now = Calendar.getInstance();
                now.set(Calendar.HOUR_OF_DAY, 0);
                now.set(Calendar.MINUTE, 0);
                now.set(Calendar.SECOND, 0);

                if (inv.compareTo(now) < 0) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Sólo se pueden anular facturas que esten dentro del periodo de anulación"));
                }
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToCreate(UserInfoImpl userInfo, AutoFacturaParam param) {

        if (param.getIdTipoFactura() != null || param.getCodigoTipoFactura() != null) {
            TipoFactura tipoFactura = facades
                    .getTipoFacturaFacade()
                    .find(param.getIdTipoFactura(), param.getCodigoTipoFactura());

            if (tipoFactura != null) {
                param.setIdTipoFactura(tipoFactura.getId());
                param.setCodigoTipoFactura(tipoFactura.getCodigo());
            }
        }

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        TipoFactura tipoFactura = facades
                .getTipoFacturaFacade()
                .find(param.getIdTipoFactura(), param.getCodigoTipoFactura());

        if (tipoFactura == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de factura"));
        } else {
            param.setIdTipoFactura(tipoFactura.getId());
            param.setCodigoTipoFactura(tipoFactura.getCodigo());
        }

        NaturalezaCliente naturaleza = facades.getNaturalezaClienteFacade()
                .find(param.getIdNaturalezaVendedor(), param.getCodigoNaturalezaVendedor());

        if (naturaleza == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la naturaleza del cliente"));
        } else {
            param.setIdNaturalezaVendedor(naturaleza.getId());
            param.setCodigoNaturalezaVendedor(naturaleza.getCodigo());
        }

        TipoCambioPojo tipoCambio = null;
        if (param.getIdTipoCambio() != null) {
            tipoCambio = facades.getTipoCambioFacade().findFirst(param.getIdTipoCambio());
            if (tipoCambio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el tipo de cambio"));
            }
        }

        Moneda moneda = facades.getMonedaFacade().find(param.getIdMoneda(), param.getCodigoMoneda());

        if (moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
            param.setCodigoMoneda(moneda.getCodigo());

            if (param.getCodigoMoneda().equals("PYG")) {
                param.setIdTipoCambio(null);
            } else if (tipoCambio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el tipo de cambio"));
            } else if (!tipoCambio.getCodigoMoneda().equals(param.getCodigoMoneda())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La moneda de la factura y tipo de cambio no coinciden"));
            }
        }

        TalonarioPojo talonario;
        
        if(param.getIdTalonario() != null) {
            TalonarioParam param2 = new TalonarioParam();
            param2.setIdEmpresa(param.getIdEmpresa());
            param2.setId(param.getIdTalonario());
            talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        } else {
            TalonarioParam param2 = new TalonarioParam();
            param2.setIdEmpresa(param.getIdEmpresa());
            param2.setTimbrado(param.getTimbrado());
            param2.setDigital(param.getDigital());
            param2.setNroSucursal(param.getEstablecimiento());
            param2.setNroPuntoVenta(param.getPuntoExpedicion());
            talonario = facades.getTalonarioFacade().findFirstPojo(param2);
        }

        if (talonario == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el talonario"));
        }

        Usuario encargado = facades.getUsuarioFacade().find(param.getIdEncargado());

        if (isNull(encargado)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el encargado"));
        }

        ClienteParam paramc = new ClienteParam();
        paramc.setIdEmpresa(param.getIdEmpresa());
        paramc.setNroDocumentoEq(param.getRuc());
        Cliente cliente = facades.getClienteFacade().findFirst(paramc);

        ReferenciaGeograficaParam param3 = new ReferenciaGeograficaParam();

        param3.setId(param.getIdDepartamento());
        Long sizeDepartamento = param.getIdDepartamento() == null
                ? null
                : facades.getReferenciaGeograficaFacade().findSize(param3);

        param3.setId(param.getIdDistrito());
        Long sizeDistrito = param.getIdDistrito() == null
                ? null
                : facades.getReferenciaGeograficaFacade().findSize(param3);

        param3.setId(param.getIdCiudad());
        Long sizeCiudad = param.getIdCiudad() == null
                ? null
                : facades.getReferenciaGeograficaFacade().findSize(param3);

        if (param.getIdDepartamento() != null && sizeDepartamento <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el departamento"));
        }

        if (param.getIdDistrito() != null && sizeDistrito <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el distrito"));
        }

        if (param.getIdCiudad() != null && sizeCiudad <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la ciudad"));
        }

        String ruc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");

        if (ruc == null || ruc.trim().isEmpty()) {
            ruc = "";
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el valor de configuración de RUC"));
        }

        if (param.getDigital().equals('S')) {

            String codSeguridad = "000000000" + String.valueOf(param.getFecha().getTime());
            codSeguridad = codSeguridad.substring(codSeguridad.length() - 9, codSeguridad.length());
            param.setCodSeguridad(codSeguridad);

            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(param.getNroAutofactura(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch (j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            int i = 0;

            String dRucEm = "";
            String dDVEmi = "";

            for (StringTokenizer stringTokenizer = new StringTokenizer(ruc, "-"); stringTokenizer.hasMoreTokens(); i++) {

                String token = stringTokenizer.nextToken();

                switch (i) {
                    case 0:
                        dRucEm = token;
                        break;

                    case 1:
                        dDVEmi = token;
                        break;
                }
            }

            String cdc = Document.getCDCWithoutDv("04", dRucEm, dDVEmi, dEst, dPunt, nroDoc,
                    String.valueOf(2), param.getFecha(), "1", param.getCodSeguridad());
            cdc = Document.getCDC(cdc);
            param.setCdc(cdc);
        }

        Date date = Calendar.getInstance().getTime();
        TalonarioParam param1 = new TalonarioParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setId(param.getIdTalonario());
        param1.setIdTipoDocumento(5);
        param1.setDigital(param.getDigital());
        param1.setFecha(date);
        TalonarioPojo talonarioC = facades.getTalonarioFacade().getTalonario(param1);

        if (talonarioC == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un talonario activo para el tipo de documento factura"));
        } else {
            if (!talonarioC.getId().equals(param.getIdTalonario())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("El talonario seleccionado no corresponde con el talonario activo"));
            }

            if (!facades.getTalonarioFacade().fechaDocumentoEnRango(talonario, param.getFecha())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la factura debe estar en el rango de vigencia del timbrado"));
            }
        }

        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(), param.getCodigoEstado(), "AUTOFACTURA");

        if (estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }

        AutoFacturaPojo item = obtenerDatosCrear(talonario, null);

        if (param.getAsignarNroFactura() != null
                && param.getAsignarNroFactura()) {
            param.setNroAutofactura(item.getNroAutofactura());
        }

        if (!item.getNroAutofactura().equals(param.getNroAutofactura())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de la factura debe ser correlativo"));
        }

        if (item.getFecha().after(param.getFecha())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("La factura no puede tener fecha a una creada previamente"));
        }

        if (!facades.getTalonarioFacade().nroDocEnRango(talonario, param.getNroAutofactura())) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("El nro de factura no se encuentra en el rango del talonario"));
        }

        for (AutoFacturaDetalleParam detalle : param.getFacturaDetalles()) {
           // facades.getFacturaDetalleFacade().validToCreate(detalle, Boolean.FALSE);
        }

        if (!isNull(param.getFacturaDncp())) {
            facades.getFacturaDncpFacade().validToCreate(param.getFacturaDncp(), Boolean.FALSE);
        }

        if (!isNull(param.getFacturaNotificaciones())) {
            for (FacturaNotificacionParam detalle : param.getFacturaNotificaciones()) {
                facades.getFacturaNotificacionFacade().validToCreate(detalle, Boolean.FALSE);
            }
        }

        return !param.tieneErrores();
    }
//
//    @Override
//    public Boolean reenviarFacturasRechazadas(FacturaParam param) {
//        
//        if(!param.isValidToResendRejected()) {
//            return Boolean.FALSE;
//        }
//        
//        List<FacturaPojo> list = findRejected(param);
//        
//        for (FacturaPojo factura : list) {
//            FacturaParam item = new FacturaParam();
//            item.setId(factura.getId());
//            item.setRuc(param.getRuc());
//            item.setGeneradoSet('N');
//            item.setEstadoSincronizado('N');
//            editAlt(item);
//        }
//        
//        return Boolean.TRUE;
//    }
  /**
    private List<FacturaPojo> findRejected(FacturaParam param) {
        
        String where = "";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if(param.getCodigoResultado() != null && !param.getCodigoResultado().trim().isEmpty()) {
            where = String.format("%s and dp.codigo_resultado = '%s'", where, param.getCodigoResultado().trim());
        }
        
        if(param.getFechaInsercionDesde() != null) {
            where = String.format("%s and f.fecha_insercion >= '%s'", where, sdf.format(param.getFechaInsercionDesde()));
        }
        
        String sql = String.format("select f.id, f.digital, f.generado_set, f.estado_sincronizado\n"
                + "from facturacion.factura f\n"
                + "join info.estado e on f.id_estado=e.id\n"
                + "join set.detalle_procesamiento dp on dp.id_procesamiento=f.id_procesamiento\n"
                + "where f.estado_sincronizado='S' and f.archivo_set='S' and f.generado_set='S'\n"
                + "and e.codigo='RECHAZADO' and f.digital='S' %s\n"
                + "offset %d limit %d", where, param.getFirstResult(), param.getPageSize());
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<FacturaPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            FacturaPojo item = new FacturaPojo();
            item.setId((Integer)objects[i++]);
            item.setDigital((Character)objects[i++]);
            item.setGeneradoSet((Character)objects[i++]);
            item.setEstadoSincronizado((Character)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
 **/   
    
  /**  
    public Boolean editAlt(FacturaParam param) {
        
        if(param.getId() == null) {
            return Boolean.FALSE;
        }
        
        String set = String.format("id = %d", param.getId());
        
        if(param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            set = String.format("%s, ruc = '%s'", set, param.getRuc().trim());
        }
        
        if(param.getArchivoEdi() != null) {
            set = String.format("%s, archivo_edi = '%s'", set, param.getArchivoEdi());
        }
        
        if(param.getArchivoSet() != null) {
            set = String.format("%s, archivo_set = '%s'", set, param.getArchivoSet());
        }
        
        if(param.getGeneradoEdi() != null) {
            set = String.format("%s, generado_edi = '%s'", set, param.getGeneradoEdi());
        }
        
        if(param.getGeneradoSet() != null) {
            set = String.format("%s, generado_set = '%s'", set, param.getGeneradoSet());
        }
        
        if(param.getEstadoSincronizado() != null) {
            set = String.format("%s, estado_sincronizado = '%s'", set, param.getEstadoSincronizado());
        }
        
        if(param.getIdProcesamiento() != null) {
            set = String.format("%s, id_procesamiento = %d", set, param.getIdProcesamiento());
        }
        
        String sql = String.format("UPDATE facturacion.factura SET %s WHERE id = %d", set, param.getId());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        q.executeUpdate();
        
        return Boolean.TRUE;
    }
  **/  
    
    /**
     * 
     * @param param
     * @param userInfo
     * @return 
     */
    
    @Override
    public AutoFactura edit(AutoFacturaParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        AutoFactura factura = find(param.getId());

        factura.setRuc(param.getRuc() != null ? param.getRuc() : factura.getRuc());
        factura.setObservacion(param.getObservacion());
        factura.setImpreso(param.getImpreso());
        factura.setEntregado(param.getEntregado());
        factura.setFechaEntrega(param.getFechaEntrega());
        factura.setArchivoEdi(param.getArchivoEdi());
        factura.setArchivoSet(param.getArchivoSet());
        factura.setGeneradoEdi(param.getGeneradoEdi());
        factura.setEstadoSincronizado(factura.getDigital().equals('S')
                && param.getArchivoSet().equals('S')
                && !factura.getGeneradoSet().equals(param.getGeneradoSet())
                ? 'N'
                : param.getEstadoSincronizado() != null
                ? param.getEstadoSincronizado()
                : factura.getEstadoSincronizado());
        factura.setGeneradoSet(param.getGeneradoSet());
        factura.setIdEstado(param.getIdEstado() != null ? param.getIdEstado() : factura.getIdEstado());
        factura.setIdProcesamiento(param.getIdProcesamiento() != null ? param.getIdProcesamiento() : factura.getIdProcesamiento());

        edit(factura);

        return factura;
    }
    
    @Override
    public AutoFacturaPojo editPojo(AutoFacturaParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        AutoFactura factura = find(param.getId());

        factura.setRuc(param.getRuc() != null ? param.getRuc() : factura.getRuc());
        factura.setObservacion(param.getObservacion());
        factura.setImpreso(param.getImpreso());
        factura.setEntregado(param.getEntregado());
        factura.setFechaEntrega(param.getFechaEntrega());
        factura.setArchivoEdi(param.getArchivoEdi());
        factura.setArchivoSet(param.getArchivoSet());
        factura.setGeneradoEdi(param.getGeneradoEdi());
        factura.setEstadoSincronizado(factura.getDigital().equals('S')
                && param.getArchivoSet().equals('S')
                && !factura.getGeneradoSet().equals(param.getGeneradoSet())
                ? 'N'
                : param.getEstadoSincronizado() != null
                ? param.getEstadoSincronizado()
                : factura.getEstadoSincronizado());
        factura.setGeneradoSet(param.getGeneradoSet());
        factura.setIdEstado(param.getIdEstado() != null ? param.getIdEstado() : factura.getIdEstado());
        factura.setIdProcesamiento(param.getIdProcesamiento() != null ? param.getIdProcesamiento() : factura.getIdProcesamiento());

        edit(factura);
                 
        
    //  AutoFacturaPojo autofactura = findPojo(param);
        
        AutoFacturaPojo autofactura = new AutoFacturaPojo();
        autofactura.setId(factura.getId());
        autofactura.setIdEmpresa(factura.getIdEmpresa());
        autofactura.setEmpresa(factura.getEmpresa().getDescripcion());
        autofactura.setIdTalonario(factura.getTalonario().getId());
        autofactura.setFechaVencimientoTimbrado(factura.getTalonario().getFechaVencimiento());
        autofactura.setFechaInicioVigenciaTimbrado(factura.getTalonario().getFechaInicio());
        autofactura.setTimbrado(factura.getTalonario().getTimbrado());
        autofactura.setIdMoneda(factura.getIdMoneda());
        autofactura.setMoneda(factura.getMoneda().getDescripcion());
        autofactura.setCodigoMoneda(factura.getMoneda().getCodigo());
        autofactura.setIdTipoFactura(factura.getIdTipoFactura());
        autofactura.setTipoFactura(factura.getTipoFactura().getDescripcion());
        autofactura.setCodigoTipoFactura(factura.getTipoFactura().getCodigo());
        autofactura.setIdEstado(factura.getIdEstado());
        autofactura.setEstado(factura.getEstado().getDescripcion());
        autofactura.setCodigoEstado(factura.getEstado().getCodigo());
        autofactura.setNroDocumentoVendedor(factura.getNroDocumentoVendedor());
        autofactura.setNroAutofactura(factura.getNroAutofactura());
        autofactura.setFecha(factura.getFecha());
        autofactura.setFechaVencimiento(factura.getFechaVencimiento());
        autofactura.setNombreVendedor(factura.getNombreVendedor());
        autofactura.setTelefono(factura.getTelefono());
        autofactura.setEmail(factura.getEmail());
        autofactura.setAnulado(factura.getAnulado());
        autofactura.setCobrado(factura.getCobrado());
        autofactura.setImpreso(factura.getImpreso());
        autofactura.setEntregado(factura.getEntregado());
        autofactura.setDigital(factura.getDigital());
        autofactura.setArchivoEdi(factura.getArchivoEdi());
        autofactura.setArchivoSet(factura.getArchivoSet());
        autofactura.setGeneradoEdi(factura.getGeneradoEdi());
        autofactura.setGeneradoSet(factura.getGeneradoSet());
        autofactura.setEstadoSincronizado(factura.getEstadoSincronizado());
        autofactura.setMontoTotalDescuentoParticular(factura.getMontoTotalDescuentoParticular());
        autofactura.setMontoTotalDescuentoGlobal(factura.getMontoTotalDescuentoGlobal());
        autofactura.setMontoTotalAutofactura(factura.getMontoTotalAutofactura()); 
        autofactura.setMontoTotalGuaranies(factura.getMontoTotalGuaranies());
        autofactura.setCdc(factura.getCdc()); 
        autofactura.setCodSeguridad(factura.getCodSeguridad()); 
        autofactura.setSaldo(factura.getSaldo()); 
        autofactura.setRazonSocial(factura.getRazonSocial()); 
        autofactura.setNroConstancia(factura.getNroConstancia()); 
        autofactura.setNroControlConstancia(factura.getNroControlConstancia()); 
        
        return autofactura;
    }

    
    @Override
    public AutoFactura anular(AutoFacturaParam param, UserInfoImpl userInfo) {

        if (!validToCancel(param)) {
            return null;
        }

        Boolean anular = Boolean.FALSE;
        String codigoEstado = "CANCELADO";

        AutoFactura factura = find(param.getId());

        if (factura.getDigital().equals('N')) {

            anular = Boolean.TRUE;

        } else {

            String urlSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "HOST_SIEDI_JSON");

            if (urlSiedi == null || urlSiedi.trim().isEmpty()) {
                param.addError(MensajePojo.createInstance().descripcion("No existe la configuración HOST_SIEDI_JSON"));
                return null;
            }

            EventoService service = new EventoService(userInfo.getToken(), urlSiedi);

            String dEst = "";
            String dPunt = "";
            String nroDoc = "";

            int j = 0;

            for (StringTokenizer stringTokenizer = new StringTokenizer(factura.getNroAutofactura(), "-"); stringTokenizer.hasMoreTokens(); j++) {

                String token = stringTokenizer.nextToken();

                switch (j) {
                    case 0:
                        dEst = token;
                        break;

                    case 1:
                        dPunt = token;
                        break;

                    case 2:
                        nroDoc = token;
                        break;
                }
            }

            BodyResponse<ResultadoOperacion> result = null;

            if (factura.getEstado().getCodigo().equals("APROBADO")) {
                result = service.cancelarDte(factura.getCdc(),
                        param.getMotivoAnulacion());
                codigoEstado = "CANCELADO";
            } else {
                result = service.inutilizar(factura.getTalonario().getTimbrado(),
                        dEst, dPunt, nroDoc, nroDoc, 4, param.getMotivoAnulacion());
                codigoEstado = "INUTILIZADO";
            }

            if (!result.getSuccess()) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Se produjo un error al anular el documento"));
            } else {
                if (result.getPayload().getOperaciones() != null
                        && !result.getPayload().getOperaciones().isEmpty()
                        && result.getPayload().getOperaciones().get(0).getEstado().equalsIgnoreCase("Aprobado")) {
                    anular = Boolean.TRUE;
                }
            }

            BodyResponse<List<ResultadoOperacionSiediApi>> result1 = null;
            String siediApi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API");
            String userSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
            String passSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
            String siediApiHost = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "SIEDI_API_HOST");

            if (factura.getEstado().getCodigo().startsWith("APROBADO")) {

                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.cancelarDteSiediApi(factura.getCdc(),
                            param.getMotivoAnulacion());
                    codigoEstado = "CANCELADO";
                } else {
                    result = service.cancelarDte(factura.getCdc(),
                            param.getMotivoAnulacion());
                    codigoEstado = "CANCELADO";
                }
            } else {
                if (siediApi != null && siediApi.equalsIgnoreCase("S")) {
                    ConnectionPojo connectionPojo = Host.createConnectionPojo(siediApiHost);
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);
                    SiediApiServiceClient siediApiService = new SiediApiServiceClient(tokenSiedi, connectionPojo);
                    result1 = siediApiService.inutilizarSiediApi(factura.getTalonario().getTimbrado(),
                            dEst, dPunt, nroDoc, nroDoc, 1, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                } else {
                    result = service.inutilizar(factura.getTalonario().getTimbrado(),
                            dEst, dPunt, nroDoc, nroDoc, 1, param.getMotivoAnulacion());
                    codigoEstado = "INUTILIZADO";
                }
            }
            if (result != null) {
                if (!result.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Rechazado")))) {

                        for (DetalleResultado dr : result1.getPayload().get(0).getResultado().getDetalles()) {
                           if ("4003".equals(dr.getCodigo())) {
                               anular = Boolean.TRUE;
                           } else {
                                param.addError(MensajePojo.createInstance()
                            .descripcion(dr.getMensaje()));
                           }     
                       } 
                    } else if (result.getPayload().getOperaciones() != null
                            && !result.getPayload().getOperaciones().isEmpty()
                            && result.getPayload().getOperaciones().get(0).getEstado().equalsIgnoreCase("Aprobado")) {
                        anular = Boolean.TRUE;
                    }
                }
            } else if (result1 != null) {
                if (!result1.getSuccess()) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("Se produjo un error al anular el documento"));
                } else {
                    if (result1.getPayload() != null
                            && !result1.getPayload().isEmpty()
                            && result1.getPayload().get(0).getResultado() != null
                            && ((result1.getPayload().get(0).getResultado().getEstado() != null
                            && result1.getPayload().get(0).getResultado().getEstado().equalsIgnoreCase("Aprobado"))
                            || (result1.getPayload().get(0).getResultado().getDetalles() != null
                            && result1.getPayload().get(0).getResultado().getDetalles().stream().filter(item -> item.getCodigo().equals("4003")).count() > 0))) {
                        anular = Boolean.TRUE;
                    }
                }
            } else {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Se produjo un error al anular el documento"));
            }
        }

        if (anular) {

             EstadoPojo estado = facades.getEstadoFacade().find(null, codigoEstado, "AUTOFACTURA");

            factura.setAnulado('S');
            factura.setIdEstado(estado.getId());
            factura.setIdMotivoAnulacion(param.getIdMotivoAnulacion());
            factura.setObservacionAnulacion(param.getObservacionAnulacion());

            edit(factura);

            return factura;
        } else {
            return null;
        }
        
    }

    private String getCDCWithoutDv(AutoFacturaParam param) {

        String establecimiento = "";
        String puntoExpedicion = "";
        String numeroDocumento = "";

        int i = 0;
        for (StringTokenizer stringTokenizer = new StringTokenizer(param.getNroAutofactura(), "-"); stringTokenizer.hasMoreTokens(); i++) {
            String token = stringTokenizer.nextToken();
            switch (i) {
                case 0:
                    establecimiento = token;
                    break;
                case 1:
                    puntoExpedicion = token;
                    break;
                case 2:
                    numeroDocumento = token;
                    break;
            }
        }

        String rucEmisor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");
        String ruc = "";
        String dvRuc = "";

        int j = 0;
        for (StringTokenizer stringTokenizer = new StringTokenizer(rucEmisor, "-"); stringTokenizer.hasMoreTokens(); j++) {
            String token = stringTokenizer.nextToken();
            switch (j) {
                case 0:
                    ruc = token;
                    break;
                case 1:
                    dvRuc = token;
                    break;
            }
        }

        String tipoDocumento = "04";//Factura
        String tipoContribuyente = "2";//Persona Juridica
        String tipoEmision = "1";//Normal
        String codSeguridad = "000000000" + param.getCodSeguridad();
        codSeguridad = codSeguridad.substring(codSeguridad.length() - 9, codSeguridad.length());

        return Document.getCDCWithoutDv(tipoDocumento, ruc, dvRuc,
                establecimiento, puntoExpedicion, numeroDocumento,
                tipoContribuyente, param.getFecha(), tipoEmision, codSeguridad);
    }

    private String getCDC(AutoFacturaParam param) {
        String cdcWithoutDv = getCDCWithoutDv(param);
        return Document.getCDC(cdcWithoutDv);
    }

    @Override
    public AutoFactura create(AutoFacturaParam param, UserInfoImpl userInfo) {

        if (isNull(param.getIdEncargado())) {
            param.setIdEncargado(userInfo.getId());
        }

        if (!validToCreate(userInfo, param)) {
            return null;
        }

        param.setCodSeguridad(Strings.randomNumeric(9));
        param.setCdc(getCDC(param));

        AutoFactura factura = new AutoFactura();
        factura.setIdEmpresa(param.getIdEmpresa());
        factura.setAnulado(param.getAnulado());
        factura.setCdc(param.getCdc());
        factura.setCobrado(param.getCobrado());
        factura.setCodSeguridad(param.getCodSeguridad());
        factura.setRuc(param.getRuc());
        factura.setRazonSocial(param.getRazonSocial());
        factura.setNroDocumentoVendedor(param.getNroDocumentoVendedor());
        factura.setIdNaturalezaVendedor(param.getIdNaturalezaVendedor());
        factura.setDigital(param.getDigital());
        factura.setDireccion(param.getDireccion());
        factura.setEntregado(param.getEntregado());
        factura.setFecha(param.getFecha());
        factura.setFechaEntrega(param.getFechaEntrega());
        factura.setFechaVencimiento(param.getFechaVencimiento());
        factura.setIdCiudad(param.getIdCiudad());
        factura.setIdDepartamento(param.getIdDepartamento());
        factura.setIdDistrito(param.getIdDistrito());
        factura.setIdCiudadTransaccion(param.getIdCiudadTransaccion());
        factura.setIdDepartamentoTransaccion(param.getIdDepartamentoTransaccion());
        factura.setIdDistritoTransaccion(param.getIdDistritoTransaccion());
        factura.setIdMoneda(param.getIdMoneda());
        factura.setIdTalonario(param.getIdTalonario());
        factura.setIdTipoCambio(param.getIdTipoCambio());
        factura.setIdTipoFactura(param.getIdTipoFactura());
        factura.setImpreso(param.getImpreso());
        
        factura.setMontoTotalDescuentoParticular(param.getMontoTotalDescuentoParticular().stripTrailingZeros());
        factura.setMontoTotalDescuentoGlobal(param.getMontoTotalDescuentoGlobal().stripTrailingZeros());

        factura.setMontoTotalAutofactura(param.getMontoTotalFactura().stripTrailingZeros());
        factura.setMontoTotalGuaranies(param.getMontoTotalGuaranies().stripTrailingZeros());
        factura.setSaldo(param.getMontoTotalGuaranies().stripTrailingZeros());
        factura.setNroCasa(param.getNroCasa());
        factura.setNroAutofactura(param.getNroAutofactura());
        factura.setObservacion(param.getObservacion());
        factura.setNombreVendedor(param.getNombreVendedor());
        factura.setRuc(param.getRuc());
        factura.setTelefono(param.getTelefono());
        factura.setEmail(param.getEmail());
        factura.setIdEncargado(param.getIdEncargado());
        factura.setArchivoEdi(param.getArchivoEdi());
        factura.setArchivoSet(param.getArchivoSet());
        factura.setGeneradoEdi('N');
        factura.setGeneradoSet('N');
        factura.setEstadoSincronizado(param.getEstadoSincronizado());
        factura.setCodSeguridad(param.getCodSeguridad());
        factura.setCdc(param.getCdc());
        factura.setIdEstado(param.getIdEstado());
        factura.setFechaInsercion(Calendar.getInstance().getTime());
        factura.setNroConstancia(param.getNroConstancia());
        factura.setNroControlConstancia(param.getNroControlConstancia());
        

        if (param.getIdLocalOrigen() != null) {
            factura.setIdLocalOrigen(param.getIdLocalOrigen());
        }

        if (param.getIdLocalDestino() != null) {
            factura.setIdLocalDestino(param.getIdLocalDestino());
        }

        create(factura);

        for (AutoFacturaDetalleParam detalle : param.getFacturaDetalles()) {
            detalle.setIdAutofactura(factura.getId());
            facades.getAutoFacturaDetalleFacade().create(detalle, userInfo);
        }

        if (!isNull(param.getFacturaDescuentos())) {
            for (FacturaDescuentoParam detalle : param.getFacturaDescuentos()) {
                detalle.setIdFactura(factura.getId());
                facades.getFacturaDescuentoFacade().create(detalle, userInfo);
            }
        }

        if (!isNull(param.getFacturaDncp())) {
            param.getFacturaDncp().setIdFactura(factura.getId());
            facades.getFacturaDncpFacade().create(param.getFacturaDncp(), userInfo);
        }

        if (!isNull(param.getFacturaNotificaciones())) {
            for (FacturaNotificacionParam detalle : param.getFacturaNotificaciones()) {
                detalle.setIdFactura(factura.getId());
                facades.getFacturaNotificacionFacade().create(detalle, userInfo);
            }
        }

        return factura;
    }

//    @Override
//    public void actualizarSaldoFactura(Integer idFactura, BigDecimal saldo) {
//
//        if(idFactura == null) {
//            return;
//        }
//        
//        Factura factura = find(idFactura);
//
//        BigDecimal temp = factura.getSaldo();
//        temp = temp.subtract(saldo);
//
//        Character cobrado = temp.compareTo(BigDecimal.ZERO) > 0 ? 'N' : 'S';
//
//        factura.setSaldo(temp);
//        factura.setCobrado(cobrado);
//
//        edit(factura);
//    }

    @Override
    public List<AutoFacturaPojo> findPojo(AutoFacturaParam param) {

        AbstractFind find = new AbstractFind(AutoFacturaPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idTalonario"),
                        getPath("talonario").get("fechaVencimiento"),
                        getPath("talonario").get("fechaInicio"),
                        getPath("talonario").get("timbrado"),
                        getPath("root").get("idMoneda"),
                        getPath("moneda").get("descripcion"),
                        getPath("moneda").get("codigo"),
                        getPath("root").get("idTipoCambio"),
                        getPath("root").get("idTipoFactura"),
                        getPath("tipoFactura").get("descripcion"),
                        getPath("tipoFactura").get("codigo"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("nroAutofactura"),
                        getPath("root").get("serie"),
                        getPath("root").get("fecha"),
                        getPath("root").get("fechaVencimiento"),
                        getPath("root").get("nombreVendedor"),
                        getPath("root").get("nroDocumentoVendedor"),
                        getPath("root").get("direccion"),
                        getPath("root").get("idDepartamento"),
                        getPath("root").get("idDistrito"),
                        getPath("root").get("idCiudad"),
                        getPath("root").get("idDepartamentoTransaccion"),
                        getPath("root").get("idDistritoTransaccion"),
                        getPath("root").get("idCiudadTransaccion"),
                        getPath("root").get("nroCasa"),                
                        getPath("root").get("telefono"),
                        getPath("root").get("email"),
                        getPath("root").get("anulado"),
                        getPath("root").get("cobrado"),
                        getPath("root").get("impreso"),
                        getPath("root").get("entregado"),
                        getPath("root").get("digital"),
                        getPath("root").get("archivoEdi"),
                        getPath("root").get("archivoSet"),
                        getPath("root").get("generadoEdi"),
                        getPath("root").get("generadoSet"),
                        getPath("root").get("estadoSincronizado"),
                        getPath("root").get("fechaEntrega"),
                        getPath("root").get("montoTotalDescuentoGlobal"),
                        getPath("root").get("montoTotalDescuentoParticular"),
                        getPath("root").get("montoTotalAutofactura"),
                        getPath("root").get("montoTotalGuaranies"),
                        getPath("root").get("cdc"),
                        getPath("root").get("codSeguridad"),
                        getPath("root").get("idProcesamiento"),
                        getPath("root").get("saldo"),
                        getPath("root").get("ruc"),
                        getPath("root").get("razonSocial"),
                        getPath("root").get("nroConstancia"),
                        getPath("root").get("nroControlConstancia")

                );
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<AutoFactura> find(AutoFacturaParam param) {

        AbstractFind find = new AbstractFind(AutoFactura.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, AutoFacturaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<AutoFactura> root = cq.from(AutoFactura.class);
        Join<AutoFactura, Empresa> empresa = root.join("empresa");
        Join<AutoFactura, Moneda> moneda = root.join("moneda");
        Join<AutoFactura, TipoFactura> tipoFactura = root.join("tipoFactura");
        Join<AutoFactura, Estado> estado = root.join("estado", JoinType.LEFT);
      //  Join<Factura, FacturaDncp> dncp = root.join("facturaDncp", JoinType.LEFT);
        Join<AutoFactura, Talonario> talonario = root.join("talonario");
        Join<AutoFactura, Procesamiento> procesamiento = null;
        Join<Procesamiento, DetalleProcesamiento> detalleProcesamiento = null;

        find.addPath("root", root);
        find.addPath("moneda", moneda);
        find.addPath("empresa", empresa);
        find.addPath("tipoFactura", tipoFactura);
        find.addPath("estado", estado);
        find.addPath("talonario", talonario);
      //  find.addPath("dncp", dncp);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdLocalTalonario() != null) {
            predList.add(qb.equal(talonario.get("idLocal"), param.getIdLocalTalonario()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }

        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }

        if (param.getIdTipoFactura() != null) {
            predList.add(qb.equal(root.get("idTipoFactura"), param.getIdTipoFactura()));
        }

        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getEntregado() != null) {
            predList.add(qb.equal(root.get("entregado"), param.getEntregado()));
        }

        if (param.getImpreso() != null) {
            predList.add(qb.equal(root.get("impreso"), param.getImpreso()));
        }

        if (param.getCobrado() != null) {
            predList.add(qb.equal(root.get("cobrado"), param.getCobrado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }

        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }

        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }

        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }

        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }

        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }


        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroAutofactura() != null && !param.getNroAutofactura().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroAutofactura")),
                    String.format("%%%s%%", param.getNroAutofactura().trim().toUpperCase())));
        }
        
        if (param.getNroDocumentoVendedor() != null && !param.getNroDocumentoVendedor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroDocumentoVendedor")),
                    String.format("%%%s%%", param.getNroDocumentoVendedor().trim().toUpperCase())));
        }
        
        if (param.getNombreVendedor() != null && !param.getNombreVendedor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombreVendedor")),
                    String.format("%%%s%%", param.getNombreVendedor().trim().toUpperCase())));
        }

        if (param.getNroFacturaEq() != null && !param.getNroFacturaEq().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroAutofactura"), param.getNroFacturaEq().trim()));
        }

        if (param.getCodigoResultado() != null && !param.getCodigoResultado().trim().isEmpty()) {
            procesamiento = root.join("procesamiento", JoinType.LEFT);
            detalleProcesamiento = procesamiento.join("detalleProcesamientos", JoinType.LEFT);
            predList.add(qb.equal(detalleProcesamiento.get("codigoResultado"), param.getCodigoResultado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(param.getNroFacturaDescendente() ? qb.desc(root.get("fecha")) : qb.asc(root.get("fecha")),
                param.getNroFacturaDescendente() ? qb.desc(root.get("nroAutofactura")) : qb.asc(root.get("nroAutofactura")),
                param.getIdDescendente() ? qb.desc(root.get("id")) : qb.asc(root.get("id")));

        cq.where(pred);

        if (detalleProcesamiento != null) {
            cq.groupBy(root.get("id"), moneda.get("id"), estado.get("id"),
                    talonario.get("id"),  tipoFactura.get("id"),
                    empresa.get("id"));
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(AutoFacturaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<AutoFactura> root = cq.from(AutoFactura.class);
        Join<AutoFactura, Empresa> empresa = root.join("empresa");
        Join<AutoFactura, Estado> estado = root.join("estado", JoinType.LEFT);
        Join<AutoFactura, Talonario> talonario = root.join("talonario");
        Join<AutoFactura, Procesamiento> procesamiento;
        Join<Procesamiento, DetalleProcesamiento> detalleProcesamiento;

        cq.select(qb.countDistinct(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdLocalTalonario() != null) {
            predList.add(qb.equal(talonario.get("idLocal"), param.getIdLocalTalonario()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }

        if (param.getIdTalonario() != null) {
            predList.add(qb.equal(root.get("idTalonario"), param.getIdTalonario()));
        }

        if (param.getIdTipoFactura() != null) {
            predList.add(qb.equal(root.get("idTipoFactura"), param.getIdTipoFactura()));
        }

        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }

        if (param.getEntregado() != null) {
            predList.add(qb.equal(root.get("entregado"), param.getEntregado()));
        }

        if (param.getImpreso() != null) {
            predList.add(qb.equal(root.get("impreso"), param.getImpreso()));
        }

        if (param.getCobrado() != null) {
            predList.add(qb.equal(root.get("cobrado"), param.getCobrado()));
        }

        if (param.getDigital() != null) {
            predList.add(qb.equal(root.get("digital"), param.getDigital()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
            predList.add(qb.equal(estado.get("codigo"), param.getCodigoEstado().trim()));
        }

        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }

        if (param.getArchivoSet() != null) {
            predList.add(qb.equal(root.get("archivoSet"), param.getArchivoSet()));
        }

        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }

        if (param.getGeneradoSet() != null) {
            predList.add(qb.equal(root.get("generadoSet"), param.getGeneradoSet()));
        }

        if (param.getCompraPublica() != null) {
            predList.add(qb.equal(root.get("compraPublica"), param.getCompraPublica()));
        }
        
        if (param.getEstadoSincronizado() != null) {
            predList.add(qb.equal(root.get("estadoSincronizado"), param.getEstadoSincronizado()));
        }

        if (param.getFecha() != null) {
            predList.add(qb.equal(root.get("fecha"), param.getFecha()));
        }

        if (param.getFechaDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fecha"), param.getFechaDesde()));
        }

        if (param.getFechaHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fecha"), param.getFechaHasta()));
        }

        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }

        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getFechaVencimientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoDesde()));
        }

        if (param.getFechaVencimientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaVencimiento"), param.getFechaVencimientoHasta()));
        }

        if (param.getCdc() != null && !param.getCdc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("cdc")),
                    String.format("%%%s%%", param.getCdc().trim().toUpperCase())));
        }

        if (param.getNombreVendedor() != null && !param.getNombreVendedor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nombreVendedor")),
                    String.format("%%%s%%", param.getNombreVendedor().trim().toUpperCase())));
        }
        
        if (param.getNroDocumentoVendedor() != null && !param.getNroDocumentoVendedor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroDocumentoVendedor")),
                    String.format("%%%s%%", param.getNroDocumentoVendedor().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getNroAutofactura() != null && !param.getNroAutofactura().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("nroAutofactura")),
                    String.format("%%%s%%", param.getNroAutofactura().trim().toUpperCase())));
        }

        if (param.getNroFacturaEq() != null && !param.getNroFacturaEq().trim().isEmpty()) {
            predList.add(qb.equal(root.get("nroAutofactura"), param.getNroFacturaEq().trim()));
        }

        if (param.getCodigoResultado() != null && !param.getCodigoResultado().trim().isEmpty()) {
            procesamiento = root.join("procesamiento", JoinType.LEFT);
            detalleProcesamiento = procesamiento.join("detalleProcesamientos", JoinType.LEFT);
            predList.add(qb.equal(detalleProcesamiento.get("codigoResultado"), param.getCodigoResultado().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
//
//    @Override
//    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param) {
//
//        String where = "true";
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        if (param.getIdEmpresa() != null) {
//            where = String.format("%s and f.id_empresa = '%d'", where, param.getIdEmpresa());
//        }
//
//        if (param.getFechaDesde() != null) {
//            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//
//        if (param.getFechaHasta() != null) {
//            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//
//        String sql = String.format("select f.ruc, 109 as tipo_comprobante, f.fecha, t.timbrado, "
//                + "f.nro_factura, f.monto_total_10, f.monto_total_5, "
//                + "f.monto_total_exento, f.monto_total_factura, "
//                + "case when f.id_tipo_factura = 1 then 2 else 1 end as tipo_factura, "
//                + "case when m.codigo in ('PYG','Gs.') then 'N' else 'S' end  as codigo_moneda, "
//                + "tc.venta, tc.compra "
//                + "from facturacion.factura f "
//                + "join comercial.moneda m on m.id = f.id_moneda "
//                + "join facturacion.talonario t on t.id = f.id_talonario "
//                + "left join facturacion.tipo_cambio tc on tc.id = f.id_tipo_cambio "
//                + "where %s offset %d limit %d",
//                where, param.getFirstResult(), param.getPageSize());
//
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        List<Object[]> list = q.getResultList();
//
//        List<RegistroComprobantePojo> result = new ArrayList<>();
//
//        for (Object[] objects : list) {
//            int i = 0;
//            RegistroComprobantePojo pojo = new RegistroComprobantePojo();
//            pojo.setIdentificacionComprador((String) objects[i++]);
//            pojo.setTipoComprobante((Integer) objects[i++]);
//            pojo.setFechaEmisionComprobante((Date) objects[i++]);
//            pojo.setNumeroTimbrado(new Integer((String) objects[i++]));
//            pojo.setNumeroComprobante((String) objects[i++]);
//            pojo.setMontoIva10(new BigDecimal(objects[i++] + ""));
//            pojo.setMontoIva5(new BigDecimal(objects[i++] + ""));
//            pojo.setMontoExento(new BigDecimal(objects[i++] + ""));
//            pojo.setMontoTotal(new BigDecimal(objects[i++] + ""));
//            pojo.setCondicionVenta((Integer) objects[i++]);
//            pojo.setMonedaExtrangera(((String) objects[i++]).charAt(0));
//            pojo.setTipoCambioVenta(new BigDecimal("0" + (objects[i++] == null ? "0" : objects[i++])));
//            pojo.setTipoCambioCompra(new BigDecimal("0" + (objects[i++] == null ? "0" : objects[i++])));
//            result.add(pojo);
//        }
//
//        return result;
//    }

//    @Override
//    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param) {
//
//        String where = "true";
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        if (param.getIdEmpresa() != null) {
//            where = String.format("%s and f.id_empresa = '%d'", where, param.getIdEmpresa());
//        }
//
//        if (param.getFechaDesde() != null) {
//            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//
//        if (param.getFechaHasta() != null) {
//            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//
//        String sql = String.format("select count(f.*) "
//                + "from facturacion.factura f "
//                + "where %s ", where);
//
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        Object object = q.getSingleResult();
//
//        Integer result = object == null
//                ? 0
//                : ((Number) object).intValue();
//
//        return result;
//    }

//    @Override
//    public List<FacturaPojo> findMonto(FacturaParam param) {
//
//        String where = "true ";
//
//        if (param.getId() != null) {
//            where = String.format("%s and f.id = %d", where, param.getId());
//        }
//
//        if (param.getIdEstado() != null) {
//            where = String.format("%s and f.id_estado = %d", where, param.getIdEstado());
//        }
//
//        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
//            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEstado().trim());
//        }
//
//        if (param.getIdEmpresa() != null) {
//            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
//        }
//
//        if (param.getIdMoneda() != null) {
//            where = String.format("%s and f.id_moneda = %d", where, param.getIdMoneda());
//        }
//
//        if (param.getIdCliente() != null) {
//            where = String.format("%s and f.id_cliente = %d", where, param.getIdCliente());
//        }
//
//        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
//            where = String.format("%s and f.id_cliente in (%s)", where, join(param.getIdClientes()));
//        }
//
//        if (param.getAnulado() != null) {
//            where = String.format("%s and f.anulado = '%s'", where, param.getAnulado());
//        }
//
//        if (param.getEntregado() != null) {
//            where = String.format("%s and f.entregado = '%s'", where, param.getEntregado());
//        }
//
//        if (param.getCobrado() != null) {
//            where = String.format("%s and f.cobrado = '%s'", where, param.getCobrado());
//        }
//
//        if (param.getArchivoEdi() != null) {
//            where = String.format("%s and f.archivo_edi = '%s'", where, param.getArchivoEdi());
//        }
//
//        if (param.getArchivoSet() != null) {
//            where = String.format("%s and f.archivo_set = '%s'", where, param.getArchivoSet());
//        }
//
//        if (param.getGeneradoEdi() != null) {
//            where = String.format("%s and f.generado_edi = '%s'", where, param.getGeneradoEdi());
//        }
//
//        if (param.getGeneradoSet() != null) {
//            where = String.format("%s and f.generado_set = '%s'", where, param.getGeneradoSet());
//        }
//
//        if (param.getCompraPublica() != null) {
//            where = String.format("%s and f.compra_publica = '%s'", where, param.getCompraPublica());
//        }
//
//        if (param.getEstadoSincronizado() != null) {
//            where = String.format("%s and f.estado_sincronizado = '%s'", where, param.getEstadoSincronizado());
//        }
//
//        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
//            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
//        }
//
//        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
//            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
//        }
//
//        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
//            where = String.format("%s and f.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
//        }
//
//        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
//            where = String.format("%s and oc.nro_orden_compra = '%s'", where, param.getNroOrdenCompra().trim());
//        }
//
//        if (param.getIdLocalTalonario() != null) {
//            where = String.format("%s and t.id_local = %d", where, param.getIdLocalTalonario());
//        }
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
//
//        if (param.getFechaDesde() != null) {
//            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//
//        if (param.getFechaHasta() != null) {
//            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//
//        if (param.getFechaVencimientoDesde() != null) {
//            where = String.format("%s and f.fecha_vencimiento >= '%s'", where, sdf.format(param.getFechaVencimientoDesde()));
//        }
//
//        if (param.getFechaVencimientoHasta() != null) {
//            where = String.format("%s and f.fecha_vencimiento <= '%s'", where, sdf.format(param.getFechaVencimientoHasta()));
//        }
//
//        if (param.getTieneRetencion() != null) {
//            where = String.format("%s and %s", where, param.getTieneRetencion()
//                    ? "not (rd.monto is null)"
//                    : "rd.monto is null");
//        }
//
//        if (param.getTieneOrdenCompra() != null) {
//            where = String.format("%s and %s", where, param.getTieneOrdenCompra()
//                    ? "not (oc.id is null)"
//                    : "oc.id is null");
//        }
//
//        if (param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {
//            where = String.format("%s and to_char(f.fecha, 'yyyy-MM') = '%s'", where, param.getAnhoMes());
//        }
//
//        String where2 = "true ";
//
//        if (param.getTieneSaldo() != null) {
//            where2 = String.format("%s and f.saldo %s", where2,
//                    param.getTieneSaldo() ? "> 0" : "<= 0");
//        }
//
//        String orderBy = "";
//
//        if (param.getNroFacturaDescendente() != null) {
//            orderBy = String.format("%s%s, ", orderBy, param.getNroFacturaDescendente()
//                    ? "f.nro_factura desc"
//                    : "f.nro_factura asc");
//        }
//
//        if (param.getIdDescendente() != null) {
//            orderBy = String.format("%s %s, ", orderBy, param.getIdDescendente()
//                    ? "f.id desc"
//                    : "f.id asc");
//        }
//
//        orderBy = String.format("%s f.fecha desc", orderBy);
//
//        String sql = String.format("with f as (with\n"
//                + "nc as (select ncd.id_factura, sum(ncd.monto_total) as monto from facturacion.nota_credito_detalle ncd, facturacion.nota_credito nc where ncd.id_nota_credito = nc.id and nc.anulado = 'N' group by ncd.id_factura),\n"
//                + "cd as (select cd.id_factura, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura),\n"
//                + "rd as (select rd.id_factura, sum(rd.monto_retenido) as monto from facturacion.retencion_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura)\n"
//                + "select distinct f.id, f.id_cliente, tf.codigo as codigo_tipo_factura, oc.nro_orden_compra, f.id_talonario, t.timbrado, l.descripcion as descripcion_local_talonario, f.id_moneda, m.codigo as codigo_moneda,\n"
//                + "f.id_tipo_cambio, tc.venta, tc.compra, f.id_tipo_factura, f.id_estado, e.codigo as codigo_estado, f.nro_factura, f.fecha, f.fecha_vencimiento, f.razon_social, f.direccion, f.ruc, f.telefono, f.email, f.anulado,\n"
//                + "f.cobrado, f.impreso, f.entregado, f.digital, f.archivo_edi, f.archivo_set, f.generado_edi, f.generado_set, f.estado_sincronizado, f.fecha_entrega, f.dias_credito, f.observacion, f.monto_iva_5, f.monto_imponible_5, f.monto_total_5,\n"
//                + "f.monto_iva_10, f.monto_imponible_10, f.monto_total_10, f.monto_total_exento, f.monto_iva_total, f.monto_imponible_total, f.monto_total_factura, f.monto_total_guaranies, f.cdc, f.id_procesamiento,\n"
//                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
//                + "coalesce(cd.monto, 0) as monto_cobro,\n"
//                + "coalesce(rd.monto, 0) as monto_retencion,\n"
//                + "f.saldo,\n"
//                + "p.id as id_comercial, coalesce(p.razon_social, p.nombre || ' ' || p.apellido) as comercial\n"
//                + "from facturacion.factura f\n"
//                + "join facturacion.talonario t on t.id = f.id_talonario\n"
//                + "left join info.local l on l.id = t.id_local\n"
//                + "left join facturacion.orden_compra oc on oc.id = f.id_orden_compra\n"
//                + "join comercial.moneda m on m.id = f.id_moneda\n"
//                + "join facturacion.tipo_factura tf on tf.id = f.id_tipo_factura\n"
//                + "left join info.estado e on e.id = f.id_estado\n"
//                + "left join facturacion.tipo_cambio tc on tc.id = f.id_tipo_cambio\n"
//                + "left join nc on nc.id_factura = f.id\n"
//                + "left join rd on rd.id_factura = f.id\n"
//                + "left join cd on cd.id_factura = f.id\n"
//                + "left join comercial.cliente c on c.id_cliente = f.id_cliente\n"
//                + "left join info.persona p on p.id = c.id_comercial\n"
//                + "where %s) select * from f where %s order by %s offset %d limit %d",
//                where, where2, orderBy, param.getFirstResult(), param.getPageSize());
//
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        List<FacturaPojo> result = new ArrayList<>();
//
//        List<Object[]> list = q.getResultList();
//
//        for (Object[] objects : list) {
//            int i = 0;
//            FacturaPojo pojo = new FacturaPojo();
//            pojo.setId((Integer) objects[i++]);
//            pojo.setIdCliente((Integer) objects[i++]);
//            pojo.setCodigoTipoFactura((String) objects[i++]);
//            pojo.setNroOrdenCompra((String) objects[i++]);
//            pojo.setIdTalonario((Integer) objects[i++]);
//            pojo.setTimbrado((String) objects[i++]);
//            pojo.setDescripcionLocalTalonario((String) objects[i++]);
//            pojo.setIdMoneda((Integer) objects[i++]);
//            pojo.setCodigoMoneda((String) objects[i++]);
//            pojo.setIdTipoCambio((Integer) objects[i++]);
//            pojo.setTipoCambioVenta((Number) objects[i++]);
//            pojo.setTipoCambioCompra((Number) objects[i++]);
//            pojo.setIdTipoFactura((Integer) objects[i++]);
//            pojo.setIdEstado((Integer) objects[i++]);
//            pojo.setCodigoEstado((String) objects[i++]);
//            pojo.setNroFactura((String) objects[i++]);
//            pojo.setFecha((Date) objects[i++]);
//            pojo.setFechaVencimiento((Date) objects[i++]);
//            pojo.setRazonSocial((String) objects[i++]);
//            pojo.setDireccion((String) objects[i++]);
//            pojo.setRuc((String) objects[i++]);
//            pojo.setTelefono((String) objects[i++]);
//            pojo.setEmail((String) objects[i++]);
//            pojo.setAnulado((Character) objects[i++]);
//            pojo.setCobrado((Character) objects[i++]);
//            pojo.setImpreso((Character) objects[i++]);
//            pojo.setEntregado((Character) objects[i++]);
//            pojo.setDigital((Character) objects[i++]);
//            pojo.setArchivoEdi((Character) objects[i++]);
//            pojo.setArchivoSet((Character) objects[i++]);
//            pojo.setGeneradoEdi((Character) objects[i++]);
//            pojo.setGeneradoSet((Character) objects[i++]);
//            pojo.setEstadoSincronizado((Character) objects[i++]);
//            pojo.setFechaEntrega((Date) objects[i++]);
//            pojo.setDiasCredito((Integer) objects[i++]);
//            pojo.setObservacion((String) objects[i++]);
//            pojo.setMontoIva5((BigDecimal) objects[i++]);
//            pojo.setMontoImponible5((BigDecimal) objects[i++]);
//            pojo.setMontoTotal5((BigDecimal) objects[i++]);
//            pojo.setMontoIva10((BigDecimal) objects[i++]);
//            pojo.setMontoImponible10((BigDecimal) objects[i++]);
//            pojo.setMontoTotal10((BigDecimal) objects[i++]);
//            pojo.setMontoTotalExento((BigDecimal) objects[i++]);
//            pojo.setMontoIvaTotal((BigDecimal) objects[i++]);
//            pojo.setMontoImponibleTotal((BigDecimal) objects[i++]);
//            pojo.setMontoTotalFactura((BigDecimal) objects[i++]);
//            pojo.setMontoTotalGuaranies((BigDecimal) objects[i++]);
//            pojo.setCdc((String) objects[i++]);
//            pojo.setIdProcesamiento((Integer) objects[i++]);
//            pojo.setMontoNotaCredito((BigDecimal) objects[i++]);
//            pojo.setMontoCobro((BigDecimal) objects[i++]);
//            pojo.setMontoRetencion((BigDecimal) objects[i++]);
//            pojo.setSaldo((BigDecimal) objects[i++]);
//            pojo.setIdComercial((Integer) objects[i++]);
//            pojo.setComercial((String) objects[i++]);
//
//            result.add(pojo);
//        }
//
//        return result;
//    }
//
//    /**
//     * Obtiene el tamaño de la lista de facturas con montos
//     *
//     * @param param Parámetros
//     * @return Lista
//     */
//    @Override
//    public Long findMontoSize(FacturaParam param) {
//
//        String where = "true ";
//
//        if (param.getId() != null) {
//            where = String.format("%s and f.id = %d", where, param.getId());
//        }
//
//        if (param.getIdEstado() != null) {
//            where = String.format("%s and f.id_estado = %d", where, param.getIdEstado());
//        }
//
//        if (param.getCodigoEstado() != null && !param.getCodigoEstado().trim().isEmpty()) {
//            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEstado().trim());
//        }
//
//        if (param.getIdEmpresa() != null) {
//            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
//        }
//
//        if (param.getIdMoneda() != null) {
//            where = String.format("%s and f.id_moneda = %d", where, param.getIdMoneda());
//        }
//
//        if (param.getIdCliente() != null) {
//            where = String.format("%s and f.id_cliente = %d", where, param.getIdCliente());
//        }
//
//        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
//            where = String.format("%s and f.id_cliente in (%s)", where, join(param.getIdClientes()));
//        }
//
//        if (param.getAnulado() != null) {
//            where = String.format("%s and f.anulado = '%s'", where, param.getAnulado());
//        }
//
//        if (param.getEntregado() != null) {
//            where = String.format("%s and f.estado = '%s'", where, param.getEntregado());
//        }
//
//        if (param.getCobrado() != null) {
//            where = String.format("%s and f.cobrado = '%s'", where, param.getCobrado());
//        }
//
//        if (param.getArchivoEdi() != null) {
//            where = String.format("%s and f.archivo_edi = '%s'", where, param.getArchivoEdi());
//        }
//
//        if (param.getArchivoSet() != null) {
//            where = String.format("%s and f.archivo_set = '%s'", where, param.getArchivoSet());
//        }
//
//        if (param.getGeneradoEdi() != null) {
//            where = String.format("%s and f.generado_edi = '%s'", where, param.getGeneradoEdi());
//        }
//
//        if (param.getGeneradoSet() != null) {
//            where = String.format("%s and f.generado_set = '%s'", where, param.getGeneradoSet());
//        }
//        
//        if (param.getCompraPublica() != null) {
//            where = String.format("%s and f.compra_publica = '%s'", where, param.getCompraPublica());
//        }
//
//        if (param.getEstadoSincronizado() != null) {
//            where = String.format("%s and f.estado_sincronizado = '%s'", where, param.getEstadoSincronizado());
//        }
//
//        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
//            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
//        }
//
//        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
//            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
//        }
//
//        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
//            where = String.format("%s and f.razon_social ilike '%%%s%%'", where, param.getRazonSocial().trim());
//        }
//
//        if (param.getNroOrdenCompra() != null && !param.getNroOrdenCompra().trim().isEmpty()) {
//            where = String.format("%s and oc.nro_orden_compra = '%s'", where, param.getNroOrdenCompra().trim());
//        }
//
//        if (param.getIdLocalTalonario() != null) {
//            where = String.format("%s and t.id_local = %d", where, param.getIdLocalTalonario());
//        }
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
//
//        if (param.getFechaDesde() != null) {
//            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//
//        if (param.getFechaHasta() != null) {
//            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//
//        if (param.getFechaVencimientoDesde() != null) {
//            where = String.format("%s and f.fecha_vencimiento >= '%s'", where, sdf.format(param.getFechaVencimientoDesde()));
//        }
//
//        if (param.getFechaVencimientoHasta() != null) {
//            where = String.format("%s and f.fecha_vencimiento <= '%s'", where, sdf.format(param.getFechaVencimientoHasta()));
//        }
//
//        if (param.getTieneRetencion() != null) {
//            where = String.format("%s and %s", where, param.getTieneRetencion()
//                    ? "not (rd.monto is null)"
//                    : "rd.monto is null");
//        }
//
//        if (param.getTieneOrdenCompra() != null) {
//            where = String.format("%s and %s", where, param.getTieneOrdenCompra()
//                    ? "not (oc.id is null)"
//                    : "oc.id is null");
//        }
//
//        if (param.getAnhoMes() != null && !param.getAnhoMes().trim().isEmpty()) {
//            where = String.format("%s and to_char(f.fecha, 'yyyy-MM') = '%s'", where, param.getAnhoMes());
//        }
//
//        String where2 = "true ";
//
//        if (param.getTieneSaldo() != null) {
//            where2 = String.format("%s and f.saldo %s", where2,
//                    param.getTieneSaldo() ? "> 0" : "<= 0");
//        }
//
//        String sql = String.format("with f as (with\n"
//                + "nc as (select ncd.id_factura, sum(ncd.monto_total) as monto from facturacion.nota_credito_detalle ncd, facturacion.nota_credito nc where ncd.id_nota_credito = nc.id and nc.anulado = 'N' group by ncd.id_factura),\n"
//                + "cd as (select cd.id_factura, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura),\n"
//                + "rd as (select rd.id_factura, sum(rd.monto_retenido) as monto from facturacion.retencion_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura)\n"
//                + "select f.id,\n"
//                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
//                + "coalesce(cd.monto, 0) as monto_cobro,\n"
//                + "coalesce(rd.monto, 0) as monto_retencion,\n"
//                + "f.saldo\n"
//                + "from facturacion.factura f\n"
//                + "join facturacion.talonario t on t.id = f.id_talonario\n"
//                + "left join info.estado e on e.id = f.id_estado\n"
//                + "left join facturacion.orden_compra oc on oc.id = f.id_orden_compra\n"
//                + "left join nc on nc.id_factura = f.id\n"
//                + "left join rd on rd.id_factura = f.id\n"
//                + "left join cd on cd.id_factura = f.id\n"
//                + "where %s) select count(*) from f where %s", where, where2);
//
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        Object object = q.getSingleResult();
//
//        Long result = object == null
//                ? 0
//                : ((Number) object).longValue();
//
//        return result;
//    }

    /**
     * Obtiene el tamaño de la lista de facturas con montos
     *
     * @param param Parámetros
     * @return Lista
     */
//    @Override
//    public FacturaPojo findMontoSumatoria(FacturaParam param) {
//
//        String where = "true ";
//
//        if (param.getIdCliente() != null) {
//            where = String.format("%s and f.id_cliente = %d", where, param.getIdCliente());
//        }
//
//        if (param.getIdClientes() != null && !param.getIdClientes().isEmpty()) {
//            where = String.format("%s and f.id_cliente in (%s)", where, join(param.getIdClientes()));
//        }
//
//        if (param.getIdEmpresa() != null) {
//            where = String.format("%s and f.id_empresa = %d", where, param.getIdEmpresa());
//        }
//
//        if (param.getEntregado() != null) {
//            where = String.format("%s and f.estado = '%s'", where, param.getEntregado());
//        }
//
//        if (param.getCobrado() != null) {
//            where = String.format("%s and f.cobrado = '%s'", where, param.getCobrado());
//        }
//
//        if (param.getArchivoEdi() != null) {
//            where = String.format("%s and f.archivo_edi = '%s'", where, param.getArchivoEdi());
//        }
//
//        if (param.getArchivoSet() != null) {
//            where = String.format("%s and f.archivo_set = '%s'", where, param.getArchivoSet());
//        }
//
//        if (param.getGeneradoEdi() != null) {
//            where = String.format("%s and f.generado_edi = '%s'", where, param.getGeneradoEdi());
//        }
//
//        if (param.getGeneradoSet() != null) {
//            where = String.format("%s and f.generado_set = '%s'", where, param.getGeneradoSet());
//        }
//
//        if (param.getCompraPublica() != null) {
//            where = String.format("%s and f.compra_publica = '%s'", where, param.getCompraPublica());
//        }
//        
//        if (param.getEstadoSincronizado() != null) {
//            where = String.format("%s and f.estado_sincronizado = '%s'", where, param.getEstadoSincronizado());
//        }
//
//        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
//            where = String.format("%s and f.ruc ilike '%%%s%%'", where, param.getRuc().trim());
//        }
//
//        if (param.getNroFactura() != null && !param.getNroFactura().trim().isEmpty()) {
//            where = String.format("%s and f.nro_factura ilike '%%%s%%'", where, param.getNroFactura().trim());
//        }
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        if (param.getFechaDesde() != null) {
//            where = String.format("%s and f.fecha >= '%s'", where, sdf.format(param.getFechaDesde()));
//        }
//
//        if (param.getFechaHasta() != null) {
//            where = String.format("%s and f.fecha <= '%s'", where, sdf.format(param.getFechaHasta()));
//        }
//
//        if (param.getFechaVencimientoDesde() != null) {
//            where = String.format("%s and f.fecha_vencimiento >= '%s'", where, sdf.format(param.getFechaVencimientoDesde()));
//        }
//
//        if (param.getFechaVencimientoHasta() != null) {
//            where = String.format("%s and f.fecha_vencimiento <= '%s'", where, sdf.format(param.getFechaVencimientoHasta()));
//        }
//
//        String where2 = "true ";
//
//        if (param.getTieneSaldo() != null) {
//            where2 = String.format("%s and f.saldo %s", where2,
//                    param.getTieneSaldo() ? "> 0" : "<= 0");
//        }
//
//        String sql = String.format("with f as (with\n"
//                + "nc as (select ncd.id_factura, sum(ncd.monto_total) as monto from facturacion.nota_credito_detalle ncd, facturacion.nota_credito nc where ncd.id_nota_credito = nc.id and nc.anulado = 'N' group by ncd.id_factura),\n"
//                + "cd as (select cd.id_factura, sum(cd.monto_cobro) as monto from facturacion.cobro_detalle cd left join info.estado e on e.id = cd.id_estado where e.codigo = 'COBRADO' group by cd.id_factura),\n"
//                + "rd as (select rd.id_factura, sum(rd.monto_retenido) as monto from facturacion.retencion_detalle rd left join info.estado e on e.id = rd.id_estado where e.codigo = 'CONFIRMADO' group by rd.id_factura)\n"
//                + "select f.id, f.id_cliente, f.id_moneda, m.descripcion as moneda, f.id_tipo_factura, tf.descripcion as tipo_factura,\n"
//                + "f.nro_factura, f.fecha, f.fecha_vencimiento, f.razon_social, f.direccion, f.ruc, f.telefono, f.anulado,\n"
//                + "f.cobrado, f.impreso, f.entregado, f.digital, f.fecha_entrega, f.dias_credito, f.observacion, f.monto_iva_5, f.monto_imponible_5, f.monto_total_5,\n"
//                + "f.monto_iva_10, f.monto_imponible_10, f.monto_total_10, f.monto_total_exento, f.monto_iva_total, f.monto_imponible_total, f.monto_total_factura, f.cdc,\n"
//                + "coalesce(nc.monto, 0) as monto_nota_credito,\n"
//                + "coalesce(cd.monto, 0) as monto_cobro,\n"
//                + "coalesce(rd.monto, 0) as monto_retencion,\n"
//                + "f.monto_total_factura - coalesce(nc.monto, 0) - coalesce(cd.monto, 0) - coalesce(rd.monto, 0) as saldo\n"
//                + "from facturacion.factura f\n"
//                + "left join comercial.moneda m on m.id = f.id_moneda\n"
//                + "left join facturacion.tipo_factura tf on tf.id = f.id_tipo_factura\n"
//                + "left join nc on nc.id_factura = f.id\n"
//                + "left join rd on rd.id_factura = f.id\n"
//                + "left join cd on cd.id_factura = f.id\n"
//                + "where %s) select sum(f.monto_total_factura) as monto_total_factura,\n"
//                + "sum(f.monto_nota_credito) as monto_nota_credito, sum(f.monto_cobro) as monto_cobro,\n"
//                + "sum(f.monto_retencion) as monto_retencion, sum(f.saldo) as saldo, count(*) as cantidad from f where %s", where, where2);
//
//        Query q = getEntityManager().createNativeQuery(sql);
//
//        FacturaPojo pojo = new FacturaPojo();
//        pojo.setMontoTotalFactura(BigDecimal.ZERO);
//        pojo.setMontoNotaCredito(BigDecimal.ZERO);
//        pojo.setMontoCobro(BigDecimal.ZERO);
//        pojo.setMontoRetencion(BigDecimal.ZERO);
//        pojo.setSaldo(BigDecimal.ZERO);
//        pojo.setCantidad(0);
//
//        List<Object[]> list = q.getResultList();
//
//        for (Object[] objects : list) {
//            pojo.setMontoTotalFactura((BigDecimal) objects[0]);
//            pojo.setMontoNotaCredito((BigDecimal) objects[1]);
//            pojo.setMontoCobro((BigDecimal) objects[2]);
//            pojo.setMontoRetencion((BigDecimal) objects[3]);
//            pojo.setSaldo((BigDecimal) objects[4]);
//            pojo.setCantidad(((Number) objects[5]).intValue());
//        }
//
//        return pojo;
//    }

    @Override
    public byte[] getPdfFactura(AutoFacturaParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGetPdf(param)) {
            return null;
        }

        AutoFacturaPojo factura = findFirstPojo(param);

        byte[] result = null;

        Closer closer = Closer.instance();

        try {
            if (factura.getDigital().equals('N')) {

//                InputStream is = null;
//                Boolean jdbc = Boolean.FALSE;
//
//                String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_FACTURA_AUTOIMPRESOR");
//
//                if (templateFacturaAutoimpresor != null && !templateFacturaAutoimpresor.trim().isEmpty()) {
//                    is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
//                    jdbc = Boolean.FALSE;
//                }
//
//                if (isNull(is)) {
//                    String templateFacturaAutoimpresorJdbc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_FACTURA_AUTOIMPRESOR_JDBC");
//                    is = facades.getReportUtils().getResource(templateFacturaAutoimpresorJdbc, closer);
//                    jdbc = Boolean.TRUE;
//                }
//
//                if (isNull(is)) {
//                    param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para factura autoimpresor"));
//                } else {
//
//                    byte[] template = IOUtils.toByteArray(is);
//
//                    List<byte[]> bytesList = Lists.empty();
//
//                    //FacturaAutoimpresorParam faparams = facades.getReportUtils().getParam(closer, factura);
//                    if (param.getImprimirOriginal()) {
//                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//                            FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
//                            params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.ORIGINAL);
//                            // byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//                            byte[] bytes = generarPdfFactura(closer, template, params, jdbc);
//                            if (bytes != null && bytes.length > 0) {
//                                bytesList.add(bytes);
//                            }
//                        }
//                    }
//
//                    if (param.getImprimirDuplicado()) {
//                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//                            FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
//                            params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.DUPLICADO);
//                            //byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//                            byte[] bytes = generarPdfFactura(closer, template, params, jdbc);
//                            if (bytes != null && bytes.length > 0) {
//                                bytesList.add(bytes);
//                            }
//                        }
//                    }
//
//                    if (param.getImprimirTriplicado()) {
//                        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//                            FacturaAutoimpresorParam params = facades.getReportUtils().getParam(closer, factura);
//                            params.setTipoFactura(FacturaAutoimpresorParam.TipoFactura.TRIPLICADO);
//                            //byte[] bytes = JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//                            byte[] bytes = generarPdfFactura(closer, template, params, jdbc);
//                            if (bytes != null && bytes.length > 0) {
//                                bytesList.add(bytes);
//                            }
//                        }
//                    }
//
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                    facades.getReportUtils().merge(bytesList, baos, null);
//                    result = baos.toByteArray();
//                }
            } else {

                Boolean ticket = param.getTicket();
                String cdc = factura.getCdc();
                String nombreArchivo = String.format("AF_%s.txt", cdc);
                JsonObject object = new JsonObject();
                           
                String hostSiedi;
                boolean siediApi = Boolean.TRUE.equals(param.getSiediApi())
                        || "S".equalsIgnoreCase(facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API"));

                String claveHost = siediApi ? "SIEDI_API_HOST" : "HOST_SIEDI";
                hostSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), claveHost);
                
                if (siediApi) {

                    ConnectionPojo connectionPojo = Host.createConnectionPojo(hostSiedi);
                    String userSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_USER");
                    String passSiedi = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "ARCHIVOS_DTE_SIEDI_API_PASS");
                    String tokenSiedi = LoginServiceSiediApi.doLogin(userSiedi, passSiedi, connectionPojo);

                    if (tokenSiedi == null) {

                        param.addError(MensajePojo.createInstance()
                                .descripcion("Credenciales de siediApi ncorrectas."));
                        return null;
                    }

                    return KudeDteAdapter.obtener(tokenSiedi, hostSiedi, cdc, nombreArchivo, object, ticket, true);
                } else {
                    return KudeDteAdapter.obtener(null, hostSiedi, cdc, nombreArchivo, object, ticket, false);
                }
            }
        } finally {
            closer.close();
        }

        return result;
    }

//    private byte[] generarPdfFactura(Closer closer, byte[] template, FacturaAutoimpresorParam params, Boolean jdbc) throws Exception {
//        try (ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
//            if (jdbc) {
//                //Obtenemos una conexion del DataSource
//                Context ctx = new InitialContext();
//                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
//                try (Connection connection = ds.getConnection()) {
//                    return JasperReporteGenerator.exportReportToPdfBytes(params, connection, bais);
//                }
//            } else {
//                return JasperReporteGenerator.exportReportToPdfBytes(params, bais);
//            }
//        }
//    }

//    @Override
//    public byte[] getXlsReporteVenta(FacturaParam param, UserInfoImpl userInfo) throws Exception {
//
//        if (!param.isValidToGetReporteVenta()) {
//            return null;
//        }
//
//        byte[] result = null;
//
//        Closer closer = Closer.instance();
//
//        try {
//
//            ReporteVentaFacturaParam params = facades.getReportUtils().getReporteVentaFacturaParam(closer, param);
//
//            String templateFacturaAutoimpresor = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_REPORTE_VENTA_FACTURA");
//            InputStream is = facades.getReportUtils().getResource(templateFacturaAutoimpresor, closer);
//
//            if (isNull(is)) {
//                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el reporte de venta"));
//            } else {
//
//                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//                configuration.setOnePagePerSheet(false);
//                configuration.setDetectCellType(true);
//                //borrar espacio entre filas
//                configuration.setRemoveEmptySpaceBetweenRows(true);
//                //borrar espacio entre columnas
//                configuration.setRemoveEmptySpaceBetweenColumns(true);
//                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
//                configuration.setIgnoreCellBorder(false);
//                configuration.setWhitePageBackground(false);
//
//                //Obtenemos una conexion del DataSource
//                Context ctx = new InitialContext();
//                DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
//                try (Connection connection = ds.getConnection()) {
//                    result = JasperReporteGenerator.exportReportToXlsxBytes(params, connection, is, configuration);
//                }
//            }
//
//        } finally {
//            closer.close();
//        }
//
//        return result;
//    }

//    @Override
//    public byte[] getXlsEstadoCuenta(FacturaParam param, UserInfoImpl userInfo) {
//
//        if (!param.isValidToGetEstadoCuenta()) {
//            return null;
//        }
//
//        byte[] result = null;
//
//        Closer closer = Closer.instance();
//
//        try {
//
//            Comparator<EstadoCuentaDetalle> comparadorMultiple = Comparator.comparing(EstadoCuentaDetalle::getNroFacturaAsociada)
//                    .thenComparing(Comparator.comparing(EstadoCuentaDetalle::getFechaEmision));
//
//            String cliente = null;
//
//            List<EstadoCuentaDetalle> detalles = Lists.empty();
//
//            int i = 0;
//            param.setAnulado('N');
//            param.setFirstResult(i);
//            param.setPageSize(10);
//
//            List<FacturaPojo> facturas = facades.getFacturaFacade().findPojo(param);
//
//            while (!facturas.isEmpty()) {
//
//                for (FacturaPojo factura : facturas) {
//
//                    cliente = factura.getRazonSocial();
//
//                    EstadoCuentaDetalle item = new EstadoCuentaDetalle();
//                    item.setIdFacturaAsociada(factura.getId());
//                    item.setNroFacturaAsociada(factura.getNroFactura());
//                    item.setNroComprobante(factura.getNroFactura());
//                    item.setTipoComprobante("Factura");
//                    item.setFechaEmision(factura.getFecha());
//                    item.setFechaVencimiento(factura.getFechaVencimiento());
//                    item.setDiasAtraso(Dates.diference(Calendar.getInstance().getTime(), factura.getFechaVencimiento(), TimeUnit.DAYS));
//                    item.setImporte(factura.getMontoTotalFactura());
//
//                    List<EstadoCuentaDetalle> lista = Lists.empty();
//                    lista.add(item);
//
//                    RetencionDetalleParam rparam = new RetencionDetalleParam();
//                    rparam.setCodigoEstado("CONFIRMADO");
//                    rparam.setIdFactura(factura.getId());
//                    rparam.setFirstResult(0);
//                    rparam.setPageSize(30000);
//                    List<RetencionDetalle> retenciones = facades.getRetencionDetalleFacade().find(rparam);
//
//                    for (RetencionDetalle retencion : retenciones) {
//                        EstadoCuentaDetalle item1 = new EstadoCuentaDetalle();
//                        item1.setIdFacturaAsociada(factura.getId());
//                        item1.setNroFacturaAsociada(factura.getNroFactura());
//                        item1.setNroComprobante(retencion.getRetencion().getNroRetencion());
//                        item1.setTipoComprobante("Retención");
//                        item1.setFechaEmision(retencion.getRetencion().getFecha());
//                        item1.setImporte(retencion.getMontoRetenido().negate());
//                        lista.add(item1);
//                    }
//
//                    NotaCreditoDetalleParam ncparam = new NotaCreditoDetalleParam();
//                    ncparam.setIdFactura(factura.getId());
//                    ncparam.setFirstResult(0);
//                    ncparam.setPageSize(30000);
//                    List<NotaCreditoDetalle> notaCreditoDetalles = facades.getNotaCreditoDetalleFacade().find(ncparam);
//
//                    for (NotaCreditoDetalle notaCreditoDetalle : notaCreditoDetalles) {
//
//                        if (!notaCreditoDetalle.getNotaCredito().getAnulado().equals('N')) {
//                            continue;
//                        }
//
//                        EstadoCuentaDetalle item1 = new EstadoCuentaDetalle();
//                        item1.setIdFacturaAsociada(factura.getId());
//                        item1.setNroFacturaAsociada(factura.getNroFactura());
//                        item1.setNroComprobante(notaCreditoDetalle.getNotaCredito().getNroNotaCredito());
//                        item1.setTipoComprobante("Nota de Crédito");
//                        item1.setFechaEmision(notaCreditoDetalle.getNotaCredito().getFecha());
//                        item1.setImporte(notaCreditoDetalle.getMontoTotal().negate());
//                        lista.add(item1);
//                    }
//
//                    CobroDetalleParam cparam = new CobroDetalleParam();
//                    cparam.setIdFactura(factura.getId());
//                    cparam.setCodigoEstado("COBRADO");
//                    cparam.setFirstResult(0);
//                    cparam.setPageSize(30000);
//                    List<CobroDetalle> cobroDetalles = facades.getCobroDetalleFacade().find(cparam);
//
//                    for (CobroDetalle cobroDetalle : cobroDetalles) {
//                        EstadoCuentaDetalle item1 = new EstadoCuentaDetalle();
//                        item1.setIdFacturaAsociada(factura.getId());
//                        item1.setNroFacturaAsociada(factura.getNroFactura());
//                        item1.setNroComprobante(cobroDetalle.getCobro().getNroRecibo());
//                        item1.setTipoComprobante("Recibo");
//                        item1.setFechaEmision(cobroDetalle.getCobro().getFecha());
//                        item1.setImporte(cobroDetalle.getMontoCobro().negate());
//                        lista.add(item1);
//                    }
//
//                    lista = lista.stream().sorted(comparadorMultiple).collect(Collectors.toList());
//
//                    for (int j = 0; j < lista.size(); j++) {
//
//                        EstadoCuentaDetalle actual = lista.get(j);
//                        EstadoCuentaDetalle anterior = j - 1 < 0 ? null : lista.get(j - 1);
//
//                        if (j == 0) {
//                            actual.setSaldoFactura(actual.getImporte());
//                            continue;
//                        }
//
//                        actual.setSaldoFactura(anterior.getSaldoFactura().add(actual.getImporte()));
//
//                    }
//
//                    detalles.addAll(lista);
//                }
//
//                i = i + 10;
//
//                param.setFirstResult(i);
//
//                facturas = facades.getFacturaFacade().findPojo(param);
//            }
//
//            detalles = detalles.stream().sorted(comparadorMultiple).collect(Collectors.toList());
//
//            for (int j = 0; j < detalles.size(); j++) {
//
//                EstadoCuentaDetalle actual = detalles.get(j);
//                EstadoCuentaDetalle anterior = j - 1 < 0 ? null : detalles.get(j - 1);
//
//                if (j == 0) {
//                    actual.setSaldoTotal(actual.getImporte());
//                    continue;
//                }
//
//                actual.setSaldoTotal(anterior.getSaldoTotal().add(actual.getImporte()));
//
//            }
//
//            EstadoCuentaParam params = facades.getReportUtils().getEstadoCuentaParam(closer, cliente,
//                    param.getIdEmpresa(), param.getFechaDesde(), param.getFechaHasta(),
//                    detalles.isEmpty() ? BigDecimal.ZERO : detalles.get(detalles.size() - 1).getSaldoTotal(), detalles);
//
//            String templateEstadoCuenta = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "TEMPLATE_ESTADO_CUENTA");
//            InputStream is = facades.getReportUtils().getResource(templateEstadoCuenta, closer);
//
//            if (isNull(is)) {
//                param.addError(MensajePojo.createInstance().descripcion("No se encuentra el template para el estado de cuenta"));
//            } else {
//
//                SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//                configuration.setOnePagePerSheet(false);
//                configuration.setDetectCellType(false);
//                //borrar espacio entre filas
//                configuration.setRemoveEmptySpaceBetweenRows(true);
//                //borrar espacio entre columnas
//                configuration.setRemoveEmptySpaceBetweenColumns(true);
//                //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
//                configuration.setIgnoreCellBorder(false);
//                configuration.setWhitePageBackground(false);
//
//                result = JasperReporteGenerator.exportReportToXlsxBytes(params, is, configuration);
//            }
//
//        } finally {
//            closer.close();
//        }
//
//        return result;
//    }

//    @Override
//    public List<String> createFacturaMasiva(List<FacturaParam> fpl, List<FacturaDetalleParam> fdpl, UserInfoImpl userInfo) {
//     
//        //Tratamiento para cabecera
//        List<String> result = new ArrayList();
//        for (FacturaParam facturaParam : fpl) {
//            FacturaParam fpNuevo = new FacturaParam();
//            ClienteParam cp = new ClienteParam();
//            List<Cliente> lista = new ArrayList<>();
//
//            //Se obtiene la lista de clientes con el filtro de RUC asignado
//            cp.setNroDocumentoEq(facturaParam.getRuc());
//            cp.setIdEmpresa(userInfo.getIdEmpresa());
//            lista = facades.getClienteFacade().find(cp);
//            //Si el identificador de cliente no es null
//            if (!lista.isEmpty() && lista.get(0).getIdCliente() != null) {
//                TalonarioParam tp = new TalonarioParam();
//                FacturaPojo fp = new FacturaPojo();
//
//                tp.setDigital(facturaParam.getDigital());
//                tp.setIdEmpresa(userInfo.getIdEmpresa());
//                Date date = Calendar.getInstance().getTime();
//                tp.setFecha(date);
//                tp.setIdTipoDocumento(1);
//                TalonarioPojo talonario = facades
//                        .getTalonarioFacade()
//                        .getTalonario(tp);
//
//                if (talonario != null) {
//                    //TODO: buscar el id moneda por el cod moneda
//                    //Se setean valores de cabecera de factura
//                    fp = facades.getFacturaFacade().obtenerDatosCrear(talonario, lista.get(0).getIdCliente());
//                    fpNuevo.setIdCliente(lista.get(0).getIdCliente());
//                    fpNuevo.setIdNaturalezaCliente(lista.get(0).getIdNaturalezaCliente());
//                    fpNuevo.setIdEmpresa(userInfo.getIdEmpresa());
//                    fpNuevo.setIdMoneda(1);
//                    fpNuevo.setNroFactura(fp.getNroFactura());
//                    fpNuevo.setRazonSocial(fp.getRazonSocial());
//                    fpNuevo.setRuc(fp.getRuc());
//                    fpNuevo.setTelefono(fp.getTelefono());
//                    fpNuevo.setEmail(fp.getEmail());
//                    fpNuevo.setDigital(facturaParam.getDigital());
//                    if(facturaParam.getDigital() == 'S'){
//                        fpNuevo.setArchivoSet('S');
//                    }
//                    fpNuevo.setAnulado('N');
//                    fpNuevo.setCobrado('N');
//                    fpNuevo.setImpreso('N');
//                    fpNuevo.setEntregado('N');
//                    fpNuevo.setIdTalonario(fp.getIdTalonario());
//                    fpNuevo.setIdTipoFactura(facturaParam.getCodigoTipoFactura().equalsIgnoreCase("CONTADO") ? 2 : 1);
//                    if (fpNuevo.getIdTipoFactura().equals(1)) {
//                        fpNuevo.setFecha(facturaParam.getFecha());
//                        fpNuevo.setCodigoTipoOperacionCredito("PLAZO");
//                        fpNuevo.setDiasCredito(facturaParam.getDiasCredito());
//
//                        Calendar cal = Calendar.getInstance();
//                        cal.setTime(facturaParam.getFecha());
//                        cal.add(Calendar.DAY_OF_MONTH, facturaParam.getDiasCredito());
//                        fpNuevo.setFechaVencimiento(cal.getTime());
//                    } else {
//                        fpNuevo.setFecha(facturaParam.getFecha());
//                        fpNuevo.setFechaVencimiento(facturaParam.getFecha());
//                    }
//
//                    List<FacturaDetalleParam> listaDetalleFactura = fdpl
//                            .stream()
//                            .filter(c -> c.getNroFactura().equals(facturaParam.getNroFactura()))
//                            .collect(Collectors.toList());
//
//                    calcularDetalle(fpNuevo, listaDetalleFactura);
//
//                    Factura factura = facades
//                            .getFacturaFacade()
//                            .create(fpNuevo, userInfo);
//                    if (factura == null ){
//                        String error=String.format("%s;ERROR;",facturaParam.getNroFactura());
//                        error = error.concat("%s| ");
//                        for(MensajePojo mp : fpNuevo.getErrores()){
//                            error = String.format(error,mp.getDescripcion());
//                            error = error.concat("%s| ");
//                        }
//                        //Al final se eliminan los ultimos 3 caracteres sobrantes %s;
//                        error = error.substring(0,error.length()-5);
//                        error=error.concat(";;");
//                        result.add(error);
//                    } else {
//                        String ok=String.format("%s;OK;;%s",facturaParam.getNroFactura(),factura.getId().toString());
//                        result.add(ok);
//                    }
//                    
//                }
//
//            } else {
//                String error=String.format("%s;ERROR;El Nro. de documento asignado a esta factura no pertenece a ningun cliente registrado en el sistema;;",facturaParam.getNroFactura());
//                result.add(error);
//            }
//          
//        }
//
//        return result;
//    }

//    public void calcularDetalle(FacturaParam fpNuevo, List<FacturaDetalleParam> listaDetalleFactura) {
//        BigDecimal totalFactura = new BigDecimal("0");
//        int i = 0;
//
//        for (FacturaDetalleParam info : listaDetalleFactura) {
//            i++;
//            info.setPorcentajeGravada(100);
//            // Se calcula el monto total del detalle
//            BigDecimal cantidad = info.getCantidad();
//            BigDecimal precioUnitarioConIVA = info.getPrecioUnitarioConIva();
//            BigDecimal montoPrecioXCantidad = precioUnitarioConIVA.multiply(cantidad);
//
//            // Se calcula el monto base
//            BigDecimal auxA = new BigDecimal(info.getPorcentajeIva()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
//            BigDecimal auxB = new BigDecimal(info.getPorcentajeGravada()).divide(new BigDecimal("100"), 5, RoundingMode.HALF_UP);
//            BigDecimal auxC = auxA.multiply(auxB);
//            BigDecimal pfinal = auxC.add(BigDecimal.ONE);
//            BigDecimal montoBase = montoPrecioXCantidad.divide(pfinal, 5, RoundingMode.HALF_UP);
//
//            // Se calcula el monto exento gravado
//            BigDecimal auxD = new BigDecimal("100").subtract(new BigDecimal(info.getPorcentajeGravada()));
//            BigDecimal auxF = auxD.divide(new BigDecimal("100"));
//            BigDecimal montoExentoGravado = montoBase.multiply(auxF);
//
//            BigDecimal montoImponible = montoBase.multiply(auxB);
//
//            //Se calcula el monto iva
//            // Integer aux3 = info.getPorcentajeIva() / 100;
//            BigDecimal montoIva = montoImponible.multiply(auxA);
//
//            //Se suman los montos para el total
//            BigDecimal total = (montoExentoGravado.add(montoImponible)).add(montoIva);
//
//            //Precio unitario
//            BigDecimal precioUnitarioSinIva = precioUnitarioConIVA.divide(pfinal, 5, RoundingMode.HALF_UP);
//            
//            info.setNroLinea(i);
//            info.setMontoIva(montoIva);
//            info.setMontoImponible(montoImponible);
//            info.setMontoExentoGravado(montoExentoGravado);
//            info.setMontoTotal(total);
//            info.setPrecioUnitarioSinIva(precioUnitarioSinIva);
//            info.setDescuentoParticularUnitario(BigDecimal.valueOf(0));
//            info.setDescuentoGlobalUnitario(BigDecimal.valueOf(0));
//            info.setMontoDescuentoParticular(BigDecimal.valueOf(0));
//            info.setMontoDescuentoGlobal(BigDecimal.valueOf(0));
//            totalFactura = totalFactura.add(total);
//        }
//        fpNuevo.setMontoTotalFactura(totalFactura);
//        fpNuevo.setMontoTotalGuaranies(totalFactura);
//        fpNuevo.setFacturaDetalles(listaDetalleFactura);
//        calcularTotalesGenerales(fpNuevo, fpNuevo.getFacturaDetalles());
//
//    }

//    public void calcularTotalesGenerales(FacturaParam fpNuevo, List<FacturaDetalleParam> listaDetalle) {
//        /**
//         * Acumulador para Monto Imponible 5
//         */
//        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto IVA 5
//         */
//        BigDecimal montoIva5Acumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto Total 5
//         */
//        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto Imponible 10
//         */
//        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto IVA 10
//         */
//        BigDecimal montoIva10Acumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto Total 10
//         */
//        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto Excento 0%
//         */
//        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
//        /**
//         * Acumulador para Monto Excento 0%
//         */
//        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
//        /**
//         * Acumulador para el total del IVA
//         */
//        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");
//
//        for (int i = 0; i < listaDetalle.size(); i++) {
//            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
//                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
//            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
//                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
//                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
//                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
//            } else {
//                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
//                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
//                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
//            }
//            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
//            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
//        }
//
//        fpNuevo.setMontoIva5(montoIva5Acumulador);
//        fpNuevo.setMontoImponible5(montoImponible5Acumulador);
//        fpNuevo.setMontoTotal5(montoTotal5Acumulador);
//        fpNuevo.setMontoIva10(montoIva10Acumulador);
//        fpNuevo.setMontoImponible10(montoImponible10Acumulador);
//        fpNuevo.setMontoTotal10(montoTotal10Acumulador);
//        fpNuevo.setMontoTotalExento(montoExcentoAcumulador);
//        fpNuevo.setMontoImponibleTotal(montoImponibleTotalAcumulador);
//        fpNuevo.setMontoIvaTotal(montoIvaTotalAcumulador);
//        fpNuevo.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
//        fpNuevo.setMontoTotalDescuentoParticular(new BigDecimal("0"));
//    }

    
    public AutoFactura find(Integer id) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(AutoFactura.class);
        Root<AutoFactura> root = cq.from(AutoFactura.class);
        
        cq.select(root);

        cq.where(qb.equal(root.get("id"), id));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(0)
                .setMaxResults(1)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<AutoFactura> result = q.getResultList();

        return result == null || result.isEmpty() ? null : result.get(0);
    }

}
