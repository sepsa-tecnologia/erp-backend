/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class DevolucionDetallePojo {

    public DevolucionDetallePojo(Integer id, Integer idDevolucion,
            Integer idMotivo, String codigoMotivo, String motivo,
            Integer idFacturaDetalle, Integer idEstadoInventario,
            String codigoEstadoInventario, String estadoInventario,
            Integer idDepositoLogistico, String depositoLogistico,
            Integer idProducto, String producto, String codigoGtin,
            String codigoInterno, Integer cantidadFacturada,
            Integer cantidadDevolucion, Date fechaVencimiento) {
        this.id = id;
        this.idDevolucion = idDevolucion;
        this.idMotivo = idMotivo;
        this.codigoMotivo = codigoMotivo;
        this.motivo = motivo;
        this.idFacturaDetalle = idFacturaDetalle;
        this.idEstadoInventario = idEstadoInventario;
        this.codigoEstadoInventario = codigoEstadoInventario;
        this.estadoInventario = estadoInventario;
        this.idDepositoLogistico = idDepositoLogistico;
        this.depositoLogistico = depositoLogistico;
        this.idProducto = idProducto;
        this.producto = producto;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.cantidadFacturada = cantidadFacturada;
        this.cantidadDevolucion = cantidadDevolucion;
        this.fechaVencimiento = fechaVencimiento;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de devolucion
     */
    private Integer idDevolucion;
    
    /**
     * Identificador de motivo
     */
    private Integer idMotivo;
    
    /**
     * Código de motivo
     */
    private String codigoMotivo;
    
    /**
     * Motivo
     */
    private String motivo;
    
    /**
     * Identificador de factura detalle
     */
    private Integer idFacturaDetalle;
    
    /**
     * Identificador de estado inventario
     */
    private Integer idEstadoInventario;
    
    /**
     * Código de estado inventario
     */
    private String codigoEstadoInventario;
    
    /**
     * Estado inventario
     */
    private String estadoInventario;
    
    /**
     * Identificador de deposito logistico
     */
    private Integer idDepositoLogistico;
    
    /**
     * Depósito logistico
     */
    private String depositoLogistico;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Código gtin
     */
    private String codigoGtin;
    
    /**
     * Código interno
     */
    private String codigoInterno;
    
    /**
     * Cantidad facturada
     */
    private Integer cantidadFacturada;
    
    /**
     * Cantidad devolucion
     */
    private Integer cantidadDevolucion;
    
    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDevolucion() {
        return idDevolucion;
    }

    public void setIdDevolucion(Integer idDevolucion) {
        this.idDevolucion = idDevolucion;
    }

    public Integer getIdMotivo() {
        return idMotivo;
    }

    public void setIdMotivo(Integer idMotivo) {
        this.idMotivo = idMotivo;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Integer getIdFacturaDetalle() {
        return idFacturaDetalle;
    }

    public void setIdFacturaDetalle(Integer idFacturaDetalle) {
        this.idFacturaDetalle = idFacturaDetalle;
    }

    public Integer getIdEstadoInventario() {
        return idEstadoInventario;
    }

    public void setIdEstadoInventario(Integer idEstadoInventario) {
        this.idEstadoInventario = idEstadoInventario;
    }

    public String getCodigoEstadoInventario() {
        return codigoEstadoInventario;
    }

    public void setCodigoEstadoInventario(String codigoEstadoInventario) {
        this.codigoEstadoInventario = codigoEstadoInventario;
    }

    public String getEstadoInventario() {
        return estadoInventario;
    }

    public void setEstadoInventario(String estadoInventario) {
        this.estadoInventario = estadoInventario;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public String getDepositoLogistico() {
        return depositoLogistico;
    }

    public void setDepositoLogistico(String depositoLogistico) {
        this.depositoLogistico = depositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getCantidadFacturada() {
        return cantidadFacturada;
    }

    public void setCantidadFacturada(Integer cantidadFacturada) {
        this.cantidadFacturada = cantidadFacturada;
    }

    public Integer getCantidadDevolucion() {
        return cantidadDevolucion;
    }

    public void setCantidadDevolucion(Integer cantidadDevolucion) {
        this.cantidadDevolucion = cantidadDevolucion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }
}
