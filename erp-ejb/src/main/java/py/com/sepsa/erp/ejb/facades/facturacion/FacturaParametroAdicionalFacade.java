/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaParametroAdicional;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParametroAdicionalParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Williams Vera
 */
@Local
public interface FacturaParametroAdicionalFacade extends Facade<FacturaParametroAdicional, FacturaParametroAdicionalParam, UserInfoImpl> {

    public Boolean validToCreate(FacturaParametroAdicionalParam param, boolean validarFactura);
}