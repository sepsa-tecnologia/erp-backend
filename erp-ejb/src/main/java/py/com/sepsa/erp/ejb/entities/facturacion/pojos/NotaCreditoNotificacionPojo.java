/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

/**
 *
 * @author Williams Vera
 */
public class NotaCreditoNotificacionPojo {

    public NotaCreditoNotificacionPojo(Integer id, Integer idNotaCredito,
            Integer idTipoNotificacion, String tipoNotificacion,
            String codigoTipoNotificacion, String email) {
        this.id = id;
        this.idNotaCredito = idNotaCredito;
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.codigoTipoNotificacion = codigoTipoNotificacion;
        this.email = email;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de idNotaCredito
     */
    private Integer idNotaCredito;
    
    /**
     * Identificador de tipo de notificación
     */
    private Integer idTipoNotificacion;
    
    /**
     * Tipo de notificación
     */
    private String tipoNotificacion;
    
    /**
     * Código de tipo de notificación
     */
    private String codigoTipoNotificacion;
    
    /**
     * Email
     */
    private String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdNotaCredito() {
        return idNotaCredito;
    }

    public void setIdNotaCredito(Integer idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
