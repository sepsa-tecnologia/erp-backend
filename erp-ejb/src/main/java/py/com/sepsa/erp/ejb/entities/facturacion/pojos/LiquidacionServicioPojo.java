/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

/**
 * POJO para la entidad liquidación servicio
 * @author Jonathan
 */
public class LiquidacionServicioPojo {
    
    /**
     * Identificador 
     */
    private Integer id;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Servicio
     */
    private String servicio;
    
    /**
     * Identificador de cliente;
     */
    private Integer idCliente;
    
    /**
     * Cliente
     */
    private String cliente;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Bloqueable
     */
    private Character bloqueable;
    
    /**
     * Activo
     */
    private Character activo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public Character getActivo() {
        return activo;
    }

    public void setBloqueable(Character bloqueable) {
        this.bloqueable = bloqueable;
    }

    public Character getBloqueable() {
        return bloqueable;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }
}
