/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.utils.misc.NumberToLetterConverter;

/**
 * POJO para la entidad factura
 *
 * @author Jonathan
 */
public class FacturaPojo implements Serializable {

    public FacturaPojo() {
    }

    public FacturaPojo(Integer id, Integer idEmpresa, String empresa,
            Integer idCliente, Integer idTalonario, Date fechaVencimientoTimbrado,
            Date fechaInicioVigenciaTimbrado, String timbrado, Integer idMoneda,
            String moneda, String codigoMoneda,
            Integer idTipoCambio, Integer idTipoFactura, String tipoFactura,
            String codigoTipoFactura, Integer idEstado, String estado,
            String codigoEstado, String nroFactura, String serie, Date fecha, Date fechaVencimiento,
            String razonSocial, String direccion, Integer idDepartamento,
            Integer idDistrito, Integer idCiudad, Integer nroCasa, String ruc,
            String telefono, String email, Character anulado, Character cobrado,
            Character impreso, Character entregado, Character digital,
            Character archivoEdi, Character archivoSet, Character generadoEdi,
            Character generadoSet, Character estadoSincronizado, Character compraPublica, Date fechaEntrega,
            Integer diasCredito, Integer cantidadCuotas, Integer idTipoOperacionCredito,
            String tipoOperacionCredito, String codigoTipoOperacionCredito,
            Integer idNaturalezaCliente, String observacion, Integer idOrdenCompra, String nroOrdenCompra,
            Date fechaRecepcion, BigDecimal montoIva5, BigDecimal montoImponible5,
            BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10,
            BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal,
            BigDecimal montoTotalDescuentoGlobal, BigDecimal montoTotalDescuentoParticular,
            BigDecimal montoImponibleTotal, BigDecimal montoTotalFactura,
            BigDecimal montoTotalGuaranies, String cdc, String codSeguridad,
            Integer idProcesamiento, BigDecimal saldo, String dncpModalidad,
            String dncpEntidad, String dncpAnho, String dncpSecuencia, Date dncpFechaEmision,
            Integer idTipoTransaccion, Integer idTipoOperacion, BigDecimal porcentajeDescuentoGlobal) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idCliente = idCliente;
        this.idTalonario = idTalonario;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
        this.timbrado = timbrado;
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.codigoMoneda = codigoMoneda;
        this.idTipoCambio = idTipoCambio;
        this.idTipoFactura = idTipoFactura;
        this.tipoFactura = tipoFactura;
        this.codigoTipoFactura = codigoTipoFactura;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.nroFactura = nroFactura;
        this.serie = serie;
        this.fecha = fecha;
        this.fechaVencimiento = fechaVencimiento;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.idDepartamento = idDepartamento;
        this.idDistrito = idDistrito;
        this.idCiudad = idCiudad;
        this.nroCasa = nroCasa;
        this.ruc = ruc;
        this.telefono = telefono;
        this.email = email;
        this.anulado = anulado;
        this.cobrado = cobrado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.digital = digital;
        this.archivoEdi = archivoEdi;
        this.archivoSet = archivoSet;
        this.generadoEdi = generadoEdi;
        this.generadoSet = generadoSet;
        this.estadoSincronizado = estadoSincronizado;
        this.compraPublica = compraPublica;
        this.fechaEntrega = fechaEntrega;
        this.diasCredito = diasCredito;
        this.cantidadCuotas = cantidadCuotas;
        this.idTipoOperacionCredito = idTipoOperacionCredito;
        this.tipoOperacionCredito = tipoOperacionCredito;
        this.codigoTipoOperacionCredito = codigoTipoOperacionCredito;
        this.idNaturalezaCliente = idNaturalezaCliente;
        this.observacion = observacion;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
        this.montoTotalDescuentoParticular = montoTotalDescuentoParticular;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalFactura = montoTotalFactura;
        this.montoTotalGuaranies = montoTotalGuaranies;
        this.idOrdenCompra = idOrdenCompra;
        this.nroOrdenCompra = nroOrdenCompra;
        this.fechaRecepcion = fechaRecepcion;
        this.cdc = cdc;
        this.codSeguridad = codSeguridad;
        this.idProcesamiento = idProcesamiento;
        this.saldo = saldo;
        this.dncpModalidad = dncpModalidad;
        this.dncpEntidad = dncpEntidad;
        this.dncpAnho = dncpAnho;
        this.dncpSecuencia = dncpSecuencia;
        this.dncpFechaEmision = dncpFechaEmision;
        this.idTipoTransaccion = idTipoTransaccion;
        this.idTipoOperacion = idTipoOperacion;
        this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
    }

    public FacturaPojo(Integer id, Integer idCliente, Integer idTalonario, String timbrado,
            Date fechaVencimientoTimbrado, Date fechaInicioVigenciaTimbrado, Integer idMoneda,
            String codigoMoneda, Integer idTipoFactura, String tipoFactura, String nroFactura,
            Date fecha, Date fechaVencimiento, String razonSocial, String direccion,
            String ruc, String telefono, Character anulado, Character cobrado, Character impreso, Character entregado,
            Character digital, Character compraPublica, Date fechaEntrega, Integer diasCredito, String observacion,
            BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5,
            BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10,
            BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal,
            BigDecimal montoTotalFactura, String cdc) {
        this.id = id;
        this.idCliente = idCliente;
        this.idTalonario = idTalonario;
        this.idMoneda = idMoneda;
        this.codigoMoneda = codigoMoneda;
        this.idTipoFactura = idTipoFactura;
        this.nroFactura = nroFactura;
        this.timbrado = timbrado;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
        this.fecha = fecha;
        this.fechaVencimiento = fechaVencimiento;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.ruc = ruc;
        this.telefono = telefono;
        this.anulado = anulado;
        this.cobrado = cobrado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.digital = digital;
        this.compraPublica = compraPublica;
        this.fechaEntrega = fechaEntrega;
        this.diasCredito = diasCredito;
        this.observacion = observacion;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalFactura = montoTotalFactura;
        this.cdc = cdc;
        completarTotalLetras();
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;

    /**
     * Empresa
     */
    private String empresa;

    /**
     * Identificador de cliente
     */
    private Integer idCliente;

    /**
     * Identificador de persona
     */
    private Integer idPersona;

    /**
     * Identificador de comercial
     */
    private Integer idComercial;

    /**
     * Comercial
     */
    private String comercial;

    /**
     * Identificador de talonario
     */
    private Integer idTalonario;

    /**
     * Fecha de vencimiento de timbrado
     */
    private Date fechaVencimientoTimbrado;

    /**
     * Fecha de inicio de timbrado
     */
    private Date fechaInicioVigenciaTimbrado;

    /**
     * Timbrado
     */
    private String timbrado;

    /**
     * Identificador de moneda
     */
    private Integer idMoneda;

    /**
     * Moneda
     */
    private String moneda;

    /**
     * Código Moneda
     */
    private String codigoMoneda;

    /**
     * Identificador de tipo de cambio
     */
    private Integer idTipoCambio;

    /**
     * Tipo cambio compra
     */
    private BigDecimal tipoCambioCompra;

    /**
     * Tipo cambio venta
     */
    private BigDecimal tipoCambioVenta;

    /**
     * Identificador de tipo de factura
     */
    private Integer idTipoFactura;

    /**
     * Tipo factura
     */
    private String tipoFactura;

    /**
     * Código de tipo factura
     */
    private String codigoTipoFactura;

    /**
     * Identificador de estado
     */
    private Integer idEstado;

    /**
     * Estado
     */
    private String estado;

    /**
     * Código de estado
     */
    private String codigoEstado;

    /**
     * Establecimiento
     */
    private String establecimiento;

    /**
     * Punto de expedición
     */
    private String puntoExpedicion;

    /**
     * Número de documento
     */
    private String nroDocumento;

    /**
     * Nro de factura
     */
    private String nroFactura;

    /**
     * Serie
     */
    private String serie;

    /**
     * Local talonario
     */
    private String descripcionLocalTalonario;

    /**
     * Fecha
     */
    private Date fecha;

    /**
     * Fecha de vencimiento
     */
    private Date fechaVencimiento;

    /**
     * Razon social
     */
    private String razonSocial;

    /**
     * Dirección
     */
    private String direccion;

    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;

    /**
     * Identificador de distrito
     */
    private Integer idDistrito;

    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;

    /**
     * Nro de casa
     */
    private Integer nroCasa;

    /**
     * RUC
     */
    private String ruc;

    /**
     * Telefono
     */
    private String telefono;

    /**
     * Email
     */
    private String email;

    /**
     * Anulado
     */
    private Character anulado;

    /**
     * Compra pública
     */
    private Character compraPublica;

    /**
     * Cobrado
     */
    private Character cobrado;

    /**
     * Pagado
     */
    private Character pagado;

    /**
     * Impreso
     */
    private Character impreso;

    /**
     * Entregado
     */
    private Character entregado;

    /**
     * Digital
     */
    private Character digital;

    /**
     * Archivo EDI
     */
    private Character archivoEdi;

    /**
     * Archivo SET
     */
    private Character archivoSet;

    /**
     * Generado EDI
     */
    private Character generadoEdi;

    /**
     * Generado SET
     */
    private Character generadoSet;

    /**
     * Estado sincronizado
     */
    private Character estadoSincronizado;

    /**
     * Fecha de entrega
     */
    private Date fechaEntrega;

    /**
     * Dias de credito
     */
    private Integer diasCredito;

    /**
     * Cantidad de cuotas
     */
    private Integer cantidadCuotas;

    /**
     * Identificador de tipo de operación credito
     */
    private Integer idTipoOperacionCredito;

    /**
     * Tipo operación crédito
     */
    private String tipoOperacionCredito;

    /**
     * Código tipo operación crédito
     */
    private String codigoTipoOperacionCredito;

    /**
     * Observación
     */
    private String observacion;

    /**
     * Monto iva 5
     */
    private BigDecimal montoIva5;

    /**
     * Monto imponible 5
     */
    private BigDecimal montoImponible5;

    /**
     * Monto total 5
     */
    private BigDecimal montoTotal5;

    /**
     * Monto iva 10
     */
    private BigDecimal montoIva10;

    /**
     * Monto imponible 10
     */
    private BigDecimal montoImponible10;

    /**
     * Monto total 10
     */
    private BigDecimal montoTotal10;

    /**
     * Monto total exento
     */
    private BigDecimal montoTotalExento;

    /**
     * Monto iva total
     */
    private BigDecimal montoIvaTotal;

    /**
     * Monto total descuento particular
     */
    private BigDecimal montoTotalDescuentoParticular;

    /**
     * Monto total descuento global
     */
    private BigDecimal montoTotalDescuentoGlobal;

    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;

    /**
     * Monto total de factura
     */
    private BigDecimal montoTotalFactura;

    /**
     * Monto total en guaranies
     */
    private BigDecimal montoTotalGuaranies;

    /**
     * CDC
     */
    private String cdc;

    /**
     * Código de seguridad
     */
    private String codSeguridad;

    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;

    /**
     * Identificador de naturaleza del cliente
     */
    private Integer idNaturalezaCliente;

    /**
     * Monto nota de crédito
     */
    private BigDecimal montoNotaCredito;

    /**
     * Monto cobro
     */
    private BigDecimal montoCobro;

    /**
     * Monto retencion
     */
    private BigDecimal montoRetencion;

    /**
     * Saldo
     */
    private BigDecimal saldo;

    /**
     * Modalidad
     */
    private String dncpModalidad;

    /**
     * Entidad
     */
    private String dncpEntidad;

    /**
     * Año
     */
    private String dncpAnho;

    /**
     * Secuencia
     */
    private String dncpSecuencia;

    /**
     * Fecha emision
     */
    private Date dncpFechaEmision;

    /**
     * Cantidad
     */
    private Integer cantidad;

    /**
     * Total letras
     */
    private String totalLetras;

    /**
     * Identificador de orden de compra
     */
    private Integer idOrdenCompra;

    /**
     * Nro de orden de compra
     */
    private String nroOrdenCompra;

    /**
     * Fecha de recepción
     */
    private Date fechaRecepcion;

    /**
     * Detalle
     */
    private List<FacturaDetallePojo> detalles;

    /**
     * Identificador de tipoTransaccion;
     */
    private Integer idTipoTransaccion;

    /**
     * Identificador de tipoOperacion;
     */
    private Integer idTipoOperacion;
    /**
     * Porcentaje del descuento
     */
    private BigDecimal porcentajeDescuentoGlobal;

    /**
     * Identificador del Vendedor;
     */
    private Integer idVendedor;

    /**
     *  Nombre Completo del Vendedor
     */
    private String nombreVendedor;

    /**
     * Completa el campo total letras
     */
    public final void completarTotalLetras() {
        try {
            this.totalLetras = NumberToLetterConverter.convertNumberToLetter(
                    montoTotalFactura, NumberToLetterConverter.Moneda.valueOf(codigoMoneda));
        } catch (Exception e) {
            this.totalLetras = "---";
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getCobrado() {
        return cobrado;
    }

    public void setCobrado(Character cobrado) {
        this.cobrado = cobrado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public BigDecimal getMontoNotaCredito() {
        return montoNotaCredito;
    }

    public void setMontoNotaCredito(BigDecimal montoNotaCredito) {
        this.montoNotaCredito = montoNotaCredito;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public BigDecimal getMontoRetencion() {
        return montoRetencion;
    }

    public void setMontoRetencion(BigDecimal montoRetencion) {
        this.montoRetencion = montoRetencion;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Integer getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(Integer diasCredito) {
        this.diasCredito = diasCredito;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setDetalles(List<FacturaDetallePojo> detalles) {
        this.detalles = detalles;
    }

    public List<FacturaDetallePojo> getDetalles() {
        return detalles;
    }

    public void setTotalLetras(String totalLetras) {
        this.totalLetras = totalLetras;
    }

    public String getTotalLetras() {
        return totalLetras;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setComercial(String comercial) {
        this.comercial = comercial;
    }

    public String getComercial() {
        return comercial;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setPagado(Character pagado) {
        this.pagado = pagado;
    }

    public Character getPagado() {
        return pagado;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setNroOrdenCompra(String nroOrdenCompra) {
        this.nroOrdenCompra = nroOrdenCompra;
    }

    public String getNroOrdenCompra() {
        return nroOrdenCompra;
    }

    public void setDescripcionLocalTalonario(String descripcionLocalTalonario) {
        this.descripcionLocalTalonario = descripcionLocalTalonario;
    }

    public String getDescripcionLocalTalonario() {
        return descripcionLocalTalonario;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCodigoTipoFactura(String codigoTipoFactura) {
        this.codigoTipoFactura = codigoTipoFactura;
    }

    public String getCodigoTipoFactura() {
        return codigoTipoFactura;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public void setTipoCambioVenta(Number tipoCambioVenta) {
        this.tipoCambioVenta = tipoCambioVenta == null
                ? null
                : new BigDecimal(tipoCambioVenta + "");
    }

    public void setTipoCambioCompra(Number tipoCambioCompra) {
        this.tipoCambioCompra = tipoCambioCompra == null
                ? null
                : new BigDecimal(tipoCambioCompra + "");
    }

    public BigDecimal getTipoCambioVenta() {
        return tipoCambioVenta;
    }

    public BigDecimal getTipoCambioCompra() {
        return tipoCambioCompra;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Integer getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(Integer cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public Integer getIdTipoOperacionCredito() {
        return idTipoOperacionCredito;
    }

    public void setIdTipoOperacionCredito(Integer idTipoOperacionCredito) {
        this.idTipoOperacionCredito = idTipoOperacionCredito;
    }

    public String getTipoOperacionCredito() {
        return tipoOperacionCredito;
    }

    public void setTipoOperacionCredito(String tipoOperacionCredito) {
        this.tipoOperacionCredito = tipoOperacionCredito;
    }

    public String getCodigoTipoOperacionCredito() {
        return codigoTipoOperacionCredito;
    }

    public void setCodigoTipoOperacionCredito(String codigoTipoOperacionCredito) {
        this.codigoTipoOperacionCredito = codigoTipoOperacionCredito;
    }

    public BigDecimal getMontoTotalDescuentoParticular() {
        return montoTotalDescuentoParticular;
    }

    public void setMontoTotalDescuentoParticular(BigDecimal montoTotalDescuentoParticular) {
        this.montoTotalDescuentoParticular = montoTotalDescuentoParticular;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public String getTipoFactura() {
        return tipoFactura;
    }

    public void setTipoFactura(String tipoFactura) {
        this.tipoFactura = tipoFactura;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public void setFechaInicioVigenciaTimbrado(Date fechaInicioVigenciaTimbrado) {
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
    }

    public Date getFechaInicioVigenciaTimbrado() {
        return fechaInicioVigenciaTimbrado;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setCompraPublica(Character compraPublica) {
        this.compraPublica = compraPublica;
    }

    public Character getCompraPublica() {
        return compraPublica;
    }

    public void setDncpSecuencia(String dncpSecuencia) {
        this.dncpSecuencia = dncpSecuencia;
    }

    public String getDncpSecuencia() {
        return dncpSecuencia;
    }

    public void setDncpModalidad(String dncpModalidad) {
        this.dncpModalidad = dncpModalidad;
    }

    public String getDncpModalidad() {
        return dncpModalidad;
    }

    public void setDncpFechaEmision(Date dncpFechaEmision) {
        this.dncpFechaEmision = dncpFechaEmision;
    }

    public Date getDncpFechaEmision() {
        return dncpFechaEmision;
    }

    public void setDncpEntidad(String dncpEntidad) {
        this.dncpEntidad = dncpEntidad;
    }

    public String getDncpEntidad() {
        return dncpEntidad;
    }

    public void setDncpAnho(String dncpAnho) {
        this.dncpAnho = dncpAnho;
    }

    public String getDncpAnho() {
        return dncpAnho;
    }

    public Integer getIdTipoTransaccion() {
        return idTipoTransaccion;
    }

    public void setIdTipoTransaccion(Integer idTipoTransaccion) {
        this.idTipoTransaccion = idTipoTransaccion;
    }

    public Integer getIdTipoOperacion() {
        return idTipoOperacion;
    }

    public void setIdTipoOperacion(Integer idTipoOperacion) {
        this.idTipoOperacion = idTipoOperacion;
    }

    public BigDecimal getPorcentajeDescuentoGlobal() {
        return porcentajeDescuentoGlobal;
    }

    public void setPorcentajeDescuentoGlobal(BigDecimal porcentajeDescuentoGlobal) {
        this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    } 
}
