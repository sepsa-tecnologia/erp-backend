/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de empresa
 * @author Jonathan
 */
public class PlantillaCreacionEmpresaParam extends CommonParam {
    
    public PlantillaCreacionEmpresaParam() {
    }

    public PlantillaCreacionEmpresaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Fecha de inseción
     */
    @QueryParam("fechaInsercion")
    private String fechaInsercion;
    
    /**
     * Menu
     */
    private List<MenuEmpresaParam> menu;
    
    /**
     * Configuracion Valor
     */
    private List<ConfiguracionValorParam> configuracionValor;
    
    /**
     * Perfil
     */
    private List<PerfilParam> perfil;
    
    /**
     * Motivo de Emision NC
     */
    private List<MotivoEmisionNcParam> motivoEmisionNc;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        

        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción de la empresa"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código de la empresa"));
        }
        
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }

        return !tieneErrores();
    }
    

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public String getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public List<MenuEmpresaParam> getMenu() {
        return menu;
    }

    public void setMenu(List<MenuEmpresaParam> menu) {
        this.menu = menu;
    }

    public List<ConfiguracionValorParam> getConfiguracionValor() {
        return configuracionValor;
    }

    public void setConfiguracionValor(List<ConfiguracionValorParam> configuracionValor) {
        this.configuracionValor = configuracionValor;
    }

    public List<PerfilParam> getPerfil() {
        return perfil;
    }

    public void setPerfil(List<PerfilParam> perfil) {
        this.perfil = perfil;
    }

    public List<MotivoEmisionNcParam> getMotivoEmisionNc() {
        return motivoEmisionNc;
    }

    public void setMotivoEmisionNc(List<MotivoEmisionNcParam> motivoEmisionNc) {
        this.motivoEmisionNc = motivoEmisionNc;
    }
    
}
