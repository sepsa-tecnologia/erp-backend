/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.util.Date;
import java.util.List;

/**
 * POJO para la entidad factura
 * @author Jonathan
 */
public class NotaRemisionPojo {

    public NotaRemisionPojo() {
    }

    public NotaRemisionPojo(Integer id, Integer idCliente, Integer idTalonario, String timbrado, Date fechaVencimientoTimbrado, 
             String nroNotaRemision, String razonSocial, String direccion, String ruc, Character anulado, Date fechaEntrega, String observacion, String cdc) {
        this.id = id;
        this.idCliente = idCliente;
        this.idTalonario = idTalonario;
        this.nroNotaRemision = nroNotaRemision;
        this.timbrado = timbrado;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.ruc = ruc;
        this.anulado = anulado;
        this.fechaEntrega = fechaEntrega;
        this.observacion = observacion;
        this.cdc = cdc;
    }

    public NotaRemisionPojo(Integer id,Integer idCliente, String nroNotaRemision,
            Integer idTalonario, Date fechaVencimientoTimbrado,
            Date fechaInicioVigenciaTimbrado, String timbrado, String serie,
            String descripcionLocalTalonario, Date fecha, String razonSocial,
            String direccion, String nroCasa, Integer idDepartamento, Integer idDistrito,
            Integer idCiudad,String ruc ,String observacion, 
            Character anulado, Character digital, Character archivoSet ,Character generadoSet, Character estadoSincronizado, 
            Integer idProcesamiento,String cdc, String codSeguridad, Integer idNaturalezaCliente,
            Integer idMotivoEmisionNr, String motivoEmisionNr, String codigoMotivoEmisionNr,Integer idEstado ,String codigoEstado) {
        this.id = id;
        this.idCliente = idCliente;
        this.nroNotaRemision = nroNotaRemision;
        this.idTalonario = idTalonario;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
        this.timbrado = timbrado;
        this.serie = serie;
        this.descripcionLocalTalonario = descripcionLocalTalonario;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.nroCasa = nroCasa;
        this.idDepartamento = idDepartamento;
        this.idDistrito = idDistrito;
        this.idCiudad = idCiudad;
        this.ruc = ruc;
        this.observacion = observacion;
        this.anulado = anulado;
        this.digital = digital;
        this.archivoSet = archivoSet;
        this.generadoSet = generadoSet;
        this.estadoSincronizado = estadoSincronizado;
        this.idProcesamiento = idProcesamiento;
        this.cdc = cdc;
        this.codSeguridad = codSeguridad;
        this.idNaturalezaCliente = idNaturalezaCliente;
        this.idMotivoEmisionNr = idMotivoEmisionNr;
        this.motivoEmisionNr = motivoEmisionNr;
        this.codigoMotivoEmisionNr = codigoMotivoEmisionNr;
        this.idEstado = idEstado;
        this.codigoEstado = codigoEstado;

    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    
    /**
     * Nro de nota de crédito
     */
    private String nroNotaRemision;
    
    /**
     * Fecha de inicio de vigencia de timbrado
     */
    private Date fechaInicioVigenciaTimbrado;
    
    /**
     * Identificador de motivo de emisión
     */
    private Integer idMotivoEmisionNr;
    
    /**
     * Motivo de emisión
     */
    private String motivoEmisionNr;
    
    /**
     * Código del motivo de emisión
     */
    private String codigoMotivoEmisionNr;
    
    /**
     * Timbrado
     */
    private String timbrado;
    
    /**
     * Serie
     */
    private String serie;
    
    /**
     * Local talonario
     */
    private String descripcionLocalTalonario;
    
    /**
     * Fecha de vencimiento de timbrado
     */
    private Date fechaVencimientoTimbrado;
    
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * Razon social
     */
    private String razonSocial;
    
    /**
     * Dirección
     */
    private String direccion;
    
    /**
     * Nro de casa
     */
    private String nroCasa;
    
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    
    /**
     * RUC
     */
    private String ruc;
    
    
    /**
     * Anulado
     */
    private Character anulado;
    
    /**
     * Impreso
     */
    private Character impreso;
    
    /**
     * Entregado
     */
    private Character entregado;
    
    /**
     * Digital
     */
    private Character digital;
    
    /**
     * Generado SET
     */
    private Character generadoSet;

    
    private Character archivoSet;
    
    /**
     * Estado sincronizado
     */
    private Character estadoSincronizado;
    
    /**
     * Fecha de entrega
     */
    private Date fechaEntrega;
    
    /**
     * Observación
     */
    private String observacion;
    
    /**
     * CDC
     */
    private String cdc;
    
    /**
     * Código de seguridad
     */
    private String codSeguridad;
    
    /**
     * Identificador de naturaleza de cliente
     */
    private Integer idNaturalezaCliente;
    
    /**
     * Detalles
     */
    private List<NotaRemisionDetallePojo> detalles;
    
    /**
     * Nros de factura afectadas
     */
    private String nroFacturas;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Codigo de estado
     */
    private String codigoEstado;
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDetalles(List<NotaRemisionDetallePojo> detalles) {
        this.detalles = detalles;
    }

    public List<NotaRemisionDetallePojo> getDetalles() {
        return detalles;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getNroNotaRemision() {
        return nroNotaRemision;
    }

    public void setNroNotaRemision(String nroNotaRemision) {
        this.nroNotaRemision = nroNotaRemision;
    }



    public void setDescripcionLocalTalonario(String descripcionLocalTalonario) {
        this.descripcionLocalTalonario = descripcionLocalTalonario;
    }

    public String getDescripcionLocalTalonario() {
        return descripcionLocalTalonario;
    }

    public String getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(String nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setNroFacturas(String nroFacturas) {
        this.nroFacturas = nroFacturas;
    }

    public String getNroFacturas() {
        return nroFacturas;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setFechaInicioVigenciaTimbrado(Date fechaInicioVigenciaTimbrado) {
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
    }

    public Date getFechaInicioVigenciaTimbrado() {
        return fechaInicioVigenciaTimbrado;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public Integer getIdMotivoEmisionNr() {
        return idMotivoEmisionNr;
    }

    public void setIdMotivoEmisionNr(Integer idMotivoEmisionNr) {
        this.idMotivoEmisionNr = idMotivoEmisionNr;
    }

    public String getMotivoEmisionNr() {
        return motivoEmisionNr;
    }

    public void setMotivoEmisionNr(String motivoEmisionNr) {
        this.motivoEmisionNr = motivoEmisionNr;
    }

    public String getCodigoMotivoEmisionNr() {
        return codigoMotivoEmisionNr;
    }

    public void setCodigoMotivoEmisionNr(String codigoMotivoEmisionNr) {
        this.codigoMotivoEmisionNr = codigoMotivoEmisionNr;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }
    
    
    
}
