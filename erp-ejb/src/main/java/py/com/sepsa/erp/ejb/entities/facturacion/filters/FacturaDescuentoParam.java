/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.DescuentoParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de transaccion red
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaDescuentoParam extends CommonParam {
    
    public FacturaDescuentoParam() {
    }

    public FacturaDescuentoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Identificador de descuento
     */
    @QueryParam("idDescuento")
    private Integer idDescuento;
    
    /**
     * Descuento
     */
    private DescuentoParam descuento;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }
        
        if(isNull(idDescuento) && isNull(descuento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el descuento"));
        }
        
        if(!isNull(descuento)) {
            descuento.setIdEmpresa(idEmpresa);
            descuento.isValidToCreate();
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(descuento)) {
            list.addAll(descuento.getErrores());
        }
        
        return list;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdDescuento() {
        return idDescuento;
    }

    public void setIdDescuento(Integer idDescuento) {
        this.idDescuento = idDescuento;
    }

    public DescuentoParam getDescuento() {
        return descuento;
    }

    public void setDescuento(DescuentoParam descuento) {
        this.descuento = descuento;
    }
}
