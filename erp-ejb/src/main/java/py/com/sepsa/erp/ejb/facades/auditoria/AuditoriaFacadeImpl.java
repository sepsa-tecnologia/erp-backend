/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.auditoria.Auditoria;
import py.com.sepsa.erp.ejb.entities.auditoria.TipoAuditoria;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.AuditoriaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "AuditoriaFacade", mappedName = "AuditoriaFacade")
@Local(AuditoriaFacade.class)
public class AuditoriaFacadeImpl extends FacadeImpl<Auditoria, AuditoriaParam> implements AuditoriaFacade {

    public AuditoriaFacadeImpl() {
        super(Auditoria.class);
    }

    public Boolean validToCreate(AuditoriaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoAuditoria tipoAuditoria = facades.getTipoAuditoriaFacade()
                .find(param.getIdTipoAuditoria(), param.getCodigoTipoAuditoria());
        
        if(tipoAuditoria == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de auditoria"));
        } else {
            param.setIdTipoAuditoria(tipoAuditoria.getId());
            param.setCodigoTipoAuditoria(tipoAuditoria.getCodigo());
        }
        
        if(param.getAuditoriaDetalle() != null) {
            facades.getAuditoriaDetalleFacade().validToCreate(param.getAuditoriaDetalle(), Boolean.FALSE);
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Auditoria create(AuditoriaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Auditoria result = new Auditoria();
        result.setIdTipoAuditoria(param.getIdTipoAuditoria());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        create(result);
        
        if(param.getAuditoriaDetalle() != null) {
            param.getAuditoriaDetalle().setIdAuditoria(result.getId());
            facades.getAuditoriaDetalleFacade().create(param.getAuditoriaDetalle(), userInfo);
        }
        
        return result;
    }
    
    @Override
    public Auditoria nuevaInstancia(UserInfoImpl userInfo) {
        AuditoriaParam param = new AuditoriaParam();
        param.setCodigoTipoAuditoria("REGISTRO_BD");
        return create(param, userInfo);
    }
    
    @Override
    public List<Auditoria> find(AuditoriaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Auditoria.class);
        Root<Auditoria> root = cq.from(Auditoria.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdTipoAuditoria() != null) {
            predList.add(qb.equal(root.get("idTipoAuditoria"), param.getIdTipoAuditoria()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<Auditoria> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(AuditoriaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Auditoria> root = cq.from(Auditoria.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdTipoAuditoria() != null) {
            predList.add(qb.equal(root.get("idTipoAuditoria"), param.getIdTipoAuditoria()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
