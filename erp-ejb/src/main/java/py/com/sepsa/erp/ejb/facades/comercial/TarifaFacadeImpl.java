/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.Tarifa;
import py.com.sepsa.erp.ejb.entities.comercial.TipoTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MontoTarifaParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.TarifaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "TarifaFacade", mappedName = "TarifaFacade")
@Local(TarifaFacade.class)
public class TarifaFacadeImpl extends FacadeImpl<Tarifa, TarifaParam> implements TarifaFacade {

    public TarifaFacadeImpl() {
        super(Tarifa.class);
    }

    @Override
    public Map<String, String> getMapConditions(Tarifa item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("id_empresa", item.getIdEmpresa() + "");
        map.put("id_tipo_tarifa", item.getIdTipoTarifa() + "");
        map.put("id_servicio", item.getIdServicio()+ "");
        map.put("id_moneda", item.getIdMoneda()+ "");
        map.put("activo", item.getActivo() + "");
        map.put("codigo", item.getCodigo() + "");
        map.put("descripcion", item.getDescripcion() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(Tarifa item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToCreate(TarifaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        }
        
        Moneda moneda = facades
                .getMonedaFacade()
                .find(param.getIdMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        }
        
        TipoTarifa tipoTarifa = facades
                .getTipoTarifaFacade()
                .find(param.getIdTipoTarifa());
        
        if(tipoTarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de tarifa"));
        }
        
        if(param.getMontoTarifas() == null || param.getMontoTarifas().isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar la lista de monto tarifas"));
        } else {
            for (MontoTarifaParam montoTarifa : param.getMontoTarifas()) {
                facades.getMontoTarifaFacade().validToCreate(montoTarifa, false);
            }
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param Parámetros
     * @return Bandera
     */
    public Boolean validToEdit(TarifaParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Tarifa tarifa = facades
                .getTarifaFacade()
                .find(param.getId());
        
        if(tarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la tarifa"));
        }
        
        Servicio servicio = facades
                .getServicioFacade()
                .find(param.getIdServicio());
        
        if(servicio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el servicio"));
        }
        
        Moneda moneda = facades
                .getMonedaFacade()
                .find(param.getIdMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        }
        
        TipoTarifa tipoTarifa = facades
                .getTipoTarifaFacade()
                .find(param.getIdTipoTarifa());
        
        if(tipoTarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de tarifa"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Tarifa create(TarifaParam param, UserInfoImpl userInfo) {
        if(!validToCreate(param)) {
            return null;
        }
        
        Tarifa tarifa = new Tarifa();
        tarifa.setIdEmpresa(userInfo.getIdEmpresa());
        tarifa.setCodigo(param.getCodigo());
        tarifa.setDescripcion(param.getDescripcion());
        tarifa.setActivo(param.getActivo());
        tarifa.setIdMoneda(param.getIdMoneda());
        tarifa.setIdServicio(param.getIdServicio());
        tarifa.setIdTipoTarifa(param.getIdTipoTarifa());
        
        create(tarifa);
        
        if(param.getMontoTarifas() != null) {
            for (MontoTarifaParam montoTarifa : param.getMontoTarifas()) {
                montoTarifa.setIdTarifa(tarifa.getId());
                facades.getMontoTarifaFacade().create(montoTarifa, userInfo);
            }
        }
        
        return tarifa;
    }
    
    /**
     * Edita una instancia de Tarifa
     * @param param Parámetros
     * @return Tarifa
     */
    public Tarifa edit(TarifaParam param) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Tarifa tarifa = find(param.getId());
        tarifa.setCodigo(param.getCodigo());
        tarifa.setDescripcion(param.getDescripcion());
        tarifa.setActivo(param.getActivo());
        tarifa.setIdMoneda(param.getIdMoneda());
        tarifa.setIdServicio(param.getIdServicio());
        tarifa.setIdTipoTarifa(param.getIdTipoTarifa());
        
        edit(tarifa);
        
        return tarifa;
    }
    
    @Override
    public List<Tarifa> find(TarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Tarifa.class);
        Root<Tarifa> root = cq.from(Tarifa.class);
        Join<Tarifa, Servicio> s = root.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");
        Join<Tarifa, TipoTarifa> tt = root.join("tipoTarifa");
        Join<Tarifa, Moneda> m = root.join("moneda");
        
        cq.select(root);
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(m.get("id"), param.getIdMoneda()));
        }
        
        if (param.getIdTipoTarifa() != null) {
            predList.add(qb.equal(tt.get("id"), param.getIdTipoTarifa()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.asc(root.get("estado")),
                qb.desc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<Tarifa> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(TarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Tarifa> root = cq.from(Tarifa.class);
        Join<Tarifa, Servicio> s = root.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");
        Join<Tarifa, TipoTarifa> tt = root.join("tipoTarifa");
        Join<Tarifa, Moneda> m = root.join("moneda");
        
        cq.select(qb.count(root.get("id")));
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(m.get("id"), param.getIdMoneda()));
        }
        
        if (param.getIdTipoTarifa() != null) {
            predList.add(qb.equal(tt.get("id"), param.getIdTipoTarifa()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
