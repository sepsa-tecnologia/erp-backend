/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.PersonaEmail;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface PersonaEmailFacade extends Facade<PersonaEmail, PersonaEmailParam, UserInfoImpl> {

    public boolean validToEdit(PersonaEmailParam param);

    public boolean validToCreate(PersonaEmailParam param, boolean validarIdPersona);
    
    public PersonaEmail obtenerEmailCliente(Integer idCliente);
    
}
