/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.ContratoServicioTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ContratoServicioTarifaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ContratoServicioTarifaFacade extends Facade<ContratoServicioTarifa, ContratoServicioTarifaParam, UserInfoImpl> {

    public Boolean validToCreate(ContratoServicioTarifaParam param, boolean validadContratoServicio);

    public ContratoServicioTarifa find(Integer idContrato, Integer idServicio, Integer idTarifa);

    public Boolean validToEdit(ContratoServicioTarifaParam param);

    public ContratoServicioTarifa renovar(Integer idContratoActual, Integer idContratoNuevo, Integer idServicio, Integer idTarifa);
    
}
