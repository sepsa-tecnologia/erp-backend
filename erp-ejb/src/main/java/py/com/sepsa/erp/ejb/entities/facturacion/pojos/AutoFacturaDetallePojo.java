/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;

/**
 * POJO para la entidad factura detalle
 * @author Jonathan
 */
public class AutoFacturaDetallePojo {

    public AutoFacturaDetallePojo(Integer id, Integer idAutofactura, Integer nroLinea,
            String descripcion, String codDncpNivelGeneral,
            String codDncpNivelEspecifico,BigDecimal montoTotal,BigDecimal cantidad,
            BigDecimal precioUnitario, BigDecimal montoDescuentoParticular,
            BigDecimal montoDescuentoGlobal, BigDecimal descuentoParticularUnitario,
            BigDecimal descuentoGlobalUnitario) {
        this.id = id;
        this.idAutofactura = idAutofactura;
        this.nroLinea = nroLinea;
        this.descripcion = descripcion;
        this.codDncpNivelGeneral = codDncpNivelGeneral;
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
        this.montoTotal = montoTotal;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.montoDescuentoParticular = montoDescuentoParticular;
        this.montoDescuentoGlobal = montoDescuentoGlobal;
        this.descuentoParticularUnitario = descuentoParticularUnitario;
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;

    }
   
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de factura
     */
    private Integer idAutofactura;
    
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código DNCP de nivel general
     */
    private String codDncpNivelGeneral;
    
    /**
     * Código DNCP de nivel específico
     */
    private String codDncpNivelEspecifico;
    
    /**
     * Porcentaje de iva
     */
    private Integer porcentajeIva;
    
    /**
     * Monto iva
     */
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Identificador de liquidación
     */
    private Integer idLiquidacion;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
   
    
    /**
     * Metrica
     */
    private String metrica;
    
    /**
     * Código de métrica
     */
    private String codigoMetrica;
    
    /**
     * Cantidad
     */
    private BigDecimal cantidad;

    /**
     * Precio unitario con IVA
     */
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin IVA
     */
    private BigDecimal precioUnitarioSinIva;

    /**
     * Monto descuento particular
     */
    private BigDecimal montoDescuentoParticular;

    /**
     * Monto descuento global
     */
    private BigDecimal montoDescuentoGlobal;

    /**
     * Descuento particular unitario
     */
    private BigDecimal descuentoParticularUnitario;

    /**
     * Descuento global unitario
     */
    private BigDecimal descuentoGlobalUnitario;

    /**
     * Porcentaje gravada
     */
    private Integer porcentajeGravada;

    /**
     * Monto exento gravado
     */
    private BigDecimal montoExentoGravado;
    /**
     * Monto precio unitario
     */
    private BigDecimal precioUnitario;

    public Integer getIdAutofactura() {
        return idAutofactura;
    }

    public void setIdAutofactura(Integer idAutofactura) {
        this.idAutofactura = idAutofactura;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }
    
    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public Integer getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(Integer porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public void setMetrica(String metrica) {
        this.metrica = metrica;
    }

    public String getMetrica() {
        return metrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }
}
