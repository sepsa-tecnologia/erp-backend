/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Vendedor;
import py.com.sepsa.erp.ejb.entities.comercial.filters.VendedorParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Gustavo Benítez
 */
@Local
public interface VendedorFacade extends Facade<Vendedor, VendedorParam, UserInfoImpl> {

    public Boolean validToCreate(VendedorParam param, Boolean validarPersona);

}
