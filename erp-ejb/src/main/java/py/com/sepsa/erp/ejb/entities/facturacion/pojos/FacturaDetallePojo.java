/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;

/**
 * POJO para la entidad factura detalle
 * @author Jonathan
 */
public class FacturaDetallePojo {

    public FacturaDetallePojo(Integer id, Integer idFactura, Integer nroLinea,
            String descripcion, String codDncpNivelGeneral,
            String codDncpNivelEspecifico, Integer porcentajeIva, BigDecimal montoIva,
            BigDecimal montoImponible, BigDecimal montoTotal, Integer idLiquidacion,
            Integer idServicio, Integer idProducto, String producto,
            String codigoGtin, String codigoInterno, Date fechaVencimientoLote,
            String nroLote, String codigoMetrica,
            String metrica, BigDecimal cantidad, BigDecimal precioUnitarioConIva,
            BigDecimal precioUnitarioSinIva, BigDecimal montoDescuentoParticular,
            BigDecimal montoDescuentoGlobal, BigDecimal descuentoParticularUnitario,
            BigDecimal descuentoGlobalUnitario, BigDecimal porcentajeGravada,
            BigDecimal montoExentoGravado, String datoAdicional) {
        this.id = id;
        this.idFactura = idFactura;
        this.nroLinea = nroLinea;
        this.descripcion = descripcion;
        this.codDncpNivelGeneral = codDncpNivelGeneral;
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
        this.porcentajeIva = porcentajeIva;
        this.montoIva = montoIva;
        this.montoImponible = montoImponible;
        this.montoTotal = montoTotal;
        this.idLiquidacion = idLiquidacion;
        this.idServicio = idServicio;
        this.idProducto = idProducto;
        this.producto = producto;
        this.codigoGtin = codigoGtin;
        this.codigoInterno = codigoInterno;
        this.fechaVencimientoLote = fechaVencimientoLote;
        this.nroLote = nroLote;
        this.codigoMetrica = codigoMetrica;
        this.metrica = metrica;
        this.cantidad = cantidad;
        this.precioUnitarioConIva = precioUnitarioConIva;
        this.precioUnitarioSinIva = precioUnitarioSinIva;
        this.montoDescuentoParticular = montoDescuentoParticular;
        this.montoDescuentoGlobal = montoDescuentoGlobal;
        this.descuentoParticularUnitario = descuentoParticularUnitario;
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
        this.porcentajeGravada = porcentajeGravada;
        this.montoExentoGravado = montoExentoGravado;
        this.datoAdicional = datoAdicional;
    }
   
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Nro de linea
     */
    private Integer nroLinea;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Código DNCP de nivel general
     */
    private String codDncpNivelGeneral;
    
    /**
     * Código DNCP de nivel específico
     */
    private String codDncpNivelEspecifico;
    
    /**
     * Porcentaje de iva
     */
    private Integer porcentajeIva;
    
    /**
     * Monto iva
     */
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    private BigDecimal montoTotal;
    
    /**
     * Identificador de liquidación
     */
    private Integer idLiquidacion;
    
    /**
     * Identificador de servicio
     */
    private Integer idServicio;
    
    /**
     * Identificador de producto
     */
    private Integer idProducto;
    
    /**
     * Producto
     */
    private String producto;
    
    /**
     * Código GTIN
     */
    private String codigoGtin;
    
    /**
     * Código interno
     */
    private String codigoInterno;
    
    /**
     * Fecha vencimiento lote
     */
    private Date fechaVencimientoLote;
    
    /**
     * Nro lote
     */
    private String nroLote;
    
    /**
     * Metrica
     */
    private String metrica;
    
    /**
     * Código de métrica
     */
    private String codigoMetrica;
    
    /**
     * Cantidad
     */
    private BigDecimal cantidad;

    /**
     * Precio unitario con IVA
     */
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin IVA
     */
    private BigDecimal precioUnitarioSinIva;

    /**
     * Monto descuento particular
     */
    private BigDecimal montoDescuentoParticular;

    /**
     * Monto descuento global
     */
    private BigDecimal montoDescuentoGlobal;

    /**
     * Descuento particular unitario
     */
    private BigDecimal descuentoParticularUnitario;

    /**
     * Descuento global unitario
     */
    private BigDecimal descuentoGlobalUnitario;

    /**
     * Porcentaje gravada
     */
    private BigDecimal porcentajeGravada;

    /**
     * Monto exento gravado
     */
    private BigDecimal montoExentoGravado;
    
    /**
     * Dato adicional
     */
    private String datoAdicional;
    
    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getMontoDescuentoGlobal() {
        return montoDescuentoGlobal;
    }

    public void setMontoDescuentoGlobal(BigDecimal montoDescuentoGlobal) {
        this.montoDescuentoGlobal = montoDescuentoGlobal;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }

    public BigDecimal getDescuentoGlobalUnitario() {
        return descuentoGlobalUnitario;
    }

    public void setDescuentoGlobalUnitario(BigDecimal descuentoGlobalUnitario) {
        this.descuentoGlobalUnitario = descuentoGlobalUnitario;
    }

    public BigDecimal getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(BigDecimal porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public BigDecimal getMontoExentoGravado() {
        return montoExentoGravado;
    }

    public void setMontoExentoGravado(BigDecimal montoExentoGravado) {
        this.montoExentoGravado = montoExentoGravado;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setCodDncpNivelGeneral(String codDncpNivelGeneral) {
        this.codDncpNivelGeneral = codDncpNivelGeneral;
    }

    public String getCodDncpNivelGeneral() {
        return codDncpNivelGeneral;
    }

    public void setCodDncpNivelEspecifico(String codDncpNivelEspecifico) {
        this.codDncpNivelEspecifico = codDncpNivelEspecifico;
    }

    public String getCodDncpNivelEspecifico() {
        return codDncpNivelEspecifico;
    }

    public void setMetrica(String metrica) {
        this.metrica = metrica;
    }

    public String getMetrica() {
        return metrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }
    
}
