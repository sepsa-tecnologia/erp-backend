/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.CanalVenta;
import py.com.sepsa.erp.ejb.entities.info.filters.CanalVentaParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface CanalVentaFacade extends Facade<CanalVenta, CanalVentaParam, UserInfoImpl> {

    public CanalVenta find(Integer idEmpresa, Integer id, String codigo);
    
}
