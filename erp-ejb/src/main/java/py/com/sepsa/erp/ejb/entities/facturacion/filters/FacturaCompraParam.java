/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de factura de compra
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaCompraParam extends CommonParam {
    
    public FacturaCompraParam() {
    }

    public FacturaCompraParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Nro de timbrado
     */
    @QueryParam("nroTimbrado")
    private String nroTimbrado;
    
    /**
     * Fecha de vencimiento de timbrado
     */
    @QueryParam("fechaVencimientoTimbrado")
    private Date fechaVencimientoTimbrado;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;
    
    /**
     * Identificador de tipo de factura
     */
    @QueryParam("idTipoFactura")
    private Integer idTipoFactura;
    
    /**
     * Código de tipo de factura
     */
    @QueryParam("codigoTipoFactura")
    private String codigoTipoFactura;
    
    /**
     * Nro de factura
     */
    @QueryParam("nroFactura")
    private String nroFactura;
    
    /**
     * Fecha factura
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha Hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;
    
    /**
     * Digital
     */
    @QueryParam("digital")
    private Character digital;
    
    /**
     * Pagado
     */
    @QueryParam("pagado")
    private Character pagado;
    
    /**
     * CDC
     */
    @QueryParam("cdc")
    private String cdc;
    
    /**
     * Dias credito
     */
    @QueryParam("diasCredito")
    private Integer diasCredito;
    
    /**
     * Observacion
     */
    @QueryParam("observacion")
    private String observacion;
    
    /**
     * Monto iva 5
     */
    @QueryParam("montoIva5")
    private BigDecimal montoIva5;
    
    /**
     * Monto imponible 5
     */
    @QueryParam("montoImponible5")
    private BigDecimal montoImponible5;
    
    /**
     * Monto total 5
     */
    @QueryParam("montoTotal5")
    private BigDecimal montoTotal5;
    
    /**
     * Monto iva 10
     */
    @QueryParam("montoIva10")
    private BigDecimal montoIva10;
    
    /**
     * Monto imponible 10
     */
    @QueryParam("montoImponible10")
    private BigDecimal montoImponible10;
    
    /**
     * Monto total 10
     */
    @QueryParam("montoTotal10")
    private BigDecimal montoTotal10;
    
    /**
     * Monto total exento
     */
    @QueryParam("montoTotalExento")
    private BigDecimal montoTotalExento;
    
    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto total factura
     */
    @QueryParam("montoTotalFactura")
    private BigDecimal montoTotalFactura;
    
    /**
     * Identificador de orden de compra
     */
    @QueryParam("idOrdenCompra")
    private Integer idOrdenCompra;
    
    /**
     * Nro de orden de compra
     */
    @QueryParam("nroOrdenCompra")
    private String nroOrdenCompra;
    
    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;
    
    /**
     * Observación anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;
    
    /**
     * Tiene saldo
     */
    @QueryParam("tieneSaldo")
    private Boolean tieneSaldo;
    
    /**
     * Tiene retencion
     */
    @QueryParam("tieneRetencion")
    private Boolean tieneRetencion;
    
    /**
     * Identificador de procesamiento de archivo
     */
    @QueryParam("idProcesamientoArchivo")
    private Integer idProcesamientoArchivo;
    
    /**
     * Hash de procesamiento de archivo
     */
    @QueryParam("hashProcesamientoArchivo")
    private String hashProcesamientoArchivo;
    
    /**
     * Identificador de local origen
     */
    @QueryParam("idLocalOrigen")
    private Integer idLocalOrigen;
    
    /**
     * Identificador de local destino
     */
    @QueryParam("idLocalDestino")
    private Integer idLocalDestino;
    
    /**
     * Orden de compra
     */
    private OrdenCompraParam ordenCompra;
    
    /**
     * Factura Detalles
     */
    private List<FacturaCompraDetalleParam> facturaCompraDetalles;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(nroTimbrado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de timbrado"));
        }
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }
        
        if(isNull(idTipoFactura) && isNullOrEmpty(codigoTipoFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de factura"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de la factura"));
        } else {
            
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            
            if(cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la factura no puede ser posterior a la fecha actual"));
            }
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de anulado de la factura"));
        }
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la factura es digital o no"));
        } else {
            if(digital.equals('S')) {
                if(isNullOrEmpty(cdc)) {
                    addError(MensajePojo.createInstance()
                                .descripcion("Se debe indicar el cdc"));
                }
            } else {
                if(isNull(fechaVencimientoTimbrado)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar la fecha de vencimiento del timbrado"));
                }
            }
        }
        
        if(isNull(pagado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la factura esta pagada o no"));
        }
        
        if(!isNullOrEmpty(codigoTipoFactura) && codigoTipoFactura.equals("CREDITO") && isNull(diasCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad de dias de crédito"));
        }
        
        if(!isNull(idTipoFactura) && idTipoFactura == 1 && isNull(diasCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad de dias de crédito"));
        }
        
        if(isNull(montoIva5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 5%"));
        }
        
        if(isNull(montoImponible5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 5%"));
        }
        
        if(isNull(montoTotal5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 5%"));
        }
        
        if(isNull(montoIva10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 10%"));
        }
        
        if(isNull(montoImponible10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 10%"));
        }
        
        if(isNull(montoTotal10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 10%"));
        }
        
        if(isNull(montoTotalExento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excento"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA"));
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoTotalFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total de la factura"));
        }
        
        if(isNullOrEmpty(nroFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de la factura"));
        }
        
        if(isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del cliente"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el RUC/CI del cliente"));
        }
        
        if(isNullOrEmpty(facturaCompraDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la factura de compra"));
        } else {
            for (FacturaCompraDetalleParam detalle : facturaCompraDetalles) {
                detalle.setIdFacturaCompra(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> result = new ArrayList<>();
        
        if(!isNullOrEmpty(facturaCompraDetalles)) {
            for (FacturaCompraDetalleParam detalle : facturaCompraDetalles) {
                result.addAll(detalle.getErrores());
            }
        }
        
        return result;
    }
    
    public boolean isValidToGetReporteVenta() {
        
        limpiarErrores();
        
        if(isNull(fechaDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha desde"));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaDesde);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            fechaDesde = cal.getTime();
        }
        
        if(isNull(fechaHasta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha hasta"));
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaHasta);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            fechaHasta = cal.getTime();
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCancel() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura de compra"));
        }
        
        if(isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de motivo anulación"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToDelete() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura de compra"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToUpdateFileProcess() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura de compra"));
        }
        
        if(isNull(idProcesamientoArchivo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento de archivo"));
        }
        
        return !tieneErrores();
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Integer getIdTipoFactura() {
        return idTipoFactura;
    }

    public void setIdTipoFactura(Integer idTipoFactura) {
        this.idTipoFactura = idTipoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getDigital() {
        return digital;
    }

    public Integer getDiasCredito() {
        return diasCredito;
    }

    public void setDiasCredito(Integer diasCredito) {
        this.diasCredito = diasCredito;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalFactura() {
        return montoTotalFactura;
    }

    public void setMontoTotalFactura(BigDecimal montoTotalFactura) {
        this.montoTotalFactura = montoTotalFactura;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoTipoFactura() {
        return codigoTipoFactura;
    }

    public void setCodigoTipoFactura(String codigoTipoFactura) {
        this.codigoTipoFactura = codigoTipoFactura;
    }

    public Integer getIdOrdenCompra() {
        return idOrdenCompra;
    }

    public void setIdOrdenCompra(Integer idOrdenCompra) {
        this.idOrdenCompra = idOrdenCompra;
    }

    public void setNroOrdenCompra(String nroOrdenCompra) {
        this.nroOrdenCompra = nroOrdenCompra;
    }

    public String getNroOrdenCompra() {
        return nroOrdenCompra;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setPagado(Character pagado) {
        this.pagado = pagado;
    }

    public Character getPagado() {
        return pagado;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public Boolean getTieneSaldo() {
        return tieneSaldo;
    }

    public void setTieneSaldo(Boolean tieneSaldo) {
        this.tieneSaldo = tieneSaldo;
    }

    public Boolean getTieneRetencion() {
        return tieneRetencion;
    }

    public void setTieneRetencion(Boolean tieneRetencion) {
        this.tieneRetencion = tieneRetencion;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public Integer getIdProcesamientoArchivo() {
        return idProcesamientoArchivo;
    }

    public void setIdProcesamientoArchivo(Integer idProcesamientoArchivo) {
        this.idProcesamientoArchivo = idProcesamientoArchivo;
    }

    public String getHashProcesamientoArchivo() {
        return hashProcesamientoArchivo;
    }

    public void setHashProcesamientoArchivo(String hashProcesamientoArchivo) {
        this.hashProcesamientoArchivo = hashProcesamientoArchivo;
    }

    public List<FacturaCompraDetalleParam> getFacturaCompraDetalles() {
        return facturaCompraDetalles;
    }

    public void setFacturaCompraDetalles(List<FacturaCompraDetalleParam> facturaCompraDetalles) {
        this.facturaCompraDetalles = facturaCompraDetalles;
    }

    public Integer getIdLocalOrigen() {
        return idLocalOrigen;
    }

    public void setIdLocalOrigen(Integer idLocalOrigen) {
        this.idLocalOrigen = idLocalOrigen;
    }

    public Integer getIdLocalDestino() {
        return idLocalDestino;
    }

    public void setIdLocalDestino(Integer idLocalDestino) {
        this.idLocalDestino = idLocalDestino;
    }

    public void setOrdenCompra(OrdenCompraParam ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public OrdenCompraParam getOrdenCompra() {
        return ordenCompra;
    }
}
