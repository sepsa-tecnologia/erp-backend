/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de retención de compra
 * @author Jonathan D. Bernal Fernández
 */
public class RetencionCompraParam extends CommonParam {
    
    public RetencionCompraParam() {
    }

    public RetencionCompraParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Nro de retención
     */
    @QueryParam("nroRetencion")
    private String nroRetencion;
    
    /**
     * Fecha
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;
    
    /**
     * Porcentaje retención
     */
    @QueryParam("porcentajeRetencion")
    private Integer porcentajeRetencion;
    
    /**
     * Monto retenido total
     */
    @QueryParam("montoRetenidoTotal")
    private BigDecimal montoRetenidoTotal;
    
    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;
    
    /**
     * Observación anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;
    
    /**
     * Identificador de factura de compra
     */
    @QueryParam("idFacturaCompra")
    private Integer idFacturaCompra;
    
    /**
     * Detalle
     */
    private List<RetencionCompraDetalleParam> retencionCompraDetalles;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de retención"));
        } else {
            
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            
            if(cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la retención no puede ser posterior a la fecha actual"));
            }
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA total"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if(isNull(montoRetenidoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto retenido total"));
        }
        
        if(isNull(porcentajeRetencion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de retención"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNullOrEmpty(nroRetencion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de retención"));
        }
        
        if(isNullOrEmpty(retencionCompraDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de retención de compra"));
        } else {
            for (RetencionCompraDetalleParam detalle : retencionCompraDetalles) {
                detalle.setFecha(fecha);
                detalle.setIdRetencionCompra(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToCancel() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de retención de compra"));
        }
        
        if(isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de motivo anulación"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToDelete() {
        
        limpiarErrores();
        
        if(isNull(id) && isNull(idFacturaCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de retención de compra o identificador de factura de compra"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = new ArrayList<>();
        
        if(retencionCompraDetalles != null) {
            for (RetencionCompraDetalleParam detalle : retencionCompraDetalles) {
                result.addAll(detalle.getErrores());
            }
        }
        
        return result;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public String getNroRetencion() {
        return nroRetencion;
    }

    public void setNroRetencion(String nroRetencion) {
        this.nroRetencion = nroRetencion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public BigDecimal getMontoRetenidoTotal() {
        return montoRetenidoTotal;
    }

    public void setMontoRetenidoTotal(BigDecimal montoRetenidoTotal) {
        this.montoRetenidoTotal = montoRetenidoTotal;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public List<RetencionCompraDetalleParam> getRetencionCompraDetalles() {
        return retencionCompraDetalles;
    }

    public void setRetencionCompraDetalles(List<RetencionCompraDetalleParam> retencionCompraDetalles) {
        this.retencionCompraDetalles = retencionCompraDetalles;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }
}
