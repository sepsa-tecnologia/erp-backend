/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import py.com.sepsa.erp.ejb.entities.info.ConfiguracionGeneral;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionGeneralParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.FirmaDigitalPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Gustavo Benitez
 */
public interface ConfiguracionGeneralFacade extends Facade<ConfiguracionGeneral, ConfiguracionGeneralParam, UserInfoImpl> {

    public String getCVValor(String codigoConfiguracion);

    public List<FirmaDigitalPojo> getFirmaDigital(ConfiguracionGeneralParam param);

}
