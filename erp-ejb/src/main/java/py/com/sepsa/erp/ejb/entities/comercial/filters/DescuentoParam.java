/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.math.BigDecimal;
import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de descuento
 * @author Jonathan D. Bernal Fernández
 */
public class DescuentoParam extends CommonParam {
    
    public DescuentoParam() {
    }

    public DescuentoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de contrato
     */
    @QueryParam("idContrato")
    private Integer idContrato;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Identificador de cadena
     */
    @QueryParam("idCadena")
    private Integer idCadena;
    
    /**
     * Identificador de tipo de descuento
     */
    @QueryParam("idTipoDescuento")
    private Integer idTipoDescuento;
    
    /**
     * Código de tipo de descuento
     */
    @QueryParam("codigoTipoDescuento")
    private String codigoTipoDescuento;
    
    /**
     * Fecha inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha desde desde
     */
    @QueryParam("fechaDesdeDesde")
    private Date fechaDesdeDesde;
    
    /**
     * Fecha desde hasta
     */
    @QueryParam("fechaDesdeHasta")
    private Date fechaDesdeHasta;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Fecha hasta desde
     */
    @QueryParam("fechaHastaDesde")
    private Date fechaHastaDesde;
    
    /**
     * Fecha hasta hasta
     */
    @QueryParam("fechaHastaHasta")
    private Date fechaHastaHasta;
    
    /**
     * Excedente
     */
    @QueryParam("excedente")
    private Character excedente;
    
    /**
     * Porcentual
     */
    @QueryParam("porcentual")
    private Character porcentual;
    
    /**
     * Tiene cadena
     */
    @QueryParam("tieneCadena")
    private Boolean tieneCadena;
    
    /**
     * Tiene cliente
     */
    @QueryParam("tieneCliente")
    private Boolean tieneCliente;
    
    /**
     * Tiene servicio
     */
    @QueryParam("tieneServicio")
    private Boolean tieneServicio;
    
    /**
     * Tiene contrato
     */
    @QueryParam("tieneContrato")
    private Boolean tieneContrato;
    
    /**
     * Identificador de motivo de descuento
     */
    @QueryParam("idMotivoDescuento")
    private Integer idMotivoDescuento;
    
    /**
     * Valor de descuento
     */
    @QueryParam("valorDescuento")
    private BigDecimal valorDescuento;
    
    public boolean isValidToInactivate() {
        
        limpiarErrores();
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de descuento"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToActivate() {
        
        limpiarErrores();
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de descuento"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado"));
        }
        
        if(isNull(idTipoDescuento) && isNullOrEmpty(codigoTipoDescuento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de descuento"));
        }
        
        if(isNull(valorDescuento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor de descuento"));
        }
        
        if(isNull(excedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el descuento contempla excedente"));
        }
        
        if(isNull(porcentual)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el descuento es porcentual"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdCadena() {
        return idCadena;
    }

    public void setIdCadena(Integer idCadena) {
        this.idCadena = idCadena;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Character getExcedente() {
        return excedente;
    }

    public void setExcedente(Character excedente) {
        this.excedente = excedente;
    }

    public Character getPorcentual() {
        return porcentual;
    }

    public void setPorcentual(Character porcentual) {
        this.porcentual = porcentual;
    }

    public Integer getIdMotivoDescuento() {
        return idMotivoDescuento;
    }

    public void setIdMotivoDescuento(Integer idMotivoDescuento) {
        this.idMotivoDescuento = idMotivoDescuento;
    }

    public void setValorDescuento(BigDecimal valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public BigDecimal getValorDescuento() {
        return valorDescuento;
    }

    public Date getFechaDesdeDesde() {
        return fechaDesdeDesde;
    }

    public void setFechaDesdeDesde(Date fechaDesdeDesde) {
        this.fechaDesdeDesde = fechaDesdeDesde;
    }

    public Date getFechaDesdeHasta() {
        return fechaDesdeHasta;
    }

    public void setFechaDesdeHasta(Date fechaDesdeHasta) {
        this.fechaDesdeHasta = fechaDesdeHasta;
    }

    public Date getFechaHastaDesde() {
        return fechaHastaDesde;
    }

    public void setFechaHastaDesde(Date fechaHastaDesde) {
        this.fechaHastaDesde = fechaHastaDesde;
    }

    public Date getFechaHastaHasta() {
        return fechaHastaHasta;
    }

    public void setFechaHastaHasta(Date fechaHastaHasta) {
        this.fechaHastaHasta = fechaHastaHasta;
    }

    public Boolean getTieneCadena() {
        return tieneCadena;
    }

    public void setTieneCadena(Boolean tieneCadena) {
        this.tieneCadena = tieneCadena;
    }

    public Boolean getTieneCliente() {
        return tieneCliente;
    }

    public void setTieneCliente(Boolean tieneCliente) {
        this.tieneCliente = tieneCliente;
    }

    public Boolean getTieneServicio() {
        return tieneServicio;
    }

    public void setTieneServicio(Boolean tieneServicio) {
        this.tieneServicio = tieneServicio;
    }

    public Boolean getTieneContrato() {
        return tieneContrato;
    }

    public void setTieneContrato(Boolean tieneContrato) {
        this.tieneContrato = tieneContrato;
    }

    public Integer getIdTipoDescuento() {
        return idTipoDescuento;
    }

    public void setIdTipoDescuento(Integer idTipoDescuento) {
        this.idTipoDescuento = idTipoDescuento;
    }

    public String getCodigoTipoDescuento() {
        return codigoTipoDescuento;
    }

    public void setCodigoTipoDescuento(String codigoTipoDescuento) {
        this.codigoTipoDescuento = codigoTipoDescuento;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }
}
