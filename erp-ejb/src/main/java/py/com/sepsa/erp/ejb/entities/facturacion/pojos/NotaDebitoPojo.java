/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import py.com.sepsa.utils.misc.NumberToLetterConverter;

/**
 * POJO para la entidad factura
 * @author Williams Vera
 */
public class NotaDebitoPojo {

    public NotaDebitoPojo() {
    }

    public NotaDebitoPojo(Integer id, Integer idCliente, Integer idTalonario, String timbrado, Date fechaVencimientoTimbrado, Integer idMoneda, String moneda, String nroNotaDebito, Date fecha, String razonSocial, String direccion, String ruc, String telefono, Character anulado, Character impreso, Character entregado, Character digital, Date fechaEntrega, String observacion, BigDecimal montoIva5, BigDecimal montoImponible5, BigDecimal montoTotal5, BigDecimal montoIva10, BigDecimal montoImponible10, BigDecimal montoTotal10, BigDecimal montoTotalExento, BigDecimal montoIvaTotal, BigDecimal montoImponibleTotal, BigDecimal montoTotalNotaDebito, String cdc) {
        this.id = id;
        this.idCliente = idCliente;
        this.idTalonario = idTalonario;
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.nroNotaDebito = nroNotaDebito;
        this.timbrado = timbrado;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.ruc = ruc;
        this.telefono = telefono;
        this.anulado = anulado;
        this.impreso = impreso;
        this.entregado = entregado;
        this.digital = digital;
        this.fechaEntrega = fechaEntrega;
        this.observacion = observacion;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalNotaDebito = montoTotalNotaDebito;
        this.cdc = cdc;
        completarTotalLetras();
    }

    public NotaDebitoPojo(Integer id, Integer idCliente,String nroNotaDebito,
            Integer idTalonario, Date fechaVencimientoTimbrado,
            Date fechaInicioVigenciaTimbrado, String timbrado, String serie,
            String descripcionLocalTalonario, Date fecha, String razonSocial,
            String direccion, Integer nroCasa, Integer idDepartamento, Integer idDistrito,
            Integer idCiudad, String ruc, String telefono, String email, String observacion,
            Character anulado, Character digital, Character archivoSet,
            Character archivoEdi, Character generadoSet, Character generadoEdi,
            Character estadoSincronizado, Character entregado, Character impreso,
            Integer idEstado, String codigoEstado, Integer idProcesamiento,
            String cdc, String codSeguridad, Integer idNaturalezaCliente,
            Integer idMoneda, String moneda, String codigoMoneda,
            Integer idMotivoEmision, String motivoEmision, String codigoMotivoEmision,
            Integer idMotivoEmisionInterno, String motivoEmisionInterno, String codigoMotivoEmisionInterno,
            BigDecimal montoIva5, BigDecimal montoImponible5,
            BigDecimal montoTotal5, BigDecimal montoIva10,
            BigDecimal montoImponible10, BigDecimal montoTotal10,
            BigDecimal montoTotalExento, BigDecimal montoIvaTotal,
            BigDecimal montoImponibleTotal, BigDecimal montoTotalNotaDebito,
            String nroFacturas, BigDecimal montoTotalDescuentoGlobal, BigDecimal porcentajeDescuentoGlobal, Integer idTipoCambio) {
        this.id = id;
        this.idCliente = idCliente;
        this.nroNotaDebito = nroNotaDebito;
        this.idTalonario = idTalonario;
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
        this.timbrado = timbrado;
        this.serie = serie;
        this.descripcionLocalTalonario = descripcionLocalTalonario;
        this.fecha = fecha;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.nroCasa = nroCasa;
        this.idDepartamento = idDepartamento;
        this.idDistrito = idDistrito;
        this.idCiudad = idCiudad;
        this.ruc = ruc;
        this.telefono = telefono;
        this.email = email;
        this.observacion = observacion;
        this.anulado = anulado;
        this.digital = digital;
        this.archivoSet = archivoSet;
        this.archivoEdi = archivoEdi;
        this.generadoSet = generadoSet;
        this.generadoEdi = generadoEdi;
        this.estadoSincronizado = estadoSincronizado;
        this.entregado = entregado;
        this.impreso = impreso;
        this.idEstado = idEstado;
        this.codigoEstado = codigoEstado;
        this.idProcesamiento = idProcesamiento;
        this.cdc = cdc;
        this.codSeguridad = codSeguridad;
        this.idNaturalezaCliente = idNaturalezaCliente;
        this.idMoneda = idMoneda;
        this.moneda = moneda;
        this.codigoMoneda = codigoMoneda;
        this.idMotivoEmision = idMotivoEmision;
        this.motivoEmision = motivoEmision;
        this.codigoMotivoEmision = codigoMotivoEmision;
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
        this.motivoEmisionInterno = motivoEmisionInterno;
        this.codigoMotivoEmisionInterno = codigoMotivoEmisionInterno;
        this.montoIva5 = montoIva5;
        this.montoImponible5 = montoImponible5;
        this.montoTotal5 = montoTotal5;
        this.montoIva10 = montoIva10;
        this.montoImponible10 = montoImponible10;
        this.montoTotal10 = montoTotal10;
        this.montoTotalExento = montoTotalExento;
        this.montoIvaTotal = montoIvaTotal;
        this.montoImponibleTotal = montoImponibleTotal;
        this.montoTotalNotaDebito = montoTotalNotaDebito;
        this.nroFacturas = nroFacturas;
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
        this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
        this.idTipoCambio = idTipoCambio;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de procesamiento
     */
    private Integer idProcesamiento;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Identificador de talonario
     */
    private Integer idTalonario;
    
    /**
     * Identificador de moneda
     */
    private Integer idMoneda;
    
    /**
     * Moneda
     */
    private String moneda;
    
    /**
     * Código de moneda
     */
    private String codigoMoneda;
    
    /**
     * Nro de nota de crédito
     */
    private String nroNotaDebito;
    
    /**
     * Fecha de inicio de vigencia de timbrado
     */
    private Date fechaInicioVigenciaTimbrado;
    
    /**
     * Identificador de motivo de emisión
     */
    private Integer idMotivoEmision;
    
    /**
     * Motivo de emisión
     */
    private String motivoEmision;
    
    /**
     * Código del motivo de emisión
     */
    private String codigoMotivoEmision;
    
    /**
     * Identificador de motivo de emisión interno
     */
    private Integer idMotivoEmisionInterno;
    
    /**
     * Motivo de emisión interno
     */
    private String motivoEmisionInterno;
    
    /**
     * Código del motivo de emisión interno
     */
    private String codigoMotivoEmisionInterno;
    
    /**
     * Timbrado
     */
    private String timbrado;
    
    /**
     * Serie
     */
    private String serie;
    
    /**
     * Local talonario
     */
    private String descripcionLocalTalonario;
    
    /**
     * Fecha de vencimiento de timbrado
     */
    private Date fechaVencimientoTimbrado;
    
    /**
     * Fecha
     */
    private Date fecha;
    
    /**
     * Razon social
     */
    private String razonSocial;
    
    /**
     * Dirección
     */
    private String direccion;
    
    /**
     * Nro de casa
     */
    private Integer nroCasa;
    
    /**
     * Identificador de departamento
     */
    private Integer idDepartamento;
    
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    
    /**
     * RUC
     */
    private String ruc;
    
    /**
     * Telefono
     */
    private String telefono;
    
    /**
     * Email
     */
    private String email;
    
    /**
     * Anulado
     */
    private Character anulado;
    
    /**
     * Impreso
     */
    private Character impreso;
    
    /**
     * Entregado
     */
    private Character entregado;
    
    /**
     * Digital
     */
    private Character digital;
    
    /**
     * Archivo SET
     */
    private Character archivoSet;
    
    /**
     * Archivo EDI
     */
    private Character archivoEdi;
    
    /**
     * Generado SET
     */
    private Character generadoSet;
    
    /**
     * Generado EDI
     */
    private Character generadoEdi;
    
    /**
     * Estado sincronizado
     */
    private Character estadoSincronizado;
    
    /**
     * Fecha de entrega
     */
    private Date fechaEntrega;
    
    /**
     * Observación
     */
    private String observacion;
    
    /**
     * Monto iva 5
     */
    private BigDecimal montoIva5;
    
    /**
     * Monto imponible 5
     */
    private BigDecimal montoImponible5;
    
    /**
     * Monto total 5
     */
    private BigDecimal montoTotal5;
    
    /**
     * Monto iva 10
     */
    private BigDecimal montoIva10;
    
    /**
     * Monto imponible 10
     */
    private BigDecimal montoImponible10;
    
    /**
     * Monto total 10
     */
    private BigDecimal montoTotal10;
    
    /**
     * Monto total exento
     */
    private BigDecimal montoTotalExento;
    
    /**
     * Monto iva total
     */
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto imponible total
     */
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto total nota Débito
     */
    private BigDecimal montoTotalNotaDebito;
    
    /**
     * CDC
     */
    private String cdc;
    
    /**
     * Código de seguridad
     */
    private String codSeguridad;
    
    /**
     * Identificador de naturaleza de cliente
     */
    private Integer idNaturalezaCliente;

    /**
     * Total letras
     */
    private String totalLetras;
    
    /**
     * Detalles
     */
    private List<NotaDebitoDetallePojo> detalles;
    
    /**
     * Nros de factura afectadas
     */
    private String nroFacturas;
    
    /**
     * Monto total del descuento
     */
    private BigDecimal montoTotalDescuentoGlobal;
    /**
     * Porcentaje del descuento
     */
    private BigDecimal porcentajeDescuentoGlobal;
    /**
     * Identificador del tipo de cambio
     */
    private Integer idTipoCambio;
    /**
     * Completa el campo total letras
     */
    public final void completarTotalLetras() {
        try {
            this.totalLetras = NumberToLetterConverter.convertNumberToLetter(
                    montoTotalNotaDebito, NumberToLetterConverter.Moneda.valueOf(moneda));
        } catch (Exception e) {
            this.totalLetras = "---";
        }
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDetalles(List<NotaDebitoDetallePojo> detalles) {
        this.detalles = detalles;
    }

    public List<NotaDebitoDetallePojo> getDetalles() {
        return detalles;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public String getCdc() {
        return cdc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getNroNotaDebito() {
        return nroNotaDebito;
    }

    public void setNroNotaDebito(String nroNotaDebito) {
        this.nroNotaDebito = nroNotaDebito;
    }

    public BigDecimal getMontoTotalNotaDebito() {
        return montoTotalNotaDebito;
    }

    public void setMontoTotalNotaDebito(BigDecimal montoTotalNotaDebito) {
        this.montoTotalNotaDebito = montoTotalNotaDebito;
    }

    public void setTotalLetras(String totalLetras) {
        this.totalLetras = totalLetras;
    }

    public String getTotalLetras() {
        return totalLetras;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setDescripcionLocalTalonario(String descripcionLocalTalonario) {
        this.descripcionLocalTalonario = descripcionLocalTalonario;
    }

    public String getDescripcionLocalTalonario() {
        return descripcionLocalTalonario;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setNroFacturas(String nroFacturas) {
        this.nroFacturas = nroFacturas;
    }

    public String getNroFacturas() {
        return nroFacturas;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setFechaInicioVigenciaTimbrado(Date fechaInicioVigenciaTimbrado) {
        this.fechaInicioVigenciaTimbrado = fechaInicioVigenciaTimbrado;
    }

    public Date getFechaInicioVigenciaTimbrado() {
        return fechaInicioVigenciaTimbrado;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public String getMotivoEmision() {
        return motivoEmision;
    }

    public void setMotivoEmision(String motivoEmision) {
        this.motivoEmision = motivoEmision;
    }

    public String getCodigoMotivoEmision() {
        return codigoMotivoEmision;
    }

    public void setCodigoMotivoEmision(String codigoMotivoEmision) {
        this.codigoMotivoEmision = codigoMotivoEmision;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getPorcentajeDescuentoGlobal() {
        return porcentajeDescuentoGlobal;
    }

    public void setPorcentajeDescuentoGlobal(BigDecimal porcentajeDescuentoGlobal) {
        this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public Integer getIdMotivoEmisionInterno() {
        return idMotivoEmisionInterno;
    }

    public void setIdMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
    }

    public String getMotivoEmisionInterno() {
        return motivoEmisionInterno;
    }

    public void setMotivoEmisionInterno(String motivoEmisionInterno) {
        this.motivoEmisionInterno = motivoEmisionInterno;
    }

    public String getCodigoMotivoEmisionInterno() {
        return codigoMotivoEmisionInterno;
    }

    public void setCodigoMotivoEmisionInterno(String codigoMotivoEmisionInterno) {
        this.codigoMotivoEmisionInterno = codigoMotivoEmisionInterno;
    }

}
