/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.filters.LocalParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@javax.ejb.Local
public interface LocalFacade extends Facade<Local, LocalParam, UserInfoImpl> {
    
}
