/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.proceso.TipoLote;
import py.com.sepsa.erp.ejb.entities.proceso.filters.TipoLoteParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface TipoLoteFacade extends Facade<TipoLote, TipoLoteParam, UserInfoImpl> {

    public TipoLote find(Integer id, String codigo);
    
}
