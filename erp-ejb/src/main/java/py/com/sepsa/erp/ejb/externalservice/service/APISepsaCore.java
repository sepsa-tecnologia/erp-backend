/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.externalservice.service;

import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionGeneralParam;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;

/**
 * Cliente API Service SepsaCore
 *
 * @author Gustavo Benitez
 */
@Log4j2
public class APISepsaCore extends API {

    //Datos de conexión al API REST
    private final static String BASE_API = "SepsaCore-api";
    private final static int CONN_TIMEOUT = 5 * 1000;
    private final static int READ_TIMEOUT = 5 * 1000;
    private final static Scheme PROTOCOL = Scheme.HTTPS;

    //Datos para enviar formulario HTTP
    private static final String BOUNDARY = "--------------------------" + System.currentTimeMillis();
    private static final String LINE_FEED = "\r\n";

    private static String token;
    private static String apiToken;

    /**
     * Login al server
     *
     * @param userId Identificador del usuario
     * @param pass Contraseña del usuario
     * @return Respuesta HTTP
     */
    public HttpStringResponse login(String userId, String pass) {

        HttpStringResponse response = new HttpStringResponse();

        HttpURLConnection httpConn = POST(Resource.LOGIN.url,
                ContentType.JSON);

        try {
            PrintWriter writer = null;
            if (httpConn != null) {

                JSONObject body = new JSONObject();
                body.put("userName", userId);
                body.put("password", pass);

                this.addBody(httpConn, body.toString());

                response = this.resolve(httpConn, writer);
                httpConn.disconnect();

            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Obtiene el tiempo del servidor en milisegundos
     *
     * @return Respuesta HTTP
     */
    public HttpStringResponse ping() {
        HttpStringResponse response = new HttpStringResponse();
        Map<String, Object> params = new HashMap();
        HttpURLConnection httpConn = GET(Resource.PING.url,
                ContentType.JSON, params);
        try {
            PrintWriter writer = null;
            if (httpConn != null) {
                response = this.resolve(httpConn, writer);

                httpConn.disconnect();
            }
        } catch (Exception ex) {
            log.fatal("Error", ex);
        }

        return response;
    }

    /**
     * Obtiene datos de Firma Digital
     *
     * @param param parámetros
     * @return
     */
    public HttpStringResponse obtenerFirmaDigital(ConfiguracionGeneralParam param) {
        HttpStringResponse response = new HttpStringResponse();

        Map<String, Object> map = new HashMap<>();
        map.put("activo", "S");
        if(param.getIdClienteSepsa() != null) {
            map.put("idCliente", param.getIdClienteSepsa());
        }
        if(param.getFirstResult() != null) {
            map.put("firstResult", param.getFirstResult());
        }
        if(param.getPageSize() != null) {
            map.put("pageSize", param.getPageSize());
        }

        HttpURLConnection httpConn = GET(Resource.FIRMADIGITAL.url, ContentType.JSON, map);
        try {
            if (httpConn != null) {
                response = HttpStringResponse.createInstance(httpConn);
                httpConn.disconnect();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
        }
        return response;
    }

    public APISepsaCore(String host) {
        super(Service.EDI_SERVER, PROTOCOL, BASE_API, token, CONN_TIMEOUT,
                READ_TIMEOUT, BOUNDARY, LINE_FEED, null, apiToken);

        List<Host> hosts = Host.createInstaces(host);

        this.host = hosts == null || hosts.isEmpty() ? null : hosts.get(0);
    }

    public APISepsaCore(String host, String apiToken) {
        super(Service.EDI_SERVER, PROTOCOL, BASE_API, token, CONN_TIMEOUT,
                READ_TIMEOUT, BOUNDARY, LINE_FEED, null, apiToken);
        
        List<Host> hosts = Host.createInstaces(host);

        this.host = hosts == null || hosts.isEmpty() ? null : hosts.get(0);
    }

    /**
     * Recursos de conexión o servicios del API
     */
    public enum Resource {

        //Servicios
        FIRMADIGITAL("Servicio a firma digital", "api/firma-digital"),
        PING("Realiza un ping al servidor", "api/ping"),
        LOGIN("Ingreso al sistema", "api/usuario/login");

        /**
         * Nombre del recurso
         */
        private String resource;

        /**
         * URL del recurso
         */
        private String url;

        /**
         * Obtiene el nombre del recurso
         *
         * @return Nombre del recurso
         */
        public String getResource() {
            return resource;
        }

        /**
         * Setea el nombre del recurso
         *
         * @param resource Nombre del recurso
         */
        public void setResource(String resource) {
            this.resource = resource;
        }

        /**
         * Obtiene la URL del servicio
         *
         * @return URL del servicio
         */
        public String getUrl() {
            return url;
        }

        /**
         * Setea la URL del servicio
         *
         * @param url URL del servicio
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * Constructor de Resource
         *
         * @param resource Nombre del recurso
         * @param url URL del servicio o recurso
         */
        Resource(String resource, String url) {
            this.resource = resource;
            this.url = url;
        }

    }
}
