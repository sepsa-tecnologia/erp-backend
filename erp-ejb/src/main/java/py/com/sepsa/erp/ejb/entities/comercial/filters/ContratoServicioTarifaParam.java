/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de contrato servicio
 * @author Jonathan D. Bernal Fernández
 */
public class ContratoServicioTarifaParam extends CommonParam {
    
    public ContratoServicioTarifaParam() {
    }

    public ContratoServicioTarifaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de contrato
     */
    @QueryParam("idContrato")
    private Integer idContrato;
    
    /**
     * Identificador de Servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Identificador de Tarifa
     */
    @QueryParam("idTarifa")
    private Integer idTarifa;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idContrato)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de contrato"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(idTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tarifa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idContrato)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de contrato"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(idTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tarifa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdTarifa(Integer idTarifa) {
        this.idTarifa = idTarifa;
    }

    public Integer getIdTarifa() {
        return idTarifa;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }
}
