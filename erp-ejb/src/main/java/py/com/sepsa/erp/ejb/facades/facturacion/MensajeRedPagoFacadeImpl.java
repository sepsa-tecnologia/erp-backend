/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.facturacion.MensajeRedPago;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MensajeRedPagoFacade", mappedName = "MensajeRedPagoFacade")
@Local(MensajeRedPagoFacade.class)
public class MensajeRedPagoFacadeImpl extends FacadeImpl<MensajeRedPago, CommonParam> implements MensajeRedPagoFacade {

    public MensajeRedPagoFacadeImpl() {
        super(MensajeRedPago.class);
    }
    
}
