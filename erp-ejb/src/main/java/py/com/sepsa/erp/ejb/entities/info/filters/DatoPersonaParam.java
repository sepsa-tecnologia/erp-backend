/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * @author Jonathan
 */
public class DatoPersonaParam extends CommonParam {
    
    public DatoPersonaParam() {
    }

    public DatoPersonaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificadorde de tipo de dato persona
     */
    @QueryParam("idTipoDatoPersona")
    private Integer idTipoDatoPersona;
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Valor
     */
    @QueryParam("valor")
    private String valor;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoDatoPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de dato persona"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNullOrEmpty(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idTipoDatoPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de dato persona"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNullOrEmpty(valor)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el valor"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdTipoDatoPersona() {
        return idTipoDatoPersona;
    }

    public void setIdTipoDatoPersona(Integer idTipoDatoPersona) {
        this.idTipoDatoPersona = idTipoDatoPersona;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
