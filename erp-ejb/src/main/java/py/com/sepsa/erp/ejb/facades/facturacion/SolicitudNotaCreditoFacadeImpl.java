/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.facturacion.SolicitudNotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.SolicitudNotaCreditoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.SolicitudNotaCreditoParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Dates;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "SolicitudNotaCreditoFacade", mappedName = "SolicitudNotaCreditoFacade")
@Local(SolicitudNotaCreditoFacade.class)
public class SolicitudNotaCreditoFacadeImpl extends FacadeImpl<SolicitudNotaCredito, SolicitudNotaCreditoParam> implements SolicitudNotaCreditoFacade {

    public SolicitudNotaCreditoFacadeImpl() {
        super(SolicitudNotaCredito.class);
    }
    
    public Boolean validToCreate(SolicitudNotaCreditoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Moneda moneda = facades
                .getMonedaFacade()
                .find(param.getIdMoneda(), param.getCodigoMoneda());
        
        if(moneda == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la moneda"));
        } else {
            param.setIdMoneda(moneda.getId());
        }
        
        if(param.getRecibido().equals('N')) {

            String id = String.valueOf(maxId() + 1L);
            String date = Dates.formatToString(Calendar.getInstance().getTime(), Dates.DateFormat.DATE_SHORT_ALT);
            String nroDocumento = String.format("SNC-%s-%s", date, id);
            
            param.setNroDocumento(nroDocumento);
        }
        
        Cliente cliente = param.getIdCliente() == null
                ? null
                : facades.getClienteFacade().find(param.getIdCliente());
        
        if(param.getIdCliente() != null && cliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el cliente"));
        }
        
        if(!isNullOrEmpty(param.getHashProcesamientoArchivo())) {
            
            SolicitudNotaCreditoParam param1 = new SolicitudNotaCreditoParam();
            param1.setHashProcesamientoArchivo(param.getHashProcesamientoArchivo());
            Long size = findSize(param1);
            
            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe una solicitud de nota de crédito registrada con el mismo hash de archivo"));
            }
        }
        
        if(param.getIdLocalOrigen() != null) {
            
            py.com.sepsa.erp.ejb.entities.info.Local local = facades.getLocalFacade().find(param.getIdLocalOrigen());
            
            if(local == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local origen"));
            }
        }

        if(param.getIdLocalDestino() != null) {
            
            py.com.sepsa.erp.ejb.entities.info.Local local = facades.getLocalFacade().find(param.getIdLocalDestino());
            
            if(local == null) {
                param.addError(MensajePojo.createInstance().descripcion("No existe el local destino"));
            }
        }
        
        for (SolicitudNotaCreditoDetalleParam detalle : param.getSolicitudNotaCreditoDetalles()) {
            facades.getSolicitudNotaCreditoDetalleFacade().validToCreate(detalle, false);
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(SolicitudNotaCreditoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        SolicitudNotaCredito snc = find(param.getId());
        
        if(isNull(snc)) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la SNC"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToUpdateFileProcess(SolicitudNotaCreditoParam param) {
        
        if(!param.isValidToUpdateFileProcess()) {
            return Boolean.FALSE;
        }
        
        SolicitudNotaCredito snc = find(param.getId());
        
        if(isNull(snc)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la solicitud de nota de crédito"));
        }
        
        ProcesamientoArchivo procesamientoArchivo = facades.getProcesamientoArchivoFacade().find(param.getIdProcesamientoArchivo());
        
        if(isNull(procesamientoArchivo)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el procesamiento de archivo"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public SolicitudNotaCredito create(SolicitudNotaCreditoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        SolicitudNotaCredito snc = new SolicitudNotaCredito();
        snc.setIdEmpresa(userInfo.getIdEmpresa());
        snc.setAnulado(param.getAnulado());
        snc.setRecibido(param.getRecibido());
        snc.setArchivoEdi(param.getArchivoEdi());
        snc.setGeneradoEdi(param.getGeneradoEdi());
        snc.setFechaInsercion(Calendar.getInstance().getTime());
        snc.setIdCliente(param.getIdCliente());
        snc.setNroDocumento(param.getNroDocumento());
        snc.setIdMoneda(param.getIdMoneda());
        snc.setMontoImponible10(param.getMontoImponible10().stripTrailingZeros());
        snc.setMontoImponible5(param.getMontoImponible5().stripTrailingZeros());
        snc.setMontoImponibleTotal(param.getMontoImponibleTotal().stripTrailingZeros());
        snc.setMontoIva10(param.getMontoIva10().stripTrailingZeros());
        snc.setMontoIva5(param.getMontoIva5().stripTrailingZeros());
        snc.setMontoIvaTotal(param.getMontoIvaTotal().stripTrailingZeros());
        snc.setMontoTotal10(param.getMontoTotal10().stripTrailingZeros());
        snc.setMontoTotal5(param.getMontoTotal5().stripTrailingZeros());
        snc.setMontoTotalExento(param.getMontoTotalExento().stripTrailingZeros());
        snc.setMontoTotalSolicitudNotaCredito(param.getMontoTotalSolicitudNotaCredito().stripTrailingZeros());
        snc.setRazonSocial(param.getRazonSocial());
        snc.setRuc(param.getRuc());
        snc.setIdLocalOrigen(param.getIdLocalOrigen());
        snc.setIdLocalDestino(param.getIdLocalDestino());
        
        create(snc);
        
        for (SolicitudNotaCreditoDetalleParam detalle : param.getSolicitudNotaCreditoDetalles()) {
            detalle.setIdSolicitudNotaCredito(snc.getId());
            facades.getSolicitudNotaCreditoDetalleFacade().create(detalle, userInfo);
        }
        
        return snc;
    }

    @Override
    public SolicitudNotaCredito edit(SolicitudNotaCreditoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        SolicitudNotaCredito snc = find(param.getId());
        
        snc.setAnulado(param.getAnulado());
        snc.setRecibido(param.getRecibido());
        snc.setArchivoEdi(param.getArchivoEdi());
        snc.setGeneradoEdi(param.getGeneradoEdi());
        
        edit(snc);
        
        return snc;
    }
    
    @Override
    public SolicitudNotaCredito updateFileProcess(SolicitudNotaCreditoParam param, UserInfoImpl userInfo) {
        
        if(!validToUpdateFileProcess(param)) {
            return null;
        }
        
        SolicitudNotaCredito item = find(param.getId());
        item.setIdProcesamientoArchivo(param.getIdProcesamientoArchivo());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<SolicitudNotaCredito> find(SolicitudNotaCreditoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(SolicitudNotaCredito.class);
        Root<SolicitudNotaCredito> root = cq.from(SolicitudNotaCredito.class);
        Join<SolicitudNotaCredito, Empresa> empresa = root.join("empresa");
        Join<SolicitudNotaCredito, ProcesamientoArchivo> procesamientoArchivo = root.join("procesamientoArchivo", JoinType.LEFT);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getRecibido() != null) {
            predList.add(qb.equal(root.get("recibido"), param.getRecibido()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }
        
        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }

        if (param.getHashProcesamientoArchivo() != null && !param.getHashProcesamientoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(procesamientoArchivo.get("hash"), param.getHashProcesamientoArchivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<SolicitudNotaCredito> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(SolicitudNotaCreditoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<SolicitudNotaCredito> root = cq.from(SolicitudNotaCredito.class);
        Join<SolicitudNotaCredito, Empresa> empresa = root.join("empresa");
        Join<SolicitudNotaCredito, ProcesamientoArchivo> procesamientoArchivo = root.join("procesamientoArchivo", JoinType.LEFT);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if (param.getIdCliente() != null) {
            predList.add(qb.equal(root.get("idCliente"), param.getIdCliente()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(root.get("idMoneda"), param.getIdMoneda()));
        }
        
        if (param.getRecibido() != null) {
            predList.add(qb.equal(root.get("recibido"), param.getRecibido()));
        }
        
        if (param.getAnulado() != null) {
            predList.add(qb.equal(root.get("anulado"), param.getAnulado()));
        }
        
        if (param.getArchivoEdi() != null) {
            predList.add(qb.equal(root.get("archivoEdi"), param.getArchivoEdi()));
        }
        
        if (param.getGeneradoEdi() != null) {
            predList.add(qb.equal(root.get("generadoEdi"), param.getGeneradoEdi()));
        }
        
        if (param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if (param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }

        if (param.getRazonSocial() != null && !param.getRazonSocial().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("razonSocial")),
                    String.format("%%%s%%", param.getRazonSocial().trim().toUpperCase())));
        }

        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim().toUpperCase())));
        }

        if (param.getIdProcesamientoArchivo() != null) {
            predList.add(qb.equal(root.get("idProcesamientoArchivo"), param.getIdProcesamientoArchivo()));
        }

        if (param.getHashProcesamientoArchivo() != null && !param.getHashProcesamientoArchivo().trim().isEmpty()) {
            predList.add(qb.equal(procesamientoArchivo.get("hash"), param.getHashProcesamientoArchivo().trim()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }

    @Override
    public Long maxId() {
        
        String sql = "select max(id) from facturacion.solicitud_nota_credito";

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
