/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Pais;
import py.com.sepsa.erp.ejb.entities.info.filters.PaisParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Williams Vera
 */
@Local
public interface PaisFacade extends Facade<Pais, PaisParam, UserInfoImpl> {
    
    public Pais find(Integer idEmpresa, Integer id, String codigo);
    
}
