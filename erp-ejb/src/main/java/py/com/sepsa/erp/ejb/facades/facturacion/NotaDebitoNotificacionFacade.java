/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebitoNotificacion;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoNotificacionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Williams Vera
 */
@Local
public interface NotaDebitoNotificacionFacade extends Facade<NotaDebitoNotificacion, NotaDebitoNotificacionParam, UserInfoImpl> {

    public Boolean validToCreate(NotaDebitoNotificacionParam param, boolean validarIdNotaDebito);
    
}