/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.ReporteEmpresa;
import py.com.sepsa.erp.ejb.entities.info.filters.ReporteEmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ReportePojo;
import py.com.sepsa.erp.ejb.facades.ReportUtils.ReporteEmpresaJParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.erp.reporte.jasper.JasperReporteGenerator;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless(name = "ReporteEmpresaFacade", mappedName = "ReporteEmpresaFacade")
@Local(ReporteEmpresaFacade.class)
public class ReporteEmpresaFacadeImpl extends FacadeImpl<ReporteEmpresa, ReporteEmpresaParam> implements ReporteEmpresaFacade {

    public ReporteEmpresaFacadeImpl() {
        super(ReporteEmpresa.class);
    }

    public Boolean validToCreate(ReporteEmpresaParam param) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        ReporteEmpresaParam param1 = new ReporteEmpresaParam();
        param1.setIdReporte(param.getIdReporte());
        param1.setCodigoReporte(param.getCodigoReporte());
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigoEmpresa(param.getCodigoEmpresa());

        ReporteEmpresa item = findFirst(param1);

        if (item != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }

        ReportePojo reporte = facades.getReporteFacade().find(
                param.getIdReporte(), param.getCodigoReporte());

        if (reporte == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el reporte"));
        } else {
            param.setIdReporte(reporte.getId());
            param.setCodigoReporte(reporte.getCodigo());
        }

        if (!isNull(param.getIdEmpresa())
                && !isNullOrEmpty(param.getCodigoEmpresa())) {
            Empresa empresa = facades.getEmpresaFacade().find(
                    param.getIdEmpresa(), param.getCodigoEmpresa(), null);

            if (empresa == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe la empresa"));
            } else if (empresa != null) {
                param.setIdEmpresa(empresa.getId());
                param.setCodigoEmpresa(empresa.getCodigo());
            }
        }

        return !param.tieneErrores();
    }

    public Boolean validToEdit(ReporteEmpresaParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        ReporteEmpresaParam param1 = new ReporteEmpresaParam();
        param1.setId(param.getId());

        ReporteEmpresa item = findFirst(param1);

        if (item == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe un registro"));
        }

        return !param.tieneErrores();
    }

    @Override
    public ReporteEmpresa create(ReporteEmpresaParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param)) {
            return null;
        }

        ReporteEmpresa item = new ReporteEmpresa();
        item.setIdReporte(param.getIdReporte());
        item.setIdEmpresa(param.getIdEmpresa());
        item.setActivo(param.getActivo());
        item.setJasper(param.getJasper());
        item.setFechaInsercion(Calendar.getInstance().getTime());

        create(item);

        return item;
    }

    @Override
    public ReporteEmpresa edit(ReporteEmpresaParam param, UserInfoImpl userInfo) {

        if (!validToEdit(param)) {
            return null;
        }

        ReporteEmpresa item = find(param.getId());
        item.setActivo(param.getActivo());

        edit(item);

        return item;
    }

    @Override
    public List<ReporteEmpresa> find(ReporteEmpresaParam param) {

        String where = "true";

        if (!isNullOrEmpty(param.getCodigoReporte())) {
            where = String.format("%s and r.codigo = '%s'", where, param.getCodigoReporte().trim());
        }

        if (!isNull(param.getIdReporte())) {
            where = String.format("%s and r.id_reporte = %d", where, param.getIdReporte());
        }

        if (!isNull(param.getId())) {
            where = String.format("%s and re.id = %d", where, param.getId());
        }

        if (!isNullOrEmpty(param.getDescripcionReporte())) {
            where = String.format("%s and r.descripcion ilike '%%%s%%'", where, param.getDescripcionReporte().trim());
        }

        if (!isNull(param.getIdEmpresa())) {
            where = String.format("%s and re.id_empresa= %d", where, param.getIdEmpresa());
        }

        if (!isNull(param.getActivo())) {
            where = String.format("%s and re.activo = '%s'", where, param.getActivo());
        }

        String sql = String.format("select re.*"
                + " from info.reporte_empresa re"
                + " join info.reporte r on r.id = re.id_reporte"
                + " left join info.empresa e on e.id = re.id_empresa"
                + " where %s order by re.activo, re.fecha_insercion desc"
                + " offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, ReporteEmpresa.class);

        List<ReporteEmpresa> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(ReporteEmpresaParam param) {

        String where = "true";

        if (!isNullOrEmpty(param.getCodigoReporte())) {
            where = String.format("%s and r.codigo = '%s'", where, param.getCodigoReporte().trim());
        }

        if (!isNull(param.getIdReporte())) {
            where = String.format("%s and r.id_reporte = %d", where, param.getIdReporte());
        }
        
        if (!isNull(param.getId())) {
            where = String.format("%s and re.id = %d", where, param.getId());
        }

        if (!isNullOrEmpty(param.getDescripcionReporte())) {
            where = String.format("%s and r.descripcion ilike '%%%s%%'", where, param.getDescripcionReporte().trim());
        }
        
        if (!isNullOrEmpty(param.getCodigoEmpresa())) {
            where = String.format("%s and e.codigo = '%s'", where, param.getCodigoEmpresa().trim());
        }

        if (!isNull(param.getIdEmpresa())) {
            where = String.format("%s and re.id_empresa= %d", where, param.getIdEmpresa());
        }

        if (!isNull(param.getActivo())) {
            where = String.format("%s and re.activo = '%s'", where, param.getActivo());
        }

        String sql = String.format("select count(re.*)"
                + " from info.reporte_empresa re"
                + " join info.reporte r on r.id = re.id_reporte"
                + " left join info.empresa e on e.id = re.id_empresa"
                + " where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }

    @Override
    public byte[] generateReport(ReporteEmpresaParam param, UserInfoImpl userInfo) throws Exception {

        if (!validToGenerateReport(param)) {
            return null;
        }

        ReporteEmpresa reporteEmpresa = find(param.getId());

        byte[] result = null;

        Closer closer = Closer.instance();

        try {
            result = generateReport(closer, reporteEmpresa, param);
        } finally {
            closer.close();
        }

        return result;
    }

    private byte[] generateReport(Closer closer, ReporteEmpresa reporteEmpresa, ReporteEmpresaParam param) throws Exception {
        byte[] template = Base64.getDecoder().decode(reporteEmpresa.getJasper());
        try ( ByteArrayInputStream bais = new ByteArrayInputStream(template)) {
            ReporteEmpresaJParam params = facades.getReportUtils().getParam(param);

            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(false);
            configuration.setDetectCellType(true);
            //borrar espacio entre filas
            configuration.setRemoveEmptySpaceBetweenRows(true);
            //borrar espacio entre columnas
            configuration.setRemoveEmptySpaceBetweenColumns(true);
            //El siguiente es para decirle que no ignore los bordes de las celdas, y el ultimo es para que no use un fondo blanco, con estos dos parametros jasperreport genera la hoja de excel con los bordes de las celdas.
            configuration.setIgnoreCellBorder(false);
            configuration.setWhitePageBackground(false);
            
            Connection connection = null;
            
            switch (reporteEmpresa.getReporte().getCodigoTipoReporte()) {
                case "JDBC":
                    //Obtenemos una conexion del DataSource
                    Context ctx = new InitialContext();
                    DataSource ds = (DataSource) ctx.lookup("java:/jboss/erp");
                    connection = ds.getConnection();
                    closer.add(connection);
            }
            
            switch (reporteEmpresa.getReporte().getFormato()) {
                case "PDF":
                    return connection == null
                            ? JasperReporteGenerator.exportReportToPdfBytes(params, bais)
                            : JasperReporteGenerator.exportReportToPdfBytes(params, connection, bais);
                    
                case "XLSX":
                    return connection == null
                            ? JasperReporteGenerator.exportReportToXlsxBytes(params, bais, configuration)
                            : JasperReporteGenerator.exportReportToXlsxBytes(params, connection, bais, configuration);
                default:
                    throw new IllegalArgumentException("Formato de reporte no soportado");
            }
        }
    }

    public Boolean validToGenerateReport(ReporteEmpresaParam param) {

        if (!param.isValidToGenerateReport()) {
            return Boolean.FALSE;
        }

        ReporteEmpresa reporteEmpresa = find(param.getId());

        if (reporteEmpresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el reporte de empresa"));
        }

        return !param.tieneErrores();
    }
}
