/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.PeriodoLiquidacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.PeriodoLiquidacionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "PeriodoLiquidacionFacade", mappedName = "PeriodoLiquidacionFacade")
@Local(PeriodoLiquidacionFacade.class)
public class PeriodoLiquidacionFacadeImpl extends FacadeImpl<PeriodoLiquidacion, PeriodoLiquidacionParam> implements PeriodoLiquidacionFacade {

    public PeriodoLiquidacionFacadeImpl() {
        super(PeriodoLiquidacion.class);
    }

    @Override
    public Map<String, String> getMapConditions(PeriodoLiquidacion item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("fecha_inicio", item.getFechaInicio() + "");
        map.put("fecha_fin", item.getFechaFin()+ "");
        map.put("activo", item.getActivo() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(PeriodoLiquidacion item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    @Override
    public PeriodoLiquidacion find(Date date) {
        
        PeriodoLiquidacionParam param = new PeriodoLiquidacionParam();
        param.setFechaInicioHasta(date);
        param.setFechaFinDesde(date);
        param.setActivo('S');
        return findFirst(param);
    }
    
    @Override
    public List<PeriodoLiquidacion> find(PeriodoLiquidacionParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(param.getAno() != null && !param.getAno().isEmpty()) {
            where = String.format(" %s and to_char(fecha_fin, 'yyyy') = '%s'", where, param.getAno());
        }
        
        if(param.getMes() != null && !param.getMes().isEmpty()) {
            where = String.format(" %s and to_char(fecha_fin, 'MM') = '%s'", where, param.getMes());
        }
        
        if(param.getId() != null) {
            where = String.format(" %s and id = %d", where, param.getId());
        }
        
        if(param.getActivo() != null) {
            where = String.format(" %s and activo = '%s'", where, param.getActivo());
        }
        
        if(param.getFechaInicioDesde() != null) {
            where = String.format("%s and fecha_inicio >= '%s'", where, sdf.format(param.getFechaInicioDesde()));
        }
        
        if(param.getFechaInicioHasta() != null) {
            where = String.format("%s and fecha_inicio <= '%s'", where, sdf.format(param.getFechaInicioHasta()));
        }
        
        if(param.getFechaFinDesde() != null) {
            where = String.format("%s and fecha_fin >= '%s'", where, sdf.format(param.getFechaFinDesde()));
        }
        
        if(param.getFechaFinHasta() != null) {
            where = String.format("%s and fecha_fin <= '%s'", where, sdf.format(param.getFechaFinHasta()));
        }
        
        String sql = String.format("select *\n"
                + "from comercial.periodo_liquidacion\n"
                + "where %s\n"
                + "order by fecha_inicio desc offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, PeriodoLiquidacion.class);

        List<PeriodoLiquidacion> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(PeriodoLiquidacionParam param) {

        String where = "true";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        if(param.getAno() != null && !param.getAno().isEmpty()) {
            where = String.format(" %s and to_char(fecha_fin, 'yyyy') = '%s'", where, param.getAno());
        }
        
        if(param.getMes() != null && !param.getMes().isEmpty()) {
            where = String.format(" %s and to_char(fecha_fin, 'MM') = '%s'", where, param.getMes());
        }
        
        if(param.getId() != null) {
            where = String.format(" %s and id = %d", where, param.getId());
        }
        
        if(param.getActivo() != null) {
            where = String.format(" %s and activo = '%s'", where, param.getActivo());
        }
        
        if(param.getFechaInicioDesde() != null) {
            where = String.format("%s and fecha_inicio >= '%s'", where, sdf.format(param.getFechaInicioDesde()));
        }
        
        if(param.getFechaInicioHasta() != null) {
            where = String.format("%s and fecha_inicio <= '%s'", where, sdf.format(param.getFechaInicioHasta()));
        }
        
        if(param.getFechaFinDesde() != null) {
            where = String.format("%s and fecha_fin >= '%s'", where, sdf.format(param.getFechaFinDesde()));
        }
        
        if(param.getFechaFinHasta() != null) {
            where = String.format("%s and fecha_fin <= '%s'", where, sdf.format(param.getFechaFinHasta()));
        }
        
        String sql = String.format("select count(id) "
                + "from comercial.periodo_liquidacion\n"
                + "where %s\n", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);

        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0
                : ((Number)object).longValue();

        return result;
    }
}
