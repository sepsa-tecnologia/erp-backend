/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.MontoTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.ProductoCom;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.comercial.Tarifa;
import py.com.sepsa.erp.ejb.entities.comercial.TipoTarifa;
import py.com.sepsa.erp.ejb.entities.comercial.filters.MontoTarifaParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "MontoTarifaFacade", mappedName = "MontoTarifaFacade")
@Local(MontoTarifaFacade.class)
public class MontoTarifaFacadeImpl extends FacadeImpl<MontoTarifa, MontoTarifaParam> implements MontoTarifaFacade {

    public MontoTarifaFacadeImpl() {
        super(MontoTarifa.class);
    }

    @Override
    public Map<String, String> getMapConditions(MontoTarifa item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("id_tarifa", item.getIdTarifa() + "");
        map.put("cantidad_inicial", item.getCantidadInicial() + "");
        map.put("cantidad_final", item.getCantidadFinal()+ "");
        map.put("activo", item.getActivo() + "");
        map.put("monto", item.getMonto() == null ? "" : item.getMonto().toPlainString());
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(MontoTarifa item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param Parámetros
     * @param validarTarifa Validar tarifa
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(MontoTarifaParam param, boolean validarTarifa) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Tarifa tarifa = facades
                .getTarifaFacade()
                .find(param.getIdTarifa());
        
        if(validarTarifa && tarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la tarifa"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param Parámetros
     * @return Bandera
     */
    @Override
    public Boolean validToEdit(MontoTarifaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MontoTarifa montoTarifa = facades
                .getMontoTarifaFacade()
                .find(param.getId());
        
        if(montoTarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el monto tarifa"));
        }
        
        Tarifa tarifa = facades
                .getTarifaFacade()
                .find(param.getIdTarifa());
        
        if(tarifa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la tarifa"));
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public MontoTarifa create(MontoTarifaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        MontoTarifa tarifaRango = new MontoTarifa();
        tarifaRango.setCantidadFinal(param.getCantidadFinal());
        tarifaRango.setCantidadInicial(param.getCantidadInicial());
        tarifaRango.setIdTarifa(param.getIdTarifa());
        tarifaRango.setActivo(param.getActivo());
        tarifaRango.setMonto(param.getMonto());
        
        create(tarifaRango);
        
        return tarifaRango;
    }
    
    @Override
    public MontoTarifa edit(MontoTarifaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MontoTarifa tarifaRango = find(param.getId());
        tarifaRango.setCantidadFinal(param.getCantidadFinal());
        tarifaRango.setCantidadInicial(param.getCantidadInicial());
        tarifaRango.setIdTarifa(param.getIdTarifa());
        tarifaRango.setActivo(param.getActivo());
        tarifaRango.setMonto(param.getMonto());
        
        edit(tarifaRango);
        
        return tarifaRango;
    }

    @Override
    public List<MontoTarifa> find(MontoTarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MontoTarifa.class);
        Root<MontoTarifa> root = cq.from(MontoTarifa.class);
        Join<MontoTarifa, Tarifa> t = root.join("tarifa");
        Join<Tarifa, Servicio> s = t.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");
        Join<Tarifa, TipoTarifa> tt = t.join("tipoTarifa");
        Join<Tarifa, Moneda> m = t.join("moneda");
        
        cq.select(root);
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getIdTarifa() != null) {
            predList.add(qb.equal(root.get("idTarifa"), param.getIdTarifa()));
        }
        
        if (param.getCantidadInicialDesde()!= null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Integer>get("cantidadInicial"), param.getCantidadInicialDesde()));
        }
        
        if (param.getCantidadInicialHasta()!= null) {
            predList.add(qb.lessThanOrEqualTo(root.<Integer>get("cantidadInicial"), param.getCantidadInicialHasta()));
        }
        
        if (param.getCantidadFinalDesde()!= null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Integer>get("cantidadFinal"), param.getCantidadFinalDesde()));
        }
        
        if (param.getCantidadFinalHasta()!= null) {
            predList.add(qb.lessThanOrEqualTo(root.<Integer>get("cantidadFinal"), param.getCantidadFinalHasta()));
        }
        
        if (param.getEstadoTarifa() != null) {
            predList.add(qb.equal(t.get("estado"), param.getEstadoTarifa()));
        }

        if (param.getTarifa() != null && !param.getTarifa().trim().isEmpty()) {
            predList.add(qb.like(t.<String>get("descripcion"),
                    String.format("%%%s%%", param.getTarifa().trim())));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(m.get("id"), param.getIdMoneda()));
        }
        
        if (param.getIdTipoTarifa() != null) {
            predList.add(qb.equal(tt.get("id"), param.getIdTipoTarifa()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.asc(t.get("activo")),
                qb.desc(t.get("id")),
                qb.asc(root.get("activo")),
                qb.asc(root.get("cantidadInicial")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<MontoTarifa> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MontoTarifaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<MontoTarifa> root = cq.from(MontoTarifa.class);
        Join<MontoTarifa, Tarifa> t = root.join("tarifa");
        Join<Tarifa, Servicio> s = t.join("servicio");
        Join<Servicio, ProductoCom> p = s.join("producto");
        Join<Tarifa, TipoTarifa> tt = t.join("tipoTarifa");
        Join<Tarifa, Moneda> m = t.join("moneda");
        
        cq.select(qb.count(root.get("id")));
        
        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getIdTarifa() != null) {
            predList.add(qb.equal(root.get("idTarifa"), param.getIdTarifa()));
        }
        
        if (param.getCantidadInicialDesde()!= null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Integer>get("cantidadInicial"), param.getCantidadInicialDesde()));
        }
        
        if (param.getCantidadInicialHasta()!= null) {
            predList.add(qb.lessThanOrEqualTo(root.<Integer>get("cantidadInicial"), param.getCantidadInicialHasta()));
        }
        
        if (param.getCantidadFinalDesde()!= null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Integer>get("cantidadFinal"), param.getCantidadFinalDesde()));
        }
        
        if (param.getCantidadFinalHasta()!= null) {
            predList.add(qb.lessThanOrEqualTo(root.<Integer>get("cantidadFinal"), param.getCantidadFinalHasta()));
        }
        
        if (param.getEstadoTarifa() != null) {
            predList.add(qb.equal(t.get("estado"), param.getEstadoTarifa()));
        }

        if (param.getTarifa() != null && !param.getTarifa().trim().isEmpty()) {
            predList.add(qb.like(t.<String>get("descripcion"),
                    String.format("%%%s%%", param.getTarifa().trim())));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(p.get("id"), param.getIdProducto()));
        }
        
        if (param.getIdServicio() != null) {
            predList.add(qb.equal(s.get("id"), param.getIdServicio()));
        }
        
        if (param.getIdMoneda() != null) {
            predList.add(qb.equal(m.get("id"), param.getIdMoneda()));
        }
        
        if (param.getIdTipoTarifa() != null) {
            predList.add(qb.equal(tt.get("id"), param.getIdTipoTarifa()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
