/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import org.jboss.resteasy.annotations.providers.multipart.PartType;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de nota crédito compra
 *
 * @author Jonathan D. Bernal Fernández
 */
public class NotaCreditoParam extends CommonParam {

    public NotaCreditoParam() {
        this.notaCreditoDetalles = new ArrayList<>();
    }

    public NotaCreditoParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de local talonario
     */
    @QueryParam("idLocalTalonario")
    protected Integer idLocalTalonario;

    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;

    /**
     * Identificador de talonario
     */
    @QueryParam("idTalonario")
    private Integer idTalonario;

    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;

    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;

    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;

    /**
     * Identificador de naturaleza del cliente
     */
    @QueryParam("idNaturalezaCliente")
    private Integer idNaturalezaCliente;

    /**
     * Código de naturaleza del cliente
     */
    @QueryParam("codigoNaturalezaCliente")
    private String codigoNaturalezaCliente;

    /**
     * Identificador de usuario
     */
    @QueryParam("idUsuario")
    private Integer idUsuario;

    /**
     * Identificador de local
     */
    @QueryParam("idLocal")
    private Integer idLocal;

    /**
     * Identificador de locales
     */
    @QueryParam("idLocales")
    private List<Integer> idLocales;

    /**
     * Identificador externo de local
     */
    @QueryParam("idExternoLocal")
    private String idExternoLocal;

    /**
     * Nro de nota de crédito
     */
    @QueryParam("nroNotaCredito")
    private String nroNotaCredito;

    /**
     * Timbrado
     */
    @QueryParam("timbrado")
    private String timbrado;

    /**
     * Establecimiento
     */
    @QueryParam("establecimiento")
    private String establecimiento;

    /**
     * Punto de expedición
     */
    @QueryParam("puntoExpedicion")
    private String puntoExpedicion;

    /**
     * Fecha factura
     */
    @QueryParam("fecha")
    private Date fecha;

    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;

    /**
     * Fecha Hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;

    /**
     * Fecha insercion desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha insercion Hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;

    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;

    /**
     * Dirección
     */
    @QueryParam("direccion")
    private String direccion;

    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;

    /**
     * Telefono
     */
    @QueryParam("telefono")
    private String telefono;

    /**
     * Email
     */
    @QueryParam("email")
    private String email;

    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;

    /**
     * Impreso
     */
    @QueryParam("impreso")
    private Character impreso;

    /**
     * Entregado
     */
    @QueryParam("entregado")
    private Character entregado;

    /**
     * Fecha de entrega
     */
    @QueryParam("fechaEntrega")
    private Date fechaEntrega;

    /**
     * Observación
     */
    @QueryParam("observacion")
    private String observacion;

    /**
     * Monto iva 5
     */
    @QueryParam("montoIva5")
    private BigDecimal montoIva5;

    /**
     * Monto imponible 5
     */
    @QueryParam("montoImponible5")
    private BigDecimal montoImponible5;

    /**
     * Monto total 5
     */
    @QueryParam("montoTotal5")
    private BigDecimal montoTotal5;

    /**
     * Monto iva 10
     */
    @QueryParam("montoIva10")
    private BigDecimal montoIva10;

    /**
     * Monto imponible 10
     */
    @QueryParam("montoImponible10")
    private BigDecimal montoImponible10;

    /**
     * Monto total 10
     */
    @QueryParam("montoTotal10")
    private BigDecimal montoTotal10;

    /**
     * Monto total exento
     */
    @QueryParam("montoTotalExento")
    private BigDecimal montoTotalExento;

    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;

    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;

    /**
     * Monto total nota de crédito
     */
    @QueryParam("montoTotalNotaCredito")
    private BigDecimal montoTotalNotaCredito;

    /**
     * Monto total en guaranies
     */
    @QueryParam("montoTotalGuaranies")
    private BigDecimal montoTotalGuaranies;

    /**
     * CDC
     */
    @QueryParam("cdc")
    private String cdc;

    /**
     * Digital
     */
    @QueryParam("digital")
    private Character digital;

    /**
     * Archivo EDI
     */
    @QueryParam("archivoEdi")
    private Character archivoEdi;

    /**
     * Archivo SET
     */
    @QueryParam("archivoSet")
    private Character archivoSet;

    /**
     * Generado EDI
     */
    @QueryParam("generadoEdi")
    private Character generadoEdi;

    /**
     * Generado SET
     */
    @QueryParam("generadoSet")
    private Character generadoSet;

    /**
     * Estado sincronizado
     */
    @QueryParam("estadoSincronizado")
    private Character estadoSincronizado;

    /**
     * Cod seguridad
     */
    @QueryParam("codSeguridad")
    private String codSeguridad;

    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;

    /**
     * Código de motivo anulación
     */
    @QueryParam("codigoMotivoAnulacion")
    private String codigoMotivoAnulacion;

    /**
     * Motivo anulación
     */
    @QueryParam("motivoAnulacion")
    private String motivoAnulacion;

    /**
     * Observacion anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;

    /**
     * Identificador de encargado
     */
    @QueryParam("idEncargado")
    private Integer idEncargado;

    /**
     * Identificador de procesamiento
     */
    @QueryParam("idProcesamiento")
    private Integer idProcesamiento;

    /**
     * Identificador de motivo de emisión
     */
    @QueryParam("idMotivoEmision")
    private Integer idMotivoEmision;

    /**
     * Identificador de motivo de emisión interno
     */
    @QueryParam("idMotivoEmisionInterno")
    private Integer idMotivoEmisionInterno;

    /**
     * Código de motivo de emisión interno
     */
    @QueryParam("codigoMotivoEmisionInterno")
    private String codigoMotivoEmisionInterno;

    /**
     * Nro de casa
     */
    @QueryParam("nroCasa")
    private Integer nroCasa;

    /**
     * Identificador de departamento
     */
    @QueryParam("idDepartamento")
    private Integer idDepartamento;

    /**
     * Identificador de distrito
     */
    @QueryParam("idDistrito")
    private Integer idDistrito;

    /**
     * Identificador de ciudad
     */
    @QueryParam("idCiudad")
    private Integer idCiudad;

    /**
     * Imprimir original
     */
    @QueryParam("imprimirOriginal")
    private Boolean imprimirOriginal;

    /**
     * Imprimir duplicado
     */
    @QueryParam("imprimirDuplicado")
    private Boolean imprimirDuplicado;

    /**
     * Imprimir triplicado
     */
    @QueryParam("imprimirTriplicado")
    private Boolean imprimirTriplicado;

    /**
     * Ignorar periodo de anulación
     */
    @QueryParam("ignorarPeriodoAnulacion")
    private Boolean ignorarPeriodoAnulacion;
    /**
     * Bandera de formato ticket
     */
    @QueryParam("ticket")
    private Boolean ticket = Boolean.FALSE;

    @FormParam("uploadedFile")
    @PartType("application/octet-stream")
    private byte[] uploadedFileBytes;

    /**
     * Bandera nro nota de crédito descendente
     */
    private Boolean nroNotaCreditoDescendente = Boolean.TRUE;

    /**
     * Bandera identificador descendente
     */
    private Boolean idDescendente = Boolean.TRUE;

    /**
     * Bandera para obtener kude del siediApi
     */
    @QueryParam("siediApi")
    private Boolean siediApi = Boolean.FALSE;

    /**
     * Monto total en guaranies
     */
    @QueryParam("montoTotalDescuentoGlobal")
    private BigDecimal montoTotalDescuentoGlobal;

    /**
     * Monto total en guaranies
     */
    @QueryParam("porcentajeDescuentoGlobal")
    private BigDecimal porcentajeDescuentoGlobal;

    /**
     * Identificador del tipo de cambio
     */
    @QueryParam("idTipoCambio")
    private Integer idTipoCambio;

    /**
     * Variable para permitir que se utilice sgte nro de nota credito
     */
    @QueryParam("sgteNumeroNC")
    private Boolean sgteNumeroNC = true;

    /**
     * Detalles
     */
    private List<NotaCreditoDetalleParam> notaCreditoDetalles;

    /**
     * NotaCredito Notificación
     */
    private List<NotaCreditoNotificacionParam> notaCreditoNotificaciones;

    /**
     * Nota Credito parametros adicionales
     */
    private List<NotaCreditoParametroAdicionalParam> parametrosAdicionales;

    public boolean datosCrearValido() {

        limpiarErrores();

        if (isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de crédito es digital o no"));
        }

        return !tieneErrores();
    }

    public boolean isValidToCreateFromInvoice() {
        limpiarErrores();

        if (isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de crédito es digital o no"));
        }

        if (isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idTalonario) && isNullOrEmpty(timbrado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el timbrado o identificador de talonario"));
        }

        if (isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }

        if (isNull(idNaturalezaCliente) && isNullOrEmpty(codigoNaturalezaCliente)) {
            codigoNaturalezaCliente = "CONTRIBUYENTE";
        }

        if (isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de la nota de crédito"));
        } else {

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);

            if (cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("La fecha de la nota de crédito no puede ser posterior a la fecha actual"));
            }
        }

        if (isNull(anulado)) {
            anulado = 'N';
        }

        if (isNull(impreso)) {
            impreso = 'N';
        }

        if (isNull(entregado)) {
            entregado = 'N';
        }

        if (isNull(archivoEdi)) {
            archivoEdi = 'N';
        }

        if (isNull(archivoSet)) {
            archivoSet = 'N';
        }

        if (isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de crédito es digital o no"));
        } else {
            estadoSincronizado = digital.equals('S') && archivoSet.equals('S') ? 'N' : 'S';
        }

        if (!isNull(digital) && isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            if (digital.equals('S')) {
                codigoEstado = "PENDIENTE";
            } else {
                codigoEstado = "APROBADO";
            }
        }

        if (isNull(montoIva5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 5%"));
        }

        if (isNull(montoImponible5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 5%"));
        }

        if (isNull(montoTotal5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 5%"));
        }

        if (isNull(montoIva10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 10%"));
        }

        if (isNull(montoImponible10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 10%"));
        }

        if (isNull(montoTotal10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 10%"));
        }

        if (isNull(montoTotalExento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excento"));
        }

        if (isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA"));
        }

        if (isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }

        if (isNull(montoTotalNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total de la nota de crédito"));
        }

        if (isNull(montoTotalGuaranies)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total en guaranies"));
        }

        if (isNull(idEncargado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el encargado"));
        }

        if (isNullOrEmpty(nroNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de la nota de crédito"));
        } else {
            if (nroNotaCredito.length() >= 7) {
                establecimiento = nroNotaCredito.substring(0, 3);
                puntoExpedicion = nroNotaCredito.substring(4, 7);
            } else {
                establecimiento = "0";
                puntoExpedicion = "0";
            }
        }

        if (isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del cliente"));
        }

        if (isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el ruc/ci"));
        }

        if (!isNull(digital) && digital.equals('S')) {

            if (isNull(idMotivoEmisionInterno) && isNullOrEmpty(codigoMotivoEmisionInterno)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe de indicar el identificador/código de motivo de emisión interno"));
            }

            if (!isNullOrEmpty(direccion)) {

                if (isNull(nroCasa)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el nro de casa del cliente"));
                }

                if (isNull(idDepartamento)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el departamento del cliente"));
                }

                if (isNull(idDistrito)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar el distrito del cliente"));
                }

                if (isNull(idCiudad)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar la ciudad del cliente"));
                }
            }
        }

        if (isNullOrEmpty(notaCreditoDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la NC"));
        } else {
            for (NotaCreditoDetalleParam detalle : notaCreditoDetalles) {
                detalle.setIdNotaCredito(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }

        if (isNull(montoTotalDescuentoGlobal)) {
            this.montoTotalDescuentoGlobal = new BigDecimal("0");
        }

        if (isNull(porcentajeDescuentoGlobal)) {
            this.porcentajeDescuentoGlobal = new BigDecimal("0");
        }

        if (!isNull(parametrosAdicionales)) {
            for (NotaCreditoParametroAdicionalParam detalle : parametrosAdicionales) {
                detalle.setIdNotaCredito(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {

        List<MensajePojo> result = new ArrayList<>();

        if (notaCreditoDetalles != null) {
            for (NotaCreditoDetalleParam detalle : notaCreditoDetalles) {
                result.addAll(detalle.getErrores());
            }
        }

        if (notaCreditoNotificaciones != null) {
            for (NotaCreditoNotificacionParam detalle : notaCreditoNotificaciones) {
                result.addAll(detalle.getErrores());
            }
        }

        if (parametrosAdicionales != null) {
            for (NotaCreditoParametroAdicionalParam detalle : parametrosAdicionales) {
                result.addAll(detalle.getErrores());
            }
        }

        return result;
    }

    public boolean isValidToGetReporteVenta() {

        if (isNull(idCliente) && isNull(fechaDesde) && isNull(fechaDesde)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe elegir al menos un cliente o un rango de fechas"));
        } else {
            if (!isNull(fechaDesde) && isNull(fechaHasta)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar la fecha hasta"));
            } else {
                if (isNull(fechaDesde) && !isNull(fechaHasta)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar la fecha desde"));
                }
            }
        }

        if (!isNull(fechaDesde) && !isNull(fechaHasta)) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaDesde);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            fechaDesde = cal.getTime();

            Calendar calend = Calendar.getInstance();
            calend.setTime(fechaHasta);
            calend.set(Calendar.HOUR_OF_DAY, 23);
            calend.set(Calendar.MINUTE, 59);
            calend.set(Calendar.SECOND, 59);
            fechaHasta = calend.getTime();

        }

        nroNotaCreditoDescendente = Boolean.FALSE;
        idDescendente = Boolean.FALSE;

        return !tieneErrores();
    }

    public boolean isValidToCancel() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito"));
        }

        if (isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identiifcador de motivo de anulación"));
        }

        return !tieneErrores();
    }

    public boolean isValidToGetPdf() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito"));
        }

        if (imprimirOriginal == null) {
            imprimirOriginal = Boolean.FALSE;
        }

        if (imprimirDuplicado == null) {
            imprimirDuplicado = Boolean.FALSE;
        }

        if (imprimirTriplicado == null) {
            imprimirTriplicado = Boolean.FALSE;
        }

        if (!imprimirOriginal
                && !imprimirDuplicado
                && !imprimirTriplicado) {
            imprimirOriginal = Boolean.TRUE;
        }

        if (ticket == null) {
            ticket = Boolean.FALSE;
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito"));
        }

        if (isNull(entregado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de la nota de crédito"));
        }

        if (isNull(impreso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de impresión de la nota de crédito"));
        }

        if (isNull(archivoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo EDI"));
        }

        if (isNull(archivoSet)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un archivo SET"));
        }

        if (isNull(generadoEdi)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el archivo EDI esta generado"));
        }

        if (isNull(generadoSet)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el archivo SET esta generado"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getImpreso() {
        return impreso;
    }

    public void setImpreso(Character impreso) {
        this.impreso = impreso;
    }

    public Character getEntregado() {
        return entregado;
    }

    public void setEntregado(Character entregado) {
        this.entregado = entregado;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getDigital() {
        return digital;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public String getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(String codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public void setIdMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
    }

    public Integer getIdMotivoEmisionInterno() {
        return idMotivoEmisionInterno;
    }

    public Integer getNroCasa() {
        return nroCasa;
    }

    public void setNroCasa(Integer nroCasa) {
        this.nroCasa = nroCasa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public void setNotaCreditoDetalles(List<NotaCreditoDetalleParam> notaCreditoDetalles) {
        this.notaCreditoDetalles = notaCreditoDetalles;
    }

    public List<NotaCreditoDetalleParam> getNotaCreditoDetalles() {
        return notaCreditoDetalles;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public void setCodigoNaturalezaCliente(String codigoNaturalezaCliente) {
        this.codigoNaturalezaCliente = codigoNaturalezaCliente;
    }

    public String getCodigoNaturalezaCliente() {
        return codigoNaturalezaCliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getMontoTotalGuaranies() {
        return montoTotalGuaranies;
    }

    public void setMontoTotalGuaranies(BigDecimal montoTotalGuaranies) {
        this.montoTotalGuaranies = montoTotalGuaranies;
    }

    public void setIdEncargado(Integer idEncargado) {
        this.idEncargado = idEncargado;
    }

    public Integer getIdEncargado() {
        return idEncargado;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public Boolean getImprimirOriginal() {
        return imprimirOriginal;
    }

    public void setImprimirOriginal(Boolean imprimirOriginal) {
        this.imprimirOriginal = imprimirOriginal;
    }

    public Boolean getImprimirDuplicado() {
        return imprimirDuplicado;
    }

    public void setImprimirDuplicado(Boolean imprimirDuplicado) {
        this.imprimirDuplicado = imprimirDuplicado;
    }

    public Boolean getImprimirTriplicado() {
        return imprimirTriplicado;
    }

    public void setImprimirTriplicado(Boolean imprimirTriplicado) {
        this.imprimirTriplicado = imprimirTriplicado;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public void setNroNotaCreditoDescendente(Boolean nroNotaCreditoDescendente) {
        this.nroNotaCreditoDescendente = nroNotaCreditoDescendente;
    }

    public Boolean getNroNotaCreditoDescendente() {
        return nroNotaCreditoDescendente;
    }

    public Boolean getIdDescendente() {
        return idDescendente;
    }

    public void setIdDescendente(Boolean idDescendente) {
        this.idDescendente = idDescendente;
    }

    public void setIdLocalTalonario(Integer idLocalTalonario) {
        this.idLocalTalonario = idLocalTalonario;
    }

    public Integer getIdLocalTalonario() {
        return idLocalTalonario;
    }

    public Character getArchivoEdi() {
        return archivoEdi;
    }

    public void setArchivoEdi(Character archivoEdi) {
        this.archivoEdi = archivoEdi;
    }

    public Character getArchivoSet() {
        return archivoSet;
    }

    public void setArchivoSet(Character archivoSet) {
        this.archivoSet = archivoSet;
    }

    public Character getGeneradoEdi() {
        return generadoEdi;
    }

    public void setGeneradoEdi(Character generadoEdi) {
        this.generadoEdi = generadoEdi;
    }

    public Character getGeneradoSet() {
        return generadoSet;
    }

    public void setGeneradoSet(Character generadoSet) {
        this.generadoSet = generadoSet;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIgnorarPeriodoAnulacion(Boolean ignorarPeriodoAnulacion) {
        this.ignorarPeriodoAnulacion = ignorarPeriodoAnulacion;
    }

    public Boolean getIgnorarPeriodoAnulacion() {
        return ignorarPeriodoAnulacion;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setEstadoSincronizado(Character estadoSincronizado) {
        this.estadoSincronizado = estadoSincronizado;
    }

    public Character getEstadoSincronizado() {
        return estadoSincronizado;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Integer idLocal) {
        this.idLocal = idLocal;
    }

    public List<Integer> getIdLocales() {
        return idLocales;
    }

    public void setIdLocales(List<Integer> idLocales) {
        this.idLocales = idLocales;
    }

    public String getIdExternoLocal() {
        return idExternoLocal;
    }

    public void setIdExternoLocal(String idExternoLocal) {
        this.idExternoLocal = idExternoLocal;
    }

    public String getCodigoMotivoEmisionInterno() {
        return codigoMotivoEmisionInterno;
    }

    public void setCodigoMotivoEmisionInterno(String codigoMotivoEmisionInterno) {
        this.codigoMotivoEmisionInterno = codigoMotivoEmisionInterno;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public String getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setCodigoMotivoAnulacion(String codigoMotivoAnulacion) {
        this.codigoMotivoAnulacion = codigoMotivoAnulacion;
    }

    public String getCodigoMotivoAnulacion() {
        return codigoMotivoAnulacion;
    }

    public Boolean getTicket() {
        return ticket;
    }

    public void setTicket(Boolean ticket) {
        this.ticket = ticket;
    }

    public String getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getPuntoExpedicion() {
        return puntoExpedicion;
    }

    public void setPuntoExpedicion(String puntoExpedicion) {
        this.puntoExpedicion = puntoExpedicion;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public Boolean getSiediApi() {
        return siediApi;
    }

    public void setSiediApi(Boolean siediApi) {
        this.siediApi = siediApi;
    }

    public BigDecimal getMontoTotalDescuentoGlobal() {
        return montoTotalDescuentoGlobal;
    }

    public void setMontoTotalDescuentoGlobal(BigDecimal montoTotalDescuentoGlobal) {
        this.montoTotalDescuentoGlobal = montoTotalDescuentoGlobal;
    }

    public BigDecimal getPorcentajeDescuentoGlobal() {
        return porcentajeDescuentoGlobal;
    }

    public void setPorcentajeDescuentoGlobal(BigDecimal porcentajeDescuentoGlobal) {
        this.porcentajeDescuentoGlobal = porcentajeDescuentoGlobal;
    }

    public Integer getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(Integer idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public byte[] getUploadedFileBytes() {
        return uploadedFileBytes;
    }

    public void setUploadedFileBytes(byte[] uploadedFileBytes) {
        this.uploadedFileBytes = uploadedFileBytes;
    }

    public Boolean getSgteNumeroNC() {
        return sgteNumeroNC;
    }

    public void setSgteNumeroNC(Boolean sgteNumeroNC) {
        this.sgteNumeroNC = sgteNumeroNC;
    }

    public List<NotaCreditoNotificacionParam> getNotaCreditoNotificaciones() {
        return notaCreditoNotificaciones;
    }

    public void setNotaCreditoNotificaciones(List<NotaCreditoNotificacionParam> notaCreditoNotificaciones) {
        this.notaCreditoNotificaciones = notaCreditoNotificaciones;
    }

    public List<NotaCreditoParametroAdicionalParam> getParametrosAdicionales() {
        return parametrosAdicionales;
    }

    public void setParametrosAdicionales(List<NotaCreditoParametroAdicionalParam> parametrosAdicionales) {
        this.parametrosAdicionales = parametrosAdicionales;
    }

}
