/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.ConfigAprobacion;
import py.com.sepsa.erp.ejb.entities.info.TipoEtiqueta;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfigAprobacionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "ConfigAprobacionFacade", mappedName = "ConfigAprobacionFacade")
@Local(ConfigAprobacionFacade.class)
public class ConfigAprobacionFacadeImpl extends FacadeImpl<ConfigAprobacion, ConfigAprobacionParam> implements ConfigAprobacionFacade {
    
    public ConfigAprobacionFacadeImpl() {
        super(ConfigAprobacion.class);
    }
    
    /**
     * Verifica si el objeto es válido para creear
     * @param param parametros
     * @return Bandera
     */
    public Boolean validToCreate(ConfigAprobacionParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoEtiqueta tipoEtiqueta = facades
                .getTipoEtiquetaFacade()
                .find(param.getIdTipoEtiqueta());
        
        if(tipoEtiqueta == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de etiqueta"));
        } else {
            param.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        }
        
        ConfigAprobacionParam param1 = new ConfigAprobacionParam();
        param1.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe una configuración de aprobación para el tipo de etiqueta"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parametros
     * @return Bandera
     */
    public Boolean validToEdit(ConfigAprobacionParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        ConfigAprobacion configAprobacion = find(param.getId());
        
        if(configAprobacion == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la configuración de aprobación"));
        }
        
        TipoEtiqueta tipoEtiqueta = facades
                .getTipoEtiquetaFacade()
                .find(param.getIdTipoEtiqueta());
        
        if(tipoEtiqueta == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de etiqueta"));
        } else {
            param.setIdTipoEtiqueta(param.getId());
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Crea una instancia de configuracion aprobación
     * @param param parametros
     * @return ConfigAprobacion
     */
    @Override
    public ConfigAprobacion create(ConfigAprobacionParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        ConfigAprobacion result = new ConfigAprobacion();
        result.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        result.setDescripcion(param.getDescripcion().trim());
        create(result);
        
        return result;
    }
    
    /**
     * Edita una instancia de configuracion aprobación
     * @param param parametros
     * @return ConfigAprobacion
     */
    @Override
    public ConfigAprobacion edit(ConfigAprobacionParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ConfigAprobacion result = find(param.getId());
        result.setIdTipoEtiqueta(param.getIdTipoEtiqueta());
        result.setDescripcion(param.getDescripcion().trim());
        edit(result);
        
        return result;
    }
    
    @Override
    public List<ConfigAprobacion> find(ConfigAprobacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(ConfigAprobacion.class);
        Root<ConfigAprobacion> root = cq.from(ConfigAprobacion.class);
        Join<ConfigAprobacion, TipoEtiqueta> tipoEtiqueta = root.join("tipoEtiqueta");
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdTipoEtiqueta() != null) {
            predList.add(qb.equal(tipoEtiqueta.get("id"), param.getIdTipoEtiqueta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<ConfigAprobacion> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(ConfigAprobacionParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ConfigAprobacion> root = cq.from(ConfigAprobacion.class);
        Join<ConfigAprobacion, TipoEtiqueta> tipoEtiqueta = root.join("tipoEtiqueta");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdTipoEtiqueta() != null) {
            predList.add(qb.equal(tipoEtiqueta.get("id"), param.getIdTipoEtiqueta()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);
        
        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0 
                : ((Number)object).longValue();
        
        return result;
    }
}
