/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set.filters;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class ProcesamientoParam extends CommonParam {

    public ProcesamientoParam() {
    }

    public ProcesamientoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de envío
     */
    @QueryParam("idEnvio")
    private BigInteger idEnvio;
    
    /**
     * Número de transacción
     */
    @QueryParam("nroTransaccion")
    private BigInteger nroTransaccion;
    
    /**
     * Fecha de procesamiento
     */
    @QueryParam("fechaProcesamiento")
    private Date fechaProcesamiento;
    
    /**
     * Fecha de procesamiento desde
     */
    @QueryParam("fechaProcesamientoDesde")
    private Date fechaProcesamientoDesde;
    
    /**
     * Fecha de procesamiento hasta
     */
    @QueryParam("fechaProcesamientoHasta")
    private Date fechaProcesamientoHasta;
    
    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Estado de resultado
     */
    @QueryParam("estadoResultado")
    private String estadoResultado;
    
    /**
     * Lista de detalle de procesamiento
     */
    private List<DetalleProcesamientoParam> detalleProcesamientos;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEnvio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de envío"));
        }
        
        if(isNull(fechaProcesamiento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de procesamiento"));
        }
        
        if(isNullOrEmpty(estadoResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de resultado"));
        }
        
        if(!isNullOrEmpty(detalleProcesamientos)) {
            for (DetalleProcesamientoParam detalle : detalleProcesamientos) {
                detalle.setIdProcesamiento(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento"));
        }
        
        if(isNull(idEnvio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de envío"));
        }
        
        if(isNull(fechaProcesamiento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de procesamiento"));
        }
        
        if(isNullOrEmpty(estadoResultado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de resultado"));
        }
        
        if(isNullOrEmpty(detalleProcesamientos)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de procesamiento"));
        } else {
            for (DetalleProcesamientoParam detalle : detalleProcesamientos) {
                detalle.setIdProcesamiento(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(detalleProcesamientos)) {
            for (DetalleProcesamientoParam item : detalleProcesamientos) {
                list.addAll(item.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public BigInteger getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(BigInteger idEnvio) {
        this.idEnvio = idEnvio;
    }

    public BigInteger getNroTransaccion() {
        return nroTransaccion;
    }

    public void setNroTransaccion(BigInteger nroTransaccion) {
        this.nroTransaccion = nroTransaccion;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public String getEstadoResultado() {
        return estadoResultado;
    }

    public void setEstadoResultado(String estadoResultado) {
        this.estadoResultado = estadoResultado;
    }

    public List<DetalleProcesamientoParam> getDetalleProcesamientos() {
        return detalleProcesamientos;
    }

    public void setDetalleProcesamientos(List<DetalleProcesamientoParam> detalleProcesamientos) {
        this.detalleProcesamientos = detalleProcesamientos;
    }

    public Date getFechaProcesamientoDesde() {
        return fechaProcesamientoDesde;
    }

    public void setFechaProcesamientoDesde(Date fechaProcesamientoDesde) {
        this.fechaProcesamientoDesde = fechaProcesamientoDesde;
    }

    public Date getFechaProcesamientoHasta() {
        return fechaProcesamientoHasta;
    }

    public void setFechaProcesamientoHasta(Date fechaProcesamientoHasta) {
        this.fechaProcesamientoHasta = fechaProcesamientoHasta;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }
}
