/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.logging.log4j.util.Strings;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.info.Producto;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.EstadoPojo;
import py.com.sepsa.erp.ejb.entities.inventario.DepositoLogistico;
import py.com.sepsa.erp.ejb.entities.inventario.Inventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "InventarioFacade", mappedName = "InventarioFacade")
@javax.ejb.Local(InventarioFacade.class)
public class InventarioFacadeImp extends FacadeImpl<Inventario, InventarioParam> implements InventarioFacade {

    public InventarioFacadeImp() {
        super(Inventario.class);
    }
    
    public Boolean validToCreate(InventarioParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        DepositoLogisticoParam dlparam = new DepositoLogisticoParam();
        dlparam.setId(param.getIdDepositoLogistico());
        Long dlsize = facades.getDepositoLogisticoFacade().findSize(dlparam);
        
        if(dlsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el depósito logistico"));
        }
        
        ProductoParam pparam = new ProductoParam();
        pparam.setId(param.getIdProducto());
        Long psize = facades.getProductoFacade().findSize(pparam);
        
        if(psize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el producto"));
        }
        
        EstadoPojo estado = facades.getEstadoFacade().find(param.getIdEstado(),
                param.getCodigoEstado(), "INVENTARIO");
        
        if(estado == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el estado"));
        } else {
            param.setIdEstado(estado.getId());
            param.setCodigoEstado(estado.getCodigo());
        }
        
        InventarioParam param1 = new InventarioParam();
        param1.setIdDepositoLogistico(param.getIdDepositoLogistico());
        param1.setIdProducto(param.getIdProducto());
        param1.setIdEstado(param.getIdEstado());
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro"));
        }
        
        if(!isNullOrEmpty(param.getInventarioDetalles())) {
            for (InventarioDetalleParam detalle : param.getInventarioDetalles()) {
                facades.getInventarioDetalleFacade().validToCreate(detalle, false);
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(InventarioParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        InventarioParam param1 = new InventarioParam();
        param1.setIdDepositoLogistico(param.getIdDepositoLogistico());
        param1.setIdProducto(param.getIdProducto());
        param1.setIdEstado(param.getIdEstado());
        InventarioPojo inventario = findFirstPojo(param1);
        
        if(inventario == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro de inventario"));
        } else {
            param.setId(inventario.getId());
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Inventario create(InventarioParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Inventario result = new Inventario();
        result.setIdEmpresa(userInfo.getIdEmpresa());
        result.setIdDepositoLogistico(param.getIdDepositoLogistico());
        result.setIdProducto(param.getIdProducto());
        result.setIdEstado(param.getIdEstado());
        result.setCantidad(param.getCantidad());
        
        create(result);
        
        if(!isNullOrEmpty(param.getInventarioDetalles())) {
            for (InventarioDetalleParam detalle : param.getInventarioDetalles()) {
                detalle.setIdInventario(result.getId());
                facades.getInventarioDetalleFacade().create(detalle, userInfo);
            }
        }
        
        return result;
    }

    @Override
    public Inventario edit(InventarioParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Inventario result = find(param.getId());
        result.setCantidad(param.getCantidad());
        
        edit(result);
        
        return result;
    }

    @Override
    public List<InventarioPojo> findPojo(InventarioParam param) {

        AbstractFind find = new AbstractFind(InventarioPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idEmpresa"),
                        getPath("empresa").get("descripcion"),
                        getPath("root").get("idDepositoLogistico"),
                        getPath("depositoLogistico").get("codigo"),
                        getPath("depositoLogistico").get("descripcion"),
                        getPath("depositoLogistico").get("idLocal"),
                        getPath("local").get("descripcion"),
                        getPath("root").get("idProducto"),
                        getPath("producto").get("descripcion"),
                        getPath("producto").get("codigoGtin"),
                        getPath("producto").get("codigoInterno"),
                        getPath("root").get("idEstado"),
                        getPath("estado").get("descripcion"),
                        getPath("estado").get("codigo"),
                        getPath("root").get("cantidad"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public Inventario find(Integer idDepositoLogistico, Integer idProducto, Integer idEstado) {
        InventarioParam param = new InventarioParam();
        param.setIdDepositoLogistico(idDepositoLogistico);
        param.setIdProducto(idProducto);
        param.setIdEstado(idEstado);
        return findFirst(param);
    }
    
    @Override
    public List<Inventario> find(InventarioParam param) {

        AbstractFind find = new AbstractFind(Inventario.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, InventarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<Inventario> root = cq.from(Inventario.class);
        Join<Inventario, Empresa> empresa = root.join("empresa");
        Join<Inventario, DepositoLogistico> depositoLogistico = root.join("depositoLogistico");
        Join<DepositoLogistico, Local> local = depositoLogistico.join("local");
        Join<Inventario, Producto> producto = root.join("producto");
        Join<Inventario, Estado> estado = root.join("estado");
        
        find.addPath("root", root);
        find.addPath("empresa", empresa);
        find.addPath("depositoLogistico", depositoLogistico);
        find.addPath("local", local);
        find.addPath("producto", producto);
        find.addPath("estado", estado);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdLocal() != null) {
            predList.add(qb.equal(depositoLogistico.get("idLocal"), param.getIdLocal()));
        }

        if (param.getIdDepositoLogistico()!= null) {
            predList.add(qb.equal(root.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }

        if (param.getIdDepositoLogisticos() != null && !param.getIdDepositoLogisticos().isEmpty()) {
            predList.add(root.get("idDepositoLogistico").in(param.getIdDepositoLogisticos()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }

        if (param.getIdProductos() != null && !param.getIdProductos().isEmpty()) {
            predList.add(root.get("idProducto").in(param.getIdProductos()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);
        
        cq.orderBy(qb.asc(root.get("idDepositoLogistico")),
                qb.asc(root.get("idProducto")),
                qb.asc(root.get("idEstado")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(InventarioParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Inventario> root = cq.from(Inventario.class);
        Join<Inventario, DepositoLogistico> depositoLogistico = root.join("depositoLogistico");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdLocal() != null) {
            predList.add(qb.equal(depositoLogistico.get("idLocal"), param.getIdLocal()));
        }
        
        if (param.getIdDepositoLogistico()!= null) {
            predList.add(qb.equal(root.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }

        if (param.getIdDepositoLogisticos() != null && !param.getIdDepositoLogisticos().isEmpty()) {
            predList.add(root.get("idDepositoLogistico").in(param.getIdDepositoLogisticos()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(root.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdProductos() != null && !param.getIdProductos().isEmpty()) {
            predList.add(root.get("idProducto").in(param.getIdProductos()));
        }

        if (param.getIdEstado() != null) {
            predList.add(qb.equal(root.get("idEstado"), param.getIdEstado()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
    
    @Override
    public List<InventarioPojo> findResumen(InventarioParam param) {
        
        String where = "true";
        
        if (param.getIdEmpresa() != null) {
            where = String.format("%s and i.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdLocal( )!= null) {
            where = String.format("%s and dl.id_local = %d", where, param.getIdLocal());
        }
        
        if (param.getIdDepositoLogistico() != null) {
            where = String.format("%s and i.id_deposito_logistico = %d", where, param.getIdDepositoLogistico());
        }

        if (param.getIdDepositoLogisticos() != null && !param.getIdDepositoLogisticos().isEmpty()) {
            where = String.format("%s and i.id_deposito_logistico in (%s)", where, Strings.join(param.getIdDepositoLogisticos(), ','));
        }
        
        if (param.getIdProducto() != null) {
            where = String.format("%s and i.id_producto = %d", where, param.getIdProducto());
        }
        
        if (param.getIdProductos() != null && !param.getIdProductos().isEmpty()) {
            where = String.format("%s and i.id_producto in (%s)", where, Strings.join(param.getIdProductos(), ','));
        }
        
        String sql = String.format("select i.id_empresa, e.descripcion as empresa,\n"
                + "i.id_deposito_logistico, dl.descripcion as deposito_logistico,\n"
                + "dl.id_local, l.descripcion as local, i.id_producto,\n"
                + "p.descripcion as producto, p.codigo_gtin, p.codigo_interno,\n"
                + "sum(i.cantidad) as cantidad\n"
                + "from inventario.inventario i\n"
                + "join info.empresa e on e.id = i.id_empresa\n"
                + "join inventario.deposito_logistico dl on dl.id = i.id_deposito_logistico\n"
                + "join info.local l on l.id = dl.id_local\n"
                + "join info.producto p on p.id = i.id_producto\n"
                + "where %s\n"
                + "group by i.id_empresa, i.id_deposito_logistico, i.id_producto, p.id, e.id, l.id, dl.id\n"
                + "order by i.id_empresa, i.id_deposito_logistico, i.id_producto\n"
                + "limit %d offset %d", where, param.getPageSize(), param.getFirstResult());
        
        Query q = getEntityManager().createNativeQuery(sql);
        List<Object[]> list = q.getResultList();
        
        List<InventarioPojo> result = new ArrayList<>();
        
        for (Object[] objects : list) {
            int i = 0;
            InventarioPojo item = new InventarioPojo();
            item.setIdEmpresa((Integer)objects[i++]);
            item.setEmpresa((String)objects[i++]);
            item.setIdDepositoLogistico((Integer)objects[i++]);
            item.setDepositoLogistico((String)objects[i++]);
            item.setIdLocal((Integer)objects[i++]);
            item.setLocal((String)objects[i++]);
            item.setIdProducto((Integer)objects[i++]);
            item.setProducto((String)objects[i++]);
            item.setCodigoGtin((String)objects[i++]);
            item.setCodigoInterno((String)objects[i++]);
            item.setCantidad(((Number)objects[i++]).intValue());
            result.add(item);
        }
        
        return result;
    }
    @Override
    public Long findResumenSize(InventarioParam param) {
        
        String where = "true";
        
        if (param.getIdEmpresa() != null) {
            where = String.format("%s and i.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdLocal( )!= null) {
            where = String.format("%s and dl.id_local = %d", where, param.getIdLocal());
        }
        
        if (param.getIdDepositoLogistico()!= null) {
            where = String.format("%s and i.id_deposito_logistico = %d", where, param.getIdDepositoLogistico());
        }

        if (param.getIdDepositoLogisticos() != null && !param.getIdDepositoLogisticos().isEmpty()) {
            where = String.format("%s and i.id_deposito_logistico in (%s)", where, Strings.join(param.getIdDepositoLogisticos(), ','));
        }
        
        if (param.getIdProducto() != null) {
            where = String.format("%s and i.id_producto = %d", where, param.getIdProducto());
        }
        
        if (param.getIdProductos() != null && !param.getIdProductos().isEmpty()) {
            where = String.format("%s and i.id_producto in (%s)", where, Strings.join(param.getIdProductos(), ','));
        }
        
        String sql = String.format("select count(distinct(i.id_deposito_logistico, dl.descripcion,\n"
                + "dl.id_local, l.descripcion, i.id_producto,\n"
                + "p.descripcion, p.codigo_gtin, p.codigo_interno))\n"
                + "from inventario.inventario i\n"
                + "join inventario.deposito_logistico dl on dl.id = i.id_deposito_logistico\n"
                + "join info.local l on l.id = dl.id_local\n"
                + "join info.producto p on p.id = i.id_producto\n"
                + "where %s", where);
        
        Query q = getEntityManager().createNativeQuery(sql);
        Object object = q.getSingleResult();
        
        Long result = object == null ? 0L : ((Number)object).longValue();
        
        return result;
    }
}
