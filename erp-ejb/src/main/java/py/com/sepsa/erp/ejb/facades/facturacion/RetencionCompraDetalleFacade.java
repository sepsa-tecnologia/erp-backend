/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.RetencionCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.RetencionCompraDetalleParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface RetencionCompraDetalleFacade extends Facade<RetencionCompraDetalle, RetencionCompraDetalleParam, UserInfoImpl> {

    public Boolean validToCreate(RetencionCompraDetalleParam param, Boolean validarIdRetencionCompra);
    
}