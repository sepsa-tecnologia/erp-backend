/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.PersonaEmailNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaEmailNotificacionParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.PersonaEmailNotificacionPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "PersonaEmailNotificacionFacade", mappedName = "PersonaEmailNotificacionFacade")
@Local(PersonaEmailNotificacionFacade.class)
public class PersonaEmailNotificacionFacadeImpl extends FacadeImpl<PersonaEmailNotificacion, PersonaEmailNotificacionParam>  implements  PersonaEmailNotificacionFacade {

    public PersonaEmailNotificacionFacadeImpl() {
        super(PersonaEmailNotificacion.class);
    }

    /**
     * Obtiene la lista de contacto email notificacion asociada
     * @param idContacto Identificador de contacto
     * @param idEmail Identificador de email
     * @return Contacto email notificacion
     */
    @Override
    public List<PersonaEmailNotificacionPojo> asociado(Integer idContacto, Integer idEmail) {
        
        String sql = String.format("with pen as (select pen.*\n"
                + " from info.persona_email_notificacion pen\n"
                + "where pen.id_persona = %d\n"
                + "and pen.id_email = %d)\n"
                + "select pen.id_persona,\n"
                + "coalesce(p.razon_social, p.nombre || ' ' || p.apellido) as contacto,\n"
                + "pen.id_email, e.email, tn.id, tn.descripcion,\n"
                + "coalesce(pen.estado, 'I') as estado\n"
                + "from pen\n"
                + "right join info.tipo_notificacion tn on tn.id = pen.id_tipo_notificacion\n"
                + "left join info.persona p on p.id = pen.id_persona\n"
                + "left join info.email e on e.id = pen.id_email\n"
                + "where tn.email = 'S'",
                idContacto, idEmail);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<PersonaEmailNotificacionPojo> result = new ArrayList<>();
        
        for (Object[] object : list) {
            
            PersonaEmailNotificacionPojo pojo = new PersonaEmailNotificacionPojo();
            pojo.setIdContacto((Integer)object[0]);
            pojo.setContacto((String)object[1]);
            pojo.setIdEmail((Integer)object[2]);
            pojo.setEmail((String)object[3]);
            pojo.setIdTipoNotificacion((Integer)object[4]);
            pojo.setTipoNotificacion((String)object[5]);
            pojo.setEstado((Character)object[6]);
            
            result.add(pojo);
        }
        
        return result;
    }
    
    /**
     * Obtiene la lista de contacto email
     * @param param Parámetros
     * @return Lista
     */
    @Override
    public List<PersonaEmailNotificacion> find(PersonaEmailNotificacionParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and c.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format(" %s and (pen.id_persona = %d or pc.id_persona = %d)", where, param.getIdPersona(), param.getIdPersona());
        }
        
        if (param.getIdEmail() != null) {
            where = String.format(" %s and pen.id_email = %d", where, param.getIdEmail());
        }
        
        if (param.getIdTipoNotificacion() != null) {
            where = String.format(" %s and pen.id_tipo_notificacion = %d", where, param.getIdTipoNotificacion());
        }
        
        if (param.getActivo() != null) {
            where = String.format(" %s and pen.activo = '%s'", where, param.getActivo());
        }
        
        if (param.getContactoActivo() != null) {
            where = String.format(" %s and con.activo = '%s'", where, param.getContactoActivo());
        }
        
        if (param.getContactoEmailActivo() != null) {
            where = String.format(" %s and pe.activo = '%s'", where, param.getContactoEmailActivo());
        }
        
        if(param.getContacto()!= null && !param.getContacto().trim().isEmpty()) {
            where = String.format(" %s and coalesce(c.razon_social, c.nombre || ' ' || c.apellido) ilike '%%%s%%'", where, param.getContacto().trim());
        }
        
        if(param.getEmail()!= null && !param.getEmail().trim().isEmpty()) {
            where = String.format(" %s and e.email ilike '%%%s%%'", where, param.getEmail().trim());
        }
        
        if(param.getTipoNotificacion() != null && !param.getTipoNotificacion().trim().isEmpty()) {
            where = String.format(" %s and tn.descripcion ilike '%%%s%%'", where, param.getTipoNotificacion().trim());
        }
        
        if (param.getCodigoTipoNotificacion() != null && !param.getCodigoTipoNotificacion().trim().isEmpty()) {
            where = String.format(" %s and tn.codigo = '%s'", where, param.getCodigoTipoNotificacion().trim());
        }
        
        String sql = String.format("select pen.*\n"
                + "from info.persona_email_notificacion pen \n"
                + "left join info.persona_email pe on pe.id_persona = pen.id_persona and pe.id_email = pen.id_email\n"
                + "left join info.persona c on c.id = pen.id_persona\n"
                + "left join info.contacto con on con.id_contacto = pen.id_persona\n"
                + "left join info.email e on e.id = pen.id_email\n"
                + "left join info.tipo_notificacion tn on tn.id = pen.id_tipo_notificacion\n"
                + "left join info.persona_contacto pc on pc.id_contacto = pen.id_persona\n"
                + "where %s order by pen.activo offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, PersonaEmailNotificacion.class);
        
        List<PersonaEmailNotificacion> result = q.getResultList();
        
        return result;
    }
    
    /**
     * Obtiene el tamaño de la lista de contacto email
     * @param param Parámetros
     * @return Tamaño de la lista
     */
    @Override
    public Long findSize(PersonaEmailNotificacionParam param) {

        String where = "true";
        
        if(param.getIdEmpresa() != null) {
            where = String.format("%s and c.id_empresa = %d", where, param.getIdEmpresa());
        }
        
        if (param.getIdPersona() != null) {
            where = String.format(" %s and (pen.id_persona = %d or pc.id_persona = %d)", where, param.getIdPersona(), param.getIdPersona());
        }
        
        if (param.getIdEmail() != null) {
            where = String.format(" %s and pen.id_email = %d", where, param.getIdEmail());
        }
        
        if (param.getIdTipoNotificacion() != null) {
            where = String.format(" %s and pen.id_tipo_notificacion = %d", where, param.getIdTipoNotificacion());
        }
        
        if (param.getActivo() != null) {
            where = String.format(" %s and pen.activo = '%s'", where, param.getActivo());
        }
        
        if (param.getContactoActivo()!= null) {
            where = String.format(" %s and con.activo = '%s'", where, param.getContactoActivo());
        }
        
        if (param.getContactoEmailActivo() != null) {
            where = String.format(" %s and pe.activo = '%s'", where, param.getContactoEmailActivo());
        }
        
        if(param.getContacto()!= null && !param.getContacto().trim().isEmpty()) {
            where = String.format(" %s and coalesce(c.razon_social, c.nombre || ' ' || c.apellido) ilike '%%%s%%'", where, param.getContacto().trim());
        }
        
        if(param.getEmail()!= null && !param.getEmail().trim().isEmpty()) {
            where = String.format(" %s and e.email ilike '%%%s%%'", where, param.getEmail().trim());
        }
        
        if(param.getTipoNotificacion() != null && !param.getTipoNotificacion().trim().isEmpty()) {
            where = String.format(" %s and tn.descripcion ilike '%%%s%%'", where, param.getTipoNotificacion().trim());
        }
        
        if (param.getCodigoTipoNotificacion() != null && !param.getCodigoTipoNotificacion().trim().isEmpty()) {
            where = String.format(" %s and tn.codigo = '%s'", where, param.getCodigoTipoNotificacion().trim());
        }
        
        String sql = String.format("select count(distinct pen.*)\n"
                + "from info.persona_email_notificacion pen \n"
                + "left join info.persona_email pe on pe.id_persona = pen.id_persona and pe.id_email = pen.id_email\n"
                + "left join info.persona c on c.id = pen.id_persona\n"
                + "left join info.contacto con on con.id_contacto = pen.id_persona\n"
                + "left join info.email e on e.id = pen.id_email\n"
                + "left join info.tipo_notificacion tn on tn.id = pen.id_tipo_notificacion\n"
                + "left join info.persona_contacto pc on pc.id_contacto = pen.id_persona\n"
                + "where %s", where);

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
