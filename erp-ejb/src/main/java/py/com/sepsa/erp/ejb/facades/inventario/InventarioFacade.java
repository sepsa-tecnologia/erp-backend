/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.Inventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface InventarioFacade extends Facade<Inventario, InventarioParam, UserInfoImpl> {

    public Inventario find(Integer idDepositoLogistico, Integer idProducto, Integer idEstado);

    public Long findResumenSize(InventarioParam param);

    public List<InventarioPojo> findResumen(InventarioParam param);
    
}
