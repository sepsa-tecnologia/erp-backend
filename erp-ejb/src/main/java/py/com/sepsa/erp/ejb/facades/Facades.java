/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.facades.auditoria.AuditoriaDetalleFacade;
import py.com.sepsa.erp.ejb.facades.auditoria.AuditoriaFacade;
import py.com.sepsa.erp.ejb.facades.auditoria.OperacionFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ClienteFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ClienteServicioFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ConfigBonificacionFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ContratoFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ContratoServicioFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ContratoServicioTarifaFacade;
import py.com.sepsa.erp.ejb.facades.comercial.DescuentoFacade;
import py.com.sepsa.erp.ejb.facades.comercial.MonedaFacade;
import py.com.sepsa.erp.ejb.facades.comercial.MontoMinimoFacade;
import py.com.sepsa.erp.ejb.facades.comercial.MontoTarifaFacade;
import py.com.sepsa.erp.ejb.facades.comercial.MotivoDescuentoFacade;
import py.com.sepsa.erp.ejb.facades.comercial.PeriodoLiquidacionFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ProductoComFacade;
import py.com.sepsa.erp.ejb.facades.comercial.ServicioFacade;
import py.com.sepsa.erp.ejb.facades.comercial.TarifaFacade;
import py.com.sepsa.erp.ejb.facades.comercial.TipoDescuentoFacade;
import py.com.sepsa.erp.ejb.facades.comercial.TipoTarifaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.ChequeFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.CobroDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.CobroFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.ConceptoCobroFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.CuentaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.EntidadFinancieraFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.ExcedenteDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.ExcedenteFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaCompraDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaCompraFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaCuotaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaDescuentoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.HistoricoLiquidacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LiquidacionDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LiquidacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LiquidacionProductoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LiquidacionServicioFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LugarCobroFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.MensajeRedPagoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.MotivoAnulacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.MotivoEmisionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.MotivoEmisionInternoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.MotivoEmisionSncFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaCreditoCompraDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaCreditoCompraFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaCreditoDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaCreditoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.OperacionRedPagoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.OrdenCompraDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.OrdenCompraFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.RedPagoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.ResguardoDocumentoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.RetencionCompraDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.RetencionCompraFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.RetencionDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.RetencionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.SolicitudNotaCreditoDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.SolicitudNotaCreditoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TalonarioFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TalonarioLocalFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoCambioFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoCobroFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoDocumentoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoFacturaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoMotivoAnulacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoOperacionCreditoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TransaccionRedPagoFacade;
import py.com.sepsa.erp.ejb.facades.info.AprobacionDetalleFacade;
import py.com.sepsa.erp.ejb.facades.info.AprobacionFacade;
import py.com.sepsa.erp.ejb.facades.info.CanalVentaFacade;
import py.com.sepsa.erp.ejb.facades.info.CargoFacade;
import py.com.sepsa.erp.ejb.facades.info.ConfigAprobacionFacade;
import py.com.sepsa.erp.ejb.facades.info.ConfiguracionFacade;
import py.com.sepsa.erp.ejb.facades.info.ConfiguracionValorFacade;
import py.com.sepsa.erp.ejb.facades.info.ContactoFacade;
import py.com.sepsa.erp.ejb.facades.info.DatoPersonaFacade;
import py.com.sepsa.erp.ejb.facades.info.DireccionFacade;
import py.com.sepsa.erp.ejb.facades.info.EmailFacade;
import py.com.sepsa.erp.ejb.facades.info.EmpresaFacade;
import py.com.sepsa.erp.ejb.facades.info.EstadoFacade;
import py.com.sepsa.erp.ejb.facades.info.EtiquetaFacade;
import py.com.sepsa.erp.ejb.facades.info.LocalFacade;
import py.com.sepsa.erp.ejb.facades.info.MarcaFacade;
import py.com.sepsa.erp.ejb.facades.info.MenuFacade;
import py.com.sepsa.erp.ejb.facades.info.MenuPerfilFacade;
import py.com.sepsa.erp.ejb.facades.info.MetricaFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaContactoFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaEmailFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaEmailNotificacionFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaEtiquetaFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaTelefonoFacade;
import py.com.sepsa.erp.ejb.facades.info.PersonaTelefonoNotificacionFacade;
import py.com.sepsa.erp.ejb.facades.info.ProductoFacade;
import py.com.sepsa.erp.ejb.facades.info.ProductoPrecioFacade;
import py.com.sepsa.erp.ejb.facades.info.ReferenciaGeograficaFacade;
import py.com.sepsa.erp.ejb.facades.info.RepositorioFacade;
import py.com.sepsa.erp.ejb.facades.info.TelefonoFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoContactoFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoDatoPersonaFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoEmailFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoEmpresaFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoEstadoFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoEtiquetaFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoMenuFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoNotificacionFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoPersonaFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoReferenciaGeograficaFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoRepositorioFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoTelefonoFacade;
import py.com.sepsa.erp.ejb.facades.info.PlantillaCreacionEmpresaFacade;
import py.com.sepsa.erp.ejb.facades.proceso.FrecuenciaEjecucionFacade;
import py.com.sepsa.erp.ejb.facades.proceso.ProcesamientoArchivoDetalleFacade;
import py.com.sepsa.erp.ejb.facades.proceso.ProcesamientoArchivoFacade;
import py.com.sepsa.erp.ejb.facades.proceso.ProcesoFacade;
import py.com.sepsa.erp.ejb.facades.proceso.TipoArchivoFacade;
import py.com.sepsa.erp.ejb.facades.proceso.TipoDocumentoProcesoFacade;
import py.com.sepsa.erp.ejb.facades.proceso.TipoProcesoFacade;
import py.com.sepsa.erp.ejb.facades.usuario.PerfilFacade;
import py.com.sepsa.erp.ejb.facades.usuario.UsuarioEmpresaFacade;
import py.com.sepsa.erp.ejb.facades.usuario.UsuarioFacade;
import py.com.sepsa.erp.ejb.facades.usuario.UsuarioLocalFacade;
import py.com.sepsa.erp.ejb.facades.usuario.UsuarioPerfilFacade;
import py.com.sepsa.erp.ejb.facades.auditoria.RegistroFacade;
import py.com.sepsa.erp.ejb.facades.auditoria.TipoAuditoriaFacade;
import py.com.sepsa.erp.ejb.facades.auditoria.ValorColumnaFacade;
import py.com.sepsa.erp.ejb.facades.comercial.NaturalezaClienteFacade;
import py.com.sepsa.erp.ejb.facades.info.VendedorFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.AutoFacturaDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.AutoFacturaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaDncpFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaNotificacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.FacturaParametroAdicionalFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LocalEntregaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.LocalSalidaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.MotivoEmisionNrFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NaturalezaTransportistaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaCreditoNotificacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaCreditoParametroAdicionalFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaDebitoDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaDebitoFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaDebitoNotificacionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaRemisionDetalleFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.NotaRemisionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.ParametroAdicionalFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TipoTransaccionFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.TransportistaFacade;
import py.com.sepsa.erp.ejb.facades.facturacion.VehiculoTrasladoFacade;
import py.com.sepsa.erp.ejb.facades.info.MotivoFacade;
import py.com.sepsa.erp.ejb.facades.info.PaisFacade;
import py.com.sepsa.erp.ejb.facades.info.ProductoRelacionadoFacade;
import py.com.sepsa.erp.ejb.facades.info.ReporteEmpresaFacade;
import py.com.sepsa.erp.ejb.facades.info.ReporteFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoMotivoFacade;
import py.com.sepsa.erp.ejb.facades.info.TipoProductoFacade;
import py.com.sepsa.erp.ejb.facades.inventario.ControlInventarioDetalleFacade;
import py.com.sepsa.erp.ejb.facades.inventario.ControlInventarioFacade;
import py.com.sepsa.erp.ejb.facades.inventario.DepositoLogisticoFacade;
import py.com.sepsa.erp.ejb.facades.inventario.DevolucionDetalleFacade;
import py.com.sepsa.erp.ejb.facades.inventario.DevolucionFacade;
import py.com.sepsa.erp.ejb.facades.inventario.InventarioDetalleFacade;
import py.com.sepsa.erp.ejb.facades.inventario.InventarioFacade;
import py.com.sepsa.erp.ejb.facades.inventario.OperacionInventarioDetalleFacade;
import py.com.sepsa.erp.ejb.facades.inventario.OperacionInventarioFacade;
import py.com.sepsa.erp.ejb.facades.inventario.OrdenCompraDetalleInventarioDetalleFacade;
import py.com.sepsa.erp.ejb.facades.inventario.TipoOperacionFacade;
import py.com.sepsa.erp.ejb.facades.proceso.LoteArchivoFacade;
import py.com.sepsa.erp.ejb.facades.proceso.LoteFacade;
import py.com.sepsa.erp.ejb.facades.proceso.TipoLoteFacade;
import py.com.sepsa.erp.ejb.facades.set.AutofacturaProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.set.DetalleProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.set.FacturaProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.set.NotaCreditoProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.set.NotaDebitoProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.set.NotaRemisionProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.set.ProcesamientoFacade;
import py.com.sepsa.erp.ejb.facades.usuario.TokenFacade;
import py.com.sepsa.erp.ejb.task.FacturaMasivaArzaTask;
import py.com.sepsa.erp.ejb.task.FacturaMasivaGamblingTask;
import py.com.sepsa.erp.ejb.task.FacturaMasivaTask;
import py.com.sepsa.erp.ejb.task.NotaCreditoMasivaGamblingTask;
import py.com.sepsa.erp.ejb.task.NotaDebitoMasivaGamblingTask;
import py.com.sepsa.erp.ejb.task.NotaRemisionMasivaGamblingTask;
import py.com.sepsa.erp.ejb.facades.info.ConfiguracionGeneralFacade;
import py.com.sepsa.erp.ejb.facades.info.RucFacade;

/**
 * Clase para el manejo de facades
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless
public class Facades implements Serializable {

    @EJB
    private OperacionFacade operacionFacade;

    @EJB
    private OperacionInventarioDetalleFacade operacionInventarioDetalleFacade;

    @EJB
    private TelefonoFacade telefonoFacade;

    @EJB
    private TalonarioLocalFacade talonarioLocalFacade;

    @EJB
    private ConfigBonificacionFacade configBonificacionFacade;

    @EJB
    private TipoEstadoFacade tipoEstadoFacade;

    @EJB
    private InventarioUtils inventarioUtils;

    @EJB
    private TipoMotivoFacade tipoMotivoFacade;

    @EJB
    private ControlInventarioFacade controlInventarioFacade;

    @EJB
    private ControlInventarioDetalleFacade controlInventarioDetalleFacade;

    @EJB
    private MotivoFacade motivoFacade;

    @EJB
    private UsuarioEmpresaFacade usuarioEmpresaFacade;

    @EJB
    private ClienteServicioFacade clienteServicioFacade;

    @EJB
    private TipoCambioFacade tipoCambioFacade;

    @EJB
    private MotivoAnulacionFacade motivoAnulacionFacade;

    @EJB
    private DepositoLogisticoFacade depositoLogisticoFacade;

    @EJB
    private OperacionInventarioFacade operacionInventarioFacade;

    @EJB
    private TipoMotivoAnulacionFacade tipoMotivoAnulacionFacade;

    @EJB
    private CuentaFacade cuentaFacade;

    @EJB
    private FacturaMasivaTask facturaMasivaTask;

    @EJB
    private FacturaMasivaArzaTask facturaMasivaArzaTask;

    @EJB
    private FacturaMasivaGamblingTask facturaMasivaGamblingTask;

    @EJB
    private NotaCreditoMasivaGamblingTask notaCreditoMasivaGamblingTask;

    @EJB
    private NotaDebitoMasivaGamblingTask notaDebitoMasivaGamblingTask;

    @EJB
    private NotaRemisionMasivaGamblingTask notaRemisionMasivaGamblingTask;

    @EJB
    private OrdenCompraDetalleInventarioDetalleFacade ordenCompraDetalleInventarioDetalleFacade;

    @EJB
    private LiquidacionServicioFacade liquidacionServicioFacade;

    @EJB
    private TransaccionRedPagoFacade transaccionRedPagoFacade;

    @EJB
    private PeriodoLiquidacionFacade periodoLiquidacionFacade;

    @EJB
    private TipoRepositorioFacade tipoRepositorioFacade;

    @EJB
    private MarcaFacade marcaFacade;

    @EJB
    private MenuFacade menuFacade;

    @EJB
    private TipoMenuFacade tipoMenuFacade;

    @EJB
    private TipoOperacionFacade tipoOperacionFacade;

    @EJB
    private MetricaFacade metricaFacade;

    @EJB
    private CobroFacade cobroFacade;

    @EJB
    private TipoReferenciaGeograficaFacade tipoReferenciaGeograficaFacade;

    @EJB
    private CobroDetalleFacade cobroDetalleFacade;

    @EJB
    private ConceptoCobroFacade conceptoCobroFacade;

    @EJB
    private TipoDocumentoFacade tipoDocumentoFacade;

    @EJB
    private TalonarioFacade talonarioFacade;

    @EJB
    private RedPagoFacade redPagoFacade;

    @EJB
    private OperacionRedPagoFacade operacionRedPagoFacade;

    @EJB
    private MensajeRedPagoFacade mensajeRedPagoFacade;

    @EJB
    private NotaCreditoFacade notaCreditoFacade;

    @EJB
    private FacturaNotificacionFacade facturaNotificacionFacade;

    @EJB
    private SolicitudNotaCreditoFacade solicitudNotaCreditoFacade;

    @EJB
    private NotaCreditoCompraFacade notaCreditoCompraFacade;

    @EJB
    private NotaCreditoDetalleFacade notaCreditoDetalleFacade;

    @EJB
    private SolicitudNotaCreditoDetalleFacade solicitudNotaCreditoDetalleFacade;

    @EJB
    private NotaCreditoCompraDetalleFacade notaCreditoCompraDetalleFacade;

    @EJB
    private ExcedenteFacade excedenteFacade;

    @EJB
    private ExcedenteDetalleFacade excedenteDetalleFacade;

    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private OrdenCompraFacade ordenCompraFacade;

    @EJB
    private FacturaDetalleFacade facturaDetalleFacade;

    @EJB
    private FacturaCompraFacade facturaCompraFacade;

    @EJB
    private FacturaCompraDetalleFacade facturaCompraDetalleFacade;

    @EJB
    private FacturaDescuentoFacade facturaDescuentoFacade;

    @EJB
    private FacturaCuotaFacade facturaCuotaFacade;

    @EJB
    private FacturaDncpFacade facturaDncpFacade;

    @EJB
    private OrdenCompraDetalleFacade ordenCompraDetalleFacade;

    @EJB
    private EntidadFinancieraFacade entidadFinancieraFacade;

    @EJB
    private LiquidacionFacade liquidacionFacade;

    @EJB
    private HistoricoLiquidacionFacade historicoLiquidacionFacade;

    @EJB
    private LiquidacionDetalleFacade liquidacionDetalleFacade;

    @EJB
    private LiquidacionProductoFacade liquidacionProductoFacade;

    @EJB
    private DescuentoFacade descuentoFacade;

    @EJB
    private DevolucionFacade devolucionFacade;

    @EJB
    private DevolucionDetalleFacade devolucionDetalleFacade;

    @EJB
    private TipoFacturaFacade tipoFacturaFacade;

    @EJB
    private RetencionFacade retencionFacade;

    @EJB
    private RucFacade rucFacade;

    @EJB
    private RetencionCompraFacade retencionCompraFacade;

    @EJB
    private RetencionDetalleFacade retencionDetalleFacade;

    @EJB
    private RetencionCompraDetalleFacade retencionCompraDetalleFacade;

    @EJB
    private ChequeFacade chequeFacade;

    @EJB
    private LugarCobroFacade lugarCobroFacade;

    @EJB
    private TipoCobroFacade tipoCobroFacade;

    @EJB
    private ResguardoDocumentoFacade resguardoDocumentoFacade;

    @EJB
    private TipoEtiquetaFacade tipoEtiquetaFacade;

    @EJB
    private ConfigAprobacionFacade configAprobacionFacade;

    @EJB
    private RegistroFacade registroFacade;

    @EJB
    private EstadoFacade estadoFacade;

    @EJB
    private TipoDocumentoProcesoFacade tipoDocumentoProcesoFacade;

    @EJB
    private AprobacionFacade aprobacionFacade;

    @EJB
    private ProcesamientoFacade procesamientoFacade;

    @EJB
    private DetalleProcesamientoFacade detalleProcesamientoFacade;

    @EJB
    private FacturaProcesamientoFacade facturaProcesamientoFacade;

    @EJB
    private NotaCreditoProcesamientoFacade notaCreditoProcesamientoFacade;

    @EJB
    private AprobacionDetalleFacade aprobacionDetalleFacade;

    @EJB
    private InventarioFacade inventarioFacade;

    @EJB
    private InventarioDetalleFacade inventarioDetalleFacade;

    @EJB
    private TipoEmpresaFacade tipoEmpresaFacade;

    @EJB
    private TipoOperacionCreditoFacade tipoOperacionCreditoFacade;

    @EJB
    private ProductoPrecioFacade productoPrecioFacade;

    @EJB
    private CanalVentaFacade canalVentaFacade;

    @EJB
    private ProcesoFacade procesoFacade;

    @EJB
    private TipoProcesoFacade tipoProcesoFacade;

    @EJB
    private LoteFacade loteFacade;

    @EJB
    private LoteArchivoFacade loteArchivoFacade;

    @EJB
    private TipoLoteFacade tipoLoteFacade;

    @EJB
    private TipoArchivoFacade tipoArchivoFacade;

    @EJB
    private ProcesamientoArchivoFacade procesamientoArchivoFacade;

    @EJB
    private ProcesamientoArchivoDetalleFacade procesamientoArchivoDetalleFacade;

    @EJB
    private FrecuenciaEjecucionFacade frecuenciaEjecucionFacade;

    @EJB
    private ConfiguracionFacade configuracionFacade;

    @EJB
    private ReportUtils reportUtils;

    @EJB
    private RepositorioFacade repositorioFacade;

    @EJB
    private ConfiguracionValorFacade configuracionValorFacade;
    
    @EJB
    private ConfiguracionGeneralFacade configuracionGeneralFacade;

    @EJB
    private ReferenciaGeograficaFacade referenciaGeograficaFacade;

    @EJB
    private ProductoComFacade productoComFacade;

    @EJB
    private ProductoFacade productoFacade;

    @EJB
    private MontoTarifaFacade montoTarifaFacade;

    @EJB
    private AuditoriaDetalleFacade auditoriaDetalleFacade;

    @EJB
    private TipoTarifaFacade tipoTarifaFacade;

    @EJB
    private TarifaFacade tarifaFacade;

    @EJB
    private TokenFacade tokenFacade;

    @EJB
    private MonedaFacade monedaFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private PersonaContactoFacade personaContactoFacade;

    @EJB
    private MenuPerfilFacade menuPerfilFacade;

    @EJB
    private UsuarioPerfilFacade usuarioPerfilFacade;

    @EJB
    private UsuarioLocalFacade usuarioLocalFacade;

    @EJB
    private TipoTelefonoFacade tipoTelefonoFacade;

    @EJB
    private PersonaEmailFacade personaEmailFacade;

    @EJB
    private TipoNotificacionFacade tipoNotificacionFacade;

    @EJB
    private PersonaEmailNotificacionFacade personaEmailNotificacionFacade;

    @EJB
    private PersonaTelefonoNotificacionFacade personaTelefonoNotificacionFacade;

    @EJB
    private PersonaTelefonoFacade personaTelefonoFacade;

    @EJB
    private InvoiceUtils invoiceUtils;

    @EJB
    private LocalFacade localFacade;

    @EJB
    private TipoEmailFacade tipoEmailFacade;

    @EJB
    private EmailFacade emailFacade;

    @EJB
    private EtiquetaFacade etiquetaFacade;

    @EJB
    private PersonaEtiquetaFacade personaEtiquetaFacade;

    @EJB
    private UsuarioFacade usuarioFacade;

    @EJB
    private EmpresaFacade empresaFacade;

    @EJB
    private ValorColumnaFacade valorColumnaFacade;

    @EJB
    private PersonaFacade personaFacade;

    @EJB
    private PerfilFacade perfilFacade;

    @EJB
    private ReporteFacade reporteFacade;

    @EJB
    private ReporteEmpresaFacade reporteEmpresaFacade;

    @EJB
    private NaturalezaTransportistaFacade naturalezaTransportistaFacade;

    @EJB
    private TipoPersonaFacade tipoPersonaFacade;

    @EJB
    private DireccionFacade direccionFacade;

    @EJB
    private DatoPersonaFacade datoPersonaFacade;

    @EJB
    private TipoDatoPersonaFacade tipoDatoPersonaFacade;

    @EJB
    private NaturalezaClienteFacade naturalezaClienteFacade;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private MotivoDescuentoFacade motivoDescuentoFacade;

    @EJB
    private MotivoEmisionFacade motivoEmisionFacade;

    @EJB
    private MotivoEmisionSncFacade motivoEmisionSncFacade;

    @EJB
    private MotivoEmisionInternoFacade motivoEmisionInternoFacade;

    @EJB
    private TipoDescuentoFacade tipoDescuentoFacade;

    @EJB
    private ContratoFacade contratoFacade;

    @EJB
    private MontoMinimoFacade montoMinimoFacade;

    @EJB
    private ContratoServicioFacade contratoServicioFacade;

    @EJB
    private ContratoServicioTarifaFacade contratoServicioTarifaFacade;

    @EJB
    private ContactoFacade contactoFacade;

    @EJB
    private CargoFacade cargoFacade;

    @EJB
    private TipoContactoFacade tipoContactoFacade;

    @EJB
    private TipoAuditoriaFacade tipoAuditoriaFacade;

    @EJB
    private AuditoriaFacade auditoriaFacade;

    @EJB
    private NotaRemisionFacade notaRemisionFacade;

    @EJB
    private NotaRemisionDetalleFacade notaRemisionDetalleFacade;

    @EJB
    private LocalEntregaFacade localEntregaFacade;

    @EJB
    private LocalSalidaFacade localSalidaFacade;

    @EJB
    private VehiculoTrasladoFacade vehiculoTrasladoFacade;

    @EJB
    private TransportistaFacade transportistaFacade;

    @EJB
    private MotivoEmisionNrFacade motivoEmisionNrFacade;

    @EJB
    private NotaRemisionProcesamientoFacade notaRemisionProcesamientoFacade;

    @EJB
    private AutoFacturaFacade autoFacturaFacade;

    @EJB
    private AutoFacturaDetalleFacade autoFacturaDetalleFacade;

    @EJB
    private AutofacturaProcesamientoFacade autofacturaProcesamientoFacade;
    @EJB
    private PlantillaCreacionEmpresaFacade plantillaCreacionEmpresaFacade;

    @EJB
    private TipoTransaccionFacade tipoTransaccionFacade;

    @EJB
    private PaisFacade paisFacade;

    @EJB
    private TipoProductoFacade tipoProductoFacade;

    @EJB
    private ProductoRelacionadoFacade productoRelacionadoFacade;

    @EJB
    private NotaDebitoFacade notaDebitoFacade;

    @EJB
    private NotaDebitoDetalleFacade notaDebitoDetalleFacade;

    @EJB
    private NotaDebitoProcesamientoFacade notaDebitoProcesamientoFacade;

    @EJB
    private NotaCreditoNotificacionFacade notaCreditoNotificacionFacade;

    @EJB
    private NotaDebitoNotificacionFacade notaDebitoNotificacionFacade;

    @EJB
    private ParametroAdicionalFacade parametroAdicionalFacade;

    @EJB
    private FacturaParametroAdicionalFacade facturaParametroAdicionalFacade;

    @EJB
    private NotaCreditoParametroAdicionalFacade notaCreditoParametroAdicionalFacade;

    @EJB
    private VendedorFacade vendedorFacade;

    //<editor-fold defaultstate="collapsed" desc="***GETTER Y SETTER***">
    public PersonaFacade getPersonaFacade() {
        return personaFacade;
    }

    public TipoEmailFacade getTipoEmailFacade() {
        return tipoEmailFacade;
    }

    public ServicioFacade getServicioFacade() {
        return servicioFacade;
    }

    public TipoTarifaFacade getTipoTarifaFacade() {
        return tipoTarifaFacade;
    }

    public MontoTarifaFacade getMontoTarifaFacade() {
        return montoTarifaFacade;
    }

    public MonedaFacade getMonedaFacade() {
        return monedaFacade;
    }

    public ContactoFacade getContactoFacade() {
        return contactoFacade;
    }

    public EtiquetaFacade getEtiquetaFacade() {
        return etiquetaFacade;
    }

    public TipoContactoFacade getTipoContactoFacade() {
        return tipoContactoFacade;
    }

    public CargoFacade getCargoFacade() {
        return cargoFacade;
    }

    public UsuarioPerfilFacade getUsuarioPerfilFacade() {
        return usuarioPerfilFacade;
    }

    public TipoPersonaFacade getTipoPersonaFacade() {
        return tipoPersonaFacade;
    }

    public ProductoComFacade getProductoComFacade() {
        return productoComFacade;
    }

    public LocalFacade getLocalFacade() {
        return localFacade;
    }

    public DireccionFacade getDireccionFacade() {
        return direccionFacade;
    }

    public EmailFacade getEmailFacade() {
        return emailFacade;
    }

    public TipoTelefonoFacade getTipoTelefonoFacade() {
        return tipoTelefonoFacade;
    }

    public DatoPersonaFacade getDatoPersonaFacade() {
        return datoPersonaFacade;
    }

    public TipoDatoPersonaFacade getTipoDatoPersonaFacade() {
        return tipoDatoPersonaFacade;
    }

    public UsuarioFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public RepositorioFacade getRepositorioFacade() {
        return repositorioFacade;
    }

    public TipoAuditoriaFacade getTipoAuditoriaFacade() {
        return tipoAuditoriaFacade;
    }

    public RegistroFacade getRegistroFacade() {
        return registroFacade;
    }

    public AuditoriaFacade getAuditoriaFacade() {
        return auditoriaFacade;
    }

    public OperacionFacade getOperacionFacade() {
        return operacionFacade;
    }

    public PersonaContactoFacade getPersonaContactoFacade() {
        return personaContactoFacade;
    }

    public TelefonoFacade getTelefonoFacade() {
        return telefonoFacade;
    }

    public ClienteFacade getClienteFacade() {
        return clienteFacade;
    }

    public PerfilFacade getPerfilFacade() {
        return perfilFacade;
    }

    public PersonaEtiquetaFacade getPersonaEtiquetaFacade() {
        return personaEtiquetaFacade;
    }

    public TipoEtiquetaFacade getTipoEtiquetaFacade() {
        return tipoEtiquetaFacade;
    }

    public ConfigAprobacionFacade getConfigAprobacionFacade() {
        return configAprobacionFacade;
    }

    public TipoNotificacionFacade getTipoNotificacionFacade() {
        return tipoNotificacionFacade;
    }

    public ReferenciaGeograficaFacade getReferenciaGeograficaFacade() {
        return referenciaGeograficaFacade;
    }

    public PersonaTelefonoNotificacionFacade getPersonaTelefonoNotificacionFacade() {
        return personaTelefonoNotificacionFacade;
    }

    public PersonaTelefonoFacade getPersonaTelefonoFacade() {
        return personaTelefonoFacade;
    }

    public AprobacionFacade getAprobacionFacade() {
        return aprobacionFacade;
    }

    public AprobacionDetalleFacade getAprobacionDetalleFacade() {
        return aprobacionDetalleFacade;
    }

    public TarifaFacade getTarifaFacade() {
        return tarifaFacade;
    }

    public ContratoServicioTarifaFacade getContratoServicioTarifaFacade() {
        return contratoServicioTarifaFacade;
    }

    public ContratoServicioFacade getContratoServicioFacade() {
        return contratoServicioFacade;
    }

    public ContratoFacade getContratoFacade() {
        return contratoFacade;
    }

    public MotivoDescuentoFacade getMotivoDescuentoFacade() {
        return motivoDescuentoFacade;
    }

    public MontoMinimoFacade getMontoMinimoFacade() {
        return montoMinimoFacade;
    }

    public PersonaEmailNotificacionFacade getPersonaEmailNotificacionFacade() {
        return personaEmailNotificacionFacade;
    }

    public CobroFacade getCobroFacade() {
        return cobroFacade;
    }

    public CobroDetalleFacade getCobroDetalleFacade() {
        return cobroDetalleFacade;
    }

    public NotaCreditoFacade getNotaCreditoFacade() {
        return notaCreditoFacade;
    }

    public NotaCreditoDetalleFacade getNotaCreditoDetalleFacade() {
        return notaCreditoDetalleFacade;
    }

    public FacturaFacade getFacturaFacade() {
        return facturaFacade;
    }

    public FacturaDetalleFacade getFacturaDetalleFacade() {
        return facturaDetalleFacade;
    }

    public EntidadFinancieraFacade getEntidadFinancieraFacade() {
        return entidadFinancieraFacade;
    }

    public LiquidacionFacade getLiquidacionFacade() {
        return liquidacionFacade;
    }

    public HistoricoLiquidacionFacade getHistoricoLiquidacionFacade() {
        return historicoLiquidacionFacade;
    }

    public LiquidacionDetalleFacade getLiquidacionDetalleFacade() {
        return liquidacionDetalleFacade;
    }

    public TipoFacturaFacade getTipoFacturaFacade() {
        return tipoFacturaFacade;
    }

    public RetencionFacade getRetencionFacade() {
        return retencionFacade;
    }

    public RetencionDetalleFacade getRetencionDetalleFacade() {
        return retencionDetalleFacade;
    }

    public ChequeFacade getChequeFacade() {
        return chequeFacade;
    }

    public LugarCobroFacade getLugarCobroFacade() {
        return lugarCobroFacade;
    }

    public TipoCobroFacade getTipoCobroFacade() {
        return tipoCobroFacade;
    }

    public PeriodoLiquidacionFacade getPeriodoLiquidacionFacade() {
        return periodoLiquidacionFacade;
    }

    public ConceptoCobroFacade getConceptoCobroFacade() {
        return conceptoCobroFacade;
    }

    public TipoDocumentoFacade getTipoDocumentoFacade() {
        return tipoDocumentoFacade;
    }

    public TalonarioFacade getTalonarioFacade() {
        return talonarioFacade;
    }

    public TransaccionRedPagoFacade getTransaccionRedPagoFacade() {
        return transaccionRedPagoFacade;
    }

    public ExcedenteFacade getExcedenteFacade() {
        return excedenteFacade;
    }

    public ExcedenteDetalleFacade getExcedenteDetalleFacade() {
        return excedenteDetalleFacade;
    }

    public DescuentoFacade getDescuentoFacade() {
        return descuentoFacade;
    }

    public LiquidacionProductoFacade getLiquidacionProductoFacade() {
        return liquidacionProductoFacade;
    }

    public RedPagoFacade getRedPagoFacade() {
        return redPagoFacade;
    }

    public MensajeRedPagoFacade getMensajeRedPagoFacade() {
        return mensajeRedPagoFacade;
    }

    public OperacionRedPagoFacade getOperacionRedPagoFacade() {
        return operacionRedPagoFacade;
    }

    public TipoRepositorioFacade getTipoRepositorioFacade() {
        return tipoRepositorioFacade;
    }

    public ConfigBonificacionFacade getConfigBonificacionFacade() {
        return configBonificacionFacade;
    }

    public PersonaEmailFacade getPersonaEmailFacade() {
        return personaEmailFacade;
    }

    public ClienteServicioFacade getClienteServicioFacade() {
        return clienteServicioFacade;
    }

    public LiquidacionServicioFacade getLiquidacionServicioFacade() {
        return liquidacionServicioFacade;
    }

    public MarcaFacade getMarcaFacade() {
        return marcaFacade;
    }

    public ProductoFacade getProductoFacade() {
        return productoFacade;
    }

    public TipoEstadoFacade getTipoEstadoFacade() {
        return tipoEstadoFacade;
    }

    public OrdenCompraFacade getOrdenCompraFacade() {
        return ordenCompraFacade;
    }

    public OrdenCompraDetalleFacade getOrdenCompraDetalleFacade() {
        return ordenCompraDetalleFacade;
    }

    public ConfiguracionValorFacade getConfiguracionValorFacade() {
        return configuracionValorFacade;
    }

    public ConfiguracionFacade getConfiguracionFacade() {
        return configuracionFacade;
    }

    public CuentaFacade getCuentaFacade() {
        return cuentaFacade;
    }

    public ReportUtils getReportUtils() {
        return reportUtils;
    }

    public EstadoFacade getEstadoFacade() {
        return estadoFacade;
    }

    public FacturaDescuentoFacade getFacturaDescuentoFacade() {
        return facturaDescuentoFacade;
    }

    public MenuFacade getMenuFacade() {
        return menuFacade;
    }

    public TipoDescuentoFacade getTipoDescuentoFacade() {
        return tipoDescuentoFacade;
    }

    public TipoProcesoFacade getTipoProcesoFacade() {
        return tipoProcesoFacade;
    }

    public UsuarioLocalFacade getUsuarioLocalFacade() {
        return usuarioLocalFacade;
    }

    public ProcesoFacade getProcesoFacade() {
        return procesoFacade;
    }

    public FrecuenciaEjecucionFacade getFrecuenciaEjecucionFacade() {
        return frecuenciaEjecucionFacade;
    }

    public TipoMenuFacade getTipoMenuFacade() {
        return tipoMenuFacade;
    }

    public TipoCambioFacade getTipoCambioFacade() {
        return tipoCambioFacade;
    }

    public MotivoEmisionInternoFacade getMotivoEmisionInternoFacade() {
        return motivoEmisionInternoFacade;
    }

    public MotivoEmisionFacade getMotivoEmisionFacade() {
        return motivoEmisionFacade;
    }

    public FacturaCompraFacade getFacturaCompraFacade() {
        return facturaCompraFacade;
    }

    public FacturaCompraDetalleFacade getFacturaCompraDetalleFacade() {
        return facturaCompraDetalleFacade;
    }

    public TipoMotivoAnulacionFacade getTipoMotivoAnulacionFacade() {
        return tipoMotivoAnulacionFacade;
    }

    public MotivoAnulacionFacade getMotivoAnulacionFacade() {
        return motivoAnulacionFacade;
    }

    public NotaCreditoCompraFacade getNotaCreditoCompraFacade() {
        return notaCreditoCompraFacade;
    }

    public RetencionCompraFacade getRetencionCompraFacade() {
        return retencionCompraFacade;
    }

    public RetencionCompraDetalleFacade getRetencionCompraDetalleFacade() {
        return retencionCompraDetalleFacade;
    }

    public NotaCreditoCompraDetalleFacade getNotaCreditoCompraDetalleFacade() {
        return notaCreditoCompraDetalleFacade;
    }

    public CanalVentaFacade getCanalVentaFacade() {
        return canalVentaFacade;
    }

    public ProductoPrecioFacade getProductoPrecioFacade() {
        return productoPrecioFacade;
    }

    public TipoArchivoFacade getTipoArchivoFacade() {
        return tipoArchivoFacade;
    }

    public ProcesamientoArchivoFacade getProcesamientoArchivoFacade() {
        return procesamientoArchivoFacade;
    }

    public ProcesamientoArchivoDetalleFacade getProcesamientoArchivoDetalleFacade() {
        return procesamientoArchivoDetalleFacade;
    }

    public TipoReferenciaGeograficaFacade getTipoReferenciaGeograficaFacade() {
        return tipoReferenciaGeograficaFacade;
    }

    public MenuPerfilFacade getMenuPerfilFacade() {
        return menuPerfilFacade;
    }

    public TipoDocumentoProcesoFacade getTipoDocumentoProcesoFacade() {
        return tipoDocumentoProcesoFacade;
    }

    public SolicitudNotaCreditoFacade getSolicitudNotaCreditoFacade() {
        return solicitudNotaCreditoFacade;
    }

    public MetricaFacade getMetricaFacade() {
        return metricaFacade;
    }

    public MotivoEmisionSncFacade getMotivoEmisionSncFacade() {
        return motivoEmisionSncFacade;
    }

    public SolicitudNotaCreditoDetalleFacade getSolicitudNotaCreditoDetalleFacade() {
        return solicitudNotaCreditoDetalleFacade;
    }

    public TipoOperacionCreditoFacade getTipoOperacionCreditoFacade() {
        return tipoOperacionCreditoFacade;
    }

    public ResguardoDocumentoFacade getResguardoDocumentoFacade() {
        return resguardoDocumentoFacade;
    }

    public UsuarioEmpresaFacade getUsuarioEmpresaFacade() {
        return usuarioEmpresaFacade;
    }

    public EmpresaFacade getEmpresaFacade() {
        return empresaFacade;
    }

    public FacturaCuotaFacade getFacturaCuotaFacade() {
        return facturaCuotaFacade;
    }

    public TalonarioLocalFacade getTalonarioLocalFacade() {
        return talonarioLocalFacade;
    }

    public AuditoriaDetalleFacade getAuditoriaDetalleFacade() {
        return auditoriaDetalleFacade;
    }

    public ProcesamientoFacade getProcesamientoFacade() {
        return procesamientoFacade;
    }

    public DetalleProcesamientoFacade getDetalleProcesamientoFacade() {
        return detalleProcesamientoFacade;
    }

    public NotaCreditoProcesamientoFacade getNotaCreditoProcesamientoFacade() {
        return notaCreditoProcesamientoFacade;
    }

    public FacturaProcesamientoFacade getFacturaProcesamientoFacade() {
        return facturaProcesamientoFacade;
    }

    public TipoMotivoFacade getTipoMotivoFacade() {
        return tipoMotivoFacade;
    }

    public MotivoFacade getMotivoFacade() {
        return motivoFacade;
    }

    public ValorColumnaFacade getValorColumnaFacade() {
        return valorColumnaFacade;
    }

    public InventarioFacade getInventarioFacade() {
        return inventarioFacade;
    }

    public TipoEmpresaFacade getTipoEmpresaFacade() {
        return tipoEmpresaFacade;
    }

    public TipoOperacionFacade getTipoOperacionFacade() {
        return tipoOperacionFacade;
    }

    public OperacionInventarioDetalleFacade getOperacionInventarioDetalleFacade() {
        return operacionInventarioDetalleFacade;
    }

    public ControlInventarioFacade getControlInventarioFacade() {
        return controlInventarioFacade;
    }

    public ControlInventarioDetalleFacade getControlInventarioDetalleFacade() {
        return controlInventarioDetalleFacade;
    }

    public DepositoLogisticoFacade getDepositoLogisticoFacade() {
        return depositoLogisticoFacade;
    }

    public OperacionInventarioFacade getOperacionInventarioFacade() {
        return operacionInventarioFacade;
    }

    public InventarioDetalleFacade getInventarioDetalleFacade() {
        return inventarioDetalleFacade;
    }

    public FacturaNotificacionFacade getFacturaNotificacionFacade() {
        return facturaNotificacionFacade;
    }

    public InventarioUtils getInventarioUtils() {
        return inventarioUtils;
    }

    public OrdenCompraDetalleInventarioDetalleFacade getOrdenCompraDetalleInventarioDetalleFacade() {
        return ordenCompraDetalleInventarioDetalleFacade;
    }

    public DevolucionFacade getDevolucionFacade() {
        return devolucionFacade;
    }

    public NaturalezaClienteFacade getNaturalezaClienteFacade() {
        return naturalezaClienteFacade;
    }

    public InvoiceUtils getInvoiceUtils() {
        return invoiceUtils;
    }

    public TokenFacade getTokenFacade() {
        return tokenFacade;
    }

    public DevolucionDetalleFacade getDevolucionDetalleFacade() {
        return devolucionDetalleFacade;
    }

    public FacturaDncpFacade getFacturaDncpFacade() {
        return facturaDncpFacade;
    }

    public NotaRemisionFacade getNotaRemisionFacade() {
        return notaRemisionFacade;
    }

    public NotaRemisionDetalleFacade getNotaRemisionDetalleFacade() {
        return notaRemisionDetalleFacade;
    }

    public LocalEntregaFacade getLocalEntregaFacade() {
        return localEntregaFacade;
    }

    public LocalSalidaFacade getLocalSalidaFacade() {
        return localSalidaFacade;
    }

    public VehiculoTrasladoFacade getVehiculoTrasladoFacade() {
        return vehiculoTrasladoFacade;
    }
    public TransportistaFacade getTransportistaFacade() {
        return transportistaFacade;
    }

    public MotivoEmisionNrFacade getMotivoEmisionNrFacade() {
        return motivoEmisionNrFacade;
    }

    public NotaRemisionProcesamientoFacade getNotaRemisionProcesamientoFacade() {
        return notaRemisionProcesamientoFacade;
    }

    public AutoFacturaFacade getAutoFacturaFacade() {
        return autoFacturaFacade;
    }

    public AutoFacturaDetalleFacade getAutoFacturaDetalleFacade() {
        return autoFacturaDetalleFacade;
    }

    public AutofacturaProcesamientoFacade getAutofacturaProcesamientoFacade() {
        return autofacturaProcesamientoFacade;
    }

    public PlantillaCreacionEmpresaFacade getPlantillaCreacionEmpresaFacade() {
        return plantillaCreacionEmpresaFacade;
    }

    public TipoTransaccionFacade getTipoTransaccionFacade() {
        return tipoTransaccionFacade;
    }

    public PaisFacade getPaisFacade() {
        return paisFacade;
    }

    public ReporteFacade getReporteFacade() {
        return reporteFacade;
    }

    public ReporteEmpresaFacade getReporteEmpresaFacade() {
        return reporteEmpresaFacade;
    }

    public NaturalezaTransportistaFacade getNaturalezaTransportistaFacade() {
        return naturalezaTransportistaFacade;
    }

    public TipoLoteFacade getTipoLoteFacade() {
        return tipoLoteFacade;
    }

    public LoteFacade getLoteFacade() {
        return loteFacade;
    }

    public TipoProductoFacade getTipoProductoFacade() {
        return tipoProductoFacade;
    }

    public ProductoRelacionadoFacade getProductoRelacionadoFacade() {
        return productoRelacionadoFacade;
    }

    public NotaDebitoFacade getNotaDebitoFacade() {
        return notaDebitoFacade;
    }

    public NotaDebitoDetalleFacade getNotaDebitoDetalleFacade() {
        return notaDebitoDetalleFacade;
    }

    public NotaDebitoProcesamientoFacade getNotaDebitoProcesamientoFacade() {
        return notaDebitoProcesamientoFacade;
    }

    public FacturaMasivaTask getFacturaMasivaTask() {
        return facturaMasivaTask;
    }

    public LoteArchivoFacade getLoteArchivoFacade() {
        return loteArchivoFacade;
    }

    public FacturaMasivaGamblingTask getFacturaMasivaGamblingTask() {
        return facturaMasivaGamblingTask;
    }

    public NotaDebitoMasivaGamblingTask getNotaDebitoMasivaGamblingTask() {
        return notaDebitoMasivaGamblingTask;
    }

    public NotaCreditoMasivaGamblingTask getNotaCreditoMasivaGamblingTask() {
        return notaCreditoMasivaGamblingTask;
    }

    public FacturaMasivaArzaTask getFacturaMasivaArzaTask() {
        return facturaMasivaArzaTask;
    }

    public NotaCreditoNotificacionFacade getNotaCreditoNotificacionFacade() {
        return notaCreditoNotificacionFacade;
    }

    public NotaDebitoNotificacionFacade getNotaDebitoNotificacionFacade() {
        return notaDebitoNotificacionFacade;
    }

    public ParametroAdicionalFacade getParametroAdicionalFacade() {
        return parametroAdicionalFacade;
    }

    public FacturaParametroAdicionalFacade getFacturaParametroAdicionalFacade() {
        return facturaParametroAdicionalFacade;
    }

    public NotaCreditoParametroAdicionalFacade getNotaCreditoParametroAdicionalFacade() {
        return notaCreditoParametroAdicionalFacade;
    }

    public NotaRemisionMasivaGamblingTask getNotaRemisionMasivaGamblingTask() {
        return notaRemisionMasivaGamblingTask;
    }

    public ConfiguracionGeneralFacade getConfiguracionGeneralFacade() {
        return configuracionGeneralFacade;
    }

    public RucFacade getRucFacade() {
        return rucFacade;
    }

    public VendedorFacade getVendedorFacade() {
        return vendedorFacade;
    }
    //</editor-fold> 
}
