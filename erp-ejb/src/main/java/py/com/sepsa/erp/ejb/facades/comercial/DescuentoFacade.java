/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.Descuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.DescuentoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface DescuentoFacade extends Facade<Descuento, DescuentoParam, UserInfoImpl> {

    public Descuento activar(DescuentoParam param);

    public Descuento inactivar(DescuentoParam param);

    public Boolean validToCreate(DescuentoParam param);
    
}
