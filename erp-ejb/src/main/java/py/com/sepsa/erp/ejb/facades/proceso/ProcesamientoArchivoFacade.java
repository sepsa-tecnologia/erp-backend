/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.proceso.ProcesamientoArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.ProcesamientoArchivoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ProcesamientoArchivoFacade extends Facade<ProcesamientoArchivo, ProcesamientoArchivoParam, UserInfoImpl> {

    public Boolean validToCreate(ProcesamientoArchivoParam param);

    public ProcesamientoArchivo reenviar(ProcesamientoArchivoParam param);
    
}
