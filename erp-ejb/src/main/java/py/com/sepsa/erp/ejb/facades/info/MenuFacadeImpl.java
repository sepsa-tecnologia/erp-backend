/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Menu;
import py.com.sepsa.erp.ejb.entities.info.TipoMenu;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "MenuFacade", mappedName = "MenuFacade")
@Local(MenuFacade.class)
public class MenuFacadeImpl extends FacadeImpl<Menu, MenuParam> implements MenuFacade {

    public MenuFacadeImpl() {
        super(Menu.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(MenuParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }
        
        MenuParam param1 = new MenuParam();
        param1.setIdEmpresa(param.getIdEmpresa());
        param1.setCodigo(param.getCodigo());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        if(!isNull(param.getIdPadre())) {
            
            Menu menu = facades.getMenuFacade().find(param.getIdPadre());
            
            if(isNull(menu)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el menú padre"));
            }
        }
        
        TipoMenu tipoMenu = facades.getTipoMenuFacade().find(param.getIdTipoMenu(), param.getCodigoTipoMenu());
        
        if(isNull(tipoMenu)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de menú"));
        } else {
            param.setIdTipoMenu(tipoMenu.getId());
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(MenuParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Empresa empresa = facades.getEmpresaFacade().find(param.getIdEmpresa());
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        }
        
        Menu menu = find(param.getId());
        
        if(isNull(menu)) {
            
            param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el menú"));
            
        } else if(!Objects.equals(menu.getCodigo(), param.getCodigo().trim())) {
            
            MenuParam param1 = new MenuParam();
            param1.setIdEmpresa(param.getIdEmpresa());
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        if(!isNull(param.getIdPadre())) {
            
            Menu menuPadre = facades.getMenuFacade().find(param.getIdPadre());
            
            if(isNull(menuPadre)) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el menú padre"));
            } else {
                
                if(!isNull(menu)) {
                    
                    List<Integer> ids = obtenerIds(null, menu);
                    
                    if(ids.contains(menuPadre.getId())) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("No es posible asignar como menú padre a un menú hijo"));
                    }
                }
            }
        }
        
        TipoMenu tipoMenu = facades.getTipoMenuFacade().find(param.getIdTipoMenu(), param.getCodigoTipoMenu());
        
        if(isNull(tipoMenu)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de menú"));
        } else {
            param.setIdTipoMenu(tipoMenu.getId());
        }
        
        return !param.tieneErrores();
    }
    
    public List<Integer> obtenerIds(List<Integer> ids, Menu menu) {
        
        ids = ids == null ? Lists.empty() : ids;
        
        if(isNull(menu)) {
            return ids;
        } else {
            ids.add(menu.getId());
            
            if(!isNullOrEmpty(menu.getHijos())) {
                for (Menu hijo : menu.getHijos()) {
                    obtenerIds(ids, hijo);
                }
            }
        }
        
        return ids;
    }
    
    @Override
    public Menu create(MenuParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Menu item = new Menu();
        item.setIdEmpresa(param.getIdEmpresa());
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        item.setTitulo(param.getTitulo().trim());
        item.setUrl(param.getUrl());
        item.setActivo(param.getActivo());
        item.setOrden(param.getOrden());
        item.setIdTipoMenu(param.getIdTipoMenu());
        item.setIdPadre(param.getIdPadre());
        create(item);
        
        return item;
    }
    
    @Override
    public Menu edit(MenuParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Menu item = find(param.getId());
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        item.setTitulo(param.getTitulo().trim());
        item.setUrl(param.getUrl());
        item.setActivo(param.getActivo());
        item.setOrden(param.getOrden());
        item.setIdTipoMenu(param.getIdTipoMenu());
        item.setIdPadre(param.getIdPadre());
        edit(item);
        
        return item;
    }

    @Override
    public Menu find(Integer idEmpresa, Integer id, String codigo) {
        MenuParam param = new MenuParam();
        param.setIdEmpresa(idEmpresa);
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        return findFirst(param);
    }
    
    @Override
    public List<Menu> find(MenuParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Menu.class);
        Root<Menu> root = cq.from(Menu.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getTienePadre() != null) {
            predList.add(param.getTienePadre()
                    ? root.get("idPadre").isNotNull()
                    : root.get("idPadre").isNull());
        }

        if (param.getTieneHijo() != null) {
            predList.add(param.getTieneHijo()
                    ? qb.isNotEmpty(root.get("hijos"))
                    : qb.isEmpty(root.get("hijos")));
        }

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdPadre() != null) {
            predList.add(qb.equal(root.get("idPadre"), param.getIdPadre()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getIdTipoMenu() != null) {
            predList.add(qb.equal(root.get("idTipoMenu"), param.getIdTipoMenu()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.asc(root.get("activo")),
                qb.asc(root.get("orden")),
                qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Menu> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(MenuParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Menu> root = cq.from(Menu.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getTienePadre() != null) {
            predList.add(param.getTienePadre()
                    ? root.get("idPadre").isNotNull()
                    : root.get("idPadre").isNull());
        }
        
        if (param.getTieneHijo() != null) {
            predList.add(param.getTieneHijo()
                    ? qb.isNotEmpty(root.get("hijos"))
                    : qb.isEmpty(root.get("hijos")));
        }
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if (param.getIdPadre() != null) {
            predList.add(qb.equal(root.get("idPadre"), param.getIdPadre()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        if (param.getIdTipoMenu() != null) {
            predList.add(qb.equal(root.get("idTipoMenu"), param.getIdTipoMenu()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
