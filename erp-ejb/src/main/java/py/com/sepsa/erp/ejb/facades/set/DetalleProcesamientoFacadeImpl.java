/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.set.DetalleProcesamiento;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.DetalleProcesamientoParam;
import py.com.sepsa.erp.ejb.entities.set.filters.ProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "DetalleProcesamientoFacade", mappedName = "DetalleProcesamientoFacade")
@Local(DetalleProcesamientoFacade.class)
public class DetalleProcesamientoFacadeImpl extends FacadeImpl<DetalleProcesamiento, DetalleProcesamientoParam> implements DetalleProcesamientoFacade {

    public DetalleProcesamientoFacadeImpl() {
        super(DetalleProcesamiento.class);
    }

    @Override
    public Boolean validToCreate(DetalleProcesamientoParam param, boolean validarProcesamiento) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        ProcesamientoParam param1 = new ProcesamientoParam();
        param1.setId(param.getIdProcesamiento());
        
        Procesamiento item = facades.getProcesamientoFacade().findFirst(param1);
        
        if(validarProcesamiento && isNull(item)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de procesamiento"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(DetalleProcesamientoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        DetalleProcesamiento dp = find(param.getId());
        
        if(isNull(dp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de detalle de procesamiento"));
        }
        
        ProcesamientoParam param1 = new ProcesamientoParam();
        param1.setId(param.getIdProcesamiento());
        
        Procesamiento item = facades.getProcesamientoFacade().findFirst(param1);
        
        if(isNull(item)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de procesamiento"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public DetalleProcesamiento create(DetalleProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        DetalleProcesamiento item = new DetalleProcesamiento();
        item.setCodigoResultado(param.getCodigoResultado());
        item.setMensajeResultado(param.getMensajeResultado());
        item.setIdProcesamiento(param.getIdProcesamiento());
        
        create(item);
        
        return item;
    }

    @Override
    public DetalleProcesamiento edit(DetalleProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        DetalleProcesamiento item = find(param.getId());
        item.setCodigoResultado(param.getCodigoResultado());
        item.setMensajeResultado(param.getMensajeResultado());
        item.setIdProcesamiento(param.getIdProcesamiento());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<DetalleProcesamiento> find(DetalleProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(DetalleProcesamiento.class);
        Root<DetalleProcesamiento> root = cq.from(DetalleProcesamiento.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if(param.getCodigoResultado() != null) {
            predList.add(qb.equal(root.get("codigoResultado"), param.getCodigoResultado()));
        }
        
        if (param.getMensajeResultado() != null && !param.getMensajeResultado().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("mensajeResultado"),
                    String.format("%%%s%%", param.getMensajeResultado().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<DetalleProcesamiento> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(DetalleProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<DetalleProcesamiento> root = cq.from(DetalleProcesamiento.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdProcesamiento() != null) {
            predList.add(qb.equal(root.get("idProcesamiento"), param.getIdProcesamiento()));
        }
        
        if(param.getCodigoResultado() != null) {
            predList.add(qb.equal(root.get("codigoResultado"), param.getCodigoResultado()));
        }
        
        if (param.getMensajeResultado() != null && !param.getMensajeResultado().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("mensajeResultado"),
                    String.format("%%%s%%", param.getMensajeResultado().trim())));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
