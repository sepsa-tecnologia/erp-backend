/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.Token;
import py.com.sepsa.erp.ejb.entities.usuario.filters.TokenParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.TokenPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface TokenFacade extends Facade<Token, TokenParam, UserInfoImpl> {

    public TokenPojo find(Integer idUsuario, Integer idEmpresa);
    
    public TokenPojo find(String clave);
    
}
