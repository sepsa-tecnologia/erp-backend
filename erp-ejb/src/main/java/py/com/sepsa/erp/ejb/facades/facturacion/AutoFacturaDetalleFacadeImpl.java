/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Servicio;
import py.com.sepsa.erp.ejb.entities.facturacion.AutoFacturaDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.AutoFacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.AutoFacturaDetallePojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "AutoFacturaDetalleFacade", mappedName = "AutoFacturaDetalleFacade")
@Local(AutoFacturaDetalleFacade.class)
public class AutoFacturaDetalleFacadeImpl extends FacadeImpl<AutoFacturaDetalle, AutoFacturaDetalleParam> implements AutoFacturaDetalleFacade {

    public AutoFacturaDetalleFacadeImpl() {
        super(AutoFacturaDetalle.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @param validarIdFactura validar identificador de factura
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(AutoFacturaDetalleParam param, boolean validarIdFactura) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        AutoFacturaParam param1 = new AutoFacturaParam();
        param1.setId(param.getId());
        Long size = facades.getAutoFacturaFacade().findSize(param1);
        
        if(validarIdFactura && size <= 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la autofactura"));
        }
        
        if(param.getIdServicio() != null) {
            
            Servicio servicio = facades.getServicioFacade().find(param.getIdServicio());
            
            if(servicio == null) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("No existe el servicio"));
            }
        }
        
        return !param.tieneErrores();
    }

    private void validarCantidad(CommonParam param, ProductoPojo producto, Integer cantidad) {
        
        BigDecimal umv = producto.getUmv() == null
                ? BigDecimal.ZERO
                : producto.getUmv().stripTrailingZeros();

        BigDecimal multiploMmv = producto.getMultiploUmv() == null
                ? BigDecimal.ZERO
                : producto.getMultiploUmv().stripTrailingZeros();
        
        BigDecimal verificar = new BigDecimal(cantidad);
        
        BigDecimal temp = verificar.subtract(umv).stripTrailingZeros();
        
        if(temp.compareTo(BigDecimal.ZERO) < 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion(String.format("La unidad minima de venta del producto %s es %d",
                            producto.getDescripcion(), umv)));
        } else {
            
            temp = multiploMmv.compareTo(BigDecimal.ZERO) == 0
                    ? BigDecimal.ZERO
                    : temp.remainder(multiploMmv).stripTrailingZeros();
            
            if(temp.compareTo(BigDecimal.ZERO) != 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion(String.format("La cantidad solicitada del producto %s debe ser múltiplo de %d",
                                producto.getDescripcion(), multiploMmv)));
            }
        }
    }
    
    @Override
    public AutoFacturaDetalle create(AutoFacturaDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param, true)) {
            return null;
        }
        
        AutoFacturaDetalle item = new AutoFacturaDetalle();
        item.setIdAutofactura(param.getIdAutofactura());
        item.setNroLinea(param.getNroLinea());
        item.setDescripcion(param.getDescripcion());

        item.setPorcentajeGravada(param.getPorcentajeGravada());
        item.setCantidad(param.getCantidad());
        item.setPrecioUnitario(param.getPrecioUnitario().stripTrailingZeros());
        item.setMontoTotal(param.getMontoTotal().stripTrailingZeros());
        item.setMontoDescuentoGlobal(BigDecimal.ZERO);
        item.setMontoDescuentoParticular(BigDecimal.ZERO);
        item.setDescuentoGlobalUnitario(BigDecimal.ZERO);
        item.setDescuentoParticularUnitario(BigDecimal.ZERO);
        

        create(item);
        
        return item;
    }
    
    @Override
    public List<AutoFacturaDetallePojo> findPojo(AutoFacturaDetalleParam param) {

        AbstractFind find = new AbstractFind(AutoFacturaDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idAutofactura"),
                        getPath("root").get("nroLinea"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codDncpNivelGeneral"),
                        getPath("root").get("codDncpNivelEspecifico"),
                        getPath("root").get("montoTotal"),
                        getPath("root").get("cantidad"),
                        getPath("root").get("precioUnitario"),
                        getPath("root").get("montoDescuentoParticular"),
                        getPath("root").get("montoDescuentoGlobal"),
                        getPath("root").get("descuentoParticularUnitario"),
                        getPath("root").get("descuentoGlobalUnitario")
                        );
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<AutoFacturaDetalle> find(AutoFacturaDetalleParam param) {

        AbstractFind find = new AbstractFind(AutoFacturaDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, AutoFacturaDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<AutoFacturaDetalle> root = cq.from(AutoFacturaDetalle.class);
        find.addPath("root", root);
      
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdAutofactura() != null) {
            predList.add(qb.equal(root.get("idAutofactura"), param.getIdAutofactura()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }
        

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.asc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(AutoFacturaDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<AutoFacturaDetalle> root = cq.from(AutoFacturaDetalle.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdAutofactura() != null) {
            predList.add(qb.equal(root.get("idAutofactura"), param.getIdAutofactura()));
        }
        
        if (param.getNroLinea() != null) {
            predList.add(qb.equal(root.get("nroLinea"), param.getNroLinea()));
        }
        
        if (param.getPorcentajeIva() != null) {
            predList.add(qb.equal(root.get("porcentajeIva"), param.getPorcentajeIva()));
        }

        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
