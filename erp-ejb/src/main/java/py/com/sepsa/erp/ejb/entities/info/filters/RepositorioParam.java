/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de repositorio
 * @author Jonathan D. Bernal Fernández
 */
public class RepositorioParam extends CommonParam {
    
    
    public RepositorioParam() {
    }

    public RepositorioParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de repositorio
     */
    @QueryParam("idTipoRepositorio")
    private Integer idTipoRepositorio;
    
    /**
     * Código de tipo de repositorio
     */
    @QueryParam("codigoTipoRepositorio")
    private String codigoTipoRepositorio;
    
    /**
     * Clave de acceso
     */
    @QueryParam("claveAcceso")
    private String claveAcceso;
    
    /**
     * Clave secreta
     */
    @QueryParam("claveSecreta")
    private String claveSecreta;
    
    /**
     * Región
     */
    @QueryParam("region")
    private String region;
    
    /**
     * Bucket
     */
    @QueryParam("bucket")
    private String bucket;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoRepositorio) && isNullOrEmpty(codigoTipoRepositorio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de tipo de repositorio"));
        }
        
        if(isNullOrEmpty(claveAcceso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la clave de acceso"));
        }
        
        if(isNullOrEmpty(claveSecreta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la clave secreta"));
        }
        
        if(isNullOrEmpty(region)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la región"));
        }
        
        if(isNullOrEmpty(bucket)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el bucket"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @return bandera
     */
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador del repositorio"));
        }
        
        if(isNull(idTipoRepositorio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de repositorio"));
        }
        
        if(isNullOrEmpty(claveAcceso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la clave de acceso"));
        }
        
        if(isNullOrEmpty(claveSecreta)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la clave secreta"));
        }
        
        if(isNullOrEmpty(region)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la región"));
        }
        
        if(isNullOrEmpty(bucket)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el bucket"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdTipoRepositorio() {
        return idTipoRepositorio;
    }

    public void setIdTipoRepositorio(Integer idTipoRepositorio) {
        this.idTipoRepositorio = idTipoRepositorio;
    }

    public String getClaveAcceso() {
        return claveAcceso;
    }

    public void setClaveAcceso(String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }

    public String getClaveSecreta() {
        return claveSecreta;
    }

    public void setClaveSecreta(String claveSecreta) {
        this.claveSecreta = claveSecreta;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public void setCodigoTipoRepositorio(String codigoTipoRepositorio) {
        this.codigoTipoRepositorio = codigoTipoRepositorio;
    }

    public String getCodigoTipoRepositorio() {
        return codigoTipoRepositorio;
    }
}
