/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Williams Vera
 */
@Local
public interface NotaDebitoFacade extends Facade<NotaDebito, NotaDebitoParam, UserInfoImpl> {

    public NotaDebitoPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente);

    public byte[] getPdfNotaDebito(NotaDebitoParam param) throws Exception;
    
    public byte[] getXmlNotaDebito(NotaDebitoParam param) throws Exception;

    public NotaDebito anular(NotaDebitoParam param, UserInfoImpl userInfo);

    public byte[] getXlsReporteVenta(NotaDebitoParam param, UserInfoImpl userInfo) throws Exception;
    
    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param);
    
    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param);
    
    @Override
    public List<NotaDebitoPojo> findPojo(NotaDebitoParam param);

    public NotaDebito createFromInvoice(NotaDebitoParam ncparam, UserInfoImpl userInfo);
}