/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.task;

import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.proceso.Lote;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.utils.FacturaUtils;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.BEAN)
@Log4j2
public class FacturaMasivaGamblingTask extends AbstracProcesamientoLote {

    @Resource
    private UserTransaction userTransaction;

    @Override
    public void procesar(Lote lote) {

        LoteArchivo archivoFacturas = null;
        LoteArchivo archivoClientes = null;

        LoteArchivoParam laparam = new LoteArchivoParam();
        laparam.setIdLote(lote.getId());
        laparam.isValidToList();
        List<LoteArchivo> archivos = facades.getLoteArchivoFacade().find(laparam);
        for (LoteArchivo archivo : archivos) {
            switch (archivo.getCodigo()) {
                case "FACTURAS":
                    archivoFacturas = archivo;
                    break;
                case "CLIENTES":
                    archivoClientes = archivo;
                    break;
            }
        }

        Result result = convert(lote, archivoFacturas, archivoClientes);
        if (!result.isSuccess()) {
            String resultadoBase64 = Base64.getEncoder().encodeToString(result.getResult().getBytes(StandardCharsets.UTF_8));

            LoteParam loteparam = new LoteParam();
            loteparam.setId(lote.getId());
            loteparam.setIdEmpresa(lote.getIdEmpresa());
            loteparam.setCodigoEstado("PROCESADO");
            loteparam.setNotificado(lote.getNotificado());
            loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
            loteparam.setResultado(resultadoBase64);
            
            facades.getLoteFacade().edit(loteparam, null);
            return;
        }

        FacturaParam param = new FacturaParam();
        param.setIdEmpresa(lote.getIdEmpresa());
        param.setIdUsuario(lote.getIdUsuario());
        param.setUploadedFileBytes(result.getResult().getBytes(StandardCharsets.UTF_8));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<FacturaParam> facturas = FacturaUtils.convertToList(facades, param);
        String inicio = String.format("TIMBRADO;NRO_FACTURA;FECHA;ID_FACTURA;ESTADO_RESULTADO");
        StringBuilder buffer = new StringBuilder();
        buffer.append(inicio).append("\r\n");
        for (FacturaParam fact : facturas) {
            if (fact.tieneErrores()) {
                String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                error = error.concat("%s| ");
                for (MensajePojo mp : fact.getErrores()) {
                    error = String.format(error, mp.getDescripcion());
                    error = error.concat("%s| ");
                }
                error = error.substring(0, error.length() - 5);
                error = error.concat(";;");
                buffer.append(error).append("\r\n");
            } else {

                try {
                    userTransaction.begin();

                    Factura factura = facades
                            .getFacturaFacade()
                            .create(fact, null);

                    if (factura == null) {
                        String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                        error = error.concat("| ");
                        for (MensajePojo mp : fact.getErrores()) {
                            error = error.concat(mp.getDescripcion());
                            error = error.concat(" | ");
                        }
                        error = error.concat(";;");
                        buffer.append(error).append("\r\n");
                    } else {
                        String ok = String.format("%s;%s;%s;%s;OK", fact.getTimbrado(), fact.getNroFactura(), sdf.format(fact.getFecha()), factura.getId().toString());
                        buffer.append(ok).append("\r\n");
                    }

                    userTransaction.commit();
                } catch (Exception exception) {
                    try {
                        userTransaction.rollback();
                    } catch (Exception e) {
                        log.fatal("Error", e);
                    }
                    String error = String.format("%s;%s;-;-;ERROR;", fact.getTimbrado(), fact.getNroFactura());
                    error = error.concat("| ");
                    error = error.concat(exception.getMessage());
                    error = error.concat(";;");
                    buffer.append(error).append("\r\n");
                }
            }
        }

        String resultadoBase64 = Base64.getEncoder().encodeToString(buffer.toString().getBytes(StandardCharsets.UTF_8));

        LoteParam loteparam = new LoteParam();
        loteparam.setId(lote.getId());
        loteparam.setIdEmpresa(lote.getIdEmpresa());
        loteparam.setCodigoEstado("PROCESADO");
        loteparam.setNotificado(lote.getNotificado());
        loteparam.setFechaProcesamiento(Calendar.getInstance().getTime());
        loteparam.setResultado(resultadoBase64);

        facades.getLoteFacade().edit(loteparam, null);
    }

    public Result convert(Lote lote, LoteArchivo file, LoteArchivo fileClient) {
        String timbrado = facades.getConfiguracionValorFacade().getCVValor(lote.getIdEmpresa(), "TIMBRADO_FACTURA_MASIVA");
        if (timbrado == null || timbrado.trim().isEmpty()) {
            return Result.builder()
                    .success(false)
                    .result("La configuración para el timbrado de carga de factura masiva es inexistente")
                    .build();
        }

        List<Cliente> clientes = getListClientes(fileClient);
        if (clientes == null || clientes.isEmpty()) {
            return Result.builder()
                    .success(false)
                    .result("No se encuentra ningún cliente valido en el archivo indicado")
                    .build();
        }

        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(file.getContenido())));  StringWriter writer = new StringWriter()) {
            int numSheets = workbook.getNumberOfSheets();
            for (int sheetNumber = 0; sheetNumber < numSheets; sheetNumber++) {

                Sheet sheet = workbook.getSheetAt(sheetNumber);

                int rowIndex = 0;
                int linea = 1; // Contador de línea
                for (Row row : sheet) {
                    if (rowIndex < 1) { // Omitir las 2 primeras filas
                        rowIndex++;
                        continue;
                    }

                    String nro = getCellValueAsString(row.getCell(1)).trim();
                    while (nro.length() < 7) {
                        nro = "0" + nro;
                    }
                    if (getCellValueAsString(row.getCell(29)) != null && !getCellValueAsString(row.getCell(29)).trim().isEmpty()) {
                        String sucursalCode = getSucursales(lote.getIdEmpresa(), Integer.parseInt(getCellValueAsString(row.getCell(29))), timbrado);
                        if (sucursalCode != null && !sucursalCode.trim().isEmpty()) {
                            String nroFactura = sucursalCode + nro;
                            String fecha = formatearFecha(getCellValueAsString(row.getCell(3)));
                            String codigoIsoMoneda = "PYG";
                            int idcli = Integer.parseInt(getCellValueAsString(row.getCell(12)));
                            String rucCi = "";
                            String dirCli = null;
                            String emailCli = "";
                            for (Cliente cli : clientes) {
                                if (idcli == cli.getIdCliente()) {
                                    rucCi = cli.getNroDocumento();
                                    if (cli.getDireccion() != null) {
                                        dirCli = cli.getDireccion();
                                    }
                                    if (cli.getEmail() != null) {
                                        emailCli = cli.getEmail();
                                    }
                                    break;
                                }
                            }
                            String razonSocial = getCellValueAsString(row.getCell(31)).replace(",", "").replace(";", "").trim();
                            String codSeguridad = generarNumeroRandom();
                            String tipoCambio = "";
                            String tipoFactura = "CREDITO";
                            String tipoCredito = "PLAZO";
                            String diasCredito = "60";
                            String cantidadCuotas = "";
                            String codigoProducto = "001";
                            String porcentajeImpuesto = "0";
                            String porcentajeGravada = "100";
                            String precioUnitarioConIva = getCellValueAsString(row.getCell(7));
                            String descuentoUnitario = calculoDescuento(row.getCell(7), row.getCell(8));
                            String cantidad = getCellValueAsString(row.getCell(11));
                            String cartonDesde = getCellValueAsString(row.getCell(9));
                            String cartonHasta = getCellValueAsString(row.getCell(10));
                            String fechaSorteo = getCellValueAsString(row.getCell(34));
                            String descripcion = "Cartones de Seneté";
                            JsonObject object = new JsonObject();
                            object.addProperty("CARTON_DESDE", cartonDesde);
                            object.addProperty("CARTON_HASTA", cartonHasta);
                            object.addProperty("FECHA_SORTEO", fechaSorteo);
                            if (dirCli != null) {
                                object.addProperty("DIREC_CLIENTE", dirCli);
                            }
                            if (emailCli != null) {
                                object.addProperty("CORREO_CLIENTE", emailCli);
                            }
                            String datoAdicional = Base64.getEncoder().encodeToString(object.toString().getBytes());

                            // Primera línea
                            String linea1 = 1 + "," + timbrado + "," + nroFactura + "," + fecha + "," + codigoIsoMoneda + "," + rucCi + "," + razonSocial + "," + codSeguridad + "," + tipoCambio + "," + tipoFactura + "," + tipoCredito + "," + diasCredito + "," + cantidadCuotas + "," + emailCli;
                            writer.write(linea1 + "\n");

                            // Segunda línea
                            String linea2 = 2 + "," + timbrado + "," + nroFactura + "," + codigoProducto + "," + porcentajeImpuesto + "," + porcentajeGravada + "," + precioUnitarioConIva + "," + descuentoUnitario + "," + cantidad + "," + descripcion + "," + datoAdicional;
                            writer.write(linea2 + "\n");
                        } else {
                            return Result.builder()
                                    .success(false)
                                    .result("No se encuentra ningún talonario registrado para la sucursal (su_id) = " + getCellValueAsString(row.getCell(29)))
                                    .build();
                        }
                    } else {
                        return Result.builder()
                                .success(false)
                                .result("No todos los registros cuentan con el número de sucursal (su_id)")
                                .build();
                    }

                    linea++;
                    rowIndex++;
                }
            }

            return Result.builder()
                    .success(true)
                    .result(writer.toString())
                    .build();
        } catch (IOException e) {
            return Result.builder()
                    .success(true)
                    .result(e.getMessage())
                    .build();
        }
    }

    public List<Cliente> getListClientes(LoteArchivo fileClient) {
        List<Cliente> clientes = new ArrayList<>();
        try ( Workbook workbook = new XSSFWorkbook(new ByteArrayInputStream(Base64.getDecoder().decode(fileClient.getContenido())));  StringWriter writer = new StringWriter()) {
            Sheet sheet = workbook.getSheetAt(0);
            int rowIndex = 0;
            for (Row row : sheet) {
                if (rowIndex < 1) {
                    rowIndex++;
                    continue;
                }
                try {
                    Cliente c = new Cliente();
                    if (getCellValueAsString(row.getCell(0)) != null && !getCellValueAsString(row.getCell(0)).trim().isEmpty() && getCellValueAsString(row.getCell(2)) != null && !getCellValueAsString(row.getCell(2)).trim().isEmpty()) {
                        c.setIdCliente(Integer.parseInt(getCellValueAsString(row.getCell(0))));
                        if (getCellValueAsString(row.getCell(2)).replace(",", "").replace(".", "").trim().equalsIgnoreCase("44444401-7")){
                            c.setNroDocumento("0");
                        } else {
                            c.setNroDocumento(getCellValueAsString(row.getCell(2)).replace(",", "").replace(".", "").trim());
                        }
                        if (getCellValueAsString(row.getCell(12)) != null && !getCellValueAsString(row.getCell(12)).trim().isEmpty()){
                            c.setDireccion(getCellValueAsString(row.getCell(12)).replace(",", "").replace(".", "").trim());
                        }
                        if (getCellValueAsString(row.getCell(13)) != null && !getCellValueAsString(row.getCell(13)).trim().isEmpty()){
                            c.setEmail(getCellValueAsString(row.getCell(13)).replace(",", "").replace(" ", "").trim());
                        }
                        if (!clientes.contains(c)) {
                            clientes.add(c);
                        }
                    }
                } catch (Exception e) {
                }
                rowIndex++;
            }
            return clientes;
        } catch (Exception e) {
            log.fatal("Error", e);
            return null;
        }
    }

    private String generarNumeroRandom() {
        Random random = new Random();
        return String.format("%09d", random.nextInt(1000000000));
    }

    private String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    return sdf.format(cell.getDateCellValue());
                } else {
                    return String.valueOf((long) cell.getNumericCellValue());
                }
            default:
                return "";
        }
    }

    private String calculoDescuento(Cell precioCell, Cell descuentoPorcentajeCell) {
        if (precioCell == null || descuentoPorcentajeCell == null) {
            return "0";
        }
        double price = precioCell.getNumericCellValue();
        double descuentoPorcentaje = descuentoPorcentajeCell.getNumericCellValue();
        return String.valueOf((long) (price * descuentoPorcentaje / 100));
    }

    private String getSucursales(Integer idEmpresa, int idSuc, String timbrado) {
        String sucursales = "";

        TalonarioParam t = new TalonarioParam();
        t.setIdEmpresa(idEmpresa);
        t.setActivo('S');
        t.setTimbrado(timbrado);
        t.setIdTipoDocumento(1);
        t.setCodigoInterno(idSuc);
        TalonarioPojo talonarioPojo = facades.getTalonarioFacade().findFirstPojo(t);
        if (talonarioPojo != null) {
            sucursales = talonarioPojo.getNroSucursal() + "-" + talonarioPojo.getNroPuntoVenta() + "-";
        }

        return sucursales;
    }
    
    public String formatearFecha(String fecha) {
        try {
            int dias = Integer.parseInt(fecha);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate fechaInicial = LocalDate.of(1899, 12, 30);
            LocalDate fechaFinal = fechaInicial.plusDays(dias);
            return fechaFinal.format(formatter);
        } catch (NumberFormatException e) {
            return fecha;
        }
    }

    @Data
    @Builder
    private static class Result {

        private boolean success;
        private String result;
    }

    @Data
    private class Cliente {

        private Integer idCliente;
        private String nroDocumento;
        private String direccion;
        private String email;
    }
}
