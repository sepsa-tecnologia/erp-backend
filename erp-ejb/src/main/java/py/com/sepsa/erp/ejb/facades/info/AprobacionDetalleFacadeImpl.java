/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.info.AprobacionDetalle;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "AprobacionDetalleFacade", mappedName = "AprobacionDetalleFacade")
@Local(AprobacionDetalleFacade.class)
public class AprobacionDetalleFacadeImpl extends FacadeImpl<AprobacionDetalle, CommonParam> implements AprobacionDetalleFacade {

    public AprobacionDetalleFacadeImpl() {
        super(AprobacionDetalle.class);
    }

}
