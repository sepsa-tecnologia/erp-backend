/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaCreditoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface NotaCreditoFacade extends Facade<NotaCredito, NotaCreditoParam, UserInfoImpl> {

    public NotaCreditoPojo obtenerDatosCrear(TalonarioPojo talonario, Integer idCliente);

    public byte[] getPdfNotaCredito(NotaCreditoParam param) throws Exception;
    
    public byte[] getXmlNotaCredito(NotaCreditoParam param) throws Exception;

    public NotaCredito anular(NotaCreditoParam param, UserInfoImpl userInfo);

    public byte[] getXlsReporteVenta(NotaCreditoParam param, UserInfoImpl userInfo) throws Exception;
    
    public List<RegistroComprobantePojo> generarComprobanteVenta(ReporteComprobanteParam param);
    
    public Integer generarComprobanteVentaSize(ReporteComprobanteParam param);
    
    @Override
    public List<NotaCreditoPojo> findPojo(NotaCreditoParam param);

    public NotaCredito createFromInvoice(NotaCreditoParam ncparam, UserInfoImpl userInfo);
    
    public Long montoTotalVentas(NotaCreditoParam param);
}