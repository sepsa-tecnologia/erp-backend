/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils.jwe;

import java.util.List;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

/**
 *
 * @author Jonathan
 */
public class AuthUtil {
    
    /**
     * Tipo de authenticación
     */
    private AuthType authType;
    
    /**
     * Carga útil
     */
    private String payload;

    public void setAuthType(AuthType authType) {
        this.authType = authType;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
    
    public static AuthUtil instance(ContainerRequestContext request) {
        
        AuthUtil result = new AuthUtil();
        result.setAuthType(AuthType.NONE);
        
        if(request == null) {
            return result;
        }
        
        //Get query parameters
        final MultivaluedMap<String, String> qparams = request.getUriInfo().getQueryParameters();
        
        //Fetch token params
        final List<String> tokens = qparams.get("token");
        
        if(tokens != null && !tokens.isEmpty() && !tokens.get(0).trim().isEmpty()) {
            result.setAuthType(AuthType.TOKEN);
            result.setPayload(tokens.get(0).trim());
            return result;
        }
        
        //Get request headers
        final MultivaluedMap<String, String> headers = request.getHeaders();

        //Fetch authorization header
        final List<String> authorizations = headers.get(HttpHeaders.AUTHORIZATION);
        
        if(authorizations != null && !authorizations.isEmpty()) {
            
            String authorization = authorizations.get(0);
            
            if (authorization.startsWith("Bearer")) {
                result.setAuthType(AuthType.BEARER);
                result.setPayload(authorization.split(" ")[1]);
            } else {
                result.setAuthType(AuthType.TOKEN);
                result.setPayload(authorization);
            }
            
            return result;
        }
        
        return result;
    }
    
    public enum AuthType {
        BEARER, TOKEN, NONE
    }
}
