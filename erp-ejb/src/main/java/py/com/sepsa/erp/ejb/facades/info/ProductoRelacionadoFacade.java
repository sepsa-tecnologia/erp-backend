/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.ProductoRelacionado;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoRelacionadoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface ProductoRelacionadoFacade extends Facade<ProductoRelacionado, ProductoRelacionadoParam, UserInfoImpl> {

    public Boolean validToEdit(ProductoRelacionadoParam param);

    public List<ProductoRelacionado> findRelacionados(ProductoRelacionadoParam param);

    public Long findRelacionadosSize(ProductoRelacionadoParam param);

    public Boolean validToCreate(ProductoRelacionadoParam param, boolean validarIdUsuario);
    
}
