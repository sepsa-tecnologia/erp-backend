/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.PersonaTelefonoNotificacion;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaTelefonoNotificacionParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.PersonaTelefonoNotificacionPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface PersonaTelefonoNotificacionFacade extends Facade<PersonaTelefonoNotificacion, PersonaTelefonoNotificacionParam, UserInfoImpl> {

    public List<PersonaTelefonoNotificacionPojo> asociado(Integer idContacto, Integer idTelefono);
    
}
