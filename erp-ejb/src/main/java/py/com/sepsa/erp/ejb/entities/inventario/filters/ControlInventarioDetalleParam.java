/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
public class ControlInventarioDetalleParam extends CommonParam {

    public ControlInventarioDetalleParam() {
    }

    public ControlInventarioDetalleParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de control de inventario
     */
    @QueryParam("idControlInventario")
    private Integer idControlInventario;

    /**
     * Identificador de depósito logistico
     */
    @QueryParam("idDepositoLogistico")
    private Integer idDepositoLogistico;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;

    /**
     * Cantidad inicial
     */
    @QueryParam("cantidadInicial")
    private Integer cantidadInicial;

    /**
     * Cantidad final
     */
    @QueryParam("cantidadFinal")
    private Integer cantidadFinal;

    /**
     * Fecha de vencimiento
     */
    @QueryParam("fechaVencimiento")
    private Date fechaVencimiento;

    /**
     * Fecha de inicio
     */
    @QueryParam("fechaInicio")
    private Date fechaInicio;

    /**
     * Fecha fin
     */
    @QueryParam("fechaFin")
    private Date fechaFin;

    /**
     * Observación
     */
    @QueryParam("observacion")
    private String observacion;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idControlInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de control de inventario"));
        }

        if (isNull(idDepositoLogistico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador depósito logistico"));
        }

        if (isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de producto"));
        }

        if (isNull(cantidadInicial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad inicial"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {

        limpiarErrores();

        if (isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }

        if (isNull(idControlInventario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de control de inventario"));
        }

        if (isNull(idDepositoLogistico)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador depósito logistico"));
        }

        if (isNull(idProducto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de producto"));
        }

        if (isNull(cantidadInicial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad inicial"));
        }

        return !tieneErrores();
    }

    public Integer getIdControlInventario() {
        return idControlInventario;
    }

    public void setIdControlInventario(Integer idControlInventario) {
        this.idControlInventario = idControlInventario;
    }

    public Integer getIdDepositoLogistico() {
        return idDepositoLogistico;
    }

    public void setIdDepositoLogistico(Integer idDepositoLogistico) {
        this.idDepositoLogistico = idDepositoLogistico;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getCantidadInicial() {
        return cantidadInicial;
    }

    public void setCantidadInicial(Integer cantidadInicial) {
        this.cantidadInicial = cantidadInicial;
    }

    public Integer getCantidadFinal() {
        return cantidadFinal;
    }

    public void setCantidadFinal(Integer cantidadFinal) {
        this.cantidadFinal = cantidadFinal;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }
}
