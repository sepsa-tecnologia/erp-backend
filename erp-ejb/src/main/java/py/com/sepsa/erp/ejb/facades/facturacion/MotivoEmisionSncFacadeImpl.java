/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionSnc;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.MotivoEmisionSncParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "MotivoEmisionSncFacade", mappedName = "MotivoEmisionSncFacade")
@Local(MotivoEmisionSncFacade.class)
public class MotivoEmisionSncFacadeImpl extends FacadeImpl<MotivoEmisionSnc, MotivoEmisionSncParam> implements MotivoEmisionSncFacade {

    public MotivoEmisionSncFacadeImpl() {
        super(MotivoEmisionSnc.class);
    }
    
    public Boolean validToCreate(MotivoEmisionSncParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        MotivoEmisionSncParam param1 = new MotivoEmisionSncParam();
        param1.setCodigoXml(param.getCodigoXml().trim());
        param1.setCodigoTxt(param.getCodigoTxt().trim());
        
        MotivoEmisionSnc mesnc = findFirst(param1);
        
        if(!isNull(mesnc)) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(MotivoEmisionSncParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        MotivoEmisionSnc item = find(param.getId());
        
        if(isNull(item)) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el registro"));
            
        } else if (!item.getCodigoXml().equals(param.getCodigoXml().trim())
                || !item.getCodigoTxt().equals(param.getCodigoTxt().trim())) {
            
            MotivoEmisionSncParam param1 = new MotivoEmisionSncParam();
            param1.setCodigoXml(param.getCodigoXml().trim());
            param1.setCodigoTxt(param.getCodigoTxt().trim());

            MotivoEmisionSnc mei = findFirst(param1);

            if(!isNull(mei)) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }

    @Override
    public MotivoEmisionSnc create(MotivoEmisionSncParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        MotivoEmisionSnc result = new MotivoEmisionSnc();
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigoXml(param.getCodigoXml().trim());
        result.setCodigoTxt(param.getCodigoTxt().trim());
        result.setActivo(param.getActivo());
        
        create(result);
        
        return result;
    }

    @Override
    public MotivoEmisionSnc edit(MotivoEmisionSncParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        MotivoEmisionSnc result = find(param.getId());
        result.setDescripcion(param.getDescripcion().trim());
        result.setCodigoXml(param.getCodigoXml().trim());
        result.setCodigoTxt(param.getCodigoTxt().trim());
        result.setActivo(param.getActivo());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public MotivoEmisionSnc find(Integer id, String codigoXml, String codigoTxt) {
        
        MotivoEmisionSncParam param = new MotivoEmisionSncParam();
        param.setId(id);
        param.setCodigoXml(param.getCodigoXml().trim());
        param.setCodigoTxt(param.getCodigoTxt().trim());
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<MotivoEmisionSnc> find(MotivoEmisionSncParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(MotivoEmisionSnc.class);
        Root<MotivoEmisionSnc> root = cq.from(MotivoEmisionSnc.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getCodigoXml() != null && !param.getCodigoXml().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoXml"), param.getCodigoXml().trim()));
        }
        
        if (param.getCodigoTxt() != null && !param.getCodigoTxt().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoTxt"), param.getCodigoTxt().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }
        
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());

        List<MotivoEmisionSnc> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(MotivoEmisionSncParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<MotivoEmisionSnc> root = cq.from(MotivoEmisionSnc.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getCodigoXml() != null && !param.getCodigoXml().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoXml"), param.getCodigoXml().trim()));
        }
        
        if (param.getCodigoTxt() != null && !param.getCodigoTxt().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigoTxt"), param.getCodigoTxt().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
}
