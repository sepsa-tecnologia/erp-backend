/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;

/**
 * Clase para el maenjo de las utilidades de repositorio de imagenes
 *
 * @author Jonathan D. Bernal Fernández
 */
@Log4j2
public class AwsUtils {
    
    /**
     * Método para subir un archivo al repositorio
     * @param repositorio Repositorio
     * @param fileName Nombre el archivo
     * @param fileStream Stream
     * @param fileSize Tamaño del archivo
     * @param contentType Tipo de contenido
     * @return Estado de resultado
     */
    public static boolean upload(Repositorio repositorio, String fileName,
            InputStream fileStream, long fileSize, String contentType) {

        boolean status = false;
        
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                repositorio.getClaveAcceso(),
                repositorio.getClaveSecreta());

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(repositorio.getRegion())
                .build();

        try {

            ObjectMetadata s3ObjectMetadata = new ObjectMetadata();
            s3ObjectMetadata.setContentLength(fileSize);
            s3ObjectMetadata.setContentType(contentType);
            
            byte[] bytes = IOUtils.toByteArray(fileStream);
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(bytes);
            
            InputStream firstClone = new ByteArrayInputStream(baos.toByteArray()); 
            
            s3Client.putObject(new PutObjectRequest(repositorio.getBucket(), fileName, firstClone, s3ObjectMetadata)
                    .withCannedAcl(CannedAccessControlList.Private));
            
            status = true;
        } catch (Exception e) {
            log.fatal("Se produjo un error", e);
        }

        return status;
    }
    
    /**
     * Obtiene un archivo 
     * @param repositorio Repositorio
     * @param fileName Nombre del archivo
     * @return bytes del archivo
     * @throws IOException 
     */
    public static byte[] getFile(Repositorio repositorio, String fileName) throws IOException {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(
                repositorio.getClaveAcceso(),
                repositorio.getClaveSecreta());
        
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(repositorio.getRegion())
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                    .build();
        
        S3Object s3Object = s3Client.getObject(new GetObjectRequest(repositorio.getBucket(), fileName));
        return IOUtils.toByteArray(s3Object.getObjectContent());
    }
}
