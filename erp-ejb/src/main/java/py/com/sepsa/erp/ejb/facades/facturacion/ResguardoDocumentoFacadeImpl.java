/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.ResguardoDocumento;
import py.com.sepsa.erp.ejb.entities.facturacion.TipoDocumento;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ResguardoDocumentoParam;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.aws.S3Utils;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ResguardoDocumentoFacade", mappedName = "ResguardoDocumentoFacade")
@Local(ResguardoDocumentoFacade.class)
public class ResguardoDocumentoFacadeImpl extends FacadeImpl<ResguardoDocumento, ResguardoDocumentoParam> implements ResguardoDocumentoFacade {

    public ResguardoDocumentoFacadeImpl() {
        super(ResguardoDocumento.class);
    }
    
    /**
     * Verifica si el objeto es válido para crear
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToCreate(ResguardoDocumentoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoDocumento tipoDocumento = facades
                .getTipoDocumentoFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento());
        
        if(tipoDocumento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el tipo de documento"));
        } else {
            param.setIdTipoDocumento(tipoDocumento.getId());
            param.setCodigoTipoDocumento(tipoDocumento.getCodigo());
        }
        
        Repositorio repositorio = facades
                .getRepositorioFacade()
                .find(param.getIdRepositorio());
        
        if(repositorio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el repositorio"));
        }
        
        return !param.tieneErrores();
    }
    
    /**
     * Verifica si el objeto es válido para editar
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(ResguardoDocumentoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoDocumento tipoDocumento = facades
                .getTipoDocumentoFacade()
                .find(param.getIdTipoDocumento(), param.getCodigoTipoDocumento());
        
        if(tipoDocumento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el tipo de documento"));
        } else {
            param.setIdTipoDocumento(tipoDocumento.getId());
            param.setCodigoTipoDocumento(tipoDocumento.getCodigo());
        }
        
        Repositorio repositorio = facades
                .getRepositorioFacade()
                .find(param.getIdRepositorio());
        
        if(repositorio == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el repositorio"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToGenerateDownloadUrl(ResguardoDocumentoParam param) {
        
        if(!param.isValidToGenerateDownloadUrl()) {
            return Boolean.FALSE;
        }
        
        ResguardoDocumento resguardoDocumento = find(param.getId());
        
        if(resguardoDocumento == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de resguardo de documento"));
        } else {
            param.setUrl(resguardoDocumento.getUrl());
            param.setIdRepositorio(resguardoDocumento.getIdRepositorio());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public String generateDownloadUrl(ResguardoDocumentoParam param) throws IOException {
        
        if(!validToGenerateDownloadUrl(param)) {
            return null;
        }
        
        Repositorio repositorio = facades.getRepositorioFacade()
                .find(param.getIdRepositorio());
        
        String result = S3Utils.getPresignedUrl(repositorio.getClaveAcceso(),
                repositorio.getClaveSecreta(), repositorio.getRegion(),
                repositorio.getBucket(), param.getUrl(), 30000);
        
        return result;
    }
    
    /**
     * Crea una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public ResguardoDocumento create(ResguardoDocumentoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        ResguardoDocumento result = new ResguardoDocumento();
        result.setIdEmpresa(userInfo.getIdEmpresa());
        result.setIdTipoDocumento(param.getIdTipoDocumento());
        result.setIdRepositorio(param.getIdRepositorio());
        result.setNombreArchivo(param.getNombreArchivo());
        result.setUrl(param.getUrl());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        create(result);
        
        return result;
    }
    
    /**
     * Edita una instancia
     * @param param parámetros
     * @return Instancia
     */
    @Override
    public ResguardoDocumento edit(ResguardoDocumentoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        ResguardoDocumento result = find(param.getId());
        result.setIdTipoDocumento(param.getIdTipoDocumento());
        result.setIdRepositorio(param.getIdRepositorio());
        result.setNombreArchivo(param.getNombreArchivo());
        result.setUrl(param.getUrl());
        edit(result);
        
        return result;
    }
    
    @Override
    public List<ResguardoDocumento> find(ResguardoDocumentoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(ResguardoDocumento.class);
        Root<ResguardoDocumento> root = cq.from(ResguardoDocumento.class);
        Join<ResguardoDocumento, TipoDocumento> tipoDocumento = root.join("tipoDocumento");
        Join<ResguardoDocumento, Empresa> empresa = root.join("empresa");
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdTipoDocumento() != null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        if(param.getCodigoTipoDocumento() != null && !param.getCodigoTipoDocumento().trim().isEmpty()) {
            predList.add(qb.equal(tipoDocumento.get("codigo"), param.getCodigoTipoDocumento().trim()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("fechaInsercion")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize());
        
        List<ResguardoDocumento> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ResguardoDocumentoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<ResguardoDocumento> root = cq.from(ResguardoDocumento.class);
        Join<ResguardoDocumento, TipoDocumento> tipoDocumento = root.join("tipoDocumento");
        Join<ResguardoDocumento, Empresa> empresa = root.join("empresa");
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdTipoDocumento() != null) {
            predList.add(qb.equal(root.get("idTipoDocumento"), param.getIdTipoDocumento()));
        }
        
        if(param.getCodigoTipoDocumento() != null && !param.getCodigoTipoDocumento().trim().isEmpty()) {
            predList.add(qb.equal(tipoDocumento.get("codigo"), param.getCodigoTipoDocumento().trim()));
        }
        
        if(param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }
        
        if(param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(empresa.get("codigo"), param.getCodigoEmpresa().trim()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
