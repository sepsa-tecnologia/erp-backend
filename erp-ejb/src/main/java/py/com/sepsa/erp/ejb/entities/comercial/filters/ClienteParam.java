/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import org.jboss.resteasy.annotations.providers.multipart.PartType;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de la clase cliente
 * @author Jonathan Bernal
 */
public class ClienteParam extends CommonParam {

    public ClienteParam() {
    }

    public ClienteParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de comercial
     */
    @QueryParam("idComercial")
    private Integer idComercial;
    
    /**
     * Porcentajde de retención
     */
    @QueryParam("porcentajeRetencion")
    private Integer porcentajeRetencion;
    
    /**
     * Nro de documento
     */
    @QueryParam("nroDocumento")
    private String nroDocumento;
    
    /**
     * Nro de documento eq
     */
    @QueryParam("nroDocumentoEq")
    private String nroDocumentoEq;
    
    /**
     * Identificador de tipo de documento
     */
    @QueryParam("idTipoDocumento")
    private Integer idTipoDocumento;
    
    /**
     * Codigo de tipo de documento
     */
    @QueryParam("codigoTipoDocumento")
    private String codigoTipoDocumento;
    
    /**
     * Identificador de naturaleza de cliente
     */
    @QueryParam("idNaturalezaCliente")
    private Integer idNaturalezaCliente;
    
    /**
     * Codigo de naturaleza de cliente
     */
    @QueryParam("codigoNaturalezaCliente")
    private String codigoNaturalezaCliente;
    
    /**
     * Identificador de canal de venta
     */
    @QueryParam("idCanalVenta")
    private Integer idCanalVenta;
    
    /**
     * Código de canal de venta
     */
    @QueryParam("codigoCanalVenta")
    private String codigoCanalVenta;
    
    /**
     * Proveedor
     */
    @QueryParam("proveedor")
    private Character proveedor;
    
    /**
     * Comprador
     */
    @QueryParam("comprador")
    private Character comprador;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * Clave de pago
     */
    @QueryParam("clavePago")
    private String clavePago;
    
    /**
     * Facturación electrónica
     */
    @QueryParam("factElectronica")
    private Character factElectronica;
    
    /**
     * Código de país
     */
    @QueryParam("codigoPais")
    private String codigoPais;
    
    @QueryParam("rucDuplicado")
    private boolean rucDuplicado;
    
    @FormParam("uploadedFile")
    @PartType("application/octet-stream")
    private byte[] uploadedFileBytes;
    
    private PersonaParam persona;
    
    public boolean isValidToGetByNroDocumento() {
        limpiarErrores();
        
        if(isNullOrEmpty(nroDocumentoEq)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de documento"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idCliente) && isNull(persona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de cliente o información de la persona"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de empresa"));
        }

        if(isNullOrEmpty(razonSocial) && isNull(persona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la razón social"));
        }
        
        if(isNull(factElectronica)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el cliente es para facturación electrónica"));
        }
        
        if(isNull(idTipoDocumento) && isNullOrEmpty(codigoTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de tipo de documento"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador/código de estado del cliente"));
        }
        
        if(isNull(idNaturalezaCliente) && isNullOrEmpty(codigoNaturalezaCliente)) {
            codigoNaturalezaCliente = "CONTRIBUYENTE";
        }
        
        if(isNullOrEmpty(nroDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el nro de documento"));
            }
        
        if (isNullOrEmpty(codigoPais)){
            codigoPais = "PRY";
        }
        
        if(!isNull(persona)) {
            persona.setIdEmpresa(idEmpresa);
            persona.isValidToCreate();
        }
        
        if (isNull(rucDuplicado)){
            rucDuplicado = false;
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(persona)) {
            if(persona.tieneErrores()) {
                list.addAll(persona.getErrores());
            }
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdComercial() {
        return idComercial;
    }

    public void setIdComercial(Integer idComercial) {
        this.idComercial = idComercial;
    }

    public Integer getPorcentajeRetencion() {
        return porcentajeRetencion;
    }

    public void setPorcentajeRetencion(Integer porcentajeRetencion) {
        this.porcentajeRetencion = porcentajeRetencion;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public void setClavePago(String clavePago) {
        this.clavePago = clavePago;
    }

    public String getClavePago() {
        return clavePago;
    }

    public void setFactElectronica(Character factElectronica) {
        this.factElectronica = factElectronica;
    }

    public Character getFactElectronica() {
        return factElectronica;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public Character getProveedor() {
        return proveedor;
    }

    public void setProveedor(Character proveedor) {
        this.proveedor = proveedor;
    }

    public Character getComprador() {
        return comprador;
    }

    public void setComprador(Character comprador) {
        this.comprador = comprador;
    }

    public void setCodigoCanalVenta(String codigoCanalVenta) {
        this.codigoCanalVenta = codigoCanalVenta;
    }

    public String getCodigoCanalVenta() {
        return codigoCanalVenta;
    }

    public void setPersona(PersonaParam persona) {
        this.persona = persona;
    }

    public PersonaParam getPersona() {
        return persona;
    }

    public void setNroDocumentoEq(String nroDocumentoEq) {
        this.nroDocumentoEq = nroDocumentoEq;
    }

    public String getNroDocumentoEq() {
        return nroDocumentoEq;
    }

    public void setCodigoNaturalezaCliente(String codigoNaturalezaCliente) {
        this.codigoNaturalezaCliente = codigoNaturalezaCliente;
    }

    public String getCodigoNaturalezaCliente() {
        return codigoNaturalezaCliente;
    }

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public byte[] getUploadedFileBytes() {
        return uploadedFileBytes;
    }

    public void setUploadedFileBytes(byte[] uploadedFileBytes) {
        this.uploadedFileBytes = uploadedFileBytes;
    }

    public boolean isRucDuplicado() {
        return rucDuplicado;
    }

    public void setRucDuplicado(boolean rucDuplicado) {
        this.rucDuplicado = rucDuplicado;
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idCliente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de cliente"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de empresa"));
        }
        
        if(isNull(idTipoDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de tipo de documento"));
        }
        
        if(isNull(factElectronica)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si el cliente es para facturación electrónica"));
        }
        
        if(isNull(idEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el identificador de estado del cliente"));
        }
        
        if(isNullOrEmpty(nroDocumento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere el nro de documento"));
        }
               
        if(isNullOrEmpty(clavePago)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la clave de pago"));
        }
        
        if(isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se requiere la razón social"));
        }
        
        if(isNull(idNaturalezaCliente) && isNullOrEmpty(codigoNaturalezaCliente)) {
            codigoNaturalezaCliente = "CONTRIBUYENTE";
        }
        
        return !tieneErrores();
    }
}
