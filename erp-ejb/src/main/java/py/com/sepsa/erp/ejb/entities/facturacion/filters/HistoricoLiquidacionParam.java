/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de historico liquidacion
 * @author Jonathan D. Bernal Fernández
 */
public class HistoricoLiquidacionParam extends CommonParam {
    
    public HistoricoLiquidacionParam() {
    }

    public HistoricoLiquidacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Bandera que indica que es el actual
     */
    @QueryParam("actual")
    private Boolean actual;
    
    /**
     * Identificador de cliente
     */
    @QueryParam("idCliente")
    private Integer idCliente;
    
    /**
     * Identificador de historico liquidacion
     */
    @QueryParam("idHistoricoLiquidacion")
    private Integer idHistoricoLiquidacion;
    
    /**
     * Fecha de proceso
     */
    @QueryParam("fechaProceso")
    private Date fechaProceso;
    
    /**
     * Monto tarifa detalle
     */
    @QueryParam("montoTarifaDetalle")
    private BigDecimal montoTarifaDetalle;
    
    /**
     * Monto descuentp detalle
     */
    @QueryParam("montoDescuentoDetalle")
    private BigDecimal montoDescuentoDetalle;
    
    /**
     * Monto total detalle
     */
    @QueryParam("montoTotalDetalle")
    private BigDecimal montoTotalDetalle;
    
    /**
     * Monto total excedente
     */
    @QueryParam("montoTotalExcedente")
    private BigDecimal montoTotalExcedente;
    
    /**
     * Monto descuentp excedente
     */
    @QueryParam("montoDescuentoExcedente")
    private BigDecimal montoDescuentoExcedente;
    
    /**
     * Monto descuento general
     */
    @QueryParam("montoDescuentoGeneral")
    private BigDecimal montoDescuentoGeneral;
    
    /**
     * Monto total parcial
     */
    @QueryParam("montoTotalParcial")
    private BigDecimal montoTotalParcial;
    
    /**
     * Monto total liquidado
     */
    @QueryParam("montoTotalLiquidado")
    private BigDecimal montoTotalLiquidado;
    
    /**
     * Monto acuerdo
     */
    @QueryParam("montoAcuerdo")
    private Character montoAcuerdo;
    
    /**
     * Monto minimo
     */
    @QueryParam("montoMinimo")
    private Character montoMinimo;
    
    /**
     * Identificador de monto minimo
     */
    @QueryParam("idMontoMinimo")
    private Integer idMontoMinimo;
    
    /**
     * Cobro adelantado
     */
    @QueryParam("cobroAdelantado")
    private Character cobroAdelantado;
    
    /**
     * Bonificado
     */
    @QueryParam("bonificado")
    private Character bonificado;
    
    /**
     * Identificador de liquidacion
     */
    @QueryParam("idLiquidacion")
    private Integer idLiquidacion;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Ano
     */
    @QueryParam("ano")
    private String ano;
    
    /**
     * Mes
     */
    @QueryParam("mes")
    private String mes;
    
    /**
     * Identificador de periodo de liquidación
     */
    @QueryParam("idPeriodoLiquidacion")
    private Integer idPeriodoLiquidacion;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Detalle de liquidación
     */
    private List<LiquidacionDetalleParam> liquidacionDetalles;
    
    /**
     * Detalle de excedente
     */
    private List<ExcedenteDetalleParam> excedenteDetalles;
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    /**
     * Verifica si el objeto es válido para listar la lista de liquidaciones por periodo
     * @return Bandera
     */
    public Boolean isValidToListPeriodo() {
        
        limpiarErrores();
        
        if(isNull(mes)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el mes"));
        }
        
        if(isNull(ano)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el año"));
        }
        
        super.isValidToList();
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(montoTarifaDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto tarifa detalle"));
        }
        
        if(isNull(montoDescuentoDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto descuento detalle"));
        }
        
        if(isNull(montoTotalDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total detalle"));
        }
        
        if(isNull(montoTotalExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excedente"));
        }
        
        if(isNull(montoDescuentoExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto descuento excedente"));
        }
        
        if(isNull(montoDescuentoGeneral)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto descuento general"));
        }
        
        if(isNull(montoTotalParcial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total parcial"));
        }
        
        if(isNull(montoTotalLiquidado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total liquidado"));
        }
        
        if(isNull(montoAcuerdo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es monto acuerdo"));
        }
        
        if(isNull(montoMinimo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es monto minimo"));
        } else if(montoMinimo.equals('S') && isNull(idMontoMinimo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de monto minimo"));
        }
        
        if(isNull(cobroAdelantado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un cobro adelantado"));
        }
        
        if(isNull(bonificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es bonificado"));
        }
        
        if(isNull(idLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de liquidación"));
        }
        
        if(isNullOrEmpty(liquidacionDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de liquidación"));
        } else {
            for (LiquidacionDetalleParam item : liquidacionDetalles) {
                item.setIdHistoricoLiquidacion(0);
                item.setIdEmpresa(idEmpresa);
                item.isValidToCreate();
            }
        }
        
        if(!isNull(excedenteDetalles)) {
            for (ExcedenteDetalleParam item : excedenteDetalles) {
                item.setIdHistoricoLiquidacion(0);
                item.setIdEmpresa(idEmpresa);
                item.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        List<MensajePojo> result = super.getCustomErrores();
        
        if(!isNull(liquidacionDetalles)) {
            for (LiquidacionDetalleParam item : liquidacionDetalles) {
                result.addAll(item.getErrores());
            }
        }
        
        if(!isNull(excedenteDetalles)) {
            for (ExcedenteDetalleParam item : excedenteDetalles) {
                result.addAll(item.getErrores());
            }
        }
        
        return result;
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(idHistoricoLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de historico liquidación"));
        }
        
        if(isNull(fechaProceso)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha del proceso"));
        }
        
        if(isNull(montoTotalDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total detalle"));
        }
        
        if(isNull(montoTotalExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excedente"));
        }
        
        if(isNull(montoDescuentoGeneral)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto descuento general"));
        }
        
        if(isNull(montoDescuentoExcedente)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto descuento excedente"));
        }
        
        if(isNull(montoDescuentoDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto descuento detalle"));
        }
        
        if(isNull(montoTotalLiquidado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total liquidado"));
        }
        
        if(isNull(montoMinimo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto minimo"));
        } else if(montoMinimo.equals('S') && isNull(idMontoMinimo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de monto minimo"));
        }
        
        if(isNull(cobroAdelantado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es un cobro adelantado"));
        }
        
        if(isNull(bonificado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si es bonificado"));
        }
        
        if(isNull(idLiquidacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de liquidación"));
        }
        
        return !tieneErrores();
    }
    
    public Integer getIdHistoricoLiquidacion() {
        return idHistoricoLiquidacion;
    }

    public void setIdHistoricoLiquidacion(Integer idHistoricoLiquidacion) {
        this.idHistoricoLiquidacion = idHistoricoLiquidacion;
    }

    public Date getFechaProceso() {
        return fechaProceso;
    }

    public void setFechaProceso(Date fechaProceso) {
        this.fechaProceso = fechaProceso;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setBonificado(Character bonificado) {
        this.bonificado = bonificado;
    }

    public Character getBonificado() {
        return bonificado;
    }

    public Character getMontoMinimo() {
        return montoMinimo;
    }

    public void setMontoMinimo(Character montoMinimo) {
        this.montoMinimo = montoMinimo;
    }

    public Integer getIdMontoMinimo() {
        return idMontoMinimo;
    }

    public void setIdMontoMinimo(Integer idMontoMinimo) {
        this.idMontoMinimo = idMontoMinimo;
    }

    public Character getCobroAdelantado() {
        return cobroAdelantado;
    }

    public void setCobroAdelantado(Character cobroAdelantado) {
        this.cobroAdelantado = cobroAdelantado;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setMes(String mes) {
        this.mes = mes;
}

    public String getMes() {
        return mes;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getAno() {
        return ano;
    }

    public BigDecimal getMontoTotalDetalle() {
        return montoTotalDetalle;
    }

    public void setMontoTotalDetalle(BigDecimal montoTotalDetalle) {
        this.montoTotalDetalle = montoTotalDetalle;
    }

    public BigDecimal getMontoTotalExcedente() {
        return montoTotalExcedente;
    }

    public void setMontoTotalExcedente(BigDecimal montoTotalExcedente) {
        this.montoTotalExcedente = montoTotalExcedente;
    }

    public BigDecimal getMontoTarifaDetalle() {
        return montoTarifaDetalle;
    }

    public void setMontoTarifaDetalle(BigDecimal montoTarifaDetalle) {
        this.montoTarifaDetalle = montoTarifaDetalle;
    }

    public BigDecimal getMontoTotalParcial() {
        return montoTotalParcial;
    }

    public void setMontoTotalParcial(BigDecimal montoTotalParcial) {
        this.montoTotalParcial = montoTotalParcial;
    }

    public Character getMontoAcuerdo() {
        return montoAcuerdo;
    }

    public void setMontoAcuerdo(Character montoAcuerdo) {
        this.montoAcuerdo = montoAcuerdo;
    }

    public BigDecimal getMontoDescuentoGeneral() {
        return montoDescuentoGeneral;
    }

    public void setMontoDescuentoGeneral(BigDecimal montoDescuentoGeneral) {
        this.montoDescuentoGeneral = montoDescuentoGeneral;
    }

    public BigDecimal getMontoDescuentoExcedente() {
        return montoDescuentoExcedente;
    }

    public void setMontoDescuentoExcedente(BigDecimal montoDescuentoExcedente) {
        this.montoDescuentoExcedente = montoDescuentoExcedente;
    }

    public BigDecimal getMontoDescuentoDetalle() {
        return montoDescuentoDetalle;
    }

    public void setMontoDescuentoDetalle(BigDecimal montoDescuentoDetalle) {
        this.montoDescuentoDetalle = montoDescuentoDetalle;
    }

    public BigDecimal getMontoTotalLiquidado() {
        return montoTotalLiquidado;
    }

    public void setMontoTotalLiquidado(BigDecimal montoTotalLiquidado) {
        this.montoTotalLiquidado = montoTotalLiquidado;
    }

    public void setLiquidacionDetalles(List<LiquidacionDetalleParam> liquidacionDetalles) {
        this.liquidacionDetalles = liquidacionDetalles;
    }

    public void setIdPeriodoLiquidacion(Integer idPeriodoLiquidacion) {
        this.idPeriodoLiquidacion = idPeriodoLiquidacion;
    }

    public Integer getIdPeriodoLiquidacion() {
        return idPeriodoLiquidacion;
    }

    public List<LiquidacionDetalleParam> getLiquidacionDetalles() {
        return liquidacionDetalles;
    }

    public void setExcedenteDetalles(List<ExcedenteDetalleParam> excedenteDetalles) {
        this.excedenteDetalles = excedenteDetalles;
    }

    public List<ExcedenteDetalleParam> getExcedenteDetalles() {
        return excedenteDetalles;
    }
}
