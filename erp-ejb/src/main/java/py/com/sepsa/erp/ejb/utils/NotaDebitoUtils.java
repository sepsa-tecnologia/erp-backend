/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.input.BOMInputStream;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.comercial.Moneda;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.comercial.filters.NaturalezaClienteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmision;
import py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.MotivoEmisionInternoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.NotaDebitoNotificacionParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TipoCambioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TipoCambioPojo;
import py.com.sepsa.erp.ejb.entities.info.filters.ProductoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ProductoPojo;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Antonella Lucero
 */
public class NotaDebitoUtils {

    public static List<NotaDebitoParam> convertToList(Facades facades, NotaDebitoParam param) {
        List<NotaDebitoParam> lista = new ArrayList<>();
        try {

            List<NotaDebitoParam> ncl = new ArrayList<>();

            InputStreamReader input = new InputStreamReader(new BOMInputStream(new ByteArrayInputStream(param.getUploadedFileBytes()), false),StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(input);
            List<String> lineas = Lists.empty();
            String line;
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                Pattern pattern = Pattern.compile(";|,");
                Matcher mather = pattern.matcher(line);
                if (mather.find() == true) {
                    lineas.add(line);
                }
            }
            for (int i = 0; i < lineas.size(); i++) {
                if (lineas.get(i) != null && !lineas.get(i).trim().isEmpty()) {
                    NotaDebitoParam nInicial = new NotaDebitoParam();
                    int errores = 0;
                    String[] stringArr = lineas.get(i).split(";|,");
                    String tipoLinea = stringArr.length <= 0 ? "" : stringArr[0];
                    String timb = stringArr.length <= 1 ? "" : stringArr[1];
                    String nroN = stringArr.length <= 2 ? "" : stringArr[2];
                    if (timb != null && !timb.trim().isEmpty()) {
                        nInicial.setTimbrado(timb);
                    } else {
                        lista.add(nInicial);
                        nInicial.setTimbrado("-");
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de timbrado de la nota de débito"));
                        errores += 1;
                    }
                    if (nroN != null && !nroN.trim().isEmpty()) {
                        nInicial.setNroNotaDebito(nroN);
                    } else {
                        nInicial.setNroNotaDebito("-");
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar el nro de la nota de débito"));
                        errores += 1;
                    }
                    if (!(tipoLinea != null && !tipoLinea.trim().isEmpty())) {
                        if (!lista.contains(nInicial)) {
                            lista.add(nInicial);
                        }
                        nInicial.addError(MensajePojo.createInstance().descripcion("Se debe ingresar un Tipo de Línea"));
                        errores += 1;
                        continue;
                    } else {
                        if (errores > 0) {
                            continue;
                        }
                        //Datos de Cabecera
                        String timbrado = null;
                        String nroNDebito = null;
                        String fechaNd = null;
                        String codMoneda = null;
                        String nroDocumento = null;
                        String razonSocial = null;
                        String codSeguridad = null;
                        String codMotivoEmision = null;
                        String tipoCambio = null;
                        //Datos de Detalle
                        String timbradoFactura = null;
                        String nroFactura = null;
                        String fechaFactura = null;
                        String cdcFactura = null;
                        String codProducto = null;
                        String porcentajeImpuesto = null;
                        String porcentajeGrav = null;
                        String precioUnitarioConIva = null;
                        String descuentoUnitario = null;
                        String cantidad = null;
                        String descripcion = null;
                        String datoAdicional = null;
                        String email = null;
                        NotaDebitoParam nd = new NotaDebitoParam();
                        NotaDebitoDetalleParam ndd = new NotaDebitoDetalleParam();
                        switch (tipoLinea) {
                            case "1":
                                //TODO cabecera
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroNDebito = (String) stringArr[k];
                                            break;
                                        case 3:
                                            fechaNd = (String) stringArr[k];
                                            break;
                                        case 4:
                                            codMoneda = (String) stringArr[k];
                                            break;
                                        case 5:
                                            nroDocumento = (String) stringArr[k];
                                            break;
                                        case 6:
                                            razonSocial = (String) stringArr[k];
                                            break;
                                        case 7:
                                            codSeguridad = (String) stringArr[k];
                                            break;
                                        case 8:
                                            codMotivoEmision = (String) stringArr[k];
                                            break;
                                        case 9:
                                            tipoCambio = (String) stringArr[k];
                                            break;
                                        case 10:
                                            email = (String) stringArr[k];
                                    }
                                }
                                nd.setArchivoSet('S');
                                nd.setIdEmpresa(param.getIdEmpresa());
                                nd.setIdUsuario(param.getIdUsuario());
                                nd.setIdEncargado(param.getIdUsuario());
                                nd.setTimbrado(timbrado);
                                nd.setSgteNumeroND(Boolean.FALSE);
                                nd.setNroNotaDebito(nroNDebito);
                                if (nd.getNroNotaDebito() != null) {
                                    if (nd.getNroNotaDebito().length() != 15 || !(nd.getNroNotaDebito().matches("^[0-9]{3}[\\-][0-9]{3}[\\-][0-9]{7}$"))) {
                                        lista.add(nd);
                                        nd.addError(MensajePojo.createInstance().descripcion("El formato del nro de nota de débito de la cabecera es inválido"));
                                        continue;
                                    }
                                }
                                if (fechaNd != null && !fechaNd.trim().isEmpty()) {
                                    nd.setFecha(new SimpleDateFormat("dd/MM/yyyy").parse(fechaNd));
                                }
                                if (codMoneda != null && !codMoneda.trim().isEmpty()) {
                                    Moneda moneda = facades.getMonedaFacade().find(nd.getIdMoneda(), codMoneda);
                                    if (moneda == null) {
                                        lista.add(nd);
                                        nd.addError(MensajePojo.createInstance().descripcion("No existe la moneda"));
                                        continue;
                                    } else {
                                        nd.setIdMoneda(moneda.getId());
                                        nd.setCodigoMoneda(moneda.getCodigo());
                                        if (nd.getCodigoMoneda().equalsIgnoreCase("PYG")) {
                                            nd.setIdTipoCambio(null);
                                        } else {
                                            if (tipoCambio != null && !tipoCambio.trim().isEmpty()) {
                                                TipoCambioParam cambio = new TipoCambioParam();
                                                cambio.setCodigoMoneda(nd.getCodigoMoneda());
                                                try {
                                                    cambio.setCompra(new BigDecimal(tipoCambio.trim().replace(" ", "")));
                                                } catch (NumberFormatException e) {
                                                    String camb = tipoCambio.replace("\"", "").trim().replace(" ", "");

                                                    DecimalFormat decimalFormat = new DecimalFormat();
                                                    decimalFormat.setParseBigDecimal(true);
                                                    Number parsedCambio = decimalFormat.parse(camb);
                                                    BigDecimal bdCambio = (BigDecimal) parsedCambio;

                                                    cambio.setCompra(bdCambio);
                                                    cambio.setVenta(bdCambio);
                                                }
                                                cambio.setActivo('S');
                                                TipoCambioPojo tipoCambioM = facades.getTipoCambioFacade().findFirstPojo(cambio);
                                                if (tipoCambioM != null) {
                                                    nd.setIdTipoCambio(tipoCambioM.getId());
                                                } else {
                                                    lista.add(nd);
                                                    nd.addError(MensajePojo.createInstance().descripcion("Tipo de Cambio Inexistente"));
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (email != null && !email.trim().isEmpty()) {
                                    int j = 0;
                                    List<NotaDebitoNotificacionParam> listEmail = new ArrayList();
                                    for (StringTokenizer stringTokenizer = new StringTokenizer(email, "-"); stringTokenizer.hasMoreTokens(); j++) {
                                        String token = stringTokenizer.nextToken();
                                        NotaDebitoNotificacionParam ncnp = new NotaDebitoNotificacionParam();
                                        ncnp.setEmail(token);
                                        ncnp.setCodigoTipoNotificacion("APROBACION_DTE");
                                        listEmail.add(ncnp);
                                    }
                                    nd.setNotaDebitoNotificaciones(listEmail);
                                }
                                nd.setRuc(nroDocumento.replace(".", ""));
                                nd.setRazonSocial(razonSocial);
                                if (nd.getRazonSocial() != null && !nd.getRazonSocial().trim().isEmpty() && nd.getRuc() != null && !nd.getRuc().trim().isEmpty()) {
                                    if (nd.getRuc().contains("-")) {
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if (n != null) {
                                            nd.setIdNaturalezaCliente(n.getId());
                                        }
                                    } else {
                                        NaturalezaClienteParam nat = new NaturalezaClienteParam();
                                        nat.setCodigo("NO_CONTRIBUYENTE");
                                        NaturalezaCliente n = facades.getNaturalezaClienteFacade().findFirst(nat);
                                        if (n != null) {
                                            nd.setIdNaturalezaCliente(n.getId());
                                        }
                                    }
                                    ClienteParam paramc = new ClienteParam();
                                    paramc.setIdEmpresa(nd.getIdEmpresa());
                                    paramc.setNroDocumentoEq(nd.getRuc());
                                    paramc.setRazonSocial(nd.getRazonSocial());
                                    Cliente cliente = facades.getClienteFacade().findFirst(paramc);
                                    if (cliente != null) {
                                        nd.setIdCliente(cliente.getIdCliente());
                                        nd.setIdNaturalezaCliente(cliente.getIdNaturalezaCliente());
                                    }
                                }
                                nd.setCodSeguridad(codSeguridad);
                                if (codMotivoEmision != null && !codMotivoEmision.trim().isEmpty()) {
                                    MotivoEmision motivoEmision = facades.getMotivoEmisionFacade().find(nd.getIdMotivoEmision(), codMotivoEmision);
                                    if (motivoEmision != null) {
                                        nd.setIdMotivoEmision(motivoEmision.getId());
                                        MotivoEmisionInternoParam motivIntern = new MotivoEmisionInternoParam();
                                        motivIntern.setActivo('S');
                                        motivIntern.setIdMotivoEmision(nd.getIdMotivoEmision());
                                        motivIntern.setIdEmpresa(nd.getIdEmpresa());
                                        MotivoEmisionInterno motivoEmisionInterno = facades.getMotivoEmisionInternoFacade().findFirst(motivIntern);
                                        if (motivoEmisionInterno != null) {
                                            nd.setIdMotivoEmisionInterno(motivoEmisionInterno.getId());
                                            nd.setCodigoMotivoEmisionInterno(motivoEmisionInterno.getCodigo());
                                        }
                                    }
                                }

                                UsuarioParam uparam = new UsuarioParam();
                                uparam.setId(param.getIdUsuario());
                                UsuarioPojo usuario = facades.getUsuarioFacade().findFirstPojo(uparam);
                                nd.setIdUsuario(usuario.getId());

                                TalonarioPojo talonario = null;
                                if (nd.getNroNotaDebito() != null && !nd.getNroNotaDebito().trim().isEmpty() && nd.getTimbrado() != null && !nd.getTimbrado().trim().isEmpty()) {
                                    TalonarioParam param2 = new TalonarioParam();
                                    param2.setIdEmpresa(nd.getIdEmpresa());
                                    param2.setTimbrado(nd.getTimbrado());
                                    param2.setActivo('S');
                                    param2.setNroSucursal(nd.getNroNotaDebito().substring(0, 3));
                                    param2.setNroPuntoVenta(nd.getNroNotaDebito().substring(4, 7));
                                    param2.setIdTipoDocumento(6);
                                    talonario = facades.getTalonarioFacade().findFirstPojo(param2);
                                    if (talonario != null) {
                                        nd.setIdTalonario(talonario.getId());
                                        nd.setDigital(talonario.getDigital());
                                    } else {
                                        lista.add(nd);
                                        nd.addError(MensajePojo.createInstance().descripcion("Nro de timbrado, nro de sucursal y nro de punto de venta inexistentes"));
                                        continue;
                                    }
                                }
                                ncl.add(nd);
                                break;
                            case "2":
                                //TODO detalle
                                for (int k = 0; k < stringArr.length; k++) {
                                    switch (k) {
                                        case 1:
                                            timbrado = (String) stringArr[k];
                                            break;
                                        case 2:
                                            nroNDebito = (String) stringArr[k];
                                            break;
                                        case 3:
                                            timbradoFactura = (String) stringArr[k];
                                            break;
                                        case 4:
                                            nroFactura = (String) stringArr[k];
                                            break;
                                        case 5:
                                            fechaFactura = (String) stringArr[k];
                                            break;
                                        case 6:
                                            cdcFactura = (String) stringArr[k];
                                            break;
                                        case 7:
                                            codProducto = (String) stringArr[k];
                                            break;
                                        case 8:
                                            porcentajeImpuesto = (String) stringArr[k];
                                            break;
                                        case 9:
                                            porcentajeGrav = (String) stringArr[k];
                                            break;
                                        case 10:
                                            precioUnitarioConIva = (String) stringArr[k];
                                            break;
                                        case 11:
                                            descuentoUnitario = (String) stringArr[k];
                                            break;
                                        case 12:
                                            cantidad = (String) stringArr[k];
                                            break;
                                        case 13:
                                            descripcion = (String) stringArr[k];
                                            break;
                                        case 14:
                                            datoAdicional = (String) stringArr[k];
                                            break;
                                    }
                                }
                                //guardar el detalle a la nc correspondiente   
                                boolean coincide = false;
                                for (NotaDebitoParam n : ncl) {
                                    if (n.getTimbrado() != null && !n.getTimbrado().trim().isEmpty() && timbrado != null && !timbrado.trim().isEmpty() && n.getNroNotaDebito() != null && !n.getNroNotaDebito().trim().isEmpty() && nroNDebito != null && !nroNDebito.trim().isEmpty()) {
                                        if (n.getTimbrado().equalsIgnoreCase(timbrado) && n.getNroNotaDebito().equalsIgnoreCase(nroNDebito)) {
                                            coincide = true;
                                            n.setDescripcion(descripcion);
                                             if (cdcFactura  != null && !cdcFactura.trim().isEmpty()){
                                                ndd.setFacturaDigital(n.getDigital());
                                            } else {
                                                ndd.setFacturaDigital('N');
                                            }
                                            n.getNotaDebitoDetalles().add(ndd);
                                            if (!lista.contains(n)) {
                                                lista.add(n);
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (coincide == false) {
                                    NotaDebitoParam nError = new NotaDebitoParam();
                                    if (nroNDebito != null && !nroNDebito.trim().isEmpty()) {
                                        nError.setNroNotaDebito(nroNDebito);
                                    } else {
                                        nError.setNroNotaDebito("-");
                                    }
                                    if (timbrado != null && !timbrado.trim().isEmpty()) {
                                        nError.setTimbrado(timbrado);
                                    } else {
                                        nError.setTimbrado("-");
                                    }
                                    lista.add(nError);
                                    nError.addError(MensajePojo.createInstance().descripcion("El detalle no coincide con ninguna cabecera"));
                                    continue;
                                }
                                ndd.setIdEmpresa(param.getIdEmpresa());
                                ndd.setNroNotaDebito(nroNDebito);
                                ndd.setNroFactura(nroFactura);
                                ndd.setTimbradoFactura(timbradoFactura);
                                if (fechaFactura != null && !fechaFactura.trim().isEmpty()) {
                                    ndd.setFechaFactura(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFactura));
                                }
                                ndd.setCdcFactura(cdcFactura);
                                ProductoParam prod = new ProductoParam();
                                if (codProducto != null && !codProducto.trim().isEmpty()) {
                                    prod.setCodigoInternoEq(codProducto);
                                    prod.setIdEmpresa(param.getIdEmpresa());
                                    prod.setActivo('S');
                                    ProductoPojo producto = facades.getProductoFacade().findFirstPojo(prod);
                                    ndd.setIdProducto(producto.getId());
                                    if (producto == null) {
                                        ndd.addError(MensajePojo.createInstance().descripcion("Producto no encontrado, verificar código interno"));
                                        continue;
                                    } else {
                                        ndd.setIdProducto(producto.getId());
                                    }
                                } else {
                                    ndd.addError(MensajePojo.createInstance().descripcion("Se debe indicar el código interno del producto"));
                                    continue;
                                }
                                ndd.setDescripcion(descripcion);
                                try {
                                    byte[] bytesDecodificados = Base64.getDecoder().decode(datoAdicional);
                                    String decoded = new String(bytesDecodificados);
                                    ndd.setDatoAdicional(decoded);
                                } catch (Exception e) {
                                    ndd.setDatoAdicional(datoAdicional);
                                }
                                if (precioUnitarioConIva != null && cantidad != null && descuentoUnitario != null) {
                                    if (!precioUnitarioConIva.trim().isEmpty() && !cantidad.trim().isEmpty() && !descuentoUnitario.trim().isEmpty()) {
                                        try {
                                            ndd.setPrecioUnitarioConIva(new BigDecimal(precioUnitarioConIva.trim().replace(" ", "")));
                                            ndd.setCantidad(new BigDecimal(cantidad.trim().replace(" ", "")));
                                            ndd.setMontoDescuentoParticular(new BigDecimal(descuentoUnitario.trim().replace(" ", "")));
                                        } catch (NumberFormatException e) {
                                            String precioU = precioUnitarioConIva.replace("\"", "").trim().replace(" ", "");
                                            String cantidad1 = cantidad.replace("\"", "").trim().replace(" ", "");
                                            String descuento1 = descuentoUnitario.replace("\"", "").trim().replace(" ", "");

                                            DecimalFormat decimalFormat = new DecimalFormat();
                                            decimalFormat.setParseBigDecimal(true);
                                            Number parsedPrecio = decimalFormat.parse(precioU);
                                            BigDecimal bdPrecio = (BigDecimal) parsedPrecio;

                                            Number parsedCantidad = decimalFormat.parse(cantidad1);
                                            BigDecimal bdCantidad = (BigDecimal) parsedCantidad;

                                            Number parsedDescuento = decimalFormat.parse(descuento1);
                                            BigDecimal bdDescuento = (BigDecimal) parsedDescuento;

                                            ndd.setPrecioUnitarioConIva(bdPrecio);
                                            ndd.setCantidad(bdCantidad);
                                            ndd.setMontoDescuentoParticular(bdDescuento);
                                        }
                                        ndd.setPorcentajeIva(Integer.parseInt(porcentajeImpuesto));
                                        if (ndd.getMontoDescuentoParticular() != null && (((ndd.getMontoDescuentoParticular().compareTo(BigDecimal.ZERO) == 1) || (ndd.getMontoDescuentoParticular().compareTo(BigDecimal.ZERO) == 0)) && (ndd.getMontoDescuentoParticular().compareTo(ndd.getPrecioUnitarioConIva()) == -1))) {
                                            BigDecimal aux = ndd.getMontoDescuentoParticular().multiply(new BigDecimal("100"));
                                            BigDecimal descuentoPorc = aux.divide(ndd.getPrecioUnitarioConIva(), 5, RoundingMode.HALF_UP);
                                            ndd.setDescuentoParticularUnitario(descuentoPorc);
                                        } else {
                                            ndd.setDescuentoParticularUnitario(BigDecimal.ZERO);
                                            ndd.setMontoDescuentoParticular(BigDecimal.ZERO);
                                        }
                                        BigDecimal precioUnitarioConIVA = ndd.getPrecioUnitarioConIva().subtract(ndd.getMontoDescuentoParticular());
                                        if (ndd.getPorcentajeIva() != null) {
                                            if (ndd.getPorcentajeIva() == 5) {
                                                BigDecimal iva = new BigDecimal("1.05");
                                                BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(iva, 5, RoundingMode.HALF_UP);
                                                ndd.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
                                                ndd.setMontoIva(precioUnitarioConIVA.subtract(ndd.getPrecioUnitarioSinIva()));
                                            } else if (ndd.getPorcentajeIva() == 10) {
                                                BigDecimal iva = new BigDecimal("1.1");
                                                BigDecimal precioUnitarioSinIVA = precioUnitarioConIVA.divide(iva, 5, RoundingMode.HALF_UP);
                                                ndd.setPrecioUnitarioSinIva(precioUnitarioSinIVA);
                                                ndd.setMontoIva(precioUnitarioConIVA.subtract(ndd.getPrecioUnitarioSinIva()));
                                            } else if (ndd.getPorcentajeIva() == 0) {
                                                ndd.setPrecioUnitarioSinIva(BigDecimal.ZERO);
                                                ndd.setMontoIva(BigDecimal.ZERO);
                                            }
                                        }
                                        if (porcentajeGrav != null) {
                                            ndd.setPorcentajeGravada(Integer.parseInt(porcentajeGrav));
                                        } else {
                                            ndd.setPorcentajeGravada(100);
                                        }
                                        BigDecimal _montoTotal = ndd.getPrecioUnitarioConIva().subtract(ndd.getMontoDescuentoParticular()).multiply(ndd.getCantidad());
                                        BigDecimal _montoImponible = _montoTotal.multiply(new BigDecimal(ndd.getPorcentajeGravada())).multiply(new BigDecimal(100))
                                                .divide(new BigDecimal(10000).add(new BigDecimal(ndd.getPorcentajeIva()).multiply(new BigDecimal(ndd.getPorcentajeGravada()))), 8, RoundingMode.HALF_UP);
                                        BigDecimal _montoIva = _montoImponible.multiply(new BigDecimal(ndd.getPorcentajeIva())).divide(new BigDecimal(100), 8, RoundingMode.HALF_UP);
                                        ndd.setMontoIva(_montoIva);
                                        ndd.setMontoImponible(_montoImponible);
                                        ndd.setMontoTotal(_montoTotal);

                                    }
                                }
                                break;
                            default:
                                if (!lista.contains(nInicial)) {
                                    lista.add(nInicial);
                                }
                                nInicial.addError(MensajePojo.createInstance().descripcion("Tipo de línea únicamente puede ser 1 o 2"));
                                break;

                        }
                    }
                }
            }
            boolean continuar = false;
            for (NotaDebitoParam nota : lista) {
                    for (NotaDebitoDetalleParam detalle : nota.getNotaDebitoDetalles()) {
                        if (detalle.getFacturaDigital()!='N') {
                            if (detalle.getCantidad() != null && detalle.getPrecioUnitarioConIva() != null) {
                                continuar = true;
                                FacturaParam f = new FacturaParam();
                                TalonarioParam param2 = new TalonarioParam();
                                if (detalle.getNroFactura() != null && !detalle.getNroFactura().trim().isEmpty()) {
                                    f.setNroFactura(detalle.getNroFactura());
                                    param2.setNroSucursal(f.getNroFactura().substring(0, 3));
                                    param2.setNroPuntoVenta(f.getNroFactura().substring(4, 7));
                                }
                                if (detalle.getFacturaDigital() != null) {
                                    f.setDigital(detalle.getFacturaDigital());
                                    param2.setDigital(f.getDigital());
                                }
                                if (detalle.getTimbradoFactura() != null && !detalle.getTimbradoFactura().trim().isEmpty()) {
                                    f.setTimbrado(detalle.getTimbradoFactura());
                                    param2.setTimbrado(f.getTimbrado());
                                }
                                param2.setIdEmpresa(nota.getIdEmpresa());
                                param2.setActivo('S');
                                param2.setIdTipoDocumento(1);
                                TalonarioPojo talonariof = facades.getTalonarioFacade().findFirstPojo(param2);
                                if (talonariof == null) {
                                    detalle.addError(MensajePojo.createInstance().descripcion("Nro de timbrado, nro de sucursal y nro de punto de venta inexistentes de la factura"));
                                    continuar = false;
                                } else {
                                    f.setIdTalonario(talonariof.getId());
                                    f.setIdEmpresa(nota.getIdEmpresa());
                                    FacturaPojo fact = facades.getFacturaFacade().findFirstPojo(f);
                                    if (fact != null) {
                                        detalle.setIdFactura(fact.getId());
                                    } else {
                                        detalle.addError(MensajePojo.createInstance().descripcion("La factura no existe"));
                                        continuar = false;
                                    }
                                }
                            } else {
                                continuar = false;
                            }
                        } else {
                            continuar = true;
                        }
                }
                if (continuar == true) {
                    try {
                        calcularDetalle(nota, nota.getNotaDebitoDetalles());
                    }catch (Exception ex)  {
                        nota.addError(MensajePojo.createInstance().descripcion("Error al calcular los montos, verificar la cantidad y precio unitario de la nota de débito"));
                        continue;
                    }
                }
            }

            return lista;
        } catch (Exception ex) {
            return lista;
        }

    }

    private static void calcularDetalle(NotaDebitoParam nc, List<NotaDebitoDetalleParam> listaDetalle) {
        BigDecimal totalDetalle = new BigDecimal("0");
        int i = 0;

        for (NotaDebitoDetalleParam info : listaDetalle) {
            i++;

            info.setNroLinea(i);
            info.setMontoIva(info.getMontoIva());
            info.setMontoImponible(info.getMontoImponible());
            info.setMontoTotal(info.getMontoTotal());
            info.setPrecioUnitarioSinIva(info.getPrecioUnitarioSinIva());

            totalDetalle = totalDetalle.add(info.getMontoTotal());
        }
        nc.setMontoTotalNotaDebito(totalDetalle);
        nc.setMontoTotalGuaranies(totalDetalle);
        nc.setNotaDebitoDetalles(listaDetalle);
        calcularTotalesGenerales(nc, nc.getNotaDebitoDetalles());

    }

    private static void calcularTotalesGenerales(NotaDebitoParam nd, List<NotaDebitoDetalleParam> listaDetalle) {
        /**
         * Acumulador para Monto Imponible 5
         */
        BigDecimal montoImponible5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 5
         */
        BigDecimal montoIva5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 5
         */
        BigDecimal montoTotal5Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Imponible 10
         */
        BigDecimal montoImponible10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto IVA 10
         */
        BigDecimal montoIva10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Total 10
         */
        BigDecimal montoTotal10Acumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoExcentoAcumulador = new BigDecimal("0");
        /**
         * Acumulador para Monto Excento 0%
         */
        BigDecimal montoImponibleTotalAcumulador = new BigDecimal("0");
        /**
         * Acumulador para el total del IVA
         */
        BigDecimal montoIvaTotalAcumulador = new BigDecimal("0");

        for (int i = 0; i < listaDetalle.size(); i++) {
            if (listaDetalle.get(i).getPorcentajeIva() == 0) {
                montoExcentoAcumulador = montoExcentoAcumulador.add(listaDetalle.get(i).getMontoImponible());
            } else if (listaDetalle.get(i).getPorcentajeIva() == 5) {
                montoImponible5Acumulador = montoImponible5Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva5Acumulador = montoIva5Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal5Acumulador = montoTotal5Acumulador.add(listaDetalle.get(i).getMontoTotal());
            } else {
                montoImponible10Acumulador = montoImponible10Acumulador.add(listaDetalle.get(i).getMontoImponible());
                montoIva10Acumulador = montoIva10Acumulador.add(listaDetalle.get(i).getMontoIva());
                montoTotal10Acumulador = montoTotal10Acumulador.add(listaDetalle.get(i).getMontoTotal());
            }
            montoImponibleTotalAcumulador = montoImponibleTotalAcumulador.add(listaDetalle.get(i).getMontoImponible());
            montoIvaTotalAcumulador = montoIvaTotalAcumulador.add(listaDetalle.get(i).getMontoIva());
        }

        nd.setMontoIva5(montoIva5Acumulador);
        nd.setMontoImponible5(montoImponible5Acumulador);
        nd.setMontoTotal5(montoTotal5Acumulador);
        nd.setMontoIva10(montoIva10Acumulador);
        nd.setMontoImponible10(montoImponible10Acumulador);
        nd.setMontoTotal10(montoTotal10Acumulador);
        nd.setMontoTotalExento(montoExcentoAcumulador);
        nd.setMontoImponibleTotal(montoImponibleTotalAcumulador);
        nd.setMontoIvaTotal(montoIvaTotalAcumulador);
        nd.setMontoTotalDescuentoGlobal(new BigDecimal("0"));
    }
}
