/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.set.filters;

import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * 
 * @author Jonathan
 */
public class NotaCreditoProcesamientoParam extends CommonParam {

    public NotaCreditoProcesamientoParam() {
    }

    public NotaCreditoProcesamientoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de nota de crédito
     */
    @QueryParam("idNotaCredito")
    private Integer idNotaCredito;
    
    /**
     * Identificador de procesamiento
     */
    @QueryParam("idProcesamiento")
    private Integer idProcesamiento;
    
    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;
    
    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Procesamiento
     */
    private ProcesamientoParam procesamiento;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito"));
        }
        
        if(!isNull(procesamiento)) {
            procesamiento.setIdEmpresa(idEmpresa);
            procesamiento.isValidToCreate();
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de procesamiento"));
        }
        
        if(isNull(idNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(procesamiento)) {
            list.addAll(procesamiento.getErrores());
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdNotaCredito(Integer idNotaCredito) {
        this.idNotaCredito = idNotaCredito;
    }

    public Integer getIdNotaCredito() {
        return idNotaCredito;
    }

    public Integer getIdProcesamiento() {
        return idProcesamiento;
    }

    public void setIdProcesamiento(Integer idProcesamiento) {
        this.idProcesamiento = idProcesamiento;
    }

    public ProcesamientoParam getProcesamiento() {
        return procesamiento;
    }

    public void setProcesamiento(ProcesamientoParam procesamiento) {
        this.procesamiento = procesamiento;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }
}
