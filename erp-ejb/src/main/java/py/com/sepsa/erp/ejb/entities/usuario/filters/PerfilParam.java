/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.filters;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javax.ws.rs.QueryParam;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import py.com.sepsa.erp.ejb.hibernate.types.JsonType;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de tipo etiqueta
 * @author Jonathan
 */
public class PerfilParam extends CommonParam {

    public PerfilParam() {
    }

    public PerfilParam(String bruto) {
        super(bruto);
    }
    
    @QueryParam("permisos")
    private String permisos;

    public String getPermisos() {
        return permisos;
    }

    public void setPermisos(String permisos) {
        this.permisos = permisos;
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de perfil"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(permisos)){
            addError(MensajePojo.createInstance()
                .descripcion("Se deben indicar los permisos"));
        }
        
        return !tieneErrores();
    }
}
