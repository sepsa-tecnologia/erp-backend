/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

/**
 * POJO para la entidad config aprobacion
 * @author Jonathan
 */
public class ConfigAprobacionPojo {

    public ConfigAprobacionPojo(Integer id, Integer idTipoEtiqueta,
            String tipoEtiqueta, String descripcion) {
        this.id = id;
        this.idTipoEtiqueta = idTipoEtiqueta;
        this.tipoEtiqueta = tipoEtiqueta;
        this.descripcion = descripcion;
    }
    
    /**
     * Identificador 
     */
    private Integer id;
    
    /**
     * Identificador de tipo de etiqueta
     */
    private Integer idTipoEtiqueta;
    
    /**
     * Tipo de etiqueta
     */
    private String tipoEtiqueta;
    
    /**
     * Descripcion
     */
    private String descripcion;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdTipoEtiqueta() {
        return idTipoEtiqueta;
    }

    public void setIdTipoEtiqueta(Integer idTipoEtiqueta) {
        this.idTipoEtiqueta = idTipoEtiqueta;
    }

    public String getTipoEtiqueta() {
        return tipoEtiqueta;
    }

    public void setTipoEtiqueta(String tipoEtiqueta) {
        this.tipoEtiqueta = tipoEtiqueta;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
