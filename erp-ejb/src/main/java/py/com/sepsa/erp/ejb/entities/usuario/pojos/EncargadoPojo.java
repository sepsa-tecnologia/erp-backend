/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.usuario.pojos;

import java.io.Serializable;

/**
 * POJO para la entidad de Encargado
 * @author Jonathan
 */
public class EncargadoPojo implements Serializable {
    
    /**
     * Identificador de encargado
     */
    private Integer id;
    
    /**
     * Razón social
     */
    private String razonSocial;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
}
