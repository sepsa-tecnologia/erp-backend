/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.math.BigInteger;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class LocalPojo {

    public LocalPojo() {
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de persona
     */
    private Integer idPersona;
    
    /**
     * Identificador de dirección
     */
    private Integer idDireccion;
    
    /**
     * GLN
     */
    private BigInteger gln;
    
    /**
     * Identificador de externo
     */
    private String idExterno;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Local externo
     */
    private Character localExterno;
    
    /**
     * Descripción
     */
    private String descripcion;
    
    /**
     * Observación
     */
    private String observacion;
    
    /**
     * Identificador de telefono
     */
    private Integer idTelefono;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public BigInteger getGln() {
        return gln;
    }

    public void setGln(BigInteger gln) {
        this.gln = gln;
    }

    public String getIdExterno() {
        return idExterno;
    }

    public void setIdExterno(String idExterno) {
        this.idExterno = idExterno;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setLocalExterno(Character localExterno) {
        this.localExterno = localExterno;
    }

    public Character getLocalExterno() {
        return localExterno;
    }

    public Integer getIdTelefono() {
        return idTelefono;
    }

    public void setIdTelefono(Integer idTelefono) {
        this.idTelefono = idTelefono;
    }
    
}
