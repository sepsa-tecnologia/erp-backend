/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.OperacionInventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface OperacionInventarioDetalleFacade extends Facade<OperacionInventarioDetalle, OperacionInventarioDetalleParam, UserInfoImpl> {

    public Boolean validToCreate(OperacionInventarioDetalleParam param, boolean validarOperacionInventario);

    public OperacionInventarioDetalle create(UserInfoImpl userInfo, Integer idOperacionInventario, String codigoTipoOperacion, Integer idProducto, Integer cantidad, Date fechaVencimiento, Integer idDepositoOrigen, Integer idEstadoInventarioOrigen, String codigoEstadoInventarioOrigen, Integer idDepositoDestino, Integer idEstadoInventarioDestino, String codigoEstadoInventarioDestino);
    
    public List<OperacionInventarioDetalle> findByIdOperacionInventario(Integer idOperacionInventario);

    public List<OperacionInventarioDetallePojo> findByIdOperacionInventarioPojo(Integer idOperacionInventario);

}
