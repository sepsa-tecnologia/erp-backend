/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.List;
import java.util.Map;
import javax.ejb.Local;
import javax.ejb.Stateless;
import py.com.sepsa.erp.ejb.entities.comercial.ConfigBonificacion;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ConfigBonificacionParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "ConfigBonificacionFacade", mappedName = "ConfigBonificacionFacade")
@Local(ConfigBonificacionFacade.class)
public class ConfigBonificacionFacadeImpl extends FacadeImpl<ConfigBonificacion, ConfigBonificacionParam> implements ConfigBonificacionFacade {

    public ConfigBonificacionFacadeImpl() {
        super(ConfigBonificacion.class);
    }

    @Override
    public Map<String, String> getMapConditions(ConfigBonificacion item) {
        Map<String, String> map = super.getMapConditions(item); //To change body of generated methods, choose Tools | Templates.
        
        map.put("id", item.getId() + "");
        map.put("id_producto_bonificado", item.getIdProductoBonificado() + "");
        map.put("id_producto_bonificador", item.getIdProductoBonificador() + "");
        map.put("id_servicio_bonificado", item.getIdServicioBonificado() + "");
        map.put("id_servicio_bonificador", item.getIdServicioBonificador() + "");
        map.put("valor", item.getValor() + "");
        map.put("porcentual", item.getPorcentual() + "");
        map.put("activo", item.getActivo() + "");
        
        return map;
    }
    
    @Override
    public Map<String, String> getMapPk(ConfigBonificacion item) {
        Map<String, String> pk = super.getMapPk(item); //To change body of generated methods, choose Tools | Templates.
        
        pk.put("id", item.getId() + "");
        
        return pk;
    }
    
    @Override
    public List<ConfigBonificacion> find(ConfigBonificacionParam param) {
        
        String where = "true";
        
        if(param.getIdContratoBonificador() != null || param.getIdContratoBonificado() != null) {
            where = String.format("%s and cobr.id_cliente = cobo.id_cliente\n"
                    + "and (cb.id_servicio_bonificador is null or csbr.id_servicio = cb.id_servicio_bonificador)\n"
                    + "and (cb.id_servicio_bonificado is null or csbo.id_servicio = cb.id_servicio_bonificado)", where);
        }
        
        if(param.getId() != null) {
            where = String.format("%s and cb.id = %d", where, param.getId());
        }
        
        if(param.getIdContratoBonificador() != null) {
            where = String.format("%s and cobr.id = %d", where, param.getIdContratoBonificador());
        }
        
        if(param.getIdProductoBonificador() != null) {
            where = String.format("%s and cb.id_producto_bonificador = %d", where, param.getIdProductoBonificador());
        }
        
        if(param.getIdServicioBonificador() != null) {
            where = String.format("%s and cb.id_servicio_bonificador = %d", where, param.getIdServicioBonificador());
        }
        
        if(param.getIdContratoBonificado() != null) {
            where = String.format("%s and cobo.id = %d", where, param.getIdContratoBonificado());
        }
        
        if(param.getIdProductoBonificado() != null) {
            where = String.format("%s and cb.id_producto_bonificado = %d", where, param.getIdProductoBonificado());
        }
        
        if(param.getIdServicioBonificado() != null) {
            where = String.format("%s and cb.id_servicio_bonificado = %d", where, param.getIdServicioBonificado());
        }
        
        if(param.getPorcentual() != null) {
            where = String.format("%s and cb.porcentual = '%s'", where, param.getPorcentual());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and cb.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select distinct cb.*"
                + " from comercial.config_bonificacion cb"
                + " left join comercial.producto pbr on pbr.id = cb.id_producto_bonificador"
                + " left join comercial.producto pbo on pbo.id = cb.id_producto_bonificado"
                + " left join comercial.servicio sbr on sbr.id = cb.id_servicio_bonificador"
                + " left join comercial.servicio sbo on sbo.id = cb.id_Servicio_bonificado"
                + " left join comercial.contrato cobr on cobr.estado = 'A'"
                + " and cobr.id_producto = cb.id_producto_bonificador"
                + " left join comercial.contrato_servicio csbr on csbr.estado = 'A'"
                + " and csbr.id_contrato = cobr.id"
                + " and csbr.id_servicio = cb.id_servicio_bonificador"
                + " left join comercial.contrato cobo on cobo.estado = 'A'"
                + " and cobo.id_producto = cb.id_producto_bonificado"
                + " left join comercial.contrato_servicio csbo on csbo.estado = 'A'"
                + " and csbo.id_contrato = cobo.id"
                + " and csbo.id_servicio = cb.id_servicio_bonificado"
                + " where %s order by cb.estado offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql, ConfigBonificacion.class);
        
        List<ConfigBonificacion> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ConfigBonificacionParam param) {
        
        String where = "true";
        
        if(param.getIdContratoBonificador() != null || param.getIdContratoBonificado() != null) {
            where = String.format("%s and cobr.id_cliente = cobo.id_cliente\n"
                    + "and (cb.id_servicio_bonificador is null or csbr.id_servicio = cb.id_servicio_bonificador)\n"
                    + "and (cb.id_servicio_bonificado is null or csbo.id_servicio = cb.id_servicio_bonificado)", where);
        }
        
        if(param.getId() != null) {
            where = String.format("%s and cb.id = %d", where, param.getId());
        }
        
        if(param.getIdContratoBonificador() != null) {
            where = String.format("%s and cobr.id = %d", where, param.getIdContratoBonificador());
        }
        
        if(param.getIdProductoBonificador() != null) {
            where = String.format("%s and cb.id_producto_bonificador = %d", where, param.getIdProductoBonificador());
        }
        
        if(param.getIdServicioBonificador() != null) {
            where = String.format("%s and cb.id_servicio_bonificador = %d", where, param.getIdServicioBonificador());
        }
        
        if(param.getIdContratoBonificado() != null) {
            where = String.format("%s and cobo.id = %d", where, param.getIdContratoBonificado());
        }
        
        if(param.getIdProductoBonificado() != null) {
            where = String.format("%s and cb.id_producto_bonificado = %d", where, param.getIdProductoBonificado());
        }
        
        if(param.getIdServicioBonificado() != null) {
            where = String.format("%s and cb.id_servicio_bonificado = %d", where, param.getIdServicioBonificado());
        }
        
        if(param.getPorcentual() != null) {
            where = String.format("%s and cb.porcentual = '%s'", where, param.getPorcentual());
        }
        
        if(param.getActivo() != null) {
            where = String.format("%s and cb.activo = '%s'", where, param.getActivo());
        }
        
        String sql = String.format("select count(distinct (cb.*))"
                + " from comercial.config_bonificacion cb"
                + " left join comercial.producto pbr on pbr.id = cb.id_producto_bonificador"
                + " left join comercial.producto pbo on pbo.id = cb.id_producto_bonificado"
                + " left join comercial.servicio sbr on sbr.id = cb.id_servicio_bonificador"
                + " left join comercial.servicio sbo on sbo.id = cb.id_Servicio_bonificado"
                + " left join comercial.contrato cobr on cobr.estado = 'A'"
                + " and cobr.id_producto = cb.id_producto_bonificador"
                + " left join comercial.contrato_servicio csbr on csbr.estado = 'A'"
                + " and csbr.id_contrato = cobr.id"
                + " and csbr.id_servicio = cb.id_servicio_bonificador"
                + " left join comercial.contrato cobo on cobo.estado = 'A'"
                + " and cobo.id_producto = cb.id_producto_bonificado"
                + " left join comercial.contrato_servicio csbo on csbo.estado = 'A'"
                + " and csbo.id_contrato = cobo.id"
                + " and csbo.id_servicio = cb.id_servicio_bonificado"
                + " where %s", where);
        
        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }
}
