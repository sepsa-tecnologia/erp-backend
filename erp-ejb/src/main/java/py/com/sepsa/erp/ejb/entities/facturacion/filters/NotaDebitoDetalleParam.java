/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de nota de débito detalle
 * @author Williams Vera
 */
public class NotaDebitoDetalleParam extends CommonParam {
    
    public NotaDebitoDetalleParam() {
    }

    public NotaDebitoDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de nota de débito
     */
    @QueryParam("idNotaDebito")
    private Integer idNotaDebito;
    
    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;
    
    /**
     * Porcentaje de iva
     */
    @QueryParam("porcentajeIva")
    private Integer porcentajeIva;
    
    /**
     * Porcentaje Gravada
     */
    @QueryParam("porcentajeGravada")
    private Integer porcentajeGravada;
    
    /**
     * Cantidad
     */
    @QueryParam("cantidad")
    private BigDecimal cantidad;

    /**
     * Precio unitario con IVA
     */
    @QueryParam("precioUnitarioConIva")
    private BigDecimal precioUnitarioConIva;

    /**
     * Precio unitario sin IVA
     */
    @QueryParam("precioUnitarioSinIva")
    private BigDecimal precioUnitarioSinIva;
    
    /**
     * Monto iva
     */
    @QueryParam("montoIva")
    private BigDecimal montoIva;
    
    /**
     * Monto imponible
     */
    @QueryParam("montoImponible")
    private BigDecimal montoImponible;
    
    /**
     * Monto total
     */
    @QueryParam("montoTotal")
    private BigDecimal montoTotal;
    
    /**
     * Identificador de liquidación
     */
    @QueryParam("idLiquidacion")
    private Integer idLiquidacion;

    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;

    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Año y mes
     */
    @QueryParam("anhoMes")
    private String anhoMes;
    
    /**
     * Timbrado de factura
     */
    @QueryParam("timbradoFactura")
    private String timbradoFactura;
    
    /**
     * Dato adicional
     */
    @QueryParam("datoAdicional")
    private String datoAdicional;
    
    /**
     * Número de factura
     */
    @QueryParam("nroFactura")
    private String nroFactura;
    
    /**
     * Nro de nota de débito
     */
    @QueryParam("nroNotaDebito")
    private String nroNotaDebito;
    
    /**
     * CDC de factura
     */
    @QueryParam("cdcFactura")
    private String cdcFactura;
    
    /**
     * Fecha de factura
     */
    @QueryParam("fechaFactura")
    private Date fechaFactura;
    
    /**
     * Factura digital
     */
    @QueryParam("facturaDigital")
    private Character facturaDigital;
    
    /**
     * Monto imponible
     */
    @QueryParam("montoDescuentoParticular")
    private BigDecimal montoDescuentoParticular;
    
    /**
     * Monto total
     */
    @QueryParam("descuentoParticularUnitario")
    private BigDecimal descuentoParticularUnitario;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idNotaDebito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de débito"));
        }
        
        if(isNull(nroNotaDebito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de nota de débito"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador nro de linea"));
        }
        
        if(isNull(idFactura)) {
            if(isNull(facturaDigital)) {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar si la factura asociada es digital o no"));
            } else {
                if(facturaDigital.equals('S')) {
                    if(isNullOrEmpty(cdcFactura)) {
                        addError(MensajePojo.createInstance()
                                .descripcion("Se debe indicar el CDC de la factura asociada"));
                    }
                } else if(facturaDigital.equals('N')) {
                    if(isNullOrEmpty(timbradoFactura)) {
                        addError(MensajePojo.createInstance()
                                .descripcion("Se debe indicar el timbrado de la factura asociada"));
                    }
                    if(isNullOrEmpty(nroFactura)) {
                        addError(MensajePojo.createInstance()
                                .descripcion("Se debe indicar el número de la factura asociada"));
                    }
                    if(isNull(fechaFactura)) {
                        addError(MensajePojo.createInstance()
                                .descripcion("Se debe indicar la fecha de la factura asociada"));
                    }
                }
            }
        }
        
        if(isNull(porcentajeIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de IVA"));
        }
        
        if(isNull(porcentajeGravada)) {
            porcentajeGravada=100;
        }
        
        if (isNull(cantidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la cantidad"));
        }

        if (isNull(precioUnitarioConIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario con IVA"));
        }

        if (isNull(precioUnitarioSinIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el precio unitario sin IVA"));
        }
        
        if(isNull(montoIva)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de IVA"));
        }
        
        if(isNull(montoImponible)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible"));
        }
        
        if(isNull(montoDescuentoParticular)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de descuento particular unitario"));
        }
        
        if(isNull(descuentoParticularUnitario)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de descuento particular unitario"));
        }
        
        if(isNull(montoTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(!tieneErrores()) {
            BigDecimal _montoTotal = precioUnitarioConIva
                    .subtract(montoDescuentoParticular)
                    .multiply(cantidad);
            if(_montoTotal.subtract(montoTotal).abs().compareTo(BigDecimal.ONE) >= 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("El cálculo del monto total no corresponde"));
            }
            
            BigDecimal _montoImponible = _montoTotal
                    .multiply(new BigDecimal(porcentajeGravada))
                    .multiply(new BigDecimal(100))
                    .divide(new BigDecimal(10000).add(new BigDecimal(porcentajeIva).multiply(new BigDecimal(porcentajeGravada))), 8, RoundingMode.HALF_UP);
            if(_montoImponible.subtract(montoImponible).abs().compareTo(BigDecimal.ONE) >= 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("El cálculo del monto imponible no corresponde"));
            }
            
            BigDecimal _montoIva = _montoImponible
                    .multiply(new BigDecimal(porcentajeIva))
                    .divide(new BigDecimal(100), 8, RoundingMode.HALF_UP);
            if(_montoIva.subtract(montoIva).abs().compareTo(BigDecimal.ONE) >= 0) {
                addError(MensajePojo.createInstance()
                        .descripcion("El cálculo del monto iva no corresponde"));
            }
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdNotaDebito(Integer idNotaDebito) {
        this.idNotaDebito = idNotaDebito;
    }

    public Integer getIdNotaDebito() {
        return idNotaDebito;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public String getNroNotaDebito() {
        return nroNotaDebito;
    }

    public void setNroNotaDebito(String nroNotaDebito) {
        this.nroNotaDebito = nroNotaDebito;
    }

    public Integer getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Integer porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getPorcentajeGravada() {
        return porcentajeGravada;
    }

    public void setPorcentajeGravada(Integer porcentajeGravada) {
        this.porcentajeGravada = porcentajeGravada;
    }

    public BigDecimal getMontoIva() {
        return montoIva;
    }

    public void setMontoIva(BigDecimal montoIva) {
        this.montoIva = montoIva;
    }

    public BigDecimal getMontoImponible() {
        return montoImponible;
    }

    public void setMontoImponible(BigDecimal montoImponible) {
        this.montoImponible = montoImponible;
    }

    public BigDecimal getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(BigDecimal montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setAnhoMes(String anhoMes) {
        this.anhoMes = anhoMes;
    }

    public String getAnhoMes() {
        return anhoMes;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecioUnitarioSinIva(BigDecimal precioUnitarioSinIva) {
        this.precioUnitarioSinIva = precioUnitarioSinIva;
    }

    public BigDecimal getPrecioUnitarioSinIva() {
        return precioUnitarioSinIva;
    }

    public void setPrecioUnitarioConIva(BigDecimal precioUnitarioConIva) {
        this.precioUnitarioConIva = precioUnitarioConIva;
    }

    public BigDecimal getPrecioUnitarioConIva() {
        return precioUnitarioConIva;
    }

    public Integer getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(Integer idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getTimbradoFactura() {
        return timbradoFactura;
    }

    public void setTimbradoFactura(String timbradoFactura) {
        this.timbradoFactura = timbradoFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public String getCdcFactura() {
        return cdcFactura;
    }

    public void setCdcFactura(String cdcFactura) {
        this.cdcFactura = cdcFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public Character getFacturaDigital() {
        return facturaDigital;
    }

    public void setFacturaDigital(Character facturaDigital) {
        this.facturaDigital = facturaDigital;
    }

    public BigDecimal getMontoDescuentoParticular() {
        return montoDescuentoParticular;
    }

    public void setMontoDescuentoParticular(BigDecimal montoDescuentoParticular) {
        this.montoDescuentoParticular = montoDescuentoParticular;
    }

    public BigDecimal getDescuentoParticularUnitario() {
        return descuentoParticularUnitario;
    }

    public void setDescuentoParticularUnitario(BigDecimal descuentoParticularUnitario) {
        this.descuentoParticularUnitario = descuentoParticularUnitario;
    }    

    public void setDatoAdicional(String datoAdicional) {
        this.datoAdicional = datoAdicional;
    }

    public String getDatoAdicional() {
        return datoAdicional;
    }
    
}
