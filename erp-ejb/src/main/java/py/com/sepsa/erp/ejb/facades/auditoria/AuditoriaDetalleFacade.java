/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.auditoria;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.auditoria.AuditoriaDetalle;
import py.com.sepsa.erp.ejb.entities.auditoria.filters.AuditoriaDetalleParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface AuditoriaDetalleFacade extends Facade<AuditoriaDetalle, AuditoriaDetalleParam, UserInfoImpl> {

    public Boolean validToCreate(AuditoriaDetalleParam param, Boolean validarAuditoria);

    public AuditoriaDetalle nuevaInstancia(String codigo, UserInfoImpl userInfo);

    public AuditoriaDetalle editarInstancia(Integer idAuditoria, String codigo, UserInfoImpl userInfo);
    
}
