/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.facturacion.OrdenCompraDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraDetalleParam;
import py.com.sepsa.erp.ejb.entities.info.Estado;
import py.com.sepsa.erp.ejb.entities.inventario.Inventario;
import py.com.sepsa.erp.ejb.entities.inventario.InventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.OrdenCompraDetalleInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OrdenCompraDetalleInventarioDetalleParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioDetallePojo;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.OrdenCompraDetalleInventarioDetallePojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "OrdenCompraDetalleInventarioDetalleFacade", mappedName = "OrdenCompraDetalleInventarioDetalleFacade")
@javax.ejb.Local(OrdenCompraDetalleInventarioDetalleFacade.class)
public class OrdenCompraDetalleInventarioDetalleFacadeImp extends FacadeImpl<OrdenCompraDetalleInventarioDetalle, OrdenCompraDetalleInventarioDetalleParam> implements OrdenCompraDetalleInventarioDetalleFacade {

    public OrdenCompraDetalleInventarioDetalleFacadeImp() {
        super(OrdenCompraDetalleInventarioDetalle.class);
    }
    
    public Boolean validToCreate(OrdenCompraDetalleInventarioDetalleParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraDetalleInventarioDetalleParam param1 = new OrdenCompraDetalleInventarioDetalleParam();
        param1.setIdOrdenCompraDetalle(param.getIdOrdenCompraDetalle());
        param1.setIdInventarioDetalle(param.getIdInventarioDetalle());
        param1.setIdProducto(param.getIdProducto());
        param1.setIdDepositoLogistico(param.getIdDepositoLogistico());
        param1.setIdEstadoInventario(param.getIdEstadoInventario());
        param1.setCodigoEstadoInventario(param.getCodigoEstadoInventario());
        param1.setFechaVencimiento(param.getFechaVencimiento());
        OrdenCompraDetalleInventarioDetallePojo ocdid = findFirstPojo(param1);
        
        if(ocdid != null) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe un registro"));
        }
        
        OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
        ocdparam.setId(param.getIdOrdenCompraDetalle());
        Long ocdsize = facades.getOrdenCompraDetalleFacade().findSize(ocdparam);
        if(ocdsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la orden de compra detalle"));
        }
        
        InventarioDetalleParam idparam = new InventarioDetalleParam();
        idparam.setId(param.getIdInventarioDetalle());
        idparam.setIdProducto(param.getIdProducto());
        idparam.setIdDepositoLogistico(param.getIdDepositoLogistico());
        idparam.setIdEstado(param.getIdEstadoInventario());
        idparam.setCodigoEstado(param.getCodigoEstadoInventario());
        idparam.setFechaVencimiento(param.getFechaVencimiento());
        InventarioDetallePojo inventarioDetalle = facades.getInventarioDetalleFacade().findFirstPojo(idparam);
        if(inventarioDetalle == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el inventario detalle"));
        } else {
            param.setIdInventarioDetalle(inventarioDetalle.getId());
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(OrdenCompraDetalleInventarioDetalleParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        OrdenCompraDetalleInventarioDetalleParam param1 = new OrdenCompraDetalleInventarioDetalleParam();
        param1.setIdOrdenCompraDetalle(param.getIdOrdenCompraDetalle());
        param1.setIdInventarioDetalle(param.getIdInventarioDetalle());
        param1.setIdProducto(param.getIdProducto());
        param1.setIdDepositoLogistico(param.getIdDepositoLogistico());
        param1.setIdEstado(param.getIdEstadoInventario());
        param1.setCodigoEstado(param.getCodigoEstadoInventario());
        param1.setFechaVencimiento(param.getFechaVencimiento());
        OrdenCompraDetalleInventarioDetallePojo ocdid = findFirstPojo(param1);
        
        if(ocdid == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe un registro"));
        } else {
            param.setId(ocdid.getId());
        }
        
        OrdenCompraDetalleParam ocdparam = new OrdenCompraDetalleParam();
        ocdparam.setId(param.getIdOrdenCompraDetalle());
        Long ocdsize = facades.getOrdenCompraDetalleFacade().findSize(ocdparam);
        if(ocdsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe la orden de compra detalle"));
        }
        
        InventarioDetalleParam idparam = new InventarioDetalleParam();
        idparam.setId(param.getIdInventarioDetalle());
        idparam.setIdProducto(param.getIdProducto());
        idparam.setIdDepositoLogistico(param.getIdDepositoLogistico());
        idparam.setIdEstado(param.getIdEstadoInventario());
        idparam.setCodigoEstado(param.getCodigoEstadoInventario());
        idparam.setFechaVencimiento(param.getFechaVencimiento());
        InventarioDetallePojo inventarioDetalle = facades.getInventarioDetalleFacade().findFirstPojo(idparam);
        if(inventarioDetalle == null) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el inventario detalle"));
        } else {
            param.setIdInventarioDetalle(inventarioDetalle.getId());
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public OrdenCompraDetalleInventarioDetalle create(OrdenCompraDetalleInventarioDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        OrdenCompraDetalleInventarioDetalle result = new OrdenCompraDetalleInventarioDetalle();
        result.setIdInventarioDetalle(param.getIdInventarioDetalle()); 
        result.setIdOrdenCompraDetalle(param.getIdOrdenCompraDetalle());
        result.setCantidad(param.getCantidad());
        
        create(result);
        
        return result;
    }
    
    @Override
    public OrdenCompraDetalleInventarioDetalle edit(OrdenCompraDetalleInventarioDetalleParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        OrdenCompraDetalleInventarioDetalle result = find(param.getId());
        result.setIdInventarioDetalle(param.getIdInventarioDetalle()); 
        result.setIdOrdenCompraDetalle(param.getIdOrdenCompraDetalle());
        result.setCantidad(param.getCantidad());
        
        edit(result);
        
        return result;
    }
    
    @Override
    public List<OrdenCompraDetalleInventarioDetallePojo> findPojo(OrdenCompraDetalleInventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(OrdenCompraDetalleInventarioDetallePojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idOrdenCompraDetalle"),
                        getPath("root").get("idInventarioDetalle"),
                        getPath("root").get("cantidad"),
                        getPath("inventario").get("idEstado"),
                        getPath("estadoInventario").get("codigo"),
                        getPath("estadoInventario").get("descripcion"),
                        getPath("inventario").get("idProducto"),
                        getPath("inventario").get("idDepositoLogistico"),
                        getPath("inventarioDetalle").get("fechaVencimiento"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<OrdenCompraDetalleInventarioDetalle> find(OrdenCompraDetalleInventarioDetalleParam param) {

        AbstractFind find = new AbstractFind(OrdenCompraDetalleInventarioDetalle.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, OrdenCompraDetalleInventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<OrdenCompraDetalleInventarioDetalle> root = cq.from(OrdenCompraDetalleInventarioDetalle.class);
        Join<OrdenCompraDetalleInventarioDetalle, OrdenCompraDetalle> ordenCompraDetalle = root.join("ordenCompraDetalle");
        Join<OrdenCompraDetalleInventarioDetalle, InventarioDetalle> inventarioDetalle = root.join("inventarioDetalle");
        Join<InventarioDetalle, Inventario> inventario = inventarioDetalle.join("inventario");
        Join<Inventario, Estado> estadoInventario = inventario.join("estado");
        
        find.addPath("root", root);
        find.addPath("inventarioDetalle", inventarioDetalle);
        find.addPath("inventario", inventario);
        find.addPath("estadoInventario", estadoInventario);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(ordenCompraDetalle.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }
        
        if (param.getIdOrdenCompraDetalle() != null) {
            predList.add(qb.equal(root.get("idOrdenCompraDetalle"), param.getIdOrdenCompraDetalle()));
        }
        
        if (param.getIdInventarioDetalle() != null) {
            predList.add(qb.equal(root.get("idInventarioDetalle"), param.getIdInventarioDetalle()));
        }
        
        if (param.getIdEstadoInventario() != null) {
            predList.add(qb.equal(inventario.get("idEstado"), param.getIdEstadoInventario()));
        }
        
        if (param.getCodigoEstadoInventario() != null && !param.getCodigoEstadoInventario().trim().isEmpty()) {
            predList.add(qb.equal(estadoInventario.get("codigo"), param.getCodigoEstadoInventario().trim()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(inventario.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(inventario.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }
        
        if (param.getFechaVencimiento() != null) {
            predList.add(qb.equal(inventarioDetalle.get("fechaVencimiento"), param.getFechaVencimiento()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        cq.orderBy(qb.asc(inventarioDetalle.get("fechaVencimiento")),
                qb.asc(root.get("id")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(OrdenCompraDetalleInventarioDetalleParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<OrdenCompraDetalleInventarioDetalle> root = cq.from(OrdenCompraDetalleInventarioDetalle.class);
        Join<OrdenCompraDetalleInventarioDetalle, OrdenCompraDetalle> ordenCompraDetalle = root.join("ordenCompraDetalle");
        Join<OrdenCompraDetalleInventarioDetalle, InventarioDetalle> inventarioDetalle = root.join("inventarioDetalle");
        Join<InventarioDetalle, Inventario> inventario = inventarioDetalle.join("inventario");
        Join<Inventario, Estado> estadoInventario = inventario.join("estado");
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getIdOrdenCompra() != null) {
            predList.add(qb.equal(ordenCompraDetalle.get("idOrdenCompra"), param.getIdOrdenCompra()));
        }
        
        if (param.getIdOrdenCompraDetalle() != null) {
            predList.add(qb.equal(root.get("idOrdenCompraDetalle"), param.getIdOrdenCompraDetalle()));
        }
        
        if (param.getIdInventarioDetalle() != null) {
            predList.add(qb.equal(root.get("idInventarioDetalle"), param.getIdInventarioDetalle()));
        }
        
        if (param.getIdEstadoInventario() != null) {
            predList.add(qb.equal(inventario.get("idEstado"), param.getIdEstadoInventario()));
        }
        
        if (param.getCodigoEstadoInventario() != null && !param.getCodigoEstadoInventario().trim().isEmpty()) {
            predList.add(qb.equal(estadoInventario.get("codigo"), param.getCodigoEstadoInventario().trim()));
        }
        
        if (param.getIdProducto() != null) {
            predList.add(qb.equal(inventario.get("idProducto"), param.getIdProducto()));
        }
        
        if (param.getIdDepositoLogistico() != null) {
            predList.add(qb.equal(inventario.get("idDepositoLogistico"), param.getIdDepositoLogistico()));
        }
        
        if (param.getFechaVencimiento() != null) {
            predList.add(qb.equal(inventarioDetalle.get("fechaVencimiento"), param.getFechaVencimiento()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
