/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de nota crédito compra
 * @author Jonathan D. Bernal Fernández
 */
public class NotaCreditoCompraParam extends CommonParam {
    
    public NotaCreditoCompraParam() {
    }

    public NotaCreditoCompraParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Nro de timbrao
     */
    @QueryParam("nroTimbrado")
    private String nroTimbrado;
    
    /**
     * Fecha de vencimiento de timbrao
     */
    @QueryParam("fechaVencimientoTimbrado")
    private Date fechaVencimientoTimbrado;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Código de moneda
     */
    @QueryParam("codigoMoneda")
    private String codigoMoneda;
    
    /**
     * Identificador de motivo de emisión
     */
    @QueryParam("idMotivoEmision")
    private Integer idMotivoEmision;
    
    /**
     * Identificador de motivo de emisión interno
     */
    @QueryParam("idMotivoEmisionInterno")
    private Integer idMotivoEmisionInterno;
    
    /**
     * Nro de nota de crédito
     */
    @QueryParam("nroNotaCredito")
    private String nroNotaCredito;
    
    /**
     * Fecha factura
     */
    @QueryParam("fecha")
    private Date fecha;
    
    /**
     * Fecha desde
     */
    @QueryParam("fechaDesde")
    private Date fechaDesde;
    
    /**
     * Fecha hasta
     */
    @QueryParam("fechaHasta")
    private Date fechaHasta;
    
    /**
     * Razon social
     */
    @QueryParam("razonSocial")
    private String razonSocial;
    
    /**
     * RUC
     */
    @QueryParam("ruc")
    private String ruc;
    
    /**
     * Anulado
     */
    @QueryParam("anulado")
    private Character anulado;
    
    /**
     * Digital
     */
    @QueryParam("digital")
    private Character digital;
    
    /**
     * CDC
     */
    @QueryParam("cdc")
    private String cdc;
    
    /**
     * Monto iva 5
     */
    @QueryParam("montoIva5")
    private BigDecimal montoIva5;
    
    /**
     * Monto imponible 5
     */
    @QueryParam("montoImponible5")
    private BigDecimal montoImponible5;
    
    /**
     * Monto total 5
     */
    @QueryParam("montoTotal5")
    private BigDecimal montoTotal5;
    
    /**
     * Monto iva 10
     */
    @QueryParam("montoIva10")
    private BigDecimal montoIva10;
    
    /**
     * Monto imponible 10
     */
    @QueryParam("montoImponible10")
    private BigDecimal montoImponible10;
    
    /**
     * Monto total 10
     */
    @QueryParam("montoTotal10")
    private BigDecimal montoTotal10;
    
    /**
     * Monto total exento
     */
    @QueryParam("montoTotalExento")
    private BigDecimal montoTotalExento;
    
    /**
     * Monto iva total
     */
    @QueryParam("montoIvaTotal")
    private BigDecimal montoIvaTotal;
    
    /**
     * Monto imponible total
     */
    @QueryParam("montoImponibleTotal")
    private BigDecimal montoImponibleTotal;
    
    /**
     * Monto total nota de crédito
     */
    @QueryParam("montoTotalNotaCredito")
    private BigDecimal montoTotalNotaCredito;
    
    /**
     * Identificador de motivo anulación
     */
    @QueryParam("idMotivoAnulacion")
    private Integer idMotivoAnulacion;
    
    /**
     * Observación anulación
     */
    @QueryParam("observacionAnulacion")
    private String observacionAnulacion;
    
    /**
     * Identificador de factura de compra
     */
    @QueryParam("idFacturaCompra")
    private Integer idFacturaCompra;
    
    /**
     * Detalles
     */
    private List<NotaCreditoCompraDetalleParam> notaCreditoCompraDetalles;
    
    public boolean datosCrearValido() {
        
        limpiarErrores();
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de crédito es digital o no"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(nroTimbrado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de timbrado"));
        }
        
        if(isNull(idMoneda) && isNullOrEmpty(codigoMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de moneda"));
        }
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha"));
        } else {
            
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            
            if(cal.getTime().compareTo(fecha) < 0) {
                addError(MensajePojo.createInstance()
                    .descripcion("La fecha de la nota de crédito no puede ser posterior a la fecha actual"));
            }
        }
        
        if(isNull(anulado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el estado de anulado"));
        }
        
        if(isNull(digital)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la nota de crédito es digital o no"));
        } else {
            if(digital.equals('S')) {

                if(isNull(idMotivoEmisionInterno)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe de indicar el identificador de motivo de emisión interno"));
                }

                if(isNullOrEmpty(cdc)) {
                    addError(MensajePojo.createInstance()
                                .descripcion("Se debe indicar el cdc"));
                }
            } else {
                if(isNull(fechaVencimientoTimbrado)) {
                    addError(MensajePojo.createInstance()
                            .descripcion("Se debe indicar la fecha de vencimiento del timbrado"));
                }
            }
        }
        
        if(isNull(montoIva5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 5%"));
        }
        
        if(isNull(montoImponible5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 5%"));
        }
        
        if(isNull(montoTotal5)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 5%"));
        }
        
        if(isNull(montoIva10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto IVA 10%"));
        }
        
        if(isNull(montoImponible10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible IVA 10%"));
        }
        
        if(isNull(montoTotal10)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA 10%"));
        }
        
        if(isNull(montoTotalExento)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total excento"));
        }
        
        if(isNull(montoIvaTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total IVA"));
        }
        
        if(isNull(montoImponibleTotal)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto imponible total"));
        }
        
        if(isNull(montoTotalNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto total de la nota de crédito"));
        }
        
        if(isNull(fecha)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de la nota de crédito"));
        }
        
        if(isNullOrEmpty(nroNotaCredito)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el número de la nota de crédito"));
        }
        
        if(isNullOrEmpty(razonSocial)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la razón social del cliente"));
        }
        
        if(isNullOrEmpty(ruc)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el ruc/ci"));
        }
        
        if(isNullOrEmpty(notaCreditoCompraDetalles)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el detalle de la NC"));
        } else {
            for (NotaCreditoCompraDetalleParam detalle : notaCreditoCompraDetalles) {
                detalle.setIdNotaCreditoCompra(0);
                detalle.setIdEmpresa(idEmpresa);
                detalle.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> result = new ArrayList<>();
        
        if(notaCreditoCompraDetalles != null) {
            for (NotaCreditoCompraDetalleParam detalle : notaCreditoCompraDetalles) {
                result.addAll(detalle.getErrores());
            }
        }
        
        return result;
    }
    
    public boolean isValidToCancel() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito de compra"));
        }
        
        if(isNull(idMotivoAnulacion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de motivo anulación"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToDelete() {
        
        limpiarErrores();
        
        if(isNull(id) && isNull(idFacturaCompra)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de nota de crédito de compra o identificador de factura de compra"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setCdc(String cdc) {
        this.cdc = cdc;
    }

    public String getCdc() {
        return cdc;
    }

    public Character getAnulado() {
        return anulado;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Character getDigital() {
        return digital;
    }

    public String getNroNotaCredito() {
        return nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public BigDecimal getMontoIva5() {
        return montoIva5;
    }

    public void setMontoIva5(BigDecimal montoIva5) {
        this.montoIva5 = montoIva5;
    }

    public BigDecimal getMontoImponible5() {
        return montoImponible5;
    }

    public void setMontoImponible5(BigDecimal montoImponible5) {
        this.montoImponible5 = montoImponible5;
    }

    public BigDecimal getMontoTotal5() {
        return montoTotal5;
    }

    public void setMontoTotal5(BigDecimal montoTotal5) {
        this.montoTotal5 = montoTotal5;
    }

    public BigDecimal getMontoIva10() {
        return montoIva10;
    }

    public void setMontoIva10(BigDecimal montoIva10) {
        this.montoIva10 = montoIva10;
    }

    public BigDecimal getMontoImponible10() {
        return montoImponible10;
    }

    public void setMontoImponible10(BigDecimal montoImponible10) {
        this.montoImponible10 = montoImponible10;
    }

    public BigDecimal getMontoTotal10() {
        return montoTotal10;
    }

    public void setMontoTotal10(BigDecimal montoTotal10) {
        this.montoTotal10 = montoTotal10;
    }

    public BigDecimal getMontoTotalExento() {
        return montoTotalExento;
    }

    public void setMontoTotalExento(BigDecimal montoTotalExento) {
        this.montoTotalExento = montoTotalExento;
    }

    public BigDecimal getMontoIvaTotal() {
        return montoIvaTotal;
    }

    public void setMontoIvaTotal(BigDecimal montoIvaTotal) {
        this.montoIvaTotal = montoIvaTotal;
    }

    public BigDecimal getMontoImponibleTotal() {
        return montoImponibleTotal;
    }

    public void setMontoImponibleTotal(BigDecimal montoImponibleTotal) {
        this.montoImponibleTotal = montoImponibleTotal;
    }

    public BigDecimal getMontoTotalNotaCredito() {
        return montoTotalNotaCredito;
    }

    public void setMontoTotalNotaCredito(BigDecimal montoTotalNotaCredito) {
        this.montoTotalNotaCredito = montoTotalNotaCredito;
    }

    public void setNotaCreditoCompraDetalles(List<NotaCreditoCompraDetalleParam> notaCreditoCompraDetalles) {
        this.notaCreditoCompraDetalles = notaCreditoCompraDetalles;
    }

    public List<NotaCreditoCompraDetalleParam> getNotaCreditoCompraDetalles() {
        return notaCreditoCompraDetalles;
    }

    public void setCodigoMoneda(String codigoMoneda) {
        this.codigoMoneda = codigoMoneda;
    }

    public String getCodigoMoneda() {
        return codigoMoneda;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdMotivoAnulacion() {
        return idMotivoAnulacion;
    }

    public void setIdMotivoAnulacion(Integer idMotivoAnulacion) {
        this.idMotivoAnulacion = idMotivoAnulacion;
    }

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getFechaVencimientoTimbrado() {
        return fechaVencimientoTimbrado;
    }

    public void setFechaVencimientoTimbrado(Date fechaVencimientoTimbrado) {
        this.fechaVencimientoTimbrado = fechaVencimientoTimbrado;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public Integer getIdMotivoEmision() {
        return idMotivoEmision;
    }

    public void setIdMotivoEmision(Integer idMotivoEmision) {
        this.idMotivoEmision = idMotivoEmision;
    }

    public Integer getIdMotivoEmisionInterno() {
        return idMotivoEmisionInterno;
    }

    public void setIdMotivoEmisionInterno(Integer idMotivoEmisionInterno) {
        this.idMotivoEmisionInterno = idMotivoEmisionInterno;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }
}
