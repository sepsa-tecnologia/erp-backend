/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de periodo liquidacion
 * @author Jonathan D. Bernal Fernández
 */
public class PeriodoLiquidacionParam extends CommonParam {
    
    public PeriodoLiquidacionParam() {
    }

    public PeriodoLiquidacionParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Fecha inicio
     */
    @QueryParam("fechaInicio")
    private Date fechaInicio;
    
    /**
     * Fecha inicio desde
     */
    @QueryParam("fechaInicioDesde")
    private Date fechaInicioDesde;
    
    /**
     * Fecha inicio hasta
     */
    @QueryParam("fechaInicioHasta")
    private Date fechaInicioHasta;
    
    /**
     * Fecha fin
     */
    @QueryParam("fechaFin")
    private Date fechaFin;
    
    /**
     * Fecha fin desde
     */
    @QueryParam("fechaFinDesde")
    private Date fechaFinDesde;
    
    /**
     * Fecha fin hasta
     */
    @QueryParam("fechaFinHasta")
    private Date fechaFinHasta;
    
    /**
     * Año
     */
    @QueryParam("ano")
    private String ano;
    
    /**
     * Mes
     */
    @QueryParam("mes")
    private String mes;

    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Date getFechaInicioDesde() {
        return fechaInicioDesde;
    }

    public void setFechaInicioDesde(Date fechaInicioDesde) {
        this.fechaInicioDesde = fechaInicioDesde;
    }

    public Date getFechaInicioHasta() {
        return fechaInicioHasta;
    }

    public void setFechaInicioHasta(Date fechaInicioHasta) {
        this.fechaInicioHasta = fechaInicioHasta;
    }

    public Date getFechaFinDesde() {
        return fechaFinDesde;
    }

    public void setFechaFinDesde(Date fechaFinDesde) {
        this.fechaFinDesde = fechaFinDesde;
    }

    public Date getFechaFinHasta() {
        return fechaFinHasta;
    }

    public void setFechaFinHasta(Date fechaFinHasta) {
        this.fechaFinHasta = fechaFinHasta;
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToCreate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
