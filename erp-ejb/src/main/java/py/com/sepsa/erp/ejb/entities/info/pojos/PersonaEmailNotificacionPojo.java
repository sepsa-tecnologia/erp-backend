/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.io.Serializable;

/**
 * POJO para la entidad contacto email notificacion
 * @author Jonathan
 */
public class PersonaEmailNotificacionPojo implements Serializable {

    public PersonaEmailNotificacionPojo() {
    }

    public PersonaEmailNotificacionPojo(Integer idContacto, String contacto,
            Integer idEmail, String email, Integer idTipoNotificacion,
            String tipoNotificacion, Character estado) {
        this.idContacto = idContacto;
        this.contacto = contacto;
        this.idEmail = idEmail;
        this.email = email;
        this.idTipoNotificacion = idTipoNotificacion;
        this.tipoNotificacion = tipoNotificacion;
        this.estado = estado;
    }
    
    /**
     * Identificador de contacto
     */
    private Integer idContacto;
    
    /**
     * Contacto
     */
    private String contacto;
    
    /**
     * Identificador de email
     */
    private Integer idEmail;
    
    /**
     * Email
     */
    private String email;
    
    /**
     * Identificador de tipo de notificacion
     */
    private Integer idTipoNotificacion;
    
    /**
     * Tipo de notificacion
     */
    private String tipoNotificacion;
    
    /**
     * Código de Tipo de notificacion
     */
    private String codigoTipoNotificacion;
    
    /**
     * Estado
     */
    private Character estado;

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Integer getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(Integer idEmail) {
        this.idEmail = idEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setIdTipoNotificacion(Integer idTipoNotificacion) {
        this.idTipoNotificacion = idTipoNotificacion;
    }

    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public Character getEstado() {
        return estado;
    }

    public void setCodigoTipoNotificacion(String codigoTipoNotificacion) {
        this.codigoTipoNotificacion = codigoTipoNotificacion;
    }

    public String getCodigoTipoNotificacion() {
        return codigoTipoNotificacion;
    }
}
