/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.usuario;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioEmpresa;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioEmpresaParam;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;

/**
 *
 * @author Jonathan
 */
@Local
public interface UsuarioEmpresaFacade extends Facade<UsuarioEmpresa, UsuarioEmpresaParam, UserInfoImpl> {

    public Boolean validToCreate(UsuarioEmpresaParam param);

    public Boolean validToEdit(UsuarioEmpresaParam param);

    public void asociarMasivo(UsuarioEmpresaParam param, UserInfoImpl userInfo);

    public List<UsuarioEmpresa> findRelacionados(UsuarioEmpresaParam param);

    public Long findRelacionadosSize(UsuarioEmpresaParam param);
    
}
