/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.sepsa.erp.ejb.entities.proceso.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan Bernal
 */
public class LotePojo {

    public LotePojo() {
    }
    
    public LotePojo(Integer id, Integer idEmpresa, String codigoEmpresa,
            String empresa, Integer idTipoLote, String codigoTipoLote,
            String tipoLote, Integer idUsuario, String usuario, Integer idEstado,
            String codigoEstado, String estado, String nombreArchivo,
            Date fechaInsercion, Date fechaProcesamiento, Character notificado,
            String email, String observacion) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.codigoEmpresa = codigoEmpresa;
        this.empresa = empresa;
        this.idTipoLote = idTipoLote;
        this.codigoTipoLote = codigoTipoLote;
        this.tipoLote = tipoLote;
        this.idUsuario = idUsuario;
        this.usuario = usuario;
        this.idEstado = idEstado;
        this.codigoEstado = codigoEstado;
        this.estado = estado;
        this.nombreArchivo = nombreArchivo;
        this.fechaInsercion = fechaInsercion;
        this.fechaProcesamiento = fechaProcesamiento;
        this.notificado = notificado;
        this.email = email;
        this.observacion = observacion;
    }

    /**
     * Identificador
     */
    private Integer id;

    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;

    /**
     * Código de empresa
     */
    private String codigoEmpresa;

    /**
     * Empresa
     */
    private String empresa;

    /**
     * Identificador de tipo lote
     */
    private Integer idTipoLote;

    /**
     * Código de tipo de lote
     */
    private String codigoTipoLote;

    /**
     * Tipo de lote
     */
    private String tipoLote;

    /**
     * Identificador de usuario
     */
    private Integer idUsuario;

    /**
     * Usuario
     */
    private String usuario;

    /**
     * Identificador de estado
     */
    private Integer idEstado;

    /**
     * Código de estado
     */
    private String codigoEstado;

    /**
     * Estado
     */
    private String estado;

    /**
     * Nombre de archivo
     */
    private String nombreArchivo;

    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;

    /**
     * Fecha de procesamiento
     */
    private Date fechaProcesamiento;

    /**
     * Notificado
     */
    private Character notificado;

    /**
     * Email
     */
    private String email;

    /**
     * Resultado
     */
    private String resultado;

    /**
     * Observación
     */
    private String observacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getIdTipoLote() {
        return idTipoLote;
    }

    public void setIdTipoLote(Integer idTipoLote) {
        this.idTipoLote = idTipoLote;
    }

    public String getCodigoTipoLote() {
        return codigoTipoLote;
    }

    public void setCodigoTipoLote(String codigoTipoLote) {
        this.codigoTipoLote = codigoTipoLote;
    }

    public String getTipoLote() {
        return tipoLote;
    }

    public void setTipoLote(String tipoLote) {
        this.tipoLote = tipoLote;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaProcesamiento() {
        return fechaProcesamiento;
    }

    public void setFechaProcesamiento(Date fechaProcesamiento) {
        this.fechaProcesamiento = fechaProcesamiento;
    }

    public Character getNotificado() {
        return notificado;
    }

    public void setNotificado(Character notificado) {
        this.notificado = notificado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getResultado() {
        return resultado;
    }
}
