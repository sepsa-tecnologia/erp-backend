/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.comercial.TipoDescuento;
import py.com.sepsa.erp.ejb.entities.comercial.filters.TipoDescuentoParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface TipoDescuentoFacade extends Facade<TipoDescuento, TipoDescuentoParam, UserInfoImpl> {

    public TipoDescuento find(Integer id, String codigo);

}
