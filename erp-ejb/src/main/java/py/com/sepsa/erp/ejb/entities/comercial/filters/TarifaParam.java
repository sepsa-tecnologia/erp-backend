/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.filters;

import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de tarifa
 * @author Jonathan D. Bernal Fernández
 */
public class TarifaParam extends CommonParam {
    
    public TarifaParam() {
    }

    public TarifaParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de tarifa
     */
    @QueryParam("idTipoTarifa")
    private Integer idTipoTarifa;
    
    /**
     * Identificador de producto
     */
    @QueryParam("idProducto")
    private Integer idProducto;
    
    /**
     * Identificador de servicio
     */
    @QueryParam("idServicio")
    private Integer idServicio;
    
    /**
     * Identificador de moneda
     */
    @QueryParam("idMoneda")
    private Integer idMoneda;
    
    /**
     * Monto tarifas
     */
    private List<MontoTarifaParam> montoTarifas;

    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idTipoTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de tarifa"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(idMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de moneda"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la tarifa esta activa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(montoTarifas)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Debe indicar al menos un monto tarifa"));
        } else {
            for (MontoTarifaParam montoTarifa : montoTarifas) {
                montoTarifa.setIdTarifa(0);
                montoTarifa.setIdEmpresa(idEmpresa);
                montoTarifa.isValidToCreate();
            }
        }
        
        return !tieneErrores();
    }
    
    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> result = super.getCustomErrores();
        
        if(montoTarifas != null) {
            for (MontoTarifaParam montoTarifa : montoTarifas) {
                result.addAll(montoTarifa.getErrores());
            }
        }
        
        return result;
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idTipoTarifa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de tarifa"));
        }
        
        if(isNull(idServicio)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de servicio"));
        }
        
        if(isNull(idMoneda)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de moneda"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la tarifa esta activa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public Integer getIdTipoTarifa() {
        return idTipoTarifa;
    }

    public void setIdTipoTarifa(Integer idTipoTarifa) {
        this.idTipoTarifa = idTipoTarifa;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public void setMontoTarifas(List<MontoTarifaParam> montoTarifas) {
        this.montoTarifas = montoTarifas;
    }

    public List<MontoTarifaParam> getMontoTarifas() {
        return montoTarifas;
    }
}
