/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

/**
 * Clase para el manejo de los parámetros de local
 * @author Jonathan
 */
public class ProductoParam extends CommonParam {

    public ProductoParam() {
    }

    public ProductoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Código gtin
     */
    @QueryParam("codigoGtin")
    private String codigoGtin;
    
    /**
     * Código gtin eq
     */
    @QueryParam("codigoGtinEq")
    private String codigoGtinEq;

    /**
     * Código interno
     */
    @QueryParam("codigoInterno")
    private String codigoInterno;
    
    /**
     * Código interno Eq
     */
    @QueryParam("codigoInternoEq")
    private String codigoInternoEq;


    /**
     * Porcentaje de impuesto
     */
    @QueryParam("porcentajeImpuesto")
    private Integer porcentajeImpuesto;

    /**
     * Identificador de marca
     */
    @QueryParam("idMarca")
    private Integer idMarca;

    /**
     * Identificador de métrica
     */
    @QueryParam("idMetrica")
    private Integer idMetrica;

    /**
     * Código de métrica
     */
    @QueryParam("codigoMetrica")
    private String codigoMetrica;

    /**
     * Cuenta contable
     */
    @QueryParam("cuentaContable")
    private String cuentaContable;

    /**
     * Unidad mímina de venta
     */
    @QueryParam("umv")
    private BigDecimal umv;

    /**
     * Múltiplo de unidad mímina de venta
     */
    @QueryParam("multiploUmv")
    private BigDecimal multiploUmv;

    /**
     * Fecha de inserción desde
     */
    @QueryParam("fechaInsercionDesde")
    private Date fechaInsercionDesde;

    /**
     * Fecha de inserción hasta
     */
    @QueryParam("fechaInsercionHasta")
    private Date fechaInsercionHasta;
    
    /**
     * Fecha de vencimiento del lote
     */
    @QueryParam("fechaVencimientoLote")
    private Date fechaVencimientoLote;
    
    /**
     * Código de métrica
     */
    @QueryParam("nroLote")
    private String nroLote;
    
    @FormParam("uploadedFile")
    @PartType("application/octet-stream")
    private byte[] uploadedFileBytes;
    
    private MarcaParam marca;
    /**
     * Porcentaje relacionado
     */
    @QueryParam("porcentajeRelacionado")
    private BigDecimal porcentajeRelacionado;
    
     /**
     * Identificador de tipo de producto
     */
    @QueryParam("idTipoProducto")
    private Integer idTipoProducto;
    
    /**
     * Identificador del producto padre relacionado
     */
    @QueryParam("idPadre")
    private Integer idPadre;
    
    private List<ProductoParam> productosRelacionados;
    
    public boolean isValidToGetByCodigoGtin() {
        limpiarErrores();
        
        if(isNullOrEmpty(codigoGtinEq)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código gtin"));
        }
        
        return !tieneErrores();
    }
    
    public boolean isValidToGetByCodigoInterno() {
        limpiarErrores();
        
        if(isNullOrEmpty(codigoInternoEq)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código interno"));
        }
        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigoGtin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código gtin"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(porcentajeImpuesto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de impuesto"));
        }
        
        if(!isNull(marca)) {
            marca.setIdEmpresa(idEmpresa);
            marca.isValidToCreate();
        }
        
        
        if (isNull(idTipoProducto)) {
            if (isNullOrEmpty(productosRelacionados)) {
                idTipoProducto = 1;
            } else {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el identificador de tipo de producto"));
            }
        }

        
        return !tieneErrores();
    }
    
    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador"));
        }
        
        if(isNull(idEmpresa)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de empresa"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigoGtin)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código gtin"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(porcentajeImpuesto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el porcentaje de impuesto"));
        }
        
        if (isNull(idTipoProducto)) {
            if (isNullOrEmpty(productosRelacionados)) {
                idTipoProducto = 1;
            } else {
                addError(MensajePojo.createInstance()
                        .descripcion("Se debe indicar el identificador de tipo de producto"));
            }
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public void setCodigoGtinEq(String codigoGtinEq) {
        this.codigoGtinEq = codigoGtinEq;
    }

    public String getCodigoGtinEq() {
        return codigoGtinEq;
    }

    public String getCodigoGtin() {
        return codigoGtin;
    }

    public void setCodigoGtin(String codigoGtin) {
        this.codigoGtin = codigoGtin;
    }

    public String getCodigoInterno() {
        return codigoInterno;
    }

    public void setCodigoInterno(String codigoInterno) {
        this.codigoInterno = codigoInterno;
    }

    public Integer getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(Integer porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public Integer getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Integer idMarca) {
        this.idMarca = idMarca;
    }

    public MarcaParam getMarca() {
        return marca;
    }

    public void setMarca(MarcaParam marca) {
        this.marca = marca;
    }

    public void setUploadedFileBytes(byte[] uploadedFileBytes) {
        this.uploadedFileBytes = uploadedFileBytes;
    }

    public byte[] getUploadedFileBytes() {
        return uploadedFileBytes;
    }

    public Date getFechaInsercionDesde() {
        return fechaInsercionDesde;
    }

    public void setFechaInsercionDesde(Date fechaInsercionDesde) {
        this.fechaInsercionDesde = fechaInsercionDesde;
    }

    public Date getFechaInsercionHasta() {
        return fechaInsercionHasta;
    }

    public void setFechaInsercionHasta(Date fechaInsercionHasta) {
        this.fechaInsercionHasta = fechaInsercionHasta;
    }

    public Integer getIdMetrica() {
        return idMetrica;
    }

    public void setIdMetrica(Integer idMetrica) {
        this.idMetrica = idMetrica;
    }

    public String getCodigoMetrica() {
        return codigoMetrica;
    }

    public void setCodigoMetrica(String codigoMetrica) {
        this.codigoMetrica = codigoMetrica;
    }

    public BigDecimal getUmv() {
        return umv;
    }

    public void setUmv(BigDecimal umv) {
        this.umv = umv;
    }

    public BigDecimal getMultiploUmv() {
        return multiploUmv;
    }

    public void setMultiploUmv(BigDecimal multiploUmv) {
        this.multiploUmv = multiploUmv;
    }

    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

    public String getCuentaContable() {
        return cuentaContable;
    }

    public Date getFechaVencimientoLote() {
        return fechaVencimientoLote;
    }

    public void setFechaVencimientoLote(Date fechaVencimientoLote) {
        this.fechaVencimientoLote = fechaVencimientoLote;
    }

    public String getNroLote() {
        return nroLote;
    }

    public void setNroLote(String nroLote) {
        this.nroLote = nroLote;
    }

    public BigDecimal getPorcentajeRelacionado() {
        return porcentajeRelacionado;
    }

    public void setPorcentajeRelacionado(BigDecimal porcentajeRelacionado) {
        this.porcentajeRelacionado = porcentajeRelacionado;
    }

    public List<ProductoParam> getProductosRelacionados() {
        return productosRelacionados;
    }

    public void setProductosRelacionados(List<ProductoParam> productosRelacionados) {
        this.productosRelacionados = productosRelacionados;
    }

    public Integer getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(Integer idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    public Integer getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Integer idPadre) {
        this.idPadre = idPadre;
    }

    public String getCodigoInternoEq() {
        return codigoInternoEq;
    }

    public void setCodigoInternoEq(String codigoInternoEq) {
        this.codigoInternoEq = codigoInternoEq;
    }

}
