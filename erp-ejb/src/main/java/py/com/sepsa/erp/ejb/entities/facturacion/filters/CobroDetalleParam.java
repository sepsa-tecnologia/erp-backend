/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de cobro
 * @author Jonathan D. Bernal Fernández
 */
public class CobroDetalleParam extends CommonParam {
    
    public CobroDetalleParam() {
    }

    public CobroDetalleParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de cobro
     */
    @QueryParam("idCobro")
    private Integer idCobro;
    
    /**
     * Nro de linea
     */
    @QueryParam("nroLinea")
    private Integer nroLinea;
    
    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;
    
    /**
     * Identificador de tipo de cobro
     */
    @QueryParam("idTipoCobro")
    private Integer idTipoCobro;
    
    /**
     * Código de tipo de cobro
     */
    @QueryParam("codigoTipoCobro")
    private String codigoTipoCobro;
    
    /**
     * Identificador de cheque
     */
    @QueryParam("idCheque")
    private Integer idCheque;
    
    /**
     * Monto de cobro
     */
    @QueryParam("montoCobro")
    private BigDecimal montoCobro;
    
    /**
     * Identificador de concepto de cobro
     */
    @QueryParam("idConceptoCobro")
    private Integer idConceptoCobro;
    
    /**
     * Código de concepto de cobro
     */
    @QueryParam("codigoConceptoCobro")
    private String codigoConceptoCobro;
    
    /**
     * Identificador de entidad financiera
     */
    @QueryParam("idEntidadFinanciera")
    private Integer idEntidadFinanciera;
    
    /**
     * Monto de cobro en guaraníes
     */
    @QueryParam("montoCobroGuaranies")
    private BigDecimal montoCobroGuaranies;
    
    /**
     * Parámetros de cheque
     */
    private ChequeParam cheque;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idCobro)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de cobro"));
        }
        
        if(isNull(nroLinea)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nro de línea"));
        }
        
//        if(isNull(idFactura)) {
//            addError(MensajePojo.createInstance()
//                    .descripcion("Se debe indicar el identificador de factura"));
//        }
        
        if(isNull(idTipoCobro) && isNullOrEmpty(codigoTipoCobro)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de tipo de cobro"));
        }
        
        if(isNull(montoCobro)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el monto de cobro para el detalle de cobro"));
        }
        
        if(isNull(idEstado) && isNullOrEmpty(codigoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de estado del detalle de cobro"));
        }
        
        if(isNull(idConceptoCobro) && isNullOrEmpty(codigoConceptoCobro)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador/código de concepto de cobro"));
        }
        
        if(cheque != null) {
            cheque.setIdEmpresa(idEmpresa);
            cheque.isValidToCreate();
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> result = new ArrayList<>();
        
        if(cheque != null) {
            result.addAll(cheque.getErrores());
        }
        
        return result;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdCobro() {
        return idCobro;
    }

    public void setIdCobro(Integer idCobro) {
        this.idCobro = idCobro;
    }

    public Integer getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(Integer nroLinea) {
        this.nroLinea = nroLinea;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdTipoCobro() {
        return idTipoCobro;
    }

    public void setIdTipoCobro(Integer idTipoCobro) {
        this.idTipoCobro = idTipoCobro;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public BigDecimal getMontoCobro() {
        return montoCobro;
    }

    public void setMontoCobro(BigDecimal montoCobro) {
        this.montoCobro = montoCobro;
    }

    public Integer getIdConceptoCobro() {
        return idConceptoCobro;
    }

    public void setIdConceptoCobro(Integer idConceptoCobro) {
        this.idConceptoCobro = idConceptoCobro;
    }

    public void setCheque(ChequeParam cheque) {
        this.cheque = cheque;
    }

    public ChequeParam getCheque() {
        return cheque;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setCodigoTipoCobro(String codigoTipoCobro) {
        this.codigoTipoCobro = codigoTipoCobro;
    }

    public String getCodigoTipoCobro() {
        return codigoTipoCobro;
    }

    public void setCodigoConceptoCobro(String codigoConceptoCobro) {
        this.codigoConceptoCobro = codigoConceptoCobro;
    }

    public String getCodigoConceptoCobro() {
        return codigoConceptoCobro;
    }

    public BigDecimal getMontoCobroGuaranies() {
        return montoCobroGuaranies;
    }

    public void setMontoCobroGuaranies(BigDecimal montoCobroGuaranies) {
        this.montoCobroGuaranies = montoCobroGuaranies;
    }
    
}
