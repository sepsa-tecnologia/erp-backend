package py.com.sepsa.erp.ejb.facades;

import static com.itextpdf.layout.properties.TextAlignment.CENTER;
import static com.itextpdf.layout.properties.VerticalAlignment.TOP;
import static java.lang.Math.PI;

import java.io.IOException;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.extgstate.PdfExtGState;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Text;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 *
 * @author Ralf Adam
 */
public class WatermarkUtils {

    /**
     * metodo para agregar una marca de agua a un archivo pdf existente}
     * 
     * @param bytes     byte array conteniendo los bytes del pdf
     * @param watermark marca de agua a poner en el pdf
     * @param fontSize tamaño de las letras de la marca de agua
     *
     * @return ByteArrayOutputStream; el output stream conteniendo el pdf modificado
     */
    public static ByteArrayOutputStream addWatermarkToExistingPdf(byte[] bytes, String watermark, Integer fontSize) throws IOException {
        //fontSize = ((fontSize == null) ? 76 : fontSize);
        if(fontSize == null){fontSize = 76;}

        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);//byte input
        ByteArrayOutputStream baos = new ByteArrayOutputStream();//byte output

        //crea el pdf, el inputStream con el contenido original y el outputStream con el contenido modificado
        try (PdfDocument pdfDocument = new PdfDocument(new PdfReader(bais), new PdfWriter(baos))) {
            Document document = new Document(pdfDocument);

            //crea la marca de agua
            Paragraph paragraph = createWatermarkParagraph(watermark, fontSize);
            PdfExtGState transparentGraphicState = new PdfExtGState().setFillOpacity(0.5f);
            
            //añade la marca a cada pagina
            for (int i = 1; i <= document.getPdfDocument()
                    .getNumberOfPages(); i++) {
                addWatermarkToExistingPage(document, i, paragraph, transparentGraphicState, 0f);
            }
            //guarda el contenido al outputStream
            baos.writeTo(document.getPdfDocument().getWriter().getOutputStream());
        }
        return baos;
    }

    /**
     * metodo para crear la marca de agua
     * 
     * @param watermark
     * @param fontSize
     * 
     * @return parrafo, retorna el texto para la marca de agua
     */
    public static Paragraph createWatermarkParagraph(String watermark, Integer fontSize) throws IOException {
        PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);
        Text text = new Text(watermark);//  TEXTO
        text.setFont(font);//   FUENTE
        text.setFontSize(fontSize);//   TAMAÑO
        text.setOpacity(0.3f);//    OPACIDAD

        return new Paragraph(text);
    }

    /**
     * metodo para agregar una marca de agua 
     * a una pagina de un documento pdf
     * 
     * @param document
     * @param pageIndex
     * @param graphicState
     * @param verticalOffset
     * 
     * llamado por cada pagina del pdf por el metodo addWatermarkToExistingPdf
     */
    public static void addWatermarkToExistingPage(Document document, int pageIndex, Paragraph paragraph, PdfExtGState graphicState, float verticalOffset) {
        PdfDocument pdfDoc = document.getPdfDocument();
        PdfPage pdfPage = pdfDoc.getPage(pageIndex);
        PageSize pageSize = (PageSize) pdfPage.getPageSizeWithRotation();

        float x = (pageSize.getLeft() + pageSize.getRight()) / 2;
        float y = (pageSize.getTop() + pageSize.getBottom()) / 2;
        
        PdfCanvas over = new PdfCanvas(pdfDoc.getPage(pageIndex));
        over.saveState();
        over.setExtGState(graphicState);
        
        float xOffset = 100f / 2;
        float rotationInRadians = (float) (PI / 180 * 45f);
        
        document.showTextAligned(paragraph, x - xOffset, y + verticalOffset, pageIndex, CENTER, TOP, rotationInRadians);
        document.flush();
        
        over.restoreState();
        over.release();
    }
}
