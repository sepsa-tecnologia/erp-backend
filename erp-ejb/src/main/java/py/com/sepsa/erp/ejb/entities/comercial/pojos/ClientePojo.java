/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.comercial.pojos;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class ClientePojo {

    public ClientePojo() {
    }

    public ClientePojo(Integer idCliente, Integer idEmpresa, String empresa,
            Integer idEstado, String estado, String codigoEstado,
            Character proveedor, Character comprador, String razonSocial,
            String nroDocumento, Integer idTipoDocumento, String tipoDocumento,
            String codigoTipoDocumento, Integer idTipoPersona, String tipoPersona,
            String codigoTipoPersona, Integer idCanalVenta, String canalVenta,
            Integer idDireccion, String direccion, Integer idDepartamento,
            String departamento, Integer idDistrito, String distrito,
            Integer idCiudad, String ciudad, Integer numeroDireccion, String codigoPais) {
        this.idCliente = idCliente;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.idEstado = idEstado;
        this.estado = estado;
        this.codigoEstado = codigoEstado;
        this.proveedor = proveedor;
        this.comprador = comprador;
        this.razonSocial = razonSocial;
        this.nroDocumento = nroDocumento;
        this.idTipoDocumento = idTipoDocumento;
        this.tipoDocumento = tipoDocumento;
        this.codigoTipoDocumento = codigoTipoDocumento;
        this.idTipoPersona = idTipoPersona;
        this.tipoPersona = tipoPersona;
        this.codigoTipoPersona = codigoTipoPersona;
        this.idCanalVenta = idCanalVenta;
        this.canalVenta = canalVenta;
        this.idDireccion = idDireccion;
        this.direccion = direccion;
        this.idDepartamento = idDepartamento;
        this.departamento = departamento;
        this.idDistrito = idDistrito;
        this.distrito = distrito;
        this.idCiudad = idCiudad;
        this.ciudad = ciudad;
        this.numeroDireccion = numeroDireccion;
        this.codigoPais = codigoPais;
    }
    
    /**
     * Identificador de cliente
     */
    private Integer idCliente;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Proveedor
     */
    private Character proveedor;
    
    /**
     * Comprador
     */
    private Character comprador;
    
    /**
     * Razón social
     */
    private String razonSocial;
    
    /**
     * Nro de documento
     */
    private String nroDocumento;
    
    /**
     * Identificador de tipo de documento
     */
    private Integer idTipoDocumento;
    
    /**
     * Tipo de documento
     */
    private String tipoDocumento;
    
    /**
     * Código de tipo documento
     */
    private String codigoTipoDocumento;
    
    /**
     * Identificador de tipo de persona
     */
    private Integer idTipoPersona;
    
    /**
     * Tipo persona
     */
    private String tipoPersona;
    
    /**
     * Código de tipo persona
     */
    private String codigoTipoPersona;
    
    /**
     * Identificador de naturaleza de cliente
     */
    private Integer idNaturalezaCliente;
    
    /**
     * Naturaleza de cliente
     */
    private String naturalezaCliente;
    
    /**
     * Código de naturaleza de cliente
     */
    private String codigoNaturalezaCliente;
    
    /**
     * Identificador de canal de venta
     */
    private Integer idCanalVenta;
    
    /**
     * Canal de venta
     */
    private String canalVenta;
    
    /**
     * Identificador de dirección
     */
    private Integer idDireccion;
    
    /**
     * Dirección
     */
    private String direccion;
    
    /**
     * Identificador de distrito
     */
    private Integer idDepartamento;
    
    /**
     * Departamento
     */
    private String departamento;
    
    /**
     * Identificador de distrito
     */
    private Integer idDistrito;
    
    /**
     * Distrito
     */
    private String distrito;
    
    /**
     * Identificador de ciudad
     */
    private Integer idCiudad;
    
    /**
     * Ciudad
     */
    private String ciudad;
    
    /**
     * Nro de direccion
     */
    private Integer numeroDireccion;
    
    /**
     * Codigo de País
     */
    private String codigoPais;    

    
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Character getProveedor() {
        return proveedor;
    }

    public void setProveedor(Character proveedor) {
        this.proveedor = proveedor;
    }

    public Character getComprador() {
        return comprador;
    }

    public void setComprador(Character comprador) {
        this.comprador = comprador;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getCodigoTipoDocumento() {
        return codigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(String codigoTipoDocumento) {
        this.codigoTipoDocumento = codigoTipoDocumento;
    }

    public Integer getIdCanalVenta() {
        return idCanalVenta;
    }

    public void setIdCanalVenta(Integer idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    public String getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(String canalVenta) {
        this.canalVenta = canalVenta;
    }

    public Integer getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Integer idDireccion) {
        this.idDireccion = idDireccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public Integer getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Integer idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setNumeroDireccion(Integer numeroDireccion) {
        this.numeroDireccion = numeroDireccion;
    }

    public Integer getNumeroDireccion() {
        return numeroDireccion;
    }

    public Integer getIdTipoPersona() {
        return idTipoPersona;
    }

    public void setIdTipoPersona(Integer idTipoPersona) {
        this.idTipoPersona = idTipoPersona;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getCodigoTipoPersona() {
        return codigoTipoPersona;
    }

    public void setCodigoTipoPersona(String codigoTipoPersona) {
        this.codigoTipoPersona = codigoTipoPersona;
    }

    public Integer getIdNaturalezaCliente() {
        return idNaturalezaCliente;
}

    public void setIdNaturalezaCliente(Integer idNaturalezaCliente) {
        this.idNaturalezaCliente = idNaturalezaCliente;
    }

    public String getNaturalezaCliente() {
        return naturalezaCliente;
    }

    public void setNaturalezaCliente(String naturalezaCliente) {
        this.naturalezaCliente = naturalezaCliente;
    }

    public String getCodigoNaturalezaCliente() {
        return codigoNaturalezaCliente;
    }

    public void setCodigoNaturalezaCliente(String codigoNaturalezaCliente) {
        this.codigoNaturalezaCliente = codigoNaturalezaCliente;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }
   
}
