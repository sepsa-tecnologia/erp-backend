/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LazilyParsedNumber;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaCompra;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCredito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaCreditoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebito;
import py.com.sepsa.erp.ejb.entities.facturacion.NotaDebitoDetalle;
import py.com.sepsa.erp.ejb.entities.facturacion.ParametroAdicional;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.CobroDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDescuentoParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaDetalleParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ParametroAdicionalParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.ReporteComprobanteParam;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.TalonarioParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.CobroDetallePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.CobroPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaDescuentoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaDetallePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaCreditoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.NotaDebitoPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.OrdenCompraPojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.RegistroComprobantePojo;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.TalonarioPojo;
import py.com.sepsa.erp.ejb.entities.info.filters.ReporteEmpresaParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.ControlInventarioPojo;
import py.com.sepsa.erp.reporte.jasper.SepsaJRDataSource;
import py.com.sepsa.erp.reporte.pojos.ReporteParams;
import py.com.sepsa.erp.reporte.pojos.cobro.CobroDetalleFactura;
import py.com.sepsa.erp.reporte.pojos.cobro.CobroDetallePago;
import py.com.sepsa.erp.reporte.pojos.cobro.CobroParam;
import py.com.sepsa.erp.reporte.pojos.factura.EstadoCuentaDetalle;
import py.com.sepsa.erp.reporte.pojos.factura.EstadoCuentaParam;
import py.com.sepsa.erp.reporte.pojos.factura.FacturaAutoimpresorDescuento;
import py.com.sepsa.erp.reporte.pojos.factura.FacturaAutoimpresorDetalle;
import py.com.sepsa.erp.reporte.pojos.factura.FacturaAutoimpresorParam;
import py.com.sepsa.erp.reporte.pojos.factura.ReporteCompraParam;
import py.com.sepsa.erp.reporte.pojos.factura.ReporteVentaFacturaParam;
import py.com.sepsa.erp.reporte.pojos.inventario.ControlInventarioParam;
import py.com.sepsa.erp.reporte.pojos.notacredito.NotaCreditoAutoimpresorDetalle;
import py.com.sepsa.erp.reporte.pojos.notacredito.NotaCreditoAutoimpresorParam;
import py.com.sepsa.erp.reporte.pojos.notacredito.ReporteVentaNotaCreditoParam;
import py.com.sepsa.erp.reporte.pojos.notadebito.NotaDebitoAutoimpresorDetalle;
import py.com.sepsa.erp.reporte.pojos.notadebito.NotaDebitoAutoimpresorParam;
import py.com.sepsa.erp.reporte.pojos.notadebito.ReporteVentaNotaDebitoParam;
import py.com.sepsa.erp.reporte.pojos.ordencompra.OrdenCompraParam;
import py.com.sepsa.utils.misc.Closer;
import py.com.sepsa.utils.misc.Lists;
import py.com.sepsa.utils.misc.NumberToLetterConverter;
import py.com.sepsa.utils.misc.Units;
import py.com.sepsa.utils.zip.ZipUtils;

/**
 *
 * @author Jonathan Bernal
 */
@Stateless
@Log4j2
public class ReportUtils implements Serializable {

    @EJB
    private Facades facades;

    public InputStream getResource(String resource, Closer closer) {

        InputStream result = null;

        try {
            result = new URL(resource).openStream();
            closer.add(result);
            return result;
        } catch (Exception e) {
            log.fatal("Error al obtener recurso", e);
        }

        try {
            result = new FileInputStream(resource);
            closer.add(result);
            return result;
        } catch (Exception e) {
            log.fatal("Error al obtener recurso", e);
        }

        return result;
    }

    /**
     * Obtiene los parametros el reporte de venta de las facturas
     *
     * @param closer closer
     * @param param Parámetros
     * @return Mapa de parametros
     */
    public ReporteVentaFacturaParam getReporteVentaFacturaParam(Closer closer, FacturaParam param) {
        ReporteVentaFacturaParam result = new ReporteVentaFacturaParam();
        result.setLocale(new Locale("es", "PY"));
        result.setIdEmpresa(param.getIdEmpresa());
        result.setIdCliente(param.getIdCliente());
        result.setMontoAnuladasCero(param.getMontoAnuladasCero());
        result.setFechaDesde(param.getFechaDesde());
        result.setFechaHasta(param.getFechaHasta());
        result.setAnulado(param.getAnulado());
        result.setIdLocal(param.getIdLocalTalonario());
        result.setDetalles(new ArrayList<>());
        return result;
    }

    /**
     * Obtiene los parametros del pdf de control de inventario
     *
     * @param closer closer
     * @param param Parámetros
     * @return Mapa de parametros
     */
    public ControlInventarioParam getControlInventarioParam(Closer closer, ControlInventarioPojo param) {
        ControlInventarioParam result = new ControlInventarioParam();
        result.setLocale(new Locale("es", "PY"));
        result.setIdEmpresa(param.getIdEmpresa());
        result.setIdControlInventario(param.getId());
        result.setFechaInsercion(param.getFechaInsercion());
        result.setFechaInicio(param.getFechaInicio());
        result.setFechaFin(param.getFechaFin());
        result.setUsuarioEncargado(param.getUsuarioEncargado());
        result.setUsuarioSupervisor(param.getUsuarioSupervisor());
        result.setMotivo(param.getMotivo());
        result.setCodigo(param.getCodigo());
        return result;
    }

    /**
     * Obtiene los parametros del pdf de orden de compra
     *
     * @param closer closer
     * @param param Parámetros
     * @return Mapa de parametros
     */
    public OrdenCompraParam getOrdenCompraParam(Closer closer, OrdenCompraPojo param) {
        OrdenCompraParam result = new OrdenCompraParam();
        result.setLocale(new Locale("es", "PY"));
        result.setIdEmpresa(param.getIdEmpresa());
        result.setIdOrdenCompra(param.getId());
        result.setFechaInsercion(param.getFechaInsercion());
        result.setFechaRecepcion(param.getFechaRecepcion());
        result.setNroOrdenCompra(param.getNroOrdenCompra());
        result.setLocalOrigen(param.getLocalOrigen());
        result.setLocalDestino(param.getLocalDestino());
        result.setClienteOrigen(param.getClienteOrigen());
        result.setClienteDestino(param.getClienteDestino());
        return result;
    }

    /**
     * Obtiene los parametros el reporte de venta
     *
     * @param closer closer
     * @param idEmpresa Identificador de empresa
     * @param fechaDesde Fecha hasta
     * @param fechaHasta Fecha desde
     * @param ncs Lista de ncs
     * @return Mapa de parametros
     */
    public ReporteVentaNotaCreditoParam getReporteVentaNotaCreditoParam(Closer closer, Integer idEmpresa, Integer idCliente, Date fechaDesde, Date fechaHasta, List<NotaCreditoPojo> ncs) {

        List<NotaCreditoAutoimpresorParam> list = new ArrayList<>();

        BigDecimal totalGravada10 = BigDecimal.ZERO;
        BigDecimal totalIva10 = BigDecimal.ZERO;
        BigDecimal totalGravada5 = BigDecimal.ZERO;
        BigDecimal totalIva5 = BigDecimal.ZERO;
        BigDecimal totalExento = BigDecimal.ZERO;
        BigDecimal total = BigDecimal.ZERO;

        for (NotaCreditoPojo nc : ncs) {
            NotaCreditoAutoimpresorParam ncparam = new NotaCreditoAutoimpresorParam();
            ncparam.setTimbrado(nc.getTimbrado());
            ncparam.setLocalTalonario(nc.getDescripcionLocalTalonario());

            ncparam.setMoneda(nc.getMoneda());
            ncparam.setCodigoMoneda(nc.getCodigoMoneda());
            ncparam.setAnulado(nc.getAnulado().toString());
            ncparam.setNroFacturas(nc.getNroFacturas());
            ncparam.setFechaEmision(nc.getFecha());
            ncparam.setNroNotaCredito(nc.getNroNotaCredito());
            ncparam.setNroDocumentoReceptor(nc.getRuc());
            ncparam.setRazonSocialReceptor(nc.getRazonSocial());
            ncparam.setDireccionReceptor(nc.getDireccion());
            ncparam.setNroCasaReceptor(nc.getNroCasa());
            ncparam.setTelefonoReceptor(nc.getTelefono());
            ncparam.setObservacion(nc.getObservacion());
            ncparam.setMontoIva5(nc.getMontoIva5());
            ncparam.setMontoImponible5(nc.getMontoImponible5());
            ncparam.setMontoTotal5(nc.getMontoTotal5());
            ncparam.setMontoIva10(nc.getMontoIva10());
            ncparam.setMontoImponible10(nc.getMontoImponible10());
            ncparam.setMontoTotal10(nc.getMontoTotal10());
            ncparam.setMontoTotalExento(nc.getMontoTotalExento());
            ncparam.setMontoIvaTotal(nc.getMontoIvaTotal());
            ncparam.setMontoImponibleTotal(nc.getMontoImponibleTotal());
            ncparam.setMontoTotalNotaCredito(nc.getMontoTotalNotaCredito());
            ncparam.setMontoTotalGuaranies(nc.getMontoTotalNotaCredito());

            totalGravada10 = totalGravada10.add(nc.getMontoImponible10());
            totalIva10 = totalIva10.add(nc.getMontoIva10());
            totalGravada5 = totalGravada5.add(nc.getMontoImponible5());
            totalIva5 = totalIva5.add(nc.getMontoIva5());
            totalExento = totalExento.add(nc.getMontoTotalExento());
            total = total.add(nc.getMontoTotalNotaCredito());

            list.add(ncparam);
        }

        ReporteVentaNotaCreditoParam result = new ReporteVentaNotaCreditoParam();
        result.setIdEmpresa(idEmpresa);
        result.setIdCliente(idCliente);
        result.setLocale(new Locale("es", "PY"));
        result.setFechaDesde(fechaDesde);
        result.setFechaHasta(fechaHasta);
        result.setTotalGravada10(totalGravada10);
        result.setTotalIva10(totalIva10);
        result.setTotalGravada5(totalGravada5);
        result.setTotalIva5(totalIva5);
        result.setTotalExento(totalExento);
        result.setTotal(total);
        result.setDetalles(list);

        return result;
    }

    /**
     * Obtiene los parametros el estado de cuenta
     *
     * @param closer closer
     * @param cliente Cliente
     * @param idEmpresa Identificador de empresa
     * @param fechaDesde Fecha hasta
     * @param fechaHasta Fecha desde
     * @param saldoTotal Salto total
     * @param detalles Lista de detalle
     * @return Mapa de parametros
     */
    public EstadoCuentaParam getEstadoCuentaParam(Closer closer, String cliente, Integer idEmpresa, Date fechaDesde, Date fechaHasta, BigDecimal saldoTotal, List<EstadoCuentaDetalle> detalles) {

        EstadoCuentaParam result = new EstadoCuentaParam();
        result.setLocale(new Locale("es", "PY"));
        result.setIdEmpresa(idEmpresa);
        result.setFechaGeneracion(Calendar.getInstance().getTime());
        result.setFechaDesde(fechaDesde);
        result.setFechaHasta(fechaHasta);
        result.setSaldoTotal(saldoTotal);
        result.setCliente(cliente);
        result.setDetalles(detalles);

        return result;
    }

    /**
     * Obtiene los parametros el reporte de compra
     *
     * @param closer closer
     * @param idEmpresa Identificador de empresa
     * @param fechaDesde Fecha hasta
     * @param fechaHasta Fecha desde
     * @param facturas Lista de facturas
     * @return Mapa de parametros
     */
    public ReporteCompraParam getReporteCompraParam(Closer closer, Integer idEmpresa, Date fechaDesde, Date fechaHasta, List<FacturaCompra> facturas) {

        List<FacturaAutoimpresorParam> list = new ArrayList<>();

        BigDecimal totalGravada10 = BigDecimal.ZERO;
        BigDecimal totalIva10 = BigDecimal.ZERO;
        BigDecimal totalGravada5 = BigDecimal.ZERO;
        BigDecimal totalIva5 = BigDecimal.ZERO;
        BigDecimal totalExento = BigDecimal.ZERO;
        BigDecimal total = BigDecimal.ZERO;

        for (FacturaCompra factura : facturas) {
            FacturaAutoimpresorParam fparam = new FacturaAutoimpresorParam();
            fparam.setTimbrado(factura.getNroTimbrado());
            fparam.setFechaFinVigencia(factura.getFechaVencimientoTimbrado());

            fparam.setFechaEmision(factura.getFecha());
            fparam.setNroFactura(factura.getNroFactura());
            fparam.setCondicionVenta(FacturaAutoimpresorParam.CondicionVenta.valueOf(factura.getTipoFactura().getCodigo()));
            fparam.setCondicionVentaCredito(factura.getDiasCredito() == null
                    ? null
                    : String.format("%d DIAS", factura.getDiasCredito()));
            fparam.setNroDocumentoReceptor(factura.getRuc());
            fparam.setRazonSocialReceptor(factura.getRazonSocial());
            fparam.setNroOrden(Units.execute(() -> factura.getOrdenCompra().getNroOrdenCompra()));
            fparam.setFechaOrden(Units.execute(() -> factura.getOrdenCompra().getFechaRecepcion()));
            fparam.setMontoIva5(factura.getMontoIva5());
            fparam.setMontoImponible5(factura.getMontoImponible5());
            fparam.setMontoTotal5(factura.getMontoTotal5());
            fparam.setMontoIva10(factura.getMontoIva10());
            fparam.setMontoImponible10(factura.getMontoImponible10());
            fparam.setMontoTotal10(factura.getMontoTotal10());
            fparam.setMontoTotalExento(factura.getMontoTotalExento());
            fparam.setMontoIvaTotal(factura.getMontoIvaTotal());
            fparam.setMontoImponibleTotal(factura.getMontoImponibleTotal());
            fparam.setMontoTotalFactura(factura.getMontoTotalFactura());

            totalGravada10 = totalGravada10.add(factura.getMontoImponible10());
            totalIva10 = totalIva10.add(factura.getMontoIva10());
            totalGravada5 = totalGravada5.add(factura.getMontoImponible5());
            totalIva5 = totalIva5.add(factura.getMontoIva5());
            totalExento = totalExento.add(factura.getMontoTotalExento());
            total = total.add(factura.getMontoTotalFactura());

            list.add(fparam);
        }

        ReporteCompraParam result = new ReporteCompraParam();
        result.setLocale(new Locale("es", "PY"));
        result.setIdEmpresa(idEmpresa);
        result.setFechaDesde(fechaDesde);
        result.setFechaHasta(fechaHasta);
        result.setTotalGravada10(totalGravada10);
        result.setTotalIva10(totalIva10);
        result.setTotalGravada5(totalGravada5);
        result.setTotalIva5(totalIva5);
        result.setTotalExento(totalExento);
        result.setTotal(total);
        result.setDetalles(list);

        return result;
    }

    /**
     * Obtiene los parametros de la factura para la impresion en PDF
     *
     * @param closer closer
     * @param factura Factura
     * @return Mapa de parametros
     */
    public FacturaAutoimpresorParam getParam(Closer closer, FacturaPojo factura) {

        FacturaAutoimpresorParam fparam = new FacturaAutoimpresorParam();
        fparam.setLocale(new Locale("es", "PY"));
        fparam.setId(factura.getId());
        fparam.setIdEmpresa(factura.getIdEmpresa());

        TalonarioParam tparam = new TalonarioParam();
        tparam.setId(factura.getIdTalonario());
        TalonarioPojo talonario = facades.getTalonarioFacade().findFirstPojo(tparam);

        fparam.setAnulado(factura.getAnulado().toString());
        fparam.setTimbrado(talonario.getTimbrado());
        fparam.setDireccionLocalTalonario(talonario.getDireccionLocalTalonario());
        fparam.setLocalTalonario(talonario.getLocalTalonario());
        fparam.setIdExternoLocalTalonario(talonario.getIdExternoLocalTalonario());
        fparam.setFechaInicioVigencia(talonario.getFechaInicio());
        fparam.setFechaFinVigencia(talonario.getFechaVencimiento());
        fparam.setFechaResolucion(talonario.getFechaResolucion());
        fparam.setNroResolucion(talonario.getNroResolucion());

        String rutaLogo = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "LOGO");
        String descripcionNegocioEmisor = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "DESCRIPCION_NEGOCIO");
        String rucEmisor = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "RUC");
        String razonSocialEmisor = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "RAZON_SOCIAL");
        String direccionEmisor = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "DIRECCION");
        String telefonoEmisor = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "TELEFONO");
        String emailEmisor = facades.getConfiguracionValorFacade().getCVValor(factura.getIdEmpresa(), "EMAIL");

        OrdenCompraPojo ordenCompraPojo = null;
        if(factura.getIdOrdenCompra() != null) {
            py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam ocparam
                    = new py.com.sepsa.erp.ejb.entities.facturacion.filters.OrdenCompraParam();
            ocparam.setId(factura.getIdOrdenCompra());
            ordenCompraPojo = facades.getOrdenCompraFacade().findFirstPojo(ocparam);
        }
        
        if(ordenCompraPojo != null) {
            fparam.setLocalOrigen(ordenCompraPojo.getLocalOrigen());
            fparam.setLocalDestino(ordenCompraPojo.getLocalDestino());
        }
        
        InputStream logo = getResource(rutaLogo, closer);
        fparam.setLogo(logo);
        fparam.setDescripcionNegocioEmisor(descripcionNegocioEmisor);
        fparam.setRucEmisor(rucEmisor);
        fparam.setRazonSocialEmisor(razonSocialEmisor);
        fparam.setDireccionEmisor(direccionEmisor);
        fparam.setTelefonoEmisor(telefonoEmisor);
        fparam.setEmailEmisor(emailEmisor);
        fparam.setFechaEmision(factura.getFecha());
        fparam.setNroFactura(factura.getNroFactura());
        fparam.setMoneda(factura.getMoneda());
        fparam.setCodigoMoneda(factura.getCodigoMoneda());
        fparam.setCondicionVenta(FacturaAutoimpresorParam.CondicionVenta.valueOf(factura.getCodigoTipoFactura()));
        fparam.setCondicionVentaCredito(factura.getDiasCredito() == null
                ? null
                : String.format("%d DIAS", factura.getDiasCredito()));
        fparam.setNroDocumentoReceptor(factura.getRuc());
        fparam.setRazonSocialReceptor(factura.getRazonSocial());
        fparam.setDireccionReceptor(factura.getDireccion());
        fparam.setNroCasaReceptor(factura.getNroCasa());
        fparam.setTelefonoReceptor(factura.getTelefono());
        fparam.setEmailReceptor(factura.getEmail());
        fparam.setNroOrden(factura.getNroOrdenCompra());
        fparam.setFechaOrden(factura.getFechaRecepcion());
        fparam.setMontoIva5(factura.getMontoIva5());
        fparam.setMontoImponible5(factura.getMontoImponible5());
        fparam.setMontoTotal5(factura.getMontoTotal5());
        fparam.setMontoIva10(factura.getMontoIva10());
        fparam.setMontoImponible10(factura.getMontoImponible10());
        fparam.setMontoTotal10(factura.getMontoTotal10());
        fparam.setMontoTotalExento(factura.getMontoTotalExento());
        fparam.setMontoIvaTotal(factura.getMontoIvaTotal());
        fparam.setMontoTotalDescuentoParticular(factura.getMontoTotalDescuentoParticular());
        fparam.setMontoTotalDescuentoGlobal(factura.getMontoTotalDescuentoGlobal());
        fparam.setMontoImponibleTotal(factura.getMontoImponibleTotal());
        fparam.setMontoTotalFactura(factura.getMontoTotalFactura());
        fparam.setMontoTotalGuaranies(factura.getMontoTotalGuaranies());

        String montoTotalLetras = "--";
        try {
            NumberToLetterConverter.Moneda moneda = NumberToLetterConverter.Moneda.valueOf(factura.getCodigoMoneda());
            montoTotalLetras = NumberToLetterConverter.convertNumberToLetter(factura.getMontoTotalFactura(), moneda) + ".---";
        } catch (Exception e) {
            e.printStackTrace();
        }
        fparam.setMontoTotalLetras(montoTotalLetras);

        List<FacturaAutoimpresorDetalle> list = new ArrayList<>();

        FacturaDetalleParam fdparam = new FacturaDetalleParam();
        fdparam.setIdFactura(factura.getId());
        fdparam.setFirstResult(0);
        fdparam.setPageSize(1000);
        List<FacturaDetallePojo> fdlist = facades.getFacturaDetalleFacade().findPojo(fdparam);

        for (FacturaDetallePojo item : fdlist) {
            FacturaAutoimpresorDetalle fadparam = new FacturaAutoimpresorDetalle();
            fadparam.setDescripcion(item.getDescripcion());
            fadparam.setCodigoGtin(Units.execute(() -> item.getCodigoGtin()));
            fadparam.setCodigoInterno(Units.execute(() -> item.getCodigoInterno()));
            fadparam.setCantidad(item.getCantidad().intValue());
            fadparam.setPorcentajeIva(item.getPorcentajeIva());
            fadparam.setMontoDescuentoParticular(item.getMontoDescuentoParticular());
            fadparam.setMontoDescuentoGlobal(item.getMontoDescuentoGlobal());
            fadparam.setDescuentoParticularUnitario(item.getDescuentoParticularUnitario());
            fadparam.setDescuentoGlobalUnitario(item.getDescuentoGlobalUnitario());
            fadparam.setPrecioUnitarioConIva(item.getPrecioUnitarioConIva());
            fadparam.setPrecioUnitarioSinIva(item.getPrecioUnitarioSinIva());
            fadparam.setMontoTotal(item.getMontoTotal());

            switch (item.getPorcentajeIva()) {
                case 0:
                    fadparam.setMontoExento(item.getMontoTotal());
                    break;

                case 5:
                    fadparam.setMontoTotal5(item.getMontoTotal());
                    break;

                case 10:
                    fadparam.setMontoTotal10(item.getMontoTotal());
                    break;
            }

            list.add(fadparam);
        }

        fparam.setDetalles(list);

        List<FacturaAutoimpresorDescuento> list1 = new ArrayList<>();

        FacturaDescuentoParam fdeparam = new FacturaDescuentoParam();
        fdeparam.setIdFactura(factura.getId());
        fdeparam.setFirstResult(0);
        fdeparam.setPageSize(1000);
        List<FacturaDescuentoPojo> fdelist = facades.getFacturaDescuentoFacade().findPojo(fdeparam);

        for (FacturaDescuentoPojo item : fdelist) {
            FacturaAutoimpresorDescuento fadparam = new FacturaAutoimpresorDescuento();
            fadparam.setCodigoTipoDescuento(item.getCodigoTipoDescuento());
            fadparam.setTipoDescuento(item.getTipoDescuento());
            fadparam.setCodigo(item.getCodigo());
            fadparam.setDescripcion(item.getDescripcion());
            fadparam.setValorDescuento(item.getValorDescuento());

            list1.add(fadparam);
        }

        fparam.setDescuentos(list1);

        try {
            JsonObject object = new JsonParser()
                    .parse(factura.getObservacion())
                    .getAsJsonObject();
            fparam.valoresAdicionales(object);
        } catch (Exception e) {
            log.fatal("Error", e);
            fparam.setObservacion(factura.getObservacion());
        }

        return fparam;
    }

    /**
     * Obtiene los parametros del cobro para la impresion en PDF
     *
     * @param closer closer
     * @param cobro Cobro
     * @return Mapa de parametros
     */
    public CobroParam getCobroParam(Closer closer, CobroPojo cobro) {

        String rutaLogo = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "LOGO");
        String descripcionNegocioEmisor = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "DESCRIPCION_NEGOCIO");
        String rucEmisor = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "RUC");
        String razonSocialEmisor = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "RAZON_SOCIAL");
        String direccionEmisor = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "DIRECCION");
        String telefonoEmisor = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "TELEFONO");
        String emailEmisor = facades.getConfiguracionValorFacade().getCVValor(cobro.getIdEmpresa(), "EMAIL");

        ParametroAdicionalParam param = new ParametroAdicionalParam();
        param.setIdEmpresa(cobro.getIdEmpresa());
        param.setCodigo("LOGO_ESTABLECIMIENTO_RECIBO");
        param.setCodigoTipoParametroAdicional("COBRO");
        param.setActivo('S');
        ParametroAdicional parametroAdicional = facades.getParametroAdicionalFacade().findFirst(param);
        rutaLogo = Units.execute(rutaLogo, () -> new JsonParser()
                .parse(parametroAdicional.getValorDefecto())
                .getAsJsonObject()
                .get(cobro.getNroRecibo().split("-")[0])
                .getAsString());
        
        InputStream logo = getResource(rutaLogo, closer);
        CobroParam cparam = new CobroParam();
        cparam.setLocale(new Locale("es", "PY"));
        cparam.setId(cobro.getId());
        cparam.setIdEmpresa(cobro.getIdEmpresa());
        cparam.setLogo(logo);
        cparam.setLugarCobro(cobro.getLugarCobro());
        cparam.setDigital(cobro.getDigital());
        cparam.setEstado(cobro.getEstado());
        cparam.setCodigoEstado(cobro.getCodigoEstado());
        cparam.setIdTransaccionRedPago(cobro.getIdTransaccionRedPago());
        cparam.setRedPago(cobro.getRedPago());
        cparam.setMotivoAnulacion(cobro.getMotivoAnulacion());
        cparam.setObservacionAnulacion(cobro.getObservacionAnulacion());
        cparam.setRazonSocialEmisor(razonSocialEmisor);
        cparam.setRucEmisor(rucEmisor);
        cparam.setDescripcionNegocioEmisor(descripcionNegocioEmisor);
        cparam.setDireccionEmisor(direccionEmisor);
        cparam.setTelefonoEmisor(telefonoEmisor);
        cparam.setEmailEmisor(emailEmisor);
        cparam.setNroRecibo(cobro.getNroRecibo());
        cparam.setMoneda(cobro.getMoneda());
        cparam.setCodigoMoneda(cobro.getCodigoMoneda());
        cparam.setFechaEmision(cobro.getFecha());
        cparam.setNroDocumentoReceptor(cobro.getNroDocumento());
        cparam.setRazonSocialReceptor(cobro.getRazonSocial());
        cparam.setMontoCobro(cobro.getMontoCobro());

        String montoTotalLetras = "--";
        try {
            NumberToLetterConverter.Moneda moneda = NumberToLetterConverter.Moneda.valueOf(cobro.getCodigoMoneda());
            montoTotalLetras = NumberToLetterConverter.convertNumberToLetter(cobro.getMontoCobro(), moneda) + ".---";
        } catch (Exception e) {
            e.printStackTrace();
        }
        cparam.setMontoTotalLetras(montoTotalLetras);

        CobroDetalleParam cdeparam = new CobroDetalleParam();
        cdeparam.setIdCobro(cobro.getId());
        cdeparam.setFirstResult(0);
        cdeparam.setPageSize(1000);
        List<CobroDetallePojo> cdelist = facades.getCobroDetalleFacade().findPojo(cdeparam);

        List<py.com.sepsa.erp.reporte.pojos.cobro.CobroDetalle> list = new ArrayList<>();
        List<CobroDetalleFactura> facturas = new ArrayList<>();
        List<CobroDetallePago> pagos = new ArrayList<>();

        for (CobroDetallePojo item : cdelist) {
            py.com.sepsa.erp.reporte.pojos.cobro.CobroDetalle cdparam
                    = new py.com.sepsa.erp.reporte.pojos.cobro.CobroDetalle();
            cdparam.setId(item.getId());
            cdparam.setNroLinea(item.getNroLinea());
            cdparam.setIdFactura(item.getIdFactura());
            cdparam.setNroFactura(item.getNroFactura());
            cdparam.setFechaFactura(item.getFechaFactura());
            cdparam.setTipoCobro(item.getTipoCobro());
            cdparam.setCodigoTipoCobro(item.getCodigoTipoCobro());
            cdparam.setConceptoCobro(item.getConceptoCobro());
            cdparam.setDescripcion(item.getDescripcion());
            cdparam.setMontoCobro(item.getMontoCobro());
            cdparam.setMontoCheque(Units.execute(BigDecimal.ZERO, () -> item.getMontoCheque().stripTrailingZeros()));
            cdparam.setEntidadFinanciera(Units.execute(() -> item.getEntidadFinanciera()));
            cdparam.setNroCheque(item.getNroCheque());
            cdparam.setFechaEmisionCheque(item.getFechaEmisionCheque());

            list.add(cdparam);

            CobroDetalleFactura cdfparam
                    = new CobroDetalleFactura();
            cdfparam.setIdFactura(item.getIdFactura());
            cdfparam.setNroFactura(item.getNroFactura());
            cdfparam.setFechaFactura(item.getFechaFactura());

            if (!facturas.contains(cdfparam)) {
                facturas.add(cdfparam);
            }

            CobroDetallePago cdpparam = new CobroDetallePago();
            cdpparam.setTipoCobro(item.getTipoCobro());
            cdpparam.setCodigoTipoCobro(item.getCodigoTipoCobro());
            cdpparam.setMontoCobro(item.getMontoCobro());
            cdpparam.setEntidadFinanciera(Units.execute(() -> item.getEntidadFinanciera()));
            cdpparam.setNroCheque(item.getNroCheque());
            cdpparam.setFechaEmisionCheque(item.getFechaEmisionCheque());

            if (!pagos.contains(cdpparam)) {
                pagos.add(cdpparam);
            } else {
                CobroDetallePago temp = pagos.get(pagos.indexOf(cdpparam));
                temp.setMontoCobro(temp.getMontoCobro().add(cdpparam.getMontoCobro()));
            }
        }

        cparam.setDetalles(list);
        cparam.setFacturas(facturas);
        cparam.setPagos(pagos);

        return cparam;
    }

    public ReporteEmpresaJParam getParam(ReporteEmpresaParam param) {
        return new ReporteEmpresaJParam(param.getParametros());
    }

    /**
     * Obtiene los parametros de la nota de crédito para la impresion en PDF
     *
     * @param closer closer
     * @param notaCredito Nota de crédito
     * @return Mapa de parametros
     */
    public NotaCreditoAutoimpresorParam getParam(Closer closer, NotaCredito notaCredito) {

        NotaCreditoAutoimpresorParam ncparam = new NotaCreditoAutoimpresorParam();
        ncparam.setLocale(new Locale("es", "PY"));
        ncparam.setIdEmpresa(notaCredito.getIdEmpresa());
        ncparam.setTimbrado(notaCredito.getTalonario().getTimbrado());
        ncparam.setDireccionLocalTalonario(Units.execute(() -> notaCredito.getTalonario().getLocal().getDireccion().getDireccion()));
        ncparam.setLocalTalonario(Units.execute(() -> notaCredito.getTalonario().getLocal().getDescripcion()));
        ncparam.setIdExternoLocalTalonario(Units.execute(() -> notaCredito.getTalonario().getLocal().getIdExterno()));
        ncparam.setFechaInicioVigencia(notaCredito.getTalonario().getFechaInicio());
        ncparam.setFechaFinVigencia(notaCredito.getTalonario().getFechaVencimiento());
        ncparam.setFechaResolucion(notaCredito.getTalonario().getFechaResolucion());
        ncparam.setNroResolucion(notaCredito.getTalonario().getNroResolucion());

        String rutaLogo = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "LOGO");
        String descripcionNegocioEmisor = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "DESCRIPCION_NEGOCIO");
        String rucEmisor = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "RUC");
        String razonSocialEmisor = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "RAZON_SOCIAL");
        String direccionEmisor = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "DIRECCION");
        String telefonoEmisor = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "TELEFONO");
        String emailEmisor = facades.getConfiguracionValorFacade().getCVValor(notaCredito.getIdEmpresa(), "EMAIL");

        InputStream logo = getResource(rutaLogo, closer);
        ncparam.setLogo(logo);
        ncparam.setDescripcionNegocioEmisor(descripcionNegocioEmisor);
        ncparam.setRucEmisor(rucEmisor);
        ncparam.setRazonSocialEmisor(razonSocialEmisor);
        ncparam.setDireccionEmisor(direccionEmisor);
        ncparam.setTelefonoReceptor(telefonoEmisor);
        ncparam.setEmailReceptor(emailEmisor);
        ncparam.setFechaEmision(notaCredito.getFecha());
        ncparam.setNroNotaCredito(notaCredito.getNroNotaCredito());
        ncparam.setCondicionVenta(NotaCreditoAutoimpresorParam.CondicionVenta.CONTADO);
        ncparam.setNroDocumentoReceptor(notaCredito.getRuc());
        ncparam.setRazonSocialReceptor(notaCredito.getRazonSocial());
        ncparam.setDireccionReceptor(notaCredito.getDireccion());
        ncparam.setNroCasaReceptor(notaCredito.getNroCasa());
        ncparam.setTelefonoReceptor(notaCredito.getTelefono());
        ncparam.setEmailReceptor(notaCredito.getEmail());
        ncparam.setObservacion(notaCredito.getObservacion());
        ncparam.setMontoIva5(notaCredito.getMontoIva5());
        ncparam.setMontoImponible5(notaCredito.getMontoImponible5());
        ncparam.setMontoTotal5(notaCredito.getMontoTotal5());
        ncparam.setMontoIva10(notaCredito.getMontoIva10());
        ncparam.setMontoImponible10(notaCredito.getMontoImponible10());
        ncparam.setMontoTotal10(notaCredito.getMontoTotal10());
        ncparam.setMontoTotalExento(notaCredito.getMontoTotalExento());
        ncparam.setMontoIvaTotal(notaCredito.getMontoIvaTotal());
        ncparam.setMontoImponibleTotal(notaCredito.getMontoImponibleTotal());
        ncparam.setMontoTotalNotaCredito(notaCredito.getMontoTotalNotaCredito());
        ncparam.setMontoTotalGuaranies(notaCredito.getMontoTotalGuaranies());

        String montoTotalLetras = "--";
        try {
            NumberToLetterConverter.Moneda moneda = NumberToLetterConverter.Moneda.valueOf(notaCredito.getMoneda().getCodigo());
            montoTotalLetras = NumberToLetterConverter.convertNumberToLetter(notaCredito.getMontoTotalNotaCredito(), moneda) + ".---";
        } catch (Exception e) {
            e.printStackTrace();
        }
        ncparam.setMontoTotalLetras(montoTotalLetras);

        List<NotaCreditoAutoimpresorDetalle> list = new ArrayList<>();

        for (NotaCreditoDetalle item : notaCredito.getNotaCreditoDetalles()) {
            NotaCreditoAutoimpresorDetalle ncdparam = new NotaCreditoAutoimpresorDetalle();
            ncdparam.setDescripcion(item.getDescripcion());
            ncdparam.setCodigoGtin(Units.execute(() -> item.getProducto().getCodigoGtin()));
            ncdparam.setCodigoInterno(Units.execute(() -> item.getProducto().getCodigoInterno()));
            ncdparam.setCantidad(item.getCantidad().intValue());
            ncdparam.setPorcentajeIva(item.getPorcentajeIva());
            ncdparam.setPrecioUnitarioConIva(item.getPrecioUnitarioConIva());
            ncdparam.setPrecioUnitarioSinIva(item.getPrecioUnitarioSinIva());
            ncdparam.setMontoTotal(item.getMontoTotal());

            switch (item.getPorcentajeIva()) {
                case 0:
                    ncdparam.setMontoExento(item.getMontoTotal());
                    break;

                case 5:
                    ncdparam.setMontoTotal5(item.getMontoTotal());
                    break;

                case 10:
                    ncdparam.setMontoTotal10(item.getMontoTotal());
                    break;
            }

            list.add(ncdparam);
        }

        ncparam.setDetalles(list);

        return ncparam;
    }
    /**
     * Obtiene los parametros el reporte de venta
     *
     * @param closer closer
     * @param idEmpresa Identificador de empresa
     * @param fechaDesde Fecha hasta
     * @param fechaHasta Fecha desde
     * @param ncs Lista de ncs
     * @return Mapa de parametros
     */
    public ReporteVentaNotaDebitoParam getReporteVentaNotaDebitoParam(Closer closer, Integer idEmpresa, Date fechaDesde, Date fechaHasta, List<NotaDebitoPojo> ncs) {

        List<NotaDebitoAutoimpresorParam> list = new ArrayList<>();

        BigDecimal totalGravada10 = BigDecimal.ZERO;
        BigDecimal totalIva10 = BigDecimal.ZERO;
        BigDecimal totalGravada5 = BigDecimal.ZERO;
        BigDecimal totalIva5 = BigDecimal.ZERO;
        BigDecimal totalExento = BigDecimal.ZERO;
        BigDecimal total = BigDecimal.ZERO;

        for (NotaDebitoPojo nc : ncs) {
            NotaDebitoAutoimpresorParam ndparam = new NotaDebitoAutoimpresorParam();
            ndparam.setTimbrado(nc.getTimbrado());
            ndparam.setLocalTalonario(nc.getDescripcionLocalTalonario());

            ndparam.setMoneda(nc.getMoneda());
            ndparam.setCodigoMoneda(nc.getCodigoMoneda());
            ndparam.setAnulado(nc.getAnulado().toString());
            ndparam.setNroFacturas(nc.getNroFacturas());
            ndparam.setFechaEmision(nc.getFecha());
            ndparam.setNroNotaDebito(nc.getNroNotaDebito());
            ndparam.setNroDocumentoReceptor(nc.getRuc());
            ndparam.setRazonSocialReceptor(nc.getRazonSocial());
            ndparam.setDireccionReceptor(nc.getDireccion());
            ndparam.setNroCasaReceptor(nc.getNroCasa());
            ndparam.setTelefonoReceptor(nc.getTelefono());
            ndparam.setObservacion(nc.getObservacion());
            ndparam.setMontoIva5(nc.getMontoIva5());
            ndparam.setMontoImponible5(nc.getMontoImponible5());
            ndparam.setMontoTotal5(nc.getMontoTotal5());
            ndparam.setMontoIva10(nc.getMontoIva10());
            ndparam.setMontoImponible10(nc.getMontoImponible10());
            ndparam.setMontoTotal10(nc.getMontoTotal10());
            ndparam.setMontoTotalExento(nc.getMontoTotalExento());
            ndparam.setMontoIvaTotal(nc.getMontoIvaTotal());
            ndparam.setMontoImponibleTotal(nc.getMontoImponibleTotal());
            ndparam.setMontoTotalNotaDebito(nc.getMontoTotalNotaDebito());
            ndparam.setMontoTotalGuaranies(nc.getMontoTotalNotaDebito());

            totalGravada10 = totalGravada10.add(nc.getMontoImponible10());
            totalIva10 = totalIva10.add(nc.getMontoIva10());
            totalGravada5 = totalGravada5.add(nc.getMontoImponible5());
            totalIva5 = totalIva5.add(nc.getMontoIva5());
            totalExento = totalExento.add(nc.getMontoTotalExento());
            total = total.add(nc.getMontoTotalNotaDebito());

            list.add(ndparam);
        }

        ReporteVentaNotaDebitoParam result = new ReporteVentaNotaDebitoParam();
        result.setIdEmpresa(idEmpresa);
        result.setLocale(new Locale("es", "PY"));
        result.setFechaDesde(fechaDesde);
        result.setFechaHasta(fechaHasta);
        result.setTotalGravada10(totalGravada10);
        result.setTotalIva10(totalIva10);
        result.setTotalGravada5(totalGravada5);
        result.setTotalIva5(totalIva5);
        result.setTotalExento(totalExento);
        result.setTotal(total);
        result.setDetalles(list);

        return result;
    }
    
    
     /**
     * Obtiene los parametros de la nota de débito para la impresion en PDF
     *
     * @param closer closer
     * @param notaDebito Nota de débito
     * @return Mapa de parametros
     */
    public NotaDebitoAutoimpresorParam getParam(Closer closer, NotaDebito notaDebito) {

        NotaDebitoAutoimpresorParam ncparam = new NotaDebitoAutoimpresorParam();
        ncparam.setLocale(new Locale("es", "PY"));
        ncparam.setIdEmpresa(notaDebito.getIdEmpresa());
        ncparam.setTimbrado(notaDebito.getTalonario().getTimbrado());
        ncparam.setDireccionLocalTalonario(Units.execute(() -> notaDebito.getTalonario().getLocal().getDireccion().getDireccion()));
        ncparam.setLocalTalonario(Units.execute(() -> notaDebito.getTalonario().getLocal().getDescripcion()));
        ncparam.setIdExternoLocalTalonario(Units.execute(() -> notaDebito.getTalonario().getLocal().getIdExterno()));
        ncparam.setFechaInicioVigencia(notaDebito.getTalonario().getFechaInicio());
        ncparam.setFechaFinVigencia(notaDebito.getTalonario().getFechaVencimiento());
        ncparam.setFechaResolucion(notaDebito.getTalonario().getFechaResolucion());
        ncparam.setNroResolucion(notaDebito.getTalonario().getNroResolucion());

        String rutaLogo = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "LOGO");
        String descripcionNegocioEmisor = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "DESCRIPCION_NEGOCIO");
        String rucEmisor = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "RUC");
        String razonSocialEmisor = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "RAZON_SOCIAL");
        String direccionEmisor = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "DIRECCION");
        String telefonoEmisor = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "TELEFONO");
        String emailEmisor = facades.getConfiguracionValorFacade().getCVValor(notaDebito.getIdEmpresa(), "EMAIL");

        InputStream logo = getResource(rutaLogo, closer);
        ncparam.setLogo(logo);
        ncparam.setDescripcionNegocioEmisor(descripcionNegocioEmisor);
        ncparam.setRucEmisor(rucEmisor);
        ncparam.setRazonSocialEmisor(razonSocialEmisor);
        ncparam.setDireccionEmisor(direccionEmisor);
        ncparam.setTelefonoReceptor(telefonoEmisor);
        ncparam.setEmailReceptor(emailEmisor);
        ncparam.setFechaEmision(notaDebito.getFecha());
        ncparam.setNroNotaDebito(notaDebito.getNroNotaDebito());
        ncparam.setCondicionVenta(NotaDebitoAutoimpresorParam.CondicionVenta.CONTADO);
        ncparam.setNroDocumentoReceptor(notaDebito.getRuc());
        ncparam.setRazonSocialReceptor(notaDebito.getRazonSocial());
        ncparam.setDireccionReceptor(notaDebito.getDireccion());
        ncparam.setNroCasaReceptor(notaDebito.getNroCasa());
        ncparam.setTelefonoReceptor(notaDebito.getTelefono());
        ncparam.setEmailReceptor(notaDebito.getEmail());
        ncparam.setObservacion(notaDebito.getObservacion());
        ncparam.setMontoIva5(notaDebito.getMontoIva5());
        ncparam.setMontoImponible5(notaDebito.getMontoImponible5());
        ncparam.setMontoTotal5(notaDebito.getMontoTotal5());
        ncparam.setMontoIva10(notaDebito.getMontoIva10());
        ncparam.setMontoImponible10(notaDebito.getMontoImponible10());
        ncparam.setMontoTotal10(notaDebito.getMontoTotal10());
        ncparam.setMontoTotalExento(notaDebito.getMontoTotalExento());
        ncparam.setMontoIvaTotal(notaDebito.getMontoIvaTotal());
        ncparam.setMontoImponibleTotal(notaDebito.getMontoImponibleTotal());
        ncparam.setMontoTotalNotaDebito(notaDebito.getMontoTotalNotaDebito());
        ncparam.setMontoTotalGuaranies(notaDebito.getMontoTotalGuaranies());

        String montoTotalLetras = "--";
        try {
            NumberToLetterConverter.Moneda moneda = NumberToLetterConverter.Moneda.valueOf(notaDebito.getMoneda().getCodigo());
            montoTotalLetras = NumberToLetterConverter.convertNumberToLetter(notaDebito.getMontoTotalNotaDebito(), moneda) + ".---";
        } catch (Exception e) {
            e.printStackTrace();
        }
        ncparam.setMontoTotalLetras(montoTotalLetras);

        List<NotaDebitoAutoimpresorDetalle> list = new ArrayList<>();

        for (NotaDebitoDetalle item : notaDebito.getNotaDebitoDetalles()) {
            NotaDebitoAutoimpresorDetalle ndparam = new NotaDebitoAutoimpresorDetalle();
            ndparam.setDescripcion(item.getDescripcion());
            ndparam.setCodigoGtin(Units.execute(() -> item.getProducto().getCodigoGtin()));
            ndparam.setCodigoInterno(Units.execute(() -> item.getProducto().getCodigoInterno()));
            ndparam.setCantidad(item.getCantidad().intValue());
            ndparam.setPorcentajeIva(item.getPorcentajeIva());
            ndparam.setPrecioUnitarioConIva(item.getPrecioUnitarioConIva());
            ndparam.setPrecioUnitarioSinIva(item.getPrecioUnitarioSinIva());
            ndparam.setMontoTotal(item.getMontoTotal());

            switch (item.getPorcentajeIva()) {
                case 0:
                    ndparam.setMontoExento(item.getMontoTotal());
                    break;

                case 5:
                    ndparam.setMontoTotal5(item.getMontoTotal());
                    break;

                case 10:
                    ndparam.setMontoTotal10(item.getMontoTotal());
                    break;
            }

            list.add(ndparam);
        }

        ncparam.setDetalles(list);

        return ncparam;
    }

    public RegistroComprobantePojo generarReporteVenta(List<RegistroComprobantePojo> lista) throws IOException {
        return generarReporte("reporte_venta", "V0000", lista);
    }

    public RegistroComprobantePojo generarReporteCompra(List<RegistroComprobantePojo> lista) throws IOException {
        return generarReporte("reporte_compra", "C0000", lista);
    }

    private RegistroComprobantePojo generarReporte(String nombreZip, String formatoNombre, List<RegistroComprobantePojo> lista) throws IOException {

        File tempFile = File.createTempFile(UUID.randomUUID().toString(), null);
        ZipUtils zip = new ZipUtils(tempFile.getAbsolutePath());

        for (int i = 0; i < lista.size(); i++) {
            String index = String.format("%s%d.zip", formatoNombre, i);
            index = index.substring(index.length() - 9);
            String fileName = lista.get(i).getNombreArchivo();
            fileName = fileName.replaceFirst(String.format("%s.zip", formatoNombre), index);
            zip.addOrReplaceEntry(fileName,
                    new ByteArrayInputStream(lista.get(i).getBytes()));
        }

        byte[] bytes = Files.readAllBytes(tempFile.toPath());

        RegistroComprobantePojo result = new RegistroComprobantePojo();
        result.setBytes(bytes);
        result.setNombreArchivo(String.format("%s.zip", nombreZip));
        result.setTipoContenido(Files.probeContentType(tempFile.toPath()));

        tempFile.delete();

        return result;
    }

    public RegistroComprobantePojo generarReporteVenta(int indice,
            List<RegistroComprobantePojo> lista,
            ReporteComprobanteParam param) throws IOException {

        String imputaIva = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_VENTA_IMPUTA_IVA");
        String imputaIre = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_VENTA_IMPUTA_IRE");
        String imputaIrpRsp = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_VENTA_IMPUTA_IRP_RSP");
        String ruc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");

        String content = "";
        for (RegistroComprobantePojo item : lista) {
            content = String.format("%s%s\r\n", content, item
                    .generarComprobanteVenta(imputaIva, imputaIre, imputaIrpRsp));
        }

        SimpleDateFormat sdf = new SimpleDateFormat(param.getAnual() ? "yyyy" : "MMyyyy");

        String fileName = String.format("%s_REG_%s_V0000",
                ruc, sdf.format(param.getFecha()));

        String index = String.format("V0000%d", indice);
        index = index.substring(index.length() - 5);
        fileName = fileName.replaceFirst("V0000", index);

        return generarZip(fileName, content);
    }

    public RegistroComprobantePojo generarReporteCompra(int indice,
            List<RegistroComprobantePojo> lista,
            ReporteComprobanteParam param) throws IOException {

        String imputaIva = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_COMPRA_IMPUTA_IVA");
        String imputaIre = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_COMPRA_IMPUTA_IRE");
        String imputaIrpRsp = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_COMPRA_IMPUTA_IRP_RSP");
        String noImputa = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "COMPROBANTE_COMPRA_NO_IMPUTA");
        String ruc = facades.getConfiguracionValorFacade().getCVValor(param.getIdEmpresa(), "RUC");

        String content = "";
        for (RegistroComprobantePojo item : lista) {
            content = String.format("%s%s\r\n", content, item
                    .generarComprobanteCompra(imputaIva, imputaIre, imputaIrpRsp, noImputa));
        }

        SimpleDateFormat sdf = new SimpleDateFormat(param.getAnual() ? "yyyy" : "MMyyyy");

        String fileName = String.format("%s_REG_%s_C0000",
                ruc, sdf.format(param.getFecha()));

        String index = String.format("C0000%d", indice);
        index = index.substring(index.length() - 5);
        fileName = fileName.replaceFirst("C0000", index);

        return generarZip(fileName, content);
    }

    private RegistroComprobantePojo generarZip(String fileName, String content) throws IOException {

        File file = File.createTempFile(UUID.randomUUID().toString(), null);

        try ( FileWriter fileWriter = new FileWriter(file);  PrintWriter printWriter = new PrintWriter(fileWriter)) {
            printWriter.print(content);
        }

        File tempFile = File.createTempFile(UUID.randomUUID().toString(), null);
        ZipUtils zip = new ZipUtils(tempFile.getAbsolutePath());
        zip.addOrReplaceEntry(String.format("%s.csv", fileName),
                new ByteArrayInputStream(Files.readAllBytes(file.toPath())));

        byte[] bytes = Files.readAllBytes(tempFile.toPath());

        RegistroComprobantePojo result = new RegistroComprobantePojo();
        result.setBytes(bytes);
        result.setNombreArchivo(String.format("%s.zip", fileName));
        result.setTipoContenido(Files.probeContentType(tempFile.toPath()));

        file.delete();
        tempFile.delete();

        return result;
    }

    public void merge(List<byte[]> listBytes, OutputStream os, Rectangle pageSize) {
        Document document = new Document();

        if (pageSize != null) {
            document.setPageSize(pageSize);
        }

        try {
            List<PdfReader> readers = new ArrayList();
            for (int i = 0; i < listBytes.size(); i++) {
                PdfReader pdfReader = new PdfReader(listBytes.get(i));
                readers.add(pdfReader);
                if (pageSize == null) {
                    document.setPageSize(pdfReader.getPageSize(1));
                }
            }
            PdfWriter writer = PdfWriter.getInstance(document, os);
            document.open();
            PdfContentByte cb = writer.getDirectContent();
            PdfImportedPage page;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();

            float pageHeight = document.getPageSize().getHeight();
            float heightSeg = pageHeight;

            while (iteratorPDFReader.hasNext()) {
                PdfReader pdfReader = iteratorPDFReader.next();
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {

                    pageOfCurrentReaderPDF++;
                    page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);

                    float newPageHeight = page.getHeight();

                    boolean addNewPage = (heightSeg - newPageHeight) <= 0;

                    if (addNewPage) {
                        document.newPage();
                        heightSeg = pageHeight;
                    }

                    heightSeg -= newPageHeight;

                    cb.addTemplate(page, 0, heightSeg);
                }
                pageOfCurrentReaderPDF = 0;
            }
            os.flush();
        } catch (DocumentException | IOException ex) {
            log.fatal("Error", ex);
        } finally {
            if (document.isOpen()) {
                document.close();
            }
        }
    }

    public class ReporteEmpresaJParam implements ReporteParams {

        public ReporteEmpresaJParam(JsonElement parametros) {
            this.parametros = parametros;
        }

        private final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        private final JsonElement parametros;

        @Override
        public Map toMap() {
            Map<String, Object> result = new HashMap<>();

            if (!parametros.isJsonArray()) {
                return result;
            }

            for (JsonElement element : parametros.getAsJsonArray()) {
                JsonObject object = element.getAsJsonObject();

                String tipo = object.get("tipo").getAsString();
                String codigo = object.get("codigo").getAsString();
                JsonElement valor = object.get("valor");

                if (valor.isJsonNull()) {
                    //No hacer nada
                } else if (tipo.equals("STRING")) {
                    result.put(codigo, valor.getAsJsonPrimitive().getAsString());
                } else if (tipo.equals("INTEGER")) {
                    result.put(codigo, valor.getAsJsonPrimitive().getAsNumber().intValue());
                } else if (tipo.equals("BOOLEAN")) {
                    result.put(codigo, valor.getAsJsonPrimitive().getAsBoolean());
                } else if (tipo.equals("FECHA")) {
                    try {
                        result.put(codigo, sdf1.parse(valor.getAsJsonPrimitive().getAsString()));
                    } catch (Exception e) {
                        log.fatal("Error", e);
                    }
                } else if (valor.isJsonPrimitive()) {
                    if (valor.getAsJsonPrimitive().isBoolean()) {
                        result.put(codigo, valor.getAsJsonPrimitive().getAsBoolean());
                    } else if (valor.getAsJsonPrimitive().isNumber()) {
                        Number number = valor.getAsJsonPrimitive().getAsNumber();
                        if (number instanceof Long) {
                            result.put(codigo, number.longValue());
                        } else if (number instanceof Double) {
                            result.put(codigo, number.doubleValue());
                        } else if (number instanceof Integer) {
                            result.put(codigo, number.intValue());
                        } else if (number instanceof Short) {
                            result.put(codigo, number.shortValue());
                        } else if (number instanceof Float) {
                            result.put(codigo, number.floatValue());
                        } else if (number instanceof BigInteger) {
                            result.put(codigo, number);
                        } else if (number instanceof BigDecimal) {
                            result.put(codigo, number);
                        } else if (number instanceof LazilyParsedNumber) {
                            try {
                                Integer.parseInt(valor.getAsJsonPrimitive().getAsString());
                                result.put(codigo, valor.getAsJsonPrimitive().getAsBigInteger().longValue());
                            } catch (Exception e) {
                                result.put(codigo, valor.getAsJsonPrimitive().getAsBigDecimal().doubleValue());
                            }
                        }
                    } else if (valor.getAsJsonPrimitive().isString()) {
                        result.put(codigo, valor.getAsJsonPrimitive().getAsString());
                    } else {
                        result.put(codigo, valor.getAsJsonPrimitive().getAsString());
                    }

                } else if (valor.isJsonArray()) {

                    JsonArray data = new JsonArray();

                    for (JsonElement item : valor.getAsJsonArray()) {

                        if (item.isJsonObject()) {
                            data.add(item);
                        }

                    }

                    result.put(codigo, SepsaJRDataSource.getDataSource(data));
                }
            }

            return result;
        }

        @Override
        public Collection getDataSourceCollection() {
            return Lists.empty();
        }

    }

    /**
     * Estado del documento
     */
    public enum ReportFormat {
        PDF("PDF", "PDF"),
        XLS("XLS", "XLS"),
        TXT("TXT", "TXT");

        /**
         * Etiqueta del tipo enum
         */
        private String label;

        /**
         * Valor del tipo enum
         */
        private String value;

        /**
         * Obtiene la etiqueta del tipo enum
         *
         * @return Etiqueta del tipo enum
         */
        public String getLabel() {
            return label;
        }

        /**
         * Setea la etiqueta del tipo enum
         *
         * @param label Etiqueta del tipo enum
         */
        public void setLabel(String label) {
            this.label = label;
        }

        /**
         * Obtiene el valor del tipo enum
         *
         * @return Valor del tipo enum
         */
        public String getValue() {
            return value;
        }

        /**
         * Setea el valor del tipo enum
         *
         * @param value Valor del tipo enum
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Constructor de ReportFormat
         *
         * @param label Etiqueta
         * @param value Valor
         */
        private ReportFormat(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}
