/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.set;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.set.Procesamiento;
import py.com.sepsa.erp.ejb.entities.set.filters.DetalleProcesamientoParam;
import py.com.sepsa.erp.ejb.entities.set.filters.ProcesamientoParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "ProcesamientoFacade", mappedName = "ProcesamientoFacade")
@Local(ProcesamientoFacade.class)
public class ProcesamientoFacadeImpl extends FacadeImpl<Procesamiento, ProcesamientoParam> implements ProcesamientoFacade {

    public ProcesamientoFacadeImpl() {
        super(Procesamiento.class);
    }

    @Override
    public Boolean validToCreate(ProcesamientoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        if(!isNullOrEmpty(param.getDetalleProcesamientos())) {
            for (DetalleProcesamientoParam item : param.getDetalleProcesamientos()) {
                facades.getDetalleProcesamientoFacade().validToCreate(item, false);
            }
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(ProcesamientoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        Procesamiento dp = find(param.getId());
        
        if(isNull(dp)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de procesamiento"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public Procesamiento create(ProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Procesamiento item = new Procesamiento();
        item.setEstadoResultado(param.getEstadoResultado());
        item.setFechaInsercion(Calendar.getInstance().getTime());
        item.setFechaProcesamiento(param.getFechaProcesamiento());
        item.setIdEnvio(param.getIdEnvio());
        item.setNroTransaccion(param.getNroTransaccion());
        
        create(item);
        
        if(!isNullOrEmpty(param.getDetalleProcesamientos())) {
            for (DetalleProcesamientoParam dp : param.getDetalleProcesamientos()) {
                dp.setIdProcesamiento(item.getId());
                facades.getDetalleProcesamientoFacade().create(dp, userInfo);
            }
        }
        
        return item;
    }

    @Override
    public Procesamiento edit(ProcesamientoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Procesamiento item = find(param.getId());
        item.setEstadoResultado(param.getEstadoResultado());
        item.setFechaProcesamiento(param.getFechaProcesamiento());
        item.setIdEnvio(param.getIdEnvio());
        item.setNroTransaccion(param.getNroTransaccion());
        
        edit(item);
        
        return item;
    }
    
    @Override
    public List<Procesamiento> find(ProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery(Procesamiento.class);
        Root<Procesamiento> root = cq.from(Procesamiento.class);
        
        cq.select(root);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEnvio() != null) {
            predList.add(qb.equal(root.get("idEnvio"), param.getIdEnvio()));
        }
        
        if(param.getNroTransaccion() != null) {
            predList.add(qb.equal(root.get("nroTransaccion"), param.getNroTransaccion()));
        }
        
        if(param.getEstadoResultado() != null) {
            predList.add(qb.equal(root.get("estadoResultado"), param.getEstadoResultado()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(param.getFechaProcesamientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaProcesamiento"), param.getFechaProcesamientoDesde()));
        }
        
        if(param.getFechaProcesamientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaProcesamiento"), param.getFechaProcesamientoHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("fechaProcesamiento")));
        
        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Procesamiento> result = q.getResultList();
        
        return result;
    }
    
    @Override
    public Long findSize(ProcesamientoParam param) {
            
        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<Procesamiento> root = cq.from(Procesamiento.class);
        
        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();
        
        if(param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if(param.getIdEnvio() != null) {
            predList.add(qb.equal(root.get("idEnvio"), param.getIdEnvio()));
        }
        
        if(param.getNroTransaccion() != null) {
            predList.add(qb.equal(root.get("nroTransaccion"), param.getNroTransaccion()));
        }
        
        if(param.getEstadoResultado() != null) {
            predList.add(qb.equal(root.get("estadoResultado"), param.getEstadoResultado()));
        }
        
        if(param.getFechaInsercionDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionDesde()));
        }
        
        if(param.getFechaInsercionHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaInsercion"), param.getFechaInsercionHasta()));
        }
        
        if(param.getFechaProcesamientoDesde() != null) {
            predList.add(qb.greaterThanOrEqualTo(root.<Date>get("fechaProcesamiento"), param.getFechaProcesamientoDesde()));
        }
        
        if(param.getFechaProcesamientoHasta() != null) {
            predList.add(qb.lessThanOrEqualTo(root.<Date>get("fechaProcesamiento"), param.getFechaProcesamientoHasta()));
        }
        
        if(!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if(pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();
        
        Long result = object == null
                ? 0L
                : ((Number)object).longValue();
        
        return result;
    }

}
