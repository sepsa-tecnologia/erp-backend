/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.auditoria.filters;

import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de valor columna
 *
 * @author Jonathan
 */
public class ValorColumnaParam extends CommonParam {

    public ValorColumnaParam() {
    }

    public ValorColumnaParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de auditoria
     */
    @QueryParam("idAuditoria")
    private Integer idAuditoria;

    /**
     * Identificador de auditoria detalle
     */
    @QueryParam("idAuditoriaDetalle")
    private Integer idAuditoriaDetalle;

    /**
     * Nombre columna
     */
    @QueryParam("nombreColumna")
    private String nombreColumna;

    /**
     * Valor anterior
     */
    @QueryParam("valorAnterior")
    private String valorAnterior;

    /**
     * Valor nuevo
     */
    @QueryParam("valorNuevo")
    private String valorNuevo;

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idAuditoriaDetalle)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de auditoria detalle"));
        }

        if (isNullOrEmpty(nombreColumna)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el nombre de la columna"));
        }

        return !tieneErrores();
    }

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdAuditoriaDetalle() {
        return idAuditoriaDetalle;
    }

    public void setIdAuditoriaDetalle(Integer idAuditoriaDetalle) {
        this.idAuditoriaDetalle = idAuditoriaDetalle;
    }

    public String getNombreColumna() {
        return nombreColumna;
    }

    public void setNombreColumna(String nombreColumna) {
        this.nombreColumna = nombreColumna;
    }

    public String getValorAnterior() {
        return valorAnterior;
    }

    public void setValorAnterior(String valorAnterior) {
        this.valorAnterior = valorAnterior;
    }

    public String getValorNuevo() {
        return valorNuevo;
    }

    public void setValorNuevo(String valorNuevo) {
        this.valorNuevo = valorNuevo;
    }

    public void setIdAuditoria(Integer idAuditoria) {
        this.idAuditoria = idAuditoria;
    }

    public Integer getIdAuditoria() {
        return idAuditoria;
    }
}
