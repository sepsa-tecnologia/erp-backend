/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * POJO para el manejo de parámetros de estado
 * @author Jonathan
 */
public class EstadoParam extends CommonParam {

    public EstadoParam() {
    }

    public EstadoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de tipo de estado
     */
    @QueryParam("idTipoEstado")
    protected Integer idTipoEstado;

    /**
     * Codigo de tipo de estado
     */
    @QueryParam("codigoTipoEstado")
    protected String codigoTipoEstado;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idTipoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de estado"));
        }
        
        return !tieneErrores();
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        return new ArrayList<>();
    }

    @Override
    public boolean isValidToEdit() {
        
        limpiarErrores();
        
        if(isNull(id)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de estado"));
        }
        
        if(isNullOrEmpty(descripcion)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la descripción"));
        }
        
        if(isNullOrEmpty(codigo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el código"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si esta activo"));
        }
        
        if(isNull(idTipoEstado)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de tipo de estado"));
        }
        
        return !tieneErrores();
    }

    public Integer getIdTipoEstado() {
        return idTipoEstado;
    }

    public void setIdTipoEstado(Integer idTipoEstado) {
        this.idTipoEstado = idTipoEstado;
    }

    public void setCodigoTipoEstado(String codigoTipoEstado) {
        this.codigoTipoEstado = codigoTipoEstado;
    }

    public String getCodigoTipoEstado() {
        return codigoTipoEstado;
    }
}
