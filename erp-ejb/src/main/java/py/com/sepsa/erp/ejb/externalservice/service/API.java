package py.com.sepsa.erp.ejb.externalservice.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.externalservice.http.Host;
import py.com.sepsa.erp.ejb.externalservice.http.HttpStringResponse;
import py.com.sepsa.erp.ejb.externalservice.http.ResponseCode;

/**
 * API
 *
 * @author Jonathan Dario Bernal Fernández
 */
@Log4j2
public abstract class API {

    //Datos del Software
    protected final static String VERSION = "2.0.2";
    protected final static String SOFTWARE = "SIEDI";

    //Datos para el manejo de conexión
    private static int TRY_QUANT = 0;
    private static int PRIORITY = 0;

    /**
     * Protocolo de conexión
     */
    private Scheme protocol;

    /**
     * Host de petición
     */
    protected Host host;

    /**
     * Path base del API
     */
    private String baseApi;

    /**
     * Token de sesión
     */
    private String token;

    /**
     * ApiToken de sesión
     */
    private String apiToken;

    /**
     * Tiempo máximo de conexión
     */
    private int connTimeOut;

    /**
     * Tiempo máximo de lectura
     */
    private int readTimeOut;

    /**
     * Límite de request http
     */
    private String boundary;

    /**
     * Separador de línea
     */
    private String lineFeed;

    /**
     * Cantidad de intentos de conexión anteriores.
     */
    private Boolean connectionAlert = false;

    /**
     * Obtiene la cantidad de intentos de conexión anteriores
     *
     * @return Cantidad de intentos de conexión anteriores
     */
    public Boolean getConnectionAlert() {
        return connectionAlert;
    }

    /**
     * Setea la cantidad de intentos de conexión anteriores
     *
     * @param connectionAlert Cantidad de intentos de conexión anteriores
     */
    public void setConnectionAlert(Boolean connectionAlert) {
        this.connectionAlert = connectionAlert;
    }

    /**
     * Obtiene el protocolo de conexión
     *
     * @return Protocolo de conexión
     */
    public Scheme getProtocol() {
        return protocol;
    }

    /**
     * Setea el protocolo de conexión
     *
     * @param protocol Protocolo de conexión
     */
    public void setProtocol(Scheme protocol) {
        this.protocol = protocol;
    }

    /**
     * Obtiene el path base del servicio
     *
     * @return Path base del servicio
     */
    public String getBaseApi() {
        return baseApi;
    }

    /**
     * Setea el path base del servicio
     *
     * @param baseApi Path base del servicio
     */
    public void setBaseApi(String baseApi) {
        this.baseApi = baseApi;
    }

    /**
     * Obtiene el token de sesión
     *
     * @return Token de sesión
     */
    public String getToken() {
        return token;
    }

    /**
     * Setea el token de sesión
     *
     * @param token Token de sesión
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Obtiene el ApiToken de sesión
     *
     * @return
     */
    public String getApiToken() {
        return apiToken;
    }

    /**
     * Setea el apiToken de sesión
     *
     * @param apiToken
     */
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    /**
     * Obtiene el tiempo máximo de conexión al servidor
     *
     * @return Tiempo máximo de conexión al servidor
     */
    public int getConnTimeOut() {
        return connTimeOut;
    }

    /**
     * Setea el tiempo máximo de conexión al servidor
     *
     * @param connTimeOut Tiempo máximo de conexión al servidor
     */
    public void setConnTimeOut(int connTimeOut) {
        this.connTimeOut = connTimeOut;
    }

    /**
     * Setea el tiempo máximo de lectura de respuesta
     *
     * @param readTimeOut Tiempo máximo de lectura de respuesta
     */
    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }

    /**
     * Obtiene el tiempo máximo de lectura de respuesta
     *
     * @return Tiempo máximo de lectura de respuesta
     */
    public int getReadTimeOut() {
        return readTimeOut;
    }

    /**
     * Obtiene el límite para el request http
     *
     * @return Límite para el request http
     */
    public String getBoundary() {
        return boundary;
    }

    /**
     * Setea el límite para el request http
     *
     * @param boundary Límite para el request http
     */
    public void setBoundary(String boundary) {
        this.boundary = boundary;
    }

    /**
     * Obtiene el separador de línea
     *
     * @return Separador de línea
     */
    public String getLineFeed() {
        return lineFeed;
    }

    /**
     * Setea el separador de línea
     *
     * @param lineFeed Separador de línea
     */
    public void setLineFeed(String lineFeed) {
        this.lineFeed = lineFeed;
    }

    /**
     * Setea el host de conexion
     *
     * @param host Host de conexion
     */
    public void setHost(Host host) {
        this.host = host;
    }

    /**
     * Obtiene el host de conexion
     *
     * @return Host de conexion
     */
    public Host getHost() {
        return host;
    }

    /**
     * Obtiene la URL de conexión
     *
     * @return
     */
    public String getURL() {
        return String.format("%s/%s/", host.getDireccion(), baseApi);
    }

    /**
     * GET para servicios
     *
     * @param service Servicio web
     * @param contentType Tipo de contenido
     * @param params Lista de parámetros
     * @return Conexión HTTP
     */
    protected HttpURLConnection GET(String service, ContentType contentType,
            Map<String, Object> params) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            if (params != null && !params.isEmpty()) {
                url = String.format("%s%s", url, "?");

                List<String> keyList = new ArrayList(params.keySet());

                for (int i = 0; i < keyList.size(); i++) {

                    String value = "";

                    Object val = params.get(keyList.get(i));

                    if ((val instanceof String) || ((val instanceof Number))) {

                        String format
                                = i == keyList.size() - 1
                                ? "%s%s=%s"
                                : "%s%s=%s&";

                        value = String.valueOf(val);

                        url = String.format(format, url, keyList.get(i), value);

                    } else if (val instanceof Collection) {

                        for (String item : (Collection<String>) val) {
                            value = String.format("%s%s=%s&", value, keyList.get(i), item);
                        }

                        url = String.format("%s%s", url, value);
                    }
                }
            }

            log.info(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setRequestMethod(Method.GET.toString());

            httpConn.setConnectTimeout(connTimeOut);
            httpConn.setReadTimeout(readTimeOut);

            httpConn.setRequestProperty("Content-Type", contentType.getValue());

            if (token != null && !token.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization",
                        token.startsWith("bearer ")
                        || token.startsWith("Bearer ")
                        ? token
                        : String.format("Bearer %s", token));
            }

            if (apiToken != null && !apiToken.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", apiToken);
            }

        } catch (IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }

        return httpConn;
    }

    /**
     * PUT para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @return Conexión HTTP
     */
    protected HttpURLConnection PUT(String service, ContentType contentType) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            log.info(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setRequestMethod(Method.PUT.toString());

            httpConn.setConnectTimeout(connTimeOut);
            httpConn.setReadTimeout(readTimeOut);

            httpConn.setRequestProperty("Content-type", contentType.getValue());

            if (token != null && !token.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization",
                        token.startsWith("bearer ")
                        || token.startsWith("Bearer ")
                        ? token
                        : String.format("Bearer %s", token));
            }
            if (apiToken != null && !apiToken.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", apiToken);
            }

            httpConn.setDoOutput(true);
        } catch (IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }

        return httpConn;
    }

    /**
     * PUT para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @param params parámetros
     * @return Conexión HTTP
     */
    protected HttpURLConnection PUT(String service, ContentType contentType,
            Map<String, String> params) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            if (params != null && !params.isEmpty()) {
                url = String.format("%s%s", url, "?");

                List<String> keyList = new ArrayList(params.keySet());

                for (int i = 0; i < keyList.size(); i++) {
                    String format
                            = i == keyList.size() - 1
                            ? "%s%s=%s"
                            : "%s%s=%s&";

                    url = String.format(format, url, keyList.get(i),
                            String.valueOf(params.get(keyList.get(i))));
                }
            }

            log.info(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setRequestMethod(Method.PUT.toString());

            httpConn.setConnectTimeout(connTimeOut);
            httpConn.setReadTimeout(readTimeOut);

            httpConn.setRequestProperty("Content-type", contentType.getValue());

            if (token != null && !token.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization",
                        token.startsWith("bearer ")
                        || token.startsWith("Bearer ")
                        ? token
                        : String.format("Bearer %s", token));
            }
            if (apiToken != null && !apiToken.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", apiToken);
            }

            httpConn.setDoOutput(true);
        } catch (IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }

        return httpConn;
    }

    /**
     * POST para servicios
     *
     * @param service Servicio WEB
     * @param contentType Tipo de contenido
     * @return Conexión HTTP
     */
    protected HttpURLConnection POST(String service, ContentType contentType) {

        HttpURLConnection httpConn = null;

        try {

            String url = String.format("%s%s", getURL(), service);

            log.info(String.format("URL: %s", url));

            URL restServiceURL = new URL(url);

            httpConn = (HttpURLConnection) restServiceURL.openConnection();

            httpConn.setUseCaches(false);

            httpConn.setRequestMethod(Method.POST.toString());

            httpConn.setConnectTimeout(connTimeOut);
            httpConn.setReadTimeout(readTimeOut);

            String content = contentType.getValue();

            //Se agrega el boundary para las diferentes partes del POST
            if (boundary != null && contentType.equals(ContentType.MULTIPART)) {
                content = String.format("%s; boundary=%s", content, boundary);
            }

            httpConn.setRequestProperty("Content-type", content);

            if (token != null && !token.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization",
                        token.startsWith("bearer ")
                        || token.startsWith("Bearer ")
                        ? token
                        : String.format("Bearer %s", token));
            }
            if (apiToken != null && !apiToken.trim().isEmpty()) {
                httpConn.setRequestProperty("Authorization", apiToken);
            }

            httpConn.setDoOutput(true);
        } catch (MalformedURLException | ProtocolException ex) {
            log.fatal("Se ha producido un error:", ex);
        } catch (IOException ex) {
            log.fatal("Se ha producido un error:", ex);
        }

        return httpConn;
    }

    /**
     * Agrega un body al request
     *
     * @param httpConn Conexión HTTP
     * @param body Parametros del mensaje
     */
    protected void addBodyUrlEncoded(HttpURLConnection httpConn,
            Map<String, String> body) {
        try {
            if (body != null && !body.isEmpty()) {
                String parameters = "";

                List<String> keyList = new ArrayList(body.keySet());

                for (int i = 0; i < keyList.size(); i++) {
                    String format
                            = i == keyList.size() - 1
                            ? "%s%s=%s"
                            : "%s%s=%s&";

                    parameters = String.format(format, parameters, keyList.get(i),
                            String.valueOf(body.get(keyList.get(i))));
                }

                log.info(String.format("Body request: %s",
                        parameters.length() < 1500
                        ? parameters
                        : "Respuesta muy larga para el log"));

                OutputStream os = httpConn.getOutputStream();
                os.write(parameters.getBytes(StandardCharsets.UTF_8));
                os.flush();
            }
        } catch (IOException ex) {
            log.fatal("Se ha producido un error", ex);
        }
    }

    /**
     * Agrega un body al request
     *
     * @param httpConn Conexión HTTP
     * @param body Body del mensaje
     */
    protected void addBody(HttpURLConnection httpConn, String body) {
        try {
            if (body != null && !body.trim().isEmpty()) {
                log.info(String.format("Body request: %s",
                        body.length() < 1500
                        ? body
                        : "Respuesta muy larga para el log"));

                OutputStream os = httpConn.getOutputStream();
                os.write(body.getBytes("UTF-8"));
                os.flush();
            }
        } catch (IOException ex) {
            log.fatal("Se ha producido un error", ex);
        }
    }

    /**
     * Agrega un campo a un formulario
     *
     * @param writer Clase utilizada para escribir en el buss de datos
     * @param name Nombre del atributo
     * @param value Valor del atributo
     */
    protected void addFormField(PrintWriter writer, String name, String value) {

        if (name != null && !name.trim().isEmpty() && value != null
                && !value.trim().isEmpty() && writer != null) {

            String data = String
                    .format("Content-Disposition: form-data; name=\"%s\"",
                            name);

            writer
                    .append(String.format("--%s", boundary))
                    .append(lineFeed)
                    .append(data)
                    .append(lineFeed)
                    .append(lineFeed)
                    .append(value)
                    .append(lineFeed);

            writer.flush();
        }

    }

    /**
     * Resuelve la respuesta HTTP
     *
     * @param httpConn Conexión HTTP
     * @param writer Writer
     * @return Respuesta HTTP
     * @throws java.io.IOException
     */
    protected HttpStringResponse resolve(HttpURLConnection httpConn,
            PrintWriter writer) throws IOException {

        //Respuesta genérica en caso de falla
        HttpStringResponse response = new HttpStringResponse();

        if (writer != null) {
            writer.append(lineFeed)
                    .append(String.format("--%s--", boundary))
                    .append(lineFeed);
            writer.close();
        }

        if (httpConn != null) {

            ResponseCode code;
            log.info(String
                    .format("Código: %s ", httpConn.getResponseCode()));

            boolean error = true;

            InputStream stream;
            switch (httpConn.getResponseCode()) {
                case 200:
                    code = ResponseCode.OK;
                    stream = httpConn.getInputStream();
                    TRY_QUANT = 0;
                    error = false;
                    break;
                case 404:
                    code = ResponseCode.NOT_FOUND;
                    stream = httpConn.getErrorStream();
                    break;
                case 400:
                    code = ResponseCode.BAD_REQUEST;
                    stream = httpConn.getErrorStream();
                    break;
                default:
                    code = ResponseCode.ERROR;
                    stream = httpConn.getErrorStream();
                    break;
            }

            response.setRespCode(code);

            if (stream != null) {
                response = new HttpStringResponse(stream, code,
                        httpConn.getContentType(),
                        httpConn.getHeaderFields());
                log.info(String
                        .format("Respuesta: %s ", response.getPayload()));
            }

            httpConn.disconnect();
        }

        return response;
    }

    /**
     * Constructor de API
     *
     * @param service Servicio
     * @param protocol Protocolo de conexión
     * @param baseApi Path base de los servicios
     * @param token Token de sesión
     * @param connTimeOut Tiempo máximo de conexión
     * @param readTimeOut Tiempo máximo de lectura
     * @param boundary Limite para request http
     * @param lineFeed Separador de línea
     * @param host Host
     * @param apiToken
     */
    public API(Service service, Scheme protocol, String baseApi,
            String token, int connTimeOut, int readTimeOut,
            String boundary, String lineFeed, Host host, String apiToken) {
        this.protocol = protocol;
        this.baseApi = baseApi;
        this.token = token;
        this.connTimeOut = connTimeOut;
        this.readTimeOut = readTimeOut;
        this.boundary = boundary;
        this.lineFeed = lineFeed;
        this.host = host;
        this.apiToken = apiToken;
    }

    /**
     * Métodos para el request
     */
    public enum Method {
        GET, POST, PUT, PATCH
    }

    /**
     * Tipo de contenido
     */
    public enum ContentType {

        JSON("application/json"),
        XML("application/xml"),
        MULTIPART("multipart/form-data"),
        X_WWW_FORM_URLENCODED("application/x-www-form-urlencoded"),
        SOAP("application/soap+xml;charset=UTF-8");

        /**
         * Valor del enum
         */
        private String value;

        /**
         * Obtiene el valor del enum
         *
         * @return Valor del enum
         */
        public String getValue() {
            return value;
        }

        /**
         * Setea el valor del enum
         *
         * @param value Valor del enum
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Constructor de ContentType
         *
         * @param value Valor
         */
        private ContentType(String value) {
            this.value = value;
        }
    }

    /**
     * Enum de los servicios
     */
    public enum Service {
        SEPSA_CORE, EDI_SERVER, SEPSA_SET, NINGUNO;
    }

    /**
     * Esquema de conexión
     */
    public enum Scheme {
        HTTP("http"),
        HTTPS("https");

        /**
         * Esquema
         */
        private String scheme;

        /**
         * Obtiene el esquema
         *
         * @return Esquema
         */
        public String getScheme() {
            return scheme;
        }

        /**
         * Setea el esquema
         *
         * @param scheme Esquema
         */
        public void setScheme(String scheme) {
            this.scheme = scheme;
        }

        /**
         * Constructor de Scheme
         *
         * @param scheme Esquema
         */
        Scheme(String scheme) {
            this.scheme = scheme;
        }
    }

}
