/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.filters;

import java.util.Date;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import static py.com.sepsa.utils.misc.Assertions.isNullOrEmpty;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de factura cuota
 *
 * @author Jonathan D. Bernal Fernández
 */
public class FacturaDncpParam extends CommonParam {

    public FacturaDncpParam() {
    }

    public FacturaDncpParam(String bruto) {
        super(bruto);
    }

    /**
     * Identificador de factura
     */
    @QueryParam("idFactura")
    private Integer idFactura;

    /**
     * Modalidad
     */
    @QueryParam("modalidad")
    private String modalidad;

    /**
     * Entidad
     */
    @QueryParam("entidad")
    private String entidad;

    /**
     * Año
     */
    @QueryParam("anho")
    private String anho;

    /**
     * Secuencia
     */
    @QueryParam("secuencia")
    private String secuencia;

    /**
     * Fecha de emisión
     */
    @QueryParam("fechaEmision")
    private Date fechaEmision;

    /**
     * Loguea el objeto
     *
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    @Override
    public boolean isValidToCreate() {

        limpiarErrores();

        if (isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (isNullOrEmpty(modalidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la modalidad"));
        }

        if (isNullOrEmpty(entidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la entidad"));
        }

        if (isNullOrEmpty(anho)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el año"));
        }

        if (isNullOrEmpty(secuencia)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la secuencia"));
        }

        if (isNull(fechaEmision)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de emisión"));
        }

        return !tieneErrores();
    }

    @Override
    public boolean isValidToEdit() {
        limpiarErrores();

        if (isNull(idFactura)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de factura"));
        }

        if (isNullOrEmpty(modalidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la modalidad"));
        }

        if (isNullOrEmpty(entidad)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la entidad"));
        }

        if (isNullOrEmpty(anho)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el año"));
        }

        if (isNullOrEmpty(secuencia)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la secuencia"));
        }

        if (isNull(fechaEmision)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar la fecha de emisión"));
        }

        return !tieneErrores();
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getAnho() {
        return anho;
    }

    public void setAnho(String anho) {
        this.anho = anho;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
}
