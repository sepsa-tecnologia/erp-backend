/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.info.Configuracion;
import py.com.sepsa.erp.ejb.entities.info.ConfiguracionGeneral;
import py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionGeneralParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ConfiguracionGeneralPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.FirmaDigitalPojo;
import py.com.sepsa.erp.ejb.externalservice.adapters.FirmaDigitalAdapter;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Gustavo Benitez
 */
@Log4j2
@Stateless(name = "ConfiguracionGeneralFacade", mappedName = "ConfiguracionGeneralFacade")
@Local(ConfiguracionGeneralFacade.class)
public class ConfiguracionGeneralFacadeImpl extends FacadeImpl<ConfiguracionGeneral, ConfiguracionGeneralParam> implements ConfiguracionGeneralFacade {

    public ConfiguracionGeneralFacadeImpl() {
        super(ConfiguracionGeneral.class);
    }

    @Override
    public String getCVValor(String codigoConfiguracion) {

        ConfiguracionGeneralParam param = new ConfiguracionGeneralParam();
        param.setCodigoConfiguracion(codigoConfiguracion);

        ConfiguracionGeneralPojo configuracion = findFirstPojo(param);
        return configuracion == null ? null : configuracion.getValor();
    }

    /**
     * Obtiene la lista de Configuraciones General
     *
     * @param param
     * @return
     */
    @Override
    public List<ConfiguracionGeneral> find(ConfiguracionGeneralParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(ConfiguracionGeneral.class);
        Root<ConfiguracionGeneral> root = cq.from(ConfiguracionGeneral.class);
        Join<ConfiguracionValor, Configuracion> configuracion = root.join("configuracion");

        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdConfiguracion() != null) {
            predList.add(qb.equal(root.get("idConfiguracion"), param.getIdConfiguracion()));
        }

        if (param.getValor() != null && !param.getValor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("valor")), String.format("%%%s%%", param.getValor().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getCodigoConfiguracion() != null && !param.getCodigoConfiguracion().trim().isEmpty()) {
            predList.add(qb.equal(configuracion.get("codigo"), param.getCodigoConfiguracion().trim()));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.asc(root.get("activo")), qb.asc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<ConfiguracionGeneral> result = q.getResultList();

        return result;
    }

    /**
     * Obtiene el tamaño de la lista de configuracion general
     *
     * @param param
     * @return
     */
    @Override
    public Long findSize(ConfiguracionGeneralParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<ConfiguracionGeneral> root = cq.from(ConfiguracionGeneral.class);
        Join<ConfiguracionGeneral, Configuracion> configuracion = root.join("configuracion");

        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdConfiguracion() != null) {
            predList.add(qb.equal(root.get("idConfiguracion"), param.getIdConfiguracion()));
        }

        if (param.getValor() != null && !param.getValor().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("valor")), String.format("%%%s%%", param.getValor().trim().toUpperCase())));
        }
        if (param.getCodigoConfiguracion() != null && !param.getCodigoConfiguracion().trim().isEmpty()) {
            predList.add(qb.equal(configuracion.get("codigo"), param.getCodigoConfiguracion().trim()));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        Object object = q.getSingleResult();
        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }

    /**
     * Obtiene la lista de configuracion general pojo
     *
     * @param param
     * @return
     */
    @Override
    public List<ConfiguracionGeneralPojo> findPojo(ConfiguracionGeneralParam param) {
        String where = "";

        if (param.getCodigoConfiguracion() != null) {
            where = String.format(" %s and c.codigo = '%s'", where, param.getCodigoConfiguracion());
        }
        
        String sql = String.format("select cg.id_configuracion, cg.valor, c.codigo \n"
                + "from info.configuracion_general cg \n"
                + "join info.configuracion c on c.id = cg.id_configuracion \n"
                + "where cg.activo = 'S' %s\n"
                + "offset %d limit %d",
                where, param.getFirstResult(), param.getPageSize());

        javax.persistence.Query q = getEntityManager().createNativeQuery(sql);
        List<Object[]> list = q.getResultList();
        List<ConfiguracionGeneralPojo> result = new ArrayList<>();

        for (Object[] object : list) {
            ConfiguracionGeneralPojo pojo = new ConfiguracionGeneralPojo();
            pojo.setIdConfiguracion((Integer) object[0]);
            pojo.setValor((String) object[1]);
            pojo.setCodigoConfiguracion((String) object[2]);
            result.add(pojo);
        }

        return result;
    }

    /**
     * Obtiene listado Fimra Digital asociado a idClienteSepsa tabla
     * configuracion_general
     *
     * @param param
     * @return Listado de firma digital
     */
    @Override
    public List<FirmaDigitalPojo> getFirmaDigital(ConfiguracionGeneralParam param) {
        String hostSepsaCore = getCVValor("HOST_SEPSA_CORE");
        if (hostSepsaCore == null || hostSepsaCore.trim().isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra valor para el parámetro HOST_SEPSA_CORE"));
            return null;
        }
        
        String token = getCVValor("API_TOKEN_SEPSA_CORE");
        if (token == null || token.trim().isEmpty()) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra valor para el parámetro API_TOKEN_SEPSA_CORE"));
            return null;
        }

        List<FirmaDigitalPojo> firma = FirmaDigitalAdapter.obtener(hostSepsaCore, token, param);

        return firma;
    }

}
