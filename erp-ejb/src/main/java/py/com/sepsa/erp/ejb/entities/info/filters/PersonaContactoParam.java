/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.filters;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import static py.com.sepsa.utils.misc.Assertions.isNull;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de los parámetros de persona contacto
 * @author Jonathan
 */
public class PersonaContactoParam extends CommonParam {
    
    public PersonaContactoParam() {
    }

    public PersonaContactoParam(String bruto) {
        super(bruto);
    }
    
    /**
     * Identificador de persona
     */
    @QueryParam("idPersona")
    private Integer idPersona;
    
    /**
     * Identificador de contacto
     */
    @QueryParam("idContacto")
    private Integer idContacto;
    
    /**
     * Identificador de tipo de contacto
     */
    @QueryParam("idTipoContacto")
    private Integer idTipoContacto;
    
    /**
     * Identificador de cargo
     */
    @QueryParam("idCargo")
    private Integer idCargo;
    
    /**
     * Persona
     */
    @QueryParam("persona")
    private String persona;
    
    /**
     * Nombre contacto
     */
    @QueryParam("nombreContacto")
    private String nombreContacto;
    
    /**
     * Contacto
     */
    private ContactoParam contacto;
    
    @Override
    public boolean isValidToCreate() {
        
        limpiarErrores();
        
        if(isNull(idPersona)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el identificador de persona"));
        }
        
        if(isNull(idContacto) && isNull(contacto)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar el contacto"));
        }
        
        if(isNull(activo)) {
            addError(MensajePojo.createInstance()
                    .descripcion("Se debe indicar si la persona contacto esta activo"));
        }
        
        return !tieneErrores();
    }

    @Override
    public List<MensajePojo> getCustomErrores() {
        
        List<MensajePojo> list = super.getCustomErrores();
        
        if(!isNull(contacto)) {
            list.addAll(contacto.getErrores());
        }
        
        return list;
    }
    
    /**
     * Loguea el objeto
     * @param header Cabecera
     */
    @Override
    public void log(String header) {
        logObject(header, this);
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public String getPersona() {
        return persona;
    }

    public void setContacto(ContactoParam contacto) {
        this.contacto = contacto;
    }

    public ContactoParam getContacto() {
        return contacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public Integer getIdTipoContacto() {
        return idTipoContacto;
    }

    public void setIdTipoContacto(Integer idTipoContacto) {
        this.idTipoContacto = idTipoContacto;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    @Override
    public boolean isValidToEdit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
