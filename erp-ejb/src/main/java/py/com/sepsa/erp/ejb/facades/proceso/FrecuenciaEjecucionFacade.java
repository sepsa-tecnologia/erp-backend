/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.proceso.FrecuenciaEjecucion;
import py.com.sepsa.erp.ejb.entities.proceso.filters.FrecuenciaEjecucionParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface FrecuenciaEjecucionFacade extends Facade<FrecuenciaEjecucion, FrecuenciaEjecucionParam, UserInfoImpl> {

    public Boolean validToCreate(FrecuenciaEjecucionParam param);
    
}
