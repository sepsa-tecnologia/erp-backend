/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.comercial;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.NaturalezaCliente;
import py.com.sepsa.erp.ejb.entities.comercial.filters.NaturalezaClienteParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "NaturalezaClienteFacade", mappedName = "NaturalezaClienteFacade")
@Local(NaturalezaClienteFacade.class)
public class NaturalezaClienteFacadeImpl extends FacadeImpl<NaturalezaCliente, NaturalezaClienteParam> implements NaturalezaClienteFacade {

    public NaturalezaClienteFacadeImpl() {
        super(NaturalezaCliente.class);
    }
    
    public Boolean validToCreate(NaturalezaClienteParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        NaturalezaClienteParam param1 = new NaturalezaClienteParam();
        param1.setCodigo(param.getCodigo().trim());
        
        Long size = findSize(param1);
        
        if(size > 0) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe un registro con el código"));
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(NaturalezaClienteParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        NaturalezaCliente naturalezaCliente = find(param.getId());
        
        if(naturalezaCliente == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la naturaleza del cliente"));
        } else if(!naturalezaCliente.getCodigo().equals(param.getCodigo().trim())) {
            
            NaturalezaClienteParam param1 = new NaturalezaClienteParam();
            param1.setCodigo(param.getCodigo().trim());

            Long size = findSize(param1);

            if(size > 0) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe un registro con el código"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public NaturalezaCliente create(NaturalezaClienteParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        NaturalezaCliente item = new NaturalezaCliente();
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        create(item);
        
        return item;
    }
    
    @Override
    public NaturalezaCliente edit(NaturalezaClienteParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        NaturalezaCliente item = find(param.getId());
        item.setCodigo(param.getCodigo().trim());
        item.setDescripcion(param.getDescripcion().trim());
        edit(item);
        
        return item;
    }
    
    @Override
    public NaturalezaCliente find(Integer id, String codigo) {
        NaturalezaClienteParam param = new NaturalezaClienteParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.isValidToList();
        
        return findFirst(param);
    }
    
    @Override
    public List<NaturalezaCliente> find(NaturalezaClienteParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(NaturalezaCliente.class);
        Root<NaturalezaCliente> root = cq.from(NaturalezaCliente.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);;

        List<NaturalezaCliente> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(NaturalezaClienteParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<NaturalezaCliente> root = cq.from(NaturalezaCliente.class);
        
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(root.<String>get("descripcion"),
                    String.format("%%%s%%", param.getDescripcion().trim())));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
