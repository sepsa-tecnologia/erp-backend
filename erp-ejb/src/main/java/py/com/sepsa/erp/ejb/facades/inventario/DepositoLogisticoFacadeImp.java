/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.inventario;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.Cliente;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.Local;
import py.com.sepsa.erp.ejb.entities.info.filters.LocalParam;
import py.com.sepsa.erp.ejb.entities.inventario.DepositoLogistico;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.DepositoLogisticoPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "DepositoLogisticoFacade", mappedName = "DepositoLogisticoFacade")
@javax.ejb.Local(DepositoLogisticoFacade.class)
public class DepositoLogisticoFacadeImp extends FacadeImpl<DepositoLogistico, DepositoLogisticoParam> implements DepositoLogisticoFacade {

    public DepositoLogisticoFacadeImp() {
        super(DepositoLogistico.class);
    }
    
    public Boolean validToCreate(DepositoLogisticoParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        DepositoLogisticoParam param1 = new DepositoLogisticoParam();
        param1.setCodigo(param.getCodigo().trim());
        Long dsize = findSize(param1);
        if(dsize > 0) {
            param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro con el mismo código"));
        }
        
        LocalParam lparam = new LocalParam();
        lparam.setId(param.getIdLocal());
        Long lsize = facades.getLocalFacade().findSize(lparam);
        if(lsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el local"));
        }
        
        return !param.tieneErrores();
    }
    
    public Boolean validToEdit(DepositoLogisticoParam param) {
        
        if(!param.isValidToEdit()) {
            return Boolean.FALSE;
        }
        
        DepositoLogistico depositoLogistico = find(param.getId());
        
        if(depositoLogistico == null) {
            
            param.addError(MensajePojo.createInstance().descripcion("No existe el deposito logístico"));
            
        } else if (!depositoLogistico.getCodigo().equals(param.getCodigo().trim())) {
            
            DepositoLogisticoParam param1 = new DepositoLogisticoParam();
            param1.setCodigo(param.getCodigo().trim());

            DepositoLogistico dl = findFirst(param1);

            if(dl != null) {
                param.addError(MensajePojo.createInstance().descripcion("Ya existe otro registro con el mismo código"));
            }
        }
        
        LocalParam lparam = new LocalParam();
        lparam.setId(param.getIdLocal());
        Long lsize = facades.getLocalFacade().findSize(lparam);
        if(lsize <= 0) {
            param.addError(MensajePojo.createInstance().descripcion("No existe el local"));
        }
        
        return !param.tieneErrores();
    }

    @Override
    public DepositoLogistico create(DepositoLogisticoParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        DepositoLogistico result = new DepositoLogistico();
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setIdLocal(param.getIdLocal());
        result.setFechaInsercion(Calendar.getInstance().getTime());
        
        create(result);
        
        return result;
    }

    @Override
    public DepositoLogistico edit(DepositoLogisticoParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        DepositoLogistico result = find(param.getId());
        result.setCodigo(param.getCodigo().trim());
        result.setDescripcion(param.getDescripcion().trim());
        result.setActivo(param.getActivo());
        result.setIdLocal(param.getIdLocal());
           
        edit(result);
        
        return result;
    }

    @Override
    public List<DepositoLogisticoPojo> findPojo(DepositoLogisticoParam param) {

        AbstractFind find = new AbstractFind(DepositoLogisticoPojo.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("descripcion"),
                        getPath("root").get("codigo"),
                        getPath("root").get("activo"),
                        getPath("root").get("fechaInsercion"),
                        getPath("root").get("idLocal"),
                        getPath("local").get("descripcion"),
                        getPath("local").get("activo"),
                        getPath("local").get("gln"),
                        getPath("local").get("idExterno"),
                        getPath("cliente").get("idCliente"),
                        getPath("cliente").get("razonSocial"),
                        getPath("local").get("idEmpresa"),
                        getPath("empresa").get("descripcion"));
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };
        
        return find(find, param);
    }
    
    @Override
    public List<DepositoLogistico> find(DepositoLogisticoParam param) {

        AbstractFind find = new AbstractFind(DepositoLogistico.class) {
            
            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root")).distinct(true);
            }
            
            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
            
        };
        
        return find(find, param);
    }
    
    @Override
    public <T> List<T> find(AbstractFind find, DepositoLogisticoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<DepositoLogistico> root = cq.from(DepositoLogistico.class);
        Join<DepositoLogistico, Local> local = root.join("local");
        Join<DepositoLogistico, Cliente> cliente = local.join("cliente", JoinType.LEFT);
        Join<DepositoLogistico, Empresa> empresa = local.join("empresa", JoinType.LEFT);
        
        find.addPath("root", root);
        find.addPath("local", local);
        find.addPath("cliente", cliente);
        find.addPath("empresa", empresa);
        
        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdLocal( )!= null) {
            predList.add(qb.equal(root.get("idLocal"), param.getIdLocal()));
        }
        
        if (param.getIdEmpresa( )!= null) {
            predList.add(qb.equal(empresa.get("id"), param.getIdEmpresa()));
        }
        
        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(DepositoLogisticoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<DepositoLogistico> root = cq.from(DepositoLogistico.class);
        Join<DepositoLogistico, Local> local = root.join("local");
               
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }
        
        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }

        if (param.getIdLocal( )!= null) {
            predList.add(qb.equal(root.get("idLocal"), param.getIdLocal()));
        }
        
        if (param.getIdEmpresa( )!= null) {
            predList.add(qb.equal(local.get("idEmpresa"), param.getIdEmpresa()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : (Long) object;
        
        return result;
    }
}
