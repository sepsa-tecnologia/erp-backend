/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.List;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Motivo;
import py.com.sepsa.erp.ejb.entities.info.filters.MotivoParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoPojo;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface MotivoFacade extends Facade<Motivo, MotivoParam, UserInfoImpl> {

    public MotivoPojo find(Integer id, String codigoMotivo, String codigoTipoMotivo);

    public List<MotivoPojo> findPojoAlternative(MotivoParam param);
    
}
