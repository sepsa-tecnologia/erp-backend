/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.proceso;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.proceso.LoteArchivo;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteArchivoParam;
import py.com.sepsa.erp.ejb.entities.proceso.filters.LoteParam;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "LoteArchivoFacade", mappedName = "LoteArchivoFacade")
@Local(LoteArchivoFacade.class)
public class LoteArchivoFacadeImpl extends FacadeImpl<LoteArchivo, LoteArchivoParam> implements LoteArchivoFacade {

    public LoteArchivoFacadeImpl() {
        super(LoteArchivo.class);
    }

    @Override
    public Boolean validToCreate(LoteArchivoParam param, Boolean validarLote) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        LoteParam lparam = new LoteParam();
        lparam.setId(param.getIdLote());

        Long size = facades.getLoteFacade().findSize(lparam);

        if (validarLote && (size == null || size <= 0)) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No se encuentra el registro de lote"));
        }

        return !param.tieneErrores();
    }

    @Override
    public LoteArchivo create(LoteArchivoParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        LoteArchivo item = new LoteArchivo();
        item.setIdLote(param.getIdLote());
        item.setNombreArchivo(param.getNombreArchivo().trim());
        item.setContenido(param.getContenido().trim());
        item.setCodigo(param.getCodigo().trim());

        create(item);

        return item;
    }

    @Override
    public List<LoteArchivo> find(LoteArchivoParam param) {

        AbstractFind find = new AbstractFind(LoteArchivo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, LoteArchivoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<LoteArchivo> root = cq.from(LoteArchivo.class);

        find.addPath("root", root);

        find.select(cq, qb);

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdLote() != null) {
            predList.add(qb.equal(root.get("idLote"), param.getIdLote()));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        cq.orderBy(qb.desc(root.get("id")));

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(LoteArchivoParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        javax.persistence.criteria.CriteriaQuery cq = qb.createQuery();
        Root<LoteArchivo> root = cq.from(LoteArchivo.class);

        cq.select(qb.count(root.get("id")));

        Predicate pred = null;

        List<Predicate> predList = new ArrayList<>();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdLote() != null) {
            predList.add(qb.equal(root.get("idLote"), param.getIdLote()));
        }

        if (!predList.isEmpty()) {
            pred = predList.get(0);

            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(pred, predList.get(i));
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0L
                : ((Number) object).longValue();

        return result;
    }

}
