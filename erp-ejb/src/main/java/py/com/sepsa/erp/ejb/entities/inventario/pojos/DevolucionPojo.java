/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.inventario.pojos;

import java.util.Date;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
public class DevolucionPojo {

    public DevolucionPojo(Integer id, Integer idEmpresa, String empresa,
            Character anulado, Integer idFactura, Integer idEstado,
            String codigoEstado, String estado, Date fechaInsercion) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.anulado = anulado;
        this.idFactura = idFactura;
        this.idEstado = idEstado;
        this.codigoEstado = codigoEstado;
        this.estado = estado;
        this.fechaInsercion = fechaInsercion;
    }
    
    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Anulado
     */
    private Character anulado;
    
    /**
     * Identificador de factura
     */
    private Integer idFactura;
    
    /**
     * Identificador de estado
     */
    private Integer idEstado;
    
    /**
     * Código de estado
     */
    private String codigoEstado;
    
    /**
     * Estado
     */
    private String estado;
    
    /**
     * Fecha de inserción
     */
    private Date fechaInsercion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Integer idFactura) {
        this.idFactura = idFactura;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public String getCodigoEstado() {
        return codigoEstado;
    }

    public void setCodigoEstado(String codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setAnulado(Character anulado) {
        this.anulado = anulado;
    }

    public Character getAnulado() {
        return anulado;
    }
}
