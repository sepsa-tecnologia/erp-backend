/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.io.IOException;
import javax.ejb.Local;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;
import py.com.sepsa.erp.ejb.entities.info.filters.RepositorioParam;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan
 */
@Local
public interface RepositorioFacade extends Facade<Repositorio, RepositorioParam, UserInfoImpl> {

    public Repositorio getRepositorio(Integer idEmpresa, String codigo);

    public Boolean guardar(Repositorio repositorio, String fileName, byte[] bytes, String contentType);

    public byte[] obtener(Repositorio repositorio, String fileName) throws IOException;
    
}
