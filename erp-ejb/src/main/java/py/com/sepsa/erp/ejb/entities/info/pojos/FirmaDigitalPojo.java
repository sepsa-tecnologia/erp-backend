/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.info.pojos;

import java.util.Date;

/**
 *
 * @author Gustavo Benitez
 */
public class FirmaDigitalPojo {

    private Integer id;
    private Integer idCliente;
    private Date fechaInsercion;
    private Date fechaVencimiento;
    private Character activo;

    public FirmaDigitalPojo() {
    }

    public FirmaDigitalPojo(Integer id, Integer idCliente, Date fechaInsercion, Date fechaVencimiento, Character activo) {
        this.id = id;
        this.idCliente = idCliente;
        this.fechaInsercion = fechaInsercion;
        this.fechaVencimiento = fechaVencimiento;
        this.activo = activo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

}
