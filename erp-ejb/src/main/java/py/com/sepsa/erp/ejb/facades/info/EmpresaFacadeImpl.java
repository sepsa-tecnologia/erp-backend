/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.comercial.filters.ClienteParam;
import py.com.sepsa.erp.ejb.entities.info.Configuracion;
import py.com.sepsa.erp.ejb.entities.info.Empresa;
import py.com.sepsa.erp.ejb.entities.info.MenuPerfil;
import py.com.sepsa.erp.ejb.entities.info.Persona;
import py.com.sepsa.erp.ejb.entities.info.PlantillaCreacionEmpresa;
import py.com.sepsa.erp.ejb.entities.info.Repositorio;
import py.com.sepsa.erp.ejb.entities.info.TipoEmpresa;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionParam;
import py.com.sepsa.erp.ejb.entities.info.filters.ConfiguracionValorParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EmailParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.EstadoParam;
import py.com.sepsa.erp.ejb.entities.info.filters.MenuParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PersonaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.PlantillaCreacionEmpresaParam;
import py.com.sepsa.erp.ejb.entities.info.filters.RepositorioParam;
import py.com.sepsa.erp.ejb.entities.info.pojos.ConfiguracionValor;
import py.com.sepsa.erp.ejb.entities.info.pojos.EmpresaPojo;
import py.com.sepsa.erp.ejb.entities.info.pojos.Menu;
import py.com.sepsa.erp.ejb.entities.info.pojos.MotivoEmisionNc;
import py.com.sepsa.erp.ejb.entities.info.pojos.Perfil;
import py.com.sepsa.erp.ejb.entities.usuario.Usuario;
import py.com.sepsa.erp.ejb.entities.usuario.UsuarioPerfil;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioEmpresaParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioParam;
import py.com.sepsa.erp.ejb.entities.usuario.filters.UsuarioPerfilParam;
import py.com.sepsa.erp.ejb.entities.usuario.pojos.UsuarioPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.misc.Assertions;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Stateless(name = "EmpresaFacade", mappedName = "EmpresaFacade")
@Local(EmpresaFacade.class)
public class EmpresaFacadeImpl extends FacadeImpl<Empresa, EmpresaParam> implements EmpresaFacade{
    
    public EmpresaFacadeImpl() {
        super(Empresa.class);
    }

    public Boolean validToCreate(EmpresaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoEmpresa tipoEmpresa = facades.getTipoEmpresaFacade()
                .find(param.getIdTipoEmpresa(), param.getCodigoTipoEmpresa());
        
        if(tipoEmpresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de empresa"));
        } else {
            param.setIdTipoEmpresa(tipoEmpresa.getId());
            param.setCodigoTipoEmpresa(tipoEmpresa.getCodigo());
        }
        
        Empresa empresa = find(null, param.getCodigo(), null);
        
        if(empresa != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe otra empresa con el mismo código"));
        }
        
        empresa = find(null, null, param.getRuc());
        
        if(empresa != null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("Ya existe otra empresa con el mismo RUC"));
        }
        
        if(!Assertions.isNullOrEmpty(param.getUsuarios())) {
            for (UsuarioParam usuario : param.getUsuarios()) {
                facades.getUsuarioFacade().validToCreate(usuario);
            }
        }
        
        return !param.tieneErrores();
    }

    public Boolean validToEdit(EmpresaParam param) {
        
        if(!param.isValidToCreate()) {
            return Boolean.FALSE;
        }
        
        TipoEmpresa tipoEmpresa = facades.getTipoEmpresaFacade()
                .find(param.getIdTipoEmpresa(), param.getCodigoTipoEmpresa());
        
        if(tipoEmpresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el tipo de empresa"));
        } else {
            param.setIdTipoEmpresa(tipoEmpresa.getId());
            param.setCodigoTipoEmpresa(tipoEmpresa.getCodigo());
        }
        
        Empresa empresa = find(param.getId());
        if(empresa == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe la empresa"));
        } else {
            
            empresa = find(null, param.getCodigo(), null);
            if(empresa != null && !empresa.getId().equals(param.getId())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe otra empresa con el mismo código"));
            }

            empresa = find(null, null, param.getRuc());
            if(empresa != null && !empresa.getId().equals(param.getId())) {
                param.addError(MensajePojo.createInstance()
                        .descripcion("Ya existe otra empresa con el mismo RUC"));
            }
        }
        
        return !param.tieneErrores();
    }
    
    @Override
    public Empresa create(EmpresaParam param, UserInfoImpl userInfo) {
        
        if(!validToCreate(param)) {
            return null;
        }
        
        Empresa empresa = new Empresa();

        empresa.setDescripcion(param.getDescripcion());
        empresa.setCodigo(param.getCodigo());
        empresa.setRuc(param.getRuc());
        empresa.setActivo(param.getActivo());
        empresa.setIdTipoEmpresa(param.getIdTipoEmpresa());
        empresa.setIdClienteSepsa(param.getIdClienteSepsa());
        empresa.setFacturadorElectronico(param.getFacturadorElectronico());
        
        create(empresa);
        
        param.setId(empresa.getId());
        
        try {
            crearTemplate(param.getCodigoTemplate(),param, userInfo);
        } catch (Exception ex) {
            Logger.getLogger(EmpresaFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
        }        
        if(!Assertions.isNullOrEmpty(param.getUsuarios())) {
            for (UsuarioParam usuario : param.getUsuarios()) {
                usuario.setIdEmpresa(empresa.getId());
                facades.getUsuarioFacade().create(usuario, userInfo);
            }
        }
        
        return empresa;
    }
    
    public void crearTemplate(String codigo, EmpresaParam empresa, UserInfoImpl userInfo) throws Exception{
        if (codigo== null || codigo.equals("")){
            codigo = "ESTANDAR";
        }
    
        Integer idEmpresa = empresa.getId();
        PlantillaCreacionEmpresaParam pce = new PlantillaCreacionEmpresaParam();
        pce.setCodigo(codigo);
        PlantillaCreacionEmpresa plantilla = facades.getPlantillaCreacionEmpresaFacade().find(null, codigo);
        
        
        //Se asocia el usuario 1 Admin a la Empresa.
        UsuarioEmpresaParam userEmpresa = new UsuarioEmpresaParam();
        userEmpresa.setIdEmpresa(idEmpresa);
        userEmpresa.setIdUsuario(1);
        userEmpresa.setActivo('S');
        facades.getUsuarioEmpresaFacade().create(userEmpresa, userInfo);
          
        if (plantilla != null){
            setMenuHijos(plantilla.getMenu(),null,idEmpresa);
            
            //Se crea a la empresa como cliente
            PersonaParam p = new PersonaParam();
            p.setRazonSocial(empresa.getDescripcion());
            p.setNombreFantasia(empresa.getDescripcion());
            p.setIdEmpresa(idEmpresa);
            p.setIdTipoPersona(2);
            p.setActivo('S');
            Persona persona = facades.getPersonaFacade().create(p,userInfo);
            
            ClienteParam cliente = new ClienteParam();
            cliente.setIdCliente(persona.getId());
            cliente.setIdEmpresa(idEmpresa);
            cliente.setIdEstado(4);
            cliente.setRazonSocial(empresa.getDescripcion());
            cliente.setProveedor('N');
            cliente.setComprador('N');
            cliente.setIdNaturalezaCliente(1);
            cliente.setIdTipoDocumento(1);
            cliente.setNroDocumento(empresa.getRuc());
            cliente.setFactElectronica('S');
            cliente.setPersona(p);
            facades.getClienteFacade().create(cliente,userInfo);
                 
            //Se crea el valor de configuración de RUC de la empresa
            py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor confValorRuc = new py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor();
            confValorRuc.setActivo('S');
            confValorRuc.setValor(empresa.getRuc());
            confValorRuc.setIdConfiguracion(32);
            confValorRuc.setIdEmpresa(idEmpresa);
            facades.getConfiguracionValorFacade().create(confValorRuc);

            //Se crean las configuraciones del template
            if(plantilla.getConfiguracionValor()!= null && !plantilla.getConfiguracionValor().isEmpty()){
                for (ConfiguracionValor cv : plantilla.getConfiguracionValor()) {
                    ConfiguracionParam config = new ConfiguracionParam();
                    config.setCodigo(cv.getCodigo());
                    Configuracion configuracion = facades.getConfiguracionFacade().findFirst(config);
                    
                    py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor confValor = new py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor();
                    confValor.setActivo(cv.getActivo());
                    confValor.setValor(cv.getValor());
                    confValor.setIdConfiguracion(configuracion.getId());
                    confValor.setIdEmpresa(idEmpresa);
                    facades.getConfiguracionValorFacade().create(confValor);
                }
            }
            
            //Se crea el registro de repositorio
            RepositorioParam repoFilter = new RepositorioParam();
            repoFilter.setIdEmpresa(1);
            Repositorio repositorio = facades.getRepositorioFacade().findFirst(repoFilter);
            
            RepositorioParam repositorioCreate = new RepositorioParam();
            repositorioCreate.setActivo('S');
            repositorioCreate.setIdEmpresa(idEmpresa);
            repositorioCreate.setBucket(repositorio.getBucket());
            repositorioCreate.setClaveAcceso(repositorio.getClaveAcceso());
            repositorioCreate.setClaveSecreta(repositorio.getClaveSecreta());
            repositorioCreate.setRegion(repositorio.getRegion());
            repositorioCreate.setIdTipoRepositorio(repositorio.getIdTipoRepositorio());
            
            facades.getRepositorioFacade().create(repositorioCreate, userInfo);
            
            //Se crean los motivos de emisión nc del template
            if(plantilla.getMotivoEmisionNc() != null && !plantilla.getMotivoEmisionNc().isEmpty()){
                for (MotivoEmisionNc mEmision: plantilla.getMotivoEmisionNc()) {
                    py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno motivoEmision = new py.com.sepsa.erp.ejb.entities.facturacion.MotivoEmisionInterno();             
                    motivoEmision.setActivo(mEmision.getActivo());
                    motivoEmision.setIdEmpresa(idEmpresa);
                    motivoEmision.setCodigo(mEmision.getCodigo());
                    motivoEmision.setDescripcion(mEmision.getDescripcion());
                    motivoEmision.setIdMotivoEmision(mEmision.getIdTipoEmision());
                    facades.getMotivoEmisionInternoFacade().create(motivoEmision);    
                }
            }
            
            //Se crean los perfiles del template
            for (Perfil perf : plantilla.getPerfil()){
                py.com.sepsa.erp.ejb.entities.usuario.Perfil perfil = new py.com.sepsa.erp.ejb.entities.usuario.Perfil();
                perfil.setActivo(perf.getActivo());
                perfil.setCodigo(perf.getCodigo());
                perfil.setDescripcion(perf.getDescripcion());
                perfil.setIdEmpresa(idEmpresa);
                
                facades.getPerfilFacade().create(perfil);
                
                PlantillaCreacionEmpresa plantillaMenu = facades.getPlantillaCreacionEmpresaFacade().find(null, perfil.getCodigo());
                
                crearMenuPerfil(plantillaMenu.getMenu(),idEmpresa,perfil.getId());
            }
            
            for(UsuarioParam usuarioParam : empresa.getUsuarios()){
                    //Se crea a la empresa como cliente
                PersonaParam personaFisicaParam = new PersonaParam();
                personaFisicaParam.setNombre(usuarioParam.getPersonaFisica().getNombre());
                personaFisicaParam.setApellido(usuarioParam.getPersonaFisica().getApellido());
                personaFisicaParam.setIdEmpresa(idEmpresa);
                personaFisicaParam.setIdTipoPersona(1);
                personaFisicaParam.setActivo('S');
                Persona personaFisica = facades.getPersonaFacade().create(personaFisicaParam,userInfo);
                
                //Se crean los usuarios
                UsuarioParam userParam= new UsuarioParam();
                userParam.setUsuario(usuarioParam.getUsuario());
                userParam.setContrasena("IdoWm123_412807vbdfbo0_932479350s090sdf_812");
                userParam.setIdEmpresa(idEmpresa);
                userParam.setIdEstado(22);
                userParam.setIdPersonaFisica(personaFisica.getId());
                
                EmailParam email = new EmailParam();
                email.setEmail(usuarioParam.getEmail().getEmail());
                email.setIdTipoEmail(1);
                email.setPrincipal('S');
                userParam.setEmail(email);
                
                py.com.sepsa.erp.ejb.entities.usuario.Usuario userCreated = facades.getUsuarioFacade().create(userParam,userInfo);

                EstadoParam estadoParam = new EstadoParam();
                estadoParam.setCodigoTipoEstado("USUARIO_PERFIL");
                estadoParam.setCodigoEstado("ACTIVO");
                Integer idEstado = facades.getEstadoFacade().findFirst(estadoParam).getId();
                           
                for (UsuarioPerfilParam userPerfilParam : usuarioParam.getUsuarioPerfiles()){
                    py.com.sepsa.erp.ejb.entities.usuario.filters.PerfilParam perfilFilter = new py.com.sepsa.erp.ejb.entities.usuario.filters.PerfilParam();
                    perfilFilter.setCodigo(userPerfilParam.getCodigo());
                    perfilFilter.setIdEmpresa(idEmpresa);
                    perfilFilter.setFirstResult(0);
                    perfilFilter.setPageSize(10);
                    List<py.com.sepsa.erp.ejb.entities.usuario.Perfil> perfiles = facades.getPerfilFacade().find(perfilFilter);
                    Integer idPerfil = perfiles.get(0).getId();
                     
                    UsuarioPerfil upCreate = new UsuarioPerfil();
                    upCreate.setIdPerfil(idPerfil);
                    upCreate.setIdUsuario(userCreated.getId());
                    upCreate.setIdEstado(idEstado);
                    facades.getUsuarioPerfilFacade().create(upCreate);
                                       
                }
               
                userParam.setUrlDestino(empresa.getApiUrl());
                facades.getUsuarioFacade().enviarCorreoRecuperacion(userParam);
            }
            //Se crea el usuario para los procesos en hilo
            PersonaParam personaP = new PersonaParam();
            personaP.setNombre(String.format("proceso_%s",empresa.getDescripcion().toLowerCase().trim().replace(" ","")));
            personaP.setApellido(String.format("proceso_%s",empresa.getDescripcion().toLowerCase().trim().replace(" ","")));
            personaP.setIdEmpresa(idEmpresa);
            personaP.setIdTipoPersona(1);
            personaP.setActivo('S');
            Persona personaF = facades.getPersonaFacade().create(personaP,userInfo);

            UsuarioParam usuarioProceso= new UsuarioParam();
            usuarioProceso.setUsuario(personaP.getNombre());
            usuarioProceso.setContrasena(personaP.getNombre());
            usuarioProceso.setIdEmpresa(idEmpresa);
            usuarioProceso.setIdEstado(22);
            usuarioProceso.setIdPersonaFisica(personaF.getId());
            Usuario usuarioCreado = facades.getUsuarioFacade().create(usuarioProceso,userInfo);
            
            //Se obtiene el perfil para asginar al usuario
            py.com.sepsa.erp.ejb.entities.usuario.filters.PerfilParam perfilFilter = new py.com.sepsa.erp.ejb.entities.usuario.filters.PerfilParam();
            perfilFilter.setCodigo("ROLE_ADMIN");
            perfilFilter.setIdEmpresa(idEmpresa);
            perfilFilter.setFirstResult(0);
            perfilFilter.setPageSize(10);
            List<py.com.sepsa.erp.ejb.entities.usuario.Perfil> perfiles = facades.getPerfilFacade().find(perfilFilter);
            Integer idPerfil = perfiles.get(0).getId();
            
            EstadoParam estadoParam = new EstadoParam();
            estadoParam.setCodigoTipoEstado("USUARIO_PERFIL");
            estadoParam.setCodigoEstado("ACTIVO");
            Integer idEstado = facades.getEstadoFacade().findFirst(estadoParam).getId();
            
            UsuarioPerfil upCreate = new UsuarioPerfil();
            upCreate.setIdPerfil(idPerfil);
            upCreate.setIdUsuario(usuarioCreado.getId());
            upCreate.setIdEstado(idEstado);
            facades.getUsuarioPerfilFacade().create(upCreate);
            
            //Asignar el usuario creado a la configuración de la empresa
            ConfiguracionParam configParam = new ConfiguracionParam();
            configParam.setCodigo("USER");
            Integer idConfigUser = facades.getConfiguracionFacade().findFirst(configParam).getId();
            configParam.setCodigo("PASS");
            Integer idConfigPass = facades.getConfiguracionFacade().findFirst(configParam).getId();
            
            ConfiguracionValorParam cvParam = new ConfiguracionValorParam();
            cvParam.setIdEmpresa(idEmpresa);
            
            cvParam.setIdConfiguracion(idConfigUser);
            py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor confValorUser = facades.getConfiguracionValorFacade().findFirst(cvParam);
            confValorUser.setValor(usuarioProceso.getUsuario());
            facades.getConfiguracionValorFacade().edit(confValorUser);
            
            cvParam.setIdConfiguracion(idConfigPass);
            py.com.sepsa.erp.ejb.entities.info.ConfiguracionValor confValorPass = facades.getConfiguracionValorFacade().findFirst(cvParam);
            confValorPass.setValor(usuarioProceso.getUsuario());
            facades.getConfiguracionValorFacade().edit(confValorPass);
                      
        }
    }
    
    /**
     * Método recursivo para crear objetos Menu, si un menú tiene hijos que deben ser creados
     * el método vuelve a llamarse.
     * @param menu
     * @param idPadre
     * @param idEmpresa 
     */
    public void setMenuHijos(List<Menu> menu, Integer idPadre, Integer idEmpresa){
        
        for (Menu m : menu){
            py.com.sepsa.erp.ejb.entities.info.Menu menuHijo = new py.com.sepsa.erp.ejb.entities.info.Menu();
            menuHijo.setActivo(m.getActivo());
            menuHijo.setCodigo(m.getCodigo());
            menuHijo.setDescripcion(m.getDescripcion());
            menuHijo.setUrl(m.getUrl());
            menuHijo.setIdTipoMenu(m.getIdTipoMenu());
            menuHijo.setOrden(m.getOrden());
            menuHijo.setTitulo(m.getTitulo());
            menuHijo.setIdPadre(idPadre);
            menuHijo.setIdEmpresa(idEmpresa);
            
            facades.getMenuFacade().create(menuHijo);
            
            //Si el menú tiene "hijos", primero se crean a todos sus hijos antes de pasar al siguiente menú de la iteración.
            if(m.getHijos() != null && !m.getHijos().isEmpty()){
                setMenuHijos(m.getHijos(),menuHijo.getId(),idEmpresa);
            }
            

        }
    }
    
    public void crearMenuPerfil(List<Menu> menu,Integer idEmpresa, Integer idPerfil){
        for (Menu m : menu){
            
            MenuParam param = new MenuParam();
            param.setIdEmpresa(idEmpresa);
            param.setCodigo(m.getCodigo());
            
            py.com.sepsa.erp.ejb.entities.info.Menu menuCreado = facades.getMenuFacade().findFirst(param);
            
            MenuPerfil menuPerfilCreate = new MenuPerfil();
            menuPerfilCreate.setIdMenu(menuCreado.getId());
            menuPerfilCreate.setIdPerfil(idPerfil);
            menuPerfilCreate.setActivo('S');
                        
            facades.getMenuPerfilFacade().create(menuPerfilCreate);
            
            if(m.getHijos() != null && !m.getHijos().isEmpty()){
                crearMenuPerfil(m.getHijos(),idEmpresa, idPerfil);
            }
        }
    }
    
    @Override
    public Empresa edit(EmpresaParam param, UserInfoImpl userInfo) {
        
        if(!validToEdit(param)) {
            return null;
        }
        
        Empresa empresa = find(param.getId());
        empresa.setDescripcion(param.getDescripcion());
        empresa.setCodigo(param.getCodigo());
        empresa.setRuc(param.getRuc());
        empresa.setActivo(param.getActivo());
        empresa.setIdTipoEmpresa(param.getIdTipoEmpresa());
        empresa.setIdClienteSepsa(param.getIdClienteSepsa());
        if(param.getFacturadorElectronico() != null) {
            empresa.setFacturadorElectronico(param.getFacturadorElectronico());
        }
        
        edit(empresa);
        
        return empresa;
    }
    
    @Override
    public Empresa find(Integer id, String codigo, String ruc) {
        EmpresaParam param = new EmpresaParam();
        param.setId(id);
        param.setCodigo(codigo);
        param.setRuc(ruc);
        return findFirst(param);
    }
    
    @Override
    public List<Empresa> find(EmpresaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(Empresa.class);
        Root<Empresa> root = cq.from(Empresa.class);
        
        cq.select(root);

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        if (param.getIdTipoEmpresa() != null) {
            predList.add(qb.equal(root.get("idTipoEmpresa"), param.getIdTipoEmpresa()));
        }
        
        if (param.getIdClienteSepsa() != null) {
            predList.add(qb.equal(root.get("idClienteSepsa"), param.getIdClienteSepsa()));
        }
        
        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getFacturadorElectronico() != null) {
            predList.add(qb.equal(root.get("facturadorElectronico"), param.getFacturadorElectronico()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
        
        List<Empresa> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(EmpresaParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Empresa> root = cq.from(Empresa.class);
        
        cq.select(qb.count(root.get("id")));

        List<Predicate> predList = new ArrayList<>();
        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getCodigo() != null && !param.getCodigo().trim().isEmpty()) {
            predList.add(qb.equal(root.get("codigo"), param.getCodigo().trim()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")),
                    String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }
        
        if (param.getIdTipoEmpresa() != null) {
            predList.add(qb.equal(root.get("idTipoEmpresa"), param.getIdTipoEmpresa()));
        }
        
        if (param.getIdClienteSepsa() != null) {
            predList.add(qb.equal(root.get("idClienteSepsa"), param.getIdClienteSepsa()));
        }
        
        if (param.getRuc() != null && !param.getRuc().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("ruc")),
                    String.format("%%%s%%", param.getRuc().trim())));
        }
        
        if (param.getActivo() != null) {
            predList.add(qb.equal(root.get("activo"), param.getActivo()));
        }
        
        if (param.getFacturadorElectronico() != null) {
            predList.add(qb.equal(root.get("facturadorElectronico"), param.getFacturadorElectronico()));
        }
        
        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager().createQuery(cq);
        
        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number)object).longValue();
        
        return result;
    }
    
    @Override
    public List<EmpresaPojo> findCantidadEmpresas(EmpresaParam param) {
        
        String sql = "select e.id_cliente_sepsa, count(*)"
                + " from info.empresa e"
                + " where not e.id_cliente_sepsa is null"
                + " and e.activo = 'S'"
                + " group by e.id_cliente_sepsa;";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<EmpresaPojo> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            EmpresaPojo item = new EmpresaPojo();
            item.setIdClienteSepsa((Integer)objects[i++]);
            item.setCantidad((Number)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public List<EmpresaPojo> findCantidadUsuarios(EmpresaParam param) {
        
        String sql = "select e.id_cliente_sepsa, count(*)"
                + " from info.empresa e"
                + " join usuario.usuario_empresa ue on ue.id_empresa = e.id"
                + " join usuario.usuario u on ue.id_usuario = u.id"
                + " join info.estado s on u.id_estado = s.id"
                + " where not e.id_cliente_sepsa is null"
                + " and e.activo = 'S'"
                + " and ue.activo = 'S'"
                + " and ue.facturable = 'S'"
                + " and not u.usuario ilike 'proceso%'"
                + " and not u.usuario = 'admin'"
                + " and s.codigo = 'ACTIVO'"
                + " group by e.id_cliente_sepsa;";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<EmpresaPojo> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            EmpresaPojo item = new EmpresaPojo();
            item.setIdClienteSepsa((Integer)objects[i++]);
            item.setCantidad((Number)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    @Override
    public List<UsuarioPojo> findUsuarios(Integer idClienteSepsa, String bandera) {
        
        String sql = String.format("select u.id, u.usuario, es.codigo\n"
                + "from info.empresa e\n"
                + "join usuario.usuario_empresa ue on ue.id_empresa = e.id\n"
                + "join usuario.usuario u on ue.id_usuario = u.id\n"
                + "join info.estado es on es.id = u.id_estado\n"
                + "where e.id_cliente_sepsa = %s and es.codigo = '%s'\n"
                + "and (e.activo = 'S' or e.activo = 'B')\n"
                + "and ue.activo = 'S'\n"
                + "and ue.facturable = 'S'", idClienteSepsa, bandera);
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<UsuarioPojo> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            UsuarioPojo item = new UsuarioPojo();
            item.setId((Integer)objects[i++]);
            item.setUsuario((String)objects[i++]);
            item.setEstado((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    
    @Override
    public List<Empresa> findEmpresasFacturasPendientes(EmpresaParam param) {
        
        String sql = "select distinct e.*\n" +
"	from info.empresa e\n" +
"	join facturacion.factura f on f.id_empresa = e.id\n" +
"	where e.activo = 'S' and e.facturador_electronico = 'S'\n" +
"	and f.digital = 'S' and f.anulado = 'N' and f.archivo_set = 'S'\n" +
"	and f.generado_set = 'N' and f.fecha_insercion >= (now() - interval '30 day')";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
        List<Empresa> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            Empresa item = new Empresa();
            item.setId((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigo((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setRuc((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    
     @Override
    public List<Empresa> findEmpresasNotaCreditoPendientes(EmpresaParam param) {
        
        String sql = "select distinct e.*\n" +
"	from info.empresa e\n" +
"	join facturacion.nota_credito nc on nc.id_empresa = e.id\n" +
"	where e.activo = 'S' and e.facturador_electronico = 'S'\n" +
"	and nc.digital = 'S' and nc.anulado = 'N' and nc.archivo_set = 'S'\n" +
"	and nc.generado_set = 'N' and nc.fecha_insercion >= (now() - interval '30 day')";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
       List<Empresa> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            Empresa item = new Empresa();
            item.setId((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigo((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setRuc((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    
     @Override
    public List<Empresa> findEmpresasNotaDebitoPendientes(EmpresaParam param) {
        
        String sql = "select distinct e.*\n" +
"	from info.empresa e\n" +
"	join facturacion.nota_debito nd on nd.id_empresa = e.id\n" +
"	where e.activo = 'S' and e.facturador_electronico = 'S'\n" +
"	and nd.digital = 'S' and nd.anulado = 'N' and nd.archivo_set = 'S'\n" +
"	and nd.generado_set = 'N' and nd.fecha_insercion >= (now() - interval '30 day')";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
       List<Empresa> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            Empresa item = new Empresa();
            item.setId((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigo((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setRuc((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    
     @Override
    public List<Empresa> findEmpresasAutofacturasPendientes(EmpresaParam param) {
        
        String sql = "select distinct e.*\n" +
"	from info.empresa e\n" +
"	join facturacion.autofactura af on af.id_empresa = e.id\n" +
"	where e.activo = 'S' and e.facturador_electronico = 'S'\n" +
"	and af.digital = 'S' and af.anulado = 'N' and af.archivo_set = 'S'\n" +
"	and af.generado_set = 'N' and af.fecha_insercion >= (now() - interval '30 day')";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
       List<Empresa> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            Empresa item = new Empresa();
            item.setId((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigo((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setRuc((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
    
    
    @Override
    public List<Empresa> findEmpresasNotaRemisionPendientes(EmpresaParam param) {
        
        String sql = "select distinct e.*\n" +
"	from info.empresa e\n" +
"	join facturacion.nota_remision nr on nr.id_empresa = e.id\n" +
"	where e.activo = 'S' and e.facturador_electronico = 'S'\n" +
"	and nr.digital = 'S' and nr.anulado = 'N' and nr.archivo_set = 'S'\n" +
"	and nr.generado_set = 'N' and nr.fecha_insercion >= (now() - interval '30 day')";
        
        Query q = getEntityManager().createNativeQuery(sql);
        
        List<Object[]> list = q.getResultList();
        
       List<Empresa> result = new ArrayList();
        
        for (Object[] objects : list) {
            int i = 0;
            Empresa item = new Empresa();
            item.setId((Integer)objects[i++]);
            item.setDescripcion((String)objects[i++]);
            item.setCodigo((String)objects[i++]);
            item.setActivo((Character)objects[i++]);
            item.setRuc((String)objects[i++]);
            result.add(item);
        }
        
        return result;
    }
}
