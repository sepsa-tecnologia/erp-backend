/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.info;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import py.com.sepsa.erp.ejb.entities.info.TipoContacto;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 *
 * @author Jonathan
 */
@Stateless(name = "TipoContactoFacade", mappedName = "TipoContactoFacade")
@Local(TipoContactoFacade.class)
public class TipoContactoFacadeImpl extends FacadeImpl<TipoContacto, CommonParam> implements TipoContactoFacade {

    public TipoContactoFacadeImpl() {
        super(TipoContacto.class);
    }

    @Override
    public List<TipoContacto> find(CommonParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoContacto> root = cq.from(TipoContacto.class);
        cq.select(root);
        cq.orderBy(qb.asc(root.get("id")));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;
        
        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<TipoContacto> result = q.getResultList();

        return result;
    }
    
    @Override
    public Long findSize(CommonParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<TipoContacto> root = cq.from(TipoContacto.class);
        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = null;

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getDescripcion() != null && !param.getDescripcion().trim().isEmpty()) {
            predList.add(qb.like(qb.upper(root.<String>get("descripcion")), String.format("%%%s%%", param.getDescripcion().trim().toUpperCase())));
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (predList.size() > 0) {
            pred = predList.get(0);
            for (int i = 1; i < predList.size(); i++) {
                pred = qb.and(predList.get(i), pred);
            }
        }

        if (pred != null) {
            cq.where(pred);
        }

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        Object object = q.getSingleResult();
        
        Long result = object == null
                ? null
                : ((Number)object).longValue();

        return result;
    }

}
