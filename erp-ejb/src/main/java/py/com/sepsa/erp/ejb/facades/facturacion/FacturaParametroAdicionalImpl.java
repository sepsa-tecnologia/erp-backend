/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.facades.facturacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.facturacion.Factura;
import py.com.sepsa.erp.ejb.entities.facturacion.FacturaParametroAdicional;
import py.com.sepsa.erp.ejb.entities.facturacion.ParametroAdicional;
import py.com.sepsa.erp.ejb.entities.facturacion.filters.FacturaParametroAdicionalParam;
import py.com.sepsa.erp.ejb.entities.facturacion.pojos.FacturaParametroAdicionalPojo;
import py.com.sepsa.erp.ejb.utils.FacadeImpl;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.utils.facades.AbstractFind;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 *
 * @author Williams Vera
 */
@Stateless(name = "FacturaParametroAdicionalFacade", mappedName = "FacturaParametroAdicionalFacade")
@Local(FacturaParametroAdicionalFacade.class)
@Log4j2
public class FacturaParametroAdicionalImpl extends FacadeImpl<FacturaParametroAdicional, FacturaParametroAdicionalParam> implements FacturaParametroAdicionalFacade {

    public FacturaParametroAdicionalImpl() {
        super(FacturaParametroAdicional.class);
    }

    /**
     * Verifica si el objeto es válido para editar
     *
     * @param param parámetros
     * @return Bandera
     */
    public Boolean validToEdit(FacturaParametroAdicionalParam param) {

        if (!param.isValidToEdit()) {
            return Boolean.FALSE;
        }

        FacturaParametroAdicional parametroAdicional = facades.getFacturaParametroAdicionalFacade().find(param.getId());

        if (parametroAdicional == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de parametro adicional"));
        }

        return !param.tieneErrores();
    }

    /**
     * Verifica si el objeto es válido para crear
     *
     * @param param parámetros
     * @param validarFactura Validar factura
     * @return Bandera
     */
    @Override
    public Boolean validToCreate(FacturaParametroAdicionalParam param, boolean validarFactura) {

        if (!param.isValidToCreate()) {
            return Boolean.FALSE;
        }

        ParametroAdicional parametroAdicional = facades.getParametroAdicionalFacade().find(param.getIdParametroAdicional());

        if (parametroAdicional == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de parametro adicional"));
        } else {
            switch (parametroAdicional.getCodigoTipoParametroAdicional()) {
                case "NUMERIC":
                    try {
                    BigDecimal big = new BigDecimal(param.getValor());
                } catch (java.lang.NumberFormatException e) {
                    param.addError(MensajePojo.createInstance()
                            .descripcion("El valor del parametro de tipo Numerico es incorrecto"));
                }
                break;

                case "BOOLEAN":
                    if (!param.getValor().equalsIgnoreCase("true") && !param.getValor().equalsIgnoreCase("false")) {
                        param.addError(MensajePojo.createInstance()
                                .descripcion("El valor del parametro de tipo Boolean es incorrecto"));
                    }
                    break;
            }
        }

        Factura factura = facades.getFacturaFacade().find(param.getIdFactura());

        if (validarFactura && factura == null) {
            param.addError(MensajePojo.createInstance()
                    .descripcion("No existe el registro de factura"));
        }

        return !param.tieneErrores();
    }

    @Override
    public FacturaParametroAdicional create(FacturaParametroAdicionalParam param, UserInfoImpl userInfo) {

        if (!validToCreate(param, true)) {
            return null;
        }

        FacturaParametroAdicional parametroAdicional = new FacturaParametroAdicional();
        parametroAdicional.setIdEmpresa(userInfo.getIdEmpresa());
        parametroAdicional.setIdFactura(param.getIdFactura());
        parametroAdicional.setIdParametroAdicional(param.getIdParametroAdicional());
        parametroAdicional.setValor(param.getValor());
        parametroAdicional.setFechaInsercion(Calendar.getInstance().getTime());

        create(parametroAdicional);

        return parametroAdicional;
    }

    /**
     * Edita una instancia de documento
     *
     * @param param parámetros
     * @return Instancia
     */
    public FacturaParametroAdicional edit(FacturaParametroAdicionalParam param) {

        if (!validToEdit(param)) {
            return null;
        }

        FacturaParametroAdicional parametroAdicional = facades.getFacturaParametroAdicionalFacade().find(param.getId());
        parametroAdicional.setValor(param.getValor());
        edit(parametroAdicional);

        return parametroAdicional;
    }

    @Override
    public List<FacturaParametroAdicionalPojo> findPojo(FacturaParametroAdicionalParam param) {

        AbstractFind find = new AbstractFind(FacturaParametroAdicionalPojo.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.multiselect(getPath("root").get("id"),
                        getPath("root").get("idFactura"),
                        getPath("root").get("idParametroAdicional"),
                        getPath("parametroAdicional").get("codigo"),
                        getPath("root").get("valor"),
                        getPath("parametroAdicional").get("datosAdicionales"),
                        getPath("parametroAdicional").get("codigoTipoDatoParametroAdicional"),
                        getPath("parametroAdicional").get("codigoTipoParametroAdicional"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }
        };

        return find(find, param);
    }

    @Override
    public List<FacturaParametroAdicional> find(FacturaParametroAdicionalParam param) {

        AbstractFind find = new AbstractFind(FacturaParametroAdicional.class) {

            @Override
            public void select(CriteriaQuery cq, CriteriaBuilder qb) {
                cq.select(getPath("root"));
            }

            @Override
            public Query query(CriteriaQuery cq, EntityManager em) {
                return em.createQuery(cq)
                        .setFirstResult(param.getFirstResult())
                        .setMaxResults(param.getPageSize())
                        .setHint(REFRESH_HINT, REFRESH_HINT_VAL);
            }

        };

        return find(find, param);
    }

    @Override
    public <T> List<T> find(AbstractFind find, FacturaParametroAdicionalParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery(find.getResultClass());
        Root<FacturaParametroAdicional> root = cq.from(FacturaParametroAdicional.class);
        Join<FacturaParametroAdicional, ParametroAdicional> parametroAdicional = root.join("parametroAdicional");

        find.addPath("root", root);
        find.addPath("parametroAdicional", parametroAdicional);

        find.select(cq, qb);

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getIdParametroAdicional() != null) {
            predList.add(qb.equal(root.get("idParametroAdicional"), param.getIdParametroAdicional()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(parametroAdicional.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getCodigoTipoParametroAdicional() != null && !param.getCodigoTipoParametroAdicional().trim().isEmpty()) {
            predList.add(qb.equal(parametroAdicional.get("codigoTipoParametroAdicional"), param.getCodigoTipoParametroAdicional().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.orderBy(qb.desc(root.get("id")));
        cq.where(pred);

        javax.persistence.Query q = getEntityManager()
                .createQuery(cq)
                .setFirstResult(param.getFirstResult())
                .setMaxResults(param.getPageSize())
                .setHint(REFRESH_HINT, REFRESH_HINT_VAL);

        List<T> result = q.getResultList();

        return result;
    }

    @Override
    public Long findSize(FacturaParametroAdicionalParam param) {

        CriteriaBuilder qb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<FacturaParametroAdicional> root = cq.from(FacturaParametroAdicional.class);
        Join<FacturaParametroAdicional, ParametroAdicional> parametroAdicional = root.join("parametroAdicional");

        cq.select(qb.count(root));

        List<Predicate> predList = new ArrayList<>();

        Predicate pred = qb.and();

        if (param.getId() != null) {
            predList.add(qb.equal(root.get("id"), param.getId()));
        }

        if (param.getIdEmpresa() != null) {
            predList.add(qb.equal(root.get("idEmpresa"), param.getIdEmpresa()));
        }

        if (param.getIdFactura() != null) {
            predList.add(qb.equal(root.get("idFactura"), param.getIdFactura()));
        }

        if (param.getIdParametroAdicional() != null) {
            predList.add(qb.equal(root.get("idParametroAdicional"), param.getIdParametroAdicional()));
        }

        if (param.getCodigoEmpresa() != null && !param.getCodigoEmpresa().trim().isEmpty()) {
            predList.add(qb.equal(parametroAdicional.get("codigo"), param.getCodigoEmpresa().trim()));
        }

        if (param.getCodigoTipoParametroAdicional() != null && !param.getCodigoTipoParametroAdicional().trim().isEmpty()) {
            predList.add(qb.equal(parametroAdicional.get("codigoTipoParametroAdicional"), param.getCodigoTipoParametroAdicional().trim()));
        }

        for (int i = 0; i < predList.size(); i++) {
            pred = qb.and(predList.get(i), pred);
        }

        cq.where(pred);

        javax.persistence.Query q = getEntityManager().createQuery(cq);

        Object object = q.getSingleResult();

        Long result = object == null
                ? 0
                : ((Number) object).longValue();

        return result;
    }
}
