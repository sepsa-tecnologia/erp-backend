/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.erp.ejb.entities.facturacion.pojos;

import java.util.Date;

/**
 * POJO para la entidad talonario
 * @author Jonathan
 */
public class TalonarioPojo {

    public TalonarioPojo(Integer id, Integer idEmpresa, String empresa,
            String timbrado, Integer inicio, Integer fin, Date fechaVencimiento,
            Character activo, String nroSucursal, String nroPuntoVenta,
            Date fechaInicio, String nroResolucion, Date fechaResolucion,
            String serie, Character digital, Integer idTipoDocumento,
            String tipoDocumento, String direccionLocalTalonario,
            String localTalonario, String idExternoLocalTalonario) {
        this.id = id;
        this.idEmpresa = idEmpresa;
        this.empresa = empresa;
        this.timbrado = timbrado;
        this.inicio = inicio;
        this.fin = fin;
        this.fechaVencimiento = fechaVencimiento;
        this.activo = activo;
        this.nroSucursal = nroSucursal;
        this.nroPuntoVenta = nroPuntoVenta;
        this.fechaInicio = fechaInicio;
        this.nroResolucion = nroResolucion;
        this.fechaResolucion = fechaResolucion;
        this.serie = serie;
        this.digital = digital;
        this.idTipoDocumento = idTipoDocumento;
        this.tipoDocumento = tipoDocumento;
        this.direccionLocalTalonario = direccionLocalTalonario;
        this.localTalonario = localTalonario;
        this.idExternoLocalTalonario = idExternoLocalTalonario;
    }

    /**
     * Identificador
     */
    private Integer id;
    
    /**
     * Identificador de empresa
     */
    private Integer idEmpresa;
    
    /**
     * Empresa
     */
    private String empresa;
    
    /**
     * Timbrado
     */
    private String timbrado;
    
    /**
     * Inicio
     */
    private Integer inicio;
    
    /**
     * Fin
     */
    private Integer fin;
    
    /**
     * Fecha vencimiento
     */
    private Date fechaVencimiento;
    
    /**
     * Activo
     */
    private Character activo;
    
    /**
     * Nro de sucursal
     */
    private String nroSucursal;
    
    /**
     * Nro de punto de venta
     */
    private String nroPuntoVenta;
    
    /**
     * Fecha de inicio
     */
    private Date fechaInicio;
    
    /**
     * Nro de resolución
     */
    private String nroResolucion;
    
    /**
     * Fecha de resolución
     */
    private Date fechaResolucion;
    
    /**
     * Serie
     */
    private String serie;
    
    /**
     * Digital
     */
    private Character digital;
    
    /**
     * Identificador de tipo de documento
     */
    private Integer idTipoDocumento;
    
    /**
     * Tipo documento
     */
    private String tipoDocumento;
    
    /**
     * Dirección local talonario
     */
    private String direccionLocalTalonario;
    
    /**
     * Local talonario
     */
    private String localTalonario;
    
    /**
     * Identificador externo local talonario
     */
    private String idExternoLocalTalonario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(String timbrado) {
        this.timbrado = timbrado;
    }

    public Integer getInicio() {
        return inicio;
    }

    public void setInicio(Integer inicio) {
        this.inicio = inicio;
    }

    public Integer getFin() {
        return fin;
    }

    public void setFin(Integer fin) {
        this.fin = fin;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    public String getNroSucursal() {
        return nroSucursal;
    }

    public void setNroSucursal(String nroSucursal) {
        this.nroSucursal = nroSucursal;
    }

    public String getNroPuntoVenta() {
        return nroPuntoVenta;
    }

    public void setNroPuntoVenta(String nroPuntoVenta) {
        this.nroPuntoVenta = nroPuntoVenta;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getNroResolucion() {
        return nroResolucion;
    }

    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    public Date getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(Date fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Character getDigital() {
        return digital;
    }

    public void setDigital(Character digital) {
        this.digital = digital;
    }

    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Integer idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDireccionLocalTalonario() {
        return direccionLocalTalonario;
    }

    public void setDireccionLocalTalonario(String direccionLocalTalonario) {
        this.direccionLocalTalonario = direccionLocalTalonario;
    }

    public String getLocalTalonario() {
        return localTalonario;
    }

    public void setLocalTalonario(String localTalonario) {
        this.localTalonario = localTalonario;
    }

    public String getIdExternoLocalTalonario() {
        return idExternoLocalTalonario;
    }

    public void setIdExternoLocalTalonario(String idExternoLocalTalonario) {
        this.idExternoLocalTalonario = idExternoLocalTalonario;
    }
    }
