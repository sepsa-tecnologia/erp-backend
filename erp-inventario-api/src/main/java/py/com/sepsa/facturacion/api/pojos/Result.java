/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.pojos;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.ws.rs.core.Response;
import py.com.sepsa.utils.rest.parameters.CommonParam;
import py.com.sepsa.utils.rest.parameters.MensajePojo;

/**
 * Clase para el manejo de las respuestas
 * @author Jonathan
 */
public class Result extends AbstractResult {
    
    public Result() {
        this.status = Response.Status.BAD_REQUEST;
        this.success = false;
    }
    
    public Result ok() {
        this.status = Response.Status.OK;
        this.success = true;
        this.responseCode = ResponseCode.SERVICE_OK;
        return this;
    }
    
    public Result br() {
        this.status = Response.Status.BAD_REQUEST;
        this.success = false;
        this.responseCode = ResponseCode.SERVICE_BAD_REQUEST;
        return this;
    }
    
    public Result fb() {
        this.status = Response.Status.FORBIDDEN;
        this.success = false;
        this.responseCode = ResponseCode.ACCESS_FORBIDDEN;
        return this;
    }
    
    public Result ko() {
        this.status = Response.Status.INTERNAL_SERVER_ERROR;
        this.success = false;
        this.responseCode = ResponseCode.SERVICE_KO;
        return this;
    }
    
    public Result un() {
        this.status = Response.Status.UNAUTHORIZED;
        this.success = false;
        this.responseCode = ResponseCode.ACCESS_DENIED;
        return this;
    }
    
    /**
     * Bandera que indica si el servicio se ejecuto con éxito
     */
    private boolean success;
    
    /**
     * Código de respuesta
     */
    private ResponseCode responseCode;
    
    /**
     * Lista de parámetros
     */
    private CommonParam parametros;
    
    /**
     * Datos asociados a la respuesta
     */
    protected JsonElement payload;
    
    /**
     * Verifica si el servicio se ejecuto con éxito
     * @return Si el servicio se ejecuto con éxito
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Setea si el servicio se ejecuto con éxito
     * @param success Bandera que indica si el servicio se ejecuto con éxito
     * @return Result
     */
    public Result setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    /**
     * Obtiene el código de respuesta
     * @return Código de respuesta
     */
    public ResponseCode getResponseCode() {
        return responseCode;
    }
    
    /**
     * Setea los parámetros
     * @param parametros parámetros
     * @return Result
     */
    public Result setParametros(CommonParam parametros) {
        this.parametros = parametros;
        return this;
    }
    
    /**
     * Setea el código de respuesta
     * @param responseCode Código de respuesta
     * @return Result
     */
    public Result setResponseCode(ResponseCode responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    /**
     * Obtiene los datos asociados a la respuesta
     * @return Datos asociados a la respuesta
     */
    public JsonElement getPayload() {
        return payload;
    }

    /**
     * Setea los datos asociados a la respuesta
     * @param payload Datos asociados a la respuesta
     * @return Result
     */
    public Result setPayload(JsonElement payload) {
        this.payload = payload;
        return this;
    }

    @Override
    public String getJsonResponse() {
        
        JsonObject json = new JsonObject();
        json.addProperty("success", success);

        JsonObject status = new JsonObject();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        String date = sdf.format(Calendar.getInstance().getTime());
        status.addProperty("stamp", date);
        
        if(responseCode != null) {
            status.addProperty("description", responseCode.getDescription());
        }
        
        if(parametros != null && parametros.tieneErrores()) {
            
            JsonArray mensajes = new JsonArray();
            
            for (MensajePojo error : parametros.getErrores()) {
                
                JsonObject element = new JsonObject();
                element.addProperty("descripcion", error.getDescripcion());
                
                mensajes.add(element);
            }
            
            status.add("mensajes", mensajes);
        }
        
        json.add("status", status);
        
        generatePayload();
        
        if(payload != null) {
            json.add("payload", payload);
        }
        
        return json.toString();
    }

    @Override
    public void generatePayload() {}

    /**
     * Crea una instancia de Result
     * @return Result
     */
    public static Result createInstance() {
        return new Result();
    }
}
