/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.inventario;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.inventario.Inventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioParam;
import py.com.sepsa.erp.ejb.entities.inventario.pojos.InventarioPojo;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.facturacion.api.pojos.ResultList;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
@Path("/inventario")
@Log4j2
public class InventarioService extends AbstractService<Inventario, InventarioParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @GET
    @Path("/resumen")
    @RolesAllowed({"SEPSA_ERP"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response findResumen(@BeanParam InventarioParam param) {

        if (!param.isValidToList()) {
            return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .build();
        }

        try {
            
            param.log("Método: findResumen");

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            switch(cp.getUserInfo().getCodigoTipoEmpresa()) {
                case "CLIENTE":
                    param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
                    break;
            }
            
            List<InventarioPojo> lista = facades.getInventarioFacade().findResumen(param);
            Long totalSize = facades.getInventarioFacade().findResumenSize(param);
            
            return ResultList.createInstance()
                    .storeData(param, lista, totalSize)
                    .build();

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<Inventario, InventarioParam, UserInfoImpl> getFacade() {
        return facades.getInventarioFacade();
    }
}