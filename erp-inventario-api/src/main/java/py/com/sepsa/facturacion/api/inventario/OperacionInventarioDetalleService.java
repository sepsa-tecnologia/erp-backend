/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.inventario;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.erp.ejb.entities.inventario.OperacionInventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.OperacionInventarioDetalleParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.utils.facades.Facade;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Path("/operacion-inventario-detalle")
public class OperacionInventarioDetalleService extends AbstractService<OperacionInventarioDetalle, OperacionInventarioDetalleParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @Override
    public Facade<OperacionInventarioDetalle, OperacionInventarioDetalleParam, UserInfoImpl> getFacade() {
        return facades.getOperacionInventarioDetalleFacade();
    }
}
