/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.pojos;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.parameters.CommonParam;

/**
 * Clase para el manejo de resultado de listas
 * @author Jonathan
 */
public class ResultList extends Result {
    /**
     * Cantidad total de registros
     */
    private Number totalSize;
    
    /**
     * Tamaño de la página
     */
    private Integer pageSize;
    
    /**
     * Primer resultado a retornar
     */
    private Integer firstResult;
    
    /**
     * Lista de resultado
     */
    private List result = new ArrayList<>();
    
    /**
     * Gson a utilizar
     */
    private Gson gson;

    @Override
    public void generatePayload() {
        
        if(gson == null) {
            gson = GsonUtils.generateDetault();
        }
        
        JsonArray data = new JsonArray();
        
        for (Object object : result) {
            data.add(gson.toJsonTree(object));
        }
        
        JsonObject result = new JsonObject();
        result.add("data", data);
        result.addProperty("totalSize", totalSize);
        result.addProperty("firstResult", firstResult);
        result.addProperty("pageSize", pageSize);
        
        this.payload = result;
    }
    
    /**
     * Actualiza los datos del objeto
     * @param param parámetros
     * @param data datos
     * @param totalSize Cantidad total de registros
     * @return ResultList
     */
    public ResultList storeData(CommonParam param, List data, Number totalSize) {
        this.firstResult = param.getFirstResult();
        this.pageSize = param.getPageSize();
        this.totalSize = totalSize;
        this.result = data;
        return this;
    }
    
    public Number getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Number totalSize) {
        this.totalSize = totalSize;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List getResult() {
        return result;
    }

    public void setResult(List result) {
        this.result = result;
    }

    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public Integer getFirstResult() {
        return firstResult;
    }

    public ResultList setGson(Gson gson) {
        this.gson = gson;
        return this;
    }

    public Gson getGson() {
        return gson;
    }
    
    /**
     * Crea una instancia de ResultList
     * @return ResultList
     */
    public static ResultList createInstance() {
        ResultList result = new ResultList();
        result.setStatus(Response.Status.OK);
        result.setSuccess(true);
        result.setResponseCode(ResponseCode.SERVICE_OK);
        return result;
    }
}
