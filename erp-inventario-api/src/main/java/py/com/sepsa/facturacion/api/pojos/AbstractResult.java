
package py.com.sepsa.facturacion.api.pojos;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * Respuesta genérica del servicio
 * @author Daniel F. Escauriza Arza
 */
public abstract class AbstractResult {
    
    /**
     * Estatus de la respuesta
     */
    protected Response.Status status;
    
    /**
     * Obtiene una respuesta al servicio
     * @return Respuesta al servicio
     */
    public Response build() {
        ResponseBuilder builder = Response
                .status(status)
                .entity(getJsonResponse());

        return builder.build();
    }

    public abstract String getJsonResponse();
    
    public abstract void generatePayload();
    
    public AbstractResult setStatus(Response.Status status) {
        this.status = status;
        return this;
    }

    public Response.Status getStatus() {
        return status;
    }
}
