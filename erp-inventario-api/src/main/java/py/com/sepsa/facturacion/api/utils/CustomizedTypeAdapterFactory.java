/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

/**
 *
 * @author Jonathan
 * @param <C>
 * @param <D>
 */
public abstract class CustomizedTypeAdapterFactory<C, D> implements TypeAdapterFactory {

    private final Class<C> customizedClass;
    private final Class<D> resultClass;

    public CustomizedTypeAdapterFactory(Class<C> customizedClass,
            Class<D> resultClass) {
        this.customizedClass = customizedClass;
        this.resultClass = resultClass;
    }

    @SuppressWarnings("unchecked") // we use a runtime check to guarantee that 'C' and 'T' are equal
    @Override
    public final <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        return type.getRawType() == customizedClass
                ? (TypeAdapter<T>) customizeMyClassAdapter(gson, (TypeToken<C>) type)
                : null;
    }

    private TypeAdapter<D> customizeMyClassAdapter(Gson gson, TypeToken<C> type) {
        
        TypeToken<D> typeToken = TypeToken.get(resultClass);
        
        final TypeAdapter<D> delegate = gson.getDelegateAdapter(this, typeToken);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<D>() {

            @Override
            public void write(JsonWriter out, Object value) throws IOException {
                D val = processValue(value);
                JsonElement tree = delegate.toJsonTree(val);
                beforeWrite(val, tree);
                elementAdapter.write(out, tree);
            }

            @Override
            public D read(JsonReader in) throws IOException {
                JsonElement tree = elementAdapter.read(in);
                afterRead(tree);
                return delegate.fromJsonTree(tree);
            }
        };
    }
    
    /**
     * Procesa el valor
     *
     * @param value valor a preprocesar
     * @return Valor
     */
    protected abstract D processValue(Object value);
    
    /**
     * Override this to muck with {@code toSerialize} before it is written to
     * the outgoing JSON stream.
     *
     * @param source
     * @param toSerialize
     */
    protected void beforeWrite(D source, JsonElement toSerialize) {
    }

    /**
     * Override this to muck with {@code deserialized} before it parsed into the
     * application type.
     *
     * @param deserialized
     */
    protected void afterRead(JsonElement deserialized) {
    }
}
