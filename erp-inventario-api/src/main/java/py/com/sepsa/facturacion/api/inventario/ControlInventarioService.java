/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.inventario;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.ByteArrayInputStream;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.inventario.ControlInventario;
import py.com.sepsa.erp.ejb.entities.inventario.filters.ControlInventarioParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 *
 * @author Jonathan D. Bernal Fernández
 */
@Path("/control-inventario")
@Log4j2
public class ControlInventarioService extends AbstractService<ControlInventario, ControlInventarioParam> {

    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearControlInventario(ControlInventarioParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: crearControlInventario");

            ControlInventario controlInventario = facades
                    .getControlInventarioFacade()
                    .createTemplate(param, cp.getUserInfo());

            if (controlInventario != null) {

                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(controlInventario, ControlInventario.class);

                return Result.createInstance().ok()
                        .setPayload(json)
                        .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Path("/cambiar-estado")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cambiarEstadoControlInventario(ControlInventarioParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: cambiarEstadoControlInventario");

            ControlInventario depositoLogistico = facades
                    .getControlInventarioFacade()
                    .changeState(param, cp.getUserInfo());

            if (depositoLogistico != null) {
                return Result.createInstance().ok().build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Path("/completar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response completarOperacionInventario(ControlInventarioParam param) {

        try {

            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal) sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.log("Método: completarOperacionInventario");

            ControlInventario depositoLogistico = facades
                    .getControlInventarioFacade()
                    .complete(param, cp.getUserInfo());

            if (depositoLogistico != null) {
                return Result.createInstance().ok().build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }

    @RolesAllowed({"SEPSA_ERP"})
    @GET
    @Path("/consulta/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM})
    public Response consultarPdfControlInventario(@PathParam("id") Integer id, @BeanParam ControlInventarioParam param) {

        try {
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            param.setIdEmpresa(cp.getUserInfo().getIdEmpresa());
            param.setId(id);
            param.log("Método: consultarPdfControlInventario");

            byte[] bytes = facades.getControlInventarioFacade().getPdfControlInventario(param, cp.getUserInfo());
            
            if (bytes != null) {
                //Para octet stream
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);

                Response.ResponseBuilder responseBuilder = Response.ok(input);
                responseBuilder.type("application/pdf");
                responseBuilder.header("Content-Disposition",
                        String.format("attachment; filename=\"%d.pdf\"", param.getId()));
                return responseBuilder.build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<ControlInventario, ControlInventarioParam, UserInfoImpl> getFacade() {
        return facades.getControlInventarioFacade();
    }
}
