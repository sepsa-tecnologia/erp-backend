/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.inventario;

import javax.inject.Inject;
import javax.ws.rs.Path;
import py.com.sepsa.erp.ejb.entities.inventario.InventarioDetalle;
import py.com.sepsa.erp.ejb.entities.inventario.filters.InventarioDetalleParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.utils.facades.Facade;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
@Path("/inventario-detalle")
public class InventarioDetalleService extends AbstractService<InventarioDetalle, InventarioDetalleParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;
    
    @Override
    public Facade<InventarioDetalle, InventarioDetalleParam, UserInfoImpl> getFacade() {
        return facades.getInventarioDetalleFacade();
    }
}
