/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.sepsa.facturacion.api.inventario;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;
import py.com.sepsa.erp.ejb.entities.inventario.DepositoLogistico;
import py.com.sepsa.erp.ejb.entities.inventario.filters.DepositoLogisticoParam;
import py.com.sepsa.erp.ejb.facades.Facades;
import py.com.sepsa.erp.ejb.utils.jwe.UserInfoImpl;
import py.com.sepsa.facturacion.api.AbstractService;
import py.com.sepsa.facturacion.api.pojos.ResponseCode;
import py.com.sepsa.facturacion.api.pojos.Result;
import py.com.sepsa.utils.facades.Facade;
import py.com.sepsa.utils.gson.GsonUtils;
import py.com.sepsa.utils.rest.CustomPrincipal;

/**
 * 
 * @author Jonathan D. Bernal Fernández
 */
@Path("/deposito-logistico")
@Log4j2
public class DepositoLogisticoService extends AbstractService<DepositoLogistico, DepositoLogisticoParam> {
    /**
     * Manejador de facades
     */
    @Inject
    private Facades facades;

    @RolesAllowed({"SEPSA_ERP"})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearDepositoLogistico(DepositoLogisticoParam param) {
        
        try {
            
            param.log("Método: crearDepositoLogistico");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            DepositoLogistico depositoLogistico = facades
                    .getDepositoLogisticoFacade()
                    .create(param, cp.getUserInfo());
            
            if(depositoLogistico != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(depositoLogistico, DepositoLogistico.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @RolesAllowed({"SEPSA_ERP"})
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarDepositoLogistico(DepositoLogisticoParam param) {

        try {
            
            param.log("Método: editarDepositoLogistico");
            
            CustomPrincipal<UserInfoImpl> cp = (CustomPrincipal)sc.getUserPrincipal();
            
            DepositoLogistico depositoLogistico = facades
                    .getDepositoLogisticoFacade()
                    .edit(param, cp.getUserInfo());
            
            if(depositoLogistico != null) {
                
                Gson gson = GsonUtils.generateDetault();
                JsonElement json = gson.toJsonTree(depositoLogistico, DepositoLogistico.class);
                
                return Result.createInstance().ok()
                    .setPayload(json)
                    .build();
            } else {
                return Result.createInstance().br()
                        .setResponseCode(ResponseCode.PARAM_ERROR)
                        .setParametros(param)
                        .build();
            }

        } catch (Exception ex) {
            log.fatal("Error", ex);
            return Result.createInstance().ko().build();
        }
    }
    
    @Override
    public Facade<DepositoLogistico, DepositoLogisticoParam, UserInfoImpl> getFacade() {
        return facades.getDepositoLogisticoFacade();
    }
}
